package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.util.AES256Utils;

@Entity
@Table(name="mst_pengajuan_asuransi_mobil")
public class MstPengajuanAsuransiMobil {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Long userId;
	
	private Date transactionDateTime;
	
	@Column(length = 200)
	private String name;
	
	@Column(length = 200)
	private String email;
	
	@Column(length = 200)
	private String phone;
	
	@Column(length = 255)
	private String nameDataUser;
	
	@Column(length = 200)
	private String emailDataUser;
	
	@Column(length = 200)
	private String phoneDataUser;
	
	@Column(length = 200)
	private String policyHolderName;
	
	@Column(length = 200)
	private String policyHolderLastName;
	
	@Column(length = 200)
	private String policyHolderEmail;
	
	@Column(length = 200)
	private String policyHolderPhone;
	
	@Column(length = 200)
	private String policyHolderBirthdate;
	
	@Column(length = 200)
	private String policyHolderIdentityNumber;
	
	@Column(length = 250)
	private String policyHolderIdentityAddress;
	
	@Column(length = 50)
	private String policyHolderZipcode;
	
	@Column(length = 100)
	private String policyPeriod;
	
	@Column(length = 50)
	private String premi;
	
	@Column(length = 50)
	private String hargaPertanggungan;
	
	@Column(length = 50)
	private String usage;
	
	@Column(length = 50)
	private String chassisNumber;
	
	@Column(length = 50)
	private String machineNumber;
	
	@Column(length = 50)
	private String totalTransaction;
	
	@Column(length = 100)
	private String sumIssured;
	
	@Column(length = 100)
	private String kontribusiDasar;
	
	@Column(length = 100)
	private String kecelakaanDiriPengemudi;
	
	@Column(length = 100)
	private String estimasiTotalKontribusi;
	
	@Column(length = 100)
	private String productTitle;
	
	@Column(length = 50)
	private String jenisAsuransi;
	
	@Column(length = 250)
	private String kendaraan;
	
	@Column(length = 250)
	private String periodePerlindungan;
	
	@Column(length = 50)
	private String kodeWilayahPlat;
	
    @Column(length = 50)
    private String kodePromo;
    
    @Column(length = 50)
    private String namaPromo;
	
	@JsonIgnore
	@Column(length = 2, name = "status")
	private String asuransiMobilStatus;
	
	@Column(length = 50)
	private String encryptionStatus = "ENCRYPTED";
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;
 
	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@JsonIgnore
	@Column(length = 50)
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public String getName() {
		if(name != null && !"".equals(name) && encryptionStatus==null) {
			return name;
		}else if(name != null && !"".equals(name)) {
			String nama = AES256Utils.decrypt(name);
			return nama;
		}else {
			return name;
		}
	}

	public void setName(String name) {
		if(name != null && !"".equals(name)) {
			String nama = AES256Utils.encrypt(name);
			this.name = nama;
		}else {
			this.name = name;
		}
	}

	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getEmail() {
		if(email != null && !"".equals(email) && encryptionStatus==null) {
			return email;
		}else if(email != null && !"".equals(email)) {
			String emailLeads = AES256Utils.decrypt(email);
			return emailLeads;
		}else {
			return email;
		}
	}

	public void setEmail(String email) {
		if(email != null && !"".equals(email)) {
			String emailLeads = AES256Utils.encrypt(email);
			this.email = emailLeads;
		}else {
			this.email = email;
		}
	}

	public String getPhone() {
		if(phone != null && !"".equals(phone) && encryptionStatus==null) {
			return phone;
		}else if(phone != null && !"".equals(phone)) {
			String tlp = AES256Utils.decrypt(phone);
			return tlp;
		}else {
			return phone;
		}
	}

	public void setPhone(String phone) {
		if(phone != null && !"".equals(phone)) {
			String tlp = AES256Utils.encrypt(phone);
			this.phone = tlp;
		}else {
			this.phone = phone;
		}
	}

	public String getJenisAsuransi() {
		return jenisAsuransi;
	}

	public void setJenisAsuransi(String jenisAsuransi) {
		this.jenisAsuransi = jenisAsuransi;
	}

	public String getKendaraan() {
		return kendaraan;
	}

	public void setKendaraan(String kendaraan) {
		this.kendaraan = kendaraan;
	}

	public String getKodeWilayahPlat() {
		return kodeWilayahPlat;
	}

	public void setKodeWilayahPlat(String kodeWilayahPlat) {
		this.kodeWilayahPlat = kodeWilayahPlat;
	}

	public String getAsuransiMobilStatus() {
		return asuransiMobilStatus;
	}

	public void setAsuransiMobilStatus(String asuransiMobilStatus) {
		this.asuransiMobilStatus = asuransiMobilStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getNameDataUser() {
		if(nameDataUser != null && !"".equals(nameDataUser) && encryptionStatus==null) {
			return nameDataUser;
		}else if(nameDataUser != null && !"".equals(nameDataUser)) {
			String namaUser = AES256Utils.decrypt(nameDataUser);
			return namaUser;
		}else {
			return nameDataUser;
		}
	}

	public void setNameDataUser(String nameDataUser) {
		if(nameDataUser != null && !"".equals(nameDataUser)) {
			String namaUser = AES256Utils.encrypt(nameDataUser);
			this.nameDataUser = namaUser;
		}else {
			this.nameDataUser = nameDataUser;
		}
	}

	public String getEmailDataUser() {
		if(emailDataUser != null && !"".equals(emailDataUser) && encryptionStatus==null) {
			return emailDataUser;
		}else if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.decrypt(emailDataUser);
			return emailUser;
		}else {
			return emailDataUser;
		}
	}

	public void setEmailDataUser(String emailDataUser) {
		if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.encrypt(emailDataUser);
			this.emailDataUser = emailUser;
		}else {
			this.emailDataUser = emailDataUser;
		}
	}

	public String getPhoneDataUser() {
		if(phoneDataUser != null && !"".equals(phoneDataUser) && encryptionStatus==null) {
			return phoneDataUser;
		}else if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.decrypt(phoneDataUser);
			return phoneUser;
		}else {
			return phoneDataUser;
		}
	}

	public void setPhoneDataUser(String phoneDataUser) {
		if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.encrypt(phoneDataUser);
			this.phoneDataUser = phoneUser;
		}else {
			this.phoneDataUser = phoneDataUser;
		}
	}

	public String getPolicyHolderName() {
		if(policyHolderName != null && !"".equals(policyHolderName) && encryptionStatus==null) {
			return policyHolderName;
		}else if(policyHolderName != null && !"".equals(policyHolderName)) {
			String namaPolis = AES256Utils.decrypt(policyHolderName);
			return namaPolis;
		}else {
			return policyHolderName;
		}
	}

	public void setPolicyHolderName(String policyHolderName) {
		if(policyHolderName != null && !"".equals(policyHolderName)) {
			String namaPolis = AES256Utils.encrypt(policyHolderName);
			this.policyHolderName = namaPolis;
		}else {
			this.policyHolderName = policyHolderName;
		}
	}

	public String getPolicyHolderLastName() {
		if(policyHolderLastName != null && !"".equals(policyHolderLastName) && encryptionStatus==null) {
			return policyHolderLastName;
		}else if(policyHolderLastName != null && !"".equals(policyHolderLastName)) {
			String namaLastPolis = AES256Utils.decrypt(policyHolderLastName);
			return namaLastPolis;
		}else {
			return policyHolderLastName;
		}
	}

	public void setPolicyHolderLastName(String policyHolderLastName) {
		if(policyHolderLastName != null && !"".equals(policyHolderLastName)) {
			String namaLastPolis = AES256Utils.encrypt(policyHolderLastName);
			this.policyHolderLastName = namaLastPolis;
		}else {
			this.policyHolderLastName = policyHolderLastName;
		}
	}

	public String getPolicyHolderEmail() {
		if(policyHolderEmail != null && !"".equals(policyHolderEmail) && encryptionStatus==null) {
			return policyHolderEmail;
		}else if(policyHolderEmail != null && !"".equals(policyHolderEmail)) {
			String policyEmail = AES256Utils.decrypt(policyHolderEmail);
			return policyEmail;
		}else {
			return policyHolderEmail;
		}
	}

	public void setPolicyHolderEmail(String policyHolderEmail) {
		if(policyHolderEmail != null && !"".equals(policyHolderEmail)) {
			String policyEmail = AES256Utils.encrypt(policyHolderEmail);
			this.policyHolderEmail = policyEmail;
		}else {
			this.policyHolderEmail = policyHolderEmail;
		}
	}

	public String getPolicyHolderPhone() {
		if(policyHolderPhone != null && !"".equals(policyHolderPhone) && encryptionStatus==null) {
			return policyHolderPhone;
		}else if(policyHolderPhone != null && !"".equals(policyHolderPhone)) {
			String policyPhone = AES256Utils.decrypt(policyHolderPhone);
			return policyPhone;
		}else {
			return policyHolderPhone;
		}
	}

	public void setPolicyHolderPhone(String policyHolderPhone) {
		if(policyHolderPhone != null && !"".equals(policyHolderPhone)) {
			String policyPhone = AES256Utils.encrypt(policyHolderPhone);
			this.policyHolderPhone = policyPhone;
		}else {
			this.policyHolderPhone = policyHolderPhone;
		}
	}

	public String getPolicyHolderBirthdate() {
		if(policyHolderBirthdate != null && !"".equals(policyHolderBirthdate) && encryptionStatus==null) {
			return policyHolderBirthdate;
		}else if(policyHolderBirthdate != null && !"".equals(policyHolderBirthdate)) {
			String birthdatePolis = AES256Utils.decrypt(policyHolderBirthdate);
			return birthdatePolis;
		}else {
			return policyHolderBirthdate;
		}
	}

	public void setPolicyHolderBirthdate(String policyHolderBirthdate) {
		if(policyHolderBirthdate != null && !"".equals(policyHolderBirthdate)) {
			String birthdatePolis = AES256Utils.encrypt(policyHolderBirthdate);
			this.policyHolderBirthdate = birthdatePolis;
		}else {
			this.policyHolderBirthdate = policyHolderBirthdate;
		}
	}

	public String getPolicyHolderIdentityNumber() {
		if(policyHolderIdentityNumber != null && !"".equals(policyHolderIdentityNumber) && encryptionStatus==null) {
			return policyHolderIdentityNumber;
		}else if(policyHolderIdentityNumber != null && !"".equals(policyHolderIdentityNumber)) {
			String identityPolis = AES256Utils.decrypt(policyHolderIdentityNumber);
			return identityPolis;
		}else {
			return policyHolderIdentityNumber;
		}
	}

	public void setPolicyHolderIdentityNumber(String policyHolderIdentityNumber) {
		if(policyHolderIdentityNumber != null && !"".equals(policyHolderIdentityNumber)) {
			String identityPolis = AES256Utils.encrypt(policyHolderIdentityNumber);
			this.policyHolderIdentityNumber = identityPolis;
		}else {
			this.policyHolderIdentityNumber = policyHolderIdentityNumber;
		}
	}

	public String getPolicyHolderIdentityAddress() {
		if(policyHolderIdentityAddress != null && !"".equals(policyHolderIdentityAddress) && encryptionStatus==null) {
			return policyHolderIdentityAddress;
		}else if(policyHolderIdentityAddress != null && !"".equals(policyHolderIdentityAddress)) {
			String policyIdentityAddress = AES256Utils.decrypt(policyHolderIdentityAddress);
			return policyIdentityAddress;
		}else {
			return policyHolderIdentityAddress;
		}
	}

	public void setPolicyHolderIdentityAddress(String policyHolderIdentityAddress) {
		if(policyHolderIdentityAddress != null && !"".equals(policyHolderIdentityAddress)) {
			String policyIdentityAddress = AES256Utils.encrypt(policyHolderIdentityAddress);
			this.policyHolderIdentityAddress = policyIdentityAddress;
		}else {
			this.policyHolderIdentityAddress = policyHolderIdentityAddress;
		}
	}

	public String getPolicyHolderZipcode() {
		return policyHolderZipcode;
	}

	public void setPolicyHolderZipcode(String policyHolderZipcode) {
		this.policyHolderZipcode = policyHolderZipcode;
	}

	public String getPolicyPeriod() {
		return policyPeriod;
	}

	public void setPolicyPeriod(String policyPeriod) {
		this.policyPeriod = policyPeriod;
	}

	public String getTotalTransaction() {
		return totalTransaction;
	}

	public void setTotalTransaction(String totalTransaction) {
		this.totalTransaction = totalTransaction;
	}

	public String getPremi() {
		return premi;
	}

	public void setPremi(String premi) {
		this.premi = premi;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getChassisNumber() {
		return chassisNumber;
	}

	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}

	public String getMachineNumber() {
		return machineNumber;
	}

	public void setMachineNumber(String machineNumber) {
		this.machineNumber = machineNumber;
	}

	public String getSumIssured() {
		return sumIssured;
	}

	public void setSumIssured(String sumIssured) {
		this.sumIssured = sumIssured;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getNamaPromo() {
		return namaPromo;
	}

	public void setNamaPromo(String namaPromo) {
		this.namaPromo = namaPromo;
	}

	public String getKodePromo() {
		return kodePromo;
	}

	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}

	public String getKontribusiDasar() {
		return kontribusiDasar;
	}

	public void setKontribusiDasar(String kontribusiDasar) {
		this.kontribusiDasar = kontribusiDasar;
	}

	public String getKecelakaanDiriPengemudi() {
		return kecelakaanDiriPengemudi;
	}

	public void setKecelakaanDiriPengemudi(String kecelakaanDiriPengemudi) {
		this.kecelakaanDiriPengemudi = kecelakaanDiriPengemudi;
	}

	public String getEstimasiTotalKontribusi() {
		return estimasiTotalKontribusi;
	}

	public void setEstimasiTotalKontribusi(String estimasiTotalKontribusi) {
		this.estimasiTotalKontribusi = estimasiTotalKontribusi;
	}

	public String getHargaPertanggungan() {
		return hargaPertanggungan;
	}

	public void setHargaPertanggungan(String hargaPertanggungan) {
		this.hargaPertanggungan = hargaPertanggungan;
	}

	public String getPeriodePerlindungan() {
		return periodePerlindungan;
	}

	public void setPeriodePerlindungan(String periodePerlindungan) {
		this.periodePerlindungan = periodePerlindungan;
	}

}
