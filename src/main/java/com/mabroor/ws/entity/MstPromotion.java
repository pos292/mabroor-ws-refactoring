package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_promotion")
public class MstPromotion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 200)
	private String title;
	
	@Column(length = 3)
	private String bestDeal;
	
	@Column(length = 3)
	private String statusDP;
	
	@Column(length = 100)
	private String ctaButton;

	@Column(length = 500000)
	private String description;
	
	@Column(length = 150)
	private String descriptionNotif;
	
	@ManyToOne
	@JoinColumn(name = "id_main_product")
	private MstMainProduct idMainProduct;
	
	@ManyToOne
	@JoinColumn(name = "id_main_product_child")
	private MstMainProductChild idMainProductChild;
	
	private String banner;
	
	@Column(length = 150)
	private String titleNotif;
	
	@Column(length = 200)
	private String promoCode;

	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Asia/Jakarta")
	private Date startDate;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Asia/Jakarta")
	private Date endDate;
	
	@Column(length = 500000)
	private String termsAndConditions;
	
	@Column(length = 5)
	private String isActivePromo;
	
	@Column(length = 5)
	private String isActiveHomepage;
	
	@Column(length = 5)
	private String isActiveHomepageLandingPage;
	
	@Column(length = 2, name = "status")
	private String promotionStatus;
	
	@Column(length = 2, name = "notifStatus")
	private String promotionNotifStatus;
	
	private String pushNotificationState;
	
	@Column(length = 20)
	private String blastNotification;
	
	@Column(length = 1000)
	private String directLink;
	
	private String fileExcel;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;
	
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;
	
	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MstMainProduct getIdMainProduct() {
		return idMainProduct;
	}

	public void setIdMainProduct(MstMainProduct idMainProduct) {
		this.idMainProduct = idMainProduct;
	}

	public MstMainProductChild getIdMainProductChild() {
		return idMainProductChild;
	}

	public void setIdMainProductChild(MstMainProductChild idMainProductChild) {
		this.idMainProductChild = idMainProductChild;
	}

	public String getBanner() {
		return banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public String getIsActivePromo() {
		return isActivePromo;
	}

	public void setIsActivePromo(String isActivePromo) {
		this.isActivePromo = isActivePromo;
	}

	public String getIsActiveHomepage() {
		return isActiveHomepage;
	}

	public void setIsActiveHomepage(String isActiveHomepage) {
		this.isActiveHomepage = isActiveHomepage;
	}

	public String getIsActiveHomepageLandingPage() {
		return isActiveHomepageLandingPage;
	}

	public void setIsActiveHomepageLandingPage(String isActiveHomepageLandingPage) {
		this.isActiveHomepageLandingPage = isActiveHomepageLandingPage;
	}

	public String getPromotionStatus() {
		return promotionStatus;
	}

	public void setPromotionStatus(String promotionStatus) {
		this.promotionStatus = promotionStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getTitleNotif() {
		return titleNotif;
	}

	public void setTitleNotif(String titleNotif) {
		this.titleNotif = titleNotif;
	}

	public String getDescriptionNotif() {
		return descriptionNotif;
	}

	public void setDescriptionNotif(String descriptionNotif) {
		this.descriptionNotif = descriptionNotif;
	}

	public String getPromotionNotifStatus() {
		return promotionNotifStatus;
	}

	public void setPromotionNotifStatus(String promotionNotifStatus) {
		this.promotionNotifStatus = promotionNotifStatus;
	}

	public String getPushNotificationState() {
		return pushNotificationState;
	}

	public void setPushNotificationState(String pushNotificationState) {
		this.pushNotificationState = pushNotificationState;
	}

	public String getBestDeal() {
		return bestDeal;
	}

	public void setBestDeal(String bestDeal) {
		this.bestDeal = bestDeal;
	}

	public String getStatusDP() {
		return statusDP;
	}

	public void setStatusDP(String statusDP) {
		this.statusDP = statusDP;
	}

	public String getBlastNotification() {
		return blastNotification;
	}

	public void setBlastNotification(String blastNotification) {
		this.blastNotification = blastNotification;
	}

	public String getDirectLink() {
		return directLink;
	}

	public void setDirectLink(String directLink) {
		this.directLink = directLink;
	}

	public String getFileExcel() {
		return fileExcel;
	}

	public void setFileExcel(String fileExcel) {
		this.fileExcel = fileExcel;
	}

	public String getCtaButton() {
		return ctaButton;
	}

	public void setCtaButton(String ctaButton) {
		this.ctaButton = ctaButton;
	}
}
