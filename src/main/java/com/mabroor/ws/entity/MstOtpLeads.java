package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.util.AES256Utils;

@Entity
@Table(name = "mst_otp_leads")
public class MstOtpLeads {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 4)
	private String otpCode;

	@Column(length = 50)
	private String phoneNumber;

	@Column(length = 50)
	private String email;

	@Column(length = 200)
	private String name;

	@Column(length = 50)
	private String goals;

	//	@Temporal(TemporalType.DATE)
	//	@Column(length = 50)
	//    private Date birthDate;

	@Column(length = 200)
	private String birthDate;
	
	@Column(length = 50)
	private String encryptionStatus = "ENCRYPTED";

	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable= false)
	private Date otpExpiry;

	@Column(name = "is_verified")
	private Boolean isVerified;

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getOtpExpiry() {
		return otpExpiry;
	}

	public void setOtpExpiry(Date otpExpiry) {
		this.otpExpiry = otpExpiry;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getName() {
		if(name != null && !"".equals(name)) {
			String nama = AES256Utils.decrypt(name);
			return nama;
		}else {
			return name;
		}
	}

	public void setName(String name) {
		if(name != null && !"".equals(name)) {
			String nama = AES256Utils.encrypt(name);
			this.name = nama;
		}else {
			this.name = name;
		}
	}

	public String getGoals() {
		return goals;
	}

	public void setGoals(String goals) {
		this.goals = goals;
	}

	public String getBirthDate() {
		if(birthDate != null && !"".equals(birthDate)) {
			String birth = AES256Utils.decrypt(birthDate);
			return birth;
		}else {
			return birthDate;
		}
	}

	public void setBirthDate(String birthDate) {
		if(birthDate != null && !"".equals(birthDate)) {
			String birth = AES256Utils.encrypt(birthDate);
			this.birthDate = birth;
		}else {
			this.birthDate = birthDate;
		}
	}

}
