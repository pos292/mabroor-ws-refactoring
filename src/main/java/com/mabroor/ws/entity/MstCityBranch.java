package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "mst_citybranch")
public class MstCityBranch {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 100)
	private String city;
	
	@Column(length = 100)
	private String officeCode;
	
	@Column(length = 100)
	private String officeName;
	
	@Column(length = 3)
	private String accAvailable;
	
	@JsonIgnore
	@Column(length = 2, name = "status")
	private String cityBranchStatus;
	
	@Column(length = 3)
	private String tafAvailable;
	
	private Long idKabupaten;
	
	@CreationTimestamp
	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAccAvailable() {
		return accAvailable;
	}

	public void setAccAvailable(String accAvailable) {
		this.accAvailable = accAvailable;
	}

	public String getCityBranchStatus() {
		return cityBranchStatus;
	}

	public void setCityBranchStatus(String cityBranchStatus) {
		this.cityBranchStatus = cityBranchStatus;
	}

	public String getTafAvailable() {
		return tafAvailable;
	}

	public void setTafAvailable(String tafAvailable) {
		this.tafAvailable = tafAvailable;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getIdKabupaten() {
		return idKabupaten;
	}

	public void setIdKabupaten(Long idKabupaten) {
		this.idKabupaten = idKabupaten;
	}
	
}
