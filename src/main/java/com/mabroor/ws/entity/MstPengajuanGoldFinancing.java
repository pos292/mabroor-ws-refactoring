package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.util.AES256Utils;

@Entity
@Table(name="mst_pengajuan_gold_financing")
public class MstPengajuanGoldFinancing {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Long userId;
	
	private Date transactionDateTime;
	
	@Column(length = 200)
	private String name;
	
	@Column(length = 200)
	private String email;
	
	@Column(length = 200)
	private String phone;
	
	@Column(length = 255)
	private String nameDataUser;
	
	@Column(length = 200)
	private String emailDataUser;
	
	@Column(length = 200)
	private String phoneDataUser;
	
	@Column(length = 200)
	private String namaDepan;

	@Column(length = 200)
	private String namaBelakang;
	
	@JsonIgnore
	@Column(length = 2, name = "status")
	private String goldFinancingStatus;
	
	@Column(length = 50)
	private String encryptionStatus = "ENCRYPTED";
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;
 
	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@JsonIgnore
	@Column(length = 50)
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getName() {
		if(name != null && !"".equals(name) && encryptionStatus==null) {
			return name;
		}else if(name != null && !"".equals(name)) {
			String nama = AES256Utils.decrypt(name);
			return nama;
		}else {
			return name;
		}
	}

	public void setName(String name) {
		if(name != null && !"".equals(name)) {
			String nama = AES256Utils.encrypt(name);
			this.name = nama;
		}else {
			this.name = name;
		}
	}

	public String getEmail() {
		if(email != null && !"".equals(email) && encryptionStatus==null) {
			return email;
		}else if(email != null && !"".equals(email)) {
			String emailLeads = AES256Utils.decrypt(email);
			return emailLeads;
		}else {
			return email;
		}
	}

	public void setEmail(String email) {
		if(email != null && !"".equals(email)) {
			String emailLeads = AES256Utils.encrypt(email);
			this.email = emailLeads;
		}else {
			this.email = email;
		}
	}

	public String getPhone() {
		if(phone != null && !"".equals(phone) && encryptionStatus==null) {
			return phone;
		}else if(phone != null && !"".equals(phone)) {
			String tlp = AES256Utils.decrypt(phone);
			return tlp;
		}else {
			return phone;
		}
	}

	public void setPhone(String phone) {
		if(phone != null && !"".equals(phone)) {
			String tlp = AES256Utils.encrypt(phone);
			this.phone = tlp;
		}else {
			this.phone = phone;
		}
	}

	public String getGoldFinancingStatus() {
		return goldFinancingStatus;
	}

	public void setGoldFinancingStatus(String goldFinancingStatus) {
		this.goldFinancingStatus = goldFinancingStatus;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getNameDataUser() {
		if(nameDataUser != null && !"".equals(nameDataUser) && encryptionStatus==null) {
			return nameDataUser;
		}else if(nameDataUser != null && !"".equals(nameDataUser)) {
			String namaUser = AES256Utils.decrypt(nameDataUser);
			return namaUser;
		}else {
			return nameDataUser;
		}
	}

	public void setNameDataUser(String nameDataUser) {
		if(nameDataUser != null && !"".equals(nameDataUser)) {
			String namaUser = AES256Utils.encrypt(nameDataUser);
			this.nameDataUser = namaUser;
		}else {
			this.nameDataUser = nameDataUser;
		}
	}

	public String getEmailDataUser() {
		if(emailDataUser != null && !"".equals(emailDataUser) && encryptionStatus==null) {
			return emailDataUser;
		}else if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.decrypt(emailDataUser);
			return emailUser;
		}else {
			return emailDataUser;
		}
	}

	public void setEmailDataUser(String emailDataUser) {
		if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.encrypt(emailDataUser);
			this.emailDataUser = emailUser;
		}else {
			this.emailDataUser = emailDataUser;
		}
	}

	public String getPhoneDataUser() {
		if(phoneDataUser != null && !"".equals(phoneDataUser) && encryptionStatus==null) {
			return phoneDataUser;
		}else if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.decrypt(phoneDataUser);
			return phoneUser;
		}else {
			return phoneDataUser;
		}
	}

	public void setPhoneDataUser(String phoneDataUser) {
		if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.encrypt(phoneDataUser);
			this.phoneDataUser = phoneUser;
		}else {
			this.phoneDataUser = phoneDataUser;
		}
	}
	
	public String getNamaDepan() {
		if(namaDepan != null && !"".equals(namaDepan) && encryptionStatus==null) {
			return namaDepan;
		}else if(namaDepan != null && !"".equals(namaDepan)) {
			String nameFirst = AES256Utils.decrypt(namaDepan);
			return nameFirst;
		}else {
			return namaDepan;
		}
	}

	public void setNamaDepan(String namaDepan) {
		if(namaDepan != null && !"".equals(namaDepan)) {
			String nameFirst = AES256Utils.encrypt(namaDepan);
			this.namaDepan = nameFirst;
		}else {
			this.namaDepan = namaDepan;
		}
	}

	public String getNamaBelakang() {
		if(namaBelakang != null && !"".equals(namaBelakang) && encryptionStatus==null) {
			return namaBelakang;
		}else if(namaBelakang != null && !"".equals(namaBelakang)) {
			String nameLast = AES256Utils.decrypt(namaBelakang);
			return nameLast;
		}else {
			return namaBelakang;
		}
	}

	public void setNamaBelakang(String namaBelakang) {
		if(namaBelakang != null && !"".equals(namaBelakang)) {
			String nameLast = AES256Utils.encrypt(namaBelakang);
			this.namaBelakang = nameLast;
		}else {
			this.namaBelakang = namaBelakang;
		}
	}
	
}
