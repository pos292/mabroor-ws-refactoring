package com.mabroor.ws.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_pembiayaan_qurban")
public class MstPembiayaanQurban {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "nama_partner")
	private MstPartnerQurban namaPartner;
	
	@Column(length = 250)
	private String namaHewan;
	
	private String icon;
	
	@Column(length = 100)
	private String berat;
	
	@Column(length = 50)
	private BigDecimal harga = BigDecimal.ZERO;
	
	@Column(length = 250)
	private String urlWebviewPayment;
	
	@Column(length = 2, name = "status")
	private String qurbanStatus;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@JsonIgnore
	@Column(length = 50)
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MstPartnerQurban getNamaPartner() {
		return namaPartner;
	}

	public void setNamaPartner(MstPartnerQurban namaPartner) {
		this.namaPartner = namaPartner;
	}

	public String getNamaHewan() {
		return namaHewan;
	}

	public void setNamaHewan(String namaHewan) {
		this.namaHewan = namaHewan;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getBerat() {
		return berat;
	}

	public void setBerat(String berat) {
		this.berat = berat;
	}

	public BigDecimal getHarga() {
		return harga;
	}

	public void setHarga(BigDecimal harga) {
		this.harga = harga;
	}

	public String getUrlWebviewPayment() {
		return urlWebviewPayment;
	}

	public void setUrlWebviewPayment(String urlWebviewPayment) {
		this.urlWebviewPayment = urlWebviewPayment;
	}

	public String getQurbanStatus() {
		return qurbanStatus;
	}

	public void setQurbanStatus(String qurbanStatus) {
		this.qurbanStatus = qurbanStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}

