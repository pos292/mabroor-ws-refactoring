package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.util.AES256Utils;

@Entity
@Table(name="mst_wisata_religi")
public class MstWisataReligi {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private Long purposeId;

	@Column(length = 100)
	private String integrationLeadsId;
	
	@Column(length = 200)
	private String nama;

	@Column(length = 200)
	private String namaDepan;

	@Column(length = 200)
	private String namaBelakang;

	@Column(length = 200)
	private String tanggalLahir;

	@Column(length = 200)
	private String alamatTempatTinggal;

	@Column(length = 200)
	private String provinsi;
	
	@Column(length = 50)
	private String provinsiId;

	@Column(length = 200)
	private String kota;
	
	@Column(length = 200)
	private String kabupatenId;

	@Column(length = 200)
	private String kecamatan;
	
	@Column(length = 50)
	private String kecamatanId;
	
	@Column(length = 200)
	private String kodePos;
	
	@Column(length = 200)
	private String kodePosId;
	
	@Column(length = 50)
	private String kelurahan;
	
	@Column(length = 50)
	private String kelurahanId;

	@Column(length = 200)
	private String alamatSesuaiKtp;

	@Column(length = 200)
	private String kodePosKtp;

	@Column(length = 200)
	private String email;

	@Column(length = 200)
	private String noHandphone;

	@Column(length = 200)
	private String productChoose;

	@Column(length = 200)
	private String productPurchase;
	
	@Column(length = 200)
	private String productPurchaseCode;

	@Column(length = 200)
	private String tenor;

	@Column(length = 200)
	private String dateDispatch;

	@Column(length = 50)
	private String zipCode;
	
	@Column(length = 50)
	private String jumlahPeserta;
	
	@Column(length = 50)
	private String partnerPembiayaan;

	@Column(length = 200)
	private String lokasiCabang;

	@Column(length = 50)
	private String amountDp;

	@Column(length = 50)
	private String amountCicilan;

	@Column(length = 50)
	private String amountCostTotal;
	
	@Column(length = 50)
	private String rt;

	@Column(length = 50)
	private String rw;

	@Column(length = 50)
	private String namaIbuKandung;

	@Column(length = 50)
	private String purchaseONH;

	@JsonIgnore
	@Column(length = 2, name = "status")
	private String wisataStatus;

	private Long userId;

	@Column(length = 200)
	private String nameDataUser;

	@Column(length = 200)
	private String emailDataUser;

	@Column(length = 50)
	private String phoneDataUser;

	@Column(length = 50)
	private String kodePromo;

	@Column(length = 50)
	private String namaPromo;
	
	@Column(length = 50)
	private String gender;
	
	@Column(length = 100)
	private String noKtp;
	
	@JsonIgnore
	@Column(length = 50)
	private String encryptionStatus = "ENCRYPTED";
	
	@Column(length = 50)
	private String source = "MABROOR";

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date transactionDateTime;

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@JsonIgnore
	@Column(length = 50)
	private String updatedBy;
	
	

	public String getAmountDp() {
		return amountDp;
	}

	public void setAmountDp(String amountDp) {
		this.amountDp = amountDp;
	}

	public String getKodePromo() {
		return kodePromo;
	}

	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}

	public String getNamaPromo() {
		return namaPromo;
	}

	public void setNamaPromo(String namaPromo) {
		this.namaPromo = namaPromo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public String getKodePosId() {
		return kodePosId;
	}

	public void setKodePosId(String kodePosId) {
		this.kodePosId = kodePosId;
	}

	public String getNama() {
		if(nama != null && !"".equals(nama) && encryptionStatus==null) {
			return nama;
		}else if(nama != null && !"".equals(nama)) {
			String name = AES256Utils.decrypt(nama);
			return name;
		}else {
			return nama;
		}
	}

	public void setNama(String nama) {
		if(nama != null && !"".equals(nama)) {
			String name = AES256Utils.encrypt(nama);
			this.nama = name;
		}else {
			this.nama = nama;
		}
	}

	public String getNamaDepan() {
		if(namaDepan != null && !"".equals(namaDepan) && encryptionStatus==null) {
			return namaDepan;
		}else if(namaDepan != null && !"".equals(namaDepan)) {
			String nameFirst = AES256Utils.decrypt(namaDepan);
			return nameFirst;
		}else {
			return namaDepan;
		}
	}

	public void setNamaDepan(String namaDepan) {
		if(namaDepan != null && !"".equals(namaDepan)) {
			String nameFirst = AES256Utils.encrypt(namaDepan);
			this.namaDepan = nameFirst;
		}else {
			this.namaDepan = namaDepan;
		}
	}

	public String getNamaBelakang() {
		if(namaBelakang != null && !"".equals(namaBelakang) && encryptionStatus==null) {
			return namaBelakang;
		}else if(namaBelakang != null && !"".equals(namaBelakang)) {
			String nameLast = AES256Utils.decrypt(namaBelakang);
			return nameLast;
		}else {
			return namaBelakang;
		}
	}

	public void setNamaBelakang(String namaBelakang) {
		if(namaBelakang != null && !"".equals(namaBelakang)) {
			String nameLast = AES256Utils.encrypt(namaBelakang);
			this.namaBelakang = nameLast;
		}else {
			this.namaBelakang = namaBelakang;
		}
	}

	public String getTanggalLahir() {
		if(tanggalLahir != null && !"".equals(tanggalLahir) && encryptionStatus==null) {
			return tanggalLahir;
		}else if(tanggalLahir != null && !"".equals(tanggalLahir)) {
			String tglLahir = AES256Utils.decrypt(tanggalLahir);
			return tglLahir;
		}else {
			return tanggalLahir;
		}
	}

	public void setTanggalLahir(String tanggalLahir) {
		if(tanggalLahir != null && !"".equals(tanggalLahir)) {
			String tglLahir = AES256Utils.encrypt(tanggalLahir);
			this.tanggalLahir = tglLahir;
		}else {
			this.tanggalLahir = tanggalLahir;
		}
	}

	public String getAlamatTempatTinggal() {
		return alamatTempatTinggal;
	}

	public void setAlamatTempatTinggal(String alamatTempatTinggal) {
		this.alamatTempatTinggal = alamatTempatTinggal;
	}

	public String getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	public String getAlamatSesuaiKtp() {
		if(alamatSesuaiKtp != null && !"".equals(alamatSesuaiKtp) && encryptionStatus==null) {
			return alamatSesuaiKtp;
		}else if(alamatSesuaiKtp != null && !"".equals(alamatSesuaiKtp)) {
			String alamat = AES256Utils.decrypt(alamatSesuaiKtp);
			return alamat;
		}else {
			return alamatSesuaiKtp;
		}
	}

	public void setAlamatSesuaiKtp(String alamatSesuaiKtp) {
		if(alamatSesuaiKtp != null && !"".equals(alamatSesuaiKtp)) {
			String alamat = AES256Utils.encrypt(alamatSesuaiKtp);
			this.alamatSesuaiKtp = alamat;
		}else {
			this.alamatSesuaiKtp = alamatSesuaiKtp;
		}
	}

	public String getKodePosKtp() {
		return kodePosKtp;
	}

	public void setKodePosKtp(String kodePosKtp) {
		this.kodePosKtp = kodePosKtp;
	}

	public String getEmail() {
		if(email != null && !"".equals(email) && encryptionStatus==null) {
			return email;
		}else if(email != null && !"".equals(email)) {
			String emails = AES256Utils.decrypt(email);
			return emails;
		}else {
			return email;
		}
	}

	public void setEmail(String email) {
		if(email != null && !"".equals(email)) {
			String emails = AES256Utils.encrypt(email);
			this.email = emails;
		}else {
			this.email = email;
		}
	}

	public String getNoHandphone() {
		if(noHandphone != null && !"".equals(noHandphone) && encryptionStatus==null) {
			return noHandphone;
		}else if(noHandphone != null && !"".equals(noHandphone)) {
			String hp = AES256Utils.decrypt(noHandphone);
			return hp;
		}else {
			return noHandphone;
		}
	}

	public void setNoHandphone(String noHandphone) {
		if(noHandphone != null && !"".equals(noHandphone)) {
			String hp = AES256Utils.encrypt(noHandphone);
			this.noHandphone = hp;
		}else {
			this.noHandphone = noHandphone;
		}
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getProductChoose() {
		return productChoose;
	}

	public void setProductChoose(String productChoose) {
		this.productChoose = productChoose;
	}

	public String getDateDispatch() {
		return dateDispatch;
	}

	public void setDateDispatch(String dateDispatch) {
		this.dateDispatch = dateDispatch;
	}

	public String getWisataStatus() {
		return wisataStatus;
	}

	public void setWisataStatus(String wisataStatus) {
		this.wisataStatus = wisataStatus;
	}

	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getPurposeId() {
		return purposeId;
	}

	public void setPurposeId(Long purposeId) {
		this.purposeId = purposeId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNameDataUser() {
		if(nameDataUser != null && !"".equals(nameDataUser) && encryptionStatus==null) {
			return nameDataUser;
		}else if(nameDataUser != null && !"".equals(nameDataUser)) {
			String nameUser = AES256Utils.decrypt(nameDataUser);
			return nameUser;
		}else {
			return nameDataUser;
		}
	}

	public void setNameDataUser(String nameDataUser) {
		if(nameDataUser != null && !"".equals(nameDataUser)) {
			String nameUser = AES256Utils.encrypt(nameDataUser);
			this.nameDataUser = nameUser;
		}else {
			this.nameDataUser = nameDataUser;
		}
	}

	public String getEmailDataUser() {
		if(emailDataUser != null && !"".equals(emailDataUser) && encryptionStatus==null) {
			return emailDataUser;
		}else if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.decrypt(emailDataUser);
			return emailUser;
		}else {
			return emailDataUser;
		}
	}

	public void setEmailDataUser(String emailDataUser) {
		if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.encrypt(emailDataUser);
			this.emailDataUser = emailUser;
		}else {
			this.emailDataUser = emailDataUser;
		}
	}

	public String getPhoneDataUser() {
		if(phoneDataUser != null && !"".equals(phoneDataUser) && encryptionStatus==null) {
			return phoneDataUser;
		}else if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.decrypt(phoneDataUser);
			return phoneUser;
		}else {
			return phoneDataUser;
		}
	}

	public void setPhoneDataUser(String phoneDataUser) {
		if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.encrypt(phoneDataUser);
			this.phoneDataUser = phoneUser;
		}else {
			this.phoneDataUser = phoneDataUser;
		}
	}

	public String getProductPurchase() {
		return productPurchase;
	}

	public void setProductPurchase(String productPurchase) {
		this.productPurchase = productPurchase;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getPurchaseONH() {
		return purchaseONH;
	}

	public void setPurchaseONH(String purchaseONH) {
		this.purchaseONH = purchaseONH;
	}

	public String getIntegrationLeadsId() {
		return integrationLeadsId;
	}

	public void setIntegrationLeadsId(String integrationLeadsId) {
		this.integrationLeadsId = integrationLeadsId;
	}

	public String getProvinsiId() {
		return provinsiId;
	}

	public void setProvinsiId(String provinsiId) {
		this.provinsiId = provinsiId;
	}

	public String getKecamatanId() {
		return kecamatanId;
	}

	public void setKecamatanId(String kecamatanId) {
		this.kecamatanId = kecamatanId;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKelurahanId() {
		return kelurahanId;
	}

	public void setKelurahanId(String kelurahanId) {
		this.kelurahanId = kelurahanId;
	}

	public String getKabupatenId() {
		return kabupatenId;
	}

	public void setKabupatenId(String kabupatenId) {
		this.kabupatenId = kabupatenId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getJumlahPeserta() {
		return jumlahPeserta;
	}

	public void setJumlahPeserta(String jumlahPeserta) {
		this.jumlahPeserta = jumlahPeserta;
	}

	public String getLokasiCabang() {
		return lokasiCabang;
	}

	public void setLokasiCabang(String lokasiCabang) {
		this.lokasiCabang = lokasiCabang;
	}

	public String getAmountCicilan() {
		return amountCicilan;
	}

	public void setAmountCicilan(String amountCicilan) {
		this.amountCicilan = amountCicilan;
	}

	public String getProductPurchaseCode() {
		return productPurchaseCode;
	}

	public void setProductPurchaseCode(String productPurchaseCode) {
		this.productPurchaseCode = productPurchaseCode;
	}

	public String getPartnerPembiayaan() {
		return partnerPembiayaan;
	}

	public void setPartnerPembiayaan(String partnerPembiayaan) {
		this.partnerPembiayaan = partnerPembiayaan;
	}

	public String getAmountCostTotal() {
		return amountCostTotal;
	}

	public void setAmountCostTotal(String amountCostTotal) {
		this.amountCostTotal = amountCostTotal;
	}
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNoKtp() {
		return noKtp;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public String getRt() {
		return rt;
	}

	public void setRt(String rt) {
		this.rt = rt;
	}

	public String getRw() {
		return rw;
	}

	public void setRw(String rw) {
		this.rw = rw;
	}

	public String getNamaIbuKandung() {
		return namaIbuKandung;
	}

	public void setNamaIbuKandung(String namaIbuKandung) {
		this.namaIbuKandung = namaIbuKandung;
	}

	

}
