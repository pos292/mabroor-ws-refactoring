package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "mst_needs_denganjaminan")
public class MstNeedsDenganJaminan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 50)
	private String needsTitle;
	
	@Column(length = 50)
	private String code;
	
	@JsonIgnore
	@Column(length = 2, name = "status")
	private String pinjamanDenganJaminanStatus;
	
	@Column(length = 2)
	private int seq;

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;
	
	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@JsonIgnore
	@Column(length = 50)
	private String updatedBy;
	
	public String getNeedsTitle() {
		return needsTitle;
	}

	public void setNeedsTitle(String needsTitle) {
		this.needsTitle = needsTitle;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getPinjamanDenganJaminanStatus() {
		return pinjamanDenganJaminanStatus;
	}

	public void setPinjamanDenganJaminanStatus(String pinjamanDenganJaminanStatus) {
		this.pinjamanDenganJaminanStatus = pinjamanDenganJaminanStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
