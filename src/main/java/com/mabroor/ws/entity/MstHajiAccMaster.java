package com.mabroor.ws.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "mst_haji_acc_master")
public class MstHajiAccMaster {
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

    @Column(length = 200)
    private String productName;
    
    @Column(length = 200) //HAJIM
    private String productCode;

    @Column(length = 200)
    private String jumlahPeserta;

    @Column(length = 200)
    private String tenor;

    @Column(length = 15)
    private BigDecimal biayaAdmin;

    @Column(length = 15)
    private BigDecimal amountDp;

    @Column(length = 15)
    private BigDecimal amountCicilan;

    @Column(length = 15)
    private BigDecimal amountCostTotal;

    @JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getJumlahPeserta() {
        return jumlahPeserta;
    }

    public void setJumlahPeserta(String jumlahPeserta) {
        this.jumlahPeserta = jumlahPeserta;
    }

    public BigDecimal getAmountCostTotal() {
        return amountCostTotal;
    }

    public void setAmountCostTotal(BigDecimal amountCostTotal) {
        this.amountCostTotal = amountCostTotal;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public BigDecimal getBiayaAdmin() {
        return biayaAdmin;
    }

    public void setBiayaAdmin(BigDecimal biayaAdmin) {
        this.biayaAdmin = biayaAdmin;
    }

    public BigDecimal getAmountDp() {
        return amountDp;
    }

    public void setAmountDp(BigDecimal amountDp) {
        this.amountDp = amountDp;
    }

    public BigDecimal getAmountCicilan() {
        return amountCicilan;
    }

    public void setAmountCicilan(BigDecimal amountCicilan) {
        this.amountCicilan = amountCicilan;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    
}
