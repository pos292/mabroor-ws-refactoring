package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_promotion_bundle")
public class MstPromotionBundle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name ="id", columnDefinition = "bigint")
	private Long id;
	@Column(name ="user_id", columnDefinition = "bigint")
	private Long userId;
	@Column(name ="leads_id", columnDefinition = "bigint")
	private Long leadsId;
	@Column(name ="submission_id", columnDefinition = "bigint")
	private Long submissionId;
	
	@Column(length = 200)
	private String userName;
	
	@Column(length = 200)
	private String tanggalLahir;
	
	@Column(length = 200)
	private String noHp;
	
	@Column(length = 200)
	private String email;
	
	@Column(length = 200)
	private String gender;
	
	@Column(length = 200)
	private String submissionName;
	
	@Column(length = 200)
	private String identityNumber;
	
	@Column(length = 200)
	private String kodePromo;
	
	@Column(length = 200)
	private String productType;
	
	@Column(length = 2, name = "status")
	private String promotionBundleStatus;
	
	private Date transactionDateTime;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;
	
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;
	
	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getSubmissionId() {
		return submissionId;
	}

	public void setSubmissionId(Long submissionId) {
		this.submissionId = submissionId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getNoHp() {
		return noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSubmissionName() {
		return submissionName;
	}

	public void setSubmissionName(String submissionName) {
		this.submissionName = submissionName;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public String getKodePromo() {
		return kodePromo;
	}

	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getPromotionBundleStatus() {
		return promotionBundleStatus;
	}

	public void setPromotionBundleStatus(String promotionBundleStatus) {
		this.promotionBundleStatus = promotionBundleStatus;
	}

	public Long getLeadsId() {
		return leadsId;
	}

	public void setLeadsId(Long leadsId) {
		this.leadsId = leadsId;
	}
	
	
	
	
	
	
	
	

}
