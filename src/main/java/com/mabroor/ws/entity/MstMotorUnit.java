package com.mabroor.ws.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_motor_unit")
public class MstMotorUnit {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 150)
	private String title;

	@Column(length = 2)
	private int seq;
	
	@ManyToOne
	@JoinColumn(name = "motor_brand_id")
	private MstMotorBrand motorBrand;
	
	@ManyToOne
	@JoinColumn(name = "motor_category_id")
	private MstMotorCategory motorCategory;

	@ManyToOne
	@JoinColumn(name = "motor_type_id")
	private MstMotorType motorType;

	@ManyToOne
	@JoinColumn(name = "motor_model_id")
	private MstMotorModel motorModel;
	
	@ManyToOne
	@JoinColumn(name = "location_id")
	private MstLocations location;
	
	@Column(precision = 15, scale = 0)
	private BigDecimal price = BigDecimal.ZERO;

	@Column(precision = 15, scale = 0)
	private BigDecimal DP = BigDecimal.ZERO;

	@Column(precision = 15, scale = 0)
	private BigDecimal cicilan = BigDecimal.ZERO;
	
	@Column(length = 3)
	private String bestDeal;

	@Column(length = 2, name = "status")
	private String motorStatus;

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@JsonIgnore
	@Column(length = 50)
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public MstMotorBrand getMotorBrand() {
		return motorBrand;
	}

	public void setMotorBrand(MstMotorBrand motorBrand) {
		this.motorBrand = motorBrand;
	}
	
	public MstMotorCategory getMotorCategory() {
		return motorCategory;
	}

	public void setMotorCategory(MstMotorCategory motorCategory) {
		this.motorCategory = motorCategory;
	}

	public MstMotorType getMotorType() {
		return motorType;
	}

	public void setMotorType(MstMotorType motorType) {
		this.motorType = motorType;
	}

	public MstMotorModel getMotorModel() {
		return motorModel;
	}

	public void setMotorModel(MstMotorModel motorModel) {
		this.motorModel = motorModel;
	}

	public MstLocations getLocation() {
		return location;
	}

	public void setLocation(MstLocations location) {
		this.location = location;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDP() {
		return DP;
	}

	public void setDP(BigDecimal dP) {
		DP = dP;
	}

	public BigDecimal getCicilan() {
		return cicilan;
	}

	public void setCicilan(BigDecimal cicilan) {
		this.cicilan = cicilan;
	}

	public String getBestDeal() {
		return bestDeal;
	}

	public void setBestDeal(String bestDeal) {
		this.bestDeal = bestDeal;
	}

	public String getMotorStatus() {
		return motorStatus;
	}

	public void setMotorStatus(String motorStatus) {
		this.motorStatus = motorStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}
