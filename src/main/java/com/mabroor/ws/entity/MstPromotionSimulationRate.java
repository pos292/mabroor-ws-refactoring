package com.mabroor.ws.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_promotion_simulation_rate")
public class MstPromotionSimulationRate {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
		
	@Column(length = 200)
	private String insuranceType;
		
	private Long tenor;
		
	@Column(precision = 15, scale = 3)
	private BigDecimal ratePercentage;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "promo_id")
	private MstPromotion mstPromotion;

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public Long getTenor() {
		return tenor;
	}

	public void setTenor(Long tenor) {
		this.tenor = tenor;
	}

	public BigDecimal getRatePercentage() {
		return ratePercentage;
	}

	public void setRatePercentage(BigDecimal ratePercentage) {
		this.ratePercentage = ratePercentage;
	}
	
	public MstPromotion getMstPromotion() {
		return mstPromotion;
	}

	public void setMstPromotion(MstPromotion mstPromotion) {
		this.mstPromotion = mstPromotion;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	

}
