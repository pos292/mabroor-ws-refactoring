package com.mabroor.ws.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_motor_dimension")
public class MstMotorDimension {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 200)
	private String dimensionLong = "-";
	
	@Column(precision = 15, scale = 0)
	private BigDecimal tallSeat = BigDecimal.ZERO;
	
	@Column(precision = 15, scale = 0)
	private BigDecimal wheelAxis = BigDecimal.ZERO;
	
	@Column(precision = 15, scale = 0)
	private BigDecimal lowestGround = BigDecimal.ZERO;
	
	@Column(precision = 15, scale = 0)
	private BigDecimal curbWeight = BigDecimal.ZERO;
	
	@Column(precision = 15, scale = 0)
	private BigDecimal turningRadius = BigDecimal.ZERO;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "motor_unit_id")
	private MstMotorUnit motorUnit;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	
	public MstMotorUnit getMotorUnit() {
		return motorUnit;
	}

	public void setMotorUnit(MstMotorUnit motorUnit) {
		this.motorUnit = motorUnit;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDimensionLong() {
		return dimensionLong;
	}

	public void setDimensionLong(String dimensionLong) {
		this.dimensionLong = dimensionLong;
	}

	public BigDecimal getTallSeat() {
		return tallSeat;
	}

	public void setTallSeat(BigDecimal tallSeat) {
		this.tallSeat = tallSeat;
	}

	public BigDecimal getWheelAxis() {
		return wheelAxis;
	}

	public void setWheelAxis(BigDecimal wheelAxis) {
		this.wheelAxis = wheelAxis;
	}

	public BigDecimal getLowestGround() {
		return lowestGround;
	}

	public void setLowestGround(BigDecimal lowestGround) {
		this.lowestGround = lowestGround;
	}

	public BigDecimal getCurbWeight() {
		return curbWeight;
	}

	public void setCurbWeight(BigDecimal curbWeight) {
		this.curbWeight = curbWeight;
	}

	public BigDecimal getTurningRadius() {
		return turningRadius;
	}

	public void setTurningRadius(BigDecimal turningRadius) {
		this.turningRadius = turningRadius;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
