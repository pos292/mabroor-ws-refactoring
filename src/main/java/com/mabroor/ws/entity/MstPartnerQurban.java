package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "mst_partner_qurban")
public class MstPartnerQurban {

	@Id
	private String namaPartner;
	
	@Column(length = 250)
	private String label;
	
	@Column(length = 50)
	private String jumlahQurban;
	
	@Column(length = 2, name = "status")
	private String partnerStatus;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	public String getNamaPartner() {
		return namaPartner;
	}

	public void setNamaPartner(String namaPartner) {
		this.namaPartner = namaPartner;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getJumlahQurban() {
		return jumlahQurban;
	}

	public void setJumlahQurban(String jumlahQurban) {
		this.jumlahQurban = jumlahQurban;
	}

	public String getPartnerStatus() {
		return partnerStatus;
	}

	public void setPartnerStatus(String partnerStatus) {
		this.partnerStatus = partnerStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}
