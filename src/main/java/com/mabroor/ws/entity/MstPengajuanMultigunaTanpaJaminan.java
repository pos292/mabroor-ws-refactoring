package com.mabroor.ws.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="mst_pengajuan_multiguna_tanpa_jaminan")
public class MstPengajuanMultigunaTanpaJaminan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "pendapatan")
    private Double pendapatan;

    @Column(name = "jml_pinjaman")
    private Double jmlPinjaman;

    @Column(name = "tenor")
    private Integer tenor;

    @Column(name = "keb_pinjaman")
    private String kebPinjaman;

    @Column(name = "keb_pinjaman_lainnya")
    private String kebPinjamanLainnya;

    @Column(name = "is_kartu_kredit")
    private Boolean KartuKredit;

    @Column(name = "bank_penerbit")
    private String bankPenerbit;

    @Column(name = "status")
    private String status;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "tanggal_pengajuan")
    private Date tanggalPengajuan;

    @CreationTimestamp
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date")
    private Date updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(Double pendapatan) {
        this.pendapatan = pendapatan;
    }

    public Double getJmlPinjaman() {
        return jmlPinjaman;
    }

    public void setJmlPinjaman(Double jmlPinjaman) {
        this.jmlPinjaman = jmlPinjaman;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public String getKebPinjaman() {
        return kebPinjaman;
    }

    public void setKebPinjaman(String kebPinjaman) {
        this.kebPinjaman = kebPinjaman;
    }

    public String getKebPinjamanLainnya() {
        return kebPinjamanLainnya;
    }

    public void setKebPinjamanLainnya(String kebPinjamanLainnya) {
        this.kebPinjamanLainnya = kebPinjamanLainnya;
    }

    public Boolean getKartuKredit() {
        return KartuKredit;
    }

    public void setKartuKredit(Boolean kartuKredit) {
        KartuKredit = kartuKredit;
    }

    public String getBankPenerbit() {
        return bankPenerbit;
    }

    public void setBankPenerbit(String bankPenerbit) {
        this.bankPenerbit = bankPenerbit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getTanggalPengajuan() {
        return tanggalPengajuan;
    }

    public void setTanggalPengajuan(Date tanggalPengajuan) {
        this.tanggalPengajuan = tanggalPengajuan;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
