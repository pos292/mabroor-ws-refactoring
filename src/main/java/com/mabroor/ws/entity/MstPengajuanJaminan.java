package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.util.AES256Utils;


@Entity
@Table(name = "mst_pengajuan_jaminan")
public class MstPengajuanJaminan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 100)
	private String integrationLeadsId;

	@Column(length = 100)
	private String integrationRefRegNo;

	@Column(length = 200)
	private String name;

	@Column(length = 200)
	private String email;

	@Column(length = 200)
	private String phone;

	@Column(length = 50)
	private String distributionType;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)

	private Date transactionDateTime;

	private Date masaBerlakuStnk;

	private Long userId;

	@Column(length = 200)
	private String nameDataUser;

	@Column(length = 200)
	private String emailDataUser;

	@Column(length = 200)
	private String phoneDataUser;

	@Column(length = 50)
	private String jenisJaminan;

	@Column(length = 50)
	private String jumlahPinjaman;

	@Column(length = 50)
	private String brand;
	
	@Column(length = 50)
	private String brandCode;

	@Column(length = 50)
	private String type;
	
	@Column(length = 50)
	private String typeCode;

	@Column(length = 50)
	private String model;
	
	@Column(length = 50)
	private String modelCode;
	
	@Column(length = 50)
	private String officeName;
	
	@Column(length = 50)
	private String officeCode;

	@Column(length = 50)
	private String tahun;

	@Column(length = 50)
	private String otr;

	@Column(length = 50)
	private String pendapatanPerBulan;

	@Column(length = 50)
	private String kebutuhanPinjaman;
	
	@Column(length = 50)
	private String kebutuhanPinjamanCode;

	@Column(length = 50)
	private String tenor;

	@Column(length = 100)
	private String kotaPengajuan;
	
	@Column(length = 100)
	private String kotaPengajuanId;

	@Column(length = 200)
	private String noBpkbMobil;

	private String fotoBpkbMobil;

	@Column(length = 200)
	private String noBpkbMotor;

	private String fotoBpkbMotor;

	@Column(length = 200)
	private String noStnk;

	private String fotoStnk;

	private String fotoKananDepan;

	private String fotoKananBelakang;

	private String fotoKiriBelakang;

	private String fotoKiriDepan;

	@Column(length = 50)
	private String jaminanMotor;

	@Column(length = 50)
	private String jaminanMobil;

	@Column(length = 200)
	private String namaPemilikBpkb;

	@Column(length = 200)
	private String namaLengkapKtp;

	@Column(length = 200)
	private String tanggalLahirKtp;

	@Column(length = 200)
	private String noKtp;

	@Column(length = 250)
	private String alamat;

	@Column(length = 50)
	private String provinsi;
	
	@Column(length = 50)
	private String provinsiId;

	@Column(length = 50)
	private String kabupaten;
	
	@Column(length = 50)
	private String kabupatenId;

	@Column(length = 50)
	private String kecamatan;
	
	@Column(length = 50)
	private String kecamatanId;

	@Column(length = 50)
	private String kelurahan;
	
	@Column(length = 50)
	private String kelurahanId;

	@Column(length = 25)
	private String kodepos;
	
	@Column(length = 50)
	private String kodePosId;

	@Column(length = 3)
	private String newUserSubmit;

	@Column(length = 50)
	private String kodePromo;

	@Column(length = 200)
	private String namaPromo;
	
	@Column(length = 50)
	private String amountDp;
	
	@Column(length = 50)
	private String amountDpPercent;
	
	@Column(length = 50)
	private String gender;
	
	@Column(length = 50)
	private String source = "MABROOR";
	
	@JsonIgnore
	@Column(length = 50)
	private String encryptionStatus = "ENCRYPTED";

	@JsonIgnore
	@Column(length = 2, name = "status")
	private String jaminanStatus;

	@CreationTimestamp
	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@JsonIgnore
	@Column(length = 50)
	private String updatedBy;

	public String getKodePromo() {
		return kodePromo;
	}

	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}

	public String getNamaPromo() {
		return namaPromo;
	}

	public void setNamaPromo(String namaPromo) {
		this.namaPromo = namaPromo;
	}

	public Date getMasaBerlakuStnk() {
		return masaBerlakuStnk;
	}

	public void setMasaBerlakuStnk(Date masaBerlakuStnk) {
		this.masaBerlakuStnk = masaBerlakuStnk;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDistributionType() {
		return distributionType;
	}

	public void setDistributionType(String distributionType) {
		this.distributionType = distributionType;
	}

	public String getName() {
		if(name != null && !"".equals(name) && encryptionStatus==null) {
			return name;
		}else if(name != null && !"".equals(name)) {
			String nama = AES256Utils.decrypt(name);
			return nama;
		}else {
			return name;
		}
	}

	public void setName(String name) {
		if(name != null && !"".equals(name)) {
			String nama = AES256Utils.encrypt(name);
			this.name = nama;
		}else {
			this.name = name;
		}
	}

	public String getEmail() {
		if(email != null && !"".equals(email) && encryptionStatus==null) {
			return email;
		}else if(email != null && !"".equals(email)) {
			String emailLeads = AES256Utils.decrypt(email);
			return emailLeads;
		}else {
			return email;
		}
	}

	public void setEmail(String email) {
		if(email != null && !"".equals(email)) {
			String emailLeads = AES256Utils.encrypt(email);
			this.email = emailLeads;
		}else {
			this.email = email;
		}
	}

	public String getPhone() {
		if(phone != null && !"".equals(phone) && encryptionStatus==null) {
			return phone;
		}else if(phone != null && !"".equals(phone)) {
			String tlp = AES256Utils.decrypt(phone);
			return tlp;
		}else {
			return phone;
		}
	}

	public void setPhone(String phone) {
		if(phone != null && !"".equals(phone)) {
			String tlp = AES256Utils.encrypt(phone);
			this.phone = tlp;
		}else {
			this.phone = phone;
		}
	}

	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getKotaPengajuan() {
		return kotaPengajuan;
	}

	public void setKotaPengajuan(String kotaPengajuan) {
		this.kotaPengajuan = kotaPengajuan;
	}

	public String getNameDataUser() {
		if(nameDataUser != null && !"".equals(nameDataUser) && encryptionStatus==null) {
			return nameDataUser;
		}else if(nameDataUser != null && !"".equals(nameDataUser)) {
			String nameUser = AES256Utils.decrypt(nameDataUser);
			return nameUser;
		}else {
			return nameDataUser;
		}
	}

	public void setNameDataUser(String nameDataUser) {
		if(nameDataUser != null && !"".equals(nameDataUser)) {
			String nameUser = AES256Utils.encrypt(nameDataUser);
			this.nameDataUser = nameUser;
		}else {
			this.nameDataUser = nameDataUser;
		}
	}

	public String getEmailDataUser() {
		if(emailDataUser != null && !"".equals(emailDataUser) && encryptionStatus==null) {
			return emailDataUser;
		}else if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.decrypt(emailDataUser);
			return emailUser;
		}else {
			return emailDataUser;
		}
	}

	public void setEmailDataUser(String emailDataUser) {
		if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.encrypt(emailDataUser);
			this.emailDataUser = emailUser;
		}else {
			this.emailDataUser = emailDataUser;
		}
	}

	public String getPhoneDataUser() {
		if(phoneDataUser != null && !"".equals(phoneDataUser) && encryptionStatus==null) {
			return phoneDataUser;
		}else if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.decrypt(phoneDataUser);
			return phoneUser;
		}else {
			return phoneDataUser;
		}
	}

	public void setPhoneDataUser(String phoneDataUser) {
		if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.encrypt(phoneDataUser);
			this.phoneDataUser = phoneUser;
		}else {
			this.phoneDataUser = phoneDataUser;
		}
	}

	public String getJenisJaminan() {
		return jenisJaminan;
	}

	public void setJenisJaminan(String jenisJaminan) {
		this.jenisJaminan = jenisJaminan;
	}

	public String getJumlahPinjaman() {
		return jumlahPinjaman;
	}

	public void setJumlahPinjaman(String jumlahPinjaman) {
		this.jumlahPinjaman = jumlahPinjaman;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getTahun() {
		return tahun;
	}

	public void setTahun(String tahun) {
		this.tahun = tahun;
	}

	public String getPendapatanPerBulan() {
		return pendapatanPerBulan;
	}

	public void setPendapatanPerBulan(String pendapatanPerBulan) {
		this.pendapatanPerBulan = pendapatanPerBulan;
	}

	public String getKebutuhanPinjaman() {
		return kebutuhanPinjaman;
	}

	public void setKebutuhanPinjaman(String kebutuhanPinjaman) {
		this.kebutuhanPinjaman = kebutuhanPinjaman;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getNoBpkbMobil() {
		if(noBpkbMobil != null && !"".equals(noBpkbMobil) && encryptionStatus==null) {
			return noBpkbMobil;
		}else if(noBpkbMobil != null && !"".equals(noBpkbMobil)) {
			String nomorBpkbMobil = AES256Utils.decrypt(noBpkbMobil);
			return nomorBpkbMobil;
		}else {
			return noBpkbMobil;
		}
	}

	public void setNoBpkbMobil(String noBpkbMobil) {
		if(noBpkbMobil != null && !"".equals(noBpkbMobil)) {
			String nomorBpkbMobil = AES256Utils.encrypt(noBpkbMobil);
			this.noBpkbMobil = nomorBpkbMobil;
		}else {
			this.noBpkbMobil = noBpkbMobil;
		}
	}

	public String getFotoBpkbMobil() {
		if(fotoBpkbMobil != null && !"".equals(fotoBpkbMobil) && encryptionStatus==null) {
			return fotoBpkbMobil;
		}else if(fotoBpkbMobil != null && !"".equals(fotoBpkbMobil)) {
			String photoBpkbMobil = AES256Utils.decrypt(fotoBpkbMobil);
			return photoBpkbMobil;
		}else {
			return fotoBpkbMobil;
		}
	}

	public void setFotoBpkbMobil(String fotoBpkbMobil) {
		if(fotoBpkbMobil != null && !"".equals(fotoBpkbMobil)) {
			String photoBpkbMobil = AES256Utils.encrypt(fotoBpkbMobil);
			this.fotoBpkbMobil = photoBpkbMobil;
		}else {
			this.fotoBpkbMobil = fotoBpkbMobil;
		}
	}

	public String getNoBpkbMotor() {
		if(noBpkbMotor != null && !"".equals(noBpkbMotor) && encryptionStatus==null) {
			return noBpkbMotor;
		}else if(noBpkbMotor != null && !"".equals(noBpkbMotor)) {
			String nomorBpkbMotor = AES256Utils.decrypt(noBpkbMotor);
			return nomorBpkbMotor;
		}else {
			return noBpkbMotor;
		}
	}

	public void setNoBpkbMotor(String noBpkbMotor) {
		if(noBpkbMotor != null && !"".equals(noBpkbMotor)) {
			String nomorBpkbMotor = AES256Utils.encrypt(noBpkbMotor);
			this.noBpkbMotor = nomorBpkbMotor;
		}else {
			this.noBpkbMotor = noBpkbMotor;
		}
	}

	public String getFotoBpkbMotor() {
		if(fotoBpkbMotor != null && !"".equals(fotoBpkbMotor) && encryptionStatus==null) {
			return fotoBpkbMotor;
		}else if(fotoBpkbMotor != null && !"".equals(fotoBpkbMotor)) {
			String photoBpkbMotor = AES256Utils.decrypt(fotoBpkbMotor);
			return photoBpkbMotor;
		}else {
			return fotoBpkbMotor;
		}
	}

	public void setFotoBpkbMotor(String fotoBpkbMotor) {
		if(fotoBpkbMotor != null && !"".equals(fotoBpkbMotor)) {
			String photoBpkbMotor = AES256Utils.encrypt(fotoBpkbMotor);
			this.fotoBpkbMotor = photoBpkbMotor;
		}else {
			this.fotoBpkbMotor = fotoBpkbMotor;
		}
	}

	public String getNoStnk() {
		if(noStnk != null && !"".equals(noStnk) && encryptionStatus==null) {
			return name;
		}else if(noStnk != null && !"".equals(noStnk)) {
			String nomorStnk = AES256Utils.decrypt(noStnk);
			return nomorStnk;
		}else {
			return noStnk;
		}
	}

	public void setNoStnk(String noStnk) {
		if(noStnk != null && !"".equals(noStnk)) {
			String nomorStnk = AES256Utils.encrypt(noStnk);
			this.noStnk = nomorStnk;
		}else {
			this.noStnk = noStnk;
		}
	}

	public String getFotoStnk() {
		if(fotoStnk != null && !"".equals(fotoStnk) && encryptionStatus==null) {
			return fotoStnk;
		}else if(fotoStnk != null && !"".equals(fotoStnk)) {
			String photoStnk = AES256Utils.decrypt(fotoStnk);
			return photoStnk;
		}else {
			return fotoStnk;
		}
	}

	public void setFotoStnk(String fotoStnk) {
		if(fotoStnk != null && !"".equals(fotoStnk)) {
			String photoStnk = AES256Utils.encrypt(fotoStnk);
			this.fotoStnk = photoStnk;
		}else {
			this.fotoStnk = fotoStnk;
		}
	}

	public String getFotoKananDepan() {
		if(fotoKananDepan != null && !"".equals(fotoKananDepan) && encryptionStatus==null) {
			return fotoKananDepan;
		}else if(fotoKananDepan != null && !"".equals(fotoKananDepan)) {
			String photoKananDepan = AES256Utils.decrypt(fotoKananDepan);
			return photoKananDepan;
		}else {
			return fotoKananDepan;
		}
	}

	public void setFotoKananDepan(String fotoKananDepan) {
		if(fotoKananDepan != null && !"".equals(fotoKananDepan)) {
			String photoKananDepan = AES256Utils.encrypt(fotoKananDepan);
			this.fotoKananDepan = photoKananDepan;
		}else {
			this.fotoKananDepan = fotoKananDepan;
		}
	}

	public String getFotoKananBelakang() {
		if(fotoKananBelakang != null && !"".equals(fotoKananBelakang) && encryptionStatus==null) {
			return fotoKananBelakang;
		}else if(fotoKananBelakang != null && !"".equals(fotoKananBelakang)) {
			String photoKananBelakang = AES256Utils.decrypt(fotoKananBelakang);
			return photoKananBelakang;
		}else {
			return fotoKananBelakang;
		}
	}

	public void setFotoKananBelakang(String fotoKananBelakang) {
		if(fotoKananBelakang != null && !"".equals(fotoKananBelakang)) {
			String photoKananBelakang = AES256Utils.encrypt(fotoKananBelakang);
			this.fotoKananBelakang = photoKananBelakang;
		}else {
			this.fotoKananBelakang = fotoKananBelakang;
		}
	}

	public String getFotoKiriBelakang() {
		if(fotoKiriBelakang != null && !"".equals(fotoKiriBelakang) && encryptionStatus==null) {
			return fotoKiriBelakang;
		}else if(fotoKiriBelakang != null && !"".equals(fotoKiriBelakang)) {
			String photoKiriBelakang = AES256Utils.decrypt(fotoKiriBelakang);
			return photoKiriBelakang;
		}else {
			return fotoKiriBelakang;
		}
	}

	public void setFotoKiriBelakang(String fotoKiriBelakang) {
		if(fotoKiriBelakang != null && !"".equals(fotoKiriBelakang)) {
			String photoKiriBelakang = AES256Utils.encrypt(fotoKiriBelakang);
			this.fotoKiriBelakang = photoKiriBelakang;
		}else {
			this.fotoKiriBelakang = fotoKiriBelakang;
		}
	}

	public String getFotoKiriDepan() {
		if(fotoKiriDepan != null && !"".equals(fotoKiriDepan) && encryptionStatus==null) {
			return name;
		}else if(fotoKiriDepan != null && !"".equals(fotoKiriDepan)) {
			String photoKiriDepan = AES256Utils.decrypt(fotoKiriDepan);
			return photoKiriDepan;
		}else {
			return fotoKiriDepan;
		}
	}

	public void setFotoKiriDepan(String fotoKiriDepan) {
		if(fotoKiriDepan != null && !"".equals(fotoKiriDepan)) {
			String photoKiriDepan = AES256Utils.encrypt(fotoKiriDepan);
			this.fotoKiriDepan = photoKiriDepan;
		}else {
			this.fotoKiriDepan = fotoKiriDepan;
		}
	}

	public String getJaminanStatus() {
		return jaminanStatus;
	}

	public void setJaminanStatus(String jaminanStatus) {
		this.jaminanStatus = jaminanStatus;
	}

	public String getJaminanMotor() {
		return jaminanMotor;
	}

	public void setJaminanMotor(String jaminanMotor) {
		this.jaminanMotor = jaminanMotor;
	}

	public String getJaminanMobil() {
		return jaminanMobil;
	}

	public void setJaminanMobil(String jaminanMobil) {
		this.jaminanMobil = jaminanMobil;
	}

	public String getNamaPemilikBpkb() {
		return namaPemilikBpkb;
	}

	public void setNamaPemilikBpkb(String namaPemilikBpkb) {
		this.namaPemilikBpkb = namaPemilikBpkb;
	}

	public String getTanggalLahirKtp() {
		if(tanggalLahirKtp != null && !"".equals(tanggalLahirKtp) && encryptionStatus==null) {
			return tanggalLahirKtp;
		}else if(tanggalLahirKtp != null && !"".equals(tanggalLahirKtp)) {
			String tglLahirKtp = AES256Utils.decrypt(tanggalLahirKtp);
			return tglLahirKtp;
		}else {
			return tanggalLahirKtp;
		}
	}

	public void setTanggalLahirKtp(String tanggalLahirKtp) {
		if(tanggalLahirKtp != null && !"".equals(tanggalLahirKtp)) {
			String tglLahirKtp = AES256Utils.encrypt(tanggalLahirKtp);
			this.tanggalLahirKtp = tglLahirKtp;
		}else {
			this.tanggalLahirKtp = tanggalLahirKtp;
		}
	}

	public String getNoKtp() {
		if(noKtp != null && !"".equals(noKtp) && encryptionStatus==null) {
			return noKtp;
		}else if(noKtp != null && !"".equals(noKtp)) {
			String ktp = AES256Utils.decrypt(noKtp);
			return ktp;
		}else {
			return noKtp;
		}
	}

	public void setNoKtp(String noKtp) {
		if(noKtp != null && !"".equals(noKtp)) {
			String ktp = AES256Utils.encrypt(noKtp);
			this.noKtp = ktp;
		}else {
			this.noKtp = noKtp;
		}
	}

	public String getAlamat() {
		if(alamat != null && !"".equals(alamat) && encryptionStatus==null) {
			return alamat;
		}else if(alamat != null && !"".equals(alamat)) {
			String address = AES256Utils.decrypt(alamat);
			return address;
		}else {
			return alamat;
		}
	}

	public void setAlamat(String alamat) {
		if(alamat != null && !"".equals(alamat)) {
			String address = AES256Utils.encrypt(alamat);
			this.alamat = address;
		}else {
			this.alamat = alamat;
		}
	}

	public String getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public String getKabupaten() {
		return kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNamaLengkapKtp() {
		if(namaLengkapKtp != null && !"".equals(namaLengkapKtp) && encryptionStatus==null) {
			return namaLengkapKtp;
		}else if(namaLengkapKtp != null && !"".equals(namaLengkapKtp)){
			String namaKtp = AES256Utils.decrypt(namaLengkapKtp);
			return namaKtp;
		}else {
			return namaLengkapKtp;
		}
	}

	public void setNamaLengkapKtp(String namaLengkapKtp) {
		if(namaLengkapKtp != null && !"".equals(namaLengkapKtp)) {
			String namaKtp = AES256Utils.encrypt(namaLengkapKtp);
			this.namaLengkapKtp = namaKtp;
		}else {
			this.namaLengkapKtp = namaLengkapKtp;
		}
	}

	public String getNewUserSubmit() {
		return newUserSubmit;
	}

	public void setNewUserSubmit(String newUserSubmit) {
		this.newUserSubmit = newUserSubmit;
	}

	public String getIntegrationLeadsId() {
		return integrationLeadsId;
	}

	public void setIntegrationLeadsId(String integrationLeadsId) {
		this.integrationLeadsId = integrationLeadsId;
	}

	public String getIntegrationRefRegNo() {
		return integrationRefRegNo;
	}

	public void setIntegrationRefRegNo(String integrationRefRegNo) {
		this.integrationRefRegNo = integrationRefRegNo;
	}

	public String getOtr() {
		return otr;
	}

	public void setOtr(String otr) {
		this.otr = otr;
	}

	public String getProvinsiId() {
		return provinsiId;
	}

	public void setProvinsiId(String provinsiId) {
		this.provinsiId = provinsiId;
	}

	public String getKabupatenId() {
		return kabupatenId;
	}

	public void setKabupatenId(String kabupatenId) {
		this.kabupatenId = kabupatenId;
	}

	public String getKecamatanId() {
		return kecamatanId;
	}

	public void setKecamatanId(String kecamatanId) {
		this.kecamatanId = kecamatanId;
	}

	public String getKelurahanId() {
		return kelurahanId;
	}

	public void setKelurahanId(String kelurahanId) {
		this.kelurahanId = kelurahanId;
	}

	public String getKodePosId() {
		return kodePosId;
	}

	public void setKodePosId(String kodePosId) {
		this.kodePosId = kodePosId;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getModelCode() {
		return modelCode;
	}

	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getKebutuhanPinjamanCode() {
		return kebutuhanPinjamanCode;
	}

	public void setKebutuhanPinjamanCode(String kebutuhanPinjamanCode) {
		this.kebutuhanPinjamanCode = kebutuhanPinjamanCode;
	}

	public String getAmountDp() {
		return amountDp;
	}

	public void setAmountDp(String amountDp) {
		this.amountDp = amountDp;
	}

	public String getAmountDpPercent() {
		return amountDpPercent;
	}

	public void setAmountDpPercent(String amountDpPercent) {
		this.amountDpPercent = amountDpPercent;
	}

	public String getKotaPengajuanId() {
		return kotaPengajuanId;
	}

	public void setKotaPengajuanId(String kotaPengajuanId) {
		this.kotaPengajuanId = kotaPengajuanId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
