package com.mabroor.ws.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mst_pengajuan_umroh")
public class MstPengajuanUmroh {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "integration_leads_id")
    private String IntegrationLeadsId;

    @Column(name = "produk_pembiayaan")
    private String produkPembiayaan;

    @Column(name = "paket_kode")
    private String paketKode;

    @Column(name = "dp")
    private Long dp;

    @Column(name = "tenor")
    private Integer tenor;

    @Column(name = "status")
    private String status;

    @Column(name = "promo_name")
    private String promoName;

    @Column(name = "promo_code")
    private String promoCode;

    //fk
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private MstUserMobile mstUserMobile;

    @OneToOne(mappedBy = "mstPengajuanUmroh")
    private MstPengajuanUmrohDataDiri mstPengajuanUmrohDataDiri;

    @OneToOne(mappedBy = "mstPengajuanUmroh")
    private MstPengajuanUmrohAlamat mstPengajuanUmrohAlamat;

    @CreationTimestamp
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date")
    private Date updatedDate;

    @Column(name = "updated_by")
    private String updatedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntegrationLeadsId() {
        return IntegrationLeadsId;
    }

    public void setIntegrationLeadsId(String integrationLeadsId) {
        IntegrationLeadsId = integrationLeadsId;
    }

    public String getProdukPembiayaan() {
        return produkPembiayaan;
    }

    public void setProdukPembiayaan(String produkPembiayaan) {
        this.produkPembiayaan = produkPembiayaan;
    }

    public String getPaketKode() {
        return paketKode;
    }

    public void setPaketKode(String paketKode) {
        this.paketKode = paketKode;
    }

    public Long getDp() {
        return dp;
    }

    public void setDp(Long dp) {
        this.dp = dp;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromoName() {
        return promoName;
    }

    public void setPromoName(String promoName) {
        this.promoName = promoName;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public MstUserMobile getMstUserMobile() {
        return mstUserMobile;
    }

    public void setMstUserMobile(MstUserMobile mstUserMobile) {
        this.mstUserMobile = mstUserMobile;
    }

    public MstPengajuanUmrohDataDiri getMstPengajuanUmrohDataDiri() {
        return mstPengajuanUmrohDataDiri;
    }

    public void setMstPengajuanUmrohDataDiri(MstPengajuanUmrohDataDiri mstPengajuanUmrohDataDiri) {
        this.mstPengajuanUmrohDataDiri = mstPengajuanUmrohDataDiri;
    }

    public MstPengajuanUmrohAlamat getMstPengajuanUmrohAlamat() {
        return mstPengajuanUmrohAlamat;
    }

    public void setMstPengajuanUmrohAlamat(MstPengajuanUmrohAlamat mstPengajuanUmrohAlamat) {
        this.mstPengajuanUmrohAlamat = mstPengajuanUmrohAlamat;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
