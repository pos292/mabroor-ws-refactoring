package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_news")
public class MstNews {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 150)
	private String title;
	
	@Column(length = 150)
	private String titleNotif;

	@Column(length = 100)
	private String subTitle;
	
	@Column(length = 100)
	private String subCategory;

	@Column(length = 150)
	private String descriptionNotif;
	
	@ManyToOne
	@JoinColumn(name = "main_product_id")
	private MstMainProduct mainProduct;

	private String image;

	@Column(length = 100000)
	private String description;

	@Column(length = 2, name = "status")
	private String newsStatus;
	
	@Column(length = 2, name = "notifStatus")
	private String newsNotifStatus;

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@JsonIgnore
	@Column(length = 50)
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleNotif() {
		return titleNotif;
	}

	public void setTitleNotif(String titleNotif) {
		this.titleNotif = titleNotif;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getDescriptionNotif() {
		return descriptionNotif;
	}

	public void setDescriptionNotif(String descriptionNotif) {
		this.descriptionNotif = descriptionNotif;
	}

	public MstMainProduct getMainProduct() {
		return mainProduct;
	}

	public void setMainProduct(MstMainProduct mainProduct) {
		this.mainProduct = mainProduct;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNewsStatus() {
		return newsStatus;
	}

	public void setNewsStatus(String newsStatus) {
		this.newsStatus = newsStatus;
	}

	public String getNewsNotifStatus() {
		return newsNotifStatus;
	}

	public void setNewsNotifStatus(String newsNotifStatus) {
		this.newsNotifStatus = newsNotifStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

}
