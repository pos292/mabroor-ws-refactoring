package com.mabroor.ws.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="mst_al_quran")
public class MstAlQuran {
	
	@Id
	private Long id;
	
	@JsonProperty("_id")
	private Long id_sura;
	
	@Column(length = 10000)
	private String ar;
	
	@Column(length = 10000)
	private Long count;
	
	@Column(length = 10000)
	private String enSura; 
	
	@Column(length = 10000)
	private Long firstPage;
	
	@Column(length = 10000)
	private String indSura;
	
	@Column(length = 10000)
	private Long aya;
	
	@Column(length = 10000)
	private Long juz;
	
	@Column(length = 10000)
	private String enAya; 
	
	@Column(length = 10000)
	private Long page;
	
	@Column(length = 10000)
	private String idAya;
	
	@Column(length = 10000)
	private String literasi;
	
	@Column(length = 10000)
	private String indopak;
	
	@Column(length = 10000)
	private String utsmani;
	
	@Column(length = 10000)
	private String nameSura;
	
	@Column(length = 10000)
	private String ms;
	
	@Column(length = 10000)
	private Long sura;
	
	@Column(length = 10000)
	private String phonetic;
	
	@Column(length = 10000)
	private String phoneticInd;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAr() {
		return ar;
	}

	public void setAr(String ar) {
		this.ar = ar;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public String getEnSura() {
		return enSura;
	}

	public void setEnSura(String enSura) {
		this.enSura = enSura;
	}

	public Long getFirstPage() {
		return firstPage;
	}

	public void setFirstPage(Long firstPage) {
		this.firstPage = firstPage;
	}

	public String getPhonetic() {
		return phonetic;
	}

	public void setPhonetic(String phonetic) {
		this.phonetic = phonetic;
	}

	public String getPhoneticInd() {
		return phoneticInd;
	}

	public void setPhoneticInd(String phoneticInd) {
		this.phoneticInd = phoneticInd;
	}

	public String getIndSura() {
		return indSura;
	}

	public void setIndSura(String indSura) {
		this.indSura = indSura;
	}

	public Long getAya() {
		return aya;
	}

	public void setAya(Long aya) {
		this.aya = aya;
	}

	public Long getJuz() {
		return juz;
	}

	public void setJuz(Long juz) {
		this.juz = juz;
	}

	public String getEnAya() {
		return enAya;
	}

	public void setEnAya(String enAya) {
		this.enAya = enAya;
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public String getIdAya() {
		return idAya;
	}

	public void setIdAya(String idAya) {
		this.idAya = idAya;
	}

	public String getLiterasi() {
		return literasi;
	}

	public void setLiterasi(String literasi) {
		this.literasi = literasi;
	}

	public String getIndopak() {
		return indopak;
	}

	public void setIndopak(String indopak) {
		this.indopak = indopak;
	}

	public String getUtsmani() {
		return utsmani;
	}

	public void setUtsmani(String utsmani) {
		this.utsmani = utsmani;
	}

	public String getNameSura() {
		return nameSura;
	}

	public void setNameSura(String nameSura) {
		this.nameSura = nameSura;
	}

	public String getMs() {
		return ms;
	}

	public void setMs(String ms) {
		this.ms = ms;
	}

	public Long getSura() {
		return sura;
	}

	public void setSura(Long sura) {
		this.sura = sura;
	}

	public Long getId_sura() {
		return id_sura;
	}

	public void setId_sura(Long id_sura) {
		this.id_sura = id_sura;
	}

}
