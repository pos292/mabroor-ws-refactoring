package com.mabroor.ws.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.util.AES256Utils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mst_pengajuan_umroh_data_diri")
public class MstPengajuanUmrohDataDiri {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //fk
    @OneToOne
    @JoinColumn(name = "pengajuan_id", referencedColumnName = "id", nullable = false)
    private MstPengajuanUmroh mstPengajuanUmroh;

    @Column(name = "nama_depan")
    private String namaDepan;

    @Column(name = "nama_belakang")
    private String namaBelakang;

    @Column(name = "gender")
    private String gender;

    @Column(name = "tanggal_lahir")
    private Date tanggalLahir;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @Column(name = "ibu_kandung")
    private String ibuKandung;

    @Column(name = "email")
    private String email;

    @Column(name = "no_hp")
    private String noHp;

    @Column(name = "no_ktp")
    private String noKtp;

    @CreationTimestamp
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date")
    private Date updatedDate;

    @Column(name = "updated_by")
    private String updatedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MstPengajuanUmroh getMstPengajuanUmroh() {
        return mstPengajuanUmroh;
    }

    public void setMstPengajuanUmroh(MstPengajuanUmroh mstPengajuanUmroh) {
        this.mstPengajuanUmroh = mstPengajuanUmroh;
    }

    public String getNamaDepan() {
        return namaDepan;
    }

    public void setNamaDepan(String namaDepan) {
        this.namaDepan = namaDepan;
    }

    public String getNamaBelakang() {
        return namaBelakang;
    }

    public void setNamaBelakang(String namaBelakang) {
        this.namaBelakang = namaBelakang;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getIbuKandung() {
        if(ibuKandung != null && !"".equals(ibuKandung)) {
            String ibukandung = AES256Utils.decrypt(ibuKandung);
            return ibukandung;
        }else {
            return ibuKandung;
        }
    }

    public void setIbuKandung(String ibuKandung) {
        if(ibuKandung != null && !"".equals(ibuKandung)) {
            String ibukandung = AES256Utils.encrypt(ibuKandung);
            this.ibuKandung = ibukandung;
        }else {
            this.ibuKandung = ibuKandung;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNoHp() {
        if(noHp != null && !"".equals(noHp)) {
            String nohp = AES256Utils.decrypt(noHp);
            return nohp;
        }else {
            return noHp;
        }
    }

    public void setNoHp(String noHp) {
        if(noHp != null && !"".equals(noHp)) {
            String nohp = AES256Utils.encrypt(noHp);
            this.noHp = nohp;
        }else {
            this.noHp = noHp;
        }
    }

    public String getNoKtp() {
        if(noKtp != null && !"".equals(noKtp)) {
            String noktp = AES256Utils.decrypt(noKtp);
            return noktp;
        }else {
            return noKtp;
        }
    }

    public void setNoKtp(String noKtp) {
        if(noKtp != null && !"".equals(noKtp)) {
            String noktp = AES256Utils.encrypt(noKtp);
            this.noKtp = noktp;
        }else {
            this.noKtp = noKtp;
        }
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
