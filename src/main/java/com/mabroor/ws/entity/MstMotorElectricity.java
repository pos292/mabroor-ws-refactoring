package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_motor_electricity")
public class MstMotorElectricity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 200)
	private String battery = "-";
	
	@Column(length = 200)
	private String ignitionSystem = "-";
	
	@Column(length = 200)
	private String sparkPlugs = "-";
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "motor_unit_id")
	private MstMotorUnit motorUnit;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	
	public MstMotorUnit getMotorUnit() {
		return motorUnit;
	}

	public void setMotorUnit(MstMotorUnit motorUnit) {
		this.motorUnit = motorUnit;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(String battery) {
		this.battery = battery;
	}

	public String getIgnitionSystem() {
		return ignitionSystem;
	}

	public void setIgnitionSystem(String ignitionSystem) {
		this.ignitionSystem = ignitionSystem;
	}

	public String getSparkPlugs() {
		return sparkPlugs;
	}

	public void setSparkPlugs(String sparkPlugs) {
		this.sparkPlugs = sparkPlugs;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
