package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mabroor.ws.util.AES256Utils;

@Entity
@Table(name="mst_user_mobile")
@JsonInclude(Include.NON_NULL)
public class MstUserMobile {
	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_user_mobile_generator")
//	@SequenceGenerator(name="mst_user_mobile_generator", sequenceName = "mst_user_mobile_seq", initialValue = 10000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 200)
	private String name;

	@Column(length = 200)
	private String firstName;

	@Column(length = 200)
	private String lastName;

	@Column(length = 200)
	private String email;

	@Column(length = 200)
	private String phone;

	@Column(length = 200)
	private String maskedPhone;

	@Column(length = 200)
	private String ktpNumber;

	@Column(length = 200)
	private String npwpNumber;

	@Column(length = 200)
	private String gender;

	@Column(length = 200)
	private String statusUser;

	@Column(length = 200)
	private String address;

	@Column(length = 200)
	private String addressKtp;

	@ManyToOne
	@JoinColumn(name = "province_id")
	private MstLocations province;

	@ManyToOne
	@JoinColumn(name = "city_id")
	private MstKabupaten city;

	@ManyToOne
	@JoinColumn(name = "district_id")
	private MstKecamatan district;

	@ManyToOne
	@JoinColumn(name = "subDistrict_id")
	private MstKelurahan subDistrict;

	//	@Temporal(TemporalType.DATE)
	//	@Column
	//	private Date birthDate;
	@Column(length = 200)
	private String birthDate;

	private String ktpImage;

	private String npwpImage;

	private String bpkbMobilImage;

	private String bpkbMotorImage;

	private String kkImage;

	private String ppImage;

	@Column(length = 250)
	private String firebaseToken;

	@Column(length = 1000)
	private String token;

	@Column(length = 50)
	private String userMobileUuid;

	@Column(length = 50)
	private String goals;

	@JsonIgnore
	@Column(length = 1000)
	private String pin;

	@Column(length = 2, name = "status")
	private String userMobileStatus;

	@Column(length = 200)
	private String zipcode;

	@Column(length = 200)
	private String zipcodeKtp;

	private Long previousUserId;

	@Column(length = 3)
	private String fingerprintToggle;

	@JsonIgnore
	private Long countPin;
	
	@Column(length = 200)
	private String signature;
	
	@JsonIgnore
	@Column(length = 200)
	private String acceptanceMoxaMabroor;
	
	@Column(length = 100)
	private String fingerprintHashKey;
	
	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	private Date acceptanceMoxaMabroorDate;
	
	@JsonIgnore
	@Column(length = 50)
	private String encryptionStatus = "ENCRYPTED";

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	private Date pinLockDateTime;

	@JsonIgnore
	private String pinLockStatus;

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	public String getBpkbMobilImage() {
		if(bpkbMobilImage != null && !"".equals(bpkbMobilImage) && encryptionStatus==null) {
			return bpkbMobilImage;
		}else if(bpkbMobilImage != null && !"".equals(bpkbMobilImage)) {
			String bpkbMobilImg = AES256Utils.decrypt(bpkbMobilImage);
			return bpkbMobilImg;
		}else {
			return bpkbMobilImage;
		}
	}

	public void setBpkbMobilImage(String bpkbMobilImage) {
		if(bpkbMobilImage != null && !"".equals(bpkbMobilImage)) {
			String bpkbMobilImg = AES256Utils.encrypt(bpkbMobilImage);
			this.bpkbMobilImage = bpkbMobilImg;
		}else {
			this.bpkbMobilImage = bpkbMobilImage;
		}
	}

	public String getBpkbMotorImage() {
		if(bpkbMotorImage != null && !"".equals(bpkbMotorImage) && encryptionStatus==null) {
			return bpkbMotorImage;
		}else if(bpkbMotorImage != null && !"".equals(bpkbMotorImage)) {
			String bpkbMotorImg = AES256Utils.decrypt(bpkbMotorImage);
			return bpkbMotorImg;
		}else {
			return bpkbMotorImage;
		}
	}

	public void setBpkbMotorImage(String bpkbMotorImage) {
		if(bpkbMotorImage != null && !"".equals(bpkbMotorImage)) {
			String bpkbMotorImg = AES256Utils.encrypt(bpkbMotorImage);
			this.bpkbMotorImage = bpkbMotorImg;
		}else {
			this.bpkbMotorImage = bpkbMotorImage;
		}
	}

	public String getGoals() {
		return goals;
	}

	public void setGoals(String goals) {
		this.goals = goals;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getFirebaseToken() {
		return firebaseToken;
	}

	public void setFirebaseToken(String firebaseToken) {
		this.firebaseToken = firebaseToken;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public String getBirthDate() {
		if(birthDate != null && !"".equals(birthDate) && encryptionStatus==null) {
			return birthDate;
		}else if(birthDate != null && !"".equals(birthDate)) {
			String birth = AES256Utils.decrypt(birthDate);
			return birth;
		}else {
			return birthDate;
		}
	}

	public void setBirthDate(String birthDate) {
		if(birthDate != null && !"".equals(birthDate)) {
			String birth = AES256Utils.encrypt(birthDate);
			this.birthDate = birth;
		}else {
			this.birthDate = birthDate;
		}
	}

	public String getNpwpNumber() {
		if(npwpNumber != null && !"".equals(npwpNumber) && encryptionStatus==null) {
			return npwpNumber;
		}else if(npwpNumber != null && !"".equals(npwpNumber)) {
			String npwpNum = AES256Utils.decrypt(npwpNumber);
			return npwpNum;
		}else {
			return npwpNumber;
		}
	}

	public void setNpwpNumber(String npwpNumber) {
		if(npwpNumber != null && !"".equals(npwpNumber)) {
			String npwpNum = AES256Utils.encrypt(npwpNumber);
			this.npwpNumber = npwpNum;
		}else {
			this.npwpNumber = npwpNumber;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		if(name != null && !"".equals(name) && encryptionStatus==null) {
			return name;
		}else 
			if(name != null && !"".equals(name)) {
			String nama = AES256Utils.decrypt(name);
			return nama;
		}else {
			return name;
		}
	}

	public void setName(String name) {
		if(name != null && !"".equals(name)) {
			String nama = AES256Utils.encrypt(name);
			this.name = nama;
		}else {
			this.name = name;
		}
	}

	public String getEmail() {
		if(email != null && !"".equals(email) && encryptionStatus==null) {
			return email;
		}else if(email != null && !"".equals(email)) {
			String mail = AES256Utils.decrypt(email);
			return mail;
		}else {
			return email;
		}
	}

	public void setEmail(String email) {
		if(email != null && !"".equals(email)) {
			String mail = AES256Utils.encrypt(email);
			this.email = mail;
		}else {
			this.email = email;
		}
	}

	public String getPhone() {
		if(phone != null && !"".equals(phone) && encryptionStatus==null) {
			return phone;
		}else if(phone != null && !"".equals(phone)) {
			String hp = AES256Utils.decrypt(phone);
			return hp;
		}else {
			return phone;
		}
	}

	public void setPhone(String phone) {
		if(phone != null && !"".equals(phone)) {
			String hp = AES256Utils.encrypt(phone);
			this.phone = hp;
		}else {
			this.phone = phone;
		}
	}

	public String getKtpNumber() {
		if(ktpNumber != null && !"".equals(ktpNumber) && encryptionStatus==null) {
			return ktpNumber;
		}else if(ktpNumber != null && !"".equals(ktpNumber)) {
			String ktp = AES256Utils.decrypt(ktpNumber);
			return ktp;
		}else {
			return ktpNumber;
		}
	}

	public void setKtpNumber(String ktpNumber) {
		if(ktpNumber != null && !"".equals(ktpNumber)) {
			String ktp = AES256Utils.encrypt(ktpNumber);
			this.ktpNumber = ktp;
		}else {
			this.ktpNumber = ktpNumber;
		}
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getStatusUser() {
		return statusUser;
	}

	public void setStatusUser(String statusUser) {
		this.statusUser = statusUser;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getKtpImage() {
		if(ktpImage != null && !"".equals(ktpImage) && encryptionStatus==null) {
			return ktpImage;
		}else if(ktpImage != null && !"".equals(ktpImage)) {
			String ktpImg = AES256Utils.decrypt(ktpImage);
			return ktpImg;
		}else {
			return ktpImage;
		}
	}

	public void setKtpImage(String ktpImage) {
		if(ktpImage != null && !"".equals(ktpImage)) {
			String ktpImg = AES256Utils.encrypt(ktpImage);
			this.ktpImage = ktpImg;
		}else {
			this.ktpImage = ktpImage;
		}
	}

	public String getNpwpImage() {
		if(npwpImage != null && !"".equals(npwpImage) && encryptionStatus==null) {
			return npwpImage;
		}else if(npwpImage != null && !"".equals(npwpImage)) {
			String npwpImg = AES256Utils.decrypt(npwpImage);
			return npwpImg;
		}else {
			return npwpImage;
		}
		
	}

	public void setNpwpImage(String npwpImage) {
		if(npwpImage != null && !"".equals(npwpImage)) {
			String npwpImg = AES256Utils.encrypt(npwpImage);
			this.npwpImage = npwpImg;
		}else {
			this.npwpImage = npwpImage;
		}
	}

	public String getKkImage() {
		if(kkImage != null && !"".equals(kkImage) && encryptionStatus==null) {
			return kkImage;
		}else if(kkImage != null && !"".equals(kkImage)) {
			String kkImg = AES256Utils.decrypt(kkImage);
			return kkImg;
		}else {
			return kkImage;
		}
	}

	public void setKkImage(String kkImage) {
		if(kkImage != null && !"".equals(kkImage)) {
			String kkImg = AES256Utils.encrypt(kkImage);
			this.kkImage = kkImg;
		}else{
			this.kkImage = kkImage;
		}
	}

	public String getPpImage() {
		if(ppImage != null && !"".equals(ppImage) && encryptionStatus==null) {
			return ppImage;
		}else if(ppImage != null && !"".equals(ppImage)) {
			String ppImg = AES256Utils.decrypt(ppImage);
			return ppImg;
		}else {
			return ppImage;
		}
	}

	public void setPpImage(String ppImage) {
		if(ppImage != null && !"".equals(ppImage)) {
			String ppImg = AES256Utils.encrypt(ppImage);
			this.ppImage = ppImg;
		}else {
			this.ppImage = ppImage;
		}
	}

	public String getUserMobileStatus() {
		return userMobileStatus;
	}

	public void setUserMobileStatus(String userMobileStatus) {
		this.userMobileStatus = userMobileStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getMaskedPhone() {
			return maskedPhone;
	}

	public void setMaskedPhone(String maskedPhone) {
			this.maskedPhone = maskedPhone;
	}

	public String getUserMobileUuid() {
		return userMobileUuid;
	}

	public void setUserMobileUuid(String userMobileUuid) {
		this.userMobileUuid = userMobileUuid;
	}

	public Long getPreviousUserId() {
		return previousUserId;
	}

	public void setPreviousUserId(Long previousUserId) {
		this.previousUserId = previousUserId;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getFirstName() {
		if(firstName != null && !"".equals(firstName) && encryptionStatus==null) {
			return firstName;
		}else if(firstName != null && !"".equals(firstName)) {
			String firstNama = AES256Utils.decrypt(firstName);
			return firstNama;
		}else {
			return firstName;
		}
	}

	public void setFirstName(String firstName) {
		if(firstName != null && !"".equals(firstName)) {
			String firstNama = AES256Utils.encrypt(firstName);
			this.firstName = firstNama;
		}else {
			this.firstName = firstName;
		}
	}

	public String getLastName() {
		if(lastName != null && !"".equals(lastName) && encryptionStatus==null) {
			return lastName;
		}else if(lastName != null && !"".equals(lastName)) {
			String lastNama = AES256Utils.decrypt(lastName);
			return lastNama;
		}else {
			return lastName;
		}
	}

	public void setLastName(String lastName) {
		if(lastName != null && !"".equals(lastName)) {
			String lastNama = AES256Utils.encrypt(lastName);
			this.lastName = lastNama;
		}else {
			this.lastName = lastName;
		}
	}

	public String getAddressKtp() {
		if(addressKtp != null && !"".equals(addressKtp) && encryptionStatus==null) {
			return addressKtp;
		}else if(addressKtp != null && !"".equals(addressKtp)) {
			String alamatKtp = AES256Utils.decrypt(addressKtp);
			return alamatKtp;
		}else {
			return addressKtp;
		}
	}

	public void setAddressKtp(String addressKtp) {
		if(addressKtp != null && !"".equals(addressKtp)) {
			String alamatKtp = AES256Utils.encrypt(addressKtp);
			this.addressKtp = alamatKtp;
		}else {
			this.addressKtp = addressKtp;
		}
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getZipcodeKtp() {
		return zipcodeKtp;
	}

	public void setZipcodeKtp(String zipcodeKtp) {
		this.zipcodeKtp = zipcodeKtp;
	}

	public Long getCountPin() {
		return countPin;
	}

	public Date getPinLockDateTime() {
		return pinLockDateTime;
	}

	public void setCountPin(Long countPin) {
		this.countPin = countPin;
	}

	public void setPinLockDateTime(Date pinLockDateTime) {
		this.pinLockDateTime = pinLockDateTime;
	}

	public String getPinLockStatus() {
		return pinLockStatus;
	}

	public void setPinLockStatus(String pinLockStatus) {
		this.pinLockStatus = pinLockStatus;
	}

	public MstLocations getProvince() {
		return province;
	}

	public void setProvince(MstLocations province) {
		this.province = province;
	}

	public MstKabupaten getCity() {
		return city;
	}

	public void setCity(MstKabupaten city) {
		this.city = city;
	}

	public MstKecamatan getDistrict() {
		return district;
	}

	public void setDistrict(MstKecamatan district) {
		this.district = district;
	}

	public MstKelurahan getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(MstKelurahan subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getFingerprintToggle() {
		return fingerprintToggle;
	}

	public void setFingerprintToggle(String fingerprintToggle) {
		this.fingerprintToggle = fingerprintToggle;
	}

	public String getFingerprintHashKey() {
		return fingerprintHashKey;
	}

	public void setFingerprintHashKey(String fingerprintHashKey) {
		this.fingerprintHashKey = fingerprintHashKey;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getAcceptanceMoxaMabroor() {
		return acceptanceMoxaMabroor;
	}

	public void setAcceptanceMoxaMabroor(String acceptanceMoxaMabroor) {
		this.acceptanceMoxaMabroor = acceptanceMoxaMabroor;
	}

	public Date getAcceptanceMoxaMabroorDate() {
		return acceptanceMoxaMabroorDate;
	}

	public void setAcceptanceMoxaMabroorDate(Date acceptanceMoxaMabroorDate) {
		this.acceptanceMoxaMabroorDate = acceptanceMoxaMabroorDate;
	}
}
