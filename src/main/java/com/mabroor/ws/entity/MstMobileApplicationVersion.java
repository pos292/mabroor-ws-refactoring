package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "mst_mobile_application_version")
public class MstMobileApplicationVersion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 50)
	private String androidVersion;
	
	@Column(length = 50)
	private String iosVersion;
	
	@Column(length = 50)
	private String forceUpdate;
	
	@Column(length = 255)
	private String androidLink;
	
	@Column(length = 255)
	private String iosLink;
	
	@Column(length = 255)
	private String androidMessage;
	
	@Column(length = 255)
	private String iosMessage;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	public String getIosVersion() {
		return iosVersion;
	}

	public void setIosVersion(String iosVersion) {
		this.iosVersion = iosVersion;
	}

	public String getForceUpdate() {
		return forceUpdate;
	}

	public void setForceUpdate(String forceUpdate) {
		this.forceUpdate = forceUpdate;
	}

	public String getAndroidLink() {
		return androidLink;
	}

	public void setAndroidLink(String androidLink) {
		this.androidLink = androidLink;
	}

	public String getIosLink() {
		return iosLink;
	}

	public void setIosLink(String iosLink) {
		this.iosLink = iosLink;
	}

	public String getAndroidMessage() {
		return androidMessage;
	}

	public void setAndroidMessage(String androidMessage) {
		this.androidMessage = androidMessage;
	}

	public String getIosMessage() {
		return iosMessage;
	}

	public void setIosMessage(String iosMessage) {
		this.iosMessage = iosMessage;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}
