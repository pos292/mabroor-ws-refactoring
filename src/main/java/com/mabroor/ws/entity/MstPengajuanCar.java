package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NamedNativeQuery;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.dto.CountAccV2Dto;
import com.mabroor.ws.dto.PengajuanAllCustomDto;
import com.mabroor.ws.util.AES256Utils;

@SqlResultSetMapping(
	name = "pengajuanAllResult", 
	classes = @ConstructorResult(
		targetClass = PengajuanAllCustomDto.class,
		columns = {
			@ColumnResult(name = "leads_id", type = Long.class),
			@ColumnResult(name = "variable1", type = Long.class),
			@ColumnResult(name = "variable2"),
			@ColumnResult(name = "variable3", type = Date.class),
			@ColumnResult(name = "variable4"),
			@ColumnResult(name = "variable5"),
			@ColumnResult(name = "variable6"),
			@ColumnResult(name = "variable7")
		}
	)
)


@SqlResultSetMapping(
	name = "countAccv2Result", 
	classes = @ConstructorResult(
		targetClass = CountAccV2Dto.class,
		columns = {
			@ColumnResult(name = "count_acc", type = Long.class),
			@ColumnResult(name = "row_num", type = Long.class)
		}
	)
)


@NamedNativeQuery(
	name = "pengajuanAllByUserIdPaggingQuery",
	query = "select * from (\n" + 
			"	select\n" + 
			"		mpm.id as leads_id,\n" + 
			"		'1' as variable1,\n" + 
			"		'Pembiayaan Motor' as variable2,\n" + 
			"		mpm.created_date as variable3, \n" + 
			"		mpm.tenor as variable4,\n" + 
			"		mpm.otr_price as variable5,\n" + 
			"		case\n" + 
			"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
			"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
			"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
			"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
			"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
			"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
			"			when ma.status is null then 'Pengajuan Terkirim'\n" + 
			"		end as variable6,\n" +
			"		'NON_PAYMENT' as variable7\n" +
			"	from mst_pengajuan_motor mpm\n" + 
			"	left join mst_application ma on ma.leads_id = mpm.id\n"+
			"	where mpm.user_id = ?1 and mpm.status = '01'\n" + 
			"	union \n" + 
			"	select\n" + 
			"		mpmc.id as leads_id,\n" + 
			"		'4' as variable1,\n" + 
			"		'Pembiayaan Mobil Syariah' as variable2,\n" + 
			"		mpmc.created_date as variable3, \n" + 
			"		CONCAT(mpmc.brand,' ',mpmc.type) as variable4,\n" + 
			"		mpmc.dp as variable5,\n" + 
			"		case\n" + 
			"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
			"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
			"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
			"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
			"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
			"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
			"			when ma.status is null then 'Pengajuan Terkirim'\n" + 
			"		end as variable6,\n" +
			"		'NON_PAYMENT' as variable7\n" +
			"	from mst_pengajuan_car mpmc\n" + 
			"	left join mst_application ma on ma.leads_id = mpmc.id\n"+
			"	where mpmc.user_id = ?1 and mpmc.status = '01'\n" + 
			"	union \n" + 
			"	select\n" + 
			"		mwr.id as leads_id,\n" + 
			"		'2' as variable1,\n" + 
			"		'Pembiayaan Haji' as variable2,\n" + 
			"		mwr.created_date as variable3, \n" + 
			"		'Pembiayaan Haji' as variable4,\n" + 
			"		mwr.date_dispatch as variable5,\n" + 
			"		case\n" + 
			"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
			"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
			"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
			"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
			"			when ma.status = 'Cancel Order' then 'Pengajuan Dibatalkan'\n" +
			"			when ma.status = 'Contract' then 'Nomor kontrak sudah terbentuk'\n" +
			"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
			"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
			"			when ma.status = 'Submitted' then 'Pengajuan Terkirim'\n" + 
			"			when ma.status is null then 'Pengajuan Terkirim'\n" + 
			"		end as variable6,\n" +
			"		'NON_PAYMENT' as variable7\n" +
			"	from mst_wisata_religi mwr\n" + 
			"	left join mst_application ma on ma.leads_id = mwr.id\n"+
			"	where mwr.user_id = ?1  and mwr.status = '01'\n" + 
			"	union \n" + 
			"   select\n" + 
			"		mtai.id as leads_id,\n" + 
			"		'5' as variable1,\n" + 
			"		'Asuransi Mobil' as variable2,\n" + 
			"		mtai.created_date as variable3, \n" + 
			"		'Asuransi Mobil - Garda Oto' as variable4,\n" + 
			"		mtai.harga_pertanggungan as variable5,\n" + 
			"		case\n" + 
			"			when ma.status = 'PAID' then 'Pembayaran Berhasil'\n" + 
			"			when ma.status = 'PENDING' then 'Menunggu Pembayaran'\n" + 
			"			when ma.status = 'FAILED' then 'Pembayaran dibatalkan'\n" + 
			"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
			"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
			"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
			"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
			"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
			"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
			"			when ma.status is null then 'Pengajuan Terkirim'\n" +  
			"		end as variable6,\n" + 
			"		'PAYMENT' as variable7\n" + 
			"	from mst_pengajuan_asuransi_mobil mtai\n" + 
			"	left join mst_application ma on ma.leads_id = mtai.id\n"+
			"	where mtai.user_id = ?1 and mtai.status = '01'\n" + 
			"	union \n" + 
			"	select\n" + 
			"		mpj.id as leads_id,\n" + 
			"		'3' as variable1,\n" + 
			"		'Multiguna' as variable2,\n" + 
			"		mpj.created_date as variable3, \n" + 
			"		'Jaminan' as variable4,\n" + 
			"		mpj.jumlah_pinjaman\\:\\:text as variable5,\n" + 
			"		case\n" + 
			"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
			"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
			"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
			"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
			"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
			"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
			"			when ma.status is not null then ma.status\n" + 
			"			when ma.status is null then 'Pengajuan Terkirim'\n" + 
			"		end as variable6,\n" +
			"		'NON_PAYMENT' as variable7\n" +
			"	from mst_pengajuan_jaminan mpj\n" + 
			"	left join mst_application ma on ma.leads_id = mpj.id\n"+
			"	where mpj.user_id = ?1 and mpj.status = '01'\n" +
			"	union \n" +
			"	select\n" +
			"		mgtj.id as leads_id, \n" +
			"		'6' as variable1," +
			"		'Pembiayaan Multiguna Syariah' as variable2," +
			"		mgtj.created_date as variable3," +
			"		'Pembiayaan Tanpa Jaminan' as variable4," +
			"		mgtj.tenor\\:\\:text as variable5," +
			"		mgtj.status as variable6," +
			"		'NON_PAYMENT' as variable7\n" +
			"	from mst_pengajuan_multiguna_tanpa_jaminan mgtj" +
			"	left join mst_application ma on ma.leads_id = mgtj.id\n" +
			"	where mgtj.user_id = ?1\n" +
			"	union \n" +
				"	select\n" +
				"		pu.id as leads_id, \n" +
				"		'7' as variable1," +
				"		'Pembiayaan Umroh' as variable2," +
				"		pu.created_date as variable3," +
				"		'Pembiayaan Umroh' as variable4," +
				"		pu.tenor\\:\\:text as variable5," +
				"		pu.status as variable6,\n"+
					"		'NON_PAYMENT' as variable7\n" +
				"	from mst_pengajuan_umroh pu" +
				"	left join mst_application ma on ma.leads_id = pu.id\n" +
				"	where pu.user_id = ?1\n" +
				") a order by a.variable3 desc\n" +
				"limit ?2 offset ?3",
	resultSetMapping = "pengajuanAllResult"
)


@NamedNativeQuery(
	name = "getPengajuanMotorMapped",
	query = "	select\n" + 
			"		mpm.id as leads_id,\n" + 
			"		'1' as variable1,\n" + 
			"		'Pembiayaan Motor' as variable2,\n" + 
			"		mpm.created_date as variable3, \n" + 
			"		mpm.tenor as variable4,\n" + 
			"		mpm.otr_price as variable5,\n" + 
			"		case\n" + 
			"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
			"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
			"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
			"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
			"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
			"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
			"			when ma.status is null then 'Pengajuan Terkirim'\n" + 
			"		end as variable6,\n" +
			"		'NON_PAYMENT' as variable7\n" +
			"	from mst_pengajuan_motor mpm\n" + 
			"	left join mst_application ma on ma.leads_id = mpm.id\n"+
			"	where mpm.user_id = ?1 and mpm.id = ?2 and mpm.status = '01'\n",
	resultSetMapping = "pengajuanAllResult"
)

@NamedNativeQuery(
	name = "getPengajuanWisataReligiMapped",
	query = "	select\n" + 
			"		mwr.id as leads_id,\n" + 
			"		'2' as variable1,\n" + 
			"		'Pembiayaan Haji' as variable2,\n" + 
			"		mwr.created_date as variable3, \n" + 
			"		'Pembiayaan Haji' as variable4,\n" + 
			"		mwr.date_dispatch as variable5,\n" + 
			"		case\n" + 
			"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
			"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
			"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
			"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
			"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
			"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
			"			when ma.status is null then 'Pengajuan Terkirim'\n" + 
			"		end as variable6,\n" +
			"		'NON_PAYMENT' as variable7\n" +
			"	from mst_wisata_religi mwr\n" + 
			"	left join mst_application ma on ma.leads_id = mwr.id\n"+
			"	where mwr.user_id = ?1 and mwr.id = ?2 and mwr.status = '01'\n",
	resultSetMapping = "pengajuanAllResult"
)

@NamedNativeQuery(
		name = "getPengajuanDenganJaminanMapped",
		query = "	select\n" + 
				"		mpj.id as leads_id,\n" + 
				"		'3' as variable1,\n" + 
				"		'Multiguna' as variable2,\n" + 
				"		mpj.created_date as variable3, \n" + 
				"		'Jaminan' as variable4,\n" + 
				"		mpj.jumlah_pinjaman\\:\\:text as variable5,\n" + 
				"		case\n" + 
				"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
				"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
				"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
				"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
				"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
				"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
				"			when ma.status is not null then ma.status\n" + 
				"			when ma.status is null then 'Pengajuan Terkirim'\n" + 
				"		end as variable6,\n" +
				"		'NON_PAYMENT' as variable7\n" +
				"	from mst_pengajuan_jaminan mpj\n" + 
				"	left join mst_application ma on ma.leads_id = mpj.id\n"+
				"	where mpj.user_id = ?1 and mpj.id = ?2 and mpj.status = '01'\n",
		resultSetMapping = "pengajuanAllResult"
)

@NamedNativeQuery(
		name = "getPengajuanMobilMapped",
		query = "	select\n" + 
				"		mpmc.id as leads_id,\n" + 
				"		'4' as variable1,\n" + 
				"		'Pembiayaan Mobil Syariah' as variable2,\n" + 
				"		mpmc.created_date as variable3, \n" + 
				"		CONCAT(mpmc.brand,' ',mpmc.type) as variable4,\n" + 
				"		mpmc.dp as variable5,\n" + 
				"		case\n" + 
				"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
				"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
				"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
				"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
				"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
				"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
				"			when ma.status is null then 'Pengajuan Terkirim'\n" + 
				"		end as variable6,\n" +
				"		'NON_PAYMENT' as variable7\n" +
				"	from mst_pengajuan_car mpmc\n" + 
				"	left join mst_application ma on ma.leads_id = mpmc.id\n"+
				"	where mpmc.user_id = ?1 and mpmc.id = ?2 and mpmc.status = '01'\n",
		resultSetMapping = "pengajuanAllResult"
)

@NamedNativeQuery(
		name = "getPengajuanAsuransiMobilMapped",
		query = "   select\n" + 
				"		mtai.id as leads_id,\n" + 
				"		'5' as variable1,\n" + 
				"		'Asuransi Mobil' as variable2,\n" + 
				"		mtai.created_date as variable3, \n" + 
				"		'Asuransi Mobil - Garda Oto' as variable4,\n" + 
				"		mtai.harga_pertanggungan as variable5,\n" + 
				"		case\n" + 
				"			when ma.status = 'PAID' then 'Pembayaran Berhasil'\n" + 
				"			when ma.status = 'PENDING' then 'Menunggu Pembayaran'\n" + 
				"			when ma.status = 'FAILED' then 'Pembayaran dibatalkan'\n" + 
				"			when ma.status = 'Inprogress' then 'Pengajuan Diterima'\n" +
				"			when ma.status = 'In Progress' then 'Pengajuan Diproses'\n" +
				"			when ma.status = 'Unreachable' then 'Tidak Dapat Dihubungi'\n" +
				"			when ma.status = 'Cancelled' then 'Pengajuan Dibatalkan'\n" +
				"			when ma.status = 'Rejected' then 'Pengajuan Ditolak'\n" +
				"			when ma.status = 'Approved' then 'Pengajuan Disetujui'\n" +
				"			when ma.status is null then 'Pengajuan Terkirim'\n" +  
				"		end as variable6,\n" + 
				"		'PAYMENT' as variable7\n" + 
				"	from mst_pengajuan_asuransi_mobil mtai\n" + 
				"	left join mst_application ma on ma.leads_id = mtai.id\n"+
				"	where mtai.user_id = ?1 and mtai.id = ?2 and mtai.status = '01'\n",
		resultSetMapping = "pengajuanAllResult"
)
@NamedNativeQuery(
		name = "getPembiayaanUmrohMapped",
		query = "   select\n" +
				"		mtai.id as leads_id,\n" +
				"		'6' as variable1,\n" +
				"		'Pembiayaan Umroh' as variable2,\n" +
				"		mtai.created_date as variable3, \n" +
				"		'Pembiayaan Umroh' as variable4,\n" +
				"		cast(mtai.tenor as VARCHAR) as variable5,\n" +
				"		mtai.status as variable6,\n"+
				"		'PAYMENT' as variable7\n" +
				"	from mst_pengajuan_umroh mtai\n" +
				"	left join mst_application ma on ma.leads_id = mtai.id\n"+
				"	where mtai.user_id = ?1 and mtai.id = ?2",
		resultSetMapping = "pengajuanAllResult"
)



@Entity
@Table(name = "mst_pengajuan_car")
public class MstPengajuanCar {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 200)
	private String name;
	
	@Column(length = 200)
	private String firstName;

	@Column(length = 200)
	private String lastName;

	@Column(length = 200)
	private String email;

	@Column(length = 200)
	private String phone;
	
	@Column(length = 200)
    private String birthDay;
	
	@Column(length = 50)
	private String gender;

	private Date transactionDateTime;

	private Long userId;

	@Column(length = 100)
	private String integrationLeadsId;
	
	@Column(length = 50)
    private String totalPayment1;

	@Column(length = 200)
	private String nameDataUser;

	@Column(length = 200)
	private String emailDataUser;

	@Column(length = 200)
	private String phoneDataUser;

	@Column(length = 50)
	private String brand;
	
	@Column(length = 50)
	private String brandCode;
	
	@Column(length = 50)
	private String type;
	
	@Column(length = 50)
	private String typeCode;	
	
	@Column(length = 50)
	private String model;
	
	@Column(length = 100)
	private String modelCode;
	
	@Column(length = 100)
	private String officeCode;
	    
	@Column(length = 100)
	private String officeName;
	
	@Column(length = 50)
	private String years;

	@Column(length = 50)
	private String location;
	
	 @Column(length = 50)
	 private String otrPrice;
	
	@Column(length = 50)
	private String kotaPengajuanId;

	@Column(length = 50)
	private String amountDp;
	
	@Column(length = 50)
	private String dp;

	@Column(length = 50)
	private String tenor;
	
	@Column(length = 200)
	private String companyName;
	
	@Column(length = 50)
	private String kodePromo;

	@Column(length = 200)
	private String namaPromo;
	
	@Column(length = 50)
	private String insuranceType;
	
	@Column(length = 50)
	private String insuranceNominal;
	
	@Column(length = 50)
	private String insuranceExpansion;
	
	@Column(length = 50)
	private String insurancePolicy;
	
	@Column(length = 50)
	private String asuransiPerlindunganDiri;
	
	@Column(length = 50)
	private String asuransiKerusuhanHuruHaraBencanaAlam;
	
	@Column(length = 50)
	private String asuransiTJHPihakKetiga;
	
	@Column(length = 50)
	private String totalInsurance;
	
	 @Column(length = 50)
	 private String payment;

	@Column(length = 50)
	private String encryptionStatus = "ENCRYPTED";

	@Column(length = 2, name = "status")
	private String pengajuanStatus;

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public String getName() {
		if(name != null && !"".equals(name) && encryptionStatus==null) {
			return name;
		}else if(name != null && !"".equals(name)) {
			String nama = AES256Utils.decrypt(name);
			return nama;
		}else {
			return name;
		}
	}

	public void setName(String name) {
		if(name != null && !"".equals(name)) {
			String nama = AES256Utils.encrypt(name);
			this.name = nama;
		}else {
			this.name = name;
		}
	}
	
	public String getFirstName() {
		if(firstName != null && !"".equals(firstName) && encryptionStatus==null) {
			return firstName;
		}else if(firstName != null && !"".equals(firstName)) {
			String firstNama = AES256Utils.decrypt(firstName);
			return firstNama;
		}else {
			return firstName;
		}
	}

	public void setFirstName(String firstName) {
		if(firstName != null && !"".equals(firstName)) {
			String firstNama = AES256Utils.encrypt(firstName);
			this.firstName = firstNama;
		}else {
			this.firstName = firstName;
		}
	}

	public String getLastName() {
		if(lastName != null && !"".equals(lastName) && encryptionStatus==null) {
			return lastName;
		}else if(lastName != null && !"".equals(lastName)) {
			String lastNama = AES256Utils.decrypt(lastName);
			return lastNama;
		}else {
			return lastName;
		}
	}

	public void setLastName(String lastName) {
		if(lastName != null && !"".equals(lastName)) {
			String lastNama = AES256Utils.encrypt(lastName);
			this.lastName = lastNama;
		}else {
			this.lastName = lastName;
		}
	}

	public String getIntegrationLeadsId() {
		return integrationLeadsId;
	}

	public void setIntegrationLeadsId(String integrationLeadsId) {
		this.integrationLeadsId = integrationLeadsId;
	}

	public String getEmail() {
		if(email != null && !"".equals(email) && encryptionStatus==null) {
			return email;
		}else if(email != null && !"".equals(email)) {
			String emailLeads = AES256Utils.decrypt(email);
			return emailLeads;
		}else {
			return email;
		}
	}

	public void setEmail(String email) {
		if(email != null && !"".equals(email)) {
			String emailLeads = AES256Utils.encrypt(email);
			this.email = emailLeads;
		}else {
			this.email = email;
		}
	}

	public String getPhone() {
		if(phone != null && !"".equals(phone) && encryptionStatus==null) {
			return phone;
		}else if(phone != null && !"".equals(phone)) {
			String tlp = AES256Utils.decrypt(phone);
			return tlp;
		}else {
			return phone;
		}
	}

	public void setPhone(String phone) {
		if(phone != null && !"".equals(phone)) {
			String tlp = AES256Utils.encrypt(phone);
			this.phone = tlp;
		}else {
			this.phone = phone;
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAmountDp() {
		return amountDp;
	}

	public void setAmountDp(String amountDp) {
		this.amountDp = amountDp;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getPengajuanStatus() {
		return pengajuanStatus;
	}

	public void setPengajuanStatus(String pengajuanStatus) {
		this.pengajuanStatus = pengajuanStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNameDataUser() {
		if(nameDataUser != null && !"".equals(nameDataUser) && encryptionStatus==null) {
			return nameDataUser;
		}else if(nameDataUser != null && !"".equals(nameDataUser)) {
			String nameUser = AES256Utils.decrypt(nameDataUser);
			return nameUser;
		}else {
			return nameDataUser;
		}
	}

	public void setNameDataUser(String nameDataUser) {
		if(nameDataUser != null && !"".equals(nameDataUser)) {
			String nameUser = AES256Utils.encrypt(nameDataUser);
			this.nameDataUser = nameUser;
		}else {
			this.nameDataUser = nameDataUser;
		}
	}

	public String getEmailDataUser() {
		if(emailDataUser != null && !"".equals(emailDataUser) && encryptionStatus==null) {
			return emailDataUser;
		}else if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.decrypt(emailDataUser);
			return emailUser;
		}else {
			return emailDataUser;
		}
	}

	public void setEmailDataUser(String emailDataUser) {
		if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.encrypt(emailDataUser);
			this.emailDataUser = emailUser;
		}else {
			this.emailDataUser = emailDataUser;
		}
	}

	public String getPhoneDataUser() {
		if(phoneDataUser != null && !"".equals(phoneDataUser) && encryptionStatus==null) {
			return phoneDataUser;
		}else if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.decrypt(phoneDataUser);
			return phoneUser;
		}else {
			return phoneDataUser;
		}
	}

	public void setPhoneDataUser(String phoneDataUser) {
		if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.encrypt(phoneDataUser);
			this.phoneDataUser = phoneUser;
		}else {
			this.phoneDataUser = phoneDataUser;
		}
	}

	public String getBirthDay() {
		if(birthDay != null && !"".equals(birthDay) && encryptionStatus==null) {
			return birthDay;
		}else if(birthDay != null && !"".equals(birthDay)) {
			String birth = AES256Utils.decrypt(birthDay);
			return birth;
		}else {
			return birthDay;
		}
	}

	public void setBirthDay(String birthDay) {
		if(birthDay != null && !"".equals(birthDay)) {
			String birth = AES256Utils.encrypt(birthDay);
			this.birthDay = birth;
		}else {
			this.birthDay = birthDay;
		}
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getYears() {
		return years;
	}

	public void setYears(String years) {
		this.years = years;
	}

	public String getKotaPengajuanId() {
		return kotaPengajuanId;
	}

	public void setKotaPengajuanId(String kotaPengajuanId) {
		this.kotaPengajuanId = kotaPengajuanId;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getDp() {
		return dp;
	}

	public void setDp(String dp) {
		this.dp = dp;
	}

	public String getKodePromo() {
		return kodePromo;
	}

	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}

	public String getNamaPromo() {
		return namaPromo;
	}

	public void setNamaPromo(String namaPromo) {
		this.namaPromo = namaPromo;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getInsuranceNominal() {
		return insuranceNominal;
	}

	public void setInsuranceNominal(String insuranceNominal) {
		this.insuranceNominal = insuranceNominal;
	}

	public String getInsuranceExpansion() {
		return insuranceExpansion;
	}

	public void setInsuranceExpansion(String insuranceExpansion) {
		this.insuranceExpansion = insuranceExpansion;
	}

	public String getInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(String insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}

	public String getAsuransiPerlindunganDiri() {
		return asuransiPerlindunganDiri;
	}

	public void setAsuransiPerlindunganDiri(String asuransiPerlindunganDiri) {
		this.asuransiPerlindunganDiri = asuransiPerlindunganDiri;
	}

	public String getAsuransiKerusuhanHuruHaraBencanaAlam() {
		return asuransiKerusuhanHuruHaraBencanaAlam;
	}

	public void setAsuransiKerusuhanHuruHaraBencanaAlam(String asuransiKerusuhanHuruHaraBencanaAlam) {
		this.asuransiKerusuhanHuruHaraBencanaAlam = asuransiKerusuhanHuruHaraBencanaAlam;
	}

	public String getAsuransiTJHPihakKetiga() {
		return asuransiTJHPihakKetiga;
	}

	public void setAsuransiTJHPihakKetiga(String asuransiTJHPihakKetiga) {
		this.asuransiTJHPihakKetiga = asuransiTJHPihakKetiga;
	}

	public String getTotalInsurance() {
		return totalInsurance;
	}

	public void setTotalInsurance(String totalInsurance) {
		this.totalInsurance = totalInsurance;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getTotalPayment1() {
		return totalPayment1;
	}

	public void setTotalPayment1(String totalPayment1) {
		this.totalPayment1 = totalPayment1;
	}

	public String getModelCode() {
		return modelCode;
	}

	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getOtrPrice() {
		return otrPrice;
	}

	public void setOtrPrice(String otrPrice) {
		this.otrPrice = otrPrice;
	}
}
