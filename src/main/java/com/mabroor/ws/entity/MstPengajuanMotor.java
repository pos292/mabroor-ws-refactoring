package com.mabroor.ws.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.util.AES256Utils;

@Entity
@Table(name = "mst_pengajuan_motor")
public class MstPengajuanMotor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 100)
	private String integrationLeadsId;

	@Column(length = 50)
	private String category;

	@Column(length = 200)
	private String name;
	
	@Column(length = 200)
	private String firstName;
	
	@Column(length = 200)
	private String lastName;

	@Column(length = 200)
	private String email;

	@Column(length = 50)
	private String companyName;

	@Column(length = 200)
	private String packageName;

	@Column(length = 200)
	private String disclaimer;

	@Column(length = 50)
	private String bestDeal;

	@Column(length = 200)
	private String phone;

	private Date transactionDateTime;

	private Long userId;

	@Column(length = 200)
	private String nameDataUser;

	@Column(length = 200)
	private String emailDataUser;

	@Column(length = 200)
	private String phoneDataUser;

	@Column(length = 50)
	private String location;

	@Column(length = 50)
	private String colour;

	@Column(length = 50)
	private String otrPrice;

	@Column(length = 50)
	private String dp;

	@Column(length = 50)
	private String amountDp;

	@Column(length = 50)
	private String tenor;

	@Column(length = 50)
	private String administration;

	@Column(length = 50)
	private String installment;

	@Column(length = 50)
	private String totalInstallment;

	@Column(length = 50)
	private String insuranceType;

	@Column(length = 50)
	private String insuranceMotor;

	@Column(length = 50)
	private String insurancePayment ;

	@Column(length = 50)
	private String addedTerorismInsurance;

	@Column(length = 50)
	private String accidentInsurance;

	@Column(length = 50)
	private String addedDisasterInsurance;

	@Column(length = 50)
	private String insuranceNominal;

	@Column(length = 50)
	private String insuranceExpansion;

	@Column(length = 50)
	private String asuransiSiagaPlus;

	@Column(length = 50)
	private String insurancePolicy;

	@Column(length = 50)
	private String insuranceRate;

	@Column(length = 50)
	private String totalInsuranceTransaction;

	@Column(length = 50)
	private String totalInsurance;

	@Column(length = 50)
	private String totalPayment1;

	@Column(length = 50)
	private String payment;

	@Column(length = 50)
	private String zipCode;

	@Column(length = 50)
	private String interestFlat;

	@Column(length = 50)
	private String interestSold;

	@Column(length = 50)
	private String primaryDebtStart;

	@Column(length = 50)
	private String fidusia;

	@Column(length = 50)
	private String totalDebt;

	@Column(length = 50)
	private String existingCustomer;

	@Column(length = 50)
	private String fifgroupCardnumber;

	@Column(length = 50)
	private String contractNumber;

	@Column(length = 50)
	private String gender;

	@Column(length = 200)
	private String birthDay;

	@Column(length = 200)
	private String phoneOptional;

	@Column(length = 200)
	private String ktpNumber;

	@Column(length = 250)
	private String address;

	@Column(length = 50)
	private String province;

	@Column(length = 50)
	private String city;

	@Column(length = 50)
	private String subDistrict;
	
	@Column(length = 50)
	private String village;

	@Column(length = 50)
	private String homeOwnership;

	@Column(length = 50)
	private String maritalStatus;

	@Column(length = 50)
	private String lastEducation;

	@Column(length = 50)
	private String dependents;

	private String ktpPhoto;

	private String fotoUnit;

	@Column(length = 50)
	private String kodePromo;

	@Column(length = 200)
	private String namaPromo;
	
	@Column(length = 50)
	private String vehicleType;
	
	@JsonIgnore
	@Column(length = 50)
	private String encryptionStatus = "ENCRYPTED";

	@Column(length = 2, name = "status")
	private String pengajuanStatus;
	
	@Column(length = 50)
	private String source = "MABROOR";

	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	public String getKodePromo() {
		return kodePromo;
	}

	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getIntegrationLeadsId() {
		return integrationLeadsId;
	}

	public void setIntegrationLeadsId(String integrationLeadsId) {
		this.integrationLeadsId = integrationLeadsId;
	}

	public String getNamaPromo() {
		return namaPromo;
	}

	public void setNamaPromo(String namaPromo) {
		this.namaPromo = namaPromo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		if(name != null && !"".equals(name) && encryptionStatus==null) {
			return name;
		}else if(name != null && !"".equals(name)) {
			String nama = AES256Utils.decrypt(name);
			return nama;
		}else {
			return name;
		}
	}

	public void setName(String name) {
		if(name != null && !"".equals(name)) {
			String nama = AES256Utils.encrypt(name);
			this.name = nama;
		}else {
			this.name = name;
		}
	}
	
	public String getFirstName() {
		if(firstName != null && !"".equals(firstName) && encryptionStatus==null) {
			return firstName;
		}else if(firstName != null && !"".equals(firstName)) {
			String nama = AES256Utils.decrypt(firstName);
			return nama;
		}else {
			return firstName;
		}
	}

	public void setFirstName(String firstName) {
		if(firstName != null && !"".equals(firstName)) {
			String nama = AES256Utils.encrypt(firstName);
			this.firstName = nama;
		}else {
			this.firstName = firstName;
		}
	}
	
	public String getLastName() {
		if(lastName != null && !"".equals(lastName) && encryptionStatus==null) {
			return lastName;
		}else if(lastName != null && !"".equals(lastName)) {
			String nama = AES256Utils.decrypt(lastName);
			return nama;
		}else {
			return lastName;
		}
	}

	public void setLastName(String lastName) {
		if(lastName != null && !"".equals(lastName)) {
			String nama = AES256Utils.encrypt(lastName);
			this.lastName = nama;
		}else {
			this.lastName = lastName;
		}
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public String getEmail() {
		if(email != null && !"".equals(email) && encryptionStatus==null) {
			return email;
		}else if(email != null && !"".equals(email)) {
			String emails = AES256Utils.decrypt(email);
			return emails;
		}else {
			return email;
		}
	}

	public void setEmail(String email) {
		if(email != null && !"".equals(email)) {
			String emails = AES256Utils.encrypt(email);
			this.email = emails;
		}else {
			this.email = email;
		}
	}

	public String getPhone() {
		if(phone != null && !"".equals(phone) && encryptionStatus==null) {
			return phone;
		}else if(phone != null && !"".equals(phone)) {
			String phones = AES256Utils.decrypt(phone);
			return phones;
		}else {
			return phone;
		}
	}

	public void setPhone(String phone) {
		if(phone != null && !"".equals(phone)) {
			String phones = AES256Utils.encrypt(phone);
			this.phone = phones;
		}else {
			this.phone = phone;
		}
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public String getBestDeal() {
		return bestDeal;
	}

	public void setBestDeal(String bestDeal) {
		this.bestDeal = bestDeal;
	}

	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public String getOtrPrice() {
		return otrPrice;
	}

	public void setOtrPrice(String otrPrice) {
		this.otrPrice = otrPrice;
	}

	public String getDp() {
		return dp;
	}

	public void setDp(String dp) {
		this.dp = dp;
	}

	public String getAmountDp() {
		return amountDp;
	}

	public void setAmountDp(String amountDp) {
		this.amountDp = amountDp;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getInstallment() {
		return installment;
	}

	public void setInstallment(String installment) {
		this.installment = installment;
	}

	public String getTotalInstallment() {
		return totalInstallment;
	}

	public void setTotalInstallment(String totalInstallment) {
		this.totalInstallment = totalInstallment;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getInsuranceMotor() {
		return insuranceMotor;
	}

	public void setInsuranceMotor(String insuranceMotor) {
		this.insuranceMotor = insuranceMotor;
	}

	public String getInsurancePayment() {
		return insurancePayment;
	}

	public void setInsurancePayment(String insurancePayment) {
		this.insurancePayment = insurancePayment;
	}

	public String getAddedTerorismInsurance() {
		return addedTerorismInsurance;
	}

	public void setAddedTerorismInsurance(String addedTerorismInsurance) {
		this.addedTerorismInsurance = addedTerorismInsurance;
	}

	public String getAccidentInsurance() {
		return accidentInsurance;
	}

	public void setAccidentInsurance(String accidentInsurance) {
		this.accidentInsurance = accidentInsurance;
	}

	public String getAddedDisasterInsurance() {
		return addedDisasterInsurance;
	}

	public void setAddedDisasterInsurance(String addedDisasterInsurance) {
		this.addedDisasterInsurance = addedDisasterInsurance;
	}

	public String getInsuranceNominal() {
		return insuranceNominal;
	}

	public void setInsuranceNominal(String insuranceNominal) {
		this.insuranceNominal = insuranceNominal;
	}

	public String getAsuransiSiagaPlus() {
		return asuransiSiagaPlus;
	}

	public void setAsuransiSiagaPlus(String asuransiSiagaPlus) {
		this.asuransiSiagaPlus = asuransiSiagaPlus;
	}

	public String getInsuranceExpansion() {
		return insuranceExpansion;
	}

	public void setInsuranceExpansion(String insuranceExpansion) {
		this.insuranceExpansion = insuranceExpansion;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(String insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}

	public String getInsuranceRate() {
		return insuranceRate;
	}

	public void setInsuranceRate(String insuranceRate) {
		this.insuranceRate = insuranceRate;
	}

	public String getTotalInsuranceTransaction() {
		return totalInsuranceTransaction;
	}

	public void setTotalInsuranceTransaction(String totalInsuranceTransaction) {
		this.totalInsuranceTransaction = totalInsuranceTransaction;
	}

	public String getTotalInsurance() {
		return totalInsurance;
	}

	public void setTotalInsurance(String totalInsurance) {
		this.totalInsurance = totalInsurance;
	}

	public String getTotalPayment1() {
		return totalPayment1;
	}

	public void setTotalPayment1(String totalPayment1) {
		this.totalPayment1 = totalPayment1;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getInterestFlat() {
		return interestFlat;
	}

	public void setInterestFlat(String interestFlat) {
		this.interestFlat = interestFlat;
	}

	public String getInterestSold() {
		return interestSold;
	}

	public void setInterestSold(String interestSold) {
		this.interestSold = interestSold;
	}

	public String getPrimaryDebtStart() {
		return primaryDebtStart;
	}

	public void setPrimaryDebtStart(String primaryDebtStart) {
		this.primaryDebtStart = primaryDebtStart;
	}

	public String getFidusia() {
		return fidusia;
	}

	public void setFidusia(String fidusia) {
		this.fidusia = fidusia;
	}

	public String getTotalDebt() {
		return totalDebt;
	}

	public void setTotalDebt(String totalDebt) {
		this.totalDebt = totalDebt;
	}

	public String getPengajuanStatus() {
		return pengajuanStatus;
	}

	public void setPengajuanStatus(String pengajuanStatus) {
		this.pengajuanStatus = pengajuanStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getAdministration() {
		return administration;
	}

	public void setAdministration(String administration) {
		this.administration = administration;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNameDataUser() {
		if(nameDataUser != null && !"".equals(nameDataUser) && encryptionStatus==null) {
			return nameDataUser;
		}else if(nameDataUser != null && !"".equals(nameDataUser)) {
			String nameUser = AES256Utils.decrypt(nameDataUser);
			return nameUser;
		}else {
			return nameDataUser;
		}
	}

	public void setNameDataUser(String nameDataUser) {
		if(nameDataUser != null && !"".equals(nameDataUser)) {
			String nameUser = AES256Utils.encrypt(nameDataUser);
			this.nameDataUser = nameUser;
		}else {
			this.nameDataUser = nameDataUser;
		}
	}

	public String getEmailDataUser() {
		if(emailDataUser != null && !"".equals(emailDataUser) && encryptionStatus==null) {
			return emailDataUser;
		}else if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.decrypt(emailDataUser);
			return emailUser;
		}else {
			return emailDataUser;
		}
	}

	public void setEmailDataUser(String emailDataUser) {
		if(emailDataUser != null && !"".equals(emailDataUser)) {
			String emailUser = AES256Utils.encrypt(emailDataUser);
			this.emailDataUser = emailUser;
		}else {
			this.emailDataUser = emailDataUser;
		}
	}

	public String getPhoneDataUser() {
		if(phoneDataUser != null && !"".equals(phoneDataUser) && encryptionStatus==null) {
			return phoneDataUser;
		}else if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.decrypt(phoneDataUser);
			return phoneUser;
		}else {
			return phoneDataUser;
		}
	}
	
	public void setPhoneDataUser(String phoneDataUser) {
		if(phoneDataUser != null && !"".equals(phoneDataUser)) {
			String phoneUser = AES256Utils.encrypt(phoneDataUser);
			this.phoneDataUser = phoneUser;
		}else {
			this.phoneDataUser = phoneDataUser;
		}
	}

	public String getFotoUnit() {
		if(fotoUnit != null && !"".equals(fotoUnit) && encryptionStatus==null) {
			return fotoUnit;
		}else if(fotoUnit != null && !"".equals(fotoUnit)) {
			String fotoUnits = AES256Utils.decrypt(fotoUnit);
			return fotoUnits;
		}else {
			return fotoUnit;
		}
	}

	public void setFotoUnit(String fotoUnit) {
		if(fotoUnit != null && !"".equals(fotoUnit)) {
			String fotoUnits = AES256Utils.encrypt(fotoUnit);
			this.fotoUnit = fotoUnits;
		}else {
			this.fotoUnit = fotoUnit;
		}
	}

	public String getExistingCustomer() {
		return existingCustomer;
	}

	public void setExistingCustomer(String existingCustomer) {
		this.existingCustomer = existingCustomer;
	}

	public String getContractNumber() {
		if(contractNumber != null && !"".equals(contractNumber) && encryptionStatus==null) {
			return contractNumber;
		}else if(contractNumber != null && !"".equals(contractNumber)) {
			String contractNumbers = AES256Utils.decrypt(contractNumber);
			return contractNumbers;
		}else {
			return contractNumber;
		}
	}

	public void setContractNumber(String contractNumber) {
		if(contractNumber != null && !"".equals(contractNumber)) {
			String contractNumbers = AES256Utils.encrypt(contractNumber);
			this.contractNumber = contractNumbers;
		}else {
			this.contractNumber = contractNumber;
		}
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDay() {
		if(birthDay != null && !"".equals(birthDay) && encryptionStatus==null) {
			return birthDay;
		}else if(birthDay != null && !"".equals(birthDay)) {
			String birth = AES256Utils.decrypt(birthDay);
			return birth;
		}else {
			return birthDay;
		}
	}

	public void setBirthDay(String birthDay) {
		if(birthDay != null && !"".equals(birthDay)) {
			String birth = AES256Utils.encrypt(birthDay);
			this.birthDay = birth;
		}else {
			this.birthDay = birthDay;
		}
	}

	public String getPhoneOptional() {
		if(phoneOptional != null && !"".equals(phoneOptional) && encryptionStatus==null) {
			return phoneOptional;
		}else if(phoneOptional != null && !"".equals(phoneOptional)) {
			String phoneOptionals = AES256Utils.decrypt(phoneOptional);
			return phoneOptionals;
		}else {
			return phoneOptional;
		}
	}

	public void setPhoneOptional(String phoneOptional) {
		if(phoneOptional != null && !"".equals(phoneOptional)) {
			String phoneOptionals = AES256Utils.encrypt(phoneOptional);
			this.phoneOptional = phoneOptionals;
		}else {
			this.phoneOptional = phoneOptional;
		}
	}

	public String getKtpNumber() {
		if(ktpNumber != null && !"".equals(ktpNumber) && encryptionStatus==null) {
			return ktpNumber;
		}else if(ktpNumber != null && !"".equals(ktpNumber)) {
			String ktp = AES256Utils.decrypt(ktpNumber);
			return ktp;
		}else {
			return ktpNumber;
		}
	}

	public void setKtpNumber(String ktpNumber) {
		if(ktpNumber != null && !"".equals(ktpNumber)) {
			String ktp = AES256Utils.encrypt(ktpNumber);
			this.ktpNumber = ktp;
		}else {
			this.ktpNumber = ktpNumber;
		}
	}

	public String getAddress() {
		if(address != null && !"".equals(address) && encryptionStatus==null) {
			return address;
		}else if(address != null && !"".equals(address)) {
			String addresss = AES256Utils.decrypt(address);
			return addresss;
		}else {
			return address;
		}
	}

	public void setAddress(String address) {
		if(address != null && !"".equals(address)) {
			String addresss = AES256Utils.encrypt(address);
			this.address = addresss;
		}else {
			this.address = address;
		}
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getHomeOwnership() {
		return homeOwnership;
	}

	public void setHomeOwnership(String homeOwnership) {
		this.homeOwnership = homeOwnership;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getLastEducation() {
		return lastEducation;
	}

	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}

	public String getDependents() {
		return dependents;
	}

	public void setDependents(String dependents) {
		this.dependents = dependents;
	}

	public String getKtpPhoto() {
		if(ktpPhoto != null && !"".equals(ktpPhoto) && encryptionStatus==null) {
			return ktpPhoto;
		}else if(ktpPhoto != null && !"".equals(ktpPhoto)) {
			String ktpFoto = AES256Utils.decrypt(ktpPhoto);
			return ktpFoto;
		}else {
			return ktpPhoto;
		}
	}

	public void setKtpPhoto(String ktpPhoto) {
		if(ktpPhoto != null && !"".equals(ktpPhoto)) {
			String ktpFoto = AES256Utils.encrypt(ktpPhoto);
			this.ktpPhoto = ktpFoto;
		}else {
			this.ktpPhoto = ktpPhoto;
		}
	}

	public String getFifgroupCardnumber() {
		return fifgroupCardnumber;
	}

	public void setFifgroupCardnumber(String fifgroupCardnumber) {
		this.fifgroupCardnumber = fifgroupCardnumber;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}