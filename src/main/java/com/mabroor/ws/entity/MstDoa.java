package com.mabroor.ws.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="mst_doa")
public class MstDoa {

	@Id
	private Long id;
	
	@Column(length = 10000)
	private String namaDoa;
	
	@Column(length = 10000)
	private String doa;
	
	@Column(length = 10000)
	private String bunyi;
	
	@Column(length = 10000)
	private String arti;
	
	@Column(length = 10000)
	private String createdAt;
	
	@Column(length = 10000)
	private String updatedAt;
	
	@Column(length = 200)
	private String duplicate;
	
	@ManyToOne
	@JoinColumn(name = "id_category")
	private MstDoaCategory idCategory;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamaDoa() {
		return namaDoa;
	}

	public void setNamaDoa(String namaDoa) {
		this.namaDoa = namaDoa;
	}

	public String getDoa() {
		return doa;
	}

	public void setDoa(String doa) {
		this.doa = doa;
	}

	public String getBunyi() {
		return bunyi;
	}

	public void setBunyi(String bunyi) {
		this.bunyi = bunyi;
	}

	public String getArti() {
		return arti;
	}

	public void setArti(String arti) {
		this.arti = arti;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public MstDoaCategory getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(MstDoaCategory idCategory) {
		this.idCategory = idCategory;
	}

	public String getDuplicate() {
		return duplicate;
	}

	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}
}
