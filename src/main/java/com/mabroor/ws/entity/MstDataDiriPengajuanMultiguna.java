package com.mabroor.ws.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.util.AES256Utils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="mst_datadiri_pengajuan_multiguna")
public class MstDataDiriPengajuanMultiguna {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "id_pengajuan")
    private Long idPengajuan;

    @Column(name = "jenis_kelamin")
    private String jenisKelamin;

    @Column(name = "nama_ktp")
    private String namaKtp;

    @Column(name = "nik")
    private String nik;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @Column(name = "tanggal_lahir")
    private Date tanggalLahir;

    @Column(name = "email")
    private String email;

    @Column(name = "no_tlpn")
    private String noTlpn;

    @Column(name = "pekerjaan")
    private String pekerjaan;

    @Column(name = "npwp")
    private String npwp;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "provinsi")
    private String provinsi;

    @Column(name = "kotakabupaten")
    private String kotaKabupaten;

    @Column(name = "kecamatan")
    private String kecamatan;

    @Column(name = "kelurahan")
    private String kelurahan;

    @Column(name = "kode_post")
    private String kodePost;

    @CreationTimestamp
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date")
    private Date updatedDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdPengajuan() {
        return idPengajuan;
    }

    public void setIdPengajuan(Long idPengajuan) {
        this.idPengajuan = idPengajuan;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }


    public String getNamaKtp() {
        if(namaKtp != null && !"".equals(namaKtp)) {
            String decyrpt = AES256Utils.decrypt(namaKtp);
            return decyrpt;
        }else {
            return namaKtp;
        }
    }

    public void setNamaKtp(String namaKtp) {
        if(namaKtp != null && !"".equals(namaKtp)) {
            String encrypt = AES256Utils.encrypt(namaKtp);
            this.namaKtp = encrypt;
        }else {
            this.namaKtp = namaKtp;
        }
    }

    public String getNik() {
        if(nik != null && !"".equals(nik)) {
            String decyrpt = AES256Utils.decrypt(nik);
            return decyrpt;
        }else {
            return nik;
        }
    }

    public void setNik(String nik) {
        if(nik != null && !"".equals(nik)) {
            String encrypt = AES256Utils.encrypt(nik);
            this.nik = encrypt;
        }else {
            this.nik = nik;
        }
    }

    public String getTempatLahir() {
        if(tempatLahir != null && !"".equals(tempatLahir)) {
            String decyrpt = AES256Utils.decrypt(tempatLahir);
            return decyrpt;
        }else {
            return tempatLahir;
        }
    }

    public void setTempatLahir(String tempatLahir) {
        if(tempatLahir != null && !"".equals(tempatLahir)) {
            String encrypt = AES256Utils.encrypt(tempatLahir);
            this.tempatLahir = encrypt;
        }else {
            this.tempatLahir = tempatLahir;
        }
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getEmail() {
        if(email != null && !"".equals(email)) {
            String decyrpt = AES256Utils.decrypt(email);
            return decyrpt;
        }else {
            return email;
        }
    }

    public void setEmail(String email) {
        if(email != null && !"".equals(email)) {
            String encrypt = AES256Utils.encrypt(email);
            this.email = encrypt;
        }else {
            this.email = email;
        }
    }

    public String getNoTlpn() {
        if(noTlpn != null && !"".equals(noTlpn)) {
            String decyrpt = AES256Utils.decrypt(noTlpn);
            return decyrpt;
        }else {
            return noTlpn;
        }
    }

    public void setNoTlpn(String noTlpn) {
        if(noTlpn != null && !"".equals(noTlpn)) {
            String encrypt = AES256Utils.encrypt(noTlpn);
            this.noTlpn = encrypt;
        }else {
            this.noTlpn = noTlpn;
        }
    }

    public String getPekerjaan() {
        if(pekerjaan != null && !"".equals(pekerjaan)) {
            String decyrpt = AES256Utils.decrypt(pekerjaan);
            return decyrpt;
        }else {
            return pekerjaan;
        }
    }

    public void setPekerjaan(String pekerjaan) {
        if(pekerjaan != null && !"".equals(pekerjaan)) {
            String encrypt = AES256Utils.encrypt(pekerjaan);
            this.pekerjaan = encrypt;
        }else {
            this.pekerjaan = pekerjaan;
        }
    }

    public String getNpwp() {
        if(npwp != null && !"".equals(npwp)) {
            String decyrpt = AES256Utils.decrypt(npwp);
            return decyrpt;
        }else {
            return npwp;
        }
    }

    public void setNpwp(String npwp) {
        if(npwp != null && !"".equals(npwp)) {
            String encrypt = AES256Utils.encrypt(npwp);
            this.npwp = encrypt;
        }else {
            this.npwp = npwp;
        }
    }

    public String getAlamat() {
        if(alamat != null && !"".equals(alamat)) {
            String decyrpt = AES256Utils.decrypt(alamat);
            return decyrpt;
        }else {
            return alamat;
        }
    }

    public void setAlamat(String alamat) {
        if(alamat != null && !"".equals(alamat)) {
            String encrypt = AES256Utils.encrypt(alamat);
            this.alamat = encrypt;
        }else {
            this.alamat = alamat;
        }
    }

    public String getProvinsi() {
        if(provinsi != null && !"".equals(provinsi)) {
            String decyrpt = AES256Utils.decrypt(provinsi);
            return decyrpt;
        }else {
            return provinsi;
        }
    }

    public void setProvinsi(String provinsi) {
        if(provinsi != null && !"".equals(provinsi)) {
            String encrypt = AES256Utils.encrypt(provinsi);
            this.provinsi = encrypt;
        }else {
            this.provinsi = provinsi;
        }
    }

    public String getKotaKabupaten() {
        if(kotaKabupaten != null && !"".equals(kotaKabupaten)) {
            String decyrpt = AES256Utils.decrypt(kotaKabupaten);
            return decyrpt;
        }else {
            return kotaKabupaten;
        }
    }

    public void setKotaKabupaten(String kotaKabupaten) {
        if(kotaKabupaten != null && !"".equals(kotaKabupaten)) {
            String encrypt = AES256Utils.encrypt(kotaKabupaten);
            this.kotaKabupaten = encrypt;
        }else {
            this.kotaKabupaten = kotaKabupaten;
        }
    }

    public String getKecamatan() {
        if(kecamatan != null && !"".equals(kecamatan)) {
            String decyrpt = AES256Utils.decrypt(kecamatan);
            return decyrpt;
        }else {
            return kecamatan;
        }
    }

    public void setKecamatan(String kecamatan) {
        if(kecamatan != null && !"".equals(kecamatan)) {
            String encrypt = AES256Utils.encrypt(kecamatan);
            this.kecamatan = encrypt;
        }else {
            this.kecamatan = kecamatan;
        }
    }

    public String getKelurahan() {
        if(kelurahan != null && !"".equals(kelurahan)) {
            String decyrpt = AES256Utils.decrypt(kelurahan);
            return decyrpt;
        }else {
            return kelurahan;
        }
    }

    public void setKelurahan(String kelurahan) {
        if(kelurahan != null && !"".equals(kelurahan)) {
            String encrypt = AES256Utils.encrypt(kelurahan);
            this.kelurahan = encrypt;
        }else {
            this.kelurahan = kelurahan;
        }
    }

    public String getKodePost() {
        if(kodePost != null && !"".equals(kodePost)) {
            String decyrpt = AES256Utils.decrypt(kodePost);
            return decyrpt;
        }else {
            return kodePost;
        }
    }

    public void setKodePost(String kodePost) {
        if(kodePost != null && !"".equals(kodePost)) {
            String encrypt = AES256Utils.encrypt(kodePost);
            this.kodePost = encrypt;
        }else {
            this.kodePost = kodePost;
        }
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
