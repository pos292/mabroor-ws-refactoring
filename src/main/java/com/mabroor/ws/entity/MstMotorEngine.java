package com.mabroor.ws.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mst_motor_engine")
public class MstMotorEngine {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 200)
	private String engineType = "-";
	
	@Column(precision = 15, scale = 0)
	private BigDecimal engineCapacity = BigDecimal.ZERO;
	
	@Column(length = 200)
	private String fuel = "-";
	
	@Column(precision = 15, scale = 0)
	private BigDecimal diameter = BigDecimal.ZERO;
	
	@Column(length = 200)
	private String transmissionType = "-";
	
	@Column(length = 200)
	private String compressionRatio = "-";
	
	@Column(length = 200)
	private String powerMax = "-";
	
	@Column(length = 200)
	private String torsiMax = "-";
	
	@Column(length = 200)
	private String starterType = "-";
	
	@Column(length = 200)
	private String koplingType = "-";
	
	@Column(length = 200)
	private String engineCooler = "-";
	
	@Column(length = 200)
	private String gearshift = "-";
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "motor_unit_id")
	private MstMotorUnit motorUnit;
	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;

	@JsonIgnore
	@Column(length = 50, updatable = false)
	private String createdBy;

	
	public MstMotorUnit getMotorUnit() {
		return motorUnit;
	}

	public void setMotorUnit(MstMotorUnit motorUnit) {
		this.motorUnit = motorUnit;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public BigDecimal getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(BigDecimal engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public String getFuel() {
		return fuel;
	}

	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	public BigDecimal getDiameter() {
		return diameter;
	}

	public void setDiameter(BigDecimal diameter) {
		this.diameter = diameter;
	}

	public String getTransmissionType() {
		return transmissionType;
	}

	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}

	public String getCompressionRatio() {
		return compressionRatio;
	}

	public void setCompressionRatio(String compressionRatio) {
		this.compressionRatio = compressionRatio;
	}

	public String getPowerMax() {
		return powerMax;
	}

	public void setPowerMax(String powerMax) {
		this.powerMax = powerMax;
	}

	public String getTorsiMax() {
		return torsiMax;
	}

	public void setTorsiMax(String torsiMax) {
		this.torsiMax = torsiMax;
	}

	public String getStarterType() {
		return starterType;
	}

	public void setStarterType(String starterType) {
		this.starterType = starterType;
	}

	public String getKoplingType() {
		return koplingType;
	}

	public void setKoplingType(String koplingType) {
		this.koplingType = koplingType;
	}

	public String getEngineCooler() {
		return engineCooler;
	}

	public void setEngineCooler(String engineCooler) {
		this.engineCooler = engineCooler;
	}

	public String getGearshift() {
		return gearshift;
	}

	public void setGearshift(String gearshift) {
		this.gearshift = gearshift;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


}
