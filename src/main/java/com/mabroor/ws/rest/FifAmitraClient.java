package com.mabroor.ws.rest;

import com.mabroor.ws.dto.ReqAmitraSubmission;
import com.mabroor.ws.dto.ResFifAmitraDto;
import com.mabroor.ws.dto.ResFifAmitraListDataDto;
import com.mabroor.ws.dto.ResFifAmitraListDto;
import feign.FeignException;
import feign.Headers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;

@FeignClient(name = "fifAmitra", url = "${axway.api.url}")
public interface FifAmitraClient {

    @PostMapping("/backend/amitra/ext/submit")
    @Headers("user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36")
    ResFifAmitraDto callAmitraClient(@RequestHeader("user-agent")String agent,@RequestHeader("api-key")String apiKey, @RequestBody ReqAmitraSubmission request) throws FeignException;

    @GetMapping(value = "/fif/backend/state/listAll", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    ResFifAmitraListDto getListState(@RequestHeader("user-agent")String agent,@RequestHeader("api-key")String apiKey) throws FeignException;

    @GetMapping(value = "/fif/backend/city/listbystate/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    ResFifAmitraListDto getListCity(@RequestHeader("user-agent")String agent,@RequestHeader("api-key")String apiKey, @PathVariable("id") int id) throws FeignException;

    @GetMapping(value = "/fif/backend/district/listbycity/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    ResFifAmitraListDto getListDistrict(@RequestHeader("user-agent")String agent,@RequestHeader("api-key")String apiKey, @PathVariable("id") int id) throws FeignException;

    @GetMapping(value = "/fif/backend/subdistrict/listbydistrict/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    ResFifAmitraListDto getListSubDistrict(@RequestHeader("user-agent")String agent, @RequestHeader("api-key")String apiKey, @PathVariable("id") int id) throws FeignException;

    @GetMapping(value = "/backend/amitra/status-leads/{intLeadsId}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    ResFifAmitraListDataDto getDigitalStatusLeads(@RequestHeader("user-agent")String agent, @RequestHeader("api-key")String apiKey, @PathVariable("intLeadsId") String intLeadsId) throws FeignException;

}
