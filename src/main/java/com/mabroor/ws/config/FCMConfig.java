package com.mabroor.ws.config;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@Configuration
public class FCMConfig {
	Logger logger = LoggerFactory.getLogger(FCMConfig.class);

	@PostConstruct
	public void initialize() {
		try {
			FirebaseOptions options = new FirebaseOptions.Builder().setCredentials(GoogleCredentials.fromStream(new ClassPathResource("mabroor-dev-firebase-adminsdk-qcss4-02d0320495.json").getInputStream())).build();
            if (FirebaseApp.getApps().isEmpty()) {
            	FirebaseApp.initializeApp(options);
                logger.info("Firebase application has been initialized");
            }
        } catch (IOException e) {
        	logger.error(e.getMessage());
        }
    }
}