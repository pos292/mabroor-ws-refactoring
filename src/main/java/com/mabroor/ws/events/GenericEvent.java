package com.mabroor.ws.events;

import org.springframework.context.ApplicationEvent;

public class GenericEvent<T> extends ApplicationEvent {
    private T what;
    protected boolean amitra;

    public GenericEvent(Object source, T what, boolean amitra) {
        super(source);
        this.what = what;
        this.amitra = amitra;
    }

    public T getWhat(){
        return what;
    }

    public boolean isAmitra(){
        return amitra;
    }
}
