package com.mabroor.ws.events.event;

import com.mabroor.ws.dto.PembiayaanUmrohDto;
import com.mabroor.ws.dto.ReqSubmitPengajuanDto;
import com.mabroor.ws.entity.MstPengajuanUmroh;
import org.springframework.context.ApplicationEvent;

public class SubmitAmitraEvent extends ApplicationEvent {
    private MstPengajuanUmroh pengajuanUmroh;
    private PembiayaanUmrohDto pembiayaanUmrohDto;

    public SubmitAmitraEvent(Object source, MstPengajuanUmroh pengajuanUmroh, PembiayaanUmrohDto reqSubmitPengajuanDto) {
        super(source);
        this.pengajuanUmroh = pengajuanUmroh;
        this.pembiayaanUmrohDto = reqSubmitPengajuanDto;
    }

    public MstPengajuanUmroh getPengajuanUmroh() {
        return pengajuanUmroh;
    }

    public PembiayaanUmrohDto getPembiayaanUmrohDto() {
        return pembiayaanUmrohDto;
    }
}
