package com.mabroor.ws.events.listener;

import com.mabroor.ws.dto.PembiayaanUmrohDto;
import com.mabroor.ws.dto.ReqAmitraSubmission;
import com.mabroor.ws.entity.*;
import com.mabroor.ws.events.GenericEvent;
import com.mabroor.ws.events.event.SubmitAmitraEvent;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.repository.*;
import com.mabroor.ws.service.FifAmitraService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.impl.MailClient;
import com.mabroor.ws.util.Constant;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Component
public class SubmitAmitraEventListener {

    @Autowired
    private FifAmitraService fifAmitraService;

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    UserMobileRepository userMobileRepository;

    @Autowired
    PembiayaanUmrohRepository pembiayaanUmrohRepository;

    @Autowired
    InboxCategoryRepository inboxCategoryRepository;

    @Autowired
    InboxRepository inboxRepository;

    @Autowired
    private MailClient mailClient;
    @Autowired
    private PushNotificationService pushNotificationService;
    @Value("${user-key-fifamitra}")
    private String userKey;
    @Value(("${paket-code-fifamitra}"))
    private String paketCode;

    @EventListener(condition = "#event.amitra")
    public void onApplicationEvent(GenericEvent<SubmitAmitraEvent> event) {
        MstPengajuanUmroh mstPengajuanUmroh = event.getWhat().getPengajuanUmroh();
        PembiayaanUmrohDto pembiayaanUmrohDto = event.getWhat().getPembiayaanUmrohDto();
        LinkedHashMap<String, String> response = new LinkedHashMap<>();
        List<Integer> ids = new ArrayList<>();
        AtomicInteger idProvinsi = new AtomicInteger();
        AtomicReference<String> codeKota = new AtomicReference<>("0");
        AtomicReference<String> codeKecamatan = new AtomicReference<>("0");
        AtomicReference<String> codeKelurahan = new AtomicReference<>("0");
        String prov = pembiayaanUmrohDto.getAlamat().getProvinsi().contains("Prov ") ?
                pembiayaanUmrohDto.getAlamat().getProvinsi().replaceFirst("Prov ", "") : pembiayaanUmrohDto.getAlamat().getProvinsi();
        List<Integer> stateIds = fifAmitraService.getState(prov);
        String kecamatan = pembiayaanUmrohDto.getAlamat().getKecamatan().contains("Kec ") ?
                pembiayaanUmrohDto.getAlamat().getKecamatan().replaceFirst("Kec ", "") : pembiayaanUmrohDto.getAlamat().getKecamatan();
        String kelurahan = pembiayaanUmrohDto.getAlamat().getKelurahan().contains("Kel ") ?
                pembiayaanUmrohDto.getAlamat().getKelurahan().replaceFirst("Kel ", "") : pembiayaanUmrohDto.getAlamat().getKelurahan();
        stateIds.stream().forEach(stateId -> {
            List<LinkedHashMap<String, String>> citys = fifAmitraService.getCity(stateId, pembiayaanUmrohDto.getAlamat().getKota());
            log.info("stateId : {}", stateId);
            if (!citys.isEmpty()) {
                idProvinsi.set(stateId);
                citys.stream().forEach(city -> {
                    List<LinkedHashMap<String, String>> districts = fifAmitraService.getDistrict(Integer.valueOf(city.get("cityId")), kecamatan);
                    log.info("cityCode : {}", city.get("code"));
                    if (!districts.isEmpty()){
                        codeKota.set(city.get("code"));
                        districts.stream().forEach(district -> {
                            List<LinkedHashMap<String, String>> subDistricts = fifAmitraService.getSubDistrict(Integer.valueOf(district.get("districtId")), kelurahan);
                            if (!subDistricts.isEmpty()){
                                log.info("district : {}", district.get("code"));
                                log.info("sub district : {}", subDistricts.get(0).get("code"));
                                codeKecamatan.set(district.get("code"));
                                codeKelurahan.set(subDistricts.get(0).get("code"));
                            }
                        });
                    }
                });
            }
        });
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        ReqAmitraSubmission submission = ReqAmitraSubmission.builder()
                .callVia("MP")
                .contactDetail(pembiayaanUmrohDto.getDataDiri().getNomorTlpn())
                .paketCode(paketCode)
                .paketValue("88000000")
                .tenor(String.valueOf(pembiayaanUmrohDto.getTenor()))
                .dp(String.valueOf(pembiayaanUmrohDto.getDp()))
                .produkPembiayaan("U")
                .fullname(pembiayaanUmrohDto.getDataDiri().getNamaDepan()+ " " +pembiayaanUmrohDto.getDataDiri().getNamaBelakang())
                .noKTP(pembiayaanUmrohDto.getDataDiri().getNik())
                .mobilePhone1(pembiayaanUmrohDto.getDataDiri().getNomorTlpn())
                .email(pembiayaanUmrohDto.getDataDiri().getEmail())
                .birthdate(formatDate.format(pembiayaanUmrohDto.getDataDiri().getTanggalLahir()))
                .birthPlace(pembiayaanUmrohDto.getDataDiri().getTempatLahir())
                .gender(pembiayaanUmrohDto.getDataDiri().getJenisKelamin())
                .motherName(pembiayaanUmrohDto.getDataDiri().getIbuKandung())
                .addressLine(pembiayaanUmrohDto.getAlamat().getTempatTinggal())
                .noRT(pembiayaanUmrohDto.getAlamat().getRt())
                .noRW(pembiayaanUmrohDto.getAlamat().getRw())
                .stateId(String.valueOf(idProvinsi))
                .cityId(String.valueOf(codeKota))
                .districtId(String.valueOf(codeKecamatan))
                .subDistrictId(String.valueOf(codeKelurahan))
                .zipcode(pembiayaanUmrohDto.getAlamat().getKodePos())
                .subZipcode(pembiayaanUmrohDto.getAlamatKtp().getKodePos())
                .stateName(pembiayaanUmrohDto.getAlamat().getProvinsi())
                .cityName(pembiayaanUmrohDto.getAlamat().getKota())
                .districtName(pembiayaanUmrohDto.getAlamat().getKecamatan())
                .subDistrictName(pembiayaanUmrohDto.getAlamat().getKelurahan())
                .userKey(userKey)
                .build();
        val submit = fifAmitraService.submit(submission);
        System.out.println(submit.toString());
        // generate status for tracking status submitted
        String digitalLeadStatus = submit.getResult().get("digitalLeadStatus");
        String statusSubmit = submit.getResult().get("status");
        String statusKetMoxa = "";
        String statusCodeMoxa = "";
        if(statusSubmit != null && !statusSubmit.isEmpty() && digitalLeadStatus != null && !digitalLeadStatus.isEmpty()){
            if(statusSubmit.equalsIgnoreCase(Constant.AMITRA_STATUS_SUBMITTED)
                    && digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_TERKIRIM)){
                statusKetMoxa = "Pengajuan Terkirim";
                statusCodeMoxa = "Inprogress";
            } else if (statusSubmit.equalsIgnoreCase(Constant.AMITRA_STATUS_SUBMITTED)
                    && digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_DIPROSES)) {
                statusKetMoxa = "Pengajuan Diproses";
                statusCodeMoxa = "In Progress";
            }
        }

        MstApplication mstApplication = new MstApplication();
        mstApplication.setStatus(statusCodeMoxa);
        mstApplication.setLeadsId(mstPengajuanUmroh.getId());
        mstApplication.setLeadsType("UMROH");
        mstApplication.setPickupBu("Pembiayaan Umroh");
        mstApplication.setUserId(mstPengajuanUmroh.getMstUserMobile().getId());
        mstApplication.setCompany("FIF Amitra");

        mstPengajuanUmroh.setStatus(statusKetMoxa);
        mstPengajuanUmroh.setIntegrationLeadsId(submit.getResult().get("leadId"));
        applicationRepository.save(mstApplication);
        pembiayaanUmrohRepository.save(mstPengajuanUmroh);


        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest();
        Optional<MstUserMobile> mstUserMobile = userMobileRepository.findById(mstPengajuanUmroh.getMstUserMobile().getId());
//        String statusPengajuan = pembiayaanUmrohRepository.findById(mstPengajuanUmroh.getId()).get().getStatus();
        Map<String, String> data = new HashMap<>();
        if (submit.getResult().get("status").toUpperCase(Locale.ROOT).equalsIgnoreCase("SUBMITTED") && submit.getResult().get("digitalLeadStatus").contains("berhasil terkirim")){
            pushNotificationRequest.setTitle("Pembiyaan Umroh");
            pushNotificationRequest.setTopic("pengajuan-umroh");

            data.put("Pembiayaan Umroh", "Pengajuan Pembiayaan Terkirim");
            pushNotificationRequest.setToken(mstUserMobile.get().getFirebaseToken());
            pushNotificationRequest.setData(data);

            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Diterima");
        }
        else if (submit.getResult().get("status").toUpperCase(Locale.ROOT).equalsIgnoreCase("APPROVED") && submit.getResult().get("status").contains("Pengajuan Disetujui")) {
            pushNotificationRequest.setTitle("Pembiyaan Umroh");
            pushNotificationRequest.setTopic("pengajuan-umroh");
            pushNotificationRequest.setToken(mstUserMobile.get().getFirebaseToken());

            data.put("Pembiayaan Umroh", "Pengajuan Pembiayaan Diterima");
            pushNotificationRequest.setData(data);

            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Disetujui");
        }
        else if (submit.getResult().get("status").toUpperCase(Locale.ROOT).equalsIgnoreCase("REJECTED") && submit.getResult().get("status").equalsIgnoreCase("Pengajuan Ditolak")) {
            pushNotificationRequest.setTitle("Pembiyaan Umroh");
            pushNotificationRequest.setTopic("pengajuan-umroh");
            pushNotificationRequest.setToken(mstUserMobile.get().getFirebaseToken());

            data.put("Pembiayaan Umroh", "Pengajuan Pembiayaan Ditolak");
            pushNotificationRequest.setData(data);

            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Ditolak");
        }
        MstInboxCategory mstInboxCategory = inboxCategoryRepository.findById(3L).get();
        MstInbox inbox = new MstInbox();
        String descrition = "Hi " + pembiayaanUmrohDto.getDataDiri().getNamaDepan() + "! Ada update tentang pengajuan umroh yang kamu lakukan. Klik di sini untuk melihat status terkini!";
        inbox.setIcon("inbox_pengajuan.png");
        inbox.setInboxType("DIRECT_SCREEN");
        inbox.setInboxRead("NO");
        inbox.setDescription(descrition);
        inbox.setTitle("Status Pengajuan Pembiayaan Umroh");
        inbox.setScreenName("LihatDetailUmroh");
        inbox.setIdData(mstPengajuanUmroh.getId());
        inbox.setUserId(mstPengajuanUmroh.getMstUserMobile().getId());
        inbox.setIdInboxCategory(mstInboxCategory);
        inboxRepository.save(inbox);
        try {
            pushNotificationService.sendMessageToToken(pushNotificationRequest);
            pushNotificationService.sendMessage(data,pushNotificationRequest);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        mailClient.prepareAndSendPembiayaanUmroh(pembiayaanUmrohDto.getDataDiri().getEmail(),"Pembiayaan Umroh", mstPengajuanUmroh,pembiayaanUmrohDto.getDataDiri().getNamaDepan());
    }


}



