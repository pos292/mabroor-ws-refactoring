package com.mabroor.ws.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class AuthorizationException extends RuntimeException{

    private static final long serialVersionUID = 4867193963080361998L;
    public AuthorizationException(String message) {
        super(message);
    }
}
