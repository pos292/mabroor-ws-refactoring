package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstHajiAccMaster;

public interface PembiayaanHajiAccMasterService {

    List<MstHajiAccMaster> findAll();

    List<MstHajiAccMaster> getJumlahPesertaAcc();

    List<MstHajiAccMaster> getTenorAcc();

    List<MstHajiAccMaster> getProductAcc();
}
