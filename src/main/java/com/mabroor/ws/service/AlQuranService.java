package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstAlQuran;

public interface AlQuranService {
	
	List<MstAlQuran> findAll(String title);
	
	List<MstAlQuran> listJuz();
	
	List<MstAlQuran> filterSurah(String title);
	
	List<MstAlQuran> filterJuz(Long juz);
	
	List<MstAlQuran> searchJuz(String title);
	
	List<MstAlQuran> searchSura(String title);

}
