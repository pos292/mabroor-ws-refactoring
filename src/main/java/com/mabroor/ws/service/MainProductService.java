package com.mabroor.ws.service;

import java.util.List;
import java.util.Map;

public interface MainProductService {
	List<Map<String, Object>> getListActive();
}
