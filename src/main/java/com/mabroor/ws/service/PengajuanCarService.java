package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.dto.CountAccV2Dto;
import com.mabroor.ws.entity.MstPengajuanCar;

public interface PengajuanCarService {

	List<MstPengajuanCar> findAllByStatus();
	
	List<MstPengajuanCar> findAllByUserId(Long id);
    
	MstPengajuanCar save(MstPengajuanCar mstPengajuan);
    
	MstPengajuanCar update(MstPengajuanCar mstPengajuan);

    Optional <MstPengajuanCar> findById(Long id);
    
    List<MstPengajuanCar> getListPengajuanBekas();
    
	List<MstPengajuanCar> findCompanyAvailable();

	List<MstPengajuanCar> getListPengajuan();
	
	List<MstPengajuanCar> findAllEnable();

	List<MstPengajuanCar> getCountTaf(int limit, int offset);

	List<CountAccV2Dto> getCountAccV2(String brand);
}
