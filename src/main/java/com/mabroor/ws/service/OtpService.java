package com.mabroor.ws.service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.mabroor.ws.entity.MstOtp;

public interface OtpService {

	String save(MstOtp mstOtp);
	
	String saveGeneral(MstOtp mstOtp);
	
	LinkedHashMap<String, Object> verify(MstOtp mstOtp);
	
	String verifyGeneral(MstOtp mstOtp);
	
	List<MstOtp> findAllOtp();
	
	String resend(MstOtp mstOtp);
	
	String resendGeneral(MstOtp mstOtp);
	
	List<MstOtp> findByExpiredDate(String value1, Date value2, String value3);
	
	MstOtp findByPhoneNumber (String phoneNumber);

	void updateAllVerificationByPhoneNumber(String phoneNumber);
}
