package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstOtp;
import com.mabroor.ws.entity.MstOtpCount;

public interface OtpCountService {

	List<MstOtpCount> findByMstOtp(MstOtp value);
}
