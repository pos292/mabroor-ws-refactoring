package com.mabroor.ws.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.entity.MstUserMobile;

public interface UserMobileService {

	List<MstUserMobile> findAllByStatus();
	
	String save (MultipartFile ktpImage,MultipartFile npwpImage,MultipartFile bpkbMobilImage, MultipartFile bpkbMotorImage,MultipartFile kkImage,MultipartFile ppImage, MstUserMobile mstUserMobile);
	
	MstUserMobile update (MultipartFile ktpImage,MultipartFile npwpImage,MultipartFile bpkbMobilImage,MultipartFile bpkbMotorImage,MultipartFile kkImage,MultipartFile ppImage, MstUserMobile mstUserMobile);
		
    String saveIdent (MultipartFile ktpImage,MultipartFile npwpImage,MultipartFile kkImage,MultipartFile ppImage, MstUserMobile mstUserMobile);
	
	MstUserMobile updateIdent (MultipartFile ktpImage,MultipartFile npwpImage,MultipartFile kkImage,MultipartFile ppImage, MstUserMobile mstUserMobile);
	
    String saveIdentityKtp (MultipartFile ktpImage,MstUserMobile mstUserMobile);
	
	MstUserMobile updateIdentityKtp (MultipartFile ktpImage, MstUserMobile mstUserMobile);

	String saveIdentityNpwp (MultipartFile npwpImage,MstUserMobile mstUserMobile);
		
    MstUserMobile updateIdentityNpwp (MultipartFile npwpImage, MstUserMobile mstUserMobile);
		
    String saveIdentityKk (MultipartFile kkImage,MstUserMobile mstUserMobile);
	
    MstUserMobile updateIdentityKk (MultipartFile kkImage,MstUserMobile mstUserMobile);
    
    String saveIdentityPp (MultipartFile ppImage, MstUserMobile mstUserMobile);
	
    MstUserMobile updateIdentityPp (MultipartFile ppImage, MstUserMobile mstUserMobile);
    
	List<MstUserMobile> findAllEnable();
	
	MstUserMobile save1 (MstUserMobile mstUserMobile);
	
//	void insertMabroor (MstUserMobile mstUserMobile);
	
	MstUserMobile getOneByPhoneNumber (String phoneNumber);
	
	List<MstUserMobile> getListMobile();
	
	Optional<MstUserMobile>findById(Long id);
	
	String checkPhoneNumber(String phoneNumber);
	
	String checkPhoneUser(String phone);
	
	MstUserMobile getListProfile();
	
	void updateFirebaseToken(String firebaseToken, Long userId);

	void updatePin(String pin, Long userId);

	void updateCountPin(Long count, Long userId);

	void updateLockTimePin(Date tmpExpire, Long id);

	void resetLockTImePin(Long userId);

	void updateLockStatus(String string, Long id);

	void updateFingerprintToggle(String fingerprintToggle, Long id);
	
	void updateUserType(String type, Long userId);
	
	List<MstUserMobile> findAllUserPromoBundle();

	boolean checkMoxaExistingUser(String phone);

	boolean acceptanceMoxaMabroor(String state, String phone);

	MstUserMobile findPreviousUser(String phoneNumber);
	
}
