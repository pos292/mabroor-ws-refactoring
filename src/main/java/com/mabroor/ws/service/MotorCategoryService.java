package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstMotorCategory;

public interface MotorCategoryService {

	List<MstMotorCategory> findAllByStatus();
	
	List<MstMotorCategory> getListMotorCategory();
	
	List<MstMotorCategory> findAllByBrandId(Long id);
	
	Optional<MstMotorCategory> findById(Long id);
}
