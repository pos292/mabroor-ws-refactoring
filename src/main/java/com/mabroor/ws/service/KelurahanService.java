package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstKelurahan;

public interface KelurahanService {
	
	List<MstKelurahan> findAllByStatus();

	Optional<MstKelurahan> findById(Long id);
	
	List<MstKelurahan> getListKelurahan();
	
	List<MstKelurahan> findByKecamatanId(Long id);

}
