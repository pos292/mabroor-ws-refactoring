package com.mabroor.ws.service;

import java.util.LinkedHashMap;

import com.mabroor.ws.dto.MotorFilterDto;

public interface MotorService {
	
    LinkedHashMap<String, Object> getMotorByLowCicilan();
    
    LinkedHashMap<String, Object> getMotorByLowCicilanBestDeal();
    
    LinkedHashMap<String, Object> getMotorByLowCicilanTypeIdBestDeal(Long typeId);
    
    LinkedHashMap<String, Object> getMotorByLowCicilanTypeId(Long typeId);
    
    LinkedHashMap<String, Object> getMasterFilter();
    
    LinkedHashMap<String, Object> applyMotorFilter(MotorFilterDto motorFilter);
}