package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstPengajuanAsuransiMobil;

public interface PengajuanAsuransiMobilService {

	List<MstPengajuanAsuransiMobil> getListAsuransiMobil();

	Optional<MstPengajuanAsuransiMobil> findById(Long id);
	
	MstPengajuanAsuransiMobil save (MstPengajuanAsuransiMobil mstMobil);
	
	MstPengajuanAsuransiMobil update (MstPengajuanAsuransiMobil mstMobil);
	
	List<MstPengajuanAsuransiMobil> findAllByUserId(Long id);
	
	
}
