package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstPromotionBundle;

public interface PromotionBundleService {
	
	List<MstPromotionBundle> getList();
	
	Optional<MstPromotionBundle> findById(Long id);
	
	MstPromotionBundle save(MstPromotionBundle mstBundle);
	
	MstPromotionBundle update(MstPromotionBundle mstBundle);
	
	List<MstPromotionBundle> findByData(Long id);
	
	List<MstPromotionBundle> findByEmailNull(Long id);
	
//	Optional<MstPromotionBundle> findByData();

}
