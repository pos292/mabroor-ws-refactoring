package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstOtp;
import com.mabroor.ws.entity.MstOtpCount;
import com.mabroor.ws.repository.OtpCountRepository;
import com.mabroor.ws.service.OtpCountService;

@Service
public class OtpCountServiceImpl implements OtpCountService{
	@Autowired
	private OtpCountRepository otpCountRepo;

	@Override
	public List<MstOtpCount> findByMstOtp(MstOtp value) {
		// TODO Auto-generated method stub
		return otpCountRepo.findByMstOtp(value);
	}
}
