package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstAvailableTime;
import com.mabroor.ws.repository.AvailableTimeRepository;
import com.mabroor.ws.service.AvailableTimeService;


@Service
public class AvailableTimeServiceImpl implements AvailableTimeService {

	@Autowired
	private AvailableTimeRepository timeRepository;

	@Override
	public List<MstAvailableTime> findAllByStatus() {
		// TODO Auto-generated method stub
		return timeRepository.findAllByStatus();
	}

	@Override
	public Optional<MstAvailableTime> findById(Long id) {
		// TODO Auto-generated method stub
		return timeRepository.findById(id);
	}

	@Override
	public List<MstAvailableTime> findAllEnable() {
		// TODO Auto-generated method stub
		return timeRepository.findAllEnable();
	}

	@Override
	public List<MstAvailableTime> getListAvailableTime() {
		// TODO Auto-generated method stub
		return timeRepository.getListAvailableTime();
	}

	@Override
	public List<MstAvailableTime> findByListId(String[] id) {
		// TODO Auto-generated method stub
		return timeRepository.findByListId(id);
	}
}
