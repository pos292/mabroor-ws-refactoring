package com.mabroor.ws.service.impl;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.entity.MstPengajuanMotor;
import com.mabroor.ws.repository.PengajuanMotorRepository;
import com.mabroor.ws.service.PengajuanMotorService;
import com.mabroor.ws.util.UploadImage;

@Service
public class PengajuanMotorServiceImpl implements PengajuanMotorService  {

	@Autowired
	private PengajuanMotorRepository pengajuanRepository;
	
	@Value("${storage.upload.file}")
	private String storageUploadFile;
	
	@Override
	public List<MstPengajuanMotor> findAllByStatus() {
		// TODO Auto-generated method stub
		return pengajuanRepository.findAllByStatus();
	}

	@Override
	public MstPengajuanMotor save(MultipartFile fileFotoUnit, MultipartFile fileFotoKtp, MstPengajuanMotor mstPengajuan) {
		String newFilename_fotoUnit = "";
		if (fileFotoUnit != null && !fileFotoUnit.isEmpty()) {
			String directoryName = this.storageUploadFile + "pengajuan-motor/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_fotoUnit = UploadImage.createImage(directoryName, fileFotoUnit, UUID.randomUUID().toString());
		}
		mstPengajuan.setFotoUnit(newFilename_fotoUnit);
		
		String newFilename_fotoKtp = "";
		if (fileFotoKtp != null && !fileFotoKtp.isEmpty()) {
			String directoryName = this.storageUploadFile + "pengajuan-motor/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_fotoKtp = UploadImage.createImage(directoryName, fileFotoKtp, UUID.randomUUID().toString());
		}
		mstPengajuan.setKtpPhoto(newFilename_fotoKtp);
		return pengajuanRepository.save(mstPengajuan);
	}

	@Override
	public MstPengajuanMotor update(MstPengajuanMotor mstPengajuan) {
		// TODO Auto-generated method stub
		return pengajuanRepository.save(mstPengajuan);
	}

	@Override
	public Optional<MstPengajuanMotor> findById(Long id) {
		// TODO Auto-generated method stub
		return pengajuanRepository.findById(id);
	}

	@Override
	public List<MstPengajuanMotor> getListPengajuan() {
		// TODO Auto-generated method stub
		return pengajuanRepository.getListPengajuan();
	}

	@Override
	public List<MstPengajuanMotor> findAllEnable() {
		// TODO Auto-generated method stub
		return pengajuanRepository.findAllEnable();
	}

	@Override
	public List<MstPengajuanMotor> findAllByUserId(Long phone) {
		// TODO Auto-generated method stub
		return pengajuanRepository.findAllByUserId(phone);
	}

}
