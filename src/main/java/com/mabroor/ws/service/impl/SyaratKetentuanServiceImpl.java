package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstSyaratKetentuan;
import com.mabroor.ws.repository.SyaratKetentuanRepository;
import com.mabroor.ws.service.SyaratKetentuanService;

@Service
public class SyaratKetentuanServiceImpl implements SyaratKetentuanService {

	@Autowired
	private SyaratKetentuanRepository syaratRepo;
	
	@Override
	public List<MstSyaratKetentuan> findAllByStatus() {
		// TODO Auto-generated method stub
		return syaratRepo.findAllByStatus();
	}

	@Override
	public List<MstSyaratKetentuan> findAllById() {
		// TODO Auto-generated method stub
		return syaratRepo.findAllById();
	}

	@Override
	public List<MstSyaratKetentuan> findAllEnable() {
		// TODO Auto-generated method stub
		return syaratRepo.findAllEnable();
	}

	@Override
	public List<MstSyaratKetentuan> getListSyarat() {
		// TODO Auto-generated method stub
		return syaratRepo.getListSyarat();
	}

	@Override
	public List<MstSyaratKetentuan> findByCategory(String categoryName) {
		// TODO Auto-generated method stub
		return syaratRepo.findByCategory(categoryName);
	}

}
