package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstAlQuran;
import com.mabroor.ws.repository.AlQuranRepository;
import com.mabroor.ws.service.AlQuranService;

@Service
public class AlQuranServiceImpl implements AlQuranService {
	
	@Autowired
	private AlQuranRepository quranRep;

	@Override
	public List<MstAlQuran> findAll(String title) {
		// TODO Auto-generated method stub
		return quranRep.findAll(title);
	}

	@Override
	public List<MstAlQuran> listJuz() {
		// TODO Auto-generated method stub
		return quranRep.listJuz();
	}

	@Override
	public List<MstAlQuran> filterSurah(String title) {
		// TODO Auto-generated method stub
		return quranRep.filterSurah(title);
	}

	@Override
	public List<MstAlQuran> filterJuz(Long juz) {
		// TODO Auto-generated method stub
		return quranRep.filterJuz(juz);
	}

	@Override
	public List<MstAlQuran> searchSura(String title) {
		// TODO Auto-generated method stub
		return quranRep.searchSura(title);
	}

	@Override
	public List<MstAlQuran> searchJuz(String title) {
		// TODO Auto-generated method stub
		return quranRep.searchJuz(title);
	}

}
