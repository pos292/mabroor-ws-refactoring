package com.mabroor.ws.service.impl;

import com.mabroor.ws.dto.PembiayaanUmrohDataDiriDto;
import com.mabroor.ws.dto.PembiayaanUmrohDto;
import com.mabroor.ws.entity.*;
import com.mabroor.ws.events.GenericEvent;
import com.mabroor.ws.events.event.SubmitAmitraEvent;
import com.mabroor.ws.exception.BadRequestException;
import com.mabroor.ws.repository.*;
import com.mabroor.ws.service.PembiayaanUmrohService;
import com.mabroor.ws.service.UserMobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Optional;

@Service
public class PembiayaanUmrohServiceImpl implements PembiayaanUmrohService {

    @Autowired
    PembiayaanUmrohRepository pembiayaanUmrohRepository;

    @Autowired
    PembiayaanUmrohDataDiriRepository pembiayaanUmrohDataDiriRepository;

    @Autowired
    PembiayaanUmrohAlamatRepository pembiayaanUmrohAlamatRepository;

    @Autowired
    UserMobileRepository userMobileRepository;

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public MstPengajuanUmroh saveUmroh(PembiayaanUmrohDto pembiayaanUmrohDto, MstUserMobile userMobile) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(pembiayaanUmrohDto.getDataDiri().getNamaDepan());
        stringBuilder.append(pembiayaanUmrohDto.getDataDiri().getNamaBelakang());

        String namaLengkap = stringBuilder.toString().replaceAll("\\s","");
        int panjangNama = namaLengkap.length();
        if(panjangNama > 95){
            throw new BadRequestException("Panjang Nama Lengkap tidak boleh lebih dari 95");
        }

        String jenisKelamin = pembiayaanUmrohDto.getDataDiri().getJenisKelamin();
        if (!(jenisKelamin.equalsIgnoreCase("F") || jenisKelamin.equalsIgnoreCase("M"))){
            throw new BadRequestException("Jenis Kelamin tidak boleh selain F / M");
        }
        String gender = "";
        if(pembiayaanUmrohDto.getDataDiri().getJenisKelamin().equalsIgnoreCase("F")){
            gender = "Perempuan";
        }else {
            gender = "Laki-laki";
        }
        userMobile.setGender(gender);
        userMobile.setPhone(pembiayaanUmrohDto.getDataDiri().getNomorTlpn());
        userMobile.setEmail(pembiayaanUmrohDto.getDataDiri().getEmail());
        userMobile.setKtpNumber(pembiayaanUmrohDto.getDataDiri().getNik());
        userMobile.setAddress(pembiayaanUmrohDto.getAlamat().getTempatTinggal());
        userMobile.setAddressKtp(pembiayaanUmrohDto.getAlamatKtp().getAlamatLengkap());
        userMobileRepository.save(userMobile);

        MstPengajuanUmroh umroh = new MstPengajuanUmroh();
        MstPengajuanUmrohDataDiri umrohDataDiri = new MstPengajuanUmrohDataDiri();
        MstPengajuanUmrohAlamat umrohAlamat = new MstPengajuanUmrohAlamat();
        umroh.setDp(pembiayaanUmrohDto.getDp());
        umroh.setTenor(pembiayaanUmrohDto.getTenor());
        umroh.setMstUserMobile(userMobile);
        umroh.setCreatedBy(userMobile.getName());
        umroh.setStatus("Submitted");
        umroh.setPromoName(pembiayaanUmrohDto.getPromoName());
        umroh.setPromoCode(pembiayaanUmrohDto.getPromoCode());


        umrohDataDiri.setNamaDepan(pembiayaanUmrohDto.getDataDiri().getNamaDepan());
        umrohDataDiri.setNamaBelakang(pembiayaanUmrohDto.getDataDiri().getNamaBelakang());
        umrohDataDiri.setGender(pembiayaanUmrohDto.getDataDiri().getJenisKelamin());
        umrohDataDiri.setTempatLahir(pembiayaanUmrohDto.getDataDiri().getTempatLahir());
        umrohDataDiri.setTanggalLahir(pembiayaanUmrohDto.getDataDiri().getTanggalLahir());
        umrohDataDiri.setIbuKandung(pembiayaanUmrohDto.getDataDiri().getIbuKandung());
        umrohDataDiri.setNoHp(pembiayaanUmrohDto.getDataDiri().getNomorTlpn());
        umrohDataDiri.setEmail(pembiayaanUmrohDto.getDataDiri().getEmail());
        umrohDataDiri.setNoKtp(pembiayaanUmrohDto.getDataDiri().getNik());
        umrohDataDiri.setCreatedBy(userMobile.getName());

        umrohAlamat.setAlamat(pembiayaanUmrohDto.getAlamat().getTempatTinggal());
        umrohAlamat.setProvinsi(pembiayaanUmrohDto.getAlamat().getProvinsi());
        umrohAlamat.setKota(pembiayaanUmrohDto.getAlamat().getKota());
        umrohAlamat.setKecamatan(pembiayaanUmrohDto.getAlamat().getKecamatan());
        umrohAlamat.setKelurahan(pembiayaanUmrohDto.getAlamat().getKelurahan());
        umrohAlamat.setRt(pembiayaanUmrohDto.getAlamat().getRt());
        umrohAlamat.setRw(pembiayaanUmrohDto.getAlamat().getRw());
        umrohAlamat.setKodePos(pembiayaanUmrohDto.getAlamat().getKodePos());
        umrohAlamat.setCreatedBy(userMobile.getName());

        umrohAlamat.setAlamatKtp(pembiayaanUmrohDto.getAlamatKtp().getAlamatLengkap());
        umrohAlamat.setKostpos_ktp(pembiayaanUmrohDto.getAlamatKtp().getKodePos());

        MstPengajuanUmroh umrohS = pembiayaanUmrohRepository.save(umroh);
        umrohDataDiri.setMstPengajuanUmroh(umrohS);
        umrohAlamat.setMstPengajuanUmroh(umrohS);
        MstPengajuanUmrohDataDiri umrohD = pembiayaanUmrohDataDiriRepository.save(umrohDataDiri);
        MstPengajuanUmrohAlamat umrohA = pembiayaanUmrohAlamatRepository.save(umrohAlamat);

        userMobile.setFirstName(pembiayaanUmrohDto.getDataDiri().getNamaDepan());
        userMobile.setLastName(pembiayaanUmrohDto.getDataDiri().getNamaBelakang());

        SubmitAmitraEvent emailEvent = new SubmitAmitraEvent(this, umrohS, pembiayaanUmrohDto);
        GenericEvent<SubmitAmitraEvent> event = new GenericEvent<>(this, emailEvent, true);
        applicationEventPublisher.publishEvent(event);

        return umrohS;
    }

    @Override
    public MstPengajuanUmroh saveStatus(MstPengajuanUmroh mstPengajuanUmroh) {
        return pembiayaanUmrohRepository.save(mstPengajuanUmroh);
    }


    @Override
    public Optional<MstPengajuanUmroh> findById(Long id) {
        return pembiayaanUmrohRepository.findById(id);
    }
}
