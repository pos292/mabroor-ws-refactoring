package com.mabroor.ws.service.impl;

import com.mabroor.ws.dto.ReqAmitraSubmission;
import com.mabroor.ws.dto.ResFifAmitraDto;
import com.mabroor.ws.dto.ResFifAmitraListDataDto;
import com.mabroor.ws.dto.ResFifAmitraListDto;
import com.mabroor.ws.rest.FifAmitraClient;
import com.mabroor.ws.service.FifAmitraService;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class FifAmitraServiceImpl implements FifAmitraService {

    @Value("${axway.api.key}")
    private String apiKey;

    private String agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";

    private final FifAmitraClient fifAmitraClient;

    @Override
    public List<Integer> getState(String state) {
        ResFifAmitraListDto response = new ResFifAmitraListDto();
        List<Integer> listId = new ArrayList<>();
        try{
            response = fifAmitraClient.getListState(agent, apiKey);
            List<LinkedHashMap<String, String>> result = response.getResult();
            val res = result.stream().filter(
                            x -> x.get("name").toUpperCase(Locale.ROOT)
                                    .contains(state.toUpperCase(Locale.ROOT)))
                    .collect(Collectors.toList());
            res.forEach(x -> listId.add(Integer.valueOf(x.get("stateId"))));
            return listId;
        }catch(FeignException e){
            log.error("Error feign client amitra "+e.getMessage());
            return listId;
        }
    }

    @Override
    public List<LinkedHashMap<String, String>> getCity(int id, String city) {
        ResFifAmitraListDto response = new ResFifAmitraListDto();
        List<LinkedHashMap<String, String>> res = null;
        try{
            response = fifAmitraClient.getListCity(agent, apiKey, id);
            List<LinkedHashMap<String, String>> result =
                    response.getResult();
            res = result.stream().filter(
                            x -> x.get("name").toUpperCase(Locale.ROOT)
                                    .contains(city.toUpperCase(Locale.ROOT)))
                    .collect(Collectors.toList());
            return res;
        }catch(FeignException e){
            log.error("Error feign client amitra "+e.getMessage());
            return res;
        }
    }

    @Override
    public List<LinkedHashMap<String, String>>  getDistrict(int id, String district) {
        ResFifAmitraListDto response = new ResFifAmitraListDto();
        List<LinkedHashMap<String,String>> res = null;
         try{
            response = fifAmitraClient.getListDistrict(agent, apiKey, id);
            List<LinkedHashMap<String, String>> result =
                    response.getResult();
            res = result.stream().filter(
                            x -> x.get("name").toUpperCase(Locale.ROOT)
                                    .contains(district.toUpperCase(Locale.ROOT)))
                    .collect(Collectors.toList());
            return res;
        }catch(FeignException e){
            log.error("Error feign client amitra "+e.getMessage());
            return res;
        }
    }

    @Override
    public List<LinkedHashMap<String, String>>  getSubDistrict(int id, String subDistrict) {
        ResFifAmitraListDto response = new ResFifAmitraListDto();
        List<LinkedHashMap<String,String>> res = null;
        try{
            response = fifAmitraClient.getListSubDistrict(agent, apiKey, id);
            List<LinkedHashMap<String, String>> result =
                    response.getResult();
            res = result.stream().filter(
                            x -> x.get("name").toUpperCase(Locale.ROOT)
                                    .contains(subDistrict.toUpperCase(Locale.ROOT)))
                    .collect(Collectors.toList());
            return res;
        }catch(FeignException e){
            log.error("Error feign client amitra "+e.getMessage());
            return res;
        }
    }

    @Override
    public ResFifAmitraDto submit(ReqAmitraSubmission request) {
        ResFifAmitraDto response = new ResFifAmitraDto();
        try {
            response = fifAmitraClient.callAmitraClient(agent, apiKey, request);
            return response;
        }catch(FeignException e){
            log.error("Error feign client amitra "+e.getMessage());
            return null;
        }
    }

    @Override
    public ResFifAmitraListDataDto getDigitalLeadStatus(String intLeadId) {
        ResFifAmitraListDataDto response = new ResFifAmitraListDataDto();
        try {
            response = fifAmitraClient.getDigitalStatusLeads(agent, apiKey, intLeadId);
            return response;
        }catch(FeignException e){
            log.error("Error feign client amitra "+e.getMessage());
            return null;
        }
    }
}
