package com.mabroor.ws.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstVideoReview;
import com.mabroor.ws.repository.VideoReviewRepository;
import com.mabroor.ws.service.VideoReviewService;


@Service
public class VideoReviewServiceImpl implements VideoReviewService{
	private static final Logger LOG = Logger.getLogger(VideoReviewServiceImpl.class);

	@PersistenceContext
	private EntityManager em;
	
	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Autowired
	private VideoReviewRepository videoReviewRepository;

	@Override
	public List<MstVideoReview> findAllByStatus() {
		return videoReviewRepository.findAllEnable();
	}

	@Override
	public Optional<MstVideoReview> findById(Long id) {
		Optional<MstVideoReview> tempNews = videoReviewRepository.findById(id);
		if(tempNews.isPresent()) {
			tempNews.get().setImage(this.imageBaseUrl + "videoReview/" + tempNews.get().getImage());
		}
		
		return tempNews;
	}

	@Override
	public List<MstVideoReview> findByMainProductId(Long idMainProduct) {
		// TODO Auto-generated method stub
		List<MstVideoReview> newsProductEpic = videoReviewRepository.findByMainProductId(idMainProduct);
		for (MstVideoReview b : newsProductEpic) {
			em.detach(b);
			b.setImage(this.imageBaseUrl + "videoReview/" + b.getImage());
		}
		return newsProductEpic;
	}

	@Override
	public List<MstVideoReview> findByMainProductIdList(Long idMainProduct) {
		// TODO Auto-generated method stub
		List<MstVideoReview> newsProductEpic = videoReviewRepository.findByMainProductIdList(idMainProduct);
		for (MstVideoReview b : newsProductEpic) {
			em.detach(b);
			b.setImage(this.imageBaseUrl + "videoReview/" + b.getImage());
		}
		return newsProductEpic;
	}

	@Override
	public List<Map<String, String>> findAllChips() {
		
		List<Map<String, String>> listChips = new ArrayList<>();

		Map<String, String> tmpMap = new HashMap<>();
		tmpMap.put("title", "Semua");
		listChips.add(tmpMap);

		tmpMap = new HashMap<>();
		tmpMap.put("title", "Islami");
		listChips.add(tmpMap);
		
		tmpMap = new HashMap<>();
		tmpMap.put("title", "Keuangan");
		listChips.add(tmpMap);

		tmpMap = new HashMap<>();
		tmpMap.put("title", "Otomotif");
		listChips.add(tmpMap);

		tmpMap = new HashMap<>();
		tmpMap.put("title", "Review");
		listChips.add(tmpMap);
		
		return listChips;
	}
	
	@Override
	public List<Map<String, String>> findLandingpageChips() {
		List<Map<String, String>> listChips = new ArrayList<>();

		Map<String, String> tmpMap = new HashMap<>();
		tmpMap.put("title", "Semua");
		listChips.add(tmpMap);

		List<MstVideoReview> listActiveVideoReview = videoReviewRepository.findActiveSubCategory();
		for(MstVideoReview data : listActiveVideoReview) {
			tmpMap = new HashMap<>();
			tmpMap.put("title", data.getSubCategory());
			listChips.add(tmpMap);
		}

		return listChips;
	}

	@Override
	public List<MstVideoReview> findByMainProductNameLimit(String mainProductName) {

		List<MstVideoReview> videoReview = new ArrayList<>();
		if(mainProductName.equalsIgnoreCase("semua")) {
			videoReview = videoReviewRepository.findAllEnableLimit();
		}
		else {
			videoReview = videoReviewRepository.findByCategoryLimit(mainProductName);
		}

		for (MstVideoReview b : videoReview) {
			em.detach(b);
			b.setImage(this.imageBaseUrl + "videoReview/" + b.getImage());
		}
		return videoReview;
	}

	@Override
	public List<MstVideoReview> findByMainProductName(String mainProductName) {

		List<MstVideoReview> videoReview = new ArrayList<>();
		if(mainProductName.equalsIgnoreCase("semua")) {
			videoReview = videoReviewRepository.findAllEnable();
		}
		else {
			videoReview = videoReviewRepository.findByCategory(mainProductName);
		}

		for (MstVideoReview b : videoReview) {
			em.detach(b);
			b.setImage(this.imageBaseUrl + "videoReview/" + b.getImage());
		}
		return videoReview;
	}

	@Override
	public List<MstVideoReview> findByMainProductIdandSubCategoryName(Long idMainProduct, String subcategoryName) {
		
		List<MstVideoReview> videoReview = new ArrayList<>();
		if(subcategoryName.equalsIgnoreCase("semua")){
			videoReview = videoReviewRepository.findByMainProductIdList(idMainProduct);
		}else {
			videoReview = videoReviewRepository.findByMainProductIdandSubCategoryName(idMainProduct, subcategoryName);
		}
		if(!videoReview.isEmpty()) {
			for(MstVideoReview a : videoReview) {
				a.setImage(this.imageBaseUrl + "videoReview/" + a.getImage());
			}
		}
		return videoReview;
	}
}
