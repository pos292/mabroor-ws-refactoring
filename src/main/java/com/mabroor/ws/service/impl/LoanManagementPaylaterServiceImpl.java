package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstLoanManagementPaylater;
import com.mabroor.ws.repository.LoanManagementPaylaterRepository;
import com.mabroor.ws.service.LoanManagementPaylaterService;

@Service
public class LoanManagementPaylaterServiceImpl implements LoanManagementPaylaterService {
	
	@Autowired
	private LoanManagementPaylaterRepository loanPaylaterRepo;

	@Override
	public List<MstLoanManagementPaylater> getList() {
		// TODO Auto-generated method stub
		return loanPaylaterRepo.getList();
	}

	@Override
	public Optional<MstLoanManagementPaylater> getListLoanType(String loanType) {
		// TODO Auto-generated method stub
		return loanPaylaterRepo.getListLoanType(loanType);
	}

	@Override
	public Optional<MstLoanManagementPaylater> findById(Long id) {
		// TODO Auto-generated method stub
		return loanPaylaterRepo.findById(id);
	}

}
