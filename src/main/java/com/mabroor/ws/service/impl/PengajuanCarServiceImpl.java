package com.mabroor.ws.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.mabroor.ws.dto.CountAccV2Dto;
import com.mabroor.ws.entity.MstPengajuanCar;
import com.mabroor.ws.repository.PengajuanCarRepository;
import com.mabroor.ws.service.PengajuanCarService;

@Service
public class PengajuanCarServiceImpl implements PengajuanCarService {

	@Autowired
	private PengajuanCarRepository pengajuanRepository;
	
	@Value("${storage.upload.file}")
	private String storageUploadFile;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public List<MstPengajuanCar> findAllByStatus() {
		// TODO Auto-generated method stub
		return pengajuanRepository.findAllByStatus();
	}

	@Override
	public MstPengajuanCar save(MstPengajuanCar mstPengajuan) {
		
		return pengajuanRepository.save(mstPengajuan);
	}

	@Override
	public MstPengajuanCar update(MstPengajuanCar mstPengajuan) {
		// TODO Auto-generated method stub
		return pengajuanRepository.save(mstPengajuan);
	}

	@Override
	public Optional<MstPengajuanCar> findById(Long id) {
		// TODO Auto-generated method stub
		return pengajuanRepository.findById(id);
	}

	@Override
	public List<MstPengajuanCar> getListPengajuan() {
		// TODO Auto-generated method stub
		return pengajuanRepository.getListPengajuan();
	}

	@Override
	public List<MstPengajuanCar> findAllEnable() {
		// TODO Auto-generated method stub
		return pengajuanRepository.findAllEnable();
	}

	@Override
	public List<MstPengajuanCar> findAllByUserId(Long id) {
		// TODO Auto-generated method stub
		return pengajuanRepository.findAllByUserId(id);
	}

	@Override
	public List<MstPengajuanCar> findCompanyAvailable() {
		// TODO Auto-generated method stub
		return pengajuanRepository.findCompanyAvailable();
	}

	@Override
	public List<MstPengajuanCar> getListPengajuanBekas() {
		// TODO Auto-generated method stub
		return pengajuanRepository.getListPengajuanBekas();
	}
	@Override
	public List<MstPengajuanCar> getCountTaf(int limit, int offset) {
		// TODO Auto-generated method stub
		
		List<MstPengajuanCar> arrayList = new ArrayList<MstPengajuanCar>();
		Map<String, Object> paramaterMap = new HashMap<String, Object>();
		System.out.println("limit : " + limit);
		System.out.println("offset : " + offset);
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select * from (\n" + 
				"	select id, brand, company_name, distribution_type from mst_pengajuan_car order by created_date asc limit all offset :offset \n" + 
				") a where a.company_name = 'TAF' and a.distribution_type = 'SIXONE' limit :limit");
		paramaterMap.put("limit", limit);
		paramaterMap.put("offset", offset);
		Query query = entityManager.createNativeQuery(queryBuilder.toString());
		for (String param : paramaterMap.keySet()) {
			query.setParameter(param, paramaterMap.get(param));
		}
		arrayList = (List<MstPengajuanCar>) query.getResultList();
		
		return arrayList;
	}
	
	@Override
	public List<CountAccV2Dto> getCountAccV2(String brand) {
		// TODO Auto-generated method stub
		List<CountAccV2Dto> arrayList = new ArrayList<CountAccV2Dto>();
		
		Query query = entityManager.createNamedQuery("countAccV2Car");
		query.setParameter(1, brand);
		arrayList = (List<CountAccV2Dto>) query.getResultList();
		
		return arrayList;
	}

}
