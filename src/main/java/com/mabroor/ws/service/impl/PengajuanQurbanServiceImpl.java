package com.mabroor.ws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPengajuanQurban;
import com.mabroor.ws.repository.PengajuanQurbanRepository;
import com.mabroor.ws.service.PengajuanQurbanService;

@Service
public class PengajuanQurbanServiceImpl implements PengajuanQurbanService {
	
	@Autowired
	private PengajuanQurbanRepository qurbanRep;

	@Override
	public MstPengajuanQurban save(MstPengajuanQurban mstQurban) {
		// TODO Auto-generated method stub
		return qurbanRep.save(mstQurban);
	}

	@Override
	public MstPengajuanQurban update(MstPengajuanQurban mstQurban) {
		// TODO Auto-generated method stub
		return qurbanRep.save(mstQurban);
	}

}
