package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstGeneralInsuranceCarProduct;
import com.mabroor.ws.repository.InsuranceCarProductRepository;
import com.mabroor.ws.service.InsuranceCarProductService;

@Service
public class InsuranceCarServiceImpl implements InsuranceCarProductService {
	
	@Autowired
	private InsuranceCarProductRepository inscCarRepo;

	@Override
	public List<MstGeneralInsuranceCarProduct> getList() {
		// TODO Auto-generated method stub
		return inscCarRepo.getList();
	}

	@Override
	public Optional<MstGeneralInsuranceCarProduct> findById(Long id) {
		// TODO Auto-generated method stub
		return inscCarRepo.findById(id);
	}

	@Override
	public List<MstGeneralInsuranceCarProduct> getListCarId(Long generalId) {
		// TODO Auto-generated method stub
		return inscCarRepo.getListCarId(generalId);
	}

}
