package com.mabroor.ws.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPromotion;
import com.mabroor.ws.entity.MstPromotionSetting;
import com.mabroor.ws.repository.PromotionRepository;
import com.mabroor.ws.repository.PromotionSettingRepository;
import com.mabroor.ws.service.PromotionSettingService;

@Service
public class PromotionSettingServiceImpl implements PromotionSettingService {
	
	@Autowired
	private PromotionSettingRepository promoSettingRepo;
	
	@Autowired
	private PromotionRepository promoRepo;



	@Override
	public List<MstPromotionSetting> getListPromotionSetting() {
		// TODO Auto-generated method stub
		return promoSettingRepo.getListPromotionSetting();
	}

	
}
