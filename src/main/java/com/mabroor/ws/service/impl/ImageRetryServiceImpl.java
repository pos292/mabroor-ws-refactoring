package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstImageRetry;
import com.mabroor.ws.repository.ImageRetryRepository;
import com.mabroor.ws.service.ImageRetryService;

@Service
public class ImageRetryServiceImpl implements ImageRetryService{
	
	@Autowired
	private ImageRetryRepository imgRepo;

	@Override
	public List<MstImageRetry> getListImgSelfie() {
		// TODO Auto-generated method stub
		return imgRepo.getListImg();
	}

	@Override
	public MstImageRetry save(MstImageRetry mstImage) {
		// TODO Auto-generated method stub
		return imgRepo.save(mstImage);
	}

	@Override
	public void updateListImg(String Status, Long id) {
		// TODO Auto-generated method stub
		imgRepo.updateListImg(Status, id);
	}

	

	

}
