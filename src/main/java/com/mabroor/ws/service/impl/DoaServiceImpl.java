package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstDoa;
import com.mabroor.ws.repository.DoaRepository;
import com.mabroor.ws.service.DoaService;

@Service
public class DoaServiceImpl implements DoaService{

	@Autowired
	private DoaRepository doaRep;

	@Override
	public List<MstDoa> listDoa() {
		return doaRep.listDoa();
	}

	@Override
	public List<MstDoa> listDoaByIdCategory(Long idCategory) {
		return doaRep.listDoaByIdCategory(idCategory);
	}

	@Override
	public List<MstDoa> listSearchDoa(String namaDoa) {
		return doaRep.listSearchDoa(namaDoa);
	}

	@Override
	public MstDoa save(MstDoa doa) {
		return doaRep.save(doa);
	}
}
