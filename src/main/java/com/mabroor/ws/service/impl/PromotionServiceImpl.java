package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPromotion;
import com.mabroor.ws.repository.PromotionRepository;
import com.mabroor.ws.service.PromotionService;

@Service
public class PromotionServiceImpl implements PromotionService{

	@Autowired
	private PromotionRepository promotionRepo;

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;
	
	@Override
	public List<MstPromotion> getListPromotion() {
		List<MstPromotion> promo = promotionRepo.getListPromotion();
		if(!promo.isEmpty()) {
			for(MstPromotion a : promo) {
				em.detach(a);
				a.setBanner(this.imageBaseUrl + "promotion/" + a.getBanner());
			}
		}
		return promo;
	}

	@Override
	public List<MstPromotion> getListPromotionByMainProduct(Long idMainProduct) {
		return promotionRepo.getListPromotionByMainProduct(idMainProduct);
	}

	@Override
	public MstPromotion getPromotionByDetailPromo(Long id) {
		return promotionRepo.getPromotionByDetailPromo(id);
	}

	@Override
	public MstPromotion save(MstPromotion mstPromo) {
		// TODO Auto-generated method stub
		return promotionRepo.save(mstPromo);
	}

	@Override
	public MstPromotion update(MstPromotion mstPromo) {
		// TODO Auto-generated method stub
		return promotionRepo.save(mstPromo);
	}

	@Override
	public Optional<MstPromotion> findById(Long id) {
		// TODO Auto-generated method stub
		return promotionRepo.findById(id);
	}
}
