package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.repository.InboxRepository;
import com.mabroor.ws.service.InboxService;

@Service
public class InboxServiceImpl implements InboxService {
	
	@Autowired
	private InboxRepository inboxRepo;

//	@Override
//	public List<MstInbox> getInboxListRead() {
//		// TODO Auto-generated method stub
//		return inboxRepo.getInboxListRead();
//	}

	@Override
	public List<MstInbox> getInboxAll(Long userId) {
		// TODO Auto-generated method stub
		return inboxRepo.getInboxAll(userId);
	}

	@Override
	public List<MstInbox> getInboxId(Long idInboxCategory, Long userId) {
		// TODO Auto-generated method stub
		return inboxRepo.getInboxId(idInboxCategory, userId);
	}

	@Override
	@Transactional
	public int updateInboxRead(Long id) {
		// TODO Auto-generated method stub
		return inboxRepo.updateInboxRead(id);
	}

	@Override
	public List<MstInbox> getInboxAllBeforeLogin() {
		// TODO Auto-generated method stub
		return inboxRepo.getInboxAllBeforeLogin();
	}

	@Override
	public List<MstInbox> getInboxIdBeforeLogin(long idInboxCategory) {
		// TODO Auto-generated method stub
		return inboxRepo.getInboxIdBeforeLogin(idInboxCategory);
	}

	@Override
	public List<MstInbox> getInboxNotif(Long userId) {
		// TODO Auto-generated method stub
		return inboxRepo.getInboxNotif(userId);
	}

	@Override
	public List<MstInbox> getInboxNotifBeforeLogin() {
		// TODO Auto-generated method stub
		return inboxRepo.getInboxNotifBeforeLogin();
	}

	@Override
	public MstInbox save(MstInbox mstInbox) {
		// TODO Auto-generated method stub
		return inboxRepo.save(mstInbox);
	}

	@Override
	public Optional<MstInbox> isExistInboxId(Long inboxId) {
		return inboxRepo.findById(inboxId);
	}

	@Override
	public Optional<MstInbox> findIdByIdDataAndUserId(Long idData, Long userId) {
		return inboxRepo.findIdByIdDataAndUserId(idData, userId);
	}
}
