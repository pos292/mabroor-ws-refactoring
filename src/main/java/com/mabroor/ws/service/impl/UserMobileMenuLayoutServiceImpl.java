package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstUserMobileMenuLayout;
import com.mabroor.ws.repository.UserMobileMenuLayoutRepository;
import com.mabroor.ws.service.UserMobileMenuLayoutService;


@Service
public class UserMobileMenuLayoutServiceImpl implements UserMobileMenuLayoutService {

	@Autowired
	private UserMobileMenuLayoutRepository layoutRepo;
	
	@Override
	public List<MstUserMobileMenuLayout> findAllByUserId(Long userId) {
		// TODO Auto-generated method stub
		return layoutRepo.findAllByUserId(userId);
	}

	@Override
	public MstUserMobileMenuLayout save(MstUserMobileMenuLayout mstLayout) {
		// TODO Auto-generated method stub
		return layoutRepo.save(mstLayout);
	}

	@Override
	public MstUserMobileMenuLayout update(MstUserMobileMenuLayout mstLayout) {
		// TODO Auto-generated method stub
		return layoutRepo.save(mstLayout);
	}

	@Override
	public Optional<MstUserMobileMenuLayout> findById(Long id) {
		// TODO Auto-generated method stub
		return layoutRepo.findById(id);
	}

	@Override
	public List<MstUserMobileMenuLayout> findByIdMenu(Long idMenu) {
		// TODO Auto-generated method stub
		return layoutRepo.findByIdMenu(idMenu);
	}
	
	@Override
	public int resetUserMobileMenu(Long userId) {
		// TODO Auto-generated method stub
		return layoutRepo.resetUserMobileMenu(userId);
	}

}
