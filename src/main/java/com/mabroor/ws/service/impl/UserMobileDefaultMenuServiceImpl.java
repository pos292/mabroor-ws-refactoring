package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstUserMobileDefaultMenu;
import com.mabroor.ws.repository.UserMobileDefaultMenuRepository;
import com.mabroor.ws.service.UserMobileDefaultMenuService;

@Service
public class UserMobileDefaultMenuServiceImpl implements UserMobileDefaultMenuService  {
	
	@Autowired
	private UserMobileDefaultMenuRepository defaultRepo;

	@Override
	public Optional<MstUserMobileDefaultMenu> findById(Long id) {
		// TODO Auto-generated method stub
		return defaultRepo.findById(id);
	}

	@Override
	public List<MstUserMobileDefaultMenu> getList() {
		// TODO Auto-generated method stub
		return defaultRepo.getList();
	}

}
