package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstDoaCategory;
import com.mabroor.ws.repository.DoaCategoryRepository;
import com.mabroor.ws.service.DoaCategoryService;

@Service
public class DoaCategoryServiceImpl implements DoaCategoryService{
	
	@Autowired
	private DoaCategoryRepository doaCategory;

	@Override
	public List<MstDoaCategory> getList() {
		return doaCategory.getList();
	}
}
