package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mabroor.ws.dto.MstMabroor101WithHeaderDto;
import com.mabroor.ws.entity.MstMabroor101;
import com.mabroor.ws.repository.Mabroor101Repository;
import com.mabroor.ws.service.Mabroor101Service;


@Service
public class Mabroor101ServiceImpl implements  Mabroor101Service {
	

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Autowired
	private Mabroor101Repository mabroor101Repo;

	@Override
	public MstMabroor101WithHeaderDto findAllList() {
			
		MstMabroor101WithHeaderDto data = new MstMabroor101WithHeaderDto();

		/* ID 1 == BACKGROUND IMAGE */
		Optional<MstMabroor101> header = mabroor101Repo.findById((long) 1);
		if(header.isPresent()) {
			MstMabroor101 res = header.get();
			data.setBackgroundImage(this.imageBaseUrl + "mabroor101/banner/" + res.getBannerImage());
			data.setTitleHeader(res.getTitle());
			data.setSubTitleHeader(res.getDescription());
		}
	
		// LIST DATA
		List<MstMabroor101> list = mabroor101Repo.findAllList();
		if(!list.isEmpty()) {
			for(MstMabroor101 a : list) {
				a.setIconImage(this.imageBaseUrl + "mabroor101/icon/" + a.getIconImage());
				a.setBannerImage(this.imageBaseUrl + "mabroor101/banner/" + a.getBannerImage());
			}

			data.setList(list);
		}

		return data;
	}

	@Override
	public MstMabroor101 findById(Long listId) {

		MstMabroor101 data = null;
		Optional<MstMabroor101> list = mabroor101Repo.findById(listId);

		if(list.isPresent()) {
			data = list.get();
			data.setIconImage(this.imageBaseUrl + "mabroor101/icon/" + data.getIconImage());
			data.setBannerImage(this.imageBaseUrl + "mabroor101/banner/" + data.getBannerImage());
		}
		return data;
	}

	
}
