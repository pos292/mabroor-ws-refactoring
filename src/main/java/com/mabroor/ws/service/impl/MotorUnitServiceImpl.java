package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstMotorColor;
import com.mabroor.ws.entity.MstMotorUnit;
import com.mabroor.ws.repository.MotorColorRepository;
import com.mabroor.ws.repository.MotorUnitRepository;
import com.mabroor.ws.service.MotorUnitService;

@Service
public class MotorUnitServiceImpl implements MotorUnitService {

	@Autowired
	private MotorUnitRepository motorRepo;
	
	@Autowired
	private MotorColorRepository motorColorRepository;
	
	@Override
	public List<MstMotorUnit> findByListId(Long[] id) {
		// TODO Auto-generated method stub
		return motorRepo.findByListId(id);
	}

	@Override
	public List<MstMotorColor> findByMotorUnitId(Long id) {
		// TODO Auto-generated method stub
		return motorColorRepository.findByMotorUnitId(id);
	}


}
