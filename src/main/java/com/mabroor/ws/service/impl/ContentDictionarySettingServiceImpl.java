package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstContentDictionarySetting;
import com.mabroor.ws.entity.MstNews;
import com.mabroor.ws.repository.ContentDictionarySettingRepository;
import com.mabroor.ws.service.ContentDictionarySettingService;

@Service
public class ContentDictionarySettingServiceImpl implements ContentDictionarySettingService{

	@Autowired
	private ContentDictionarySettingRepository contentKamus;
	
	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Override
	public List<MstContentDictionarySetting> findByDictionaryId(Long id) {
		return contentKamus.findByDictionaryId(id);
	}

	@Override
	public List<MstContentDictionarySetting> getList() {
		List<MstContentDictionarySetting> list = contentKamus.getList();
		if(!list.isEmpty()) {
			for(MstContentDictionarySetting a : list) {
				a.getMstDictionary().setImage(this.imageBaseUrl + "dictionary/" + a.getMstDictionary().getImage());
			}
		}
		return contentKamus.getList();
	}
}
