package com.mabroor.ws.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.dto.CalculateMrpMotorReqDto;
import com.mabroor.ws.entity.MstDanaTunaiMotorMaster;
import com.mabroor.ws.repository.DanaTunaiMotorMasterRepository;
import com.mabroor.ws.service.DanaTunaiMotorMasterService;

@Service
public class DanaTunaiMotorMasterServiceImpl implements DanaTunaiMotorMasterService {

	@Autowired 
	private DanaTunaiMotorMasterRepository danaMotorRepo;
	
	@Override
	public List<MstDanaTunaiMotorMaster> findAll() {
		// TODO Auto-generated method stub
		return danaMotorRepo.findAll();
	}

	@Override
	public List<MstDanaTunaiMotorMaster> findByBrandMotor() {
		// TODO Auto-generated method stub
		return danaMotorRepo.findByBrandMotor();
	}

	@Override
	public List<MstDanaTunaiMotorMaster> findByTypeMotor(String brand) {
		// TODO Auto-generated method stub
		return danaMotorRepo.findByTypeMotor(brand);
	}

	@Override
	public List<MstDanaTunaiMotorMaster> findByModelMotor(String type) {
		// TODO Auto-generated method stub
		return danaMotorRepo.findByModelMotor(type);
	}

	@Override
	public List<MstDanaTunaiMotorMaster> findByYearMotor(String model) {
		// TODO Auto-generated method stub
		return danaMotorRepo.findByYearMotor(model);
	}

	@Override
	public Map<String, Object> findByCalculateMrp(CalculateMrpMotorReqDto calculateMrpMotor) {
		Map<String, Object> objtemp = new HashMap<>();
		Optional<MstDanaTunaiMotorMaster> calculateMrp = danaMotorRepo.findByCalculateMrp(calculateMrpMotor.getBrand(), calculateMrpMotor.getType(), calculateMrpMotor.getModel(), calculateMrpMotor.getYear());
		if(calculateMrp.isPresent()) {
			objtemp.put("mrp", calculateMrp.get().getPrice().multiply(new BigDecimal(0.75)).setScale(0, RoundingMode.HALF_UP));
		} else {
			objtemp.put("mrp", 0);
		}
		return objtemp;
	}

	@Override
	public List<MstDanaTunaiMotorMaster> findByYearAndModelTypeMotor(String brand, String type, String model) {
		// TODO Auto-generated method stub
		return danaMotorRepo.findByYearAndModelTypeMotor(brand, type, model);
	}

	@Override
	public List<MstDanaTunaiMotorMaster> findByBrandTypeModelMotor(String brand, String type) {
		return danaMotorRepo.findByBrandTypeModelMotor(brand, type);
	}

	@Override
	public List<MstDanaTunaiMotorMaster> findByTypeMotorDistinct() {
		return danaMotorRepo.findByTypeMotorDistinct();
	}

}
