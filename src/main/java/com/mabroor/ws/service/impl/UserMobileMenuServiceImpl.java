package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstUserMobileMenu;
import com.mabroor.ws.repository.UserMobileMenuRepository;
import com.mabroor.ws.service.UserMobileMenuService;


@Service
public class UserMobileMenuServiceImpl implements UserMobileMenuService {
	
	@Autowired
	private UserMobileMenuRepository menuRepo;

	@Override
	public List<MstUserMobileMenu> getListMenu(Long idMenuCategory) {
		return menuRepo.getListMenu(idMenuCategory);
	}

	@Override
	public List<MstUserMobileMenu> getFeaturedMenu() {
		return menuRepo.getFeaturedMenu();
	}

	@Override
	public List<MstUserMobileMenu> getDefaultMenu() {
		return menuRepo.getDefaultMenu();
	}

}
