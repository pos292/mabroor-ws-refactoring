package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstTentangKami;
import com.mabroor.ws.repository.TentangKamiRepository;
import com.mabroor.ws.service.TentangKamiService;

@Service
public class TentangKamiServiceImpl implements TentangKamiService {

	@Autowired
	private TentangKamiRepository tentangKamiRepository;
	
	@Override
	public Optional<MstTentangKami> findById(Long id) {
		// TODO Auto-generated method stub
		return tentangKamiRepository.findById(id);
	}

	@Override
	public List<MstTentangKami> getListTentangKami() {
		// TODO Auto-generated method stub
		return tentangKamiRepository.getListTentangKami();
	}

}
