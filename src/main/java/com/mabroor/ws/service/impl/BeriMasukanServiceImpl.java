package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstBeriMasukan;
import com.mabroor.ws.repository.BeriMasukanRepository;
import com.mabroor.ws.service.BeriMasukanService;

@Service
public class BeriMasukanServiceImpl implements BeriMasukanService {

	@Autowired
	private BeriMasukanRepository beriMasukanRepository;

	@Override
	public MstBeriMasukan save(MstBeriMasukan mstBeriMasukan) {
		// TODO Auto-generated method stub
		return beriMasukanRepository.save(mstBeriMasukan);
	}

	@Override
	public MstBeriMasukan update(MstBeriMasukan mstBeriMasukan) {
		// TODO Auto-generated method stub
		return beriMasukanRepository.save(mstBeriMasukan);
	}

	@Override
	public Optional<MstBeriMasukan> findById(Long id) {
		// TODO Auto-generated method stub
		return beriMasukanRepository.findById(id);
	}

	@Override
	public List<MstBeriMasukan> getListBeriMasukan() {
		// TODO Auto-generated method stub
		return beriMasukanRepository.getListBeriMasukan();
	}

}
