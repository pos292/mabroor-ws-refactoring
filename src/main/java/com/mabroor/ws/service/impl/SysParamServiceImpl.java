package com.mabroor.ws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.SysParam;
import com.mabroor.ws.repository.SysParamRepository;
import com.mabroor.ws.service.SysParamService;

@Service
public class SysParamServiceImpl implements SysParamService {
	@Autowired
	private SysParamRepository sysParamRepository;
	
	@Override
	public SysParam getByValueType(String valueType) {
		return sysParamRepository.findByValueType(valueType);
	}
}
