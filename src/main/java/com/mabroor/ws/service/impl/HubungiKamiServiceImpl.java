package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstHubungiKami;
import com.mabroor.ws.repository.HubungiKamiRepository;
import com.mabroor.ws.service.HubungiKamiService;

@Service
public class HubungiKamiServiceImpl implements HubungiKamiService {

	@Autowired
	private HubungiKamiRepository hubRepo;
	
	@Override
	public List<MstHubungiKami> getListHubungiKami() {
		// TODO Auto-generated method stub
		return hubRepo.getListHubungiKami();
	}

	@Override
	public MstHubungiKami save(MstHubungiKami mstHub) {
		// TODO Auto-generated method stub
		return hubRepo.save(mstHub);
	}

	@Override
	public MstHubungiKami update(MstHubungiKami mstHub) {
		// TODO Auto-generated method stub
		return hubRepo.save(mstHub);
	}

}
