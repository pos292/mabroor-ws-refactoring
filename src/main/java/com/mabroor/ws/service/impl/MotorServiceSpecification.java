package com.mabroor.ws.service.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.mabroor.ws.entity.MstMotor;
import com.mabroor.ws.entity.MstMotorUnit;

public class MotorServiceSpecification implements Specification<MstMotorUnit>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String propertyName;
	private String propertyCondition;
	private Object propertyValue;
	private List<Long> propertyValuesList;
	public MotorServiceSpecification (String name, String condition, Object value) {
		this.propertyName = name;
		this.propertyCondition = condition;
		this.propertyValue = value;
	}
	public MotorServiceSpecification(String name, List<Long> value) {
		this.propertyName = name;
		this.propertyValuesList = value;
	}
	@Override
	public Predicate toPredicate(Root<MstMotorUnit> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		// TODO Auto-generated method stub
		if(propertyValuesList != null && !propertyValuesList.isEmpty()) {
			return root.get(propertyName).in(propertyValuesList);
		}
		if("=".equals(propertyCondition)){
			return criteriaBuilder.equal(root.get(propertyName), propertyValue);
		}else if(">=".equals(propertyCondition)) {
			return criteriaBuilder.ge(root.get(propertyName),(BigDecimal) propertyValue);
		}else if("<=".equals(propertyCondition)) {
			return criteriaBuilder.le(root.get(propertyName),(BigDecimal) propertyValue);
		}
		return criteriaBuilder.and();
	}

}
