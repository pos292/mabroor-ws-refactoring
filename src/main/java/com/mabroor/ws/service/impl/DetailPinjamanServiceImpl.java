package com.mabroor.ws.service.impl;

import com.mabroor.ws.dto.RespDetailWilayahDto;
import com.mabroor.ws.entity.*;
import com.mabroor.ws.repository.*;
import com.mabroor.ws.service.DetailPinjamanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DetailPinjamanServiceImpl implements DetailPinjamanService {

    @Autowired
    TujuanPinjamanRepository tujuanPinjamanRepository;

    @Autowired
    BankPenerbitRepository bankPenerbitRepository;

    @Autowired
    PekerjaanRepository pekerjaanRepository;

    @Autowired
    TenorRepository tenorRepository;

    @Autowired
    PengajuanMultigunaTanpaJaminanRepository pengajuanMultigunaTanpaJaminanRepository;

    @Autowired
    DataDiriPengajuanMultigunaRepository dataDiriPengajuanMultigunaRepository;

    @Autowired
    MultigunaKotaRepository kotaRepository;

    @Autowired
    MultigunaKecamatanRepository kecamatanRepository;

    @Autowired
    MultigunaKelurahanRepository kelurahanRepository;

    @Autowired
    MultigunaKodePosRepository kodePosRepository;


    @Override
    public List<MstTujuanPinjaman> findall() {
        return tujuanPinjamanRepository.findAll();
    }

    @Override
    public List<MstBankPenerbit> findallbank() {
        return bankPenerbitRepository.findAll();
    }

    @Override
    public List<MstPekerjaan> findallpekerjaan() {
        return pekerjaanRepository.findAll();
    }

    @Override
    public List<MstTenor> findalltenor() {
        return tenorRepository.findAll();
    }

    @Override
    public List<MstPengajuanMultigunaTanpaJaminan> findallpengajuan() {
        return pengajuanMultigunaTanpaJaminanRepository.findAll();
    }

    @Override
    public MstPengajuanMultigunaTanpaJaminan savepengajuan(MstPengajuanMultigunaTanpaJaminan data) {
        return pengajuanMultigunaTanpaJaminanRepository.save(data);
    }

    @Override
    public MstDataDiriPengajuanMultiguna savedatadiri(MstDataDiriPengajuanMultiguna datas) {
        return dataDiriPengajuanMultigunaRepository.save(datas);
    }

    @Override
    public List<MstMultigunaKota> findAllKota() {
        return kotaRepository.findAll();
    }

    @Override
    public List<MstMultigunaKecamatan> findAllKecamatanbyidkota(Long id_kota) {
        return kecamatanRepository.findByIdKota(id_kota);
    }

    @Override
    public List<MstMultigunaKelurahan> findAllKelurahanbyidkecamatan(Long id_kecamatan) {
        return kelurahanRepository.findByIdKecamatan(id_kecamatan);
    }

    @Override
    public List<MstMultigunaKodePos> findAllKodePosbyidkelurahan(Long id_kelurahan) {
        return kodePosRepository.findByIdKelurahan(id_kelurahan);
    }
    @Override
    public MstPengajuanMultigunaTanpaJaminan findById(Long id) {
        return pengajuanMultigunaTanpaJaminanRepository.findById(id).get();
    }

    @Override
    public MstDataDiriPengajuanMultiguna findDataDiri(Long id) {
        return dataDiriPengajuanMultigunaRepository.findByIdPengajuan(id);
    }

    @Override
    public RespDetailWilayahDto findDetailWilayahByCode(String code) {
        RespDetailWilayahDto dto = new RespDetailWilayahDto();
        List<MstMultigunaKodePos> dataKodePost = kodePosRepository.findByCode(code);
        List<MstMultigunaKecamatan> dataKecamatanList = new ArrayList<>();
        List<MstMultigunaKota> dataKotaList = new ArrayList<>();
        if(!dataKodePost.isEmpty()){
            List<HashMap<String,String>> kelurahanList = new ArrayList<>();
            for(MstMultigunaKodePos data : dataKodePost){
                MstMultigunaKelurahan dataKelurahan = kelurahanRepository.findByIdKelurahan(data.getIdKelurahan());
                if(dataKelurahan != null){
                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", dataKelurahan.getName());
                    kelurahanList.add(map);
                    MstMultigunaKecamatan dataKecamatan = kecamatanRepository.findByIdKecamatan(dataKelurahan.getIdKecamatan());
                    dataKecamatanList.add(dataKecamatan);
                    if(dataKecamatan != null){
                        MstMultigunaKota dataKota = kotaRepository.findByIdKota(dataKecamatan.getIdKota());
                        dataKotaList.add(dataKota);
                    }
                }
            }
            dto.setKodePos(dataKodePost.get(0).getCode());
            dto.setKelurahan(kelurahanList);
            if(!(dataKecamatanList.isEmpty() && dataKotaList.isEmpty())){
                dto.setKecamatan(dataKecamatanList.get(0).getName());
                dto.setKota(dataKotaList.get(0).getName());
            }else {
                dto.setKecamatan("");
                dto.setKota("");
            }
            return dto;
        }else{
            return null;
        }
    }
}
