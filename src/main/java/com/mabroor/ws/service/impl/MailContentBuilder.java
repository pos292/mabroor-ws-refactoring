package com.mabroor.ws.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.mabroor.ws.entity.*;
import com.mabroor.ws.util.IdrFormatMoney;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {
	private TemplateEngine templateEngine;
	
	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Autowired
	public MailContentBuilder(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	public String build(String type, String name, String message) {
		Context context = new Context();
		context.setVariable("type", type);
		context.setVariable("name", name);
		context.setVariable("message", message);
		return templateEngine.process("mailTemplate", context);
	}

	public String build(String type, String name, String message, String template) {
		Context context = new Context();
		context.setVariable("type", type);
		context.setVariable("name", name);
		context.setVariable("message", message);
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanMobilUserMobile(MstPengajuanCar mstPengajuanCar, String template) {
		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Context context = new Context();
		context.setVariable("name", mstPengajuanCar.getNameDataUser());
		context.setVariable("id", mstPengajuanCar.getId());
		context.setVariable("carTitle", mstPengajuanCar.getType());
		context.setVariable("kotaPengajuan", mstPengajuanCar.getLocation());
		if(new BigDecimal(mstPengajuanCar.getAmountDp()).compareTo(new BigDecimal(100))<= 0) {
		context.setVariable("dp", mstPengajuanCar.getDp() + "%");
		}else {
			context.setVariable("dp", fmt.format(new BigDecimal(mstPengajuanCar.getAmountDp())).replace(",00", ""));	
		}		
		if("ACC".equalsIgnoreCase(mstPengajuanCar.getCompanyName())) {
			context.setVariable("company", "Astra Credit Company (ACC)");
		} else if("TAF".equalsIgnoreCase(mstPengajuanCar.getCompanyName())) {
			context.setVariable("company", "Toyota Astra Finance (TAF)");
		} 		
		
		
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanMobilUserLeads(MstPengajuanCar mstPengajuanCar, String template) {
		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Context context = new Context();
		context.setVariable("name", mstPengajuanCar.getNameDataUser());
		context.setVariable("id", mstPengajuanCar.getId());
		context.setVariable("carTitle", mstPengajuanCar.getType());
		context.setVariable("kotaPengajuan", mstPengajuanCar.getLocation());
		if(new BigDecimal(mstPengajuanCar.getAmountDp()).compareTo(new BigDecimal(100))<= 0) {
			context.setVariable("dp", mstPengajuanCar.getDp() + "%");
			}else {
				context.setVariable("dp", fmt.format(new BigDecimal(mstPengajuanCar.getAmountDp())).replace(",00", ""));	
			}		
		if("ACC".equalsIgnoreCase(mstPengajuanCar.getCompanyName())) {
			context.setVariable("company", "Astra Credit Company (ACC)");
		} else if("TAF".equalsIgnoreCase(mstPengajuanCar.getCompanyName())) {
			context.setVariable("company", "Toyota Astra Finance (TAF)");
		} 		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanMotorUserMobile(MstPengajuanMotor mstPengajuanMotor, String template) {
		Context context = new Context();
		context.setVariable("name", mstPengajuanMotor.getNameDataUser());
		context.setVariable("id", mstPengajuanMotor.getId());
		context.setVariable("jenisMotor", mstPengajuanMotor.getVehicleType());
		context.setVariable("dp", mstPengajuanMotor.getDp());
		context.setVariable("tenor", mstPengajuanMotor.getTenor());
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanMotorUserLeads(MstPengajuanMotor mstPengajuanMotor, String template) {
		Context context = new Context();
		context.setVariable("name", mstPengajuanMotor.getNameDataUser());
		context.setVariable("id", mstPengajuanMotor.getId());
		context.setVariable("jenisMotor", mstPengajuanMotor.getVehicleType());
		context.setVariable("dp", mstPengajuanMotor.getDp());
		context.setVariable("tenor", mstPengajuanMotor.getTenor());
		return templateEngine.process(template, context);
	}
	
//	public String buildPengajuanCarBaruUserMobile(MstPengajuanCar mstPengajuanCar, String template) {
//		Context context = new Context();
//		context.setVariable("name", mstPengajuanCar.getNameDataUser());
//		context.setVariable("id", mstPengajuanCar.getId());
//		context.setVariable("carImage", "https://fh.astrafinancial.co.id/email-assets/mobilbekas.png");
//		context.setVariable("carTitle", mstPengajuanCar.getDetailKendaraan());
//		context.setVariable("otrPrice", mstPengajuanCar.getOtrPrice());
//		context.setVariable("downPaymentAmount", mstPengajuanCar.getAmountDp());
//		context.setVariable("tenor", mstPengajuanCar.getTenor());
//		context.setVariable("company", "Astra Credit Company (ACC)");
//		context.setVariable("link", "https://www.acc.co.id/news/read/paket-serbu-acc-tawarkan-kredit-mobil-baru-dengan-bonus-melimpah");	
//		return templateEngine.process(template, context);
//	}
//	
//	public String buildPengajuanCarBaruUserLeads(MstPengajuanCar mstPengajuanCar, String template) {
//		Context context = new Context();
//		context.setVariable("name", mstPengajuanCar.getNameDataUser());
//		context.setVariable("id", mstPengajuanCar.getId());
//		context.setVariable("carImage", "https://fh.astrafinancial.co.id/email-assets/mobilbekas.png");
//		context.setVariable("carTitle", mstPengajuanCar.getDetailKendaraan());
//		context.setVariable("otrPrice", mstPengajuanCar.getOtrPrice());
//		context.setVariable("downPaymentAmount", mstPengajuanCar.getAmountDp());
//		context.setVariable("tenor", mstPengajuanCar.getTenor());
//		context.setVariable("company", "Astra Credit Company (ACC)");
//		context.setVariable("link", "https://www.acc.co.id/news/read/paket-serbu-acc-tawarkan-kredit-mobil-baru-dengan-bonus-melimpah");	
//		return templateEngine.process(template, context);
//	}
	
	public String buildPengajuanMotorBaruUserMobile(MstPengajuanMotor mstPengajuanMotor, String template) {
		Context context = new Context();
		context.setVariable("name", mstPengajuanMotor.getNameDataUser());
		context.setVariable("id", mstPengajuanMotor.getId());
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanMotorBaruUserLeads(MstPengajuanMotor mstPengajuanMotor, String template) {
		Context context = new Context();
		context.setVariable("name", mstPengajuanMotor.getNameDataUser());
		context.setVariable("id", mstPengajuanMotor.getId());
		return templateEngine.process(template, context);
	}
	
	public String buildDisableUserMobile(MstUserMobile mstUserMobile, String template) {
		Context context = new Context();
		context.setVariable("name", mstUserMobile.getName());
		return templateEngine.process(template, context);
	}
	
	public String buildDisableUserLeads(MstUserMobile mstUserMobile, String template) {
		Context context = new Context();
		context.setVariable("name", mstUserMobile.getName());
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanHajiUserMobile(MstWisataReligi mstUmroh, String template) {
		Context context = new Context();
		context.setVariable("name", mstUmroh.getNamaDepan() + " " + mstUmroh.getNamaBelakang());
		context.setVariable("id", mstUmroh.getId());
		context.setVariable("jenisPerjalanan", mstUmroh.getProductChoose());
		context.setVariable("biaya", mstUmroh.getProductPurchase());
		context.setVariable("partner", mstUmroh.getPartnerPembiayaan());
		context.setVariable("dp", mstUmroh.getAmountDp());
		context.setVariable("peserta", mstUmroh.getJumlahPeserta());
		context.setVariable("tenor", mstUmroh.getTenor());
		// context.setVariable("link", "https://www.fifgroup.co.id/amitra/cara-praktis-daftar-umroh-lewat-amitra");
		
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanHajiUserLeads(MstWisataReligi mstUmroh, String template) {
		Context context = new Context();
		context.setVariable("name", mstUmroh.getNamaDepan() + " " + mstUmroh.getNamaBelakang());
		context.setVariable("id", mstUmroh.getId());
		context.setVariable("jenisPerjalanan", mstUmroh.getProductChoose());
		context.setVariable("biaya", mstUmroh.getProductPurchase());
		context.setVariable("partner", mstUmroh.getPartnerPembiayaan());
		context.setVariable("dp", mstUmroh.getAmountDp());
		context.setVariable("peserta", mstUmroh.getJumlahPeserta());
		context.setVariable("tenor", mstUmroh.getTenor());
		// context.setVariable("link", "https://www.fifgroup.co.id/amitra/cara-praktis-daftar-umroh-lewat-amitra");
		
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanHajiAmitraUserMobile(MstWisataReligi mstUmroh, String template) {
		Context context = new Context();
		context.setVariable("name", mstUmroh.getNameDataUser());
		context.setVariable("id", mstUmroh.getId());
		context.setVariable("produkBiaya", mstUmroh.getProductChoose());
//		Locale locale = new Locale("id", "ID");
//		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",locale);
//		Date date = new Date();
//		try {
//			 date = format.parse(mstUmroh.getDateDispatch());
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy",locale);
//		String formatDispatch = formatter.format(date);
//		context.setVariable("tanggalKeberangkatan", formatDispatch);
		context.setVariable("biaya", mstUmroh.getProductPurchase());
		context.setVariable("partner", mstUmroh.getPartnerPembiayaan());
		context.setVariable("dp", mstUmroh.getAmountDp());
		context.setVariable("tenor", mstUmroh.getTenor());
		context.setVariable("link", "https://www.fifgroup.co.id/amitra/cara-praktis-daftar-umroh-lewat-amitra");
		
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanHajiAmitraUserLeads(MstWisataReligi mstUmroh, String template) {
		Context context = new Context();
		context.setVariable("name", mstUmroh.getNameDataUser());
		context.setVariable("id", mstUmroh.getId());
		context.setVariable("produkBiaya", mstUmroh.getProductPurchase());
//		Locale locale = new Locale("id", "ID");
//		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",locale);
//		Date date = new Date();
//		try {
//			 date = format.parse(mstUmroh.getDateDispatch());
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy",locale);
//		String formatDispatch = formatter.format(date);
//		context.setVariable("tanggalKeberangkatan", formatDispatch);
		context.setVariable("biaya", mstUmroh.getProductPurchase());
		context.setVariable("partner", mstUmroh.getPartnerPembiayaan());
		context.setVariable("dp", mstUmroh.getAmountDp());
		context.setVariable("tenor", mstUmroh.getTenor());
		context.setVariable("link", "https://www.fifgroup.co.id/amitra/cara-praktis-daftar-umroh-lewat-amitra");
		
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanUmrohUserMobile(MstWisataReligi mstUmroh, String template) {
		Context context = new Context();
		context.setVariable("name", mstUmroh.getNameDataUser());
		context.setVariable("id", mstUmroh.getId());
		context.setVariable("jenisPerjalanan", mstUmroh.getProductChoose());
		Locale locale = new Locale("id", "ID");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",locale);
		Date date = new Date();
		try {
			 date = format.parse(mstUmroh.getDateDispatch());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy",locale);
		String formatDispatch = formatter.format(date);
		context.setVariable("tanggalKeberangkatan", formatDispatch);
		context.setVariable("tenor", mstUmroh.getTenor());
		context.setVariable("link", "https://www.fifgroup.co.id/amitra/cara-praktis-daftar-umroh-lewat-amitra");
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanUmrohUserLeads(MstWisataReligi mstUmroh, String template) {
		Context context = new Context();
		context.setVariable("name", mstUmroh.getNameDataUser());
		context.setVariable("id", mstUmroh.getId());
		context.setVariable("jenisPerjalanan", mstUmroh.getProductChoose());
		Locale locale = new Locale("id", "ID");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",locale);
		Date date = new Date();
		try {
			 date = format.parse(mstUmroh.getDateDispatch());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy",locale);
		String formatDispatch = formatter.format(date);
		context.setVariable("tanggalKeberangkatan", formatDispatch);
		context.setVariable("tenor", mstUmroh.getTenor());
		context.setVariable("link", "https://www.fifgroup.co.id/amitra/cara-praktis-daftar-umroh-lewat-amitra");
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanMultiGunaJaminanUserMobile(MstPengajuanJaminan mstJaminan, String template) {
		Context context = new Context();
		context.setVariable("name", mstJaminan.getNameDataUser());
		context.setVariable("id", mstJaminan.getId());
		context.setVariable("jenisMultiGuna1", "Pembiayaan Multiguna Dengan Jaminan");
		context.setVariable("jenisMultiGuna2", "Dengan Jaminan");
		context.setVariable("nominalPinjaman", mstJaminan.getJumlahPinjaman());
		context.setVariable("jenisJaminan", mstJaminan.getJenisJaminan());
		context.setVariable("kebutuhanPinjaman", mstJaminan.getKebutuhanPinjaman());
		context.setVariable("tenor", mstJaminan.getTenor());
		
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanMultiGunaJaminanUserLeads(MstPengajuanJaminan mstJaminan, String template) {
		Context context = new Context();
		context.setVariable("name", mstJaminan.getNameDataUser());
		context.setVariable("id", mstJaminan.getId());
		context.setVariable("jenisMultiGuna1", "Pembiayaan Multiguna Dengan Jaminan");
		context.setVariable("jenisMultiGuna2", "Dengan Jaminan");
		context.setVariable("nominalPinjaman", mstJaminan.getJumlahPinjaman());
		context.setVariable("jenisJaminan", mstJaminan.getJenisJaminan());
		context.setVariable("kebutuhanPinjaman", mstJaminan.getKebutuhanPinjaman());
		context.setVariable("tenor", mstJaminan.getTenor());
	
		
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanAsuransiMobilUserMobile(MstPengajuanAsuransiMobil mstAsuransiMobil, String template) {
		Context context = new Context();
		context.setVariable("name", mstAsuransiMobil.getNameDataUser());
		context.setVariable("id", mstAsuransiMobil.getId());
		context.setVariable("jenisAsuransi1", " mobil");
		context.setVariable("jenisAsuransi2", " Mobil");
		context.setVariable("jenisAsuransi", mstAsuransiMobil.getJenisAsuransi());
		context.setVariable("kendaraan", mstAsuransiMobil.getKendaraan());
		context.setVariable("kontribusidasar", mstAsuransiMobil.getKontribusiDasar());
		context.setVariable("estimasi", mstAsuransiMobil.getEstimasiTotalKontribusi());
		context.setVariable("link", "https://www.gardaoto.com/blog");
		
		
		return templateEngine.process(template, context);
	}
	
	public String buildPengajuanAsuransiMobilUserLeads(MstPengajuanAsuransiMobil mstAsuransiMobil, String template) {
		Context context = new Context();
		context.setVariable("name", mstAsuransiMobil.getNameDataUser());
		context.setVariable("id", mstAsuransiMobil.getId());
		context.setVariable("jenisAsuransi1", " mobil");
		context.setVariable("jenisAsuransi2", " Mobil");
		context.setVariable("jenisAsuransi", mstAsuransiMobil.getJenisAsuransi());		
		context.setVariable("kendaraan", mstAsuransiMobil.getKendaraan());
		context.setVariable("kontribusidasar", mstAsuransiMobil.getKontribusiDasar());
		context.setVariable("estimasi", mstAsuransiMobil.getEstimasiTotalKontribusi());
		context.setVariable("link", "https://www.gardaoto.com/blog");
		
		
		return templateEngine.process(template, context);
	}

	public String buildPengajuanMultigunaTanpaJaminan(MstPengajuanMultigunaTanpaJaminan mstPengajuanMultigunaTanpaJaminan,String name, String template){
		Context context = new Context();
		Locale locale = new Locale("id", "ID");
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
		numberFormat.setMaximumFractionDigits(0);
		String result = numberFormat.format(mstPengajuanMultigunaTanpaJaminan.getJmlPinjaman());
		context.setVariable("name",name);
		context.setVariable("idPengajuan", mstPengajuanMultigunaTanpaJaminan.getId());
		context.setVariable("produkPembiayaan", "Pembiayaan Multiguna Tanpa Jaminan");
		context.setVariable("jumlahPembiayaan", result);
		context.setVariable("tenor", String.valueOf(mstPengajuanMultigunaTanpaJaminan.getTenor())+ " Bulan");

		return templateEngine.process(template, context);
	}

	public String buildPengajuanUmroh(MstPengajuanUmroh pengajuanUmroh, String name, String template){
		Context context = new Context();
		String header = "";
		boolean footerMsg = true;
		if (pengajuanUmroh.getStatus().toUpperCase(Locale.ROOT).equalsIgnoreCase("SUBMITTED")){
			header = "Yay! Pengajuan Pembiayaan Umroh kamu sudah terkirim";
		} else if (pengajuanUmroh.getStatus().toUpperCase(Locale.ROOT).equalsIgnoreCase("APPROVED")){
			header = "Selamat! Pengajuan Pembiayaan Umroh kamu berhasil";
		} else if (pengajuanUmroh.getStatus().toUpperCase(Locale.ROOT).equalsIgnoreCase("REJECTED")){
			header = "Maaf, pengajuan Pembiayaan Umroh kamu ditolak oleh Amitra";
			footerMsg = false;
		}
		context.setVariable("name", name);
		context.setVariable("idPengajuan", pengajuanUmroh.getId());
		context.setVariable("productPembiayaan", "Pembiayaan Umroh");
		context.setVariable("DP", IdrFormatMoney.currencyIdr(String.valueOf(pengajuanUmroh.getDp())));
		context.setVariable("Tenor", String.valueOf(pengajuanUmroh.getTenor()));
		context.setVariable("header", header);
		context.setVariable("footerShow", footerMsg);

		return templateEngine.process(template, context);
	}
}
