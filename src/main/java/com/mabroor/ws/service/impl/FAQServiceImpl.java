package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstFAQ;
import com.mabroor.ws.repository.FAQRepository;
import com.mabroor.ws.service.FAQService;

@Service
public class FAQServiceImpl implements FAQService{

	@Autowired
	private FAQRepository faqRepository;
	
	@Override
	public List<MstFAQ> findAllByStatus() {
		return faqRepository.findAllByStatus();
	}

	@Override
	public List<MstFAQ> findByMainProduct(Long idMainProduct) {
		return faqRepository.findByMainProduct(idMainProduct);
	}

	@Override
	public List<MstFAQ> findByMainProductLandingPage(Long idMainProduct) {
		return faqRepository.findByMainProductLandingPage(idMainProduct);
	}

}
