package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPengajuanAsuransiMobil;
import com.mabroor.ws.repository.PengajuanAsuransiMobilRepository;
import com.mabroor.ws.service.PengajuanAsuransiMobilService;

@Service
public class PengajuanAsuransiMobilServiceImpl implements PengajuanAsuransiMobilService {

	@Autowired
	private PengajuanAsuransiMobilRepository mobilRepo;
	
	@Override
	public List<MstPengajuanAsuransiMobil> getListAsuransiMobil() {
		// TODO Auto-generated method stub
		return mobilRepo.getListAsuransiMobil();
	}

	@Override
	public Optional<MstPengajuanAsuransiMobil> findById(Long id) {
		// TODO Auto-generated method stub
		return mobilRepo.findById(id);
	}

	@Override
	public MstPengajuanAsuransiMobil save(MstPengajuanAsuransiMobil mstMobil) {
		// TODO Auto-generated method stub
		return mobilRepo.save(mstMobil);
	}

	@Override
	public MstPengajuanAsuransiMobil update(MstPengajuanAsuransiMobil mstMobil) {
		// TODO Auto-generated method stub
		return mobilRepo.save(mstMobil);
	}

	@Override
	public List<MstPengajuanAsuransiMobil> findAllByUserId(Long id) {
		// TODO Auto-generated method stub
		return mobilRepo.findAllByUserId(id);
	}

	

}
