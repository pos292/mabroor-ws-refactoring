package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPromotionBundlePopup;
import com.mabroor.ws.repository.PromotionBundlePopupRepository;
import com.mabroor.ws.service.PromotionBundlePopupService;

@Service
public class PromotionBundlePopupServiceImpl implements PromotionBundlePopupService {

	@Autowired
	private PromotionBundlePopupRepository bundleRepository;

	@Override
	public List<MstPromotionBundlePopup> findAllByStatus() {
		// TODO Auto-generated method stub
		return bundleRepository.findAllByStatus();
	}
	
}
