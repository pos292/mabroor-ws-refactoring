package com.mabroor.ws.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mabroor.ws.dto.DictionaryDto;
import com.mabroor.ws.dto.DictionaryReqDto;
import com.mabroor.ws.entity.MstDictionary;
import com.mabroor.ws.repository.DictionaryRepository;
import com.mabroor.ws.service.DictionaryService;

@Service
public class DictionaryServiceImpl implements DictionaryService{

	@Autowired
	private DictionaryRepository dictServ;
	
	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Override
	public DictionaryDto findAllByAbjad() {
		DictionaryDto dtoList = new DictionaryDto();
		List <DictionaryReqDto> dictListDtoA = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoB = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoC = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoD = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoE = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoF = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoG = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoH = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoI = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoJ = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoK = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoL = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoM = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoN = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoO = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoP = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoQ = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoR = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoS = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoT = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoU = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoV = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoW = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoX = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoY = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoZ = new ArrayList<DictionaryReqDto>();

		List<MstDictionary> dictList =  dictServ.findAllByAbjad();
		for(MstDictionary dict : dictList) {

			if(dict.getTitle()!=null && !"".equals(dict.getTitle())) {
				DictionaryReqDto dtoA = new DictionaryReqDto();
				if("A".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoA.setId(dict.getId());
					dtoA.setTitle(dict.getTitle());
					dtoA.setDescription(dict.getDescription());
					dtoA.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoA.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoA.add(dtoA);
					System.out.println(dictListDtoA);
				}

				DictionaryReqDto dtoB = new DictionaryReqDto();
				if("B".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoB.setId(dict.getId());
					dtoB.setTitle(dict.getTitle());
					dtoB.setDescription(dict.getDescription());
					dtoB.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoB.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoB.add(dtoB);
				}

				DictionaryReqDto dtoC = new DictionaryReqDto();
				if("C".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoC.setId(dict.getId());
					dtoC.setTitle(dict.getTitle());
					dtoC.setDescription(dict.getDescription());
					dtoC.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoC.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoC.add(dtoC);
				}

				DictionaryReqDto dtoD = new DictionaryReqDto();
				if("D".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoD.setId(dict.getId());
					dtoD.setTitle(dict.getTitle());
					dtoD.setDescription(dict.getDescription());
					dtoD.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoD.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoD.add(dtoD);
				}

				DictionaryReqDto dtoE = new DictionaryReqDto();
				if("E".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoE.setId(dict.getId());
					dtoE.setTitle(dict.getTitle());
					dtoE.setDescription(dict.getDescription());
					dtoE.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoE.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoE.add(dtoE);
				}

				DictionaryReqDto dtoF = new DictionaryReqDto();
				if("F".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoF.setId(dict.getId());
					dtoF.setTitle(dict.getTitle());
					dtoF.setDescription(dict.getDescription());
					dtoF.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoF.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoF.add(dtoF);
				}

				DictionaryReqDto dtoG = new DictionaryReqDto();
				if("G".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoG.setId(dict.getId());
					dtoG.setTitle(dict.getTitle());
					dtoG.setDescription(dict.getDescription());
					dtoG.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoG.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoG.add(dtoG);
				}

				DictionaryReqDto dtoH = new DictionaryReqDto();
				if("H".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoH.setId(dict.getId());
					dtoH.setTitle(dict.getTitle());
					dtoH.setDescription(dict.getDescription());
					dtoH.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoH.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoH.add(dtoH);
				}

				DictionaryReqDto dtoI = new DictionaryReqDto();
				if("I".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoI.setId(dict.getId());
					dtoI.setTitle(dict.getTitle());
					dtoI.setDescription(dict.getDescription());
					dtoI.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoI.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoI.add(dtoI);
				}

				DictionaryReqDto dtoJ = new DictionaryReqDto();
				if("J".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoJ.setId(dict.getId());
					dtoJ.setTitle(dict.getTitle());
					dtoJ.setDescription(dict.getDescription());
					dtoJ.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoJ.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoJ.add(dtoJ);
				}

				DictionaryReqDto dtoK = new DictionaryReqDto();
				if("K".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoK.setId(dict.getId());
					dtoK.setTitle(dict.getTitle());
					dtoK.setDescription(dict.getDescription());
					dtoK.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoK.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoK.add(dtoK);
				}

				DictionaryReqDto dtoL = new DictionaryReqDto();
				if("L".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoL.setId(dict.getId());
					dtoL.setTitle(dict.getTitle());
					dtoL.setDescription(dict.getDescription());
					dtoL.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoL.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoL.add(dtoL);
				}

				DictionaryReqDto dtoM = new DictionaryReqDto();
				if("M".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoM.setId(dict.getId());
					dtoM.setTitle(dict.getTitle());
					dtoM.setDescription(dict.getDescription());
					dtoM.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoM.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoM.add(dtoM);
				}

				DictionaryReqDto dtoN = new DictionaryReqDto();
				if("N".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoN.setId(dict.getId());
					dtoN.setTitle(dict.getTitle());
					dtoN.setDescription(dict.getDescription());
					dtoN.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoN.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoN.add(dtoN);
				}

				DictionaryReqDto dtoO = new DictionaryReqDto();
				if("O".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoO.setId(dict.getId());
					dtoO.setTitle(dict.getTitle());
					dtoO.setDescription(dict.getDescription());
					dtoO.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoO.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoO.add(dtoO);
				}

				DictionaryReqDto dtoP = new DictionaryReqDto();
				if("P".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoP.setId(dict.getId());
					dtoP.setTitle(dict.getTitle());
					dtoP.setDescription(dict.getDescription());
					dtoP.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoP.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoP.add(dtoP);
				}

				DictionaryReqDto dtoQ = new DictionaryReqDto();
				if("Q".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoQ.setId(dict.getId());
					dtoQ.setTitle(dict.getTitle());
					dtoQ.setDescription(dict.getDescription());
					dtoQ.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoQ.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoQ.add(dtoQ);
				}

				DictionaryReqDto dtoR = new DictionaryReqDto();
				if("R".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoR.setId(dict.getId());
					dtoR.setTitle(dict.getTitle());
					dtoR.setDescription(dict.getDescription());
					dtoR.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoR.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoR.add(dtoR);
				}

				DictionaryReqDto dtoS = new DictionaryReqDto();
				if("S".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoS.setId(dict.getId());
					dtoS.setTitle(dict.getTitle());
					dtoS.setDescription(dict.getDescription());
					dtoS.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoS.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoS.add(dtoS);
				}

				DictionaryReqDto dtoT = new DictionaryReqDto();
				if("T".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoT.setId(dict.getId());
					dtoT.setTitle(dict.getTitle());
					dtoT.setDescription(dict.getDescription());
					dtoT.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoT.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoT.add(dtoT);
				}

				DictionaryReqDto dtoU = new DictionaryReqDto();
				if("U".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoU.setId(dict.getId());
					dtoU.setTitle(dict.getTitle());
					dtoU.setDescription(dict.getDescription());
					dtoU.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoU.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoU.add(dtoU);
				}

				DictionaryReqDto dtoV = new DictionaryReqDto();
				if("V".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoV.setId(dict.getId());
					dtoV.setTitle(dict.getTitle());
					dtoV.setDescription(dict.getDescription());
					dtoV.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoV.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoV.add(dtoV);
				}

				DictionaryReqDto dtoW = new DictionaryReqDto();
				if("W".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoW.setId(dict.getId());
					dtoW.setTitle(dict.getTitle());
					dtoW.setDescription(dict.getDescription());
					dtoW.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoW.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoW.add(dtoW);
				}

				DictionaryReqDto dtoX = new DictionaryReqDto();
				if("X".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoX.setId(dict.getId());
					dtoX.setTitle(dict.getTitle());
					dtoX.setDescription(dict.getDescription());
					dtoX.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoX.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoX.add(dtoX);
				}

				DictionaryReqDto dtoY = new DictionaryReqDto();
				if("Y".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoY.setId(dict.getId());
					dtoY.setTitle(dict.getTitle());
					dtoY.setDescription(dict.getDescription());
					dtoY.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoY.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoY.add(dtoY);
				}

				DictionaryReqDto dtoZ = new DictionaryReqDto();
				if("Z".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoZ.setId(dict.getId());
					dtoZ.setTitle(dict.getTitle());
					dtoZ.setDescription(dict.getDescription());
					dtoZ.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoZ.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoZ.add(dtoZ);
				}
			}
		}
		dtoList.setA(dictListDtoA);
		dtoList.setB(dictListDtoB);
		dtoList.setC(dictListDtoC);
		dtoList.setD(dictListDtoD);
		dtoList.setE(dictListDtoE);
		dtoList.setF(dictListDtoF);
		dtoList.setG(dictListDtoG);
		dtoList.setH(dictListDtoH);
		dtoList.setI(dictListDtoI);
		dtoList.setJ(dictListDtoJ);
		dtoList.setK(dictListDtoK);
		dtoList.setL(dictListDtoL);
		dtoList.setM(dictListDtoM);
		dtoList.setN(dictListDtoN);
		dtoList.setO(dictListDtoO);
		dtoList.setP(dictListDtoP);
		dtoList.setQ(dictListDtoQ);
		dtoList.setR(dictListDtoR);
		dtoList.setS(dictListDtoS);
		dtoList.setT(dictListDtoT);
		dtoList.setU(dictListDtoU);
		dtoList.setV(dictListDtoV);
		dtoList.setW(dictListDtoW);
		dtoList.setX(dictListDtoX);
		dtoList.setY(dictListDtoY);
		dtoList.setZ(dictListDtoZ);

		return dtoList;
	}

	@Override
	public DictionaryDto findAllBySearchAbjad(String title) {
		DictionaryDto dtoList = new DictionaryDto();
		List <DictionaryReqDto> dictListDtoA = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoB = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoC = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoD = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoE = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoF = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoG = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoH = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoI = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoJ = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoK = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoL = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoM = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoN = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoO = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoP = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoQ = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoR = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoS = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoT = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoU = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoV = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoW = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoX = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoY = new ArrayList<DictionaryReqDto>();
		List <DictionaryReqDto> dictListDtoZ = new ArrayList<DictionaryReqDto>();

		List<MstDictionary> dictList =  dictServ.findAllBySearchAbjad("%"+title.toLowerCase()+"%");

		for(MstDictionary dict : dictList) {

			if(dict.getTitle()!=null && !"".equals(dict.getTitle())) {
				DictionaryReqDto dtoA = new DictionaryReqDto();
				if("A".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoA.setId(dict.getId());
					dtoA.setTitle(dict.getTitle());
					dtoA.setDescription(dict.getDescription());
					dtoA.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoA.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoA.add(dtoA);
					System.out.println(dictListDtoA);
				}

				DictionaryReqDto dtoB = new DictionaryReqDto();
				if("B".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoB.setId(dict.getId());
					dtoB.setTitle(dict.getTitle());
					dtoB.setDescription(dict.getDescription());
					dtoB.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoB.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoB.add(dtoB);
				}

				DictionaryReqDto dtoC = new DictionaryReqDto();
				if("C".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoC.setId(dict.getId());
					dtoC.setTitle(dict.getTitle());
					dtoC.setDescription(dict.getDescription());
					dtoC.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoC.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoC.add(dtoC);
				}

				DictionaryReqDto dtoD = new DictionaryReqDto();
				if("D".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoD.setId(dict.getId());
					dtoD.setTitle(dict.getTitle());
					dtoD.setDescription(dict.getDescription());
					dtoD.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoD.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoD.add(dtoD);
				}

				DictionaryReqDto dtoE = new DictionaryReqDto();
				if("E".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoE.setId(dict.getId());
					dtoE.setTitle(dict.getTitle());
					dtoE.setDescription(dict.getDescription());
					dtoE.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoE.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoE.add(dtoE);
				}

				DictionaryReqDto dtoF = new DictionaryReqDto();
				if("F".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoF.setId(dict.getId());
					dtoF.setTitle(dict.getTitle());
					dtoF.setDescription(dict.getDescription());
					dtoF.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoF.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoF.add(dtoF);
				}

				DictionaryReqDto dtoG = new DictionaryReqDto();
				if("G".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoG.setId(dict.getId());
					dtoG.setTitle(dict.getTitle());
					dtoG.setDescription(dict.getDescription());
					dtoG.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoG.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoG.add(dtoG);
				}

				DictionaryReqDto dtoH = new DictionaryReqDto();
				if("H".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoH.setId(dict.getId());
					dtoH.setTitle(dict.getTitle());
					dtoH.setDescription(dict.getDescription());
					dtoH.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoH.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoH.add(dtoH);
				}

				DictionaryReqDto dtoI = new DictionaryReqDto();
				if("I".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoI.setId(dict.getId());
					dtoI.setTitle(dict.getTitle());
					dtoI.setDescription(dict.getDescription());
					dtoI.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoI.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoI.add(dtoI);
				}

				DictionaryReqDto dtoJ = new DictionaryReqDto();
				if("J".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoJ.setId(dict.getId());
					dtoJ.setTitle(dict.getTitle());
					dtoJ.setDescription(dict.getDescription());
					dtoJ.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoJ.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoJ.add(dtoJ);
				}

				DictionaryReqDto dtoK = new DictionaryReqDto();
				if("K".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoK.setId(dict.getId());
					dtoK.setTitle(dict.getTitle());
					dtoK.setDescription(dict.getDescription());
					dtoK.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoK.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoK.add(dtoK);
				}

				DictionaryReqDto dtoL = new DictionaryReqDto();
				if("L".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoL.setId(dict.getId());
					dtoL.setTitle(dict.getTitle());
					dtoL.setDescription(dict.getDescription());
					dtoL.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoL.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoL.add(dtoL);
				}

				DictionaryReqDto dtoM = new DictionaryReqDto();
				if("M".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoM.setId(dict.getId());
					dtoM.setTitle(dict.getTitle());
					dtoM.setDescription(dict.getDescription());
					dtoM.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoM.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoM.add(dtoM);
				}

				DictionaryReqDto dtoN = new DictionaryReqDto();
				if("N".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoN.setId(dict.getId());
					dtoN.setTitle(dict.getTitle());
					dtoN.setDescription(dict.getDescription());
					dtoN.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoN.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoA.add(dtoN);
				}

				DictionaryReqDto dtoO = new DictionaryReqDto();
				if("O".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoO.setId(dict.getId());
					dtoO.setTitle(dict.getTitle());
					dtoO.setDescription(dict.getDescription());
					dtoO.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoO.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoO.add(dtoO);
				}

				DictionaryReqDto dtoP = new DictionaryReqDto();
				if("P".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoP.setId(dict.getId());
					dtoP.setTitle(dict.getTitle());
					dtoP.setDescription(dict.getDescription());
					dtoP.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoP.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoA.add(dtoP);
				}

				DictionaryReqDto dtoQ = new DictionaryReqDto();
				if("Q".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoQ.setId(dict.getId());
					dtoQ.setTitle(dict.getTitle());
					dtoQ.setDescription(dict.getDescription());
					dtoQ.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoQ.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoQ.add(dtoQ);
				}

				DictionaryReqDto dtoR = new DictionaryReqDto();
				if("R".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoR.setId(dict.getId());
					dtoR.setTitle(dict.getTitle());
					dtoR.setDescription(dict.getDescription());
					dtoR.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoR.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoR.add(dtoR);
				}

				DictionaryReqDto dtoS = new DictionaryReqDto();
				if("S".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoS.setId(dict.getId());
					dtoS.setTitle(dict.getTitle());
					dtoS.setDescription(dict.getDescription());
					dtoS.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoS.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoS.add(dtoS);
				}

				DictionaryReqDto dtoT = new DictionaryReqDto();
				if("T".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoT.setId(dict.getId());
					dtoT.setTitle(dict.getTitle());
					dtoT.setDescription(dict.getDescription());
					dtoT.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoT.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoT.add(dtoT);
				}

				DictionaryReqDto dtoU = new DictionaryReqDto();
				if("U".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoU.setId(dict.getId());
					dtoU.setTitle(dict.getTitle());
					dtoU.setDescription(dict.getDescription());
					dtoU.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoU.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoU.add(dtoU);
				}

				DictionaryReqDto dtoV = new DictionaryReqDto();
				if("V".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoV.setId(dict.getId());
					dtoV.setTitle(dict.getTitle());
					dtoV.setDescription(dict.getDescription());
					dtoV.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoV.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoV.add(dtoV);
				}

				DictionaryReqDto dtoW = new DictionaryReqDto();
				if("W".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoW.setId(dict.getId());
					dtoW.setTitle(dict.getTitle());
					dtoW.setDescription(dict.getDescription());
					dtoW.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoW.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoW.add(dtoW);
				}

				DictionaryReqDto dtoX = new DictionaryReqDto();
				if("X".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoX.setId(dict.getId());
					dtoX.setTitle(dict.getTitle());
					dtoX.setDescription(dict.getDescription());
					dtoX.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoX.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoX.add(dtoX);
				}

				DictionaryReqDto dtoY = new DictionaryReqDto();
				if("Y".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoY.setId(dict.getId());
					dtoY.setTitle(dict.getTitle());
					dtoY.setDescription(dict.getDescription());
					dtoY.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoY.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoY.add(dtoY);
				}

				DictionaryReqDto dtoZ = new DictionaryReqDto();
				if("Z".equalsIgnoreCase(dict.getTitle().substring(0, 1))) {
					dtoZ.setId(dict.getId());
					dtoZ.setTitle(dict.getTitle());
					dtoZ.setDescription(dict.getDescription());
					dtoZ.setImage(this.imageBaseUrl + "dictionary/" + dict.getImage());
					dtoZ.setIsActiveHomepage(dict.getIsActiveHomepage());
					dictListDtoZ.add(dtoZ);
				}
			}
		}
		dtoList.setA(dictListDtoA);
		dtoList.setB(dictListDtoB);
		dtoList.setC(dictListDtoC);
		dtoList.setD(dictListDtoD);
		dtoList.setE(dictListDtoE);
		dtoList.setF(dictListDtoF);
		dtoList.setG(dictListDtoG);
		dtoList.setH(dictListDtoH);
		dtoList.setI(dictListDtoI);
		dtoList.setJ(dictListDtoJ);
		dtoList.setK(dictListDtoK);
		dtoList.setL(dictListDtoL);
		dtoList.setM(dictListDtoM);
		dtoList.setN(dictListDtoN);
		dtoList.setO(dictListDtoO);
		dtoList.setP(dictListDtoP);
		dtoList.setQ(dictListDtoQ);
		dtoList.setR(dictListDtoR);
		dtoList.setS(dictListDtoS);
		dtoList.setT(dictListDtoT);
		dtoList.setU(dictListDtoU);
		dtoList.setV(dictListDtoV);
		dtoList.setW(dictListDtoW);
		dtoList.setX(dictListDtoX);
		dtoList.setY(dictListDtoY);
		dtoList.setZ(dictListDtoZ);

		return dtoList;
	}

}
