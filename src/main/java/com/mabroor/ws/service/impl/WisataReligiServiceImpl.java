package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstWisataReligi;
import com.mabroor.ws.repository.WisataReligiRepository;
import com.mabroor.ws.service.WisataReligiService;

@Service
public class WisataReligiServiceImpl implements WisataReligiService  {

	@Autowired
	private WisataReligiRepository wisataRepo;
	
	@Override
	public List<MstWisataReligi> findAllByStatus() {
		// TODO Auto-generated method stub
		return wisataRepo.findAllByStatus();
	}

	@Override
	public List<MstWisataReligi> getListWisata() {
		// TODO Auto-generated method stub
		return wisataRepo.getListWisata();
	}

	@Override
	public Optional<MstWisataReligi> findById(Long id) {
		// TODO Auto-generated method stub
		return wisataRepo.findById(id);
	}

	@Override
	public MstWisataReligi save(MstWisataReligi mstWisata) {
		// TODO Auto-generated method stub
		return wisataRepo.save(mstWisata);
	}

	@Override
	public MstWisataReligi update(MstWisataReligi mstWisata) {
		// TODO Auto-generated method stub
		return wisataRepo.save(mstWisata);
	}

	@Override
	public List<MstWisataReligi> findAllByUserId(Long id) {
		// TODO Auto-generated method stub
		return wisataRepo.findAllByUserId(id);
	}

	@Override
	public MstWisataReligi findLeadsId(String id) {
		// TODO Auto-generated method stub
		return wisataRepo.findLeadsId(id);
	}

}
