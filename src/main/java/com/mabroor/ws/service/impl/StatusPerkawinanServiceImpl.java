package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstStatusPerkawinan;
import com.mabroor.ws.repository.StatusPerkawinanRepository;
import com.mabroor.ws.service.StatusPerkawinanService;

@Service
public class StatusPerkawinanServiceImpl implements StatusPerkawinanService{

	@Autowired
	private StatusPerkawinanRepository statKawinRepo;
	
	@Override
	public List<MstStatusPerkawinan> findAllByStatus() {
		// TODO Auto-generated method stub
		return statKawinRepo.findAllByStatus();
	}

	@Override
	public List<MstStatusPerkawinan> getListPerkawinanStatus() {
		// TODO Auto-generated method stub
		return statKawinRepo.getListPerkawinanStatus();
	}

	@Override
	public Optional<MstStatusPerkawinan> findById(Long id) {
		// TODO Auto-generated method stub
		return statKawinRepo.findById(id);
	}

}
