package com.mabroor.ws.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.dto.CalculateMrpMotorReqDto;
import com.mabroor.ws.entity.MstMotorMaster;
import com.mabroor.ws.repository.MotorMasterRepository;
import com.mabroor.ws.service.MotorMasterService;

@Service
public class MotorMasterServiceImpl implements MotorMasterService{
	
	@Autowired 
	private MotorMasterRepository motorMasterRepo;
	
	@Override
	public List<MstMotorMaster> findAll() {
		// TODO Auto-generated method stub
		return motorMasterRepo.findAll();
	}

	@Override
	public List<MstMotorMaster> findByBrandMotor() {
		// TODO Auto-generated method stub
		return motorMasterRepo.findByBrandMotor();
	}

	@Override
	public List<MstMotorMaster> findByTypeMotor(String brand) {
		// TODO Auto-generated method stub
		return motorMasterRepo.findByTypeMotor(brand);
	}

	@Override
	public List<MstMotorMaster> findByModelMotor(String type) {
		// TODO Auto-generated method stub
		return motorMasterRepo.findByModelMotor(type);
	}

	@Override
	public List<MstMotorMaster> findByYearMotor(String model) {
		// TODO Auto-generated method stub
		return motorMasterRepo.findByYearMotor(model);
	}

	@Override
	public Map<String, Object> findByCalculateMrp(CalculateMrpMotorReqDto calculateMrpMotor) {
		Map<String, Object> objtemp = new HashMap<>();
		Optional<MstMotorMaster> calculateMrp = motorMasterRepo.findByCalculateMrp(calculateMrpMotor.getBrand(), calculateMrpMotor.getType(), calculateMrpMotor.getModel(), calculateMrpMotor.getYear());
		if(calculateMrp.isPresent()) {
			objtemp.put("mrp", calculateMrp.get().getPrice().multiply(new BigDecimal(0.75)).setScale(0, RoundingMode.HALF_UP));
		} else {
			objtemp.put("mrp", 0);
		}
		return objtemp;
	}

	@Override
	public List<MstMotorMaster> findByYearAndModelTypeMotor(String brand, String type, String model) {
		// TODO Auto-generated method stub
		return motorMasterRepo.findByYearAndModelTypeMotor(brand, type, model);
	}

	@Override
	public List<MstMotorMaster> findByBrandTypeModelMotor(String brand, String type) {
		return motorMasterRepo.findByBrandTypeModelMotor(brand, type);
	}

	@Override
	public List<MstMotorMaster> findByTypeMotorDistinct() {
		return motorMasterRepo.findByTypeMotorDistinct();
	}

}
