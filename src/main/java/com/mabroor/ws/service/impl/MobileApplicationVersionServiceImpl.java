package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstMobileApplicationVersion;
import com.mabroor.ws.repository.MobileApplicationVersionRepository;
import com.mabroor.ws.service.MobileApplicationVersionService;

@Service
public class MobileApplicationVersionServiceImpl implements MobileApplicationVersionService{

	@Autowired
	MobileApplicationVersionRepository appRepo;
	
	@Override
	public List<MstMobileApplicationVersion> getList() {
		// TODO Auto-generated method stub
		return appRepo.getList();
	}

}
