package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPlatKendaraan;
import com.mabroor.ws.repository.PlatKendaraanRepository;
import com.mabroor.ws.service.PlatKendaraanService;

@Service
public class PlatKendaraanServiceImpl implements PlatKendaraanService {

	@Autowired
	private PlatKendaraanRepository platRepo;
	
	@Override
	public List<MstPlatKendaraan> getListPlat() {
		// TODO Auto-generated method stub
		return platRepo.getListPlat();
	}

	@Override
	public Optional<MstPlatKendaraan> findById(Long id) {
		// TODO Auto-generated method stub
		return platRepo.findById(id);
	}

}
