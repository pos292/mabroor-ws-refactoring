package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstProductInformationBenefit;
import com.mabroor.ws.repository.ProductInformationBenefitRepository;
import com.mabroor.ws.service.ProductInformationBenefitService;

@Service
public class ProductInformationBenefitServiceImpl implements ProductInformationBenefitService{
	
	@Autowired
	ProductInformationBenefitRepository benfRepo;

	@Override
	public List<MstProductInformationBenefit> getListbyProductInfo(Long idProductInformation) {
		// TODO Auto-generated method stub
		return benfRepo.getListbyProductInfo(idProductInformation);
	}
}
