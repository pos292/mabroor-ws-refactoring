package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstGeneralInsuranceCar;
import com.mabroor.ws.repository.GeneralInsuranceCarRepository;
import com.mabroor.ws.service.GeneralInsuranceCarService;

@Service
public class GeneralInsuranceCarServiceImpl implements GeneralInsuranceCarService {

	@Autowired
	private GeneralInsuranceCarRepository insCarRepo;
	
	@Override
	public List<MstGeneralInsuranceCar> getAll() {
		// TODO Auto-generated method stub
		return insCarRepo.getAll();
	}

	@Override
	public Optional<MstGeneralInsuranceCar> getListInscType(Long id) {
		// TODO Auto-generated method stub
		return insCarRepo.getListInscType(id);
	}

}
