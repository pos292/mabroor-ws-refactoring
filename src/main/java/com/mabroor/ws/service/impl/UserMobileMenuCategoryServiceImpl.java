package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstUserMobileMenuCategory;
import com.mabroor.ws.repository.UserMobileMenuCategoryRepository;
import com.mabroor.ws.service.UserMobileMenuCategoryService;

@Service
public class UserMobileMenuCategoryServiceImpl implements UserMobileMenuCategoryService {
	
	@Autowired
	private UserMobileMenuCategoryRepository categoryRepo;

	

	@Override
	public Optional<MstUserMobileMenuCategory> findById(Long id) {
		// TODO Auto-generated method stub
		return categoryRepo.findById(id);
	}



	@Override
	public List<MstUserMobileMenuCategory> getList() {
		// TODO Auto-generated method stub
		return categoryRepo.getList();
	}

}
