package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstNeedsDenganJaminan;
import com.mabroor.ws.repository.NeedsDenganJaminanRepository;
import com.mabroor.ws.service.NeedsDenganJaminanService;

@Service
public class NeedsDenganJaminanServiceImpl implements NeedsDenganJaminanService {

	@Autowired
	private NeedsDenganJaminanRepository needDenganJaminanRepo;
	
	@Override
	public List<MstNeedsDenganJaminan> findAllByStatus() {
		// TODO Auto-generated method stub
		return needDenganJaminanRepo.findAllByStatus();
	}

	@Override
	public List<MstNeedsDenganJaminan> getListNeedsDenganJaminan() {
		// TODO Auto-generated method stub
		return needDenganJaminanRepo.getListNeedsDenganJaminan();
	}

	@Override
	public Optional<MstNeedsDenganJaminan> findById(Long id) {
		// TODO Auto-generated method stub
		return needDenganJaminanRepo.findById(id);
	}

	@Override
	public MstNeedsDenganJaminan getMstNeedsJaminanTitleDenganJaminan(String title) {
		// TODO Auto-generated method stub
		return needDenganJaminanRepo.getMstNeedsJaminanTitleDenganJaminan(title);
	}

}
