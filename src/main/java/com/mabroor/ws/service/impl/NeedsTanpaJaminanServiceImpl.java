package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstNeedsTanpaJaminan;
import com.mabroor.ws.repository.NeedsTanpaJaminanRepository;
import com.mabroor.ws.service.NeedsTanpaJaminanService;

@Service
public class NeedsTanpaJaminanServiceImpl implements NeedsTanpaJaminanService {

	@Autowired
	private NeedsTanpaJaminanRepository needTanpaJaminanRepo;
	
	@Override
	public List<MstNeedsTanpaJaminan> findAllByStatus() {
		// TODO Auto-generated method stub
		return needTanpaJaminanRepo.findAllByStatus();
	}

	@Override
	public List<MstNeedsTanpaJaminan> getListNeedsTanpaJaminan() {
		// TODO Auto-generated method stub
		return needTanpaJaminanRepo.getListNeedsTanpaJaminan();
	}

	@Override
	public Optional<MstNeedsTanpaJaminan> findById(Long id) {
		// TODO Auto-generated method stub
		return needTanpaJaminanRepo.findById(id);
	}

}
