package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstInboxCategory;
import com.mabroor.ws.repository.InboxCategoryRepository;
import com.mabroor.ws.service.InboxCategoryService;

@Service
public class InboxCategoryServiceImpl implements InboxCategoryService {

	@Autowired
	private InboxCategoryRepository categoryRepo;
	
	@Override
	public List<MstInboxCategory> getInboxCategory() {
		// TODO Auto-generated method stub
		return categoryRepo.getInboxCategory();
	}

	@Override
	public List<MstInboxCategory> getInboxCategoryAfterLogin() {
		// TODO Auto-generated method stub
		return categoryRepo.getInboxCategoryAfterLogin();
	}

	@Override
	public List<MstInboxCategory> getInboxCategoryBeforeLogin() {
		// TODO Auto-generated method stub
		return categoryRepo.getInboxCategoryBeforeLogin();
	}

	@Override
	public MstInboxCategory findById(Long id) {
		// TODO Auto-generated method stub
		return categoryRepo.findById(id).get();
	}

}
