package com.mabroor.ws.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstSholat;
import com.mabroor.ws.repository.SholatRepository;
import com.mabroor.ws.service.SholatService;

@Service
public class SholatServiceImpl implements SholatService {
	
	@Autowired
	private SholatRepository sholatRep;

	@Override
	public List<MstSholat> getListSholat() {
		// TODO Auto-generated method stub
		return sholatRep.getListSholat();
	}

	@Override
	public  List<MstSholat> getWaktuSholat(String location) {
		// TODO Auto-generated method stub
		return sholatRep.getWaktuSholat(location);
	}

	@Override
	public List<MstSholat> getLocation(String prov) {
		// TODO Auto-generated method stub
		return sholatRep.getLocation(prov);
	}

	@Override
	public List<MstSholat> getLocationAndTime(String prov, String date) {
		// TODO Auto-generated method stub
		return sholatRep.getLocationAndTime(prov, date);
	}

	@Override
	public List<MstSholat> getDate(String date) {
		// TODO Auto-generated method stub
		return sholatRep.getDate(date);
	}

	@Override
	public List<MstSholat> findByLocation() {
		// TODO Auto-generated method stub
		return sholatRep.findByLocation();
	}

}
