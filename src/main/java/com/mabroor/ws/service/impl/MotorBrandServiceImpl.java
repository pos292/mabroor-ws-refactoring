package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstMotorBrand;
import com.mabroor.ws.repository.MotorBrandRepository;
import com.mabroor.ws.service.MotorBrandService;

@Service
public class MotorBrandServiceImpl implements MotorBrandService {
	
	@Autowired
	private MotorBrandRepository motorBrandRepository;

	@Override
	public List<MstMotorBrand> findAllByStatus() {
		return motorBrandRepository.findAllByStatus();
	}

	@Override
	public Optional<MstMotorBrand> findById(Long id) {
		return motorBrandRepository.findById(id);
	}

	@Override
	public List<MstMotorBrand> getListMotorBrand() {
		return motorBrandRepository.getListMotorBrand();
	}
	
}
