package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstKelurahan;
import com.mabroor.ws.repository.KelurahanRepository;
import com.mabroor.ws.service.KelurahanService;

@Service
public class KelurahanServiceImpl implements KelurahanService {

	@Autowired 
	private KelurahanRepository lurahRepo;
	
	@Override
	public List<MstKelurahan> findAllByStatus() {
		// TODO Auto-generated method stub
		return lurahRepo.findAllByStatus();
	}

	@Override
	public Optional<MstKelurahan> findById(Long id) {
		// TODO Auto-generated method stub
		return lurahRepo.findById(id);
	}

	@Override
	public List<MstKelurahan> getListKelurahan() {
		// TODO Auto-generated method stub
		return lurahRepo.getListKelurahan();
	}

	@Override
	public List<MstKelurahan> findByKecamatanId(Long id) {
		// TODO Auto-generated method stub
		return lurahRepo.findByKecamatanId(id);
	}

}
