package com.mabroor.ws.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.dto.CountAccV2Dto;
import com.mabroor.ws.entity.MstPengajuanCar;
import com.mabroor.ws.repository.NewCarRoundRobinRepository;
import com.mabroor.ws.repository.PengajuanCarRepository;
import com.mabroor.ws.service.NewCarRoundRobinService;
import com.mabroor.ws.service.PengajuanCarService;
import com.mabroor.ws.util.UploadImage;

@Service
public class NewCarRoundRobinServiceImpl implements NewCarRoundRobinService {

	@Autowired
	private NewCarRoundRobinRepository newCarRoundRobinRepository;
	
	@Override
	public MstPengajuanCar findAllByStatus() {
		// TODO Auto-generated method stub
		return newCarRoundRobinRepository.findAllByStatus();
	}

	

}
