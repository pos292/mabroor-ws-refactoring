package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstKebijakanPrivasi;
import com.mabroor.ws.repository.KebijakanPrivasiRepository;
import com.mabroor.ws.service.KebijakanPrivasiService;

@Service
public class KebijakanPrivasiServiceImpl implements KebijakanPrivasiService {

	@Autowired
	private KebijakanPrivasiRepository privasiRepo;
	
	@Override
	public List<MstKebijakanPrivasi> getListPrivasi() {
		// TODO Auto-generated method stub
		return privasiRepo.getListPrivasi();
	}

}
