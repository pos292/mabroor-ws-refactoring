package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstKabupaten;
import com.mabroor.ws.repository.KabupatenRepository;
import com.mabroor.ws.service.KabupatenService;

@Service
public class KabupatenServiceImpl implements KabupatenService {

	@Autowired 
	private KabupatenRepository kabupatenRepo;
	
	@Override
	public List<MstKabupaten> findAllByStatus() {
		// TODO Auto-generated method stub
		return kabupatenRepo.findAllByStatus();
	}

	@Override
	public Optional<MstKabupaten> findById(Long id) {
		// TODO Auto-generated method stub
		return kabupatenRepo.findById(id);
	}

	@Override
	public List<MstKabupaten> getListKabupaten() {
		// TODO Auto-generated method stub
		return kabupatenRepo.getListKabupaten();
	}
	
	@Override
	public List<MstKabupaten> findByProvinsiId(Long id) {
		// TODO Auto-generated method stub
		return kabupatenRepo.getListKabupatenByProvinsi(id);
	}
	
	@Override
	public List<MstKabupaten> findByNamaKabupaten(String namaKabupaten) {
		// TODO Auto-generated method stub
		return kabupatenRepo.findByNamaKabupaten(namaKabupaten);
	}

}
