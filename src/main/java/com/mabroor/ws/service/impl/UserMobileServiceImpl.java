package com.mabroor.ws.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.mabroor.moxa.entity.MstUserMobileMoxa;
import com.mabroor.moxa.repository.UserMobileMoxaRepository;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.repository.UserMobileRepository;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.util.AES256Utils;
import com.mabroor.ws.util.ObjectMapperUtil;
import com.mabroor.ws.util.UploadImage;

@Service
public class UserMobileServiceImpl implements UserMobileService {
	
	private static final Logger LOG = Logger.getLogger(UserMobileServiceImpl.class);

	@Autowired
	private UserMobileRepository userMobileRep;
	
	@Autowired
	private UserMobileMoxaRepository userMobileMoxaRep;
	
	@Value("${storage.upload.file}")
	private String storageUploadFile;
	
	@Value("${moxa.storage.upload.file}")
	private String moxaStorageUploadFile;
	
	@Override
	public List<MstUserMobile> findAllByStatus() {
		// TODO Auto-generated method stub
		return userMobileRep.findAllByStatus();
	}


	@Override
	public List<MstUserMobile> findAllEnable() {
		// TODO Auto-generated method stub
		return userMobileRep.findAllEnable();
	}

	@Override
	public String save(MultipartFile ktpImage, MultipartFile npwpImage, MultipartFile bpkbMobilImage, MultipartFile bpkbMotorImage,
			MultipartFile kkImage, MultipartFile ppImage, MstUserMobile mstUserMobile) {
		
		Optional<MstUserMobile> tempUser = userMobileRep.findById(mstUserMobile.getId());
		
		String newFilename_ktp = "";
		String newFilename_npwp = "";
		String newFilename_bpkbMobilImage = "";
		String newFilename_bpkbMotorImage = "";
		String newFilename_kk = "";
		String newFilename_pp = "";
		
		if(tempUser.isPresent()) {
			newFilename_ktp = tempUser.get().getKtpImage();
			newFilename_npwp = tempUser.get().getNpwpImage();
			newFilename_bpkbMobilImage = tempUser.get().getBpkbMobilImage();
			newFilename_bpkbMotorImage = tempUser.get().getBpkbMotorImage();
			newFilename_kk = tempUser.get().getKkImage();
			newFilename_pp = tempUser.get().getPpImage();
			
			if(!tempUser.get().getPhone().equals(mstUserMobile.getPhone())) {
				List<MstUserMobile> checkPhone = userMobileRep.getByPhoneNumber(AES256Utils.encrypt(mstUserMobile.getPhone()), tempUser.get().getId());
				if(checkPhone.size() > 0) {
					return "phone_existing";
				}
			}
			
			if(tempUser.get().getEmail()!= null && !"".equals(tempUser.get().getEmail()) && !tempUser.get().getEmail().equals(mstUserMobile.getEmail())) {
				List<MstUserMobile> checkEmail = userMobileRep.getByEmail(AES256Utils.encrypt(mstUserMobile.getEmail()), tempUser.get().getId());
				if(checkEmail.size() > 0) {
					return "email_existing";
				}
			}
			
			mstUserMobile.setUserMobileUuid(tempUser.get().getUserMobileUuid());
			mstUserMobile.setPin(tempUser.get().getPin());
			mstUserMobile.setPinLockDateTime(tempUser.get().getPinLockDateTime());
			mstUserMobile.setPinLockStatus(tempUser.get().getPinLockStatus());
			mstUserMobile.setCountPin(tempUser.get().getCountPin());
		}
		
		if (bpkbMobilImage != null && !bpkbMobilImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_bpkbMobilImage = UploadImage.createImage(directoryName, bpkbMobilImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setBpkbMobilImage(newFilename_bpkbMobilImage);
		
		if (bpkbMotorImage != null && !bpkbMotorImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_bpkbMotorImage = UploadImage.createImage(directoryName, bpkbMotorImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setBpkbMotorImage(newFilename_bpkbMotorImage);
		
		if (ktpImage != null && !ktpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKtpImage(newFilename_ktp);
		
		if (npwpImage != null && !npwpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setNpwpImage(newFilename_npwp);
		
		if (kkImage != null && !kkImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKkImage(newFilename_kk);
		
		if (ppImage != null && !ppImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setPpImage(newFilename_pp);
		
		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));
		
		if(mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName() + " - " + mstUserMobile.getPhone());
		}
		
		userMobileRep.save(mstUserMobile);
		
		LOG.info("mstUserMobile Flag Acceptance : " + mstUserMobile.getAcceptanceMoxaMabroor());
		
		//CUSTOMER CONSENT
		if(mstUserMobile.getAcceptanceMoxaMabroor() != null && "YES".equals(mstUserMobile.getAcceptanceMoxaMabroor())) {
			LOG.info("[CUSTOMER CONSENT UPDATE PROFILE]");
			Optional<MstUserMobileMoxa> moxaUserTemp = userMobileMoxaRep.getOneByPhoneNumber(AES256Utils.encrypt(mstUserMobile.getPhone()));
			MstUserMobileMoxa moxaUser = null;
			String temporaryToken = "";
			String temporaryUserMobileUuid = "";
			String temporaryFirebaseToken = "";
			String temporaryPin = "";
			String temporaryFingerprintToggle = "";
			String temporaryFingerprintHashKey = "";
			Long temporaryCountPin = null;
			Date temporaryPinLockDateTime = null;
			String temporaryPinLockStatus = "";
			Long temporaryUserId = null;
			if(moxaUserTemp.isPresent()) {
				temporaryToken = moxaUserTemp.get().getToken();
				temporaryUserMobileUuid = moxaUserTemp.get().getUserMobileUuid();
				temporaryFirebaseToken = moxaUserTemp.get().getFirebaseToken();
				temporaryUserId = moxaUserTemp.get().getId();
				temporaryPin = moxaUserTemp.get().getPin();
				temporaryFingerprintToggle = moxaUserTemp.get().getFingerprintToggle();
				temporaryFingerprintHashKey = moxaUserTemp.get().getFingerprintHashKey();
				temporaryCountPin = moxaUserTemp.get().getCountPin();
				temporaryPinLockDateTime = moxaUserTemp.get().getPinLockDateTime();
				temporaryPinLockStatus = moxaUserTemp.get().getPinLockStatus();
				moxaUser = ObjectMapperUtil.map(mstUserMobile, MstUserMobileMoxa.class);
				
				moxaUser.setId(temporaryUserId);
				moxaUser.setToken(temporaryToken);
				moxaUser.setFirebaseToken(temporaryFirebaseToken);
				moxaUser.setUserMobileUuid(temporaryUserMobileUuid);
				moxaUser.setPin(temporaryPin);
				moxaUser.setFingerprintToggle(temporaryFingerprintToggle);
				moxaUser.setFingerprintHashKey(temporaryFingerprintHashKey);
				moxaUser.setCountPin(temporaryCountPin);
				moxaUser.setPinLockDateTime(temporaryPinLockDateTime);
				moxaUser.setPinLockStatus(temporaryPinLockStatus);
				
				if(moxaUser.getKtpImage() != null && !"".equals(moxaUser.getKtpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getKtpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getKtpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy KTP File from Mabroor");
						e.printStackTrace();
					}
				}
				
				if(moxaUser.getPpImage() != null && !"".equals(moxaUser.getPpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getPpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getPpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy Profile Picture File from Mabroor");
						e.printStackTrace();
					}
				}
				
				if(moxaUser.getNpwpImage() != null && !"".equals(moxaUser.getNpwpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getNpwpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getNpwpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy NPWP File from Mabroor");
						e.printStackTrace();
					}
				}
				
				userMobileMoxaRep.save(moxaUser);
			}
		}
		
		return "success";
	}


	@Override
	public MstUserMobile update(MultipartFile ktpImage, MultipartFile npwpImage, MultipartFile bpkbMobilImage, MultipartFile bpkbMotorImage,
			MultipartFile kkImage, MultipartFile ppImage, MstUserMobile mstUserMobile) {
		
		Optional<MstUserMobile> tempUser = userMobileRep.findById(mstUserMobile.getId());
		
		String newFilename_ktp = "";
		String newFilename_npwp = "";
		String newFilename_bpkbMobilImage = "";
		String newFilename_bpkbMotorImage = "";
		String newFilename_kk = "";
		String newFilename_pp = "";
		
//		if(tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_bpkbMobilImage = tempUser.get().getBpkbMobilImage();
//			newFilename_bpkbMotorImage = tempUser.get().getBpkbMotorImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//		}
		
		if (bpkbMobilImage != null && !bpkbMobilImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_bpkbMobilImage = UploadImage.createImage(directoryName, bpkbMobilImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setBpkbMobilImage(newFilename_bpkbMobilImage);
		
		if (bpkbMotorImage != null && !bpkbMotorImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_bpkbMotorImage = UploadImage.createImage(directoryName, bpkbMotorImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setBpkbMotorImage(newFilename_bpkbMotorImage);
		
		if (ktpImage != null && !ktpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKtpImage(newFilename_ktp);
		
		if (npwpImage != null && !npwpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setNpwpImage(newFilename_npwp);
		
		if (kkImage != null && !kkImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKkImage(newFilename_kk);
		
		if (ppImage != null && !ppImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setPpImage(newFilename_pp);
		
		LOG.info("mstUserMobile Flag Acceptance : " + mstUserMobile.getAcceptanceMoxaMabroor());
		
		//CUSTOMER CONSENT
		if(mstUserMobile.getAcceptanceMoxaMabroor() != null && "YES".equals(mstUserMobile.getAcceptanceMoxaMabroor())) {
			LOG.info("[CUSTOMER CONSENT UPDATE PROFILE]");
			Optional<MstUserMobileMoxa> moxaUserTemp = userMobileMoxaRep.getOneByPhoneNumber(mstUserMobile.getPhone());
			MstUserMobileMoxa moxaUser = null;
			String temporaryToken = "";
			String temporaryUserMobileUuid = "";
			String temporaryFirebaseToken = "";
			String temporaryPin = "";
			String temporaryFingerprintToggle = "";
			String temporaryFingerprintHashKey = "";
			Long temporaryCountPin = null;
			Date temporaryPinLockDateTime = null;
			String temporaryPinLockStatus = "";
			Long temporaryUserId = null;
			if(moxaUserTemp.isPresent()) {
				temporaryToken = moxaUserTemp.get().getToken();
				temporaryUserMobileUuid = moxaUserTemp.get().getUserMobileUuid();
				temporaryFirebaseToken = moxaUserTemp.get().getFirebaseToken();
				temporaryUserId = moxaUserTemp.get().getId();
				temporaryPin = moxaUserTemp.get().getPin();
				temporaryFingerprintToggle = moxaUserTemp.get().getFingerprintToggle();
				temporaryFingerprintHashKey = moxaUserTemp.get().getFingerprintHashKey();
				temporaryCountPin = moxaUserTemp.get().getCountPin();
				temporaryPinLockDateTime = moxaUserTemp.get().getPinLockDateTime();
				temporaryPinLockStatus = moxaUserTemp.get().getPinLockStatus();
				moxaUser = ObjectMapperUtil.map(mstUserMobile, MstUserMobileMoxa.class);
				
				moxaUser.setId(temporaryUserId);
				moxaUser.setToken(temporaryToken);
				moxaUser.setFirebaseToken(temporaryFirebaseToken);
				moxaUser.setUserMobileUuid(temporaryUserMobileUuid);
				moxaUser.setPin(temporaryPin);
				moxaUser.setFingerprintToggle(temporaryFingerprintToggle);
				moxaUser.setFingerprintHashKey(temporaryFingerprintHashKey);
				moxaUser.setCountPin(temporaryCountPin);
				moxaUser.setPinLockDateTime(temporaryPinLockDateTime);
				moxaUser.setPinLockStatus(temporaryPinLockStatus);
				
				if(moxaUser.getKtpImage() != null && !"".equals(moxaUser.getKtpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getKtpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getKtpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy KTP File from Mabroor");
						e.printStackTrace();
					}
				}
				
				if(moxaUser.getPpImage() != null && !"".equals(moxaUser.getPpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getPpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getPpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy Profile Picture File from Mabroor");
						e.printStackTrace();
					}
				}
				
				if(moxaUser.getNpwpImage() != null && !"".equals(moxaUser.getNpwpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getNpwpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getNpwpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy NPWP File from Mabroor");
						e.printStackTrace();
					}
				}
				
				userMobileMoxaRep.save(moxaUser);
			}
		}
		
		return userMobileRep.save(mstUserMobile);
	}


	@Override
	public List<MstUserMobile> getListMobile() {
		// TODO Auto-generated method stub
		return userMobileRep.getListMobile();
	}


	@Override
	public Optional<MstUserMobile> findById(Long id) {
		// TODO Auto-generated method stub
		return userMobileRep.findById(id);
	}
	
	@Override
	public String checkPhoneNumber(String phoneNumber) {
		// TODO Auto-generated method stub
		List<MstUserMobile> checkPhone = userMobileRep.checkPhoneExist(phoneNumber);
		if(checkPhone.size() > 0) {
			return "phone_existing";
		}
		return "success";
	}
	
	@Override
	public void updateFirebaseToken(String firebaseToken, Long userId) {
		
		Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
		
		if(tempUser.isPresent()) {
			userMobileRep.updateFirebaseToken(firebaseToken, userId);
		}
	}


	@Override
	public String checkPhoneUser(String phone) {
		// TODO Auto-generated method stub
		List<MstUserMobile> checkPhone = userMobileRep.checkPhoneUser(phone);
		if(checkPhone.size() > 0) {
			return "phone_existing";
		}
		return "success";
	}
	
	@Override
	public MstUserMobile save1(MstUserMobile mstUserMobile) {
		// TODO Auto-generated method stub
		return userMobileRep.save(mstUserMobile);
	}
	
	@Override
	public void updatePin(String pin, Long userId) {
		
		Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
		
		if(tempUser.isPresent()) {
			userMobileRep.updatePin(pin, userId);
		}
	}
	
	@Override
	public void updateCountPin(Long count, Long userId) {
		
		Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
		
		if(tempUser.isPresent()) {
			userMobileRep.updateCountPin(count, userId);
		}
	}
	
	@Override
	public void updateLockTimePin(Date lockTime, Long userId) {
		
		Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
		
		if(tempUser.isPresent()) {
			userMobileRep.updateLockTimePin(lockTime, userId);
		}
	}
	
	@Override
	public void resetLockTImePin(Long userId) {
		
		Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
		
		if(tempUser.isPresent()) {
			userMobileRep.resetLockTImePin(userId);
		}
	}
	
	@Override
	public void updateLockStatus(String lockStatus, Long userId) {
		
		Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
		
		if(tempUser.isPresent()) {
			userMobileRep.updateLockStatus(lockStatus, userId);
		}
	}


	@Override
	public void updateFingerprintToggle(String fingerprintToggle, Long userId) {
		// TODO Auto-generated method stub
		Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
		
		if(tempUser.isPresent()) {
			userMobileRep.updateFingerprintToggle(fingerprintToggle, userId);
		}
	}


//	@Override
//	public String saveIdentity(MultipartFile ktpImage, MultipartFile npwpImage, MultipartFile kkImage,
//			MultipartFile ppImage, MstUserMobile mstUserMobile) {
//		Optional<MstUserMobile> tempUser = userMobileRep.findById(mstUserMobile.getId());
//
//		String newFilename_ktp = "";
//		String newFilename_npwp = "";
//		String newFilename_kk = "";
//		String newFilename_pp = "";
//
////		if (tempUser.isPresent()) {
////			newFilename_ktp = tempUser.get().getKtpImage();
////			newFilename_npwp = tempUser.get().getNpwpImage();
////			newFilename_kk = tempUser.get().getKkImage();
////			newFilename_pp = tempUser.get().getPpImage();
////
////			
////		}
//
//		if (ktpImage != null && !ktpImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
//		}
//		mstUserMobile.setKtpImage(newFilename_ktp);
//
//		if (npwpImage != null && !npwpImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage,
//					UUID.randomUUID().toString());
//		}
//		mstUserMobile.setNpwpImage(newFilename_npwp);
//
//		if (kkImage != null && !kkImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
//		}
//		mstUserMobile.setKkImage(newFilename_kk);
//
//		if (ppImage != null && !ppImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
//		}
//		mstUserMobile.setPpImage(newFilename_pp);
//
//		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
//		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));
//
//		if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
//			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
//					+ " - " + mstUserMobile.getPhone());
//		}
//
//		userMobileRep.save(mstUserMobile);
//		return "success";
//	}
//
//	@Override
//	public MstUserMobile updateIdentity(MultipartFile ktpImage, MultipartFile npwpImage, MultipartFile kkImage,
//			MultipartFile ppImage, MstUserMobile mstUserMobile) {
//Optional<MstUserMobile> tempUser = userMobileRep.findById(mstUserMobile.getId());
//		
//		String newFilename_ktp = "";
//		String newFilename_npwp = "";
//		String newFilename_kk = "";
//		String newFilename_pp = "";
//		
////		if(tempUser.isPresent()) {
////			newFilename_ktp = tempUser.get().getKtpImage();
////			newFilename_npwp = tempUser.get().getNpwpImage();
////			newFilename_kk = tempUser.get().getKkImage();
////			newFilename_pp = tempUser.get().getPpImage();
////		}
//		
//		
//		if (ktpImage != null && !ktpImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
//		}
//		mstUserMobile.setKtpImage(newFilename_ktp);
//		
//		if (npwpImage != null && !npwpImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage, UUID.randomUUID().toString());
//		}
//		mstUserMobile.setNpwpImage(newFilename_npwp);
//		
//		if (kkImage != null && !kkImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
//		}
//		mstUserMobile.setKkImage(newFilename_kk);
//		
//		if (ppImage != null && !ppImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
//		}
//		mstUserMobile.setPpImage(newFilename_pp);
//		return userMobileRep.save(mstUserMobile);
//	}


	@Override
	public String saveIdentityKtp(MultipartFile ktpImage, MstUserMobile mstUserMobile) {
		String newFilename_ktp = "";
		
//		if (tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//
//			
//		}

		if (ktpImage != null && !ktpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKtpImage(newFilename_ktp);

		

		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));

		if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
					+ " - " + mstUserMobile.getPhone());
		}

		userMobileRep.save(mstUserMobile);
		return "success";
	
	}


	@Override
	public MstUserMobile updateIdentityKtp(MultipartFile ktpImage, MstUserMobile mstUserMobile) {
        String newFilename_ktp = "";
		
//		if (tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//
//			
//		}

		if (ktpImage != null && !ktpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKtpImage(newFilename_ktp);

		

		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));

		if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
					+ " - " + mstUserMobile.getPhone());
		}

		
		return userMobileRep.save(mstUserMobile);
	}


	@Override
	public String saveIdentityNpwp(MultipartFile npwpImage, MstUserMobile mstUserMobile) {
        String newFilename_npwp = "";
		
//		if (tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//
//			
//		}

		if (npwpImage != null && !npwpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setNpwpImage(newFilename_npwp);

		

		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));

		if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
					+ " - " + mstUserMobile.getPhone());
		}

		userMobileRep.save(mstUserMobile);
		return "success";
	
	}


	@Override
	public MstUserMobile updateIdentityNpwp(MultipartFile npwpImage, MstUserMobile mstUserMobile) {
        String newFilename_npwp = "";
		
//		if (tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//
//			
//		}

		if (npwpImage != null && !npwpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setNpwpImage(newFilename_npwp);

		

		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));

		if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
					+ " - " + mstUserMobile.getPhone());
		}

	
		return userMobileRep.save(mstUserMobile);
	}


	@Override
	public String saveIdentityKk(MultipartFile kkImage, MstUserMobile mstUserMobile) {
		 String newFilename_kk = "";
			
//			if (tempUser.isPresent()) {
//				newFilename_ktp = tempUser.get().getKtpImage();
//				newFilename_npwp = tempUser.get().getNpwpImage();
//				newFilename_kk = tempUser.get().getKkImage();
//				newFilename_pp = tempUser.get().getPpImage();
	//
//				
//			}

			if (kkImage != null && !kkImage.isEmpty()) {
				String directoryName = this.storageUploadFile + "user-mobile/";
				File directory = new File(directoryName);
				if (!directory.exists()) {
					directory.mkdir();
				}
				newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
			}
			mstUserMobile.setKkImage(newFilename_kk);

			

			LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
			LOG.info("Update data : " + new Gson().toJson(mstUserMobile));

			if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
				LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
						+ " - " + mstUserMobile.getPhone());
			}

			userMobileRep.save(mstUserMobile);
			return "success";
	}


	@Override
	public MstUserMobile updateIdentityKk(MultipartFile kkImage, MstUserMobile mstUserMobile) {
String newFilename_kk = "";
		
//		if (tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//
//			
//		}

		if (kkImage != null && !kkImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKkImage(newFilename_kk);

		

		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));

		if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
					+ " - " + mstUserMobile.getPhone());
		}

		
		return userMobileRep.save(mstUserMobile);
	}


	@Override
	public String saveIdentityPp(MultipartFile ppImage, MstUserMobile mstUserMobile) {
		String newFilename_pp = "";
		
//		if (tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//
//			
//		}

		if (ppImage != null && !ppImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setPpImage(newFilename_pp);

		

		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));

		if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
					+ " - " + mstUserMobile.getPhone());
		}

		userMobileRep.save(mstUserMobile);
		return "success";
	}


	@Override
	public MstUserMobile updateIdentityPp(MultipartFile ppImage, MstUserMobile mstUserMobile) {
String newFilename_pp = "";
		
//		if (tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//
//			
//		}

		if (ppImage != null && !ppImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setPpImage(newFilename_pp);

		

		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));

		if (mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName()
					+ " - " + mstUserMobile.getPhone());
		}

	
		return userMobileRep.save(mstUserMobile);
	}


	@Override
	public String saveIdent(MultipartFile ktpImage, MultipartFile npwpImage, MultipartFile kkImage, MultipartFile ppImage,
			MstUserMobile mstUserMobile) {
		String newFilename_ktp = "";
		String newFilename_npwp = "";
		
		String newFilename_kk = "";
		String newFilename_pp = "";

		System.out.println("aaaa"+ktpImage);
		System.out.println("aaaa"+npwpImage);
		System.out.println("aaaa"+kkImage);
		System.out.println("aaaa"+ppImage);
		if (ktpImage != null && !ktpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKtpImage(newFilename_ktp);
		
		if (npwpImage != null && !npwpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setNpwpImage(newFilename_npwp);
		
		if (kkImage != null && !kkImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setKkImage(newFilename_kk);
		
		if (ppImage != null && !ppImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
		}
		mstUserMobile.setPpImage(newFilename_pp);
		
		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
		LOG.info("Update data : " + new Gson().toJson(mstUserMobile));
		
		if(mstUserMobile.getNpwpNumber() == null || "".equals(mstUserMobile.getNpwpNumber())) {
			LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobile.getEmail() + " - " + mstUserMobile.getName() + " - " + mstUserMobile.getPhone());
		}
		
		userMobileRep.save(mstUserMobile);
		
		return "success";
	}
		

	@Override
	public MstUserMobile updateIdent(MultipartFile ktpImage, MultipartFile npwpImage, MultipartFile kkImage,
			MultipartFile ppImage, MstUserMobile mstUserMobile) {
		String newFilename_ktp = "";
		String newFilename_npwp = "";
		String newFilename_kk = "";
		String newFilename_pp = "";
		
		System.out.println("aaaa"+mstUserMobile.getKtpImage());
		System.out.println("aaaa"+mstUserMobile.getNpwpImage());
		System.out.println("aaaa"+mstUserMobile.getKkImage());
		System.out.println("aaaa"+mstUserMobile.getPpImage());
//		if(tempUser.isPresent()) {
//			newFilename_ktp = tempUser.get().getKtpImage();
//			newFilename_npwp = tempUser.get().getNpwpImage();
//			newFilename_bpkbMobilImage = tempUser.get().getBpkbMobilImage();
//			newFilename_bpkbMotorImage = tempUser.get().getBpkbMotorImage();
//			newFilename_kk = tempUser.get().getKkImage();
//			newFilename_pp = tempUser.get().getPpImage();
//		}
		
		
		if (ktpImage != null && !ktpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
		}else {
			newFilename_ktp = mstUserMobile.getKtpImage();
		}
		System.out.println("KTP FILE = " + newFilename_ktp);
		mstUserMobile.setKtpImage(newFilename_ktp);
		
		if (npwpImage != null && !npwpImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage, UUID.randomUUID().toString());
		}else {
			newFilename_npwp = mstUserMobile.getNpwpImage();
		}
		System.out.println("NPWP FILE = " + newFilename_npwp);
		mstUserMobile.setNpwpImage(newFilename_npwp);
		
		if (kkImage != null && !kkImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
		}else {
			newFilename_kk = mstUserMobile.getKkImage();
		}
		System.out.println("KK FILE = " + newFilename_kk);
		mstUserMobile.setKkImage(newFilename_kk);
		
		if (ppImage != null && !ppImage.isEmpty()) {
			String directoryName = this.storageUploadFile + "user-mobile/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
		}else {
			newFilename_pp = mstUserMobile.getPpImage();
		}
		System.out.println("PP FILE = " + newFilename_pp);
		mstUserMobile.setPpImage(newFilename_pp);
		
		//CUSTOMER CONSENT
		if(mstUserMobile.getAcceptanceMoxaMabroor() != null && "YES".equals(mstUserMobile.getAcceptanceMoxaMabroor())) {
			LOG.info("[CUSTOMER CONSENT UPDATE PROFILE]");
			Optional<MstUserMobileMoxa> moxaUserTemp = userMobileMoxaRep.getOneByPhoneNumber(AES256Utils.encrypt(mstUserMobile.getPhone()));
			MstUserMobileMoxa moxaUser = null;
			String temporaryToken = "";
			String temporaryUserMobileUuid = "";
			String temporaryFirebaseToken = "";
			String temporaryPin = "";
			String temporaryFingerprintToggle = "";
			String temporaryFingerprintHashKey = "";
			Long temporaryCountPin = null;
			Date temporaryPinLockDateTime = null;
			String temporaryPinLockStatus = "";
			Long temporaryUserId = null;
			if(moxaUserTemp.isPresent()) {
				temporaryToken = moxaUserTemp.get().getToken();
				temporaryUserMobileUuid = moxaUserTemp.get().getUserMobileUuid();
				temporaryFirebaseToken = moxaUserTemp.get().getFirebaseToken();
				temporaryUserId = moxaUserTemp.get().getId();
				temporaryPin = moxaUserTemp.get().getPin();
				temporaryFingerprintToggle = moxaUserTemp.get().getFingerprintToggle();
				temporaryFingerprintHashKey = moxaUserTemp.get().getFingerprintHashKey();
				temporaryCountPin = moxaUserTemp.get().getCountPin();
				temporaryPinLockDateTime = moxaUserTemp.get().getPinLockDateTime();
				temporaryPinLockStatus = moxaUserTemp.get().getPinLockStatus();
				moxaUser = ObjectMapperUtil.map(mstUserMobile, MstUserMobileMoxa.class);
				
				moxaUser.setId(temporaryUserId);
				moxaUser.setToken(temporaryToken);
				moxaUser.setFirebaseToken(temporaryFirebaseToken);
				moxaUser.setUserMobileUuid(temporaryUserMobileUuid);
				moxaUser.setPin(temporaryPin);
				moxaUser.setFingerprintToggle(temporaryFingerprintToggle);
				moxaUser.setFingerprintHashKey(temporaryFingerprintHashKey);
				moxaUser.setCountPin(temporaryCountPin);
				moxaUser.setPinLockDateTime(temporaryPinLockDateTime);
				moxaUser.setPinLockStatus(temporaryPinLockStatus);
				
				if(moxaUser.getKtpImage() != null && !"".equals(moxaUser.getKtpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getKtpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getKtpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy KTP File from Mabroor");
						e.printStackTrace();
					}
				}
				
				if(moxaUser.getPpImage() != null && !"".equals(moxaUser.getPpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getPpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getPpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy Profile Picture File from Mabroor");
						e.printStackTrace();
					}
				}
				
				if(moxaUser.getNpwpImage() != null && !"".equals(moxaUser.getNpwpImage())) {
					try {
						Files.copy(new File(storageUploadFile + "user-mobile/" + moxaUser.getNpwpImage()).toPath(), new File(moxaStorageUploadFile + "user-mobile/" + moxaUser.getNpwpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy NPWP File from Mabroor");
						e.printStackTrace();
					}
				}
				
				userMobileMoxaRep.save(moxaUser);
			}
		}
		
		return userMobileRep.save(mstUserMobile);
	}


	@Override
	public MstUserMobile getOneByPhoneNumber(String phoneNumber) {
		// TODO Auto-generated method stub
		System.out.println(userMobileRep.getOneByPhoneNumber(phoneNumber));
		return userMobileRep.getOneByPhoneNumber(phoneNumber);
	}


	@Override
	public List<MstUserMobile> findAllUserPromoBundle() {
		// TODO Auto-generated method stub
		return userMobileRep.findAllUserPromoBundle();
	}


	@Override
	public MstUserMobile getListProfile() {
		// TODO Auto-generated method stub
		return userMobileRep.getListProfile();
	}


	@Override
	public void updateUserType(String type, Long userId) {
		// TODO Auto-generated method stub
        Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
		
		if(tempUser.isPresent()) {
			userMobileRep.updateLockStatus(type, userId);
		}
	}


	@Override
	public boolean checkMoxaExistingUser(String phone) {
		Optional<MstUserMobileMoxa> moxaUser = userMobileMoxaRep.getOneByPhoneNumber(phone);
		MstUserMobile mabroorUser = userMobileRep.getOneByPhoneNumber(phone);
		boolean flagAcceptance = false;
		
		if(moxaUser.isPresent() && mabroorUser != null && mabroorUser.getAcceptanceMoxaMabroor() == null) {
		// if(mabroorUser != null && mabroorUser.getAcceptanceMoxaMabroor() == null) {
			flagAcceptance = true;
		}
		return flagAcceptance;
	}


	@Override
	public boolean acceptanceMoxaMabroor(String state, String phone) {
		MstUserMobile mabroorUser = userMobileRep.getOneByPhoneNumber(phone);
		LOG.info("[-acceptanceMoxaMabroor-] : " + phone + " with state : " + state);
		if(mabroorUser.getAcceptanceMoxaMabroor() == null && "YES".equals(state)) {
			Optional<MstUserMobileMoxa> moxaUser = userMobileMoxaRep.getOneByPhoneNumber(phone);
			
			LOG.info("[-acceptanceMoxaMabroor-] : copying moxa data");
			LOG.info("[-acceptanceMoxaMabroor-] : moxa user is present? " + moxaUser.isPresent());
			
			String temporaryToken = mabroorUser.getToken();
			String temporaryUserMobileUuid = mabroorUser.getUserMobileUuid();
			String temporaryFirebaseToken = mabroorUser.getFirebaseToken();
			Long temporaryUserId = mabroorUser.getId();
			if(moxaUser.isPresent()) {
				mabroorUser = ObjectMapperUtil.map(moxaUser.get(), MstUserMobile.class);
				moxaUser.get().setAcceptanceMoxaMabroor(state);
				moxaUser.get().setAcceptanceMoxaMabroorDate(new Date());
				
				mabroorUser.setId(temporaryUserId);
				mabroorUser.setToken(temporaryToken);
				mabroorUser.setUserMobileUuid(temporaryUserMobileUuid);
				mabroorUser.setFirebaseToken(temporaryFirebaseToken);
				mabroorUser.setAcceptanceMoxaMabroor(state);
				mabroorUser.setAcceptanceMoxaMabroorDate(new Date());
				
				if(mabroorUser.getKtpImage() != null && !"".equals(mabroorUser.getKtpImage())) {
					try {
						Files.copy(new File(moxaStorageUploadFile + "user-mobile/" + mabroorUser.getKtpImage()).toPath(), new File(storageUploadFile + "user-mobile/" + mabroorUser.getKtpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy KTP File from Moxa");
						e.printStackTrace();
					}
				}
				
				if(mabroorUser.getPpImage() != null && !"".equals(mabroorUser.getPpImage())) {
					try {
						Files.copy(new File(moxaStorageUploadFile + "user-mobile/" + mabroorUser.getPpImage()).toPath(), new File(storageUploadFile + "user-mobile/" + mabroorUser.getPpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy Profile Picture File from Moxa");
						e.printStackTrace();
					}
				}
				
				if(mabroorUser.getNpwpImage() != null && !"".equals(mabroorUser.getNpwpImage())) {
					try {
						Files.copy(new File(moxaStorageUploadFile + "user-mobile/" + mabroorUser.getNpwpImage()).toPath(), new File(storageUploadFile + "user-mobile/" + mabroorUser.getNpwpImage()).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						LOG.info("Failed copy NPWP File from Moxa");
						e.printStackTrace();
					}
				}
				userMobileMoxaRep.save(moxaUser.get());
				userMobileRep.save(mabroorUser);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public MstUserMobile findPreviousUser(String phoneNumber) {
		return userMobileRep.findPreviousUser(phoneNumber);
	}


//	@Override
//	public void insertMabroor(MstUserMobile mstUserMobile) {
//		// TODO Auto-generated method stub
//		
//		MstUserMobile newUser = new MstUserMobile();
//		newUser.setEmail(mstUserMobile.getEmail());
//		newUser.setPhone(mstUserMobile.getPhone());
//		newUser.setName(mstUserMobile.getName());
//		newUser.setFirstName(mstUserMobile.getName());
//		newUser.setBirthDate(mstUserMobile.getBirthDate());
//		newUser.setUserMobileStatus("05");
//		newUser.setUserMobileUuid(UUID.randomUUID().toString());
//		newUser.setFingerprintToggle("ON");
//		newUser.setUserType("Mabroor");
//		MstUserMobile newUserTemp = userMobileRep.save(newUser);
//
//	
//		
//		try {
//			byte[] salt;
//			salt = SecureUtils.getInstance().getSalt();
//			String payload = SecureUtils.getInstance().getSecurePassword(newUserTemp.getId() + newUserTemp.getEmail() + newUserTemp.getPhone() + newUserTemp.getUserMobileUuid() + new Date().getTime(), salt);
//
//			LOG.info("Generated payload : " + payload);
//			String token = "";
//			token = JWTGenerator.getInstance().createJWT(Long.toString(newUserTemp.getId()), newUserTemp.getEmail(), newUserTemp.getPhone(), new Long("2628000000"), payload);
//			newUser.setToken(token);
//
//			userMobileRep.save(newUserTemp);
//		} catch (NoSuchAlgorithmException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		
//
//	}


//	@Override
//	public void updateUserEntity(MultipartFile ktpImage, MultipartFile npwpImage, MultipartFile kkImage, MultipartFile ppImage, Long userId) {
//		String newFilename_ktp = "";
//		String newFilename_npwp = "";		
//		String newFilename_kk = "";
//		String newFilename_pp = "";
//
//        Optional<MstUserMobile> tempUser = userMobileRep.findById(userId);
//		
//		if(tempUser.isPresent()) {
//			
//		
//		if (ktpImage != null && !ktpImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_ktp = UploadImage.createImage(directoryName, ktpImage, UUID.randomUUID().toString());
//		}
//		tempUser.get().setKtpImage(newFilename_ktp);
//		
//		if (npwpImage != null && !npwpImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_npwp = UploadImage.createImage(directoryName, npwpImage, UUID.randomUUID().toString());
//		}
//		tempUser.get().setNpwpImage(newFilename_npwp);
//		
//		if (kkImage != null && !kkImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_kk = UploadImage.createImage(directoryName, kkImage, UUID.randomUUID().toString());
//		}
//		tempUser.get().setKkImage(newFilename_kk);
//		
//		if (ppImage != null && !ppImage.isEmpty()) {
//			String directoryName = this.storageUploadFile + "user-mobile/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//			newFilename_pp = UploadImage.createImage(directoryName, ppImage, UUID.randomUUID().toString());
//		}
//		tempUser.get().setPpImage(newFilename_pp);
//		
//		
//		LOG.info("======================= PROCESSING USER MOBILE IMPLEMENTATION [SAVE] ========================");
//		LOG.info("Update data : " + new Gson().toJson(tempUser.get()));
//		LOG.info(tempUser.get().getKkImage());
//		
//			
//		}
//		
//		
//		
//	}
}
