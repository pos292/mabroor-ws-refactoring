package com.mabroor.ws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstLeadsRule;
import com.mabroor.ws.repository.LeadsRuleRepository;
import com.mabroor.ws.service.LeadsRuleService;

@Service
public class LeadsRuleServiceImpl implements LeadsRuleService {

	@Autowired
	LeadsRuleRepository leadsRepo;
	
	@Override
	public MstLeadsRule save(MstLeadsRule mstLeadsRule) {
		// TODO Auto-generated method stub
		return leadsRepo.save(mstLeadsRule);
	}

}
