package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstActivityUser;
import com.mabroor.ws.repository.ActivityUserRepository;
import com.mabroor.ws.service.ActivityUserService;

 

@Service
public class ActivityUserServiceImpl implements ActivityUserService {

	@Autowired
	private ActivityUserRepository actRepo;
	
	@Override
	public List<MstActivityUser> getListActivityUser() {
		// TODO Auto-generated method stub
		return actRepo.getListActivityUser();
	}

	@Override
	public List<MstActivityUser> findAllByUserId(Long id) {
		// TODO Auto-generated method stub
		return actRepo.findAllByUserId(id);
	}

	@Override
	public Optional<MstActivityUser> findById(Long id) {
		// TODO Auto-generated method stub
		return actRepo.findById(id);
	}

	@Override
	public MstActivityUser save(MstActivityUser mstAct) {
		// TODO Auto-generated method stub
		return actRepo.save(mstAct);
	}

	@Override
	public MstActivityUser update(MstActivityUser mstAct) {
		// TODO Auto-generated method stub
		return actRepo.save(mstAct);
	}

}
