package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstCityBranch;
import com.mabroor.ws.repository.CityBranchRepository;
import com.mabroor.ws.service.CityBranchService;

@Service
public class CityBranchServiceImpl implements CityBranchService {

	@Autowired
	private CityBranchRepository cityRepo;
	
	@Override
	public List<MstCityBranch> getCityByPengajuan(String kotaPengajuan) {
		// TODO Auto-generated method stub
		return cityRepo.getCityByPengajuan(kotaPengajuan);
	}

	@Override
	public List<MstCityBranch> getList() {
		// TODO Auto-generated method stub
		return cityRepo.getList();
	}
	
	@Override
	public List<MstCityBranch> getCityByKabupatenId(Long kotaPengajuan) {
		// TODO Auto-generated method stub
		return cityRepo.getCityByKabupatenId(kotaPengajuan);
	}

}
