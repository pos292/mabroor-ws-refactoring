package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstTenorMotor;
import com.mabroor.ws.repository.TenorMotorRepository;
import com.mabroor.ws.service.TenorMotorService;

@Service
public class TenorMotorServiceImpl implements TenorMotorService {
	
	@Autowired
	private TenorMotorRepository tenorRepo;

	@Override
	public List<MstTenorMotor> getList() {
		// TODO Auto-generated method stub
		return tenorRepo.getList();
	}

}
