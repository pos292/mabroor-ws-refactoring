package com.mabroor.ws.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.util.UploadImage;
import com.mabroor.ws.entity.MstPengajuanJaminan;
import com.mabroor.ws.repository.PengajuanJaminanRepository;
import com.mabroor.ws.service.PengajuanJaminanService;

@Service
public class PengajuanJaminanServiceImpl implements PengajuanJaminanService  {

	@Autowired 
	private PengajuanJaminanRepository jaminanRepo;
	
	@Value("${storage.upload.file}")
	private String storageUploadFile;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public List<MstPengajuanJaminan> findAllByStatus() {
		// TODO Auto-generated method stub
		return jaminanRepo.findAllByStatus();
	}

	@Override
	public List<MstPengajuanJaminan> getListPengajuan() {
		// TODO Auto-generated method stub
		return jaminanRepo.getListPengajuan();
	}

	@Override
	public List<MstPengajuanJaminan> findAllByTitleAsc() {
		// TODO Auto-generated method stub
		return jaminanRepo.findAllByTitleAsc();
	}

	@Override
	public List<MstPengajuanJaminan> findAllEnable() {
		// TODO Auto-generated method stub
		return jaminanRepo.findAllEnable();
	}

	@Override
	public Optional<MstPengajuanJaminan> findById(Long id) {
		// TODO Auto-generated method stub
		return jaminanRepo.findById(id);
	}

	@Override
	public MstPengajuanJaminan save(MultipartFile fileBpkbMobil, MultipartFile fileBpkbMotor, MultipartFile fileStnk,MultipartFile fileFotoKananDepan,MultipartFile fileFotoKananBelakang,MultipartFile fileFotoKiriDepan,MultipartFile fileFotoKiriBelakang, MstPengajuanJaminan mstPengajuan) {
		String newFilename_bpkbMobil = "";
		String newFilename_bpkbMotor = "";
		String newFilename_stnk = "";
		String newFilename_kanandepan = "";
		String newFilename_kananbelakang = "";
		String newFilename_kiridepan = "";
		String newFilename_kiribelakang = "";
		
		if (fileBpkbMobil != null && !fileBpkbMobil.isEmpty()) {
			String directoryName = this.storageUploadFile + "danatunai-jaminan/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_bpkbMobil = UploadImage.createImage(directoryName, fileBpkbMobil, UUID.randomUUID().toString());
		}
		mstPengajuan.setFotoBpkbMobil(newFilename_bpkbMobil);
		
		if (fileBpkbMotor != null && !fileBpkbMotor.isEmpty()) {
			String directoryName = this.storageUploadFile + "danatunai-jaminan/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_bpkbMotor = UploadImage.createImage(directoryName, fileBpkbMotor, UUID.randomUUID().toString());
		}
		mstPengajuan.setFotoBpkbMotor(newFilename_bpkbMotor);
		
		if (fileStnk != null && !fileStnk.isEmpty()) {
			String directoryName = this.storageUploadFile + "danatunai-jaminan/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_stnk = UploadImage.createImage(directoryName, fileStnk, UUID.randomUUID().toString());
		}
		mstPengajuan.setFotoStnk(newFilename_stnk);
		
		if (fileFotoKananDepan != null && !fileFotoKananDepan.isEmpty()) {
			String directoryName = this.storageUploadFile + "danatunai-jaminan/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kanandepan = UploadImage.createImage(directoryName, fileFotoKananDepan, UUID.randomUUID().toString());
		}
		mstPengajuan.setFotoKananDepan(newFilename_kanandepan);
		
		if (fileFotoKananBelakang != null && !fileFotoKananBelakang.isEmpty()) {
			String directoryName = this.storageUploadFile + "danatunai-jaminan/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kananbelakang = UploadImage.createImage(directoryName, fileFotoKananBelakang, UUID.randomUUID().toString());
		}
		mstPengajuan.setFotoKananBelakang(newFilename_kananbelakang);
		
		if (fileFotoKiriDepan != null && !fileFotoKiriDepan.isEmpty()) {
			String directoryName = this.storageUploadFile + "danatunai-jaminan/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kiridepan = UploadImage.createImage(directoryName, fileFotoKiriDepan, UUID.randomUUID().toString());
		}
		mstPengajuan.setFotoKiriDepan(newFilename_kiridepan);
		
		if (fileFotoKiriBelakang != null && !fileFotoKiriBelakang.isEmpty()) {
			String directoryName = this.storageUploadFile + "danatunai-jaminan/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			newFilename_kiribelakang = UploadImage.createImage(directoryName, fileFotoKiriBelakang, UUID.randomUUID().toString());
		}
		mstPengajuan.setFotoKiriBelakang(newFilename_kiribelakang);
		return jaminanRepo.save(mstPengajuan);
	}

	@Override
	public MstPengajuanJaminan update(MstPengajuanJaminan mstPengajuan) {
		// TODO Auto-generated method stub
		return jaminanRepo.save(mstPengajuan);
	}

	@Override
	public List<MstPengajuanJaminan> findAllByUserId(Long id) {
		// TODO Auto-generated method stub
		return jaminanRepo.findAllByUserId(id);
	}

	@Override
	public List<MstPengajuanJaminan> findByPhone(String phone) {
		// TODO Auto-generated method stub
		return jaminanRepo.findByPhone(phone);
	}

}
