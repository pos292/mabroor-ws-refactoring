package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPendidikanTerakhir;
import com.mabroor.ws.repository.PendidikanTerakhirRepository;
import com.mabroor.ws.service.PendidikanTerakhirService;

@Service
public class PendidikanTerahirServiceImpl implements PendidikanTerakhirService{

	@Autowired
	private PendidikanTerakhirRepository pendidikanRepo;
	
	@Override
	public List<MstPendidikanTerakhir> findAllByStatus() {
		// TODO Auto-generated method stub
		return pendidikanRepo.findAllByStatus();
	}

	@Override
	public List<MstPendidikanTerakhir> getListPendidikanTerakhir() {
		// TODO Auto-generated method stub
		return pendidikanRepo.getListPendidikanTerakhir();
	}

	@Override
	public Optional<MstPendidikanTerakhir> findById(Long id) {
		// TODO Auto-generated method stub
		return pendidikanRepo.findById(id);
	}

}
