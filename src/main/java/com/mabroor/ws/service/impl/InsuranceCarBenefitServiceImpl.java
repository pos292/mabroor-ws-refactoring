package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstGeneralInsuranceCarProductBenefit;
import com.mabroor.ws.repository.InsuranceCarProductBenefitRepository;
import com.mabroor.ws.service.InsuranceCarProductBenefitService;

@Service
public class InsuranceCarBenefitServiceImpl implements InsuranceCarProductBenefitService {
	
	@Autowired
	private InsuranceCarProductBenefitRepository benefitRepo;

	@Override
	public List<MstGeneralInsuranceCarProductBenefit> getListCarProductByProductId(Long carProductId) {
		// TODO Auto-generated method stub
		return benefitRepo.getListInscByInsuranceCarProductId(carProductId);
	}

}
