package com.mabroor.ws.service.impl;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.SysParam;
import com.mabroor.ws.entity.MstOtp;
import com.mabroor.ws.entity.MstOtpCount;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.repository.OtpCountRepository;
import com.mabroor.ws.repository.OtpRepository;
import com.mabroor.ws.repository.SysParamRepository;
import com.mabroor.ws.repository.UserMobileRepository;
import com.mabroor.ws.service.OtpService;
import com.mabroor.ws.util.AES256Utils;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.util.OTPGateway;
import com.mabroor.ws.util.SecureUtils;

import io.jsonwebtoken.MalformedJwtException;

@Service
public class OtpServiceImpl implements OtpService  {

	private static final Logger LOG = Logger.getLogger(OtpServiceImpl.class);

	@Autowired
	private OtpRepository otpRepo;

	@Autowired
	private OtpCountRepository otpCountRepo;

	@Autowired
	private UserMobileRepository userMobileRepo;

	@Autowired
	private SysParamRepository sysParamRepo;

	@Override
	public String save(MstOtp mstOtp) {
		mstOtp.setOtpCode(generateOtpCode());
		mstOtp.setIsVerified(false);
		Calendar date = Calendar.getInstance();
		long t= date.getTimeInMillis();
		Date tmpExpire = new Date(t + (Constant.OTP_EXPIRY_TIME * 60000));
		mstOtp.setOtpExpiry(tmpExpire);

		Map<String, Object> obj = OTPGateway.getInstance().sendOTP(mstOtp.getPhoneNumber(),  mstOtp.getOtpCode());

		/*******
		 * 	FOR DEVELOPMENT ONLY - REMOVE THIS WHEN PRODUCTION
		 */

		String otpSuccess = "";
		if(obj.get("status") != null && "Insufficient Balance".equals(obj.get("status"))) {
			mstOtp.setOtpCode("7613");
			otpRepo.save(mstOtp);
			otpSuccess = "insufficient";
		}else if("Success".equals(obj.get("status"))) {
			otpRepo.save(mstOtp);
			otpSuccess = "success";
		}else {
			otpSuccess = "failed";
		}
		/*******
		 * 	END
		 */

		return otpSuccess;
	}
	@Override
	public String saveGeneral(MstOtp mstOtp) {
		mstOtp.setOtpCode(generateOtpCode());
		mstOtp.setIsVerified(false);
		Calendar date = Calendar.getInstance();
		long t= date.getTimeInMillis();
		Date tmpExpire = new Date(t + (Constant.OTP_EXPIRY_TIME * 60000));
		mstOtp.setOtpExpiry(tmpExpire);

		Map<String, Object> obj = OTPGateway.getInstance().sendOTP(mstOtp.getPhoneNumber(),  mstOtp.getOtpCode());

		/*******
		 * 	FOR DEVELOPMENT ONLY - REMOVE THIS WHEN PRODUCTION
		 */
		String otpSuccess = "";
		if(obj.get("status") != null && "Insufficient Balance".equals(obj.get("status"))) {
			mstOtp.setOtpCode("7613");
			otpRepo.save(mstOtp);
			otpSuccess = "insufficient";
		}else if("Success".equals(obj.get("status"))) {
			otpRepo.save(mstOtp);
			otpSuccess = "success";
		}else {
			otpSuccess = "failed";
		}
		/*******
		 * 	END
		 */

		return otpSuccess;
	}
	private String generateOtpCode() {
		Random r = new Random();
		return ""+ r.ints(0,10).findFirst().getAsInt() + 
				r.ints(0,10).findFirst().getAsInt() + 
				r.ints(0,10).findFirst().getAsInt() + 
				r.ints(0,10).findFirst().getAsInt();
	}
	@Override
	public LinkedHashMap<String, Object> verify(MstOtp mstOtp) {

		LinkedHashMap<String, Object> res = new LinkedHashMap<String, Object>(); 
		MstUserMobile user = new MstUserMobile();
//		if(mstOtp.getEncryptionStatus()!=null) {
//			user = userMobileRepo.getOneByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//		}else{
			user = userMobileRepo.getOneByPhoneNumber(mstOtp.getPhoneNumber());
//		}
		if(user != null) {
			if("LOCKED".equals(user.getPinLockStatus()) && user.getPinLockDateTime() != null && user.getPinLockDateTime().compareTo(new Date())>0){
				res.put("message", "Silakan coba beberapa saat lagi.");
				res.put("token", null);
				res.put("userProfile", null);
				return res;
			}
		}
		try {
			// TODO Auto-generated method stub
			MstOtp mstOtp1 = new MstOtp();
//			if(mstOtp.getEncryptionStatus()!=null) {
//				mstOtp1 = otpRepo.findByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//			}else {
				mstOtp1 = otpRepo.findByPhoneNumber(mstOtp.getPhoneNumber());
//			}

			SysParam sysParamMaxRetryOtp = sysParamRepo.findByValueType("otp_retry");
			int maxRetryOtp = 3;
			if (sysParamMaxRetryOtp !=null && !"".equals(sysParamMaxRetryOtp.getValueName())) {
				try {
					maxRetryOtp = Integer.parseInt(sysParamMaxRetryOtp.getValueName());
				} catch (NumberFormatException nfe) {
					nfe.printStackTrace();
					maxRetryOtp = 3;
				}
			}

			SysParam sysParamBlockOtp = sysParamRepo.findByValueType("otp_expired");
			int blockOtp = 30;
			if (sysParamBlockOtp !=null && !"".equals(sysParamBlockOtp.getValueName())) {
				try {
					blockOtp = Integer.parseInt(sysParamBlockOtp.getValueName());
				} catch (NumberFormatException nfe) {
					nfe.printStackTrace();
					blockOtp = 30;
				}
			}
			String minutesParam = blockOtp + " minute";

			SysParam sysParamMaxResendOtp = sysParamRepo.findByValueType("otp_resend");
			int maxResendOtp = 3;
			if (sysParamMaxResendOtp !=null && !"".equals(sysParamMaxResendOtp.getValueName())) {
				try {
					maxResendOtp = Integer.parseInt(sysParamMaxResendOtp.getValueName());
				} catch (NumberFormatException nfe) {
					nfe.printStackTrace();
					maxResendOtp = 3;
				}
			}


			// increment retry
			MstOtpCount mstOtpCount = new MstOtpCount();
			mstOtpCount.setMstOtp(mstOtp1);
			otpCountRepo.save(mstOtpCount);

			List<MstOtpCount> listOtpCount = otpCountRepo.findByMstOtp(mstOtp1);
			System.out.println("list OTP Count size ***********" + listOtpCount.size());

			if (mstOtp !=null && mstOtp1!=null && mstOtp.getOtpCode().equals(mstOtp1.getOtpCode()) && listOtpCount.size() < maxRetryOtp) {
				if(mstOtp1.getOtpExpiry().compareTo(new Date())<=0) {
					res.put("message", "Kode OTP salah, silakan coba lagi.");
					res.put("token", null);
					res.put("userProfile", null);
					return res;
				}

				String token ="";
				if(user != null ) {
					res.put("userProfile", user);

					byte[] salt = SecureUtils.getInstance().getSalt();

					String payload = SecureUtils.getInstance().getSecurePassword(user.getId() + user.getEmail() + user.getPhone() + user.getUserMobileUuid() + new Date().getTime(), salt);

					LOG.info("Generated payload : " + payload);

					token = JWTGenerator.getInstance().createJWT(Long.toString(user.getId()), user.getEmail(), user.getPhone(), new Long("2628000000"), payload);

					String goals = user.getGoals();
					if(goals == null || "".equals(user.getGoals())) {
						goals = "[1,5,4]";
						user.setGoals(goals);
					}
					
//					if(user.getGender() != null && !"".equals(user.getGender())) {
//						user.setGender(mstOtp1.getGender());
//					}
					user.setToken(token);
					user.setUserMobileUuid(UUID.randomUUID().toString());
					userMobileRepo.save(user);

					res.put("moxaReffId", user.getUserMobileUuid());

					mstOtp1.setIsVerified(true);
					otpRepo.save(mstOtp1);

					otpRepo.updateAllVerificationByPhoneNumber(mstOtp.getPhoneNumber());
				}else {

					String goals = mstOtp1.getGoals();
					if(goals == null || "".equals(mstOtp1.getGoals())) {
						goals = "[1,5,4]";
					}
					System.out.println("MASUK SINI DULU YA HARUSNYA");
					MstUserMobile newUser = new MstUserMobile();
					newUser.setEmail(mstOtp1.getEmail());
					newUser.setPhone(mstOtp1.getPhoneNumber());
					newUser.setName(mstOtp1.getName());
					newUser.setFirstName(mstOtp1.getName());
					newUser.setBirthDate(mstOtp1.getBirthDate());
					newUser.setGoals(goals);
					if(mstOtp1.getGender() != null && !"".equals(mstOtp1.getGender())) {
						newUser.setGender(mstOtp1.getGender());
					}
					newUser.setUserMobileUuid(UUID.randomUUID().toString());
					newUser.setUserMobileStatus("01");
					newUser.setFingerprintToggle("ON");

					MstUserMobile previousUser = new MstUserMobile();
//					if(mstOtp1.getEncryptionStatus()!=null) {
//						previousUser = userMobileRepo.findPreviousUser(AES256Utils.encrypt(mstOtp1.getPhoneNumber()));
//					}else {
						previousUser = userMobileRepo.findPreviousUser(mstOtp1.getPhoneNumber());
//					}
					if(previousUser != null) {
						newUser.setPreviousUserId(previousUser.getId());
					}

					MstUserMobile newUserTemp = userMobileRepo.save(newUser);

					byte[] salt = SecureUtils.getInstance().getSalt();

					String payload = SecureUtils.getInstance().getSecurePassword(newUserTemp.getId() + newUserTemp.getEmail() + newUserTemp.getPhone() + newUserTemp.getUserMobileUuid() + new Date().getTime(), salt);

					LOG.info("Generated payload : " + payload);

					token = JWTGenerator.getInstance().createJWT(Long.toString(newUserTemp.getId()), newUserTemp.getEmail(), newUserTemp.getPhone(), new Long("2628000000"), payload);
					newUser.setToken(token);

					userMobileRepo.save(newUserTemp);

					mstOtp1.setIsVerified(true);
					otpRepo.save(mstOtp1);

//					if(mstOtp1.getEncryptionStatus()!=null) {
//						otpRepo.updateAllVerificationByPhoneNumber(AES256Utils.encrypt(mstOtp1.getPhoneNumber()));
//					}else {
						// otpRepo.updateAllVerificationByPhoneNumber(mstOtp1.getPhoneNumber())/;
//					}
					otpRepo.updateAllVerificationByPhoneNumber(mstOtp.getPhoneNumber());

					res.put("moxaReffId", newUser.getUserMobileUuid());

					res.put("userProfile", null);
				}
				res.put("token", token);
				return res;	
			}else if (mstOtp !=null && mstOtp1!=null && listOtpCount.size() < maxRetryOtp){
				/*
				 *  HANYA UNTUK DEVELOPMENT, HARUS DI TAKEOUT JIKA PRODUCTION
				 */
				if (mstOtp !=null && mstOtp1!=null && "7613".equals(mstOtp.getOtpCode())) {
					if(mstOtp1.getOtpExpiry().compareTo(new Date())<=0) {
						res.put("message", "Kode OTP salah, silakan coba lagi.");
						res.put("token", null);
						res.put("userProfile", null);
						return res;
					}

					String token ="";
					if(user != null ) {
						if ("03".equals(user.getUserMobileStatus())) {
							user.setUserMobileStatus("01");
							res.put("userProfile", null);
						}else {
							res.put("userProfile", user);
						}

						byte[] salt = SecureUtils.getInstance().getSalt();

						String payload = SecureUtils.getInstance().getSecurePassword(user.getId() + user.getEmail() + user.getPhone() + user.getUserMobileUuid() + new Date().getTime(), salt);

						LOG.info("Generated payload : " + payload);

						token = JWTGenerator.getInstance().createJWT(Long.toString(user.getId()), user.getEmail(), user.getPhone(), new Long("2628000000"), payload);

						String goals = user.getGoals();
						if(goals == null || "".equals(user.getGoals())) {
							goals = "[1,5,4]";
							user.setGoals(goals);
						}

//						if(user.getGender() != null && !"".equals(user.getGender())) {
//							user.setGender(mstOtp1.getGender());
//						}
						user.setToken(token);
						user.setUserMobileUuid(UUID.randomUUID().toString());
						userMobileRepo.save(user);

						res.put("moxaReffId", user.getUserMobileUuid());

						mstOtp1.setIsVerified(true);
						otpRepo.save(mstOtp1);

//						if(mstOtp1.getEncryptionStatus()!=null) {
//							otpRepo.updateAllVerificationByPhoneNumber(AES256Utils.encrypt(mstOtp1.getPhoneNumber()));
//						}else {
							// otpRepo.updateAllVerificationByPhoneNumber(mstOtp1.getPhoneNumber());
//						}
						otpRepo.updateAllVerificationByPhoneNumber(mstOtp.getPhoneNumber());
					}else {

						String goals = mstOtp1.getGoals();
						if(goals == null || "".equals(mstOtp1.getGoals())) {
							goals = "[1,5,4]";
						}

						MstUserMobile newUser = new MstUserMobile();
						newUser.setEmail(mstOtp1.getEmail());
						newUser.setPhone(mstOtp1.getPhoneNumber());
						newUser.setName(mstOtp1.getName());
						newUser.setFirstName(mstOtp1.getName());
						newUser.setBirthDate(mstOtp1.getBirthDate());
						newUser.setGoals(goals);
						if(mstOtp1.getGender() != null && !"".equals(mstOtp1.getGender())) {
							newUser.setGender(mstOtp1.getGender());
						}
						newUser.setUserMobileUuid(UUID.randomUUID().toString());
						newUser.setUserMobileStatus("01");
						newUser.setFingerprintToggle("ON");

						MstUserMobile previousUser = new MstUserMobile();
//						if(mstOtp1.getEncryptionStatus()!=null) {
//							previousUser = userMobileRepo.findPreviousUser(AES256Utils.encrypt(mstOtp1.getPhoneNumber()));
//						}else {
							previousUser = userMobileRepo.findPreviousUser(mstOtp1.getPhoneNumber());
//						}
						if(previousUser != null) {
							newUser.setPreviousUserId(previousUser.getId());
						}

						MstUserMobile newUserTemp = userMobileRepo.save(newUser);

						byte[] salt = SecureUtils.getInstance().getSalt();

						String payload = SecureUtils.getInstance().getSecurePassword(newUserTemp.getId() + newUserTemp.getEmail() + newUserTemp.getPhone() + newUserTemp.getUserMobileUuid() + new Date().getTime(), salt);

						LOG.info("Generated payload : " + payload);

						token = JWTGenerator.getInstance().createJWT(Long.toString(newUserTemp.getId()), newUserTemp.getEmail(), newUserTemp.getPhone(), new Long("2628000000"), payload);
						newUser.setToken(token);

						userMobileRepo.save(newUserTemp);

						res.put("moxaReffId", newUser.getUserMobileUuid());

						mstOtp1.setIsVerified(true);
						otpRepo.save(mstOtp1);
						
//						if(mstOtp1.getEncryptionStatus()!=null) {
//							otpRepo.updateAllVerificationByPhoneNumber(AES256Utils.encrypt(mstOtp1.getPhoneNumber()));
//						}else {
							// otpRepo.updateAllVerificationByPhoneNumber(mstOtp1.getPhoneNumber());
//						}
						otpRepo.updateAllVerificationByPhoneNumber(mstOtp.getPhoneNumber());

						res.put("userProfile", null);
					}
					res.put("token", token);
					return res;	
				}
				
				/*
				 * END
				 */
			}
			
			if(mstOtp1 != null) {
				if (listOtpCount.size() < maxRetryOtp) {
					//"invalidotp";
					res.put("message", "Kode OTP salah, silakan coba lagi.");
				} else {
					mstOtp1.setOtpExpiry(mstOtp1.getCreatedDate());
					otpRepo.save(mstOtp1);
					List<MstOtp> listMstOtp = new ArrayList();
//					if(mstOtp.getEncryptionStatus()!=null) {
						listMstOtp = otpRepo.findByExpiredDate(AES256Utils.encrypt(mstOtp.getPhoneNumber()), new Date(), minutesParam);
//					}else {
//						listMstOtp = otpRepo.findByExpiredDate(mstOtp.getPhoneNumber(), new Date(), minutesParam);
//					}
					System.out.println("listMstOtp*******************" + listMstOtp.size());
					// Cek apakah belum melebihi max resend otp parameter
					if (listMstOtp != null && (listMstOtp.size()==0 || (listMstOtp.size() <= maxResendOtp))) {
						if(listOtpCount.size() > 3) {
							res.put("message", "Silakan coba beberapa saat lagi.");
						}else {
							//"expired otp"
							res.put("message", "Kode OTP salah, silakan coba lagi.");
						}
					} else {
						//"max retry"
						res.put("message", "Silakan coba beberapa saat lagi.");
					}
				}
			}else {
				// verification not valid
				res.put("message", "Silakan coba beberapa saat lagi.");
			}
			
			res.put("token", null);
			res.put("userProfile", null);
			return res;
		}catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			res.put("token", null);
			res.put("userProfile", null);
			return res;
		}catch (NullPointerException np) {
			np.printStackTrace();
			LOG.info(np.getMessage());
			res.put("token", null);
			res.put("userProfile", null);
			return res;
		}catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			res.put("token", null);
			res.put("userProfile", null);
			return res;
		}catch(RuntimeException re) {
			LOG.info(re.getMessage());
			res.put("token", null);
			res.put("userProfile", null);
			return res;
		}catch(OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			res.put("token", null);
			res.put("userProfile", null);
			return res;
		} catch (NoSuchAlgorithmException nsa) {
			LOG.info(nsa.getMessage());
			res.put("token", null);
			res.put("userProfile", null);
			return res;
		} catch(Exception e) {
			e.printStackTrace();
			LOG.info(e.getMessage());
			res.put("token", null);
			res.put("userProfile", null);
			return res;
		}
	}
	@Override
	public String verifyGeneral(MstOtp mstOtp) {
		// TODO Auto-generated method stub

		MstUserMobile user = new MstUserMobile();
//		if(mstOtp.getEncryptionStatus()!=null) {
		System.out.println("nomor kembar = " + mstOtp.getPhoneNumber());
			user = userMobileRepo.getOneByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//		}else {
//			user = userMobileRepo.getOneByPhoneNumber(mstOtp.getPhoneNumber());
//		}
		if(user != null) {
			if("LOCKED".equals(user.getPinLockStatus()) && user.getPinLockDateTime() != null && user.getPinLockDateTime().compareTo(new Date())>0){
				return "Silakan coba beberapa saat lagi.";
			}
		}

		SysParam sysParamMaxRetryOtp = sysParamRepo.findByValueType("otp_retry");
		int maxRetryOtp = 3;
		if (sysParamMaxRetryOtp !=null && !"".equals(sysParamMaxRetryOtp.getValueName())) {
			try {
				maxRetryOtp = Integer.parseInt(sysParamMaxRetryOtp.getValueName());
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				maxRetryOtp = 3;
			}
		}
		System.out.println("maxRetryyyy  =  " + maxRetryOtp);

		SysParam sysParamBlockOtp = sysParamRepo.findByValueType("otp_expired");
		int blockOtp = 30;
		if (sysParamBlockOtp !=null && !"".equals(sysParamBlockOtp.getValueName())) {
			try {
				blockOtp = Integer.parseInt(sysParamBlockOtp.getValueName());
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				blockOtp = 30;
			}
		}
		String minutesParam = blockOtp + " minute";

		SysParam sysParamMaxResendOtp = sysParamRepo.findByValueType("otp_resend");
		int maxResendOtp = 3;
		if (sysParamMaxResendOtp !=null && !"".equals(sysParamMaxResendOtp.getValueName())) {
			try {
				maxResendOtp = Integer.parseInt(sysParamMaxResendOtp.getValueName());
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				maxResendOtp = 3;
			}
		}

		MstOtp mstOtp1 = new MstOtp();
//		if(mstOtp.getEncryptionStatus()!=null) {
			mstOtp1 = otpRepo.findByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//		}else {
		System.out.println("phone otppppp ==== " + mstOtp1.getPhoneNumber());
//			mstOtp1 = otpRepo.findByPhoneNumber(mstOtp.getPhoneNumber());
//		}

		// increment retry
		MstOtpCount mstOtpCount = new MstOtpCount();
		mstOtpCount.setMstOtp(mstOtp1);
		otpCountRepo.save(mstOtpCount);

		List<MstOtpCount> listOtpCount = otpCountRepo.findByMstOtp(mstOtp1);
		System.out.println("listOTPCount = " + listOtpCount);
		System.out.println("sizee = " + listOtpCount.size());
		System.out.println("mstOtp get otp code = " + mstOtp.getOtpCode() + "tessss");
		System.out.println("mstOtp get otp 1 code = " + mstOtp1.getOtpCode()+ "tessss");
		System.out.println("expiry  otp = " + mstOtp1.getOtpExpiry());
		System.out.println("new Date = " + new Date());
		System.out.println("MSTOTP != null " + mstOtp!=null);
		System.out.println("MSTOTP 1 != null " + mstOtp1!=null);
		System.out.println("equals === " + mstOtp.getOtpCode().equals(mstOtp1.getOtpCode()));
		System.out.println("listOtp Count === " + (listOtpCount.size() < maxRetryOtp));
		System.out.println("expiryyy datee == " + (mstOtp1.getOtpExpiry().compareTo(new Date())<=0));
		if (mstOtp !=null && mstOtp1!=null && mstOtp.getOtpCode().equals(mstOtp1.getOtpCode()) && listOtpCount.size() < maxRetryOtp) {
			if(mstOtp1.getOtpExpiry().compareTo(new Date())<=0) {
				return "Kode OTP salah, silakan coba lagi.";
			}

			mstOtp1.setIsVerified(true);
			otpRepo.save(mstOtp1);	

			otpRepo.updateAllVerificationByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
			
//			if(mstOtp1.getEncryptionStatus()!=null) {
				// otpRepo.updateAllVerificationByPhoneNumber(AES256Utils.encrypt(mstOtp1.getPhoneNumber()));
//			}else {
//				otpRepo.updateAllVerificationByPhoneNumber(mstOtp1.getPhoneNumber());
//			}
			return "success";
		}else if (mstOtp !=null && mstOtp1!=null && listOtpCount.size() < maxRetryOtp){
			/*
			 *  HANYA UNTUK DEVELOPMENT, HARUS DI TAKEOUT JIKA PRODUCTION
			 */
			if (mstOtp !=null && mstOtp1!=null && "7613".equals(mstOtp.getOtpCode())) {
				if(mstOtp1.getOtpExpiry().compareTo(new Date())<=0) {
					return "Kode OTP salah, silakan coba lagi.";
				}

				mstOtp1.setIsVerified(true);
				otpRepo.save(mstOtp1);

				otpRepo.updateAllVerificationByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));

//				if(mstOtp1.getEncryptionStatus()!=null) {
					// otpRepo.updateAllVerificationByPhoneNumber(AES256Utils.encrypt(mstOtp1.getPhoneNumber()));
//				}else {
//					otpRepo.updateAllVerificationByPhoneNumber(mstOtp1.getPhoneNumber());
//				}
				return "success";	
			}
		}
		/*
		 * END
		 */

		if (listOtpCount.size() < maxRetryOtp) {
			//"invalidotp";
			return "Kode OTP salah sini, silakan coba lagi.";
		} else {
			mstOtp1.setOtpExpiry(mstOtp1.getCreatedDate());
			otpRepo.save(mstOtp1);
			List<MstOtp> listMstOtp = new ArrayList();
//			if(mstOtp.getEncryptionStatus()!=null) {
				listMstOtp = otpRepo.findByExpiredDate(AES256Utils.encrypt(mstOtp.getPhoneNumber()), new Date(), minutesParam);
//			}else {
//				listMstOtp = otpRepo.findByExpiredDate(mstOtp.getPhoneNumber(), new Date(), minutesParam);
//			}

			// Cek apakah belum melebihi max resend otp parameter
			if (listMstOtp != null && (listMstOtp.size()==0 || (listMstOtp.size() <= maxResendOtp))) {
				if(listOtpCount.size() > 3) {
					//"max retry"
					return "Silakan coba beberapa saat lagi.";
				}else {
					//"expired otp"
					return "Kode OTP salah, silakan coba lagi.";
				}
			} else {
				//"max retry"
				return "Silakan coba beberapa saat lagi.";
			}
		}
	}


	@Override
	public List<MstOtp> findAllOtp() {
		// TODO Auto-generated method stub
		return otpRepo.findOtpByNotVerify();
	}
	@Override
	public String resend(MstOtp mstOtp) {
		// TODO Auto-generated method stub
//		SysParam sysParamBlockOtp = sysParamRepo.findByValueType("otp_expired");
//		int blockOtp = 30;
//		if (sysParamBlockOtp != null && !"".equals(sysParamBlockOtp.getValueName())) {
//			try {
//				blockOtp = Integer.parseInt(sysParamBlockOtp.getValueName());
//			}catch (NumberFormatException nfe) {
//				nfe.printStackTrace();
//				blockOtp = 30;
//			}
//			String minutesParam = blockOtp + " minute";
//			SysParam sysParamMaxResendOtp = sysParamRepo.findByValueType("otp_limit");
//			int maxResendOtp = 3;
//			if (sysParamMaxResendOtp != null && !"".equals(sysParamMaxResendOtp.getValueName())) {
//				try {
//					maxResendOtp = Integer.parseInt(sysParamMaxResendOtp.getValueName());
//				} catch (NumberFormatException nfe) {
//					nfe.printStackTrace();
//					maxResendOtp = 3;
//				}
//			}
//
//			MstUserMobile user = userMobileRepo.getOneByPhoneNumber(mstOtp.getPhoneNumber());
//			if(user != null) {
//				if("LOCKED".equals(user.getPinLockStatus()) && user.getPinLockDateTime() != null && user.getPinLockDateTime().compareTo(new Date())>0){
//					return "failedlocked";
//				}
//			}
//
//			System.out.println("##############################"+maxResendOtp);
//
//			List<MstOtp> listOtp = otpRepo.findByExpiredDate(mstOtp.getPhoneNumber(), new Date(), minutesParam);
//			if (listOtp != null && (listOtp.size()==0 || (listOtp.size() <= maxResendOtp-1))) {
//
//				String otpSuccess = "";
//
//				MstOtp mstOtp1 = otpRepo.findByPhoneNumber(mstOtp.getPhoneNumber());
//				List<MstOtpCount> listOtpCount = otpCountRepo.findByMstOtp(mstOtp1);
//				if(listOtp.size() > 0 && listOtpCount.size() < 3) {
//					//"max retry"
//					otpSuccess = "failed";
//				}else {
//					mstOtp.setOtpCode(generateOtpCode());
//					mstOtp.setIsVerified(false);
//					Calendar date = Calendar.getInstance();
//					long t= date.getTimeInMillis();
//					Date tmpExpire = new Date(t + (Constant.OTP_EXPIRY_TIME * 60000));
//					mstOtp.setOtpExpiry(tmpExpire);
//
//					Map<String, Object> obj = OTPGateway.getInstance().sendOTP(mstOtp.getPhoneNumber(),  mstOtp.getOtpCode());
//
//					/*******
//					 * 	FOR DEVELOPMENT ONLY - REMOVE THIS WHEN PRODUCTION
//					 */
//
//
//					if(obj.get("status") != null && "Insufficient Balance".equals(obj.get("status"))) {
//						mstOtp.setOtpCode("7613");
//						otpRepo.save(mstOtp);
//						otpSuccess = "insufficient";
//					}else if("Success".equals(obj.get("status"))) {
//						MstOtp otp = otpRepo.save(mstOtp);
//
//						for (MstOtp otp2 : listOtp) {
//							System.out.println("#################" + otp2.getId());
//							otp2.setOtpExpiry(otp.getOtpExpiry());
//						}
//						otpRepo.saveAll(listOtp);
//
//						otpSuccess = "success";
//					}else {
//						otpSuccess = "failed";
//					}
//					/*******
//					 * 	END
//					 */
//				}
//
//				return otpSuccess;
//			}else {
//				return "failed2";
//			}
//		}
//		return "failed2";
		SysParam sysParamBlockOtp = sysParamRepo.findByValueType("otp_expired");
		int blockOtp = 30;
		if (sysParamBlockOtp != null && !"".equals(sysParamBlockOtp.getValueName())) {
			try {
				blockOtp = Integer.parseInt(sysParamBlockOtp.getValueName());
			}catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				blockOtp = 30;
			}
			String minutesParam = blockOtp + " minute";
			SysParam sysParamMaxResendOtp = sysParamRepo.findByValueType("otp_limit");
			int maxResendOtp = 3;
			if (sysParamMaxResendOtp != null && !"".equals(sysParamMaxResendOtp.getValueName())) {
				try {
					maxResendOtp = Integer.parseInt(sysParamMaxResendOtp.getValueName());
				} catch (NumberFormatException nfe) {
					nfe.printStackTrace();
					maxResendOtp = 3;
				}
			}

			System.out.println("##############################"+maxResendOtp);
			
			MstUserMobile user = new MstUserMobile();
//			if(mstOtp.getEncryptionStatus()!=null) {
				user = userMobileRepo.getOneByPhoneNumber(mstOtp.getPhoneNumber());
//			}else {
//				user = userMobileRepo.getOneByPhoneNumber(mstOtp.getPhoneNumber());
//			}
			if(user != null) {
				if("LOCKED".equals(user.getPinLockStatus()) && user.getPinLockDateTime() != null && user.getPinLockDateTime().compareTo(new Date())>0){
					return "failedlocked";
				}
			}

			List<MstOtp> listOtp = new ArrayList();
//			if(mstOtp.getEncryptionStatus()!=null) {
				listOtp = otpRepo.findByExpiredDate(mstOtp.getPhoneNumber(), new Date(), minutesParam);
//			}else {
//				listOtp = otpRepo.findByExpiredDate(mstOtp.getPhoneNumber(), new Date(), minutesParam);
//			}
			if (listOtp != null && (listOtp.size()==0 || (listOtp.size() <= maxResendOtp-1))) {

				String otpSuccess = "";

				MstOtp mstOtp1 = new MstOtp();
//				if(mstOtp.getEncryptionStatus()!=null) {
					mstOtp1 = otpRepo.findByPhoneNumber(mstOtp.getPhoneNumber());
//				}else {
//					mstOtp1 = otpRepo.findByPhoneNumber(mstOtp.getPhoneNumber());
//				}
				List<MstOtpCount> listOtpCount = otpCountRepo.findByMstOtp(mstOtp1);
				if(listOtp.size() > 0 && listOtpCount.size() > 3) {
					//"max retry"
					otpSuccess = "failed";
				}else {

					mstOtp.setOtpCode(generateOtpCode());
					mstOtp.setIsVerified(false);
					Calendar date = Calendar.getInstance();
					long t= date.getTimeInMillis();
					Date tmpExpire = new Date(t + (Constant.OTP_EXPIRY_TIME * 60000));
					mstOtp.setOtpExpiry(tmpExpire);

					Map<String, Object> obj = OTPGateway.getInstance().sendOTP(mstOtp.getPhoneNumber(),  mstOtp.getOtpCode());

					/*******
					 * 	FOR DEVELOPMENT ONLY - REMOVE THIS WHEN PRODUCTION
					 */

					if(obj.get("status") != null && "Insufficient Balance".equals(obj.get("status"))) {
						mstOtp.setOtpCode("7613");
						otpRepo.save(mstOtp);
						otpSuccess = "insufficient";
					}else if("Success".equals(obj.get("status"))) {
						MstOtp otp = otpRepo.save(mstOtp);

						for (MstOtp otp2 : listOtp) {
							System.out.println("#################" + otp2.getId());
							otp2.setOtpExpiry(otp.getOtpExpiry());
						}
						otpRepo.saveAll(listOtp);

						otpSuccess = "success";
					}else {
						otpSuccess = "failed";
					}
					/*******
					 * 	END
					 */
				}

				return otpSuccess;
			}else {
				return "failed2";
			}
		}
		return "failed2";

	}
	@Override
	public String resendGeneral(MstOtp mstOtp) {
		// TODO Auto-generated method stub
		SysParam sysParamBlockOtp = sysParamRepo.findByValueType("otp_expired");
		int blockOtp = 30;
		if (sysParamBlockOtp != null && !"".equals(sysParamBlockOtp.getValueName())) {
			try {
				blockOtp = Integer.parseInt(sysParamBlockOtp.getValueName());
			}catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				blockOtp = 30;
			}
			String minutesParam = blockOtp + " minute";
			SysParam sysParamMaxResendOtp = sysParamRepo.findByValueType("otp_limit");
			int maxResendOtp = 3;
			if (sysParamMaxResendOtp != null && !"".equals(sysParamMaxResendOtp.getValueName())) {
				try {
					maxResendOtp = Integer.parseInt(sysParamMaxResendOtp.getValueName());
				} catch (NumberFormatException nfe) {
					nfe.printStackTrace();
					maxResendOtp = 3;
				}
			}

			System.out.println("##############################"+maxResendOtp);

			MstUserMobile user = new MstUserMobile();
//			if(mstOtp.getEncryptionStatus()!=null) {
				user = userMobileRepo.getOneByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//			}else {
//				user = userMobileRepo.getOneByPhoneNumber(mstOtp.getPhoneNumber());
//			}
			if(user != null) {
				if("LOCKED".equals(user.getPinLockStatus()) && user.getPinLockDateTime() != null && user.getPinLockDateTime().compareTo(new Date())>0){
					return "failedlocked";
				}
			}

			List<MstOtp> listOtp = new ArrayList();
//			if(mstOtp.getEncryptionStatus()!=null) {
				listOtp = otpRepo.findByExpiredDate(AES256Utils.encrypt(mstOtp.getPhoneNumber()), new Date(), minutesParam);
//			}else {
//				listOtp = otpRepo.findByExpiredDate(mstOtp.getPhoneNumber(), new Date(), minutesParam);
//			}
			System.out.println("LIST OTP = " + listOtp.size());
			if (listOtp != null && (listOtp.size()==0 || (listOtp.size() <= maxResendOtp-1))) {

				String otpSuccess = "";
				
				MstOtp mstOtp1 = new MstOtp();
//				if(mstOtp.getEncryptionStatus()!=null) {
					mstOtp1 = otpRepo.findByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//				}else {
//					mstOtp1 = otpRepo.findByPhoneNumber(mstOtp.getPhoneNumber());
//				}
				List<MstOtpCount> listOtpCount = otpCountRepo.findByMstOtp(mstOtp1);
				if(listOtp.size() > 0 && listOtpCount.size() > 3) {
					//"max retry"
					otpSuccess = "failed";
				}else {

					mstOtp.setOtpCode(generateOtpCode());
					mstOtp.setIsVerified(false);
					Calendar date = Calendar.getInstance();
					long t= date.getTimeInMillis();
					Date tmpExpire = new Date(t + (Constant.OTP_EXPIRY_TIME * 60000));
					mstOtp.setOtpExpiry(tmpExpire);

					Map<String, Object> obj = OTPGateway.getInstance().sendOTP(mstOtp.getPhoneNumber(),  mstOtp.getOtpCode());

					/*******
					 * 	FOR DEVELOPMENT ONLY - REMOVE THIS WHEN PRODUCTION
					 */

					if(obj.get("status") != null && "Insufficient Balance".equals(obj.get("status"))) {
						mstOtp.setOtpCode("7613");
						otpRepo.save(mstOtp);
						otpSuccess = "insufficient";
					}else if("Success".equals(obj.get("status"))) {
						System.out.println("Belum save otp sukses");
						MstOtp otp = otpRepo.save(mstOtp);
						System.out.println("Masuk sukses send otp");
						
						for (MstOtp otp2 : listOtp) {
							System.out.println("#################" + otp2.getId());
							otp2.setOtpExpiry(otp.getOtpExpiry());
						}
						otpRepo.saveAll(listOtp);

						otpSuccess = "success";
					}else {
						otpSuccess = "failed";
					}
					/*******
					 * 	END
					 */
				}

				return otpSuccess;
			}else {
				return "failed2";
			}
		}
		return "failed2";
	}
	@Override
	public List<MstOtp> findByExpiredDate(String value1, Date value2, String value3) {
		// TODO Auto-generated method stub
		return otpRepo.findByExpiredDate(value1, value2, value3);
	}
	@Override
	public MstOtp findByPhoneNumber(String phoneNumber) {
		// TODO Auto-generated method stub
		return otpRepo.findByPhoneNumber(phoneNumber);
	}
	@Override
	public void updateAllVerificationByPhoneNumber(String phoneNumber) {
		otpRepo.updateAllVerificationByPhoneNumber(phoneNumber);
	}
}
