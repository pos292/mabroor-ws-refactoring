package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPengajuanAsuransiMobilPerluasan;
import com.mabroor.ws.repository.PengajuanAsuransiMobilPerluasanRepository;
import com.mabroor.ws.service.PengajuanAsuransiMobilPerluasanService;

@Service
public class PengajuanAsuransiMobilPerluasanServiceImpl implements PengajuanAsuransiMobilPerluasanService {

	@Autowired
	private PengajuanAsuransiMobilPerluasanRepository mobilLuasRepo;
	
	@Override
	public List<MstPengajuanAsuransiMobilPerluasan> getListPerluasanMobil() {
		// TODO Auto-generated method stub
		return mobilLuasRepo.getListPerluasanMobil();
	}

	@Override
	public List<MstPengajuanAsuransiMobilPerluasan> findByPerluasanId(Long asuransiMobilId) {
		// TODO Auto-generated method stub
		return mobilLuasRepo.findByPerluasanId(asuransiMobilId);
	}

	@Override
	public MstPengajuanAsuransiMobilPerluasan save(MstPengajuanAsuransiMobilPerluasan mstMobil) {
		// TODO Auto-generated method stub
		return mobilLuasRepo.save(mstMobil);
	}

	@Override
	public MstPengajuanAsuransiMobilPerluasan update(MstPengajuanAsuransiMobilPerluasan mstMobil) {
		// TODO Auto-generated method stub
		return mobilLuasRepo.save(mstMobil);
	}

}
