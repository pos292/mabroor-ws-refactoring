package com.mabroor.ws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mabroor.ws.entity.MstPengajuanCar;
import com.mabroor.ws.repository.CarRoundRobinRepository;
import com.mabroor.ws.service.CarRoundRobinService;


@Service
public class CarRoundRobinServiceImpl implements CarRoundRobinService {

	@Autowired
	private CarRoundRobinRepository carRoundRobinRepository;
	
	@Override
	public MstPengajuanCar findAllByStatus() {
		// TODO Auto-generated method stub
		return carRoundRobinRepository.findAllByStatus();
	}

	

}
