package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPosisi;
import com.mabroor.ws.repository.PosisiRepository;
import com.mabroor.ws.service.PosisiService;

@Service
public class PosisiServiceImpl implements PosisiService  {

	@Autowired
	private PosisiRepository posisiRepo;
	
	@Override
	public List<MstPosisi> findAllByStatus() {
		// TODO Auto-generated method stub
		return posisiRepo.findAllByStatus();
	}

	@Override
	public List<MstPosisi> getListPosisi() {
		// TODO Auto-generated method stub
		return posisiRepo.getListPosisi();
	}

	@Override
	public Optional<MstPosisi> findById(Long id) {
		// TODO Auto-generated method stub
		return posisiRepo.findById(id);
	}

}
