package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstSumberDanaPembayaran;
import com.mabroor.ws.repository.SumberDanaPembayaranRepository;
import com.mabroor.ws.service.SumberDanaPembayaranService;

@Service
public class SumberDanaPembayaranServiceImpl implements SumberDanaPembayaranService {

	@Autowired
	SumberDanaPembayaranRepository danaRepo;
	
	@Override
	public List<MstSumberDanaPembayaran> findAllByStatus() {
		// TODO Auto-generated method stub
		return danaRepo.findAllByStatus();
	}

	@Override
	public List<MstSumberDanaPembayaran> getListDanaPembayaran() {
		// TODO Auto-generated method stub
		return danaRepo.getListDanaPembayaran();
	}

	@Override
	public Optional<MstSumberDanaPembayaran> findById(Long id) {
		// TODO Auto-generated method stub
		return danaRepo.findById(id);
	}

}
