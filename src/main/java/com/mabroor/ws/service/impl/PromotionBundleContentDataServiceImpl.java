package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPromotionBundle;
import com.mabroor.ws.entity.MstPromotionBundleContentData;
import com.mabroor.ws.repository.PromotionBundleContentDataRepository;
import com.mabroor.ws.service.PromotionBundleContentDataService;

@Service
public class PromotionBundleContentDataServiceImpl implements PromotionBundleContentDataService {
	
	@Autowired
	private PromotionBundleContentDataRepository bundleRepo;

	@Override
	public Optional<MstPromotionBundleContentData> getListDetailPromoData() {
		// TODO Auto-generated method stub
		return bundleRepo.getListDetailPromoData();
	}

//	@Override
//	public MstPromotionBundle getDataDetailPromoData(Long id) {
//		// TODO Auto-generated method stub
//		return bundleRepo.getDataDetailPromoData(id);
//	}

	
	
//	@Override
//	public Optional<MstPromotionBundleContentData> getDataDetailPromoData() {
//		// TODO Auto-generated method stub
//		return bundleRepo.getDataDetailPromoData();
//	}

}
