package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstKecamatan;
import com.mabroor.ws.repository.KecamatanRepository;
import com.mabroor.ws.service.KecamatanService;

@Service
public class KecamatanServiceImpl implements KecamatanService {

	@Autowired
	private KecamatanRepository camatRepo;
	
	@Override
	public List<MstKecamatan> findAllByStatus() {
		// TODO Auto-generated method stub
		return camatRepo.findAllByStatus();
	}

	@Override
	public List<MstKecamatan> getListKecamatan() {
		// TODO Auto-generated method stub
		return camatRepo.getListKecamatan();
	}

	@Override
	public List<MstKecamatan> findByKabupatenId(Long id) {
		// TODO Auto-generated method stub
		return camatRepo.findByKabupatenId(id);
	}

	@Override
	public Optional<MstKecamatan> findById(Long id) {
		// TODO Auto-generated method stub
		return camatRepo.findById(id);
	}

}
