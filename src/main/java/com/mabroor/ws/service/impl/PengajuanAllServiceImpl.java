package com.mabroor.ws.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.mabroor.ws.dto.PengajuanAllCustomDto;
import com.mabroor.ws.service.PengajuanAllService;

@Service
public class PengajuanAllServiceImpl implements PengajuanAllService {
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<PengajuanAllCustomDto> findAllByUserIdPagging(Long userId, Long limit, Long page) {
		List<PengajuanAllCustomDto> arrayList = new ArrayList<PengajuanAllCustomDto>();
		
		Query query = entityManager.createNamedQuery("pengajuanAllByUserIdPaggingQuery");
		query.setParameter(1, userId);
		query.setParameter(2, limit);
		query.setParameter(3, page);
		arrayList = (List<PengajuanAllCustomDto>) query.getResultList();
		
		return arrayList;
	}

	@Override
	public List<PengajuanAllCustomDto> findLeadsByLeadsId(Long userId, Long leadsId, String queryType) {
		List<PengajuanAllCustomDto> arrayList = new ArrayList<PengajuanAllCustomDto>();
		
		Query query = entityManager.createNamedQuery(queryType);
		query.setParameter(1, userId);
		query.setParameter(2, leadsId);
		arrayList = (List<PengajuanAllCustomDto>) query.getResultList();
		
		return arrayList;
	}
}
