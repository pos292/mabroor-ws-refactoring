package com.mabroor.ws.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPembiayaanQurban;
import com.mabroor.ws.repository.PembiayaanQurbanRepository;
import com.mabroor.ws.service.PembiayaanQurbanService;

@Service
public class PembiayaanQurbanServiceImpl implements PembiayaanQurbanService {
	
	@Autowired
	private PembiayaanQurbanRepository qurbanRep;
	
	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Override
	public List<MstPembiayaanQurban> getList() {
		// TODO Auto-generated method stub
		List<MstPembiayaanQurban> qurban = qurbanRep.getList();
		if(!qurban.isEmpty()) {
			for(MstPembiayaanQurban a : qurban) {
				em.detach(a);
				a.setIcon(this.imageBaseUrl + "pembiayaanQurban/" + a.getIcon());
			}
		}
		return qurban;
	}

	@Override
	public List<MstPembiayaanQurban> getListByNamePartner(String namePartner) {
		// TODO Auto-generated method stub
		List<MstPembiayaanQurban> qurban = qurbanRep.getListByNamePartner(namePartner);
		if(!qurban.isEmpty()) {
			for(MstPembiayaanQurban a : qurban) {
				em.detach(a);
				a.setIcon(this.imageBaseUrl + "pembiayaanQurban/" + a.getIcon());
			}
		}
		return qurban;
	}
	

}
