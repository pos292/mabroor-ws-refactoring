package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstApplication;
import com.mabroor.ws.repository.ApplicationRepository;
import com.mabroor.ws.service.ApplicationService;

@Service
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	ApplicationRepository appRepo;
	
	@Override
	public MstApplication save(MstApplication mstApp) {
		// TODO Auto-generated method stub
		return appRepo.save(mstApp);
	}

	@Override
	public List<MstApplication> findByLeadsIdAndLeadsType(Long leadsId, String leadsType) {
		// TODO Auto-generated method stub
		return appRepo.findByLeadsIdAndLeadsType(leadsId, leadsType);
	}

	@Override
	public MstApplication findByCarBaru(Long leadsId) {
		// TODO Auto-generated method stub
		return appRepo.findByCarBaru(leadsId);
	}

	@Override
	public List<MstApplication> getList() {
		// TODO Auto-generated method stub
		return appRepo.getList();
	}

}
