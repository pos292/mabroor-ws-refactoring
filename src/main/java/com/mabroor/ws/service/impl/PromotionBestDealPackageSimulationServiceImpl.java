package com.mabroor.ws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPromotionBestDealPackageSimulation;
import com.mabroor.ws.repository.PromotionBestDealPackageSimulationRepository;
import com.mabroor.ws.repository.PromotionSimulationRateRepository;
import com.mabroor.ws.service.PromotionBestDealPackageSimulationService;
import com.mabroor.ws.service.PromotionSimulationRateService;

@Service
public class PromotionBestDealPackageSimulationServiceImpl implements PromotionBestDealPackageSimulationService {
	
	@Autowired
	private PromotionBestDealPackageSimulationRepository promoBestDealSimulRepo;

	@Override
	public MstPromotionBestDealPackageSimulation getPromotionBestDealByPromoId(Long id) {
		return promoBestDealSimulRepo.findAllByPromotion(id);
	}

}
