package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPengajuanGoldFinancing;
import com.mabroor.ws.repository.PengajuanGoldFinancingRepository;
import com.mabroor.ws.service.PengajuanGoldFinancingService;

@Service
public class PengajuanGoldFinancingServiceImpl implements PengajuanGoldFinancingService {
	
	@Autowired
	private PengajuanGoldFinancingRepository goldRepo;

	@Override
	public MstPengajuanGoldFinancing save(MstPengajuanGoldFinancing mstGold) {
		// TODO Auto-generated method stub
		return goldRepo.save(mstGold);
	}

	@Override
	public MstPengajuanGoldFinancing update(MstPengajuanGoldFinancing mstGold) {
		// TODO Auto-generated method stub
		return goldRepo.save(mstGold);
	}

	@Override
	public List<MstPengajuanGoldFinancing> findByPhone(String phone) {
		// TODO Auto-generated method stub
		return goldRepo.findByPhone(phone);
	}

}
