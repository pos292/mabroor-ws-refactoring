package com.mabroor.ws.service.impl;

import java.util.List;

import com.mabroor.ws.entity.MstPengajuanAccountBSI;
import com.mabroor.ws.repository.PengajuanAccountBSIRepository;
import com.mabroor.ws.service.PengajuanAccountBSIService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PengajuanAccountBSIServiceImpl implements PengajuanAccountBSIService {
    
    @Autowired
	private PengajuanAccountBSIRepository bsiRepo;

    @Override
    public MstPengajuanAccountBSI save(MstPengajuanAccountBSI pengajuan) {
        return bsiRepo.save(pengajuan);
    }

    @Override
    public MstPengajuanAccountBSI update(MstPengajuanAccountBSI pengajuan) {
        return bsiRepo.save(pengajuan);
    }

    @Override
    public MstPengajuanAccountBSI findByUserAndCreateDateNow(Long userid) {
        return bsiRepo.findByUserAndCreateDateNow(userid);
    }

    @Override
    public List<MstPengajuanAccountBSI> findByUserId(Long userid) {
        return bsiRepo.findByUserId(userid);
    }

    
}
