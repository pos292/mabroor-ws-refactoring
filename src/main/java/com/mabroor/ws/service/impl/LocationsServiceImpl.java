package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstLocations;
import com.mabroor.ws.repository.LocationsRepository;
import com.mabroor.ws.service.LocationsService;

@Service
public class LocationsServiceImpl implements LocationsService {
	@Autowired
	private LocationsRepository locationsRepository;

	@Override
	public List<MstLocations> findAllByStatus() {
		return locationsRepository.getListLocations();
	}

	@Override
	public Optional<MstLocations> findById(Long id) {
		return locationsRepository.findById(id);
	}

	@Override
	public List<MstLocations> getListBtdAvailable() {
		// TODO Auto-generated method stub
		return locationsRepository.getListBtdAvailable();
	}

}
