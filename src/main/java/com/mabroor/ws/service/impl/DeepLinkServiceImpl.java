package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstDeepLink;
import com.mabroor.ws.repository.DeepLinkRepository;
import com.mabroor.ws.service.DeepLinkService;

@Service
public class DeepLinkServiceImpl implements DeepLinkService {

	@Autowired
	private DeepLinkRepository linkRepo;
	
	@Override
	public List<MstDeepLink> getList() {
		// TODO Auto-generated method stub
		return linkRepo.getList();
	}

	@Override
	public List<MstDeepLink> getListLinkBsi() {
		// TODO Auto-generated method stub
		return linkRepo.getListLinkBsi();
	}

}
