package com.mabroor.ws.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstApplicationTransaction;
import com.mabroor.ws.repository.ApplicationTransactionRepository;
import com.mabroor.ws.service.ApplicationTransactionService;

@Service
public class ApplicationTransactionServiceImpl implements ApplicationTransactionService {

	@Autowired
	private ApplicationTransactionRepository trxRepo;
	
	@Override
	public MstApplicationTransaction save(MstApplicationTransaction trxApp) {
		// TODO Auto-generated method stub
		return trxRepo.save(trxApp);
	}

	@Override
	public void deleteAppId(Long Id) {
		// TODO Auto-generated method stub
		trxRepo.deleteAppId(Id);
	}

}
