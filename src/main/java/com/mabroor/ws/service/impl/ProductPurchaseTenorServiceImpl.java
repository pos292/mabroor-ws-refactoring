package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstProductPurchaseTenor;
import com.mabroor.ws.repository.ProductPurchaseTenorRepository;
import com.mabroor.ws.service.ProductPurchaseTenorService;

@Service
public class ProductPurchaseTenorServiceImpl implements ProductPurchaseTenorService {
	
	@Autowired
	private ProductPurchaseTenorRepository purchaseTenorRepo;
	
	@Override
	public List<MstProductPurchaseTenor> getList() {
		// TODO Auto-generated method stub
		return purchaseTenorRepo.getList();
	}

}
