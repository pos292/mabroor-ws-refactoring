package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstInsuranceQuestion;
import com.mabroor.ws.repository.InsuranceQuestionRepository;
import com.mabroor.ws.service.InsuranceQuestionService;

@Service
public class InsuranceQuestionServiceImpl implements InsuranceQuestionService {

	@Autowired
	private InsuranceQuestionRepository inscQuestRepo;
	
	@Override
	public List<MstInsuranceQuestion> getListInscQuest() {
		// TODO Auto-generated method stub
		return inscQuestRepo.getListInscQuest();
	}

	@Override
	public Optional<MstInsuranceQuestion> findById(Long id) {
		// TODO Auto-generated method stub
		return inscQuestRepo.findById(id);
	}

	@Override
	public List<MstInsuranceQuestion> getListInscQuestByInsuranceId(Long insuranceId) {
		// TODO Auto-generated method stub
		return inscQuestRepo.getListInscQuestByInsuranceId(insuranceId);
	}

}
