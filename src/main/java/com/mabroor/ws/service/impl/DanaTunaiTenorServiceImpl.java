package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstDanaTunaiTenor;
import com.mabroor.ws.repository.DanaTunaiTenorRepository;
import com.mabroor.ws.service.DanaTunaiTenorService;

@Service
public class DanaTunaiTenorServiceImpl implements DanaTunaiTenorService  {

	@Autowired
	private DanaTunaiTenorRepository tenorRepo; 
	
	@Override
	public List<MstDanaTunaiTenor> findAll() {
		// TODO Auto-generated method stub
		return tenorRepo.findAll();
	}

	@Override
	public List<MstDanaTunaiTenor> findByType(String type) {
		// TODO Auto-generated method stub
		return tenorRepo.findByType(type);
	}

	@Override
	public List<MstDanaTunaiTenor> findByProductType(String type, String product) {
		// TODO Auto-generated method stub
		return tenorRepo.findByProductType(type, product);
	}

}
