package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstOwnerRegion;
import com.mabroor.ws.repository.OwnerRegionRepository;
import com.mabroor.ws.service.OwnerRegionService;

@Service
public class OwnerRegionServiceImpl implements OwnerRegionService {

	@Autowired
	private OwnerRegionRepository regRepo;
	
	@Override
	public List<MstOwnerRegion> getList() {
		// TODO Auto-generated method stub
		return regRepo.getList();
	}

}
