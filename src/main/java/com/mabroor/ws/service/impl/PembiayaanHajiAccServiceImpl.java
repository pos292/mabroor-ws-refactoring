package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstHajiAccMaster;
import com.mabroor.ws.repository.PembiayaanHajiAccMasterRepository;
import com.mabroor.ws.service.PembiayaanHajiAccMasterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PembiayaanHajiAccServiceImpl implements PembiayaanHajiAccMasterService {

    @Autowired
	private PembiayaanHajiAccMasterRepository hajiAccRepo;

    @Override
    public List<MstHajiAccMaster> findAll() {
        return hajiAccRepo.findAll();
    }

    @Override
    public List<MstHajiAccMaster> getJumlahPesertaAcc() {
        return hajiAccRepo.getJumlahPesertaAcc();
    }

    @Override
    public List<MstHajiAccMaster> getTenorAcc() {
        return hajiAccRepo.getTenorAcc();
    }

    @Override
    public List<MstHajiAccMaster> getProductAcc() {
        return hajiAccRepo.getProductAcc();
    }
}
