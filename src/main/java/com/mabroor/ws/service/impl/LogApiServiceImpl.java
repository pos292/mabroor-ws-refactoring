package com.mabroor.ws.service.impl;

import com.mabroor.ws.dto.PembiayaanUmrohDto;
import com.mabroor.ws.dto.PembiayaanUmrohRespResultDto;
import com.mabroor.ws.entity.MstLogApi;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.repository.LogApiRepository;
import com.mabroor.ws.service.LogApiService;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Scanner;

@Service
@Slf4j
public class LogApiServiceImpl implements LogApiService {

    @Autowired
    LogApiRepository logApiRepository;



    @Override
    public MstLogApi saveUmrohLog(HttpServletRequest httpServletRequest, PembiayaanUmrohDto pembiayaanUmrohDto, PembiayaanUmrohRespResultDto pembiayaanUmrohRespResultDto, MstUserMobile mstUserMobile) throws IOException {
        MstLogApi mstLogApi = null;
        httpServletRequest.getRequestURI();
        try {
            mstLogApi =  MstLogApi.builder()
                    .request(convertObjectAsJson(pembiayaanUmrohDto))
                    .response(convertObjectAsJson(pembiayaanUmrohRespResultDto))
                    .path(httpServletRequest.getRequestURI())
                    .createdBy(mstUserMobile.getName())
                    .build();

            mstLogApi = logApiRepository.save(mstLogApi);
        }catch (IOException e){
            throw new IOException("Error Couldn't Save Log");
        }
        return mstLogApi;
    }

    static String convertObjectAsJson(Object object) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
    static String extractPostRequestBody(HttpServletRequest request) throws IOException {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            Scanner s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
        return "";
    }
}
