package com.mabroor.ws.service.impl;


import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.exception.AuthorizationException;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.service.ValidationService;
import com.mabroor.ws.util.JWTGenerator;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ValidationServiceImpl implements ValidationService {
    @Autowired
    private UserMobileService userMobileServ;

    @Override
    public MstUserMobile userLoginValid(String token) throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        if (token == null){
            throw new AuthorizationException("authToken cannot be null");
        }
        try {
            Claims claims = JWTGenerator.getInstance().decodeJWT(token);
            if (Objects.isNull(claims)) {
                throw new AuthorizationException("Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
            }else{
                Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
                if (!mstUserMobileOpt.isPresent()) {
                    throw new AuthorizationException("Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
                }else{
                    MstUserMobile mstUserMobile = mstUserMobileOpt.get();
                    if(!claims.getId().equals("" + mstUserMobile.getId()) && !token.equals(mstUserMobile.getToken())){
                        throw new AuthorizationException("Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
                    }
                    return mstUserMobile;
                }
            }
        }catch (Exception e){
            throw new AuthorizationException(e.getMessage());
        }
    }


}
