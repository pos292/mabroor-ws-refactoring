package com.mabroor.ws.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.dto.CalculateMrpMobilReqDto;
import com.mabroor.ws.entity.MstDanaTunaiMobilMaster;
import com.mabroor.ws.repository.DanaTunaiMobilMasterRepository;
import com.mabroor.ws.service.DanaTunaiMobilMasterService;

@Service
public class DanaTunaiMobilMasterServiceImpl implements DanaTunaiMobilMasterService  {

	@Autowired
	private DanaTunaiMobilMasterRepository danaMobilRepo;

	@Override
	public List<MstDanaTunaiMobilMaster> findAll() {
		// TODO Auto-generated method stub
		return danaMobilRepo.findAll();
	}

	@Override
	public List<MstDanaTunaiMobilMaster> findByBrandCar() {
		// TODO Auto-generated method stub
		return danaMobilRepo.findByBrandCar();
	}

	@Override
	public List<MstDanaTunaiMobilMaster> findByTypeCar(String brand) {
		// TODO Auto-generated method stub
		return danaMobilRepo.findByTypeCar(brand);
	}

	@Override
	public List<MstDanaTunaiMobilMaster> findByModelCar(String type) {
		// TODO Auto-generated method stub
		return danaMobilRepo.findByModelCar(type);
	}

	@Override
	public List<MstDanaTunaiMobilMaster> findByYearCar(String model) {
		// TODO Auto-generated method stub
		return danaMobilRepo.findByYearCar(model);
	}

	@Override
	public Map<String, Object> findByCalculateMrp(CalculateMrpMobilReqDto calculateMrpMobil) {
		Map<String, Object> objtemp = new HashMap<>();
		Optional<MstDanaTunaiMobilMaster> calculateMrp = danaMobilRepo.findByCalculateMrp(calculateMrpMobil.getBrand(), calculateMrpMobil.getType(), calculateMrpMobil.getModel(), calculateMrpMobil.getYear());
		if(calculateMrp.isPresent()) {
			objtemp.put("mrp", calculateMrp.get().getPrice().multiply(new BigDecimal(0.8)).setScale(0, RoundingMode.HALF_UP));
		} else {
			objtemp.put("mrp", 0);
		}
		return objtemp;
	}

	@Override
	public List<MstDanaTunaiMobilMaster> findByYearAndModelTypeCar(String brand, String type, String model) {
		// TODO Auto-generated method stub
		return danaMobilRepo.findByYearAndModelTypeCar(brand, type, model);
	}

	@Override
	public List<MstDanaTunaiMobilMaster> findByBrandTypeModelCar(String brand, String type) {
		return danaMobilRepo.findByBrandTypeModelCar(brand, type);
	}

	@Override
	public MstDanaTunaiMobilMaster getJaminanMobilByBtmy(String brand, String type, String model, String year) {
		// TODO Auto-generated method stub
		return danaMobilRepo.getJaminanMobilByBtmy(brand, type, model, year);
	}

}
