package com.mabroor.ws.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstNews;
import com.mabroor.ws.repository.NewsRepository;
import com.mabroor.ws.service.NewsService;


@Service
public class NewsServiceImpl implements NewsService{
	

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Autowired
	private NewsRepository newsRepository;

	@Override
	public List<MstNews> findAllByStatus() {
		return newsRepository.findAllByStatus();
	}

	@Override
	public List<MstNews> findByMainProductIdLimit(Long idMainProduct) {
		List<MstNews> news = newsRepository.findByMainProductIdLimit(idMainProduct);
		if(!news.isEmpty()) {
			for(MstNews a : news) {
				a.setImage(this.imageBaseUrl + "news/" + a.getImage());
			}
		}
		return news;
	}

	@Override
	public List<MstNews> findByMainProductAll() {
		List<MstNews> news = newsRepository.findAllLatest();
		if(!news.isEmpty()) {
			for(MstNews a : news) {
				em.detach(a);
				a.setImage(this.imageBaseUrl + "news/" + a.getImage());
			}
		}
		return news;
	}

	@Override
	public List<MstNews> findByMainProductId(Long idMainProduct) {
		List<MstNews> news = newsRepository.findByMainProductId(idMainProduct);
		if(!news.isEmpty()) {
			for(MstNews a : news) {
				a.setImage(this.imageBaseUrl + "news/" + a.getImage());
			}
		}
		return news;
	}

	@Override
	public List<Map<String, String>> findActiveSubCategory() {
		List<Map<String, String>> listSubCategory = new ArrayList<>();
		
		List<MstNews> listNews = newsRepository.findActiveSubCategory();
		
		Map<String, String> tmpMap = new HashMap<>();
		
		tmpMap.put("title", "Semua");
		
		listSubCategory.add(tmpMap);
		for(MstNews row: listNews) {
			tmpMap = new HashMap<>();
			tmpMap.put("title", row.getSubCategory());
			listSubCategory.add(tmpMap);
		}
		
		return listSubCategory;
	}

	@Override
	public List<MstNews> findBySubCategoryName(String subcategoryName) {
		List<MstNews> news = null;
		if("Semua".equalsIgnoreCase(subcategoryName)) {
			news = newsRepository.findByMainProductLimited();
		}else {
			news = newsRepository.findBySubCategoryName(subcategoryName);
		}
		if(!news.isEmpty()) {
			for(MstNews a : news) {
				em.detach(a);
				a.setImage(this.imageBaseUrl + "news/" + a.getImage());
			}
		}
		return news;
	}

	@Override
	public List<MstNews> findBySubCategoryNameAll(String subcategoryName) {
		List<MstNews> news = null;
		if("Semua".equalsIgnoreCase(subcategoryName)) {
			news = newsRepository.findAllLatest();
		}else {
			news = newsRepository.findBySubCategoryNameAll(subcategoryName);
		}
		if(!news.isEmpty()) {
			for(MstNews a : news) {
				em.detach(a);
				a.setImage(this.imageBaseUrl + "news/" + a.getImage());
			}
		}
		return news;
	}
	
	@Override
	public List<Map<String, String>> findAllSubCategory() {
		List<Map<String, String>> listSubCategory = new ArrayList<>();
		
		Map<String, String> tmpMap = new HashMap<>();
		
		tmpMap.put("title", "Semua");
		listSubCategory.add(tmpMap);
		
//		tmpMap = new HashMap<>();
//		tmpMap.put("title", "Finansial");
//		listSubCategory.add(tmpMap);
		
		tmpMap = new HashMap<>();
		tmpMap.put("title", "Islami");
		listSubCategory.add(tmpMap);
		
		tmpMap = new HashMap<>();
		tmpMap.put("title", "Keuangan");
		listSubCategory.add(tmpMap);
		
		tmpMap = new HashMap<>();
		tmpMap.put("title", "Otomotif");
		listSubCategory.add(tmpMap);
		
		tmpMap = new HashMap<>();
		tmpMap.put("title", "Review");
		listSubCategory.add(tmpMap);
		
		return listSubCategory;
	}

	@Override
	public Optional<MstNews> findById(Long id) {
		Optional<MstNews> tempNews = newsRepository.findById(id);
		if(tempNews.isPresent()) {
			tempNews.get().setImage(this.imageBaseUrl + "news/" + tempNews.get().getImage());
		}
		
		return tempNews;
	}

	@Override
	public List<MstNews> findByMainProductIdandSubCategoryName(Long idMainProduct, String subcategoryName) {
		List<MstNews> news = null;
		if("Semua".equals(subcategoryName)){
			news = newsRepository.findByMainProductId(idMainProduct);
		}else {
			news = newsRepository.findByMainProductIdandSubCategoryName(idMainProduct, subcategoryName);
		}
		if(!news.isEmpty()) {
			for(MstNews a : news) {
				a.setImage(this.imageBaseUrl + "news/" + a.getImage());
			}
		}
		return news;
	}

}
