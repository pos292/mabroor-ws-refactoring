package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstProductPurchase;
import com.mabroor.ws.repository.ProductPurchaseRepository;
import com.mabroor.ws.service.ProductPurchaseService;

@Service
public class ProductPurchaseServiceImpl implements ProductPurchaseService {

	@Autowired
	private ProductPurchaseRepository purchaseRepo;
	
	@Override
	public List<MstProductPurchase> getList() {
		return purchaseRepo.getList();
	}

}
