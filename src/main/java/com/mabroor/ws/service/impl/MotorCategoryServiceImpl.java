package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstMotorCategory;
import com.mabroor.ws.repository.MotorCategoryRepository;
import com.mabroor.ws.service.MotorCategoryService;

@Service
public class MotorCategoryServiceImpl implements MotorCategoryService {

	@Autowired
	private MotorCategoryRepository categoryRepo;
	
	@Override
	public List<MstMotorCategory> findAllByStatus() {
		// TODO Auto-generated method stub
		return categoryRepo.findAllByStatus();
	}

	@Override
	public List<MstMotorCategory> getListMotorCategory() {
		// TODO Auto-generated method stub
		return categoryRepo.getListMotorCategory();
	}

	@Override
	public List<MstMotorCategory> findAllByBrandId(Long id) {
		// TODO Auto-generated method stub
		return categoryRepo.findAllByBrandId(id);
	}

	@Override
	public Optional<MstMotorCategory> findById(Long id) {
		// TODO Auto-generated method stub
		return categoryRepo.findById(id);
	}

}
