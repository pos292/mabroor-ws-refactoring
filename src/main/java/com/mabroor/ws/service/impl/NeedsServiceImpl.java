package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstNeeds;
import com.mabroor.ws.repository.NeedsRepository;
import com.mabroor.ws.service.NeedsService;

@Service
public class NeedsServiceImpl implements NeedsService {

	@Autowired
	private NeedsRepository needRepo;
	
	@Override
	public List<MstNeeds> findAllByStatus() {
		// TODO Auto-generated method stub
		return needRepo.findAllByStatus();
	}

	@Override
	public List<MstNeeds> getListNeeds() {
		// TODO Auto-generated method stub
		return needRepo.getListNeeds();
	}

	@Override
	public Optional<MstNeeds> findById(Long id) {
		// TODO Auto-generated method stub
		return needRepo.findById(id);
	}

}
