package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstDp;
import com.mabroor.ws.repository.DpRepository;
import com.mabroor.ws.service.DpService;

@Service
public class DpServiceImpl implements DpService {

	@Autowired
	private DpRepository dpRepo;

	@Override
	public List<MstDp> getListDp() {
		// TODO Auto-generated method stub
		return dpRepo.getListDp();
	}

	@Override
	public Optional<MstDp> findById(Long id) {
		// TODO Auto-generated method stub
		return dpRepo.findById(id);
	}

	@Override
	public List<MstDp> getListCarDp() {
		// TODO Auto-generated method stub
		return dpRepo.getListCarDp();
	}
	
	@Override
	public List<MstDp> getListMotorDp() {
		// TODO Auto-generated method stub
		return dpRepo.getListMotorDp();
	}

	
}
	
	
