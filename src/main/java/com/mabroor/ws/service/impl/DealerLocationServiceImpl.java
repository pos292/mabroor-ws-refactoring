package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstDealerLocation;
import com.mabroor.ws.repository.DealerLocationRepository;
import com.mabroor.ws.service.DealerLocationService;

@Service
public class DealerLocationServiceImpl implements DealerLocationService {
	
	@Autowired
	private DealerLocationRepository dealerRepository;

	@Override
	public List<MstDealerLocation> findAllByStatus() {
		return dealerRepository.getListDealer();
	}

	@Override
	public Optional<MstDealerLocation> findById(Long id) {
		return dealerRepository.findById(id);
	}

	@Override
	public List<MstDealerLocation> getListDealer() {
		// TODO Auto-generated method stub
		return dealerRepository.getListDealer();
	}
}
