package com.mabroor.ws.service.impl;

import java.util.List;

import com.mabroor.ws.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailClient {
	private JavaMailSender mailSender;

	private MailContentBuilder mailContentBuilder;
	
	@Value("${spring.mail.username}")
	private String from;

	@Autowired
	public MailClient(JavaMailSender mailSender, MailContentBuilder mailContentBuilder) {
		this.mailSender = mailSender;
		this.mailContentBuilder = mailContentBuilder;
	}

	public void prepareAndSend(String type, String recipient, String subject, String name, String message) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			String content = mailContentBuilder.build(type, name.toLowerCase(), message);
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}

	public void prepareAndSend(String type, String recipient, String subject, String name, String message, String template) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			String content = mailContentBuilder.build(type, name.toLowerCase(), message, template);
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}

	@Async
	public void prepareAndSendPengajuanMobilUserMobile( String recipient, String subject, MstPengajuanCar mstPengajuanCar) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanMobilUserMobile( mstPengajuanCar ,"email-mobil");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanMobilUserLeads( String recipient, String subject, MstPengajuanCar mstPengajuanCar) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanMobilUserLeads( mstPengajuanCar ,"email-mobil");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanMotorUserMobile( String recipient, String subject, MstPengajuanMotor mstPengajuanMotor) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanMotorUserMobile( mstPengajuanMotor ,"email-motor");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanMotorUserLeads( String recipient, String subject, MstPengajuanMotor mstPengajuanMotor) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanMotorUserLeads( mstPengajuanMotor ,"email-motor");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
//	@Async
//	public void prepareAndSendPengajuanCarBaruUserMobile( String recipient, String subject, MstPengajuanCar mstPengajuanCar) {
//		MimeMessagePreparator messagePreparator = mimeMessage -> {
//			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
//			messageHelper.setFrom(from);
//			messageHelper.setTo(recipient);
//			messageHelper.setSubject(subject);
//			
//			String content = mailContentBuilder.buildPengajuanCarBaruUserMobile( mstPengajuanCar ,"email-mobil-baru");
//			messageHelper.setText(content, true);
//		};
//		try {
//			mailSender.send(messagePreparator);
//		} catch (MailException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	@Async
//	public void prepareAndSendPengajuanCarBaruUserLeads( String recipient, String subject, MstPengajuanCar mstPengajuanCar) {
//		MimeMessagePreparator messagePreparator = mimeMessage -> {
//			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
//			messageHelper.setFrom(from);
//			messageHelper.setTo(recipient);
//			messageHelper.setSubject(subject);
//			
//			String content = mailContentBuilder.buildPengajuanCarBaruUserLeads( mstPengajuanCar ,"email-mobil-baru");
//			messageHelper.setText(content, true);
//		};
//		try {
//			mailSender.send(messagePreparator);
//		} catch (MailException e) {
//			e.printStackTrace();
//		}
//	}
	
	@Async
	public void prepareAndSendPengajuanMotorBaruUserMobile( String recipient, String subject, MstPengajuanMotor mstPengajuanMotor) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanMotorBaruUserMobile( mstPengajuanMotor ,"email-motor-baru");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanMotorBaruUserLeads( String recipient, String subject, MstPengajuanMotor mstPengajuanMotor) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanMotorBaruUserLeads( mstPengajuanMotor ,"email-motor-baru");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanHajiUserMobile( String recipient, String subject, MstWisataReligi mstUmroh) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanUmrohUserMobile( mstUmroh ,"email-haji");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanHajiUserLeads( String recipient, String subject, MstWisataReligi mstUmroh) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanHajiUserLeads( mstUmroh ,"email-haji");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	@Async
	public void prepareAndSendPengajuanHajiAmitraUserMobile( String recipient, String subject, MstWisataReligi mstUmroh) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanHajiAmitraUserMobile( mstUmroh ,"email-haji-amitra");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanHajiAmitraUserLeads( String recipient, String subject, MstWisataReligi mstUmroh) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanHajiAmitraUserLeads( mstUmroh ,"email-haji-amitra");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	@Async
	public void prepareAndSendPengajuanUmrohUserMobile( String recipient, String subject, MstWisataReligi mstUmroh) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanHajiUserMobile( mstUmroh ,"email-haji");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanUmrohUserLeads( String recipient, String subject, MstWisataReligi mstUmroh) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanUmrohUserLeads( mstUmroh ,"email-umroh");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}

	@Async
	public void prepareAndSendDisableUserMobile( String recipient, String subject, MstUserMobile mstDisable) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildDisableUserMobile( mstDisable ,"email-disable-userMobile");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendDisableUserLeads( String recipient, String subject, MstUserMobile mstDisable) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildDisableUserLeads( mstDisable ,"email-disable-userMobile");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanMultiGunaJaminanUserMobile( String recipient, String subject, MstPengajuanJaminan mstJaminan) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanMultiGunaJaminanUserMobile( mstJaminan ,"email-multiguna");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanMultiGunaJaminanUserLeads( String recipient, String subject, MstPengajuanJaminan mstJaminan) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanMultiGunaJaminanUserLeads( mstJaminan ,"email-multiguna");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanAsuransiMobilUserMobile( String recipient, String subject, MstPengajuanAsuransiMobil mstAsuransiMobil) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanAsuransiMobilUserMobile( mstAsuransiMobil ,"email-asuransiMobil");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void prepareAndSendPengajuanAsuransiMobilUserLeads( String recipient, String subject, MstPengajuanAsuransiMobil mstAsuransiMobil) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			
			String content = mailContentBuilder.buildPengajuanAsuransiMobilUserLeads( mstAsuransiMobil ,"email-asuransiMobil");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}

	@Async
	public void prepareAndSendPengajuanMultigunaTanpaJaminan(String recipient, String subject, MstPengajuanMultigunaTanpaJaminan mstPengajuanMultigunaTanpaJaminan, String name){
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);

			String content = mailContentBuilder.buildPengajuanMultigunaTanpaJaminan( mstPengajuanMultigunaTanpaJaminan ,name ,"email-pengajuan");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}

	@Async
	public void prepareAndSendPembiayaanUmroh(String recipient, String subject, MstPengajuanUmroh pengajuanUmroh, String name){
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(from);
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);

			String content = mailContentBuilder.buildPengajuanUmroh( pengajuanUmroh ,name ,"email-umroh");
			messageHelper.setText(content, true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			e.printStackTrace();
		}
	}
}
