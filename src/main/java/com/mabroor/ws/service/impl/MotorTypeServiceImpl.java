package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstMotorType;
import com.mabroor.ws.repository.MotorTypeRepository;
import com.mabroor.ws.service.MotorTypeService;

@Service
public class MotorTypeServiceImpl implements MotorTypeService {

	@Autowired
	private MotorTypeRepository motorTypeRepository;

	@Override
	public List<MstMotorType> findAllByStatus() {
		return motorTypeRepository.findAllByStatus();
	}

	@Override
	public Optional<MstMotorType> findById(Long id) {
		return motorTypeRepository.findById(id);
	}

	@Override
	public List<MstMotorType> getListMotorType() {
		return motorTypeRepository.getListMotorType();
	}
	
	@Override
	public List<MstMotorType> getListMotorBrandId(Long brandId) {
		return motorTypeRepository.getListMotorBrandId(brandId);
	}
	
	@Override
	public List<MstMotorType> getListMotorBrandIdCategoryId(Long brandId, Long categoryId){
		return motorTypeRepository.getListMotorBrandIdCategoryId(brandId, categoryId);
	}
}
