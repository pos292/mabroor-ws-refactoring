package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstStatusKepemilikanRumah;
import com.mabroor.ws.repository.StatusKepemilikanRumahRepository;
import com.mabroor.ws.service.StatusKepemilikanRumahService;

@Service
public class StatusKepemilikanRumahServiceImpl implements StatusKepemilikanRumahService{

	@Autowired
	private StatusKepemilikanRumahRepository statRumahRepo;
	
	@Override
	public List<MstStatusKepemilikanRumah> findAllByStatus() {
		// TODO Auto-generated method stub
		return statRumahRepo.findAllByStatus();
	}

	@Override
	public List<MstStatusKepemilikanRumah> getListKepemilikanRumahStatus() {
		// TODO Auto-generated method stub
		return statRumahRepo.getListKepemilikanRumahStatus();
	}

	@Override
	public Optional<MstStatusKepemilikanRumah> findById(Long id) {
		// TODO Auto-generated method stub
		return statRumahRepo.findById(id);
	}

}
