package com.mabroor.ws.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstProductInformation;
import com.mabroor.ws.repository.ProductInformationRepository;
import com.mabroor.ws.service.ProductInformationService;

@Service
public class ProductInformationServiceImpl implements ProductInformationService{

	@Autowired
	ProductInformationRepository accidentproductRepo;

	@Override
	public List<MstProductInformation> getAll() {
		// TODO Auto-generated method stub
		return accidentproductRepo.getAll();
	}

	@Override
	public List<MstProductInformation> getListMainProduct(Long idMainProduct) {
		// TODO Auto-generated method stub
		return accidentproductRepo.getListMainProduct(idMainProduct);
	}


}
