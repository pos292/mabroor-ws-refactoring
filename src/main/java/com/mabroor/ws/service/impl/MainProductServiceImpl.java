package com.mabroor.ws.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstMainProduct;
import com.mabroor.ws.repository.MainProductRepository;
import com.mabroor.ws.service.MainProductService;
@Service
public class MainProductServiceImpl implements MainProductService {

	@Autowired
	private MainProductRepository mainRepo;
	
	@Override
	public List<Map<String, Object>> getListActive() {
		
		List<Map<String, Object>> mainProduct = new ArrayList<>();
		
		List<MstMainProduct> tmpRes = mainRepo.getListActive();
		
		for(MstMainProduct row: tmpRes) {
			Map<String, Object> tmpDt = new HashMap<>();
			tmpDt.put("id", row.getId());
			tmpDt.put("title", row.getMainProductName());
			
			mainProduct.add(tmpDt);
		}
		
		return mainProduct;
	}

}
