package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstHubungan;
import com.mabroor.ws.repository.HubunganRepository;
import com.mabroor.ws.service.HubunganService;

@Service
public class HubunganServiceImpl implements HubunganService {

	@Autowired
	private HubunganRepository hubRepo;
	
	@Override
	public List<MstHubungan> findAllByStatus() {
		// TODO Auto-generated method stub
		return hubRepo.findAllByStatus();
	}

	@Override
	public List<MstHubungan> getListHubungan() {
		// TODO Auto-generated method stub
		return hubRepo.getListHubungan();
	}

	@Override
	public Optional<MstHubungan> findById(Long id) {
		// TODO Auto-generated method stub
		return hubRepo.findById(id);
	}

}
