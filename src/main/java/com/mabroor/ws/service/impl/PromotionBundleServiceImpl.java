package com.mabroor.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstPromotionBundle;
import com.mabroor.ws.repository.PromotionBundleRepository;
import com.mabroor.ws.service.PromotionBundleService;

@Service
public class PromotionBundleServiceImpl implements PromotionBundleService {
	
	@Autowired
	private PromotionBundleRepository bundleRepo;

	@Override
	public List<MstPromotionBundle> getList() {
		// TODO Auto-generated method stub
		return bundleRepo.getList();
	}

	@Override
	public Optional<MstPromotionBundle> findById(Long id) {
		// TODO Auto-generated method stub
		return bundleRepo.findById(id);
	}

	@Override
	public MstPromotionBundle save(MstPromotionBundle mstBundle) {
		// TODO Auto-generated method stub
		return bundleRepo.save(mstBundle);
	}

	@Override
	public MstPromotionBundle update(MstPromotionBundle mstBundle) {
		// TODO Auto-generated method stub
		return bundleRepo.save(mstBundle);
	}

	@Override
	public List<MstPromotionBundle> findByData(Long id) {
		// TODO Auto-generated method stub
		return bundleRepo.findByData(id);
	}

	@Override
	public List<MstPromotionBundle> findByEmailNull(Long id) {
		// TODO Auto-generated method stub
		return bundleRepo.findByEmailNull(id);
	}

//	@Override
//	public Optional<MstPromotionBundle> findByData() {
//		// TODO Auto-generated method stub
//		return bundleRepo.findByData();
//	}

}
