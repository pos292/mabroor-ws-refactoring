package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstHubungiKami;

public interface HubungiKamiService {

	List<MstHubungiKami> getListHubungiKami();
	
	MstHubungiKami save(MstHubungiKami mstHub);
	
	MstHubungiKami update(MstHubungiKami mstHub);
}
