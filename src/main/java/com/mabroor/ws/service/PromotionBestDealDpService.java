package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstPromotionBestDealDp;

public interface PromotionBestDealDpService {
	
	List<MstPromotionBestDealDp> findAllByPromotion(Long id);
}
