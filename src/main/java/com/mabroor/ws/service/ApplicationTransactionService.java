package com.mabroor.ws.service;

import com.mabroor.ws.entity.MstApplicationTransaction;

public interface ApplicationTransactionService {

	MstApplicationTransaction save(MstApplicationTransaction trxApp);
	
	void deleteAppId(Long Id);
}
