package com.mabroor.ws.service;

import java.util.List;
import java.util.Map;

import com.mabroor.ws.dto.CalculateMrpMobilReqDto;
import com.mabroor.ws.entity.MstDanaTunaiMobilMaster;

public interface DanaTunaiMobilMasterService {

	List<MstDanaTunaiMobilMaster> findAll();
	
	List<MstDanaTunaiMobilMaster> findByBrandCar();
	
	List<MstDanaTunaiMobilMaster> findByTypeCar(String brand);
	
	List<MstDanaTunaiMobilMaster> findByModelCar(String type);
	
	List<MstDanaTunaiMobilMaster> findByYearCar(String model);
	
	List<MstDanaTunaiMobilMaster> findByYearAndModelTypeCar(String brand, String type, String model);
	
	Map<String, Object> findByCalculateMrp(CalculateMrpMobilReqDto calculateMrpMobil);
	
	MstDanaTunaiMobilMaster getJaminanMobilByBtmy(String brand, String type, String model, String year);

	Object findByBrandTypeModelCar(String brand, String type);
}
