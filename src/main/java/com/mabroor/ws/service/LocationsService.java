package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstLocations;

public interface LocationsService {

	List<MstLocations> findAllByStatus();

	Optional<MstLocations> findById(Long id);
	
	List<MstLocations> getListBtdAvailable();

}
