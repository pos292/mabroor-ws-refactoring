package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstInboxCategory;

public interface InboxCategoryService {
	
	List<MstInboxCategory> getInboxCategory();

	List<MstInboxCategory> getInboxCategoryAfterLogin();

	List<MstInboxCategory> getInboxCategoryBeforeLogin();
	
	MstInboxCategory findById(Long id);

}
