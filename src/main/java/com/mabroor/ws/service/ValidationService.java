package com.mabroor.ws.service;

import com.mabroor.ws.entity.MstPromotion;
import com.mabroor.ws.entity.MstUserMobile;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public interface ValidationService {
    MstUserMobile userLoginValid(String token) throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException;
//    void userLoginValidMutualfund(String token);

}
