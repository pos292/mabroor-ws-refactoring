package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstPengajuanAsuransiMobilPerluasan;

public interface PengajuanAsuransiMobilPerluasanService {

	List<MstPengajuanAsuransiMobilPerluasan> getListPerluasanMobil();
	
	List<MstPengajuanAsuransiMobilPerluasan> findByPerluasanId(Long asuransiMobilId);
	
	MstPengajuanAsuransiMobilPerluasan save(MstPengajuanAsuransiMobilPerluasan mstMobil);
	
	MstPengajuanAsuransiMobilPerluasan update(MstPengajuanAsuransiMobilPerluasan mstMobil);
}
