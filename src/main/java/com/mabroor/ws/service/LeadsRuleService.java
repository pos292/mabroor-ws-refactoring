package com.mabroor.ws.service;

import com.mabroor.ws.entity.MstLeadsRule;

public interface LeadsRuleService {

	MstLeadsRule save(MstLeadsRule mstLeadsRule);
}
