package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstBeriMasukan;

public interface BeriMasukanService {
    
    MstBeriMasukan save(MstBeriMasukan mstBeriMasukan);
    
    MstBeriMasukan update(MstBeriMasukan mstBeriMasukan);

    Optional <MstBeriMasukan> findById(Long id);

	List<MstBeriMasukan> getListBeriMasukan();

}
