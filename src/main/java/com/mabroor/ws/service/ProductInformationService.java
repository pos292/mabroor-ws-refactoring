package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstProductInformation;

public interface ProductInformationService {

	List<MstProductInformation> getAll();
	
	List<MstProductInformation> getListMainProduct(Long idMainProduct);
}
