package com.mabroor.ws.service;

import com.mabroor.ws.entity.SysParam;

public interface SysParamService {

	SysParam getByValueType(String valueType);

}
