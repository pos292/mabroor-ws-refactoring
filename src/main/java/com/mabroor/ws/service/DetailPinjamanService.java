package com.mabroor.ws.service;

import com.mabroor.ws.dto.RespDetailWilayahDto;
import com.mabroor.ws.dto.RespTujuanPinjamanDto;
import com.mabroor.ws.entity.*;

import java.util.List;

public interface DetailPinjamanService {

    List<MstTujuanPinjaman> findall();

    List<MstBankPenerbit> findallbank();

    List<MstPekerjaan> findallpekerjaan();

    List<MstTenor> findalltenor();

    List<MstPengajuanMultigunaTanpaJaminan> findallpengajuan();

    MstPengajuanMultigunaTanpaJaminan savepengajuan(MstPengajuanMultigunaTanpaJaminan data);

    MstDataDiriPengajuanMultiguna savedatadiri(MstDataDiriPengajuanMultiguna datas);


    List<MstMultigunaKota> findAllKota();

    List<MstMultigunaKecamatan> findAllKecamatanbyidkota(Long id_kota);

    List<MstMultigunaKelurahan> findAllKelurahanbyidkecamatan(Long id_kecamatan);

    List<MstMultigunaKodePos> findAllKodePosbyidkelurahan(Long id_kelurahan);


    MstPengajuanMultigunaTanpaJaminan findById(Long id);

    MstDataDiriPengajuanMultiguna findDataDiri(Long id);

    RespDetailWilayahDto findDetailWilayahByCode(String code);


}
