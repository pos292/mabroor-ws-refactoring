package com.mabroor.ws.service;

import com.mabroor.ws.entity.MstPromotionBestDealPackageSimulation;

public interface PromotionBestDealPackageSimulationService {
	MstPromotionBestDealPackageSimulation getPromotionBestDealByPromoId(Long id);
}
