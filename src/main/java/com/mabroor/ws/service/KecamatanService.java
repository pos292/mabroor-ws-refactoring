package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstKecamatan;

public interface KecamatanService {

	List<MstKecamatan> findAllByStatus();
	
	List<MstKecamatan> getListKecamatan();
	
	List<MstKecamatan> findByKabupatenId(Long id);
	
	Optional<MstKecamatan> findById(Long id);
}
