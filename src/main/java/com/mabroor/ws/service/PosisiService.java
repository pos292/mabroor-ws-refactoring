package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstPosisi;

public interface PosisiService {

	List<MstPosisi> findAllByStatus();
	
	List<MstPosisi> getListPosisi();
	
	Optional <MstPosisi> findById(Long id);
}
