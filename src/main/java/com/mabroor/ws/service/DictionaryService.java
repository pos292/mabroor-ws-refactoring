package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.dto.DictionaryDto;
import com.mabroor.ws.dto.DictionaryReqDto;
import com.mabroor.ws.entity.MstDictionary;

public interface DictionaryService {

	DictionaryDto findAllByAbjad();
	
	DictionaryDto findAllBySearchAbjad(String title);
}
