package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstMotorBrand;

public interface MotorBrandService {

	List<MstMotorBrand> findAllByStatus();

	Optional<MstMotorBrand> findById(Long id);

	List<MstMotorBrand> getListMotorBrand();


}
