package com.mabroor.ws.service;
import com.mabroor.ws.dto.MstMabroor101WithHeaderDto;
import com.mabroor.ws.entity.MstMabroor101;

import org.springframework.stereotype.Service;

@Service
public interface Mabroor101Service {
	
	MstMabroor101WithHeaderDto findAllList();

	MstMabroor101 findById(Long listId);
}
