package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstPendidikanTerakhir;

public interface PendidikanTerakhirService {
	
	List<MstPendidikanTerakhir> findAllByStatus();
	 
	List<MstPendidikanTerakhir> getListPendidikanTerakhir();
	
	Optional <MstPendidikanTerakhir> findById(Long id);
}
