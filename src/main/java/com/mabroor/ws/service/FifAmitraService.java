package com.mabroor.ws.service;

import com.mabroor.ws.dto.ReqAmitraSubmission;
import com.mabroor.ws.dto.ResFifAmitraDto;
import com.mabroor.ws.dto.ResFifAmitraListDataDto;
import com.mabroor.ws.dto.ResFifAmitraListDto;

import java.util.LinkedHashMap;
import java.util.List;

public interface FifAmitraService {
    List<Integer> getState(String state);
    List<LinkedHashMap<String, String>> getCity(int id, String city);
    List<LinkedHashMap<String, String>>  getDistrict(int id, String district);
    List<LinkedHashMap<String, String>>  getSubDistrict(int id, String subDistrict);
    ResFifAmitraDto submit(ReqAmitraSubmission request);
    ResFifAmitraListDataDto getDigitalLeadStatus(String intLeadId);
}
