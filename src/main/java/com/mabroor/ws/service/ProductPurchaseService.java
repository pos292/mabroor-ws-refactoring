package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstProductPurchase;

public interface ProductPurchaseService {
	
	List<MstProductPurchase> getList();

}
