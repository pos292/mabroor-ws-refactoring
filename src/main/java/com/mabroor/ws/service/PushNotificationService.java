package com.mabroor.ws.service;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.transaction.annotation.Transactional;

import com.mabroor.ws.model.PushNotificationRequest;

@Transactional
public interface PushNotificationService {
	public void sendMessage(Map<String, String> data, PushNotificationRequest request) throws InterruptedException, ExecutionException;
	public void sendMessageWithoutData(PushNotificationRequest request) throws InterruptedException, ExecutionException;
	public void sendMessageToToken(PushNotificationRequest request) throws InterruptedException, ExecutionException;
}
