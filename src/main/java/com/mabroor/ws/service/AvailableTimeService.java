package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstAvailableTime;

public interface AvailableTimeService {

	List<MstAvailableTime> findAllByStatus();
	
	Optional<MstAvailableTime>findById(Long id);
	
	List<MstAvailableTime> findAllEnable();
	
	List<MstAvailableTime> getListAvailableTime();
	
	List<MstAvailableTime> findByListId(String[] id);
}
