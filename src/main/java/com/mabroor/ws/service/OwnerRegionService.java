package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstOwnerRegion;

public interface OwnerRegionService {

	List<MstOwnerRegion> getList();
}
