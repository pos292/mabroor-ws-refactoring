package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstTenorMotor;

public interface TenorMotorService {
	
	List<MstTenorMotor> getList();

}
