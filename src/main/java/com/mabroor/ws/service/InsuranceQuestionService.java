package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstInsuranceQuestion;

public interface InsuranceQuestionService {

	List<MstInsuranceQuestion> getListInscQuest();
	
	Optional<MstInsuranceQuestion> findById(Long id);
	
	List<MstInsuranceQuestion> getListInscQuestByInsuranceId(Long insuranceId);
}
