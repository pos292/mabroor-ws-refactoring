package com.mabroor.ws.service;

import com.mabroor.ws.dto.PembiayaanUmrohDto;
import com.mabroor.ws.dto.PembiayaanUmrohRespResultDto;
import com.mabroor.ws.entity.MstLogApi;
import com.mabroor.ws.entity.MstPengajuanUmroh;
import com.mabroor.ws.entity.MstUserMobile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface LogApiService {
    MstLogApi saveUmrohLog(HttpServletRequest httpServletRequest, PembiayaanUmrohDto pembiayaanUmrohDto, PembiayaanUmrohRespResultDto pembiayaanUmrohRespResultDto, MstUserMobile mstUserMobile) throws IOException;

}
