package com.mabroor.ws.service;

import java.util.List;
import java.util.Map;

import com.mabroor.ws.dto.CalculateMrpMotorReqDto;
import com.mabroor.ws.entity.MstMotorMaster;

public interface MotorMasterService {

	List<MstMotorMaster> findAll();
	
	List<MstMotorMaster> findByBrandMotor();
	
	List<MstMotorMaster> findByTypeMotor(String brand);
	
	List<MstMotorMaster> findByModelMotor(String type);
	
	List<MstMotorMaster> findByYearMotor(String model);
	
	List<MstMotorMaster> findByYearAndModelTypeMotor(String brand, String type, String model);
	
	Map<String, Object> findByCalculateMrp(CalculateMrpMotorReqDto calculateMrpMotor);

	Object findByBrandTypeModelMotor(String brand, String type);

	List<MstMotorMaster> findByTypeMotorDistinct();
}
