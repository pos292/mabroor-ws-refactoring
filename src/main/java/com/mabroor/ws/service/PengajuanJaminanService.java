package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.entity.MstPengajuanJaminan;

public interface PengajuanJaminanService {

	List<MstPengajuanJaminan> findAllByStatus();
	
	MstPengajuanJaminan save(MultipartFile fileBpkbMobil, MultipartFile fileBpkbMotor, MultipartFile fileStnk,MultipartFile fileFotoKananDepan,MultipartFile fileFotoKananBelakang,MultipartFile fileFotoKiriDepan,MultipartFile fileFotoKiriBelakang, MstPengajuanJaminan mstPengajuan);
	
	MstPengajuanJaminan update(MstPengajuanJaminan mstPengajuan);
	
    List<MstPengajuanJaminan> getListPengajuan();
    
    List<MstPengajuanJaminan> findAllByTitleAsc();
    
    List<MstPengajuanJaminan> findAllEnable();
    
    Optional <MstPengajuanJaminan> findById(Long id);
    
    List<MstPengajuanJaminan> findAllByUserId(Long id);
	
	List<MstPengajuanJaminan> findByPhone(String phone);
}
