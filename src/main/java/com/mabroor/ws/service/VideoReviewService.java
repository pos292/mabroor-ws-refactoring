package com.mabroor.ws.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstVideoReview;

@Service
public interface VideoReviewService {
	
	List<MstVideoReview> findAllByStatus();

	Optional<MstVideoReview> findById(Long id);
	
	List<MstVideoReview> findByMainProductId(Long idMainProduct);
	
	List<MstVideoReview> findByMainProductIdList(Long idMainProduct);

	List<Map<String, String>> findAllChips();

	List<Map<String, String>> findLandingpageChips();

	List<MstVideoReview> findByMainProductNameLimit(String mainProductName);

	List<MstVideoReview> findByMainProductName(String mainProductName);

	List<MstVideoReview> findByMainProductIdandSubCategoryName(Long idMainProduct, String subcategoryName);
}
