package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstHubungan;

public interface HubunganService {

	List<MstHubungan> findAllByStatus();
	
	List<MstHubungan> getListHubungan();
	 
	Optional <MstHubungan> findById(Long id);
}
