package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstProductPurchaseTenor;

public interface ProductPurchaseTenorService {

	List<MstProductPurchaseTenor> getList();
}
