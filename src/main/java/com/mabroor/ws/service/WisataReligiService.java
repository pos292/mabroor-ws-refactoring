package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstWisataReligi;

public interface WisataReligiService {

	List<MstWisataReligi> findAllByStatus();
	
	List<MstWisataReligi> getListWisata();
	
	MstWisataReligi findLeadsId(String id);
	
	Optional<MstWisataReligi> findById(Long id);
	
	MstWisataReligi save(MstWisataReligi mstWisata);
	
	MstWisataReligi update(MstWisataReligi mstWisata);
	
	List<MstWisataReligi> findAllByUserId(Long id);
}
