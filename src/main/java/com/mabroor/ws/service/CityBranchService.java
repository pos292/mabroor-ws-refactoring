package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstCityBranch;

public interface CityBranchService {

	List<MstCityBranch> getCityByPengajuan(String kotaPengajuan);
	
	List<MstCityBranch> getList();

	List<MstCityBranch> getCityByKabupatenId(Long kotaPengajuan);
}
