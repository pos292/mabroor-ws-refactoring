package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstTentangKami;

public interface TentangKamiService {

    Optional <MstTentangKami> findById(Long id);

	List<MstTentangKami> getListTentangKami();
}
