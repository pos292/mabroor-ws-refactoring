package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstNeeds;

public interface NeedsService {

	List<MstNeeds> findAllByStatus();
	
	List<MstNeeds> getListNeeds();
	
	Optional<MstNeeds> findById(Long id);
}
