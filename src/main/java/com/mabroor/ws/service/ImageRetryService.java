package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstImageRetry;

public interface ImageRetryService {
	
	List<MstImageRetry> getListImgSelfie();
	
	MstImageRetry save(MstImageRetry mstImage);
	
	 void updateListImg(String Status, Long id);
	

}
