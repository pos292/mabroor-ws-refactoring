package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstProductInformationBenefit;

public interface ProductInformationBenefitService {

	List<MstProductInformationBenefit> getListbyProductInfo(Long idProductInformation);
}
