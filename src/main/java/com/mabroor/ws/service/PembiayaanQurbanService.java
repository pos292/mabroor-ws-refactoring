package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstPembiayaanQurban;

public interface PembiayaanQurbanService {

	List<MstPembiayaanQurban> getList();
	
	List<MstPembiayaanQurban> getListByNamePartner(String namePartner);
}
