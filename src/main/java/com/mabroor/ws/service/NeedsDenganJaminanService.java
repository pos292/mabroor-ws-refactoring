package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstNeedsDenganJaminan;

public interface NeedsDenganJaminanService {

	List<MstNeedsDenganJaminan> findAllByStatus();
	
	List<MstNeedsDenganJaminan> getListNeedsDenganJaminan();
	
	Optional<MstNeedsDenganJaminan> findById(Long id);
	
	MstNeedsDenganJaminan getMstNeedsJaminanTitleDenganJaminan(String title);
}
