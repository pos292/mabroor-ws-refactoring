package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstUserMobileMenu;

public interface UserMobileMenuService {

	List<MstUserMobileMenu> getListMenu(Long idMenuCategory);
	
	List<MstUserMobileMenu> getFeaturedMenu();
	
	List<MstUserMobileMenu> getDefaultMenu();
}
