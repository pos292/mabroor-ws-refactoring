package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstActivityUser;

public interface ActivityUserService {

	List<MstActivityUser> getListActivityUser();
	
	List<MstActivityUser> findAllByUserId(Long id);
	
	Optional <MstActivityUser> findById(Long id);
	
	MstActivityUser save (MstActivityUser mstAct);
	
	MstActivityUser update (MstActivityUser mstAct);
}
