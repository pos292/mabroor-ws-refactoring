package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstNeedsTanpaJaminan;

public interface NeedsTanpaJaminanService {

	List<MstNeedsTanpaJaminan> findAllByStatus();
	
	List<MstNeedsTanpaJaminan> getListNeedsTanpaJaminan();
	
	Optional<MstNeedsTanpaJaminan> findById(Long id);
}
