package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstKebijakanPrivasi;

public interface KebijakanPrivasiService {

	List<MstKebijakanPrivasi> getListPrivasi();
}
