package com.mabroor.ws.service;

import com.mabroor.ws.entity.MstPengajuanQurban;

public interface PengajuanQurbanService {
	
	MstPengajuanQurban save(MstPengajuanQurban mstQurban);
	
	MstPengajuanQurban update(MstPengajuanQurban mstQurban);

}
