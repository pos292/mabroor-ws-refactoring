package com.mabroor.ws.service;

import java.util.List;
import java.util.Map;

import com.mabroor.ws.dto.CalculateMrpMotorReqDto;
import com.mabroor.ws.entity.MstDanaTunaiMotorMaster;

public interface DanaTunaiMotorMasterService {

	List<MstDanaTunaiMotorMaster> findAll();
	
	List<MstDanaTunaiMotorMaster> findByBrandMotor();
	
	List<MstDanaTunaiMotorMaster> findByTypeMotor(String brand);
	
	List<MstDanaTunaiMotorMaster> findByModelMotor(String type);
	
	List<MstDanaTunaiMotorMaster> findByYearMotor(String model);
	
	List<MstDanaTunaiMotorMaster> findByYearAndModelTypeMotor(String brand, String type, String model);
	
	Map<String, Object> findByCalculateMrp(CalculateMrpMotorReqDto calculateMrpMotor);

	Object findByBrandTypeModelMotor(String brand, String type);

	List<MstDanaTunaiMotorMaster> findByTypeMotorDistinct();
}
