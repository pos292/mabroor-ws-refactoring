package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstGeneralInsuranceCarProductBenefit;

public interface InsuranceCarProductBenefitService {
	 
	 List<MstGeneralInsuranceCarProductBenefit> getListCarProductByProductId(Long carProductId);

}
