package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.dto.PengajuanAllCustomDto;
import com.mabroor.ws.entity.MstPengajuanCar;

public interface PengajuanAllService {

	List<PengajuanAllCustomDto> findAllByUserIdPagging(Long userId, Long limit, Long pagging);
	
	List<PengajuanAllCustomDto> findLeadsByLeadsId(Long userId, Long leadsId, String queryType);
}
