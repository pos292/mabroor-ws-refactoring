package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstDanaTunaiTenor;

public interface DanaTunaiTenorService {
	
	List<MstDanaTunaiTenor> findAll();
	
	List<MstDanaTunaiTenor> findByType(String type);
	
	List<MstDanaTunaiTenor> findByProductType(String type, String product);

}
