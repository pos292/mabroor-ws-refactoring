package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstDp;

public interface DpService {

	List<MstDp> getListDp();
	
	Optional<MstDp> findById(Long id);
	
	List<MstDp> getListCarDp();
	
	List<MstDp> getListMotorDp();
	
}
