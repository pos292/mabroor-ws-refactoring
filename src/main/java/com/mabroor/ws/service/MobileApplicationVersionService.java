package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstMobileApplicationVersion;

public interface MobileApplicationVersionService {

	List<MstMobileApplicationVersion> getList();
}
