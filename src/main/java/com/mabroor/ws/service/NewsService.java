package com.mabroor.ws.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mabroor.ws.entity.MstNews;

@Service
public interface NewsService {
	
	List<MstNews> findAllByStatus();
	
	List<MstNews> findByMainProductId(Long idMainProduct);
	
	List<MstNews> findByMainProductIdLimit(Long idMainProduct);
	
	List<MstNews> findByMainProductAll();
	
	Optional<MstNews> findById(Long id);

	List<Map<String, String>> findActiveSubCategory();

	List<MstNews> findBySubCategoryName(String subcategoryName);

	List<MstNews> findBySubCategoryNameAll(String subcategoryName);

	List<Map<String, String>> findAllSubCategory();

	List<MstNews> findByMainProductIdandSubCategoryName(Long idMainProduct, String subcategoryName);
}
