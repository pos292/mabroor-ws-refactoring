package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstPromotionBundle;
import com.mabroor.ws.entity.MstPromotionBundleContentData;

public interface PromotionBundleContentDataService {
	
	Optional<MstPromotionBundleContentData> getListDetailPromoData();
	
//	MstPromotionBundle getDataDetailPromoData(Long id);
//	Optional<MstPromotionBundleContentData> getDataDetailPromoData();

}
