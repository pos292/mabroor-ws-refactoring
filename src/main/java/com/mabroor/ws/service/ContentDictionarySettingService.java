package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstContentDictionarySetting;

public interface ContentDictionarySettingService {
	
	List<MstContentDictionarySetting> findByDictionaryId(Long id);
	
	List<MstContentDictionarySetting> getList();
}
