package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstInbox;

public interface InboxService {

//	List<MstInbox> getInboxListRead();
	
	List<MstInbox> getInboxAll(Long userId);
	
	List<MstInbox> getInboxId(Long idInboxCategory, Long userId);
	
	int updateInboxRead(Long id);

	List<MstInbox> getInboxAllBeforeLogin();

	List<MstInbox> getInboxIdBeforeLogin(long idInboxCategory);

	List<MstInbox> getInboxNotif(Long userId);

	List<MstInbox> getInboxNotifBeforeLogin();
	
	MstInbox save(MstInbox mstInbox);

	Optional<MstInbox> isExistInboxId(Long inboxId);

	Optional<MstInbox> findIdByIdDataAndUserId(Long idData, Long userId);
}
