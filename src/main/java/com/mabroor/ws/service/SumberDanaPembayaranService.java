package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstSumberDanaPembayaran;

public interface SumberDanaPembayaranService {

	List<MstSumberDanaPembayaran> findAllByStatus();
	
	List<MstSumberDanaPembayaran> getListDanaPembayaran();
	
	Optional <MstSumberDanaPembayaran> findById(Long id);
}
