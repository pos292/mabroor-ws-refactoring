package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstMotorType;

public interface MotorTypeService {

	List<MstMotorType> findAllByStatus();

	Optional<MstMotorType> findById(Long id);

	List<MstMotorType> getListMotorType();

	List<MstMotorType> getListMotorBrandId(Long brandId);

	List<MstMotorType> getListMotorBrandIdCategoryId(Long brandId, Long categoryId);
}
