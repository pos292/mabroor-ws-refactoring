package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstDeepLink;

public interface DeepLinkService {

	List<MstDeepLink> getList();
	List<MstDeepLink> getListLinkBsi();
}
