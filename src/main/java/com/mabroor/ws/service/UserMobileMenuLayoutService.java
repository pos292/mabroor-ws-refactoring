package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstUserMobileMenuLayout;

public interface UserMobileMenuLayoutService {
	
	 List<MstUserMobileMenuLayout> findAllByUserId(Long userId);
	 
	 Optional <MstUserMobileMenuLayout> findById(Long id);
	 
	 MstUserMobileMenuLayout save (MstUserMobileMenuLayout mstLayout);
		
	 MstUserMobileMenuLayout update (MstUserMobileMenuLayout mstLayout);
	 
	 List<MstUserMobileMenuLayout> findByIdMenu(Long idMenu);

	int resetUserMobileMenu(Long userId);

}
