package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstPlatKendaraan;

public interface PlatKendaraanService {

	List<MstPlatKendaraan> getListPlat();
	
	Optional <MstPlatKendaraan> findById(Long id);
}
