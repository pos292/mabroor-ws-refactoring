package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstGeneralInsuranceCar;

public interface GeneralInsuranceCarService {

	List<MstGeneralInsuranceCar> getAll();
	
	Optional<MstGeneralInsuranceCar> getListInscType(Long id);
	
}
