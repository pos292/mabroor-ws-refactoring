package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstMotorColor;
import com.mabroor.ws.entity.MstMotorUnit;

public interface MotorUnitService {
	
	List<MstMotorUnit> findByListId(Long[] id);
	
	List<MstMotorColor> findByMotorUnitId(Long id);
	

}
