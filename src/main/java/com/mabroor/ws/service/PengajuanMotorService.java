package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.entity.MstPengajuanMotor;

public interface PengajuanMotorService {


	List<MstPengajuanMotor> findAllByStatus();
    
	MstPengajuanMotor save(MultipartFile fileFotoUnit, MultipartFile fileFotoKtp, MstPengajuanMotor mstPengajuan);
    
	MstPengajuanMotor update(MstPengajuanMotor mstPengajuan);

    Optional <MstPengajuanMotor> findById(Long id);

	List<MstPengajuanMotor> getListPengajuan();
	
	List<MstPengajuanMotor> findAllEnable();
	
	List<MstPengajuanMotor> findAllByUserId(Long id);
}
