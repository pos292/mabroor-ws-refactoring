package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstFAQ;

public interface FAQService {
	
	List<MstFAQ> findAllByStatus();

	List<MstFAQ> findByMainProduct(Long idMainProduct);

	List<MstFAQ> findByMainProductLandingPage(Long idMainProduct);

}
