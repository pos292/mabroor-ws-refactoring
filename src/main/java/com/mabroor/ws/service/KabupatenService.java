package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstKabupaten;

public interface KabupatenService {

	List<MstKabupaten> findAllByStatus();

	Optional<MstKabupaten> findById(Long id);
	
	List<MstKabupaten> getListKabupaten();
	
	List<MstKabupaten> findByProvinsiId(Long id);

	List<MstKabupaten> findByNamaKabupaten(String namaKabupaten);

}

