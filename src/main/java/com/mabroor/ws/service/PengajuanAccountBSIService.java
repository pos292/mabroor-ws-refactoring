package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstPengajuanAccountBSI;

public interface PengajuanAccountBSIService {
    
    MstPengajuanAccountBSI save(MstPengajuanAccountBSI pengajuan);
    MstPengajuanAccountBSI update(MstPengajuanAccountBSI pengajuan);
    MstPengajuanAccountBSI findByUserAndCreateDateNow(Long userid);
    List<MstPengajuanAccountBSI> findByUserId(Long userid);
    
}
