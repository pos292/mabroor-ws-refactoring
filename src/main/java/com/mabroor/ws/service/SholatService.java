package com.mabroor.ws.service;

import java.util.Date;
import java.util.List;

import com.mabroor.ws.entity.MstSholat;

public interface SholatService {

	 List<MstSholat> getListSholat();
	 
	 List<MstSholat> getWaktuSholat(String location);
	 
	 List<MstSholat> getLocation(String prov);
	 
	 List<MstSholat> getLocationAndTime(String prov, String date);
	
	 List<MstSholat> getDate(String date);
	 
	 List<MstSholat> findByLocation();
}
