package com.mabroor.ws.service;

import java.util.LinkedHashMap;
import java.util.List;

import com.mabroor.ws.entity.MstPromotionSetting;

public interface PromotionSettingService {

	List<MstPromotionSetting> getListPromotionSetting();
}
