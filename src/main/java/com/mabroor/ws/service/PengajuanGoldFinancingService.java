package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstPengajuanGoldFinancing;

public interface PengajuanGoldFinancingService {
	
	MstPengajuanGoldFinancing save(MstPengajuanGoldFinancing mstGold);
	
	MstPengajuanGoldFinancing update(MstPengajuanGoldFinancing mstGold);
	
	List<MstPengajuanGoldFinancing> findByPhone(String phone);

}
