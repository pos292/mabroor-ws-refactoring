package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstUserMobileMenuCategory;

public interface UserMobileMenuCategoryService {

	List<MstUserMobileMenuCategory> getList();
	
	Optional<MstUserMobileMenuCategory> findById(Long id);
	
}
