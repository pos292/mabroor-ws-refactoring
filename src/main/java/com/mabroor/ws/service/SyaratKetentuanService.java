package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstSyaratKetentuan;

public interface SyaratKetentuanService {

	List<MstSyaratKetentuan> findAllByStatus();
	
	List<MstSyaratKetentuan> findAllById();
	
	List<MstSyaratKetentuan> findAllEnable();
	
	 List<MstSyaratKetentuan> getListSyarat();
	 
	 List<MstSyaratKetentuan> findByCategory(String categoryName);
}
