package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstDoa;

public interface DoaService {

	List<MstDoa> listDoa();
	
//	List<MstDoa> listDoaByIdCategory(Long idCategory, String namaDoa, Long limit, Long page);
	
	List<MstDoa> listDoaByIdCategory(Long idCategory);
	
	List<MstDoa> listSearchDoa(String namaDoa);
	
	MstDoa save(MstDoa doa);
}
