package com.mabroor.ws.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.entity.MstPromotionBundlePopup;

public interface PromotionBundlePopupService {

	List<MstPromotionBundlePopup> findAllByStatus();
}
