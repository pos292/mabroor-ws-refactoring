package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstPengajuanCar;
import com.mabroor.ws.entity.MstPromotion;

public interface PromotionService {

	List<MstPromotion> getListPromotion();
	
	List<MstPromotion> getListPromotionByMainProduct(Long idMainProduct);

	MstPromotion getPromotionByDetailPromo(Long id);
	
	MstPromotion save(MstPromotion mstPromo);
    
	MstPromotion update(MstPromotion mstPromo);
	
	Optional<MstPromotion> findById(Long id);
	
}
