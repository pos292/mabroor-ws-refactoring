package com.mabroor.ws.service;

import java.util.List;

import com.mabroor.ws.entity.MstApplication;

public interface ApplicationService {

	MstApplication save(MstApplication mstApp);
	
	List<MstApplication> findByLeadsIdAndLeadsType(Long leadsId , String leadsType);
	
	MstApplication findByCarBaru(Long leadsId);
	
	List<MstApplication> getList();
}
