package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstUserMobileDefaultMenu;

public interface UserMobileDefaultMenuService {

	List<MstUserMobileDefaultMenu> getList();
	
	Optional <MstUserMobileDefaultMenu> findById(Long id);
	
}
