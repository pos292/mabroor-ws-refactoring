package com.mabroor.ws.service;

import com.mabroor.ws.dto.PembiayaanUmrohDto;
import com.mabroor.ws.entity.*;

import java.util.Optional;

public interface PembiayaanUmrohService {

    MstPengajuanUmroh saveUmroh(PembiayaanUmrohDto pembiayaanUmrohDto, MstUserMobile userMobile);

    MstPengajuanUmroh saveStatus(MstPengajuanUmroh mstPengajuanUmroh);

    Optional<MstPengajuanUmroh> findById(Long id);

}
