package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstStatusKepemilikanRumah;

public interface StatusKepemilikanRumahService {

	List<MstStatusKepemilikanRumah> findAllByStatus();
	 
	List<MstStatusKepemilikanRumah> getListKepemilikanRumahStatus();
	
	Optional <MstStatusKepemilikanRumah> findById(Long id);
}
