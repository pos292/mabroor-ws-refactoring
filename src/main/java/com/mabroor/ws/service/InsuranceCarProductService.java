package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstGeneralInsuranceCarProduct;

public interface InsuranceCarProductService {
	
	List<MstGeneralInsuranceCarProduct> getList();
	
	Optional <MstGeneralInsuranceCarProduct> findById(Long id);
	
	List<MstGeneralInsuranceCarProduct> getListCarId(Long generalId);

}
