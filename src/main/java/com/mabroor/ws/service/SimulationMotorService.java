package com.mabroor.ws.service;

import java.util.Map;

public interface SimulationMotorService {

	Map<String, Object> getListSimulation();
}
