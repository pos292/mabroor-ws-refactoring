package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstStatusPerkawinan;

public interface StatusPerkawinanService {

	List<MstStatusPerkawinan> findAllByStatus();
	 
	List<MstStatusPerkawinan> getListPerkawinanStatus();
	
	Optional <MstStatusPerkawinan> findById(Long id);
}
