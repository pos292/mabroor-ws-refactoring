package com.mabroor.ws.service;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstLoanManagementPaylater;

public interface LoanManagementPaylaterService {
	 
	List<MstLoanManagementPaylater> getList();
	
	Optional<MstLoanManagementPaylater> getListLoanType(String loanType);
	
	Optional<MstLoanManagementPaylater> findById(Long id);
}
