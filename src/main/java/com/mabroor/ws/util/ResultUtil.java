package com.mabroor.ws.util;

import com.mabroor.ws.dto.PembiayaanUmrohRespResultDto;
import com.mabroor.ws.dto.RespResultDto;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class ResultUtil {
    private static final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

    public static <T> RespResultDto<T> createResultDto(Object status, String error, String message, T data) {
        RespResultDto<T> result = new RespResultDto<>();

        result.setTimestamp(LocalDateTime.now().toString());
        result.setRqId(UUID.randomUUID().toString());
        result.setStatus(status);
        result.setError(error);
        result.setMessage(message);
        result.setData(data);
        return result;
    }

    public static Map<String, Object> createResultMapWoData(Object status, String error, String message){
        Map<String,Object> result = new LinkedHashMap<>();
        result.put("timestamp", LocalDateTime.now().toString());
        result.put("rqId", UUID.randomUUID().toString());
        result.put("status", status);
        result.put("error", error);
        result.put("message", message);
        return result;
    }

    public static <T> PembiayaanUmrohRespResultDto<T> ResultPembiayaanUmrohDto(String error, String status, String message, T data) {
        PembiayaanUmrohRespResultDto<T> result = new PembiayaanUmrohRespResultDto<>();

        result.setReqid(UUID.randomUUID().toString());
        result.setError(error);
        result.setStatus(status);
        result.setMessage(message);
        result.setData(data);
        return result;
    }
}
