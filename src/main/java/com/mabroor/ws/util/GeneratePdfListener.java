package com.mabroor.ws.util;

import java.io.IOException;
import java.net.URL;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public final class GeneratePdfListener {
	
	static String FOOTER_IMG = "https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-footer.png";
	
	private GeneratePdfListener(){}

	public static void addFooter(PdfWriter writer, Document document) {
		PdfPTable footer = new PdfPTable(6);
        try {
            // set defaults
            footer.setWidths(new int[]{20, 20, 30, 10,10,10});
            footer.setTotalWidth(527);
            footer.setLockedWidth(true);
            footer.getDefaultCell().setFixedHeight(40);
            
            
            Image image1 = Image.getInstance(new URL(FOOTER_IMG));
            
            //RESIZE IMAGE
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / image1.getWidth()) * 100;

            // Fixed Positioning
            image1.setAlignment(1);
            image1.setScaleToFitHeight(true);
            // Scale to new height and new width of image
//            image1.scaleAbsolute(100, 20);
            image1.scalePercent(scaler);

            
            PdfPCell cell = new PdfPCell(image1);
            cell.setPadding(0);
            cell.setBorder(0);
            cell.setHorizontalAlignment(1);
            cell.setColspan(6);
	        footer.addCell(cell);
            
            // write page
            PdfContentByte canvas = writer.getDirectContent();
            canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
            footer.writeSelectedRows(0, -1, 34, 120, canvas);
            canvas.endMarkedContentSequence();
            
            System.out.println("masuk");
        } catch(DocumentException | IOException de) {
            throw new ExceptionConverter(de);
        }
	}

    public static void addFooterNoFixed(PdfWriter writer, Document document) {
        PdfPTable footer = new PdfPTable(6);
        try {
            // set defaults
            footer.setWidths(new int[]{20, 20, 30, 10,10,10});
            footer.setTotalWidth(527);
            footer.setLockedWidth(true);
            footer.getDefaultCell().setFixedHeight(40);



            Image image1 = Image.getInstance(new URL(FOOTER_IMG));

            //RESIZE IMAGE
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / image1.getWidth()) * 100;

            // Fixed Positioning
            image1.setAlignment(1);
            image1.setScaleToFitHeight(true);
            // Scale to new height and new width of image
//            image1.scaleAbsolute(100, 20);
            image1.scalePercent(scaler);


            PdfPCell cell = new PdfPCell(image1);
            cell.setPadding(0);
            cell.setBorder(0);
            cell.setHorizontalAlignment(1);
            cell.setColspan(6);
            footer.addCell(cell);

            // write page
            PdfContentByte canvas = writer.getDirectContent();
            canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
            footer.writeSelectedRows(0, -1, 34, 480, canvas);
            canvas.endMarkedContentSequence();

            System.out.println("masuk");
        } catch(DocumentException | IOException de) {
            throw new ExceptionConverter(de);
        }
    }
    public static void addFooterNoFixed2(PdfWriter writer, Document document) {
        PdfPTable footer = new PdfPTable(6);
        try {
            // set defaults
            footer.setWidths(new int[] {20,20,15,15,15,15});
            footer.setTotalWidth(527);
            footer.setLockedWidth(true);
            footer.getDefaultCell().setFixedHeight(40);

            Image image1 = Image.getInstance(new URL(FOOTER_IMG));

            //RESIZE IMAGE
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / image1.getWidth()) * 100;

            // Fixed Positioning
            image1.setAlignment(1);
            image1.setScaleToFitHeight(true);
            // Scale to new height and new width of image
//            image1.scaleAbsolute(100, 20);
            image1.scalePercent(scaler);


            PdfPCell cell = new PdfPCell(image1);
            cell.setPadding(0);
            cell.setBorder(0);
            cell.setHorizontalAlignment(1);
            cell.setColspan(8);
            footer.addCell(cell);

            // write page
            PdfContentByte canvas = writer.getDirectContent();
            canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
            footer.writeSelectedRows(0, -1, 34, 70, canvas);
            canvas.endMarkedContentSequence();

        } catch(DocumentException | IOException de) {
            throw new ExceptionConverter(de);
        }
    }
}
