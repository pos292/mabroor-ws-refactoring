package com.mabroor.ws.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;

import javax.annotation.PostConstruct;

import com.itextpdf.text.*;
import com.mabroor.ws.dto.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Component
public final class GeneratePdf {
	private static final GeneratePdf instance = new GeneratePdf();

	private static final Logger logger = LoggerFactory.getLogger(GeneratePdf.class);

	@Value("${storage.upload.file}")
	private String _fileStorageUrl;

	private static String fileStorageUrl;

	@PostConstruct     
	private void initStaticClass () {
		fileStorageUrl = this._fileStorageUrl;
	}

	static String FILE_NAME = "octopus.pdf";
	static String HEADER_IMG = "https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-header.png";

	static String FOOTER_IMG = "https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-footer.png";

	private GeneratePdf() {
	}

	public static GeneratePdf getInstance() {
		return instance;
	}

	public static String testGenerate() {

		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			byte[] bytesmontserratMedium = IOUtils.toByteArray(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("/resources/Ubuntu-Bold.ttf"));

			BaseFont baseFont_Montserrat_bold = BaseFont.createFont("Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, true, bytesmontserratMedium, null);
			Font font_Montserrat_bold = new Font(baseFont_Montserrat_bold, 11f, Font.BOLD);
			// STYLE FONT
			Font fontParagraph = new Font(baseFont_Montserrat_bold, 9, Font.NORMAL);
			Font fontTitle = FontFactory.getFont(FontFactory.HELVETICA, 15, Font.BOLD);
			Font fontSubTitle = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD);
			Font fontContent = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);
			Font fontTitleSmall = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
			Font fontDisclaimer = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.NORMAL);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,20,15,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hi Astrid,", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingBottom(20);
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/Preview-Mobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(150, 150);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Best Deal", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(4);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(""));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(4);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Toyota Avanza New Veloz 1,5 A/T", fontSubTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(4);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Rp 246.500.000", fontSubTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(4);
			table.addCell(hcell);


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.setWidthPercentage(50);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(4);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Cicilan/bulan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Rp6.400.000", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Rp61.400.000 (25%)", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Total Asuransi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Rp1.500.000", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Total Pembayaran Pertama", fontSubTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Rp61.400.000", fontSubTitle));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(2);
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfMotorDetail(GeneratePdfMotorReqDto pdfMotor) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,15,15,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfMotor.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/motor.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(150, 150);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(18);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase(pdfMotor.getModel(), fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tipe Motor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getType(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(45);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                   

			hcell = new PdfPCell(new Phrase("Model", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getModel(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(45);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                   

			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase((pdfMotor.getDp()+" " +("%")), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(45);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                        

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getTenor() + " bulan", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(45);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                   

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(325);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfMotor(GeneratePdfMotorReqDto pdfMotor) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,15,15,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfMotor.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/motor.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(150, 90);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);



			hcell = new PdfPCell(new Phrase("Motor Baru", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

//			hcell = new PdfPCell(new Phrase("Tipe Motor", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);            
//
//			hcell = new PdfPCell(new Phrase(pdfMotor.getType(), fontContent));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(45);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);                   
//
//			hcell = new PdfPCell(new Phrase("Model", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);            
//
//			hcell = new PdfPCell(new Phrase(pdfMotor.getModel(), fontContent));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(45);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);                   

			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase((pdfMotor.getDp()+" " +("%")), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(45);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                        

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getTenor() + " bulan", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(45);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                   

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(325);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfMotorDetailV2(GeneratePdfMotorV2ReqDto pdfMotor) {
		logger.info(new Gson().toJson(pdfMotor));

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,15,15,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfMotor.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-motor.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(24);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase(pdfMotor.getModel(), fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            
			
			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     
			
			hcell = new PdfPCell(new Phrase("Pembiayaan Motor Syariah", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Data Detail Motor", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 
			
			hcell = new PdfPCell(new Phrase("Nomor Kontrak FIFGROUP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			if(pdfMotor.getContractNumber() == null || "".equalsIgnoreCase(pdfMotor.getContractNumber())) {				
				hcell = new PdfPCell(new Phrase("-", fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setPaddingBottom(10);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			}else {
				hcell = new PdfPCell(new Phrase(pdfMotor.getContractNumber(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setPaddingBottom(10);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			}
			
			
			hcell = new PdfPCell(new Phrase("FIFGROUP Card Number", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			if(pdfMotor.getFifgroupCardnumber() == null || "".equalsIgnoreCase(pdfMotor.getFifgroupCardnumber())) {				
				hcell = new PdfPCell(new Phrase("-", fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setPaddingBottom(10);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			}else {
				hcell = new PdfPCell(new Phrase(pdfMotor.getFifgroupCardnumber(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setPaddingBottom(10);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			}
						

			hcell = new PdfPCell(new Phrase("Jenis Motor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getType(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);              

			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase((pdfMotor.getDp()+" " +("%")), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                        

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getTenor() + " bulan", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     

			hcell = new PdfPCell(new Phrase("Detail Data Diri", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Jenis Kelamin", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getGender(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     
			
			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getFirstName(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getLastName(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getBirthday(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    
			
			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getPhoneDataUser(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Nomor Ponsel Lainnya", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("+62".equals(pdfMotor.getPhoneDataUserOptional()) || "".equals(pdfMotor.getPhoneDataUserOptional()) ? "-" : pdfMotor.getPhoneDataUserOptional(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     
			
			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getEmail(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    
			
			hcell = new PdfPCell(new Phrase("Nomor KTP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getKtpNumber(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    
			
			hcell = new PdfPCell(new Phrase("Alamat Lengkap", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getAddress(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    
			
			hcell = new PdfPCell(new Phrase("Provinsi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getProvince(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    
			
			hcell = new PdfPCell(new Phrase("Kabupaten/Kota", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getCity(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Kecamatan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getSubDistrict(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Kelurahan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getVillage(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getZipCode(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(90);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfMotorV2(GeneratePdfMotorV2ReqDto pdfMotor) {
		
		logger.info(new Gson().toJson(pdfMotor));

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,15,15,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfMotor.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-motor.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Pembiayaan Motor Syariah", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);              


			hcell = new PdfPCell(new Phrase("Jenis Motor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getType(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);              

			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase((pdfMotor.getDp()+" " +("%")), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                        

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMotor.getTenor() + " bulan", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                       

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(420);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfMobilDetail(GeneratePdfMobilReqDto pdfMobil) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 861);
		Document document = new Document(pagesize);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfMobil.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut detail pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-mobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(19);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Pembiayaan Mobil Syariah", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);        
			
			hcell = new PdfPCell(new Phrase("Kota Pengajuan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getKota(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         
			
			hcell = new PdfPCell(new Phrase("Tahun Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getTahun(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     
			
			hcell = new PdfPCell(new Phrase("Brand Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getBrand(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         
			
			hcell = new PdfPCell(new Phrase("Tipe Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getTipe(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         

			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getDp()+"%", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                        

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMobil.getTenor() + " bulan", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     

			if(pdfMobil.getCompany() == null || !"TAF".equalsIgnoreCase(pdfMobil.getCompany())) {
			hcell = new PdfPCell(new Phrase("Detail Data Diri", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Jenis Kelamin", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMobil.getGender(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     
			
			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMobil.getNamaDepan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMobil.getNamaBelakang(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMobil.getBirth(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    
			
			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMobil.getPhone(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMobil.getEmail(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    
			}
			else if(pdfMobil.getCompany() != null && !"".equalsIgnoreCase(pdfMobil.getCompany()) && "TAF".equalsIgnoreCase(pdfMobil.getCompany())) {
				hcell = new PdfPCell(new Phrase("Asuransi", fontContentBold2));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(4);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell); 

				hcell = new PdfPCell(new Phrase("Nominal Asuransi", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);            

				hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getNominalAsuransi()).replace(",00", ""), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase("Perluasan Asuransi", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);            

				hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getPerluasanAsuransi()).replace(",00", ""), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase("Polis Asuransi", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);            

				hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getPolisAsuransi()).replace(",00", ""), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase("Total Asuransi", fontContentBold));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);            

				hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getTotalAsuransi()).replace(",00", ""), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);				
				}

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(250);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfMobil(GeneratePdfMobilReqDto pdfMobil) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfMobil.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-mobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(18);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Pembiayaan Mobil Syariah", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);        
			
			hcell = new PdfPCell(new Phrase("Kota Pengajuan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getKota(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         
			
			hcell = new PdfPCell(new Phrase("Tipe Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getTipe(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         

			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			if(new BigDecimal(pdfMobil.getDp()).compareTo(new BigDecimal(100))<= 0) {
			hcell = new PdfPCell(new Phrase(pdfMobil.getDp()+"%", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                        
			}
			else {
				hcell = new PdfPCell(new Phrase(fmt.format(new BigDecimal(pdfMobil.getDp())).replace(",00", ""), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);                     
			}

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(450);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfMobilTaf(GeneratePdfMobilReqDto pdfMobil) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfMobil.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-mobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 140);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(18);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Pembiayaan Mobil Syairah", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);        
			
			hcell = new PdfPCell(new Phrase("Kota Pengajuan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getKota(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         
			
			hcell = new PdfPCell(new Phrase("Tahun Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getTahun(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);     
			
			hcell = new PdfPCell(new Phrase("Brand Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getBrand(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         
			
			hcell = new PdfPCell(new Phrase("Tipe Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getTipe(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         

			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfMobil.getDp()+"%", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);                        

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase(pdfMobil.getTenor() + " bulan", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);   
			
			hcell = new PdfPCell(new Phrase("Asuransi", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nominal Asuransi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getNominalAsuransi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Perluasan Asuransi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getPerluasanAsuransi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Polis Asuransi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getPolisAsuransi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Total Asuransi", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getTotalAsuransi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(250);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

//	public static String generatePdfMobilBekas(GeneratePdfMobilReqDto pdfMobil) {
//
//		Locale localeCurrency = new Locale("id", "ID");
//		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
//		Document document = new Document();
//		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
//		ByteArrayOutputStream out = new ByteArrayOutputStream();
//		String fullFileName = "";
//		try {
//
//			// STYLE FONT
//			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
//			fontParagraph.setColor(19, 29, 41);
//			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
//			fontTitle.setColor(19, 29, 41);
//			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
//			fontSubTitle.setColor(19, 29, 41);
//			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
//			fontContent.setColor(19, 29, 41);
//			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
//			fontContentBold.setColor(19, 29, 41);
//			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
//			fontContentBold2.setColor(19, 29, 41);
//
//			String directoryName = fileStorageUrl + "temp-pdf/";
//			File directory = new File(directoryName);
//			if (!directory.exists()) {
//				directory.mkdir();
//			}
//
//			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);
//
//			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
//			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
//			document.addCreationDate();
//			document.addTitle("PDF Pengajuan");
//			document.open();
//
//			Image image1 = Image.getInstance(new URL(HEADER_IMG));
//
//			//RESIZE IMAGE
//			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
//					- document.rightMargin() - 0) / image1.getWidth()) * 100;
//
//			// Fixed Positioning
//			image1.setAlignment(1);
//			image1.setScaleToFitHeight(true);
//			// Scale to new height and new width of image
//			//            image1.scaleAbsolute(100, 20);
//			image1.scalePercent(scaler);
//			// Add to document
//			//            document.add(image1);
//
//
//			//PDF HEADER
//			PdfPTable table = new PdfPTable(6);
//
//			table.setWidthPercentage(100);
//			table.setWidths(new int[] {15,15,20,10,20,20});
//
//			PdfPCell cell;
//
//			cell = new PdfPCell(image1);
//			cell.setBorder(0);
//			cell.setColspan(6);
//			cell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(cell);
//
//
//			//PDF BODY
//
//			PdfPCell hcell;
//			hcell = new PdfPCell(new Phrase("Hai " + pdfMobil.getName() + ",", fontParagraph));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(6);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setColspan(6);
//			hcell.setPaddingLeft(30);
//			hcell.setPaddingBottom(20);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/mobilbekas.png"));       
//			imageUnit.setAlignment(1);
//			imageUnit.setScaleToFitHeight(true);
//			imageUnit.scaleAbsolute(100, 100);
//
//			hcell = new PdfPCell(imageUnit);
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre,  2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setRowspan(10);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase(pdfMobil.getUnitName(), fontContentBold2));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setPaddingBottom(2);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getOtr()).replace(",00", ""), fontContentBold2));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setPaddingTop(2);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//
//			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
//			imageDot.setAlignment(1);
//			imageDot.scaleAbsolute(305, 10);
//
//			hcell = new PdfPCell(imageDot);
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingTop(10);
//			hcell.setPaddingLeft(30);
//			hcell.setPaddingBottom(10);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase("Detail Kendaraan", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//
//			hcell = new PdfPCell(new Phrase(pdfMobil.getUnitName(), fontContent));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase("Harga Mobil", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getOtr()).replace(",00", ""), fontContent));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase("Down Payment", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//
//			hcell = new PdfPCell(new Phrase(fmt.format(pdfMobil.getDpAmount()).replace(",00", ""), fontContent));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell(new Phrase(pdfMobil.getTenor() + " bulan", fontContent));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			hcell = new PdfPCell();
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(6);
//			hcell.setFixedHeight(300);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			document.add(table);
//
//			GeneratePdfListener.addFooter(pdfWriter, document);
//
//			document.close();
//
//		} catch (DocumentException | IOException ex) {
//
//			logger.error("Error occurred: {0}", ex);
//		}
//
//		try {
//			Files.deleteIfExists(Paths.get(fullFileName));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
//		return encoded;
//	}

	public static String generatePdfJaminan(GeneratePdfJaminanReqDto pdfJaminan) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfJaminan.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-danatunai.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(StringUtils.capitalize(pdfJaminan.getJenisPinjaman()), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nominal Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfJaminan.getJumlahPinjaman()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);			

			hcell = new PdfPCell(new Phrase("Kebutuhan Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfJaminan.getKebutuhanPinjaman(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfJaminan.getTenor() + " bulan", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(450);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfJaminanDetail(GeneratePdfJaminanReqDto pdfJaminan) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfJaminan.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-danatunai.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(24);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(StringUtils.capitalize(pdfJaminan.getJenisPinjaman()), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Detail Jaminan", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Tahun Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(StringUtils.capitalize(pdfJaminan.getTahunKendaraan()), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Brand Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(StringUtils.capitalize(pdfJaminan.getBrandKendaraan()), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Tipe Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(StringUtils.capitalize(pdfJaminan.getTipeKendaraan()), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Kota Pengajuan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(StringUtils.capitalize(pdfJaminan.getKotaPengajuan()), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Detail Pinjaman", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nominal Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfJaminan.getJumlahPinjaman()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);			

			hcell = new PdfPCell(new Phrase("Kebutuhan Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfJaminan.getKebutuhanPinjaman(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfJaminan.getTenor() + " bulan", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Detail Peminjam", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Lengkap", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfJaminan.getNamaLengkap(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfJaminan.getPhone(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfJaminan.getEmail(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(150);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfTanpaJaminan(GeneratePdfTanpaJaminanReqDto pdfTanpaJaminan) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfTanpaJaminan.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/danaTunai.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nominal Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfTanpaJaminan.getNominalLoan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Jenis Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("-", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kebutuhan Pinjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfTanpaJaminan.getLoanNeeds(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfTanpaJaminan.getTenor() + " bulan", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(350);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfAsuransiJiwa(GeneratePdfAsuransiJiwaReqDto pdfJiwa) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 16, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 21, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfJiwa.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan penawaran produkmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(30);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransijiwadankritis.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Asuransi Jiwa", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Usia", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfJiwa.getUsia() + " Tahun", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Jenis Kelamin", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfJiwa.getGender(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Uang Pertanggungan Mulai Dari", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase(fmt.format(pdfJiwa.getUangPertanggungan()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Premi Per Bulan Mulai Dari", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfJiwa.getMonthlyPremium()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Per Tahun Mulai Dari", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfJiwa.getYearlyPremium()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(50);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfPenyakitKritis(GeneratePdfPenyakitKritisReqDto pdfKritis) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 21, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 16, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 16, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 16, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 16, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfKritis.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan penawaran produkmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransijiwadankritis.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Asuransi Penyakit Kritis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Usia", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfKritis.getUsia() + " Tahun", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Jenis Kelamin", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfKritis.getGender(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Uang Pertanggungan Mulai Dari", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase(fmt.format(pdfKritis.getUangPertanggungan()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Premi Per Bulan Mulai Dari", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfKritis.getMonthlyPremium()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Per Tahun Mulai Dari", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfKritis.getYearlyPremium()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(150);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfKurbanRingkasan(GeneratePdfKurbanReqDto pdfKurban) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfKurban.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/FotoKurban.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Harga Akhir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfKurban.getHargaAkhir()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Pilihan Hewan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfKurban.getPilihanHewan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Jumlah Hewan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfKurban.getJumlahHewan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Pilihan Partner", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfKurban.getPilihanPartner(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(400);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfWisataReligiUmroh(GeneratePdfWisataReligiReqDto pdfWisata) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfWisata.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/FotoUmroh.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Perjalanan Religi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfWisata.getProductChoose(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Tanggal Keberangkatan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date = new Date();
			try {
				date = format.parse(pdfWisata.getDateDispatch());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatDispatch = formatter.format(date);
			hcell = new PdfPCell(new Phrase(formatDispatch, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(400);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfWisataReligiHaji(GeneratePdfWisataReligiReqDto pdfWisata) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfWisata.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-haji.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);       


			hcell = new PdfPCell(new Phrase("Produk Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase(pdfWisata.getProductChoose(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Paket Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase(pdfWisata.getProductPurchase(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Partner Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase(pdfWisata.getPartner(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
//			hcell = new PdfPCell(new Phrase("Preferensi Tanggal Keberangkatan", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//			Date date = new Date();
//			try {
//				date = format.parse(pdfWisata.getDateDispatch());
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
//			String formatDispatch = formatter.format(date);
//			hcell = new PdfPCell(new Phrase(formatDispatch, fontContentBold));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("DP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase(fmt.format(pdfWisata.getDp()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfWisata.getTenor() + " bulan", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(400);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfWisataReligiHajiAcc(GeneratePdfWisataReligiReqDto pdfWisata) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfWisata.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-haji.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);       


			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Pembiayaan Haji", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Partner Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("ACC Syariah", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Paket Haji", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfWisata.getProductPurchase(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfWisata.getTenor() + " Tahun", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(450);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfWisataReligiHajiAccDetail(GeneratePdfWisataReligiReqDto pdfWisata) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 861);
		Document document = new Document(pagesize);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY    

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfWisata.getName() + ",", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(30);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut detail pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-haji.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(19);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Pembiayaan Haji", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Partner Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("ACC Syariah", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Paket Haji", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfWisata.getProductPurchase(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jumlah Peserta", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfWisata.getJumlahPeserta() + " Orang", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfWisata.getTenor() + " Tahun",fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Lokasi Cabang ACC", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfWisata.getLokasiCabang(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Pembayaran Pertama", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(new Phrase(fmt.format(pdfWisata.getDp()).replace(",00", ""), fontContentBold)));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Perkiraan Total Biaya", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(new Phrase(fmt.format(pdfWisata.getAmountCostTotal()).replace(",00", ""), fontContentBold)));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Angsuran/bulan", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(new Phrase(fmt.format(pdfWisata.getAmountCicilan()).replace(",00", ""), fontContentBold)));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Detail Data Diri", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(new Phrase(pdfWisata.getNamaDepan(), fontContentBold)));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(new Phrase(pdfWisata.getNamaBelakang(), fontContentBold)));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date = new Date();
			try {
				date = format.parse(pdfWisata.getTanggalLahir());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatTglLahir = formatter.format(date);
			hcell = new PdfPCell(new Phrase(formatTglLahir, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(new Phrase(pdfWisata.getNoHandphone(), fontContentBold)));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(new Phrase(pdfWisata.getEmail() , fontContentBold)));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(250);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfAsuransiMobil(GeneratePdfAsuransiMobilReqDto pdfAsuransiMobil) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 12, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 16, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAsuransiMobil.getName() + ",", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingTop(20);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-asuransi-mobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(140, 160);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(16);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiMobil.getJenisAsuransi(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         

			hcell = new PdfPCell(new Phrase("Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiMobil.getKendaraan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Kontribusi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiMobil.getKontribusiDasar()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Estimasi Total Kontribusi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiMobil.getEstimasiTotal()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(8);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(400);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfAsuransiMobilDetail(GeneratePdfAsuransiMobilReqDto pdfAsuransiMobil) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 861);
		Document document = new Document(pagesize);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 12, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 16, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);


			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAsuransiMobil.getName() + ",", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setPaddingTop(20);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut detail pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setColspan(6);
			hcell.setPaddingLeft(25);
			hcell.setPaddingBottom(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/mabroor-asuransi-mobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(130, 160);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(25);
			hcell.setColspan(2);
			hcell.setRowspan(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiMobil.getJenisAsuransi(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Data Kendaraan", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiMobil.getKendaraan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Kode Wilayah Plat", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(2);
			hcell.setPaddingTop(3);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiMobil.getKodeWilayahPlat(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Detail Pertanggungan", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Harga Pertanggungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiMobil.getSumIssured()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Periode Pertanggungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiMobil.getPeriodePertanggungan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kontribusi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiMobil.getKontribusiDasar()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			if(!pdfAsuransiMobil.getBenefit().isEmpty()) {
			hcell = new PdfPCell(new Phrase("Perluasan Jaminan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			}
			for (AsuransiMobilBenefitDto item : pdfAsuransiMobil.getBenefit()) {

				hcell = new PdfPCell(new Phrase(item.getCoverageDes(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(25);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				int x = 4;
				boolean z = item.isChecked();
				String s1 = Boolean.toString(z);
				logger.info(s1);
				if("Huru Hara, Kerusuhan, Banjir, Angin Topan, Gempa Bumi, Tsunami, Gunung Meletus, Tanah Longsor".equalsIgnoreCase(item.getCoverageDes())
						&& "Garda Oto Syariah - Total Loss Only".equalsIgnoreCase(pdfAsuransiMobil.getProductTitle())) {
					hcell = new PdfPCell(new Phrase("Sudah Termasuk", fontContentBold));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(7);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else if("Huru Hara, Kerusuhan, Banjir, Angin Topan, Gempa Bumi, Tsunami, Gunung Meletus, Tanah Longsor".equalsIgnoreCase(item.getCoverageDes())
						&& "Garda Oto Syariah - Comprehensive".equalsIgnoreCase(pdfAsuransiMobil.getProductTitle()) && pdfAsuransiMobil.getSumIssured().compareTo(new BigDecimal(200000000)) > 0) {
					hcell = new PdfPCell(new Phrase("Sudah Termasuk", fontContentBold));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(7);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}
//				else if("Huru Hara, Kerusuhan, Banjir, Angin Topan, Gempa Bumi, Tsunami, Gunung Meletus, Tanah Longsor".equalsIgnoreCase(item.getCoverageDes())
//						&& "Garda Oto Syairah - Comprehensive".equalsIgnoreCase(pdfAsuransiMobil.getProductTitle()) && pdfAsuransiMobil.getSumIssured().compareTo(new BigDecimal(200000000)) > 0) {
//					hcell = new PdfPCell(new Phrase("Tidak Termasuk", fontContentBold));
//					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//					hcell.setBorder(0);
//					hcell.setPadding(7);
//					hcell.setPaddingRight(30);
//					hcell.setColspan(2);
//					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//					table.addCell(hcell);
//				}else if("Huru Hara, Kerusuhan, Banjir, Angin Topan, Gempa Bumi, Tsunami, Gunung Meletus, Tanah Longsor".equalsIgnoreCase(item.getCoverageDes())
//						&& "Garda Oto Syairah - Total Loss Only".equalsIgnoreCase(pdfAsuransiMobil.getProductTitle()) && pdfAsuransiMobil.getSumIssured().compareTo(new BigDecimal(200000000)) > 0) {
//					hcell = new PdfPCell(new Phrase("Tidak Termasuk", fontContentBold));
//					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//					hcell.setBorder(0);
//					hcell.setPadding(7);
//					hcell.setPaddingRight(30);
//					hcell.setColspan(2);
//					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//					table.addCell(hcell);
//				}
//				else if("Huru Hara, Kerusuhan, Banjir, Angin Topan, Gempa Bumi, Tsunami, Gunung Meletus, Tanah Longsor".equalsIgnoreCase(item.getCoverageDes())
//						&& "Garda Oto Syairah - Lite Package Comprehensive".equalsIgnoreCase(pdfAsuransiMobil.getProductTitle()) && pdfAsuransiMobil.getSumIssured().compareTo(new BigDecimal(200000000)) > 0) {
//					hcell = new PdfPCell(new Phrase("Tidak Termasuk", fontContentBold));
//					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//					hcell.setBorder(0);
//					hcell.setPadding(7);
//					hcell.setPaddingRight(30);
//					hcell.setColspan(2);
//					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//					table.addCell(hcell);
//				}
				else {
					hcell = new PdfPCell(new Phrase(fmt.format(item.getAmountPremi()).replace(",00", ""), fontContentBold));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(7);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}
			}
			hcell = new PdfPCell(new Phrase("Estimasi Total Kontribusi"+"\n"+
			"(Sudah termasuk biaya administrasi sebesar Rp50.000)", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingLeft(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiMobil.getEstimasiTotal()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(7);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(50);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Gson gson = new Gson();
			String json = gson.toJson(pdfAsuransiMobil);
			logger.info(json);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfPengajuanAlatBerat(GeneratePdfPengajuanAlatBeratReqDto pdfAlatBerat) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAlatBerat.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/alatberat.jpg"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Sektor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBerat.getSektor(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Brand", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBerat.getBrand(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tipe", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBerat.getTipe(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Model", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBerat.getModel(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Daerah Pemakaian", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBerat.getDaerahPemakaian(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(350);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfPengajuanAlatBeratBekas(GeneratePdfPengajuanAlatBeratBekasReqDto pdfAlatBeratBekas) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAlatBeratBekas.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/alatberat.jpg"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Sektor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getSektor(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Brand", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getBrand(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tipe", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getTipe(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Model", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getModel(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Daerah Pemakaian", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getDaerahPemakaian(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Rentang Tahun", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getRentangTahun(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kondisi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getKondisi(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Waktu Penggunaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getWaktuPenggunaan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Cara Pembayaran", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAlatBeratBekas.getCaraPembayaran(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(350);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();
			

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfAsuransiKebakaran(GeneratePdfAsuransiKebakaranReqDto pdfAsuransiKebakaran) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAsuransiKebakaran.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransirumah.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(150, 150);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(12);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Asuransi Kebakaran", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nama Pemegang Polis", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getNamePemegangPolis(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);         

			hcell = new PdfPCell(new Phrase("Jenis Paket Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getJenisPaketPerlindungan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kepemilikan Bangunan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getKepemilikanBangunan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Harga Bangunan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getHargaBangunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

//			hcell = new PdfPCell(new Phrase("Alamat Lokasi Risiko", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//
//			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getAlamatRisiko(), fontContent));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getPremiDasar()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Biaya Administrasi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getBiayaAdmin()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Estimasi Total Premi", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getTotalPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("(Sudah termasuk biaya administrasi sebesar Rp.50.000)", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(350);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfAsuransiKebakaranDetail(GeneratePdfAsuransiKebakaranReqDto pdfAsuransiKebakaran) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 861);
		Document document = new Document(pagesize);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAsuransiKebakaran.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransirumah.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(150, 150);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(19);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Asuransi Kebakaran", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    

			hcell = new PdfPCell(new Phrase("Premi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getPremiDasar()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Harga Bangunan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getHargaBangunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Biaya Administrasi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getBiayaAdmin()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			//            hcell = new PdfPCell(new Phrase("Periode Perlindungan", fontContent));
			//            hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			//            hcell.setBorder(0);
			//            hcell.setPadding(5);
			//            hcell.setPaddingLeft(30);
			//            hcell.setColspan(2);
			//            hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			//            table.addCell(hcell);
			//            
			//            
			//            hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getPeriodePertanggunan() + " tahun", fontContent));
			//            hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			//            hcell.setBorder(0);
			//            hcell.setPadding(5);
			//            hcell.setPaddingRight(30);
			//            hcell.setColspan(2);
			//            hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			//            table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Total Premi yang harus dibayarkan", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getTotalPremi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Lengkap", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getNamePemegangPolis(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getTanggalLahir(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getNoIdentitas(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getAlamat(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getKodePos(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(20);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Data Bangunan", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Kepemilikan Bangunan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getKepemilikanBangunan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Harga Bangunan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKebakaran.getHargaBangunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Lokasi Risiko", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiKebakaran.getAlamatRisiko(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(4);
			hcell.setFixedHeight(150);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfRentCar(GeneratePdfRentCarDto pdfRentCar) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfRentCar.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/rentalmobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);                                   


			hcell = new PdfPCell(new Phrase("Sewa Mobil", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);           
			hcell.setColspan(2);         
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			String jenis = "";
			if("YES".equals(pdfRentCar.getWithDriver())) {
				jenis = "Sewa Mobil\n(" + "Dengan Driver" + ")";
			}else {
				jenis = "Sewa Mobil\n(" + "Tanpa Driver" + ")";
			}                                             

			hcell = new PdfPCell(new Phrase(jenis, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Kota", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getKota(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Waktu Penjemputan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date = new Date();
			try {
				date = format.parse(pdfRentCar.getPickUpTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH.mm");
			String formatJemput = formatter.format(date) + "\n" + formatter2.format(date);   		
			hcell = new PdfPCell(new Phrase(formatJemput, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Model Mobil", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getModelMobil(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Paket Sewa", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getPaketSewa(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Paket Tambahan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			String added = "";
			if(pdfRentCar.getPaketTambahan() != null && !"".equals(pdfRentCar.getPaketTambahan())) {
				added = pdfRentCar.getPaketTambahan();
			}else {
				added = "-";
			}

			hcell = new PdfPCell(new Phrase(added, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(300);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfRentCarDenganDriver(GeneratePdfRentCarDto pdfRentCar) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfRentCar.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan informasi pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/rentalmobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);                                   


			hcell = new PdfPCell(new Phrase("Sewa Mobil", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);           
			hcell.setColspan(2);         
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Sewa Mobil\n(" + "Dengan Driver" + ")", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Kota", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getKota(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Waktu Penjemputan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date = new Date();
			try {
				date = format.parse(pdfRentCar.getPickUpTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm");
			String formatJemput = formatter.format(date) + "\n" + formatter2.format(date);   		
			hcell = new PdfPCell(new Phrase(formatJemput, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Paket Sewa", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			String paketSewa = pdfRentCar.getPaketSewa()+ "," + "\n" + pdfRentCar.getPaketSewaJam();   	
			hcell = new PdfPCell(new Phrase(paketSewa, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Durasi Sewa", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date1 = new Date();
			try {
				date1 = formats.parse(pdfRentCar.getDurasiSewaTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatters = new SimpleDateFormat("dd-MM-yyyy");
			String durasiSewa = pdfRentCar.getDurasiSewa()+ "," + "\n" + formatters.format(date1);   	
			hcell = new PdfPCell(new Phrase(durasiSewa, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Jenis Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getModelMobil(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Total Biaya", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfRentCar.getTotalBiaya()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(300);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfRentCarDenganDriverDetail(GeneratePdfRentCarDto pdfRentCar) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfRentCar.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan informasi pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/rentalmobil.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(40);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);                                   


			hcell = new PdfPCell(new Phrase("Sewa Mobil", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);           
			hcell.setColspan(2);         
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Sewa Mobil\n(" + "Dengan Driver" + ")", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Kota", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getKota(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Waktu Penjemputan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date = new Date();
			try {
				date = format.parse(pdfRentCar.getPickUpTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm");
			String formatJemput = formatter.format(date) + "\n" + formatter2.format(date);   		
			hcell = new PdfPCell(new Phrase(formatJemput, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Paket Sewa", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			String paketSewa = pdfRentCar.getPaketSewa()+ "," + "\n" + pdfRentCar.getPaketSewaJam();   	
			hcell = new PdfPCell(new Phrase(paketSewa, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Durasi Sewa", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date1 = new Date();
			try {
				date1 = formats.parse(pdfRentCar.getDurasiSewaTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatters = new SimpleDateFormat("dd-MM-yyyy");
			String durasiSewa = pdfRentCar.getDurasiSewa()+ "," + "\n" + formatters.format(date1);   	
			hcell = new PdfPCell(new Phrase(durasiSewa, fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Jenis Kendaraan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getModelMobil(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Lokasi Penjemputan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getLokasi(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Catatan"+ "\n"+"(Opsional)", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			if(pdfRentCar.getCatatan() == null && "".equalsIgnoreCase(pdfRentCar.getCatatan())) {
			hcell = new PdfPCell(new Phrase("-", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			}else {
				hcell = new PdfPCell(new Phrase(pdfRentCar.getCatatan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			}
			hcell = new PdfPCell(new Phrase("Total Biaya", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfRentCar.getTotalBiaya()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Data Diri", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getNamaDepan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getNamaBelakang(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date2 = new Date();
			try {
				date2 = formats.parse(pdfRentCar.getTanggalLahir());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatterss = new SimpleDateFormat("dd MMMM yyyy");
			hcell = new PdfPCell(new Phrase(formatterss.format(date2), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Jenis Kelamin", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getJenisKelamin(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getNoPonsel(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getEmail(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nomor Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentCar.getNoIdenty(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(120);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfRentBus(GeneratePdfRentBusDto pdfRentBus) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED,14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfRentBus.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/rentalbus.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);                                   


			hcell = new PdfPCell(new Phrase("Sewa Bus", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);           
			hcell.setColspan(2);         
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			String jenis = "";
			if("YES".equals(pdfRentBus.getDalamKota())) {
				jenis = "Sewa Bus\n(" + "Dalam Kota" + ")";
			}else {
				jenis = "Sewa Bus\n(" + "Luar Kota" + ")";
			}                                             

			hcell = new PdfPCell(new Phrase(jenis, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            

			hcell = new PdfPCell(new Phrase("Alamat Jemput", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentBus.getAlamatJemput(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Destinasi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentBus.getAlamatDestinasi(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Tanggal & Jam Penjemputan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date = new Date();
			try {
				date = format.parse(pdfRentBus.getPickUpTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH.mm");    		
			String formatJemput = formatter.format(date);   		
			hcell = new PdfPCell(new Phrase(formatJemput, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Durasi Sewa", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRentBus.getDurasiSewa(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jumlah Penumpang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);           

			hcell = new PdfPCell(new Phrase(pdfRentBus.getJumlahPenumpang(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(300);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfAsuransiKecelakaan(GeneratePdfAsuransiKecelakaanReqDto pdfAsuransiKecelakaan) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAsuransiKecelakaan.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(19);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Asuransi Kecelakaan - " + pdfAsuransiKecelakaan.getNameProduct(), fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);      

			hcell = new PdfPCell(new Phrase("Jenis Paket Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getType(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKecelakaan.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			for(GeneratePdfAsuransiKecelakaanBenefitReqDto item: pdfAsuransiKecelakaan.getBenefits()) {
				hcell = new PdfPCell(new Phrase(item.getBenefit(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				if("AMOUNT".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(fmt.format(new BigDecimal(item.getValue())).replace(",00", ""), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else if("PERIODE".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(item.getValue() + " Tahun", fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else {
					hcell = new PdfPCell(new Phrase(item.getValue(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}
			}         

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(300);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfAsuransiKecelakaanDetail(GeneratePdfAsuransiKecelakaanReqDto pdfAsuransiKecelakaan) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 861);
		Document document = new Document(pagesize);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAsuransiKecelakaan.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(110, 110);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(19);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Asuransi Kecelakaan - " + pdfAsuransiKecelakaan.getNameProduct(), fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);   

			hcell = new PdfPCell(new Phrase("Jenis Paket Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getType(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfAsuransiKecelakaan.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			for(GeneratePdfAsuransiKecelakaanBenefitReqDto item: pdfAsuransiKecelakaan.getBenefits()) {
				hcell = new PdfPCell(new Phrase(item.getBenefit(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				if("AMOUNT".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(fmt.format(new BigDecimal(item.getValue())).replace(",00", ""), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else if("PERIODE".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(item.getValue() + " Tahun", fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else {
					hcell = new PdfPCell(new Phrase(item.getValue(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}
			}  

			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Lengkap", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getFullName(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getBirthDate(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getIdentityNumber(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getAddress(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAsuransiKecelakaan.getZipCode(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(20);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(4);
			hcell.setFixedHeight(150);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfAirportTransfer(GeneratePdfAirportTransferReqDto pdfAirportTransfer) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfAirportTransfer.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/airport.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Airport Transfer", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfAirportTransfer.getServiceType(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			if("AIRPORTTOADDRESS".equals(pdfAirportTransfer.getDestinationType())) {
				hcell = new PdfPCell(new Phrase("Dari (Bandara)", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfAirportTransfer.getAirport(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Ke (Alamat)", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);


				hcell = new PdfPCell(new Phrase(pdfAirportTransfer.getAddress(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			}else {
				hcell = new PdfPCell(new Phrase("Dari (Alamat)", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfAirportTransfer.getAddress(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Ke (Bandara)", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);


				hcell = new PdfPCell(new Phrase(pdfAirportTransfer.getAirport(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			}


			Date date = new Date();
			try {
				date = format.parse(pdfAirportTransfer.getDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH.mm");    		
			String formatJemput = formatter.format(date);

			hcell = new PdfPCell(new Phrase("Tanggal & Waktu Penjemputan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(formatJemput, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Model Mobil", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfAirportTransfer.getCarModel(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(300);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfGardaEdu(GeneratePdfGardaEduReqDto pdfGardaEdu) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfGardaEdu.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(12);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Asuransi Kecelakaan", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Paket Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Garda Edu", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

			hcell = new PdfPCell(new Phrase("Pekerjaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getJob(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Yang Diinginkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaEdu.getPremiDiinginkan()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Estimasi Total Premi", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaEdu.getTotalPremiBayar()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("(Sudah termasuk biaya administrasi sebesar Rp25.000)", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(0);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(380);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfGardaEduDetail(GeneratePdfGardaEduReqDto pdfGardaEdu) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 945);
		Document document = new Document(pagesize);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfGardaEdu.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(140, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(30);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Garda Edu", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(315, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi yang Diinginkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaEdu.getPremiDiinginkan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Detail Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			for(GeneratePdfGardaEduBenefitReqDto item: pdfGardaEdu.getDetailPerlindungan()) {
				hcell = new PdfPCell(new Phrase(item.getBenefit(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				if("AMOUNT".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(fmt.format(new BigDecimal(item.getValue())).replace(",00", ""), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else if("PERIODE".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(item.getValue() + " Tahun", fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else {
					hcell = new PdfPCell(new Phrase(item.getValue(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}
			}  
			
			hcell = new PdfPCell(new Phrase("Biaya Administrasi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaEdu.getBiayaAdmin()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Periode Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getPeriodePerlindungan() + " Tahun", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Total Premi yang Harus Dibayarkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaEdu.getTotalPremiBayar()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getNameDepan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getNameBelakang(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			Date date = new Date();
			try {
				date = format.parse(pdfGardaEdu.getBirthDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatBirthdate = formatter.format(date);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(formatBirthdate, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getIdentityNumb(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getAddress(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getKodePos(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Pekerjaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getJob(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Ahli Waris", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Ahli Waris", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(WordUtils.capitalizeFully(pdfGardaEdu.getAhliWaris().toLowerCase()), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Hubungan Ahli Waris", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaEdu.getHubunganAhliWaris(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nama Anak Kandung 1", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(WordUtils.capitalizeFully(pdfGardaEdu.getAnakKandung1().toLowerCase()), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Anak Kandung 2", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

				String a = "";
				if(pdfGardaEdu.getAnakKandung2() != null && !" ".equals(pdfGardaEdu.getAnakKandung2())) {
					a = WordUtils.capitalizeFully(pdfGardaEdu.getAnakKandung2().toLowerCase());
				}else {
					a = "-";
				}
			hcell = new PdfPCell(new Phrase(a, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(4);
			hcell.setFixedHeight(80);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfGardaMe(GeneratePdfGardaMeReqDto pdfGardaMe) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfGardaMe.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(12);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Asuransi Kecelakaan", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Jenis Paket Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Garda Me", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Pekerjaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaMe.getJob(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

			hcell = new PdfPCell(new Phrase("Premi Yang Diinginkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaMe.getPremi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Estimasi Total Premi", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaMe.getTotalPremiBayar()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("(Sudah termasuk biaya administrasi sebesar Rp25.000)", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(0);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(380);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfGardaMeDetail(GeneratePdfGardaMeReqDto pdfGardaMe) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 900);
		Document document = new Document(pagesize);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfGardaMe.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(140, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(30);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Garda Me", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(315, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaMe.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Detail Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			for(GeneratePdfGardaMeBenefitReqDto item: pdfGardaMe.getBenefits()) {
				hcell = new PdfPCell(new Phrase(item.getBenefit(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				if("AMOUNT".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(fmt.format(new BigDecimal(item.getValue())).replace(",00", ""), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else if("PERIODE".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(item.getValue() + " Tahun", fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else {
					hcell = new PdfPCell(new Phrase(item.getValue(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}
			} 
			
			hcell = new PdfPCell(new Phrase("Biaya Administrasi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaMe.getBiayaAdmin()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Periode Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaMe.getPeriode() + " Tahun", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Total Premi yang Harus Dibayarkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaMe.getTotalPremiBayar()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMe.getNameDepan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMe.getNameBelakang(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			Date date = new Date();
			try {
				date = format.parse(pdfGardaMe.getBirthDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatBirthdate = formatter.format(date);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(formatBirthdate, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMe.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMe.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMe.getIdentityNumb(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMe.getAddress(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaMe.getZipCode(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Pekerjaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaMe.getJob(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Ahli Waris", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Ahli Waris", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(WordUtils.capitalizeFully(pdfGardaMe.getAhliWaris().toLowerCase()), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Hubungan Ahli Waris", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaMe.getHubunganAhliWaris(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(4);
			hcell.setFixedHeight(80);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfGardaEduMikro(GeneratePdfGardaEduMikroReqDto pdfGardaEduMikro) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfGardaEduMikro.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(12);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Asuransi Kecelakaan", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Garda Edu Micro", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

//			hcell = new PdfPCell(new Phrase("Pekerjaan", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//
//			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getJob(), fontContent));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Yang Diinginkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaEduMikro.getPremi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Estimasi Total Premi", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaEduMikro.getPremi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
//			hcell = new PdfPCell(new Phrase("(Sudah termasuk biaya administrasi sebesar Rp25.000)", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(0);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(380);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfGardaEduMikroDetail(GeneratePdfGardaEduMikroReqDto pdfGardaEduMikro) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 900);
		Document document = new Document(pagesize);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfGardaEduMikro.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(140, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(30);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Garda Edu Micro", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(315, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi yang Diinginkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaEduMikro.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Detail Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			for(GeneratePdfGardaEduMikroBenefitReqDto item: pdfGardaEduMikro.getBenefits()) {
				hcell = new PdfPCell(new Phrase(item.getBenefit(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				if("AMOUNT".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(fmt.format(new BigDecimal(item.getValue())).replace(",00", ""), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else if("PERIODE".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(item.getValue() + " Tahun", fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else {
					hcell = new PdfPCell(new Phrase(item.getValue(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}
			} 
			
			hcell = new PdfPCell(new Phrase("Periode Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getPeriode() + " Tahun", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 
			
			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getNameDepan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getNameBelakang(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);



			Date date = new Date();
			try {
				date = format.parse(pdfGardaEduMikro.getBirthDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatBirthdate = formatter.format(date);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(formatBirthdate, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getIdentityNumb(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getAddress(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getZipCode(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Ahli Waris", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Ahli Waris", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(WordUtils.capitalizeFully(pdfGardaEduMikro.getAhliWaris().toLowerCase()), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Hubungan Ahli Waris", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaEduMikro.getHubunganAhliWaris(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nama Anak Kandung 1", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(WordUtils.capitalizeFully(pdfGardaEduMikro.getAnakKandung1().toLowerCase()), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
							
		    hcell = new PdfPCell(new Phrase("Nama Anak Kandung 2", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
				String a = "";
				if(pdfGardaEduMikro.getAnakKandung2() != null && !" ".equals(pdfGardaEduMikro.getAnakKandung2())) {
					a = WordUtils.capitalizeFully(pdfGardaEduMikro.getAnakKandung2().toLowerCase());
				}else {
					a = "-";
				}
			hcell = new PdfPCell(new Phrase(a, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(4);
			hcell.setFixedHeight(120);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfGardaMeMikro(GeneratePdfGardaMeMikroReqDto pdfGardaMeMikro) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfGardaMeMikro.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(12);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Asuransi Kecelakaan", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Garda Me Micro", fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

//			hcell = new PdfPCell(new Phrase("Pekerjaan", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);
//
//
//			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getJob(), fontContentBold));
//			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingRight(30);
//			hcell.setColspan(2);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Yang Diinginkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaMeMikro.getPremi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Estimasi Total Premi", fontContentBold));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaMeMikro.getPremi()).replace(",00", ""), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
//			hcell = new PdfPCell(new Phrase("(Sudah termasuk biaya administrasi sebesar Rp25.000)", fontContent));
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(0);
//			hcell.setPaddingLeft(30);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(380);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfAsuransiFlexiHealth(GeneratePdfAsuransiFlexiHealthDto pdfFlexi) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfFlexi.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikesehatanflexihealth.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Flexi Health", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfFlexi.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

			hcell = new PdfPCell(new Phrase("Santunan Tunai Harian", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfFlexi.getUangPertanggunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Periode Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfFlexi.getPeriodeLindung(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Total Premi yang Harus Dibayarkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfFlexi.getTotalPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(380);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfAsuransiFlexiHealthDetail(GeneratePdfAsuransiFlexiHealthDto pdfFlexi) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfFlexi.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikesehatanflexihealth.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(60);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Flexi Health", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfFlexi.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

			hcell = new PdfPCell(new Phrase("Santunan Tunai Harian", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfFlexi.getUangPertanggunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Periode Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfFlexi.getPeriodeLindung(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Total Premi yang Harus Dibayarkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfFlexi.getTotalPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 
			
			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfFlexi.getNamaDepan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfFlexi.getNamaBelakang(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			Date date = new Date();
			try {
				date = format.parse(pdfFlexi.getBirthDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatBirthdate = formatter.format(date);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(formatBirthdate, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfFlexi.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfFlexi.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor KTP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfFlexi.getNoKtp(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Surat Menyurat", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfFlexi.getAddress(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfFlexi.getZipCode(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Manfaat", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			for(PdfAsuransiManfaatFlexiHealth items: pdfFlexi.getBenefits()) {
				hcell = new PdfPCell(new Phrase(items.getNama(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase(items.getValue(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			} 
			
			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(200);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfAsuransiSport(GeneratePdfAsuransiSportDto pdfSport) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfSport.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransiolahraga.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(50);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Sport Insurance", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfSport.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Uang Pertanggungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfSport.getUangPertanggunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			Date date = new Date();
			try {
				date = format.parse(pdfSport.getTanggalMulai());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String tanggalMulai = formatter.format(date);
			
			hcell = new PdfPCell(new Phrase("Tanggal Mulai", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(tanggalMulai, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			if(pdfSport.getAdditionalBenefit().size() > 0) {
			hcell = new PdfPCell(new Phrase("Perlindungan Tambahan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			for(PdfAsuransiManfaatSportFisioterapi items: pdfSport.getAdditionalBenefit()) {
				hcell = new PdfPCell(new Phrase(items.getNama(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase(items.getValue(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			} 
		}
			
			hcell = new PdfPCell(new Phrase("Total Premi yang Harus Dibayarkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfSport.getTotalPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(380);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfAsuransiSportDetail(GeneratePdfAsuransiSportDto pdfSport) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfSport.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransiolahraga.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(50);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Sport Insurance", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi Dasar", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfSport.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  
			
			hcell = new PdfPCell(new Phrase("Uang Pertanggungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfSport.getUangPertanggunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			Date date = new Date();
			try {
				date = format.parse(pdfSport.getTanggalMulai());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String tanggalMulai = formatter.format(date);
			
			hcell = new PdfPCell(new Phrase("Tanggal Mulai", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(tanggalMulai, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			if(pdfSport.getAdditionalBenefit().size() > 0) {
			hcell = new PdfPCell(new Phrase("Perlindungan Tambahan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			for(PdfAsuransiManfaatSportFisioterapi items: pdfSport.getAdditionalBenefit()) {
				hcell = new PdfPCell(new Phrase(items.getNama(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase(items.getValue(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			} 
		}
			
			hcell = new PdfPCell(new Phrase("Total Premi yang Harus\nDibayarkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfSport.getTotalPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 
			
			hcell = new PdfPCell(new Phrase("Nama Lengkap", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfSport.getNamaLengkap(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Date date1 = new Date();
			try {
				date1 = format.parse(pdfSport.getBirthDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatBirthdate1 = formatter1.format(date1);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(formatBirthdate1, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfSport.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfSport.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor KTP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfSport.getNoKtp(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Manfaat", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			for(PdfAsuransiManfaatSport item: pdfSport.getBenefits()) {
				hcell = new PdfPCell(new Phrase(item.getNama(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase(item.getValue(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			} 

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(200);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	
	public static String generatePdfAsuransiHealthBuddies(GeneratePdfAsuransiHealthBuddiesDto pdfHealthBuddies) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfHealthBuddies.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikesehatan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("My Health Buddies", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfHealthBuddies.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

			if(pdfHealthBuddies.getAdditionalBenefits().size() > 0) {
			hcell = new PdfPCell(new Phrase("Asuransi tambahan\nManfaat Penyakit Kritis", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			for(AdditionalHealthBuddiesBenefitDto items: pdfHealthBuddies.getAdditionalBenefits()) {
				hcell = new PdfPCell(new Phrase(items.getNama(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase(items.getValue(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			} 
		}
			hcell = new PdfPCell(new Phrase("Uang Pertanggungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfHealthBuddies.getUangPertanggunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Total Premi yang Harus Dibayarkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfHealthBuddies.getTotalPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(380);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfAsuransiHealthBuddiesDetail(GeneratePdfAsuransiHealthBuddiesDto pdfHealthBuddies) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfHealthBuddies.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikesehatan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(50);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("My Health Buddies", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(fmt.format(pdfHealthBuddies.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);  

			if(pdfHealthBuddies.getAdditionalBenefits().size() > 0) {
			hcell = new PdfPCell(new Phrase("Asuransi tambahan\nManfaat Penyakit Kritis", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			for(AdditionalHealthBuddiesBenefitDto b : pdfHealthBuddies.getAdditionalBenefits()) {
				
				hcell = new PdfPCell(new Phrase(b.getNama(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);


				hcell = new PdfPCell(new Phrase(b.getValue(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			}
			}
			hcell = new PdfPCell(new Phrase("Uang Pertanggungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfHealthBuddies.getUangPertanggunan()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Total Premi yang Harus Dibayarkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfHealthBuddies.getTotalPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 
			
			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getNamaDepan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getNamaBelakang(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			Date date = new Date();
			try {
				date = format.parse(pdfHealthBuddies.getBirthDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatBirthdate = formatter.format(date);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(formatBirthdate, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor KTP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getNoKtp(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Surat Menyurat", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getAddress(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getZipCode(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nomor Rekening", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getNoRek(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Pemilik Rekening", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getNamaNoRek(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Bank", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getNamaBank(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Data Tertanggung Utama", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getNamaDepanTertanggung1(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getNamaBelakangTertanggung1(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			Date date1 = new Date();
			try {
				date1 = format.parse(pdfHealthBuddies.getTanggalLahirTertanggung());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatBirthdate1 = formatter1.format(date1);
			
			hcell = new PdfPCell(new Phrase("Tanggal Lahir Tertanggung", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(formatBirthdate1, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			if(pdfHealthBuddies.getHubunganDenganPemegangPolis() != null && !"".equalsIgnoreCase(pdfHealthBuddies.getHubunganDenganPemegangPolis())) {
			hcell = new PdfPCell(new Phrase("Hubungan Dengan Pemegang Polis", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfHealthBuddies.getHubunganDenganPemegangPolis(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			}	
			
			int tertanggung = 1;
			for(PdfAsuransiDataTambahanHealthBuddies add : pdfHealthBuddies.getTertanggungTambahan()) {
					hcell = new PdfPCell(new Phrase("Data Tertanggung Tambahan " + tertanggung, fontContentBold2));
					hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingLeft(30);
					hcell.setColspan(4);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
					
					hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
					hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingLeft(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);

					hcell = new PdfPCell(new Phrase(add.getNamaDepanTer(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
					
					hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
					hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingLeft(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);

					hcell = new PdfPCell(new Phrase(add.getNamaBelakangTer(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
					
					Date date2 = new Date();
					try {
						date2 = format.parse(add.getTanggalLahir());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat formatter2 = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
					String formatBirthdate2 = formatter2.format(date2);
					
					hcell = new PdfPCell(new Phrase("Tanggal Lahir Tertanggung", fontContent));
					hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingLeft(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);

					hcell = new PdfPCell(new Phrase(formatBirthdate2, fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
					
					hcell = new PdfPCell(new Phrase("Hubungan Dengan Pemegang Polis", fontContent));
					hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingLeft(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);

					hcell = new PdfPCell(new Phrase(add.getHub(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				tertanggung = tertanggung + 1;
			
		}
			hcell = new PdfPCell(new Phrase("Manfaat", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			for(PdfAsuransiManfaatHealthBuddies items: pdfHealthBuddies.getBenefits()) {
				hcell = new PdfPCell(new Phrase(items.getNama(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
				
				hcell = new PdfPCell(new Phrase(items.getValue(), fontContent));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);
			} 


			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			table.setSpacingAfter(40);
			
			document.add(table);		
			GeneratePdfListener.addFooter(pdfWriter, document);
			
//			document.newPage();
//			PdfPTable tables = new PdfPTable(6);
//
//			tables.setWidthPercentage(100);
//			tables.setWidths(new int[] {15,15,20,10,20,20});
//
//			PdfPCell cells;
//
//			cells = new PdfPCell(image1);
//			cells.setBorder(0);
//			cells.setColspan(6);
//			cells.setBackgroundColor(new BaseColor(250, 250, 250));
//			tables.addCell(cells);
//			
//		    PdfPCell hcells;
//			hcells = new PdfPCell();
//			hcells.setHorizontalAlignment(1); // 0=Left, 1=Centre, 2=Right
//			hcells.setBorder(0);
//			hcells.setPadding(5);
//			hcells.setColspan(2);
//			hcells.setRowspan(50);
//			hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//			tables.addCell(hcells);
//			
//			int tertanggung = 1;
//			for(PdfAsuransiDataTambahanHealthBuddies add : pdfHealthBuddies.getTertanggungTambahan()) {
//			
//					hcells = new PdfPCell(new Phrase("Data Tertanggung Tambahan " + tertanggung, fontContentBold2));
//					hcells.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingLeft(30);
//					hcells.setColspan(4);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//					
//					hcells = new PdfPCell(new Phrase("Nama Depan", fontContent));
//					hcells.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingLeft(30);
//					hcells.setColspan(2);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//
//					hcells = new PdfPCell(new Phrase(add.getNamaDepanTer(), fontContent));
//					hcells.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingRight(30);
//					hcells.setColspan(2);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//					
//					hcells = new PdfPCell(new Phrase("Nama Belakang", fontContent));
//					hcells.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingLeft(30);
//					hcells.setColspan(2);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//
//					hcells = new PdfPCell(new Phrase(add.getNamaBelakangTer(), fontContent));
//					hcells.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingRight(30);
//					hcells.setColspan(2);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//					
//					Date date2 = new Date();
//					try {
//						date2 = format.parse(add.getTanggalLahir());
//					} catch (ParseException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					SimpleDateFormat formatter2 = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
//					String formatBirthdate2 = formatter2.format(date2);
//					
//					hcells = new PdfPCell(new Phrase("Tanggal Lahir Tertanggung", fontContent));
//					hcells.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingLeft(30);
//					hcells.setColspan(2);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//
//					hcells = new PdfPCell(new Phrase(formatBirthdate2, fontContent));
//					hcells.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingRight(30);
//					hcells.setColspan(2);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//					
//					hcells = new PdfPCell(new Phrase("Hubungan Dengan Pemegang Polis", fontContent));
//					hcells.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingLeft(30);
//					hcells.setColspan(2);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//
//					hcells = new PdfPCell(new Phrase(add.getHub(), fontContent));
//					hcells.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//					hcells.setBorder(0);
//					hcells.setPadding(5);
//					hcells.setPaddingRight(30);
//					hcells.setColspan(2);
//					hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//					tables.addCell(hcells);
//				tertanggung = tertanggung + 1;
//			
//		}
//			hcells = new PdfPCell();
//			hcells.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcells.setBorder(0);
//			hcells.setPadding(5);
//			hcells.setPaddingRight(30);
//			hcells.setColspan(6);
//			hcells.setFixedHeight(200);
//			hcells.setBackgroundColor(new BaseColor(250, 250, 250));
//			tables.addCell(hcells);
//
//			document.add(tables);
//			GeneratePdfListener.addFooter(pdfWriter, document);
//			
//			document.newPage();
//			PdfPTable tabless = new PdfPTable(6);
//
//			tabless.setWidthPercentage(100);
//			tabless.setWidths(new int[] {15,15,20,10,20,20});
//
//			PdfPCell cellss;
//
//			cellss = new PdfPCell(image1);
//			cellss.setBorder(0);
//			cellss.setColspan(6);
//			cellss.setBackgroundColor(new BaseColor(250, 250, 250));
//			tabless.addCell(cellss);
//			
//		    PdfPCell hcellss;
//			hcellss = new PdfPCell();
//			hcellss.setHorizontalAlignment(1); // 0=Left, 1=Centre, 2=Right
//			hcellss.setBorder(0);
//			hcellss.setPadding(5);
//			hcellss.setColspan(2);
//			hcellss.setRowspan(50);
//			hcellss.setBackgroundColor(new BaseColor(250, 250, 250));
//			tabless.addCell(hcellss);
//			
//			hcellss = new PdfPCell(new Phrase("Manfaat", fontContentBold2));
//			hcellss.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcellss.setBorder(0);
//			hcellss.setPadding(5);
//			hcellss.setColspan(4);
//			hcellss.setBackgroundColor(new BaseColor(250, 250, 250));
//			tabless.addCell(hcellss);
//			
//			for(PdfAsuransiManfaatHealthBuddies items: pdfHealthBuddies.getBenefits()) {
//				hcellss = new PdfPCell(new Phrase(items.getNama(), fontContent));
//				hcellss.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//				hcellss.setBorder(0);
//				hcellss.setPadding(5);
//				hcellss.setColspan(2);
//				hcellss.setBackgroundColor(new BaseColor(250, 250, 250));
//				tabless.addCell(hcellss);
//				
//				hcellss = new PdfPCell(new Phrase(items.getValue(), fontContent));
//				hcellss.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//				hcellss.setBorder(0);
//				hcellss.setPadding(5);
//				hcellss.setPaddingRight(30);
//				hcellss.setColspan(2);
//				hcellss.setBackgroundColor(new BaseColor(250, 250, 250));
//				tabless.addCell(hcellss);
//			} 
//			
//		
//			hcellss = new PdfPCell();
//			hcellss.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
//			hcellss.setBorder(0);
//			hcellss.setPadding(5);
//			hcellss.setPaddingRight(30);
//			hcellss.setColspan(6);
//			hcellss.setFixedHeight(500);
//			hcellss.setBackgroundColor(new BaseColor(250, 250, 250));
//			tabless.addCell(hcellss);
//
//			document.add(tabless);
//			GeneratePdfListener.addFooter(pdfWriter, document);
			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfGardaMeMikroDetail(GeneratePdfGardaMeMikroReqDto pdfGardaMeMikro) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Rectangle pagesize = new Rectangle(612, 861);
		Document document = new Document(pagesize);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd",localeCurrency);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Montserrat-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Montserrat-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfGardaMeMikro.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransikecelakaan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(140, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(25);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

			hcell = new PdfPCell(new Phrase("Garda Me Micro", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(2);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);    

			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(315, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Premi yang Diinginkan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(fmt.format(pdfGardaMeMikro.getPremi()).replace(",00", ""), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Detail Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			for(GeneratePdfGardaMeMikroBenefitReqDto item: pdfGardaMeMikro.getBenefits()) {
				hcell = new PdfPCell(new Phrase(item.getBenefit(), fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setBackgroundColor(new BaseColor(250, 250, 250));
				table.addCell(hcell);

				if("AMOUNT".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(fmt.format(new BigDecimal(item.getValue())).replace(",00", ""), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else if("PERIODE".equalsIgnoreCase(item.getType())) {
					hcell = new PdfPCell(new Phrase(item.getValue() + " Tahun", fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}else {
					hcell = new PdfPCell(new Phrase(item.getValue(), fontContent));
					hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
					hcell.setBorder(0);
					hcell.setPadding(5);
					hcell.setPaddingRight(30);
					hcell.setColspan(2);
					hcell.setBackgroundColor(new BaseColor(250, 250, 250));
					table.addCell(hcell);
				}
			}  
			
			hcell = new PdfPCell(new Phrase("Periode Perlindungan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getPeriode() + " Tahun", fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Data Pemegang Polis", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 
			
			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getNameDepan(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getNameBelakang(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			Date date = new Date();
			try {
				date = format.parse(pdfGardaMeMikro.getBirthDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", localeCurrency);
			String formatBirthdate = formatter.format(date);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(formatBirthdate, fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getPhone(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getEmail(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getIdentityNumb(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Identitas", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getAddress(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getZipCode(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Ahli Waris", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell); 

			hcell = new PdfPCell(new Phrase("Nama Ahli Waris", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(WordUtils.capitalizeFully(pdfGardaMeMikro.getAhliWaris().toLowerCase()), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Hubungan Ahli Waris", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfGardaMeMikro.getHubunganAhliWaris(), fontContent));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(4);
			hcell.setFixedHeight(120);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
	
	public static String generatePdfAsuransiCovid(GeneratePdfAsuransiCovidReqDto pdfCovid) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", 16, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 21, Font.BOLD);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17, Font.BOLD);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17, Font.BOLD);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17, Font.BOLD);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {15,15,20,10,20,20});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfCovid.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan penawaran produkmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(30);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/asuransijiwadankritis.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(100, 100);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(10);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Asuransi Covid-19", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);            


			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
			imageDot.setAlignment(1);
			imageDot.scaleAbsolute(290, 10);

			hcell = new PdfPCell(imageDot);
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingTop(10);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(10);
			hcell.setColspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Perlindungan Hingga", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Rp" + pdfCovid.getPerlindungan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfCovid.getNamaDepan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase(pdfCovid.getNamaBelakang(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);
			
			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfCovid.getTglLahir(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(25);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfCovid.getNomorPonsel(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(6);
			hcell.setFixedHeight(200);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			document.add(table);

			GeneratePdfListener.addFooter(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfDetailRingkasanPengajuan(GeneratePdfDetailRingkasanPengajuanReqDto pdfDetailRingkasanPengajuanReqDto) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.NORMAL);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {20,20,15,15,15,15});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfDetailRingkasanPengajuanReqDto.getNama() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingTop(22);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/multiguna-tanpa-jaminan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(1); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(25);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);



//			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
//			imageDot.setAlignment(1);
//			imageDot.scaleAbsolute(290, 10);

//			hcell = new PdfPCell(imageDot);
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingTop(10);
//			hcell.setPaddingLeft(30);
//			hcell.setPaddingBottom(10);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Produk Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getProdukPembiayaan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Partner Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getPartnerPembiayaan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Pendapatan per Bulan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getPendapatanPerBulan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jumlah Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getJumlahPeminjaman(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getTenor(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kebutuhan Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getKebutuhanPeminjaman(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Memiliki Kartu Kredit", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getMemilikiKartuKredit(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Bank Penerbit", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getBankPenerbit(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Detail Data Diri", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingTop(6);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingTop(6);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Kelamin", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getJenisKelamin(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nama Sesuai KTP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getNamaSesuaiKtp(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("NIK", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getNik(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tempat Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getTempatLahir(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getTanggalLahir(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getEmail(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getNomorPonsel(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Pekerjaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getPekerjaan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Nomor NPWP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getNomorNpwp(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Detail Tempat Tinggal", fontContentBold2));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingTop(6);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell();
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingTop(6);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Alamat Lengkap", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getAlamatLengkap(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Provinsi", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getProvinsi(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kota/Kabupaten", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getKota(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kecamatan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getKecamatan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kelurahan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getKelurahan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(5);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPengajuanReqDto.getKodePos(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(5);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);


			Image image2 = Image.getInstance(new URL(FOOTER_IMG));

			// Fixed Positioning
			image2.setAlignment(1);
			image2.setScaleToFitHeight(true);
			// Scale to new height and new width of image
//			image2.scaleAbsolute(527, 40);
			image2.scalePercent(scaler);
			// Add to document
			//            document.add(image1);

			cell = new PdfPCell(image2);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setPaddingTop(10);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			document.add(table);

//			GeneratePdfListener.addFooterNoFixed2(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfRingkasanPengajuan(GeneratePdfRingkasanPengajuanReqDto pdfRingkasanPengajuanReqDto) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf",  BaseFont.IDENTITY_H, BaseFont.EMBEDDED,9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.NORMAL);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {20,20,15,15,15,15});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfRingkasanPengajuanReqDto.getNama() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingTop(22);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/multiguna-tanpa-jaminan.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(1); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

//			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
//			imageDot.setAlignment(1);
//			imageDot.scaleAbsolute(290, 10);

//			hcell = new PdfPCell(imageDot);
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingTop(10);
//			hcell.setPaddingLeft(30);
//			hcell.setPaddingBottom(10);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRingkasanPengajuanReqDto.getJenisLayanan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			hcell.setLeading(14,0);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Partner Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRingkasanPengajuanReqDto.getPartnerPembiayaan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jumlah Peminjaman", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRingkasanPengajuanReqDto.getJumlahPeminjaman(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(5);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(pdfRingkasanPengajuanReqDto.getTenor(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(5);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image image2 = Image.getInstance(new URL(FOOTER_IMG));

			// Fixed Positioning
			image2.setAlignment(1);
			image2.setScaleToFitHeight(true);
			// Scale to new height and new width of image
//			image2.scaleAbsolute(527, 40);
			image2.scalePercent(scaler);
			// Add to document
			//            document.add(image1);

			cell = new PdfPCell(image2);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setPaddingTop(13);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			document.add(table);

//			GeneratePdfListener.addFooterNoFixed(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generateRingkasanPembiayaanUmroh(GeneratePdfRingkasanPembiayaanUmrohDto generatePdfRingkasanPembiayaanUmrohDto ) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf",  BaseFont.IDENTITY_H, BaseFont.EMBEDDED,9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.NORMAL);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {20,20,15,15,15,15});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + generatePdfRingkasanPembiayaanUmrohDto.getName() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingTop(22);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/pembiayaanUmroh.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(1); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(4);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);

//			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
//			imageDot.setAlignment(1);
//			imageDot.scaleAbsolute(290, 10);

//			hcell = new PdfPCell(imageDot);
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingTop(10);
//			hcell.setPaddingLeft(30);
//			hcell.setPaddingBottom(10);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(generatePdfRingkasanPembiayaanUmrohDto.getProductPembiayaan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			hcell.setLeading(14,0);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Partner Pembiayaan", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(generatePdfRingkasanPembiayaanUmrohDto.getPartnerPembiayaan(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("DP", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(generatePdfRingkasanPembiayaanUmrohDto.getDp(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Tenor", fontContent));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(5);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase(generatePdfRingkasanPembiayaanUmrohDto.getTenor(), fontContentBold));
			hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingRight(30);
			hcell.setPaddingBottom(5);
			hcell.setColspan(2);
			hcell.setLeading(14,0);
			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(hcell);

			Image image2 = Image.getInstance(new URL(FOOTER_IMG));

			// Fixed Positioning
			image2.setAlignment(1);
			image2.setScaleToFitHeight(true);
			// Scale to new height and new width of image
//			image2.scaleAbsolute(527, 40);
			image2.scalePercent(scaler);
			// Add to document
			//            document.add(image1);

			cell = new PdfPCell(image2);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setPaddingTop(13);
			cell.setBackgroundColor(new BaseColor(250, 250, 250));
			table.addCell(cell);


			document.add(table);

//			GeneratePdfListener.addFooterNoFixed(pdfWriter, document);

			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}

	public static String generatePdfDetailRingkasanPembiayaanUmroh(GeneratePdfDetailRingkasanPembiayaanUmrohDto pdfDetailRingkasanPembiayaanUmrohDto) {

		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		Document document = new Document();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String fullFileName = "";
		System.out.print("RT "+pdfDetailRingkasanPembiayaanUmrohDto.getRt());
		System.out.print("RW "+pdfDetailRingkasanPembiayaanUmrohDto.getRw());
		try {

			// STYLE FONT
			Font fontParagraph = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontParagraph.setColor(19, 29, 41);
			Font fontTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.NORMAL);
			fontTitle.setColor(19, 29, 41);
			Font fontSubTitle = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontSubTitle.setColor(19, 29, 41);
			Font fontContent = FontFactory.getFont("/fonts/Ubuntu-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContent.setColor(19, 29, 41);
			Font fontContentBold = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContentBold.setColor(19, 29, 41);
			Font fontContentBold2 = FontFactory.getFont("/fonts/Ubuntu-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 9, Font.NORMAL);
			fontContentBold2.setColor(19, 29, 41);

			String directoryName = fileStorageUrl + "temp-pdf/";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}

			fullFileName = directoryName + FUtils.currentDate("yyyyMMddHHmmss") + "_" + new File(FILE_NAME);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			//			PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(fullFileName));
			document.addCreationDate();
			document.addTitle("PDF Pengajuan");
			document.open();

			Image image1 = Image.getInstance(new URL(HEADER_IMG));

			//RESIZE IMAGE
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
					- document.rightMargin() - 0) / image1.getWidth()) * 100;

			// Fixed Positioning
			image1.setAlignment(1);
			image1.setScaleToFitHeight(true);
			// Scale to new height and new width of image
			//            image1.scaleAbsolute(100, 20);
			image1.scalePercent(scaler);
			// Add to document
			//            document.add(image1);


			//PDF HEADER
			PdfPTable table = new PdfPTable(6);

			table.setWidthPercentage(100);
			table.setWidths(new int[] {20,20,20,20,15,15});

			PdfPCell cell;

			cell = new PdfPCell(image1);
			cell.setBorder(0);
			cell.setColspan(6);
			cell.setBackgroundColor(new BaseColor(255, 255, 255));
			table.addCell(cell);


			//PDF BODY

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Hai " + pdfDetailRingkasanPembiayaanUmrohDto.getNama() + ",", fontParagraph));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setPaddingTop(22);
			hcell.setColspan(6);
			hcell.setBackgroundColor(new BaseColor(255, 255, 255));
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Berikut ringkasan pengajuanmu:", fontTitle));
			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setColspan(6);
			hcell.setPaddingLeft(30);
			hcell.setPaddingBottom(20);
			hcell.setBackgroundColor(new BaseColor(255, 255, 255));
			table.addCell(hcell);

			Image imageUnit = Image.getInstance(new URL("https://cdn-mabroor-dev.moxa.id/pdf-assets/pembiayaanUmroh.png"));
			imageUnit.setAlignment(1);
			imageUnit.setScaleToFitHeight(true);
			imageUnit.scaleAbsolute(120, 120);

			hcell = new PdfPCell(imageUnit);
			hcell.setHorizontalAlignment(1); // 0=Left, 1=Centre, 2=Right
			hcell.setBorder(0);
			hcell.setPadding(5);
			hcell.setPaddingLeft(30);
			hcell.setColspan(2);
			hcell.setRowspan(25);
			hcell.setBackgroundColor(new BaseColor(255, 255, 255));
			table.addCell(hcell);

			Image imageTag = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/buttonTag.png"));
			imageTag.setAlignment(1);
			imageTag.scaleAbsolute(72, 20);



//			Image imageDot = Image.getInstance(new URL("https://fh.astrafinancial.co.id/pdf-assets/dot.png"));
//			imageDot.setAlignment(1);
//			imageDot.scaleAbsolute(290, 10);

//			hcell = new PdfPCell(imageDot);
//			hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
//			hcell.setBorder(0);
//			hcell.setPadding(5);
//			hcell.setPaddingTop(10);
//			hcell.setPaddingLeft(30);
//			hcell.setPaddingBottom(10);
//			hcell.setColspan(4);
//			hcell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(hcell);

			if(!pdfDetailRingkasanPembiayaanUmrohDto.getAlamatTempatTinggal().equalsIgnoreCase(pdfDetailRingkasanPembiayaanUmrohDto.getAlamatKtp()) &&
					!pdfDetailRingkasanPembiayaanUmrohDto.getKodePos().equalsIgnoreCase(pdfDetailRingkasanPembiayaanUmrohDto.getKodePosKtp())){
				hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);


				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getProdukPembiayaan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Partner Pembiayaan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getPartnerPembiayaan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("DP", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getDp(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Tenor", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getTenor(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Detail Data Diri", fontContentBold2));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell();
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNamaDepan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);


				hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNamaBelakang(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Jenis Kelamin", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getJenisKelamin(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Tempat Lahir", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getTempatLahir(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getTanggalLahir(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Nama Ibu Kandung", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNamaIbuKandung(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNomorPonsel(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("No KTP", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNoKtp(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Alamat", fontContentBold2));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell();
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Alamat Tempat Tinggal", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getAlamatTempatTinggal(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Provinsi", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getProvinsi(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kabupaten/Kota", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKotaKabupaten(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kecamatan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKecamatan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kelurahan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKelurahan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("RT", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getRt(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("RW", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getRw(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKodePos(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				// alamat ktp
				hcell = new PdfPCell(new Phrase("Alamat KTP", fontContentBold2));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell();
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Alamat Sesuai KTP", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getAlamatKtp(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKodePosKtp(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				document.add(table);

				GeneratePdfListener.addFooterNoFixed2(pdfWriter, document);

				document.close();

			} else {
				hcell = new PdfPCell(new Phrase("Jenis Layanan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);


				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getProdukPembiayaan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Partner Pembiayaan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getPartnerPembiayaan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("DP", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getDp(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(10,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Tenor", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getTenor(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Detail Data Diri", fontContentBold2));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell();
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Nama Depan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNamaDepan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);


				hcell = new PdfPCell(new Phrase("Nama Belakang", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNamaBelakang(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Jenis Kelamin", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getJenisKelamin(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Tempat Lahir", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getTempatLahir(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Tanggal Lahir", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getTanggalLahir(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Nama Ibu Kandung", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNamaIbuKandung(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Nomor Ponsel", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNomorPonsel(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("No KTP", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getNoKtp(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Alamat", fontContentBold2));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell();
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setPaddingTop(6);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Alamat Tempat Tinggal", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getAlamatTempatTinggal(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Provinsi", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getProvinsi(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kabupaten/Kota", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKotaKabupaten(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kecamatan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKecamatan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kelurahan", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKelurahan(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("RT", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getRt(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("RW", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getRw(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase("Kode Pos", fontContent));
				hcell.setHorizontalAlignment(0); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingLeft(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				hcell = new PdfPCell(new Phrase(pdfDetailRingkasanPembiayaanUmrohDto.getKodePos(), fontContentBold));
				hcell.setHorizontalAlignment(2); // 0=Left, 1=Centre, 2=Right
				hcell.setBorder(0);
				hcell.setPadding(5);
				hcell.setPaddingRight(30);
				hcell.setColspan(2);
				hcell.setLeading(12,0);
				hcell.setBackgroundColor(new BaseColor(255, 255, 255));
				table.addCell(hcell);

				document.add(table);

				GeneratePdfListener.addFooterNoFixed2(pdfWriter, document);

				document.close();
			}


//			Image image2 = Image.getInstance(new URL(FOOTER_IMG));
//
//			// Fixed Positioning
//			image2.setAlignment(1);
//			image2.setScaleToFitHeight(true);
//			// Scale to new height and new width of image
////			image2.scaleAbsolute(527, 40);
//			image2.scalePercent(scaler);
//			// Add to document
//			//            document.add(image1);
//
//			cell = new PdfPCell(image2);
//			cell.setBorder(0);
//			cell.setColspan(6);
//			cell.setPaddingTop(10);
//			cell.setBackgroundColor(new BaseColor(250, 250, 250));
//			table.addCell(cell);

//			document.add(table);
//
//			GeneratePdfListener.addFooterNoFixed2(pdfWriter, document);
//
//			document.close();

		} catch (DocumentException | IOException ex) {

			logger.error("Error occurred: {0}", ex);
		}

		try {
			Files.deleteIfExists(Paths.get(fullFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String encoded = Base64.getEncoder().encodeToString(out.toByteArray());
		return encoded;
	}
}