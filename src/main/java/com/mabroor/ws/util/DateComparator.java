package com.mabroor.ws.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class DateComparator implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		System.out.println("o1 " + o1);
		Locale locale = new Locale("id", "ID");
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", locale);
		try {
			Date tmpDt1 = sdf.parse(o1);
			Date tmpDt2 = sdf.parse(o2);
			
			return tmpDt2.compareTo(tmpDt1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
