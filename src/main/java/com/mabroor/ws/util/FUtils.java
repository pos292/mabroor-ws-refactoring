package com.mabroor.ws.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public interface FUtils {

	static String statusName(String status) {
		String name = "";
		if ("01".equals(status)) {
			name = "<span class=\"label label-inverse\">Enabled</span>";
		} else if ("02".equals(status)) {
			name = "<span class=\"label label-light-danger\">Disabled</span>";
		}
		return name;
	}
	
	static String statusNameWinner(String status) {
		String name = "";
		if ("01".equals(status)) {
			name = "<span class=\"label label-inverse\">Valid</span>";
		} else if ("02".equals(status)) {
			name = "<span class=\"label label-light-info\">Waiting Verification</span>";
		} else if ("03".equals(status)) {
			name = "<span class=\"label label-light-danger\">Not Valid</span>";
		}
		return name;
	}

	static String currentDate(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		return dateFormat.format(date);
	}

	static String categoryName(String category) {
		String name = "";
		switch (category) {
		case "community":
			name = "Community";
			break;
		case "event":
			name = "Event";
			break;
		case "news":
			name = "News";
			break;
		case "promotion":
			name = "Promotion";
			break;
		case "tips_trick":
			name = "Tips & Trick";
			break;
		}
		return name;
	}
	
	static String periodLuckyDraw (String startDate, String endDate) {
		String result = "";
		String[] s = startDate.split("-");
		String sDate = s[2] + "-" + s[1] + "-" + s[0];
		String[] e = endDate.split("-");
		String eDate = e[2] + "-" + e[1] + "-" + e[0];
		
		result = sDate.concat(" to ").concat(eDate);
		
		return result;
	}
	
	static String activeName(String status) {
		String name = "";
		if ("01".equals(status)) {
			name = "<span class=\"label label-inverse\">Enabled</span>";
		} else if ("02".equals(status)) {
			name = "<span class=\"label label-light-danger\">Disabled</span>";
		}
		return name;
	}
	
	static String notifName(String type) {
		String result = type;
		if (type.equalsIgnoreCase("mudik-tenang")) {
			result = "Teman Ramadan";
		}
		return result;
	}
}
