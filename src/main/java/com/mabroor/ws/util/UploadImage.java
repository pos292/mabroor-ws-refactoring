package com.mabroor.ws.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.springframework.web.multipart.MultipartFile;

public class UploadImage {
	public static String create(String path, MultipartFile file, String title) {
		String extension = file.getOriginalFilename().split("\\.")[1];
		String filename = Slug.toSlug(title) + "-" + FUtils.currentDate("yyyyMMddHHmmss") + "." + extension;
		String tempFilename = path + filename;
		String thumb_tempFilename = path + "thumb-" + filename;
		try {
			long size = file.getSize() / 1024;

			File input = new File(tempFilename);

			input.createNewFile();
			FileOutputStream fo = new FileOutputStream(input, false);
			fo.write(file.getBytes());
			fo.close();

			// Start Compress Image
			File in = new File(tempFilename);
			BufferedImage image = ImageIO.read(in);
			
			// File output = new File(path + "com-" + filename);
//			File output = new File(tempFilename);
//	        OutputStream out = new FileOutputStream(output);
//
//			ImageWriter writer = ImageIO.getImageWritersByFormatName(extension).next();
//			ImageOutputStream ios = ImageIO.createImageOutputStream(out);
//			writer.setOutput(ios);
//
//			ImageWriteParam param = writer.getDefaultWriteParam();
//			if (param.canWriteCompressed()) {
//				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//				if (size > 10000) {
//					param.setCompressionQuality(0.1f);
//				} else if (size > 1000) {
//					param.setCompressionQuality(0.3f);
//				} else {
//					param.setCompressionQuality(0.8f);
//				}
//			}

//			writer.write(null, new IIOImage(image, null, null), param);

//			out.close();
//			ios.close();
//			writer.dispose();
			// End Compress Image

//			BufferedImage image = ImageIO.read(input);
			BufferedImage resized = resizeImage(image, Constant.THUMB_WIDTH, Constant.THUMB_HEIGHT);
			File output2 = new File(thumb_tempFilename);
			ImageIO.write(resized, "png", output2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return filename;
	}

	public static String rename(String path, String filename, String title) {
		File file1 = new File(path + filename);
		File thumb_file1 = new File(path + "thumb-" + filename);

		filename = Slug.toSlug(title) + "-" + FUtils.currentDate("yyyyMMddHHmmss") + ".jpg";
		String tempFilename = path + filename;
		String thumb_tempFilename = path + "thumb-" + filename;
		File file2 = new File(tempFilename);
		File thumb_file2 = new File(thumb_tempFilename);
		file1.renameTo(file2);
		thumb_file1.renameTo(thumb_file2);

		return filename;
	}

	public static BufferedImage resizeImage(BufferedImage img, int height, int width) {
		Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}

	public static String createImage(String path, MultipartFile file, String title) {
		String extension = file.getOriginalFilename().split("\\.")[1];
		String filename = Slug.toSlug(title) + "-" + FUtils.currentDate("yyyyMMddHHmmss") + "." + extension;
		String tempFilename = path + filename;
		try {
			long size = file.getSize() / 1024;

			File input = new File(tempFilename);

			input.createNewFile();
			FileOutputStream fo = new FileOutputStream(input, false);
			fo.write(file.getBytes());
			fo.close();

			// Start Compress Image
			File in = new File(tempFilename);
			BufferedImage image = ImageIO.read(in);
			
			// File output = new File(path + "com-" + filename);
			File output = new File(tempFilename);
	        OutputStream out = new FileOutputStream(output);

			ImageWriter writer = ImageIO.getImageWritersByFormatName(extension).next();
			ImageOutputStream ios = ImageIO.createImageOutputStream(out);
			writer.setOutput(ios);

			ImageWriteParam param = writer.getDefaultWriteParam();
			if (param.canWriteCompressed()) {
				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				if (size > 10000) {
					param.setCompressionQuality(0.1f);
				} else if (size > 1000) {
					param.setCompressionQuality(0.3f);
				} else {
					param.setCompressionQuality(0.8f);
				}
			}

			writer.write(null, new IIOImage(image, null, null), param);

			out.close();
			ios.close();
			writer.dispose();
			// End Compress Image
		} catch (Exception e) {
			e.printStackTrace();
		}

		return filename;
	}
	
	public static String createImageSubmission(String path, MultipartFile file, String filename) {
		String extension = file.getOriginalFilename().split("\\.")[1];
		String tempFilename = path + filename;
		try {
			long size = file.getSize() / 1024;

			File input = new File(tempFilename);

			input.createNewFile();
			FileOutputStream fo = new FileOutputStream(input, false);
			fo.write(file.getBytes());
			fo.close();

			// Start Compress Image
			File in = new File(tempFilename);
			BufferedImage image = ImageIO.read(in);
			
			// File output = new File(path + "com-" + filename);
			File output = new File(tempFilename);
	        OutputStream out = new FileOutputStream(output);

			ImageWriter writer = ImageIO.getImageWritersByFormatName(extension).next();
			ImageOutputStream ios = ImageIO.createImageOutputStream(out);
			writer.setOutput(ios);

			ImageWriteParam param = writer.getDefaultWriteParam();
			if (param.canWriteCompressed()) {
				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				if (size > 10000) {
					param.setCompressionQuality(0.1f);
				} else if (size > 1000) {
					param.setCompressionQuality(0.3f);
				} else {
					param.setCompressionQuality(0.8f);
				}
			}

			writer.write(null, new IIOImage(image, null, null), param);

			out.close();
			ios.close();
			writer.dispose();
			// End Compress Image
		} catch (Exception e) {
			e.printStackTrace();
		}

		return filename;
	}

	public static String renameImage(String path, String filename, String title) {
		File file1 = new File(path + filename);

		filename = Slug.toSlug(title) + "-" + FUtils.currentDate("yyyyMMddHHmmss") + ".jpg";
		String tempFilename = path + filename;
		File file2 = new File(tempFilename);
		file1.renameTo(file2);

		return filename;
	}

}
