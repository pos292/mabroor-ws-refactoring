package com.mabroor.ws.util;

import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import java.util.Base64;

public class AES256Utils {

	private static final String SECRET_KEY = "BPQOPdmQZpgZzoGHcvOKIaZE3HOTcqODxRbzMfp36j9oCeZ9fcUIMr4H7bie0hBlyHGfSz3QymE9pgzFEembG4eZErRn3nbzKDUeeLg8up6GcGN3vECCHAxDRmWvKg0mtOcvIBaxMePYLCJQ28R6X64wVWVLPsigKfBqmPAsXnRiVi6F66eTPflAqB8nGJONwKvAxUjrDUGfyXnHsBVDiRQuWt2lA5X2909KhX85P4IdRZ8yHMN7wmPxg29a83swZn0XRvZcvaNb2v4wGZPjjF6SNdgPPP1mr3HlYhnGWgOTgJsuECEiEwqdT4SZzfuV1CWt8vti2UR9UwqWWTeW5rIm9AjEWEVN3NlIhPPY4F08KtWPmTFQln5t1dHqoXc373KQ4HLQYUrKhlkujrAyuv0a5o24kDxhmkltdE4eInEY4YJl8trQIXwWj72CBOOr2nY8z4CwPzWeyEnSlGcL6dnCz4LnbhCt743i9rxb9R9hXfNxKfCDgGzgBUL0nkO9";
	private static final String SALT = "L1MvHNMWNLaoRxkdsL5775UwmJZgWuCa";

	public static String encrypt(String strToEncrypt) {
		try {
			byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			IvParameterSpec ivspec = new IvParameterSpec(iv);

			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), SALT.getBytes(), 10, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
			return Base64.getEncoder()
					.encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
//			return strToEncrypt;
		} catch (Exception e) {
			System.out.println("Error while encrypting: " + e.toString());
		}
		return null;
	}
	
	public static String decrypt(String strToDecrypt) {
	    try {
	      byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	      IvParameterSpec ivspec = new IvParameterSpec(iv);
	 
	      SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
	      KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), SALT.getBytes(), 10, 256);
	      SecretKey tmp = factory.generateSecret(spec);
	      SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
	 
	      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	      cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
	      return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
//	      return strToDecrypt;
	    } catch (Exception e) {
	      System.out.println("Error while decrypting: " + e.toString());
	    }
	    return null;
	  }
}
