package com.mabroor.ws.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.compress.utils.IOUtils;

public final class Common {
	
	private Common(){
	}
	
//	public static String maskingPhone(String text, int maxDigit){
//		return text.substring(0,maxDigit) + "*****";
//	}
	
	public static String maskingPhone(String strText, int start, int end, char maskChar){
		if(strText == null || strText.equals(""))
	        return "";
	    
	    if(start < 0)
	        start = 0;
	    
	    if( end > strText.length() )
	        end = strText.length();
	        
	    if(start > end)
	        return "";
	    
	    int maskLength = end - start;
	    
	    if(maskLength == 0)
	        return strText;
	    
	    StringBuilder sbMaskString = new StringBuilder(maskLength);
	    
	    for(int i = 0; i < maskLength; i++){
	        sbMaskString.append(maskChar);
	    }
	        
	    return strText.substring(0, start) + sbMaskString.toString() + strText.substring(start + maskLength);

	}
	
	public static String maskingEmail(String strText) {
		if(strText == null || strText.equals(""))
	        return "";
		
		String[] emailMask = strText.split("@");

		StringBuilder sb = new StringBuilder();
        for(int i = 0; i < emailMask[0].length(); i++) {
	          sb.append("*");
        }
		
		return sb + "@" + emailMask[1];
	}
	
	public static String maskingString(String strText, int start) {
		if(strText == null || strText.equals(""))
	        return "";
		
		if(start == 0)
	        return "";
		
		String stringMask = strText.substring(0, start);
		
		StringBuilder sb = new StringBuilder();
        for(int i = start; i < strText.length(); i++) {
	          sb.append("*");
        }
		
		return stringMask + sb;
	}
	
	public static String maskingBirthDate(String strText) {
		String[] stringSplit = strText.split("-");
		
		StringBuilder sbm = new StringBuilder();
        for(int i = 0; i < stringSplit[1].length(); i++) {
	          sbm.append("*");
        }
        
		StringBuilder sby = new StringBuilder();
        for(int i = 0; i < stringSplit[2].length() - 1; i++) {
	          sby.append("*");
        }
		
		
		return stringSplit[0] + " - " + sbm + " - " + sby + stringSplit[2].substring(3);
	}
	
	public static String maskingBankname(String strText) {
		String[] stringSplit = strText.split(" ");
		
		StringBuilder sbMask = new StringBuilder();
		StringBuilder sbComp = new StringBuilder();
		for(int i = 0; i < stringSplit.length; i++) {
			for(int j = 0; j < stringSplit[i].length() - 1; j++) {
				sbMask.append("*");
			}
			sbComp.append(stringSplit[i].substring(0, 1) + "" + sbMask + " ");
			sbMask.setLength(0);
		}

		return sbComp + "";
	}
	
	public static Date addHoursToDate(Date date, int hours) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.add(Calendar.HOUR_OF_DAY, hours);
	    return calendar.getTime();
	}
	
	public static String decodeJwtToken(String jwtToken){
        Base64 base64Url = new Base64(true);
        return new String(base64Url.decode(jwtToken));
        
    }
	
	public static byte[] getBase64EncodedImage(String imageURL) throws IOException{
	    URL url = new URL(imageURL); 
	    InputStream is = url.openStream();  
	    byte[] bytes = IOUtils.toByteArray(is); 
	    return bytes;
	}
	
}
