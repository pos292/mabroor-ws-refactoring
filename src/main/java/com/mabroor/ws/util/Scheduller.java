package com.mabroor.ws.util;

import com.mabroor.ws.dto.ResFifAmitraDto;
import com.mabroor.ws.dto.ResFifAmitraListDataDto;
import com.mabroor.ws.dto.ResFifAmitraListDto;
import com.mabroor.ws.entity.MstApplication;
import com.mabroor.ws.entity.MstPengajuanUmroh;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.repository.PembiayaanUmrohRepository;
import com.mabroor.ws.service.ApplicationService;
import com.mabroor.ws.service.FifAmitraService;
import com.mabroor.ws.service.PembiayaanUmrohService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.impl.MailClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.ExecutionException;

@EnableScheduling
@Configuration
public class Scheduller {

    private Logger LOG = Logger.getLogger(Scheduller.class);

    @Autowired
    private PembiayaanUmrohRepository pembiayaanUmrohRepository;

    @Autowired
    private PembiayaanUmrohService pembiayaanUmrohService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private FifAmitraService fifAmitraService;

    @Autowired
    private PushNotificationService pushNotificationService;

    @Scheduled(cron = "0 0 15 * * *")
    private void updateStatusPengajuanUmroh(){
        List<MstPengajuanUmroh> listPengajuanUmroh = pembiayaanUmrohRepository.findAll();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest();
        for (MstPengajuanUmroh mstPengajuanUmroh : listPengajuanUmroh){
            LOG.info("Start Update status pengajuan for id " + mstPengajuanUmroh.getId());
            LOG.info("Current Moxa Status = " + mstPengajuanUmroh.getStatus());
            String intLeadsId = mstPengajuanUmroh.getIntegrationLeadsId();
            if(intLeadsId != null && !intLeadsId.isEmpty()){
                ResFifAmitraListDataDto digitalLeadsStatus = fifAmitraService.getDigitalLeadStatus(intLeadsId);
                if(digitalLeadsStatus != null){
                    String statusPengajuan = String.valueOf(digitalLeadsStatus.getResult().get(0).get("status"));
                    LinkedHashMap<String,String> dataAmitra = (LinkedHashMap<String, String>) digitalLeadsStatus.getResult().get(0).get("dataAmitra");
                    String digitalLeadStatus = dataAmitra.get("digitalLeadStatus");
                    if(statusPengajuan != null && !statusPengajuan.isEmpty() && digitalLeadStatus != null && !digitalLeadStatus.isEmpty()){
                        LOG.info("Status Amitra = " + statusPengajuan);
                        LOG.info("Digital Status Amitra = " + digitalLeadStatus);
                        String statusKetMoxa = "";
                        String statusCodeMoxa = "";

                        if(statusPengajuan.equalsIgnoreCase(Constant.AMITRA_STATUS_SUBMITTED) &&
                                digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_TERKIRIM)){
                            statusKetMoxa = "Pengajuan Terkirim";
                            statusCodeMoxa = "Inprogress";
                            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Dikirim");
                        }else if (statusPengajuan.equalsIgnoreCase(Constant.AMITRA_STATUS_SUBMITTED) &&
                                digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_DIPROSES)) {
                            statusKetMoxa = "Pengajuan Diproses";
                            statusCodeMoxa = "In Progress";
                            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Diproses");
                        }else if (statusPengajuan.equalsIgnoreCase(Constant.AMITRA_STATUS_CANCEL_ORDER) &&
                                digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_DIBATALKAN)) {
                            statusKetMoxa = "Pengajuan Dibatalkan";
                            statusCodeMoxa = "Cancelled";
                            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Dibatalkan");
                        }else if (statusPengajuan.equalsIgnoreCase(Constant.AMITRA_STATUS_CANCEL_ORDER) &&
                                digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_TIDAK_DAPAT_DIHUBUNGI)) {
                            statusKetMoxa = "Tidak Dapat Dihubungi";
                            statusCodeMoxa = "Unreachable";
                            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Dibatalkan Karena Nomor Telepon Anda Tidak Dapat Kami Hubungi");
                        }else if (statusPengajuan.equalsIgnoreCase(Constant.AMITRA_STATUS_REJECTED) &&
                                (digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_TIDAK_DISETUJUI) ||
                                        digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_TIDAK_DISETUJUI2))) {
                            statusKetMoxa = "Pengajuan Ditolak";
                            statusCodeMoxa = "Rejected";
                            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Ditolak");
                        }else if (statusPengajuan.equalsIgnoreCase(Constant.AMITRA_STATUS_CONTRACT) &&
                                digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_KONTRAK)) {
                            statusKetMoxa = "Pengajuan Disetujui";
                            statusCodeMoxa = "Approved";
                            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Disetujui, Kontrak Telah Terbentuk");
                        }else if (statusPengajuan.equalsIgnoreCase(Constant.AMITRA_STATUS_APPROVED) &&
                                digitalLeadStatus.contains(Constant.AMITRA_DIGITAL_LEAD_STATUS_DISETUJUI)) {
                            statusKetMoxa = "Pengajuan Disetujui";
                            statusCodeMoxa = "Approved";
                            pushNotificationRequest.setMessage("Pengajuan Pembiayaan Disetujui");
                        }


                        if(!mstPengajuanUmroh.getStatus().equalsIgnoreCase(statusKetMoxa)){
                            // update status in table mst_pengajuan_umroh
                            mstPengajuanUmroh.setStatus(statusKetMoxa);
                            pembiayaanUmrohService.saveStatus(mstPengajuanUmroh);

                            // update status in table mst_application
                            MstApplication mstApplication = applicationService.findByCarBaru(mstPengajuanUmroh.getId());
                            mstApplication.setStatus(statusCodeMoxa);
                            applicationService.save(mstApplication);

                            // notify to user
                            pushNotificationRequest.setTitle("Pembiyaan Umroh");
                            pushNotificationRequest.setTopic("pengajuan-umroh");
                            pushNotificationRequest.setToken(mstPengajuanUmroh.getMstUserMobile().getFirebaseToken());
                            Map<String, String> data = new HashMap<>();
                            pushNotificationRequest.setData(data);

                            try {
                                pushNotificationService.sendMessageToToken(pushNotificationRequest);
                            } catch (InterruptedException | ExecutionException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            LOG.info("End Update Status for id = "+ mstPengajuanUmroh.getId());
        }
    }
}
