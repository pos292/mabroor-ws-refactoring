package com.mabroor.ws.util;

import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import java.security.Key;

import io.jsonwebtoken.*;

import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;


public class JWTGenerator {

	//length 512
    private static String SECRET_KEY = "bYTUrylM2tBeQqeRwqBM6kmhvDqBezxOnUpWzHdhXplkUCy80xpg0zAxO7ZvA7HPrQ2Ka7E3ZhkZPDYJPpqt93cvgrFKr4IgTpRVGB4BG1OTB5NjHFqNeTbNrlcx2WqhGNdT9x973WrU7F63rxb8hCoz7f6dNHPBZgnLURWvfcHDpIXjNkcjooYnYUtttDSYXvWZ8btLjkpmvYlFHjdpwdPPUvK2STWqVDexYLTHGNrFiOgnvtmGWnXHSN2MfkU3tOjRXuOlCm6TMTuwWzSPybufPkJNY9KAUIaT6KwTYgHeSL7K72VqCU4KM4aKbkI4jVOgolPDNKXahJi3VoxuTvTyVyFySIQguELoHZp7ytZQubTqNHW60yYz9pWA6f4ZVGBZlbiIMANd3aIfHmihuF6dr0k4wGG1GKtPeT2T1gu7ONDzVnM7E8zNupAB4t4Di9DKFwsSRkrH5HvN0nP9hAS8bIkgpq8z2H5aMRpRWFQvCcTzzBVFbDWhNlUgljRD";
    private static final JWTGenerator instance = new JWTGenerator();
    
    private JWTGenerator() {
	}
	
	public static JWTGenerator getInstance() {
		return instance;
	}
    
    //Sample method to construct a JWT
    public String createJWT(String id, String issuer, String subject, long ttlMillis, String payload) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        String idEncrypted = AESUtils.encrypt(id, SECRET_KEY);
        String issuerEncrypted = AESUtils.encrypt(issuer, SECRET_KEY);
        String subjectEncrypted = AESUtils.encrypt(subject, SECRET_KEY);

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(idEncrypted)
                .setIssuedAt(now)
                .setSubject(subjectEncrypted)
                .setIssuer(issuerEncrypted)
                .setAudience(payload)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public Claims decodeJWT(String jwt) throws IllegalBlockSizeException {

        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                .parseClaimsJws(jwt).getBody();

        claims.setId(AESUtils.decrypt(claims.getId(), SECRET_KEY));
        claims.setIssuer(AESUtils.decrypt(claims.getIssuer(), SECRET_KEY));
        claims.setSubject(AESUtils.decrypt(claims.getSubject(), SECRET_KEY));

        return claims;
    }

}