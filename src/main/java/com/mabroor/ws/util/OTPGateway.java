package com.mabroor.ws.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

@Component
public final class OTPGateway {
	private static final String PARAM_AWO_URL = "https://astraapps.astra.co.id/awosmsapi/clientintegrationservice/api/clientintegration/otp";

	private static final OTPGateway instance = new OTPGateway();
	private static final Logger LOG = Logger.getLogger(OTPGateway.class);

	@Value("${otp.username}")
	private String _PARAM_USR;
	
	@Value("${otp.sec}")
	private String _PARAM_SEC;
	
	@Value("${otp.sender}")
	private String _PARAM_SENDER;
    
    private static String PARAM_USR;
    private static String PARAM_SEC;
    private static String PARAM_SENDER;
    
    @PostConstruct     
	private void initStaticClass () {
    	PARAM_USR = this._PARAM_USR;
    	PARAM_SEC = this._PARAM_SEC;
    	PARAM_SENDER = this._PARAM_SENDER;
	}

	private OTPGateway() {
	}
	
	public static OTPGateway getInstance() {
		return instance;
	}

	public Map<String, Object> sendOTP(String phoneNumber, String OTPCode) {
		try {
			URL obj = new URL(PARAM_AWO_URL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			//01. Add Request Header
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setDoOutput(true);
			
			String message =  Constant.OTP_MESSAGE.replaceAll("\\{otpCode\\}", OTPCode);

			Gson gsonObj = new Gson();
			Map<String, String> inputMap = new HashMap<String, String>();
			inputMap.put("user", PARAM_USR);
			inputMap.put("pwd", PARAM_SEC);
			inputMap.put("sender", PARAM_SENDER);
			inputMap.put("msisdn", phoneNumber);
			inputMap.put("message", message);
			String urlParameters = gsonObj.toJson(inputMap);

			LOG.info(urlParameters);

			//02. Send Post Request
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			LOG.info(response.toString());

			return JsonParserFactory.getJsonParser().parseMap(response.toString());
		} catch (MalformedURLException e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

//	public static void main(String[] args) throws IOException{
		//OTPGateway.getInstance().sendOTP("081xxxxxxxxxx", OTPGateway.PARAM_MEDIA_TYPE_VOI, "1234", true);
//	}
	
}
