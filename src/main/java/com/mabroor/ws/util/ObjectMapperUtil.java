package com.mabroor.ws.util;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.collection.spi.PersistentCollection;
import org.modelmapper.Condition;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MappingContext;

public final class ObjectMapperUtil {

	private static ModelMapper modelMapper = new ModelMapper();


	static {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT)
		.setPropertyCondition(new Condition<Object, Object>() {
			public boolean applies(MappingContext<Object, Object> context) {
				return !(context.getSource() instanceof PersistentCollection);
			}
		});
	}


	private ObjectMapperUtil() {
	}


	public static <D, S> D map(final S source, Class<D> destinationClass) {
		return modelMapper.map(source, destinationClass);
	}


	public static <D, E> List<D> mapAll(final Collection<E> sourceList, Class<D> destinationCLass) {
		return sourceList.stream()
				.map(entity -> map(entity, destinationCLass))
				.collect(Collectors.toList());
	}

}
