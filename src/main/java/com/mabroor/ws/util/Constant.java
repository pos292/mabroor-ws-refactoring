package com.mabroor.ws.util;

public class Constant {
	protected Constant() {
	}

	public static final int THUMB_WIDTH = 130;
	public static final int THUMB_HEIGHT = 250;
	public static final int OTP_EXPIRY_TIME = 3;// 3 menit otp expired
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String OTP_MESSAGE = "KODE OTP: {otpCode} untuk verifikasi akun Moxa Mabroor. Kode OTP berlaku selama 3 menit. Kode bersifat RAHASIA";
	
	public static final String LEADS_TYPE_ALATBERAT = "NEWALATBERAT";
	public static final String LEADS_TYPE_ALATBERAT_BEKAS = "USEDALATBERAT";
	public static final String LEADS_TYPE_ASURANSIJIWA = "ASURANSIJIWA";
	public static final String LEADS_TYPE_ASURANSIKEBAKARAN = "ASURANSIKEBAKARAN";
	public static final String LEADS_TYPE_ASURANSIMOBIL = "ASURANSIMOBIL";
	public static final String LEADS_TYPE_ASURANSIKRITIS = "ASURANSIPENYAKITKRITIS";
	public static final String LEADS_TYPE_ASURANSICOVID = "ASURANSICOVID";
	public static final String LEADS_TYPE_MOBIL = "NEWCAR";
	public static final String LEADS_TYPE_MOBIL_BEKAS = "USEDCAR";
	public static final String LEADS_TYPE_MOTOR = "NEWMCY";
	public static final String LEADS_TYPE_MOTOR_BEKAS = "USEDMCY";
	public static final String LEADS_TYPE_KDA_MOBIL = "KDACAR";
	public static final String LEADS_TYPE_KDA_MOTOR = "KDAMCY";
	public static final String LEADS_TYPE_KTA = "TANPAAGUNAN";
	public static final String LEADS_TYPE_RENTBUS = "RENTBUS";
	public static final String LEADS_TYPE_RENTCAR = "RENTCAR";
	public static final String LEADS_TYPE_AIRPORTTRANSFER = "AIRPORTTRANSFER";
	public static final String LEADS_TYPE_GARDAEDUME = "GARDAEDUME";
	public static final String LEADS_TYPE_GARDAEDUME_MIKRO = "GARDAEDUMEMIKRO";
	public static final String LEADS_TYPE_GOLD_FINANCE= "GOLDFINANCING";

	public static final String LEADS_TYPE_PINJAMAN = "PINJAMANTANPAJAMINANSYARIAH";
	
	/* constant untuk variable berulang */
	public static final String RESPONSE_STATUS_KEY = "status";
	public static final String RESPONSE_DESCRIPTION_KEY = "description";
	public static final String RESPONSE_DESCRIPTION_VALUE = "succesfully get data";
	public static final String RESPONSE_SUCCESS_STATUS_VALUE = "success";
	public static final String RESPONSE_FAILED_STATUS_VALUE = "failed";

	public static final String AMITRA_STATUS_SUBMITTED = "SUBMITTED";

	public static final String AMITRA_STATUS_CANCEL_ORDER = "CANCEL ORDER";

	public static final String AMITRA_STATUS_REJECTED = "REJECTED";

	public static final String AMITRA_STATUS_CONTRACT = "CONTRACT";

	public static final String AMITRA_STATUS_APPROVED = "APPROVED";

	public static final String AMITRA_DIGITAL_LEAD_STATUS_TERKIRIM = "berhasil terkirim";
	public static final String AMITRA_DIGITAL_LEAD_STATUS_DIPROSES = "dalam proses";

	public static final String AMITRA_DIGITAL_LEAD_STATUS_DIBATALKAN = "telah dibatalkan";

	public static final String AMITRA_DIGITAL_LEAD_STATUS_TIDAK_DAPAT_DIHUBUNGI= "tidak dapat kami hubungi";

	public static final String AMITRA_DIGITAL_LEAD_STATUS_TIDAK_DISETUJUI = "belum dapat kami setujui";

	public static final String AMITRA_DIGITAL_LEAD_STATUS_TIDAK_DISETUJUI2 = "belum dapat disetujui";

	public static final String AMITRA_DIGITAL_LEAD_STATUS_KONTRAK = "kontrak pembiayaan telah terbentuk";

	public static final String AMITRA_DIGITAL_LEAD_STATUS_DISETUJUI = "telah disetujui";



}
