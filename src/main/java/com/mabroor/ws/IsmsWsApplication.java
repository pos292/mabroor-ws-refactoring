package com.mabroor.ws;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;


@SpringBootApplication
@EnableAsync
@RefreshScope
@EnableFeignClients(basePackages = {"com.mabroor.ws.rest"})
@OpenAPIDefinition(info = @Info(title = "Moxa Mabroor API", version = "1.0", description = "API Integration for Mabroor Mobile Application"))
public class IsmsWsApplication extends SpringBootServletInitializer {
	
	@PostConstruct
	public void init(){
	    // Setting Spring Boot SetTimeZone - Asia/Jakarta
	    TimeZone.setDefault(TimeZone.getTimeZone("Asia/Jakarta"));
	}

	public static void main(String[] args) {
		SpringApplication.run(IsmsWsApplication.class, args);
	}
}
