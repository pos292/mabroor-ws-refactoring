package com.mabroor.ws.configuration;

import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;

import com.mabroor.ws.dto.RespMessageDto;
import com.mabroor.ws.dto.ResponseDto;
import com.mabroor.ws.exception.AuthorizationException;
import com.mabroor.ws.exception.BadRequestException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.ExceptionResponse;
import com.mabroor.ws.util.Constant;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {
	private static final Logger LOG = Logger.getLogger(GlobalExceptionHandler.class);
	
	HttpStatus httpStatus = null;
	
	@ExceptionHandler(value={MalformedJwtException.class})
	public ResponseEntity<ExceptionResponse> handleMalformedJwtException(MalformedJwtException e){
		LOG.info(e.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        response.setStatus(Constant.RESPONSE_FAILED_STATUS_VALUE);
        response.setDescription("Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
		
		return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(value={IllegalBlockSizeException.class})
	public ResponseEntity<ExceptionResponse> handleIllegalBlockSizeException(IllegalBlockSizeException e){
		LOG.info(e.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        response.setStatus(Constant.RESPONSE_FAILED_STATUS_VALUE);
        response.setDescription("Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
		
		return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(value={NullPointerException.class})
	public ResponseEntity<ExceptionResponse> handleNullPointerException(NullPointerException e){
		LOG.info(e.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        response.setStatus(Constant.RESPONSE_FAILED_STATUS_VALUE);
        response.setDescription("Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
		
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value={StackOverflowError.class})
	public ResponseEntity<ExceptionResponse> handleStackOverflowError(StackOverflowError e){
		LOG.info(e.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        response.setStatus(Constant.RESPONSE_FAILED_STATUS_VALUE);
        response.setDescription("Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
		
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value={RuntimeException.class})
	public ResponseEntity<ExceptionResponse> handleRuntimeException(RuntimeException e){
		e.printStackTrace();
		LOG.info(e.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        response.setStatus(Constant.RESPONSE_FAILED_STATUS_VALUE);
        response.setDescription("Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
		
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value={OutOfMemoryError.class})
	public ResponseEntity<ExceptionResponse> handleOutOfMemoryError(OutOfMemoryError e){
		LOG.info(e.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        response.setStatus(Constant.RESPONSE_FAILED_STATUS_VALUE);
        response.setDescription("Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
		
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value={ExpiredJwtException.class})
	public ResponseEntity<ExceptionResponse> handleExpiredJwtException(ExpiredJwtException e){
		LOG.info(e.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        response.setStatus(Constant.RESPONSE_FAILED_STATUS_VALUE);
        response.setDescription("Sesi Anda sudah habis, silakan melakukan login kembali.");
		
		return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler({ UsernameNotFoundException.class, IOException.class,
			AuthorizationException.class, GeneralSecurityException.class,
			IOException.class,ArrayIndexOutOfBoundsException.class, IllegalArgumentException.class, BadRequestException.class})
	public ResponseEntity<RespMessageDto> exception(RuntimeException exception) {
		RespMessageDto message = RespMessageDto.builder()
				.reqId(UUID.randomUUID().toString())
				.status(String.valueOf(HttpStatus.BAD_REQUEST.value()))
				.error("true")
				.message(exception.getMessage())
				.build();
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<RespMessageDto> badRequestExceptionHandler(MethodArgumentNotValidException e){
		List<String> errors = new ArrayList<>();
		e.getBindingResult().getFieldErrors().forEach(a->errors.add(a.getDefaultMessage()));
		RespMessageDto response = RespMessageDto.builder()
				.status(String.valueOf(HttpStatus.BAD_REQUEST.value()))
				.error("true")
				.message(errors.size() > 1 ? errors : String.valueOf(errors.get(0)))
				.build();
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}
}
