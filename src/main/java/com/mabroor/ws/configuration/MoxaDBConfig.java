package com.mabroor.ws.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "moxaEntityManagerFactory", transactionManagerRef = "moxaTransactionManager", basePackages = {
		"com.mabroor.moxa.repository" })
public class MoxaDBConfig {
	@Bean(name = "moxaDataSource")
	@ConfigurationProperties(prefix = "moxa.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "moxaEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean moxaEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("moxaDataSource") DataSource dataSource, Environment env) {
		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.physical_naming_strategy", env.getProperty("hibernate.physical_naming_strategy"));
		return builder.dataSource(dataSource).packages("com.mabroor.moxa").persistenceUnit("moxa").properties(properties).build();
	}

	@Bean(name = "moxaTransactionManager")
	public PlatformTransactionManager moxaTransactionManager(
			@Qualifier("moxaEntityManagerFactory") EntityManagerFactory moxaEntityManagerFactory) {
		return new JpaTransactionManager(moxaEntityManagerFactory);
	}
}
