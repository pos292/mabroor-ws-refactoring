package com.mabroor.ws.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.header.writers.StaticHeadersWriter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, proxyTargetClass = true)
public class ResourceServerConfig extends WebSecurityConfigurerAdapter {
	
	
	@Value("${fifhaji.basic.username}")
	private String basicusrFifHaji;
	
	@Value("${fifhaji.basic.pass}")
	private String basicpwdFifHaji;
	
	
	@Autowired
	private AuthenticationEntryPoint authEntryPoint;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser(basicusrFifHaji).password("{noop}" + basicpwdFifHaji).authorities("ROLE_HAJI");
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		
		http.cors();
		http.headers().frameOptions().sameOrigin();
		http.headers().contentTypeOptions();
	    http.headers().httpStrictTransportSecurity().includeSubDomains(true).maxAgeInSeconds(31536000);
	    http.headers().addHeaderWriter(new StaticHeadersWriter("X-Content-Security-Policy","script-src 'self'"));
	    
	    http.csrf().disable().antMatcher("/v1/api/wisatareligi/updateStatusHaji")
        .authorizeRequests().anyRequest().hasRole("HAJI")
        .and().httpBasic().authenticationEntryPoint(authEntryPoint)
        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
}
