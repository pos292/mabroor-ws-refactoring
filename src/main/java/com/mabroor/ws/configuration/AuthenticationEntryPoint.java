package com.mabroor.ws.configuration;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

@Component
public class AuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

	@Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authEx)
      throws IOException {
        response.addHeader("WWW-Authenticate", "Basic realm=" +getRealmName());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        
        LinkedHashMap<String, Object> obj = new LinkedHashMap<String, Object>();
        obj.put("status", "failed");
        obj.put("description", "Unauthorized");
        
        writer.println(new Gson().toJson(obj));
    }

	@Override
    public void afterPropertiesSet() {
        setRealmName("permatamoxa realm");
        super.afterPropertiesSet();
    }

}
