package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.KecamatanService;

@RestController
@RequestMapping("/v1/api/kecamatan")
public class KecamatanController {

	@Autowired
	private KecamatanService camatServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",camatServ.getListKecamatan());
		return response;
	}
	
	@GetMapping(value = "/kabupaten/{kabupatenId}")
	public Map<String, Object> getKabupatenById(@PathVariable Long kabupatenId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",camatServ.findByKabupatenId(kabupatenId));
		return response;
	}
}
