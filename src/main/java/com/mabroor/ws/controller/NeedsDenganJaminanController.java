package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.NeedsDenganJaminanService;

@RestController
@RequestMapping("/v1/api/needs/denganjaminan")
public class NeedsDenganJaminanController {

	@Autowired
	private NeedsDenganJaminanService needDenganJaminanServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",needDenganJaminanServ.getListNeedsDenganJaminan());
		return response;
	}
	
	@GetMapping(value = "/{needsId}")
	public LinkedHashMap<String, Object> getNeedsDenganJaminanById(@PathVariable Long needsDenganJaminanId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",needDenganJaminanServ.findById(needsDenganJaminanId));
		return response;
	}
}
