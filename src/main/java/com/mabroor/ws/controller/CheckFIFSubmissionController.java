package com.mabroor.ws.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/api/fif")
public class CheckFIFSubmissionController {
	
	
	@GetMapping(value = "/checkfifsubmission")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Calendar c1 = Calendar.getInstance();
		c1.set(Calendar.HOUR_OF_DAY, 21);
		c1.set(Calendar.MINUTE, 00);
		c1.set(Calendar.SECOND, 00);
		Calendar c2 = Calendar.getInstance();
		c2.set(Calendar.HOUR_OF_DAY, 23);
		c2.set(Calendar.MINUTE, 59);
		c1.set(Calendar.SECOND, 59);
		System.out.println(sdf.format(c2.getTime()));
		Calendar c3 = Calendar.getInstance();
		c3.setTime(new Date());
		if(c3.after(c1) && c3.before(c2)) {
			Map<String, Object> map = new HashMap<>();
			map.put("MGU2W", "DISABLED");
			map.put("HAJI", "DISABLED");
			response.put("status", "success");
			response.put("description", "Maaf saat ini pengajuanmu tidak bisa dilanjutkan karena ada pemeliharaan sistem pada jam 21.00 - 24.00. Silakan coba kembali setelah jam 24.00.");
	        response.put("data", map);
		 }else {
			 Map<String, Object> map = new HashMap<>();
			 map.put("MGU2W", "ENABLED");
			 map.put("HAJI", "ENABLED");
			 response.put("status", "success");
		     response.put("data", map);
		 }
		
		return response;
	}

}
