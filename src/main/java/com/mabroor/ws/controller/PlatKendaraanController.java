package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.PlatDto;
import com.mabroor.ws.entity.MstPlatKendaraan;
import com.mabroor.ws.service.PlatKendaraanService;

@RestController
@RequestMapping("/v1/api/platkendaraan")
public class PlatKendaraanController {

	@Autowired
	private PlatKendaraanService platServ;
	
	 @PersistenceContext
		private EntityManager em;
	
	 @GetMapping(value = "/list")
	   	@ResponseBody
	   	public LinkedHashMap<String, Object> getList() {
	   		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
	   		response.put("status", "success");
	   		response.put("description", "succesfully get data");
	   		List<MstPlatKendaraan> platList = platServ.getListPlat();
	   		List<PlatDto> platDto = new ArrayList <PlatDto>();
	   		if(platList != null && platList.size()>0) {
	   			for(MstPlatKendaraan mstPlat : platList) {
	   				em.detach(mstPlat);
	   				PlatDto platListDto = new PlatDto();
	   				platListDto.setId(mstPlat.getId());
	   				platListDto.setKodePlatWilayah(mstPlat.getPlatCode() + " - " + mstPlat.getPlatDescription());
	   				platDto.add(platListDto);
	   				
	   			}
	   		}
	   		response.put("data", platDto);
	   		return response;
	   		
}
}
