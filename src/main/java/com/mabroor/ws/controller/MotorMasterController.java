package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.CalculateMrpMotorReqDto;
import com.mabroor.ws.service.MotorMasterService;

@RestController
@RequestMapping("/v1/api/mastermotor")
public class MotorMasterController {
	@Autowired
	private MotorMasterService motorMasterServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> findAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",motorMasterServ.findAll());
		return response;
	}
	
	@GetMapping(value = "/brand/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getBrandMotor() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",motorMasterServ.findByBrandMotor());
		return response;
	}
	
	@GetMapping(value = "/type/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getListType() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",motorMasterServ.findByTypeMotorDistinct());
		return response;
	}
	 
	@GetMapping(value = "/type/{brand}")
	@ResponseBody
	public LinkedHashMap<String, Object> getTypeMotor(@PathVariable String brand) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
	   	response.put("status", "success");
	   	response.put("description", "succesfully get data");
	   	response.put("data",motorMasterServ.findByTypeMotor(brand));
	   	return response;
	}
	
	@GetMapping(value = "/model/{type}")
	@ResponseBody
	public LinkedHashMap<String, Object> getModelMotor(@PathVariable String type) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
	   	response.put("status", "success");
	   	response.put("description", "succesfully get data");
	   	response.put("data",motorMasterServ.findByModelMotor(type));
	   	return response;
	}
	
	@GetMapping(value = "/model/{brand}/{type}")
	@ResponseBody
	public LinkedHashMap<String, Object> getModelMotor2(@PathVariable String brand, @PathVariable String type) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
	   	response.put("status", "success");
	   	response.put("description", "succesfully get data");
	   	response.put("data",motorMasterServ.findByBrandTypeModelMotor(brand, type));
	   	return response;
	}
	
	@GetMapping(value = "/year/{model}")
	@ResponseBody
	public LinkedHashMap<String, Object> getYearMotor(@PathVariable String model) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
	   	response.put("status", "success");
	   	response.put("description", "succesfully get data");
	   	response.put("data",motorMasterServ.findByYearMotor(model));
	   	return response;
	}
	
	 @GetMapping(value = "/year/{brand}/{type}/{model}")
	   	@ResponseBody
	   	public LinkedHashMap<String, Object> getYearsMotor(@PathVariable String brand, @PathVariable String type, @PathVariable String model) {
	   		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
	   		response.put("status", "success");
	   		response.put("description", "succesfully get data");
	   		response.put("data",motorMasterServ.findByYearAndModelTypeMotor(brand, type, model));
	   		return response;
	   	}
	
    @PostMapping(value = "/calculatemrp")
    @ResponseBody
    public LinkedHashMap<String, Object> getCalculateMrp(@RequestBody CalculateMrpMotorReqDto calculateMrpMotor){
    	LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",motorMasterServ.findByCalculateMrp(calculateMrpMotor));
		return response;
    }
}
