package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Optional;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.entity.MstActivityUser;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.service.ActivityUserService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/v1/api/activity/user")
@Tag(name = "User Mobile Activity", description = "API to record user mobile activity")
public class ActivityUserController {
	private static final Logger LOG = Logger.getLogger(ActivityUserController.class);

	@Autowired
	private ActivityUserService actServ;
	
	@Autowired
	private UserMobileService userMobileServ;
	
	@PostMapping(produces = "application/json")
	@ResponseBody
	@Parameter(name = "appToken",
				description = "User Mobile App Token, get this token by login.", 
				content = { @Content(schema = @Schema(example = "eyJhbGciOi********************")) },
				in = ParameterIn.HEADER)
	@Operation(summary = "User Mobile Activity - Save Data")
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "Data Found", content = { @Content(mediaType = "application/json", schema = @Schema(example = "{\"status\":\"success\",\"desciption\":\"Success! Activit User successfully submitted.\"}", type = "object")) }),
			@ApiResponse(responseCode = "401", description = "appToken doesn't match or appToken Expired", content = @Content(schema = @Schema(hidden = true))),
			@ApiResponse(responseCode = "500", description = "There is an internal error", content = @Content(schema = @Schema(hidden = true))) 
	})
	public ResponseEntity<LinkedHashMap<String, Object>> save(@RequestBody MstActivityUser mstAct, @RequestHeader(value = "appToken", required = true) String token) throws IllegalBlockSizeException {


		LOG.info(mstAct.getDeviceModel());
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;

		
		Claims claims = JWTGenerator.getInstance().decodeJWT(token);
		if (Objects.isNull(claims)) {
			response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		}else {
			Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
			MstUserMobile mstUserMobile = null;
			if (!mstUserMobileOpt.isPresent()) {
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}else {
				mstUserMobile = mstUserMobileOpt.get();
				if (claims.getId().equals(""+mstUserMobile.getId())&& token.equals(mstUserMobile.getToken())){
					mstAct.setUserId(mstUserMobile.getId());
					actServ.save(mstAct);
					response.put(Constant.RESPONSE_STATUS_KEY, "success");
					response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Success! Activit User successfully submitted.");
					httpStatus = HttpStatus.OK;
				}else {
					response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
					response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}
			}
		}
		return new ResponseEntity<>(response, httpStatus);
    }
}
