package com.mabroor.ws.controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Optional;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.entity.MstPengajuanQurban;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.service.PengajuanQurbanService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.util.JWTGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/pengajuanqurban")
public class PengajuanQurbanController {
	
	private static final Logger LOG = Logger.getLogger(PengajuanQurbanController.class);
	
	@Autowired
	private PengajuanQurbanService qurbanServ;

	@Autowired
	private UserMobileService userMobileServ;
	
	@PostMapping
	@ResponseBody
	public ResponseEntity save(@RequestBody MstPengajuanQurban mstQurban, @RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;
		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				MstUserMobile mstUserMobile = null;
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					mstUserMobile = mstUserMobileOpt.get();
					System.out.println(mstUserMobile.getEmail());
					if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){
						String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
						String lastname = mstUserMobile.getLastName() != null ? mstUserMobile.getLastName() : "";
						String fullname = firstName;
						if(!"".equals(lastname)) {
							fullname += " " + lastname;
						}
						mstQurban.setQurbanStatus("01");;
						mstQurban.setUserId(mstUserMobile.getId());
						mstQurban.setEmailDataUser(mstUserMobile.getEmail());
						mstQurban.setPhoneDataUser(mstUserMobile.getPhone());
						mstQurban.setNameDataUser(fullname);
						mstQurban.setTransactionDateTime(new Date());
						
						//default set leads user to user mobile
						mstQurban.setEmail(mstUserMobile.getEmail());
						mstQurban.setPhone(mstUserMobile.getPhone());
						mstQurban.setName(fullname);
						qurbanServ.save(mstQurban);
						response.put("status", "success");
						response.put("description", "Success! Pengajuan Qurban successfully submitted.");
						httpStatus = HttpStatus.OK;
					}else {
						response.put("status", "failed");
						response.put("description", "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
					}
				}
			}catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}catch(IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}catch (NullPointerException np) {
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch(RuntimeException re) {
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch(OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
			return new ResponseEntity<>(response, httpStatus);
		}

}
