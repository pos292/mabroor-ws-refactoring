package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.crypto.IllegalBlockSizeException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mabroor.ws.dto.*;
import com.mabroor.ws.entity.*;
import com.mabroor.ws.service.*;
import com.mabroor.ws.util.IdrFormatMoney;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.util.ObjectMapperUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/pengajuanlist")
public class PengajuanController {

	private static final Logger LOG = Logger.getLogger(PengajuanController.class);

	@Autowired
	private PengajuanMotorService motorServ;

	@Autowired
	private WisataReligiService wisataServ;
	
	@Autowired
	private PengajuanAsuransiMobilService mobilServ;
	
	@Autowired
	private PengajuanAsuransiMobilPerluasanService perluasanServ;
	
	@Autowired
	private PengajuanCarService pengajuanService;
	
	@Autowired
	private PengajuanJaminanService jaminanServ;

	@Autowired
	private DetailPinjamanService detailPinjamanService;

	@Autowired
	private UserMobileService userMobileServ;

	@Autowired
	private PengajuanAllService pengajuanAllService;

	@Autowired
	private PembiayaanUmrohService pembiayaanUmrohService;

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@GetMapping(value = "/{profileId}")
	@ResponseBody
	public ResponseEntity getList(@RequestHeader(value = "appToken", required = true) String token,
			@PathVariable Long profileId) {
		System.out.println(token);

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(profileId);
		MstUserMobile mstUserMobile = null;
		if (!mstUserMobileOpt.isPresent()) {
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} else {
			mstUserMobile = mstUserMobileOpt.get();

			try {
				Claims claims = JWTGenerator.getInstance().decodeJWT(token);
				if (Objects.isNull(claims)) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						PengajuanResponDto data = new PengajuanResponDto();

						List<MstPengajuanMotor> tmpPengajuanMotor = motorServ.findAllByUserId(mstUserMobile.getId());
						for (MstPengajuanMotor b : tmpPengajuanMotor) {
							em.detach(b);
							b.setKtpPhoto(this.imageBaseUrl + "pengajuan-motor/" + b.getKtpPhoto());
							b.setFotoUnit(this.imageBaseUrl + "pengajuan-motor/" + b.getFotoUnit());
						}
						data.setPengajuanMotorList(tmpPengajuanMotor);

						data.setPengajuanWisataReligiList(wisataServ.findAllByUserId(mstUserMobile.getId()));

						List<MstPengajuanMultigunaTanpaJaminan> get = detailPinjamanService.findallpengajuan();
						List<RespPengajuanDto> respPengajuan = get.stream().map(mstPengajuan -> {
							RespPengajuanDto dto = new RespPengajuanDto();
							dto.setPendapatan(mstPengajuan.getPendapatan());
							dto.setJumlahPinjaman(mstPengajuan.getJmlPinjaman());
							dto.setTenor(mstPengajuan.getTenor());
							dto.setKebutuhanPinjaman(mstPengajuan.getKebPinjaman());
							dto.setKebutuhanPinjamanLainnya(mstPengajuan.getKebPinjamanLainnya());
							dto.setKartuKredit(mstPengajuan.getKartuKredit());
							dto.setBankPenerbit(mstPengajuan.getBankPenerbit());
							dto.setStatus(mstPengajuan.getStatus());
							dto.setUserId(mstPengajuan.getUserId());
							dto.setTanggalPengajuan(mstPengajuan.getTanggalPengajuan().toString());
							return dto;
						}).collect(Collectors.toList());
						data.setPengajuanDtos(respPengajuan);

						response.put("status", "success");
						response.put("description", "succesfully get data");
						response.put("data", data);
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						response.put("data", null);
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			} catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (NullPointerException np) {
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (RuntimeException re) {
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}

		return new ResponseEntity<>(response, httpStatus);

	}

	@GetMapping(value = "/leads/detailbytype/{screenname}/{leadsId}")
	@ResponseBody
	public ResponseEntity getListCustoms(@RequestHeader(value = "appToken", required = true) String token,
			@PathVariable String screenname, @PathVariable Long leadsId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = HttpStatus.OK;
		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang.(00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						LOG.info("Prepare screen name : " + screenname);
						LOG.info("Prepare leads id : " + leadsId);
						if ("HajiDetail".equals(screenname)) {
							List<PengajuanAllCustomDto> tmpPengajuanList = pengajuanAllService.findLeadsByLeadsId(mstUserMobile.getId(), leadsId, "getPengajuanWisataReligiMapped");

							for (PengajuanAllCustomDto item : tmpPengajuanList) {
								Map<String, Object> objtemp = new HashMap<>();
								objtemp.put("leadsId", item.getLeadsId());
								objtemp.put("variable1", item.getVariable1());
								objtemp.put("variable2", item.getVariable2());
								objtemp.put("variable3", item.getVariable3());
								objtemp.put("variable4", item.getVariable4());
								objtemp.put("variable5", item.getVariable5());
								objtemp.put("variable6", item.getVariable6());
								objtemp.put("variable7", item.getVariable7());

								objtemp.put("detail", wisataServ.findById(item.getLeadsId()));

								response.put("data", objtemp);
							}
						}  else if ("NewMcyDetail".equals(screenname)) {
							List<PengajuanAllCustomDto> tmpPengajuanList = pengajuanAllService.findLeadsByLeadsId(mstUserMobile.getId(), leadsId, "getPengajuanMotorMapped");

							for (PengajuanAllCustomDto item : tmpPengajuanList) {
								Map<String, Object> objtemp = new HashMap<>();
								objtemp.put("leadsId", item.getLeadsId());
								objtemp.put("variable1", item.getVariable1());
								objtemp.put("variable2", item.getVariable2());
								objtemp.put("variable3", item.getVariable3());
								objtemp.put("variable4", item.getVariable4());
								objtemp.put("variable5", item.getVariable5());
								objtemp.put("variable6", item.getVariable6());
								objtemp.put("variable7", item.getVariable7());

								MstPengajuanMotor tmpPengajuanMotor = motorServ.findById(item.getLeadsId()).get();
								em.detach(tmpPengajuanMotor);
								tmpPengajuanMotor.setFotoUnit(
										this.imageBaseUrl + "pengajuan-motor/" + tmpPengajuanMotor.getFotoUnit());
								tmpPengajuanMotor.setKtpPhoto(
										this.imageBaseUrl + "pengajuan-motor/" + tmpPengajuanMotor.getKtpPhoto());
								objtemp.put("detail", tmpPengajuanMotor);

								response.put("data", objtemp);
							}
						}else if ("MGUJaminanDetail".equals(screenname)) {
							List<PengajuanAllCustomDto> tmpPengajuanList = pengajuanAllService.findLeadsByLeadsId(mstUserMobile.getId(), leadsId, "getPengajuanDenganJaminanMapped");
							
							for (PengajuanAllCustomDto item : tmpPengajuanList) {
								Map<String, Object> objtemp = new HashMap<>();
								objtemp.put("leadsId", item.getLeadsId());
								objtemp.put("variable1", item.getVariable1());
								objtemp.put("variable2", item.getVariable2());
								objtemp.put("variable3", item.getVariable3());
								objtemp.put("variable4", item.getVariable4());
								objtemp.put("variable5", item.getVariable5());
								objtemp.put("variable6", item.getVariable6());
								objtemp.put("variable7", item.getVariable7());
								
								MstPengajuanJaminan tmpPengajuanJaminan = jaminanServ.findById(item.getLeadsId())
										.get();
								em.detach(tmpPengajuanJaminan);
								tmpPengajuanJaminan.setFotoBpkbMobil(this.imageBaseUrl + "danatunai-jaminan/"
										+ tmpPengajuanJaminan.getFotoBpkbMobil());
								tmpPengajuanJaminan.setFotoBpkbMotor(this.imageBaseUrl + "danatunai-jaminan/"
										+ tmpPengajuanJaminan.getFotoBpkbMotor());
								tmpPengajuanJaminan.setFotoStnk(this.imageBaseUrl + "danatunai-jaminan/"
										+ tmpPengajuanJaminan.getFotoStnk());
								tmpPengajuanJaminan.setFotoKananDepan(this.imageBaseUrl + "danatunai-jaminan/"
										+ tmpPengajuanJaminan.getFotoKananDepan());
								tmpPengajuanJaminan.setFotoKananBelakang(this.imageBaseUrl + "danatunai-jaminan/"
										+ tmpPengajuanJaminan.getFotoKananBelakang());
								tmpPengajuanJaminan.setFotoKiriDepan(this.imageBaseUrl + "danatunai-jaminan/"
										+ tmpPengajuanJaminan.getFotoKiriDepan());
								tmpPengajuanJaminan.setFotoKiriBelakang(this.imageBaseUrl + "danatunai-jaminan/"
										+ tmpPengajuanJaminan.getFotoKiriBelakang());
								objtemp.put("detail", tmpPengajuanJaminan);
								
								response.put("data", objtemp);
							}
						}else if ("NewCarDetail".equals(screenname)) {
							List<PengajuanAllCustomDto> tmpPengajuanList = pengajuanAllService.findLeadsByLeadsId(mstUserMobile.getId(), leadsId, "getPengajuanMobilMapped");
							
							for (PengajuanAllCustomDto item : tmpPengajuanList) {
								Map<String, Object> objtemp = new HashMap<>();
								objtemp.put("leadsId", item.getLeadsId());
								objtemp.put("variable1", item.getVariable1());
								objtemp.put("variable2", item.getVariable2());
								objtemp.put("variable3", item.getVariable3());
								objtemp.put("variable4", item.getVariable4());
								objtemp.put("variable5", item.getVariable5());
								objtemp.put("variable6", item.getVariable6());
								objtemp.put("variable7", item.getVariable7());
								
								MstPengajuanCar tmpPengajuanCar = pengajuanService.findById(item.getLeadsId()).get();
								
								objtemp.put("detail", tmpPengajuanCar);
								
								response.put("data", objtemp);
							}
						}else if ("AsuransiMobilDetail".equals(screenname)) {
							List<PengajuanAllCustomDto> tmpPengajuanList = pengajuanAllService.findLeadsByLeadsId(mstUserMobile.getId(), leadsId, "getPengajuanAsuransiMobilMapped");
							
							for (PengajuanAllCustomDto item : tmpPengajuanList) {
								Map<String, Object> objtemp = new HashMap<>();
								objtemp.put("leadsId", item.getLeadsId());
								objtemp.put("variable1", item.getVariable1());
								objtemp.put("variable2", item.getVariable2());
								objtemp.put("variable3", item.getVariable3());
								objtemp.put("variable4", item.getVariable4());
								objtemp.put("variable5", item.getVariable5());
								objtemp.put("variable6", item.getVariable6());
								objtemp.put("variable7", item.getVariable7());
								
								objtemp.put("detail", mobilServ.findById(item.getLeadsId()).get());
								objtemp.put("perluasan", perluasanServ.findByPerluasanId(item.getLeadsId()));
								
								response.put("data", objtemp);
							}
						}else if ("LihatDetailUmroh".equals(screenname)) {
							List<PengajuanAllCustomDto> tmpPengajuanList = pengajuanAllService.findLeadsByLeadsId(mstUserMobile.getId(), leadsId, "getPembiayaanUmrohMapped");

							for (PengajuanAllCustomDto item : tmpPengajuanList) {
								Map<String, Object> objtemp = new HashMap<>();
								objtemp.put("leadsId", item.getLeadsId());
								objtemp.put("variable1", item.getVariable1());
								objtemp.put("variable2", item.getVariable2());
								objtemp.put("variable3", item.getVariable3());
								objtemp.put("variable4", item.getVariable4());
								objtemp.put("variable5", item.getVariable5());
								objtemp.put("variable6", item.getVariable6());
								objtemp.put("variable7", item.getVariable7());

								Optional<MstPengajuanUmroh> pengajuanUmroh =
										pembiayaanUmrohService.findById(item.getLeadsId());
								RespPembiayaanUmrohDto pembiayaanUmrohDto = new RespPembiayaanUmrohDto();
								pembiayaanUmrohDto.setIdPengajuan(pengajuanUmroh.get().getIntegrationLeadsId());
								pembiayaanUmrohDto.setStatusPengajuan(pengajuanUmroh.get().getStatus());
								pembiayaanUmrohDto.setJenisLayanan("Pembiayaan Umroh");
								pembiayaanUmrohDto.setPartnerPembiayaan("AMITRA");
								pembiayaanUmrohDto.setProdukPembiayaan("Porsi Umroh Reguler");
								String dp = IdrFormatMoney.currencyIdr(pengajuanUmroh.get().getDp().toString());
								pembiayaanUmrohDto.setDp(dp);
								pembiayaanUmrohDto.setTenor(pengajuanUmroh.get().getTenor());

								RespPembiayaanUmrohDetailDto pembiayaanUmrohDetail = new RespPembiayaanUmrohDetailDto();
								RespPembiayaanUmrohDetaiDataDiriDto pembiayaanUmrohDetailDataDiri = new RespPembiayaanUmrohDetaiDataDiriDto();
								RespPembiayaanUmrohDetailAlamatDto pembiayaanUmrohDetailAlamat = new RespPembiayaanUmrohDetailAlamatDto();
								RespPembiayaanUmrohDetailAlamatKtpDto pembiayaanUmrohDetailAlamatKtpDto = null;
								pembiayaanUmrohDetail.setPartnerPembiayaan("AMITRA");
								pembiayaanUmrohDetail.setProdukPembiayaan("Porsi Umroh Reguler");
								String dpdetail = IdrFormatMoney.currencyIdr(pengajuanUmroh.get().getDp().toString());
								pembiayaanUmrohDetail.setDp(dpdetail);
								pembiayaanUmrohDetail.setTenor(pengajuanUmroh.get().getTenor());

								pembiayaanUmrohDetailDataDiri.setNamaDepan(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getNamaDepan());
								pembiayaanUmrohDetailDataDiri.setNamaBelakang(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getNamaBelakang());
								pembiayaanUmrohDetailDataDiri.setJenisKelamin(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getGender());
								pembiayaanUmrohDetailDataDiri.setTempatLahir(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getTempatLahir());
								pembiayaanUmrohDetailDataDiri.setTanggalLahir(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getTanggalLahir());
								pembiayaanUmrohDetailDataDiri.setNamaIbuKandung(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getIbuKandung());
								pembiayaanUmrohDetailDataDiri.setNoHp(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getNoHp());
								pembiayaanUmrohDetailDataDiri.setNoKtp(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getNoKtp());

								pembiayaanUmrohDetailAlamat.setAlamat(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getAlamat());
								pembiayaanUmrohDetailAlamat.setProvinsi(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getProvinsi());
								pembiayaanUmrohDetailAlamat.setKota(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKota());
								pembiayaanUmrohDetailAlamat.setKecamatan(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKecamatan());
								pembiayaanUmrohDetailAlamat.setKelurahan(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKelurahan());
								pembiayaanUmrohDetailAlamat.setRt(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getRt());
								pembiayaanUmrohDetailAlamat.setRw(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getRw());
								pembiayaanUmrohDetailAlamat.setKodePos(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKodePos());
								if(!pembiayaanUmrohDetailAlamat.getAlamat().equalsIgnoreCase(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getAlamatKtp()) &&
										!pembiayaanUmrohDetailAlamat.getKodePos().equalsIgnoreCase(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKostpos_ktp())){
									pembiayaanUmrohDetailAlamatKtpDto = new RespPembiayaanUmrohDetailAlamatKtpDto();
									pembiayaanUmrohDetailAlamatKtpDto.setAlamatLengkap(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getAlamatKtp());
									pembiayaanUmrohDetailAlamatKtpDto.setKodePos(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKostpos_ktp());
									pembiayaanUmrohDetail.setRespPembiayaanUmrohDetailAlamatKtpDto(pembiayaanUmrohDetailAlamatKtpDto);
								}

								pembiayaanUmrohDetail.setRespPembiayaanUmrohDetaiDataDiriDto(pembiayaanUmrohDetailDataDiri);
								pembiayaanUmrohDetail.setRespPembiayaanUmrohDetailAlamatDto(pembiayaanUmrohDetailAlamat);
								objtemp.put("detail", pembiayaanUmrohDetail);

								response.put("data", objtemp);
							}
						}

						response.put("status", "success");
						response.put("description", "succesfully get data");
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@GetMapping(value = "/list/{profileId}/{limit}/{page}")
	@ResponseBody
	public ResponseEntity getListCustom(@RequestHeader(value = "appToken", required = true) String token,
			@PathVariable Long profileId, @PathVariable Long limit, @PathVariable Long page) {
		System.out.println(token);

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(profileId);
		MstUserMobile mstUserMobile = null;
		if (!mstUserMobileOpt.isPresent()) {
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} else {
			mstUserMobile = mstUserMobileOpt.get();

			try {
				Claims claims = JWTGenerator.getInstance().decodeJWT(token);
				if (Objects.isNull(claims)) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						PengajuanResponDto data = new PengajuanResponDto();

						List<PengajuanAllCustomDto> tmpPengajuanList = pengajuanAllService
								.findAllByUserIdPagging(profileId, limit, (limit * page));
						List<Map<String, Object>> objPengajuanList = new ArrayList<>();
						for (PengajuanAllCustomDto item : tmpPengajuanList) {
							Map<String, Object> objtemp = new HashMap<>();
							objtemp.put("leadsId", item.getLeadsId());
							objtemp.put("variable1", item.getVariable1());
							objtemp.put("variable2", item.getVariable2());
							objtemp.put("variable3", item.getVariable3());
							objtemp.put("variable4", item.getVariable4());
							objtemp.put("variable5", item.getVariable5());
							objtemp.put("variable6", item.getVariable6());
							objtemp.put("variable7", item.getVariable7());
							if (new Long(1).equals(item.getVariable1())) {
								MstPengajuanMotor tmpPengajuanMotor = motorServ.findById(item.getLeadsId()).get();
								em.detach(tmpPengajuanMotor);
								tmpPengajuanMotor.setFotoUnit(
										this.imageBaseUrl + "pengajuan-motor/" + tmpPengajuanMotor.getFotoUnit());
								objtemp.put("detail", tmpPengajuanMotor);
								tmpPengajuanMotor.setKtpPhoto(
										this.imageBaseUrl + "pengajuan-motor/" + tmpPengajuanMotor.getKtpPhoto());
								objtemp.put("detail", tmpPengajuanMotor);
							}else if (new Long(2).equals(item.getVariable1())) {

								objtemp.put("detail", wisataServ.findById(item.getLeadsId()));
							}
							else if (new Long(3).equals(item.getVariable1())) {
								
									MstPengajuanJaminan tmpPengajuanJaminan = jaminanServ.findById(item.getLeadsId())
											.get();
									em.detach(tmpPengajuanJaminan);
									tmpPengajuanJaminan.setFotoBpkbMobil(this.imageBaseUrl + "danatunai-jaminan/"
											+ tmpPengajuanJaminan.getFotoBpkbMobil());
									tmpPengajuanJaminan.setFotoBpkbMotor(this.imageBaseUrl + "danatunai-jaminan/"
											+ tmpPengajuanJaminan.getFotoBpkbMotor());
									tmpPengajuanJaminan.setFotoStnk(this.imageBaseUrl + "danatunai-jaminan/"
											+ tmpPengajuanJaminan.getFotoStnk());
									tmpPengajuanJaminan.setFotoKananDepan(this.imageBaseUrl + "danatunai-jaminan/"
											+ tmpPengajuanJaminan.getFotoKananDepan());
									tmpPengajuanJaminan.setFotoKananBelakang(this.imageBaseUrl + "danatunai-jaminan/"
											+ tmpPengajuanJaminan.getFotoKananBelakang());
									tmpPengajuanJaminan.setFotoKiriDepan(this.imageBaseUrl + "danatunai-jaminan/"
											+ tmpPengajuanJaminan.getFotoKiriDepan());
									tmpPengajuanJaminan.setFotoKiriBelakang(this.imageBaseUrl + "danatunai-jaminan/"
											+ tmpPengajuanJaminan.getFotoKiriBelakang());
									objtemp.put("detail", tmpPengajuanJaminan);
								}else if (new Long(4).equals(item.getVariable1())) {

									objtemp.put("detail", pengajuanService.findById(item.getLeadsId()));
								}else if (new Long(5).equals(item.getVariable1())) {
									MstPengajuanAsuransiMobil mobil = mobilServ.findById(item.getLeadsId()).get();
									DaftarPengajuanAsuransiMobilDto mobilDto = ObjectMapperUtil.map(mobil,
											DaftarPengajuanAsuransiMobilDto.class); 
									List<MstPengajuanAsuransiMobilPerluasan> perluasan = perluasanServ.findByPerluasanId(item.getLeadsId());
									List<DaftarPengajuanAsuransiBenefitDto> luas = new ArrayList<>();
									for (MstPengajuanAsuransiMobilPerluasan a : perluasan) {										

										DaftarPengajuanAsuransiBenefitDto luass = new DaftarPengajuanAsuransiBenefitDto();
										luass.setTitle(a.getTitle());
										luass.setAnswer("YES");
										luass.setAmount(a.getAmount());
										luas.add(luass);
									}
									objtemp.put("detail", mobilDto);
									objtemp.put("perluasan", luas);
								}else if (new Long(6).equals(item.getVariable1())){
									MstPengajuanMultigunaTanpaJaminan multigunaTanpaJaminan =
											detailPinjamanService.findById(item.getLeadsId());
									RespPengajuanDto respPengajuanDto = new RespPengajuanDto();
									respPengajuanDto.setPendapatan(multigunaTanpaJaminan.getPendapatan());
									respPengajuanDto.setJumlahPinjaman(multigunaTanpaJaminan.getJmlPinjaman());
									respPengajuanDto.setTenor(multigunaTanpaJaminan.getTenor());
									respPengajuanDto.setKebutuhanPinjaman(multigunaTanpaJaminan.getKebPinjaman());
									respPengajuanDto.setKebutuhanPinjamanLainnya(multigunaTanpaJaminan.getKebPinjamanLainnya());
									respPengajuanDto.setKartuKredit(multigunaTanpaJaminan.getKartuKredit());
									respPengajuanDto.setBankPenerbit(multigunaTanpaJaminan.getBankPenerbit());
									respPengajuanDto.setStatus(multigunaTanpaJaminan.getStatus());
									respPengajuanDto.setUserId(multigunaTanpaJaminan.getUserId());
									respPengajuanDto.setTanggalPengajuan(multigunaTanpaJaminan.getTanggalPengajuan().toString());

									MstDataDiriPengajuanMultiguna diriPengajuanMultiguna =
											detailPinjamanService.findDataDiri(multigunaTanpaJaminan.getId());
									respPengajuanDto.setDataDiri(diriPengajuanMultiguna);

									objtemp.put("detail", respPengajuanDto);
								}else if (new Long(7).equals(item.getVariable1())){
								Optional<MstPengajuanUmroh> pengajuanUmroh =
										pembiayaanUmrohService.findById(item.getLeadsId());
								RespPembiayaanUmrohDto pembiayaanUmrohDto = new RespPembiayaanUmrohDto();
								pembiayaanUmrohDto.setIdPengajuan(pengajuanUmroh.get().getIntegrationLeadsId());
								pembiayaanUmrohDto.setStatusPengajuan(pengajuanUmroh.get().getStatus());
								pembiayaanUmrohDto.setJenisLayanan("Pembiayaan Umroh");
								pembiayaanUmrohDto.setPartnerPembiayaan("AMITRA");
								pembiayaanUmrohDto.setProdukPembiayaan("Porsi Umroh Reguler");
								String dp = IdrFormatMoney.currencyIdr(pengajuanUmroh.get().getDp().toString());
								pembiayaanUmrohDto.setDp(dp);
								pembiayaanUmrohDto.setTenor(pengajuanUmroh.get().getTenor());

								RespPembiayaanUmrohDetailDto pembiayaanUmrohDetail = new RespPembiayaanUmrohDetailDto();
								RespPembiayaanUmrohDetaiDataDiriDto pembiayaanUmrohDetailDataDiri = new RespPembiayaanUmrohDetaiDataDiriDto();
								RespPembiayaanUmrohDetailAlamatDto pembiayaanUmrohDetailAlamat = new RespPembiayaanUmrohDetailAlamatDto();
								RespPembiayaanUmrohDetailAlamatKtpDto pembiayaanUmrohDetailAlamatKtpDto = null;
								pembiayaanUmrohDetail.setPartnerPembiayaan("AMITRA");
								pembiayaanUmrohDetail.setProdukPembiayaan("Porsi Umroh Reguler");
								String dpdetail = IdrFormatMoney.currencyIdr(pengajuanUmroh.get().getDp().toString());
								pembiayaanUmrohDetail.setDp(dpdetail);
								pembiayaanUmrohDetail.setTenor(pengajuanUmroh.get().getTenor());

								pembiayaanUmrohDetailDataDiri.setNamaDepan(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getNamaDepan());
								pembiayaanUmrohDetailDataDiri.setNamaBelakang(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getNamaBelakang());
								pembiayaanUmrohDetailDataDiri.setJenisKelamin(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getGender());
								pembiayaanUmrohDetailDataDiri.setTempatLahir(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getTempatLahir());
								pembiayaanUmrohDetailDataDiri.setTanggalLahir(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getTanggalLahir());
								pembiayaanUmrohDetailDataDiri.setNamaIbuKandung(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getIbuKandung());
								pembiayaanUmrohDetailDataDiri.setNoHp(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getNoHp());
								pembiayaanUmrohDetailDataDiri.setNoKtp(pengajuanUmroh.get().getMstPengajuanUmrohDataDiri().getNoKtp());

								pembiayaanUmrohDetailAlamat.setAlamat(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getAlamat());
								pembiayaanUmrohDetailAlamat.setProvinsi(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getProvinsi());
								pembiayaanUmrohDetailAlamat.setKota(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKota());
								pembiayaanUmrohDetailAlamat.setKecamatan(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKecamatan());
								pembiayaanUmrohDetailAlamat.setKelurahan(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKelurahan());
								pembiayaanUmrohDetailAlamat.setRt(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getRt());
								pembiayaanUmrohDetailAlamat.setRw(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getRw());
								pembiayaanUmrohDetailAlamat.setKodePos(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKodePos());


								if(!pembiayaanUmrohDetailAlamat.getAlamat().equalsIgnoreCase(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getAlamatKtp()) &&
								!pembiayaanUmrohDetailAlamat.getKodePos().equalsIgnoreCase(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKostpos_ktp())){
									pembiayaanUmrohDetailAlamatKtpDto = new RespPembiayaanUmrohDetailAlamatKtpDto();
									pembiayaanUmrohDetailAlamatKtpDto.setAlamatLengkap(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getAlamatKtp());
									pembiayaanUmrohDetailAlamatKtpDto.setKodePos(pengajuanUmroh.get().getMstPengajuanUmrohAlamat().getKostpos_ktp());
									pembiayaanUmrohDetail.setRespPembiayaanUmrohDetailAlamatKtpDto(pembiayaanUmrohDetailAlamatKtpDto);
								}


								pembiayaanUmrohDetail.setRespPembiayaanUmrohDetaiDataDiriDto(pembiayaanUmrohDetailDataDiri);
								pembiayaanUmrohDetail.setRespPembiayaanUmrohDetailAlamatDto(pembiayaanUmrohDetailAlamat);
								objtemp.put("detail", pembiayaanUmrohDetail);
							}
							
							objPengajuanList.add(objtemp);
						} 
						response.put("status", "success");
						response.put("description", "succesfully get data");
						response.put("data", objPengajuanList);
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						response.put("data", null);
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			} catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (NullPointerException np) {
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (RuntimeException re) {
				re.printStackTrace();
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}

		return new ResponseEntity<>(response, httpStatus);
	}
}
