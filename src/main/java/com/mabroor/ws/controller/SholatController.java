package com.mabroor.ws.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.SholatProvinceDto;
import com.mabroor.ws.dto.SholatSearchDto;
import com.mabroor.ws.dto.WaktuTerdekatDto;
import com.mabroor.ws.entity.MstSholat;
import com.mabroor.ws.service.SholatService;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/sholat")
public class SholatController {
	
	private static final Logger LOG = Logger.getLogger(SholatController.class);
	
	@Autowired
	private SholatService sholatServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",sholatServ.getListSholat());
		return response;
	}
	
	@PostMapping(value = "/searchJadwal")
	@ResponseBody
	public ResponseEntity homepage(@RequestBody SholatSearchDto search) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;
		Locale localeCurrency = new Locale("id", "ID");
//		SimpleDateFormat formatters = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS", localeCurrency);
		try {
//			try {
////				Date date = formatters.parse(formatter);
////				
////			} catch (ParseException e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
			System.out.println(search.getDate());		
			if(search.getLocation() != null && !search.getLocation().isEmpty() && search.getDate() != null && !search.getDate().equals("")) {
				List<MstSholat> mstLeadsHomepage = sholatServ.getLocationAndTime(search.getLocation(), search.getDate());	
				response.put("status","success");
				response.put("description","data found");
				response.put("data",mstLeadsHomepage);
				httpStatus = HttpStatus.OK;
			}else if(search.getLocation() == null || search.getLocation().isEmpty()) {
				List<MstSholat> listDate = sholatServ.getDate(search.getDate());
				response.put("status","success");
				response.put("description","data found");
				response.put("data",listDate);
				httpStatus = HttpStatus.OK;
			}else if(search.getDate() == null || search.getDate().equals("")) {
				List<MstSholat> listProv = sholatServ.getLocation(search.getLocation());
				response.put("status","success");
				response.put("description","data found");
				response.put("data",listProv);
				httpStatus = HttpStatus.OK;
			}  
			else {
				response.put("status","error");
				response.put("description","data not found");
				httpStatus = HttpStatus.OK;
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(response, httpStatus);
	
	}
	
	@GetMapping(value = "/province")
	@ResponseBody
	public LinkedHashMap<String, Object> getBrandMotor() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		List<MstSholat> sholat = sholatServ.findByLocation();
		List<SholatProvinceDto> province = new ArrayList<>();      
		for(MstSholat a : sholat) {
			SholatProvinceDto provinces = new SholatProvinceDto();
			provinces.setLocation(a.getLocation());
			province.add(provinces);
		}	
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", province);
		return response;
	}
	
	@GetMapping(value = "/waktuSholat/{location}")
	@ResponseBody
	public Map<String, Object> getWaktu(@PathVariable String location) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
//		Date dateLocal = null;
//		DateTime dateTime = null;
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd hh:mm", localeCurrency);
//		List<MstSholat> sholat = sholatServ.getListSholat();
//		for(MstSholat a : sholat) {			
////			Date jamSholat = formatter.parse(a.getTime());
//			date = a.getDate() + " " + a.getTime();
//			try {
//				dateLocal = formatter.parse(date);
//				dateTime = new DateTime(dateLocal);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		Calendar c1 = Calendar.getInstance();
//		c1.get(Calendar.YEAR);
//		c1.get(Calendar.MONTH);
//		c1.get(Calendar.DAY_OF_MONTH);
//		c1.get(Calendar.HOUR_OF_DAY);
//		c1.get(Calendar.MINUTE);
////		Calendar c2 = Calendar.getInstance();
////		c2.setTime(dateLocal);
//		LOG.info(c1);
//		if(c1.before(dateTime)) {
		  Locale localeCurrency = new Locale("id", "ID");
		  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", localeCurrency);
		    List<MstSholat> sholats = sholatServ.getWaktuSholat(location);		   
		    WaktuTerdekatDto waktu = new WaktuTerdekatDto();
		    waktu.setTime(sholats.get(0).getTime());
		    waktu.setJudul("Waktu "+sholats.get(0).getWaktuPengingat());
		    
		    long diffInMillies = Math.abs(sholats.get(0).getDate().getTime() - new Date().getTime());
//		    long diff1 = TimeUnit.HOURS.convert(diffInMillies, TimeUnit.MILLISECONDS);
//		    long diff2 = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
		    long diffsec = TimeUnit.MILLISECONDS.toSeconds(diffInMillies)% 60;
		    long diffmin = TimeUnit.MILLISECONDS.toMinutes(diffInMillies)% 60;
		    long diffhour = TimeUnit.MILLISECONDS.toHours(diffInMillies)% 24;
		    System.out.println("test1 " + diffmin + diffhour);
		    long diffhourwita = diffhour + 1;
		    long diffhourwit = diffhourwita + 1;
		    String formatted1 = String.format("%02d", diffhourwita);
		    String formatted2 = String.format("%02d", diffhourwit);
		    String formatted3 = String.format("%02d", diffmin);
		    String formatted4 = String.format("%02d", diffsec);
		    String formatted5 = String.format("%02d", diffhour);
//		    if(Long.compare(diffhour, 10) > 0) {
		    	String jam = formatted5 + " jam," + " " + formatted3 + " menit lagi menuju";
//		    	String jams = "Menuju Waktu " + sholats.get(0).getWaktuPengingat() + " " + sholats.get(0).getTime() +" WIB" + " (" +formatted5+":"+formatted3+":"+formatted4 + ")";
		    	waktu.setJam(jam);
				String time = sholats.get(0).getTime().replace(":", ".");
				waktu.setWording1("Menuju Waktu " + sholats.get(0).getWaktuPengingat() + " " + time  + " "+ sholats.get(0).getTimeZone().toLowerCase());
				waktu.setWording2("(-"+formatted5+":"+formatted3+":"+formatted4+")");
//		    }else {
//		    	String jam = "0"+ diffhour + " jam," + " " + diffmin + " menit lagi menuju";
//		    	waktu.setJam(jam);
//		    }		   
		  
			response.put("status", "success");
			response.put("description", "succesfully get data");
			response.put("data", waktu);
//			response.put("waktu terdekat", "Menuju Waktu " + sholats.get(0).getWaktuPengingat() + " " + sholats.get(0).getTime() +" WIB" + " (" +"-"+formatted5+":"+formatted3+":"+formatted4 + ")");
//			response.put("waktu WITA", "Menuju Waktu " + sholats.get(0).getWaktuPengingat() + " " + sholats.get(0).getTime() +" WITA" + " (" +formatted1+":"+formatted3+":"+formatted4 + ")");
//			response.put("waktu WIT", "Menuju Waktu " + sholats.get(0).getWaktuPengingat() + " " + sholats.get(0).getTime() +" WIT" + " (" +formatted2+":"+formatted3+":"+formatted4 + ")");
//		}
		
		return response;
	}

}
