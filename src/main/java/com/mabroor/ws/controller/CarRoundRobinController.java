package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.entity.MstPengajuanCar;
import com.mabroor.ws.service.CarRoundRobinService;

@RestController
@RequestMapping("/v1/api/carroundrobin")
public class CarRoundRobinController {
	
	@Autowired
	private CarRoundRobinService carRoundRobinServ;
	
	@GetMapping(value = "/check")
	@ResponseBody
	public Map<String, Object> checkLast() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		
		String company = "";
		MstPengajuanCar mobil = carRoundRobinServ.findAllByStatus();
		
		if("ACC".equals(mobil.getCompanyName())) {
			company = "TAF";
		}else {
			company = "ACC";
		}
			response.put("status", "success");
			response.put("description", "Success get round robin last submitted.");
			response.put("result", company);
			
			return response;
		}

}
