package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.DictionaryDto;
import com.mabroor.ws.dto.DictionaryReqBodyDto;
import com.mabroor.ws.service.ContentDictionarySettingService;
import com.mabroor.ws.service.DictionaryService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/dictionary")
public class DictionaryController {

	@Autowired
	private DictionaryService dictServ;

	@Autowired
	private ContentDictionarySettingService contService;

	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		try {
			DictionaryDto kamus = dictServ.findAllByAbjad();

			LinkedHashMap<String, Object> abjad = new LinkedHashMap<>();
//			if(!kamus.getA().isEmpty()) {
//				abjad.put("A", kamus.getA());
//			}
//			if(!kamus.getB().isEmpty()) {
//				abjad.put("B", kamus.getB());
//			}
//			if(!kamus.getC().isEmpty()) {
//				abjad.put("C", kamus.getC());
//			}
//			if(!kamus.getD().isEmpty()) {
//				abjad.put("D", kamus.getD());
//			}
//			if(!kamus.getE().isEmpty()) {
//				abjad.put("E", kamus.getE());
//			}
//			if(!kamus.getF().isEmpty()) {
//				abjad.put("F", kamus.getF());
//			}
//			if(!kamus.getG().isEmpty()) {
//				abjad.put("G", kamus.getG());
//			}
//			if(!kamus.getH().isEmpty()) {
//				abjad.put("H", kamus.getH());
//			}
//			if(!kamus.getI().isEmpty()) {
//				abjad.put("I", kamus.getI());
//			}
//			if(!kamus.getJ().isEmpty()) {
//				abjad.put("J", kamus.getJ());
//			}
//			if(!kamus.getK().isEmpty()) {
//				abjad.put("K", kamus.getK());
//			}
//			if(!kamus.getL().isEmpty()) {
//				abjad.put("L", kamus.getL());
//			}
//			if(!kamus.getM().isEmpty()) {
//				abjad.put("M", kamus.getM());
//			}
//			if(!kamus.getN().isEmpty()) {
//				abjad.put("N", kamus.getN());
//			}
//			if(!kamus.getO().isEmpty()) {
//				abjad.put("O", kamus.getO());
//			}
//			if(!kamus.getP().isEmpty()) {
//				abjad.put("P", kamus.getP());
//			}
//			if(!kamus.getQ().isEmpty()) {
//				abjad.put("Q", kamus.getQ());
//			}
//			if(!kamus.getR().isEmpty()) {
//				abjad.put("R", kamus.getR());
//			}
//			if(!kamus.getS().isEmpty()) {
//				abjad.put("S", kamus.getS());
//			}
//			if(!kamus.getT().isEmpty()) {
//				abjad.put("T", kamus.getT());
//			}
//			if(!kamus.getU().isEmpty()) {
//				abjad.put("U", kamus.getU());
//			}
//			if(!kamus.getV().isEmpty()) {
//				abjad.put("V", kamus.getV());
//			}
//			if(!kamus.getW().isEmpty()) {
//				abjad.put("W", kamus.getW());
//			}
//			if(!kamus.getX().isEmpty()) {
//				abjad.put("X", kamus.getX());
//			}
//			if(!kamus.getY().isEmpty()) {
//				abjad.put("Y", kamus.getY());
//			}
//			if(!kamus.getZ().isEmpty()) {
//				abjad.put("Z", kamus.getZ());
//			}

			abjad.put("A", kamus.getA());
			abjad.put("B", kamus.getB());
			abjad.put("C", kamus.getC());
			abjad.put("D", kamus.getD());
			abjad.put("E", kamus.getE());
			abjad.put("F", kamus.getF());
			abjad.put("G", kamus.getG());
			abjad.put("H", kamus.getH());
			abjad.put("I", kamus.getI());
			abjad.put("J", kamus.getJ());
			abjad.put("K", kamus.getK());
			abjad.put("L", kamus.getL());
			abjad.put("M", kamus.getM());
			abjad.put("N", kamus.getN());
			abjad.put("O", kamus.getO());
			abjad.put("P", kamus.getP());
			abjad.put("Q", kamus.getQ());
			abjad.put("R", kamus.getR());
			abjad.put("S", kamus.getS());
			abjad.put("T", kamus.getT());
			abjad.put("U", kamus.getU());
			abjad.put("V", kamus.getV());
			abjad.put("W", kamus.getW());
			abjad.put("X", kamus.getX());
			abjad.put("Y", kamus.getY());
			abjad.put("Z", kamus.getZ());

			if(kamus.getA().isEmpty() && kamus.getB().isEmpty() && kamus.getC().isEmpty() && kamus.getD().isEmpty() && kamus.getE().isEmpty() && kamus.getF().isEmpty() && kamus.getG().isEmpty() &&
					kamus.getH().isEmpty() && kamus.getI().isEmpty() && kamus.getJ().isEmpty() && kamus.getK().isEmpty() && kamus.getL().isEmpty() && kamus.getM().isEmpty() && kamus.getN().isEmpty() && kamus.getO().isEmpty() &&
					kamus.getP().isEmpty() && kamus.getQ().isEmpty() && kamus.getR().isEmpty() && kamus.getS().isEmpty() && kamus.getT().isEmpty() && kamus.getU().isEmpty() && kamus.getV().isEmpty() && kamus.getW().isEmpty() &&
					kamus.getX().isEmpty() && kamus.getY().isEmpty() && kamus.getZ().isEmpty()){
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Data is empty.");
			}else {
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
				response.put("data", abjad);
			}
		}catch (Exception e){
			response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, "failed get data");
		}
		return response;
	}

	@PostMapping(value = "/list/search")
	@ResponseBody
	public Map<String, Object> search(@RequestBody DictionaryReqBodyDto mstKamus) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		try {
			DictionaryDto kamus = dictServ.findAllBySearchAbjad(mstKamus.getTitle());

			LinkedHashMap<String, Object> abjad = new LinkedHashMap<>();
			if(!kamus.getA().isEmpty()) {
				abjad.put("A", kamus.getA());
			}
			if(!kamus.getB().isEmpty()) {
				abjad.put("B", kamus.getB());
			}
			if(!kamus.getC().isEmpty()) {
				abjad.put("C", kamus.getC());
			}
			if(!kamus.getD().isEmpty()) {
				abjad.put("D", kamus.getD());
			}
			if(!kamus.getE().isEmpty()) {
				abjad.put("E", kamus.getE());
			}
			if(!kamus.getF().isEmpty()) {
				abjad.put("F", kamus.getF());
			}
			if(!kamus.getG().isEmpty()) {
				abjad.put("G", kamus.getG());
			}
			if(!kamus.getH().isEmpty()) {
				abjad.put("H", kamus.getH());
			}
			if(!kamus.getI().isEmpty()) {
				abjad.put("I", kamus.getI());
			}
			if(!kamus.getJ().isEmpty()) {
				abjad.put("J", kamus.getJ());
			}
			if(!kamus.getK().isEmpty()) {
				abjad.put("K", kamus.getK());
			}
			if(!kamus.getL().isEmpty()) {
				abjad.put("L", kamus.getL());
			}
			if(!kamus.getM().isEmpty()) {
				abjad.put("M", kamus.getM());
			}
			if(!kamus.getN().isEmpty()) {
				abjad.put("N", kamus.getN());
			}
			if(!kamus.getO().isEmpty()) {
				abjad.put("O", kamus.getO());
			}
			if(!kamus.getP().isEmpty()) {
				abjad.put("P", kamus.getP());
			}
			if(!kamus.getQ().isEmpty()) {
				abjad.put("Q", kamus.getQ());
			}
			if(!kamus.getR().isEmpty()) {
				abjad.put("R", kamus.getR());
			}
			if(!kamus.getS().isEmpty()) {
				abjad.put("S", kamus.getS());
			}
			if(!kamus.getT().isEmpty()) {
				abjad.put("T", kamus.getT());
			}
			if(!kamus.getU().isEmpty()) {
				abjad.put("U", kamus.getU());
			}
			if(!kamus.getV().isEmpty()) {
				abjad.put("V", kamus.getV());
			}
			if(!kamus.getW().isEmpty()) {
				abjad.put("W", kamus.getW());
			}
			if(!kamus.getX().isEmpty()) {
				abjad.put("X", kamus.getX());
			}
			if(!kamus.getY().isEmpty()) {
				abjad.put("Y", kamus.getY());
			}
			if(!kamus.getZ().isEmpty()) {
				abjad.put("Z", kamus.getZ());
			}
			
			if(kamus.getA().isEmpty() && kamus.getB().isEmpty() && kamus.getC().isEmpty() && kamus.getD().isEmpty() && kamus.getE().isEmpty() && kamus.getF().isEmpty() && kamus.getG().isEmpty() &&
					kamus.getH().isEmpty() && kamus.getI().isEmpty() && kamus.getJ().isEmpty() && kamus.getK().isEmpty() && kamus.getL().isEmpty() && kamus.getM().isEmpty() && kamus.getN().isEmpty() && kamus.getO().isEmpty() &&
					kamus.getP().isEmpty() && kamus.getQ().isEmpty() && kamus.getR().isEmpty() && kamus.getS().isEmpty() && kamus.getT().isEmpty() && kamus.getU().isEmpty() && kamus.getV().isEmpty() && kamus.getW().isEmpty() &&
					kamus.getX().isEmpty() && kamus.getY().isEmpty() && kamus.getZ().isEmpty()){
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Data is empty.");
			}else {
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
				response.put("data", abjad);
			}
		}catch (Exception e){
			response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Data is empty.");
		}

		return response;
	}

	@GetMapping(value = "/homepage")
	@ResponseBody
	public Map<String, Object> getListHomepage() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data", contService.getList());
		return response;
	}
}
