package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.MotorBrandService;

@RestController
@RequestMapping("/v1/api/motor")
public class MotorBrandController {

	@Autowired
	private MotorBrandService brandService;
	
	@GetMapping(value ="/brand/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",brandService.getListMotorBrand());
		return response;
	}

	@GetMapping(value = "/brand/{brandId}")
	public Map<String, Object> getBrandById(@PathVariable Long brandId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",brandService.findById(brandId));
		return response;
	}
}
