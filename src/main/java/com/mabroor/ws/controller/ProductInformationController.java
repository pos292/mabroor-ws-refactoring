package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.ProductInformationDto;
import com.mabroor.ws.dto.ProductInformationBenefitDto;
import com.mabroor.ws.entity.MstProductInformation;
import com.mabroor.ws.entity.MstProductInformationBenefit;
import com.mabroor.ws.service.ProductInformationBenefitService;
import com.mabroor.ws.service.ProductInformationService;
import com.mabroor.ws.util.Constant;

@Transactional
@RestController
@RequestMapping("/v1/api/productInformation")
public class ProductInformationController {

	@Autowired
	private ProductInformationService productServ;

	@Autowired
	private ProductInformationBenefitService productBenefitServ;

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@GetMapping(value = "/detail/{idMainProduct}")
	@ResponseBody
	public Map<String, Object> getDetailById(@PathVariable Long idMainProduct) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		List <ProductInformationDto> productListDto = new ArrayList<>();
		List <MstProductInformation> productList = productServ.getListMainProduct(idMainProduct);

		for(MstProductInformation product : productList) {
			em.detach(product);

			ProductInformationDto productDto = new ProductInformationDto();
			productDto.setId(product.getId());
			productDto.setTitle(product.getTitle());
			productDto.setImage(this.imageBaseUrl + "productInformation/" + product.getImage());
			productDto.setLogoCompany(this.imageBaseUrl + "productInformation/" + product.getLogoCompany());
			productDto.setMainProduct(product.getMainProduct());
			productDto.setDescription(product.getDescription());
			productDto.setProductStatus(product.getProductStatus());

			List <MstProductInformationBenefit> productBenefit = productBenefitServ.getListbyProductInfo(product.getId());
			List <ProductInformationBenefitDto> productBenefitDto = new ArrayList<>();
			for(MstProductInformationBenefit productBen : productBenefit) {
				em.detach(productBen);
				productBen.setImage(this.imageBaseUrl + "productInformation/" + productBen.getImage());
				ProductInformationBenefitDto dto = new ProductInformationBenefitDto();
				dto.setId(product.getId());
				dto.setImage(productBen.getImage());
				dto.setDescription(productBen.getDescription());

				productBenefitDto.add(dto);
			}
			productDto.setProductBenefit(productBenefitDto);

			productListDto.add(productDto);
		}

		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", productListDto);
		return response;
	}
}