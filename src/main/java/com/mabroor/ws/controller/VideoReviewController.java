package com.mabroor.ws.controller;

import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.VideoReviewService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/videoreview")
public class VideoReviewController {

	@Autowired
	private VideoReviewService videoReviewService;


	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data",videoReviewService.findAllByStatus());
		return response;
	}

	@GetMapping(value = "/{videoId}")
	public Map<String, Object> getNewsById(@PathVariable Long videoId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data",videoReviewService.findById(videoId));
		return response;
	}

	@GetMapping(value = "/mainProduct/{idMainProduct}")
	public Map<String, Object> getMainProductId(@PathVariable Long idMainProduct) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data",videoReviewService.findByMainProductId(idMainProduct));
		return response;
	}
	
	@GetMapping(value = "/mainProduct/list/{idMainProduct}")
	public Map<String, Object> getMainProductIdList(@PathVariable Long idMainProduct) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data",videoReviewService.findByMainProductIdList(idMainProduct));
		return response;
	}

	@GetMapping(value = "/mainProduct/chips/landingpage")
	public Map<String, Object> getSubCategoryLandingpage() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", videoReviewService.findLandingpageChips());
		return response;
	}
	
	@GetMapping(value = "/mainProduct/chips/all")
	public Map<String, Object> getSubCategoryAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", videoReviewService.findAllChips());
		return response;
	}

	@GetMapping(value = "/mainProduct/landingpage/limit/{mainProduct}")
	public Map<String, Object> getMainProduckLimit(@PathVariable String mainProduct) {
		
		try {
			// DECODE TO UTF-8
			mainProduct = URLDecoder.decode(mainProduct, "UTF-8");
			
		} catch (Exception e) {
			//TODO: handle exception
			return null;
		}

		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", videoReviewService.findByMainProductNameLimit(mainProduct.trim()));
		return response;
	}

	@GetMapping(value = "/mainProduct/landingpage/all/{mainProduct}")
	public Map<String, Object> getAllMainProduck(@PathVariable String mainProduct) {

		try {
			// DECODE TO UTF-8
			mainProduct = URLDecoder.decode(mainProduct, "UTF-8");
			
		} catch (Exception e) {
			//TODO: handle exception
			return null;
		}

		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", videoReviewService.findByMainProductName(mainProduct.trim()));
		return response;
	}

	@GetMapping(value = "/mainProduct/{idMainProduct}/{subcategoryName}")
	public Map<String, Object> getByMainProductSubCategory(@PathVariable Long idMainProduct, @PathVariable String subcategoryName) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data", videoReviewService.findByMainProductIdandSubCategoryName(idMainProduct, subcategoryName));
		return response;
	}

}
