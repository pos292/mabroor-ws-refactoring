package com.mabroor.ws.controller;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.mabroor.ws.dto.ZakatDto;
import com.mabroor.ws.util.Constant;


@RestController
@RequestMapping("/v1/api/zakat")
public class ZakatController {
	
	private static final Logger LOG = Logger.getLogger(ZakatController.class);

	@PostMapping(value = "/penghasilan")
	@ResponseBody
	public ResponseEntity save(@RequestBody ZakatDto mstZakat) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;
		
		if(mstZakat.getCicilanKebutuhanPokok() == null || mstZakat.getCicilanKebutuhanPokok().equalsIgnoreCase("") ) {
			mstZakat.setCicilanKebutuhanPokok("0");
		}
		if(mstZakat.getTunjanganPerBulan() == null || mstZakat.getTunjanganPerBulan().equalsIgnoreCase("") ) {
			mstZakat.setTunjanganPerBulan("0");
		}
		Locale localeCurrency = new Locale("id", "ID");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(localeCurrency);
		
		BigDecimal a = new BigDecimal(mstZakat.getPendapatanPerBulan());
		BigDecimal b = new BigDecimal(mstZakat.getTunjanganPerBulan());
		BigDecimal c = new BigDecimal(mstZakat.getHargaBeras());
		BigDecimal d = new BigDecimal(mstZakat.getCicilanKebutuhanPokok());
		
		BigDecimal total1 = a.add(b).subtract(d);
		BigDecimal total2 = c.multiply(new BigDecimal(522));
		LOG.info(total1);
		LOG.info(total2);
		if(total1.compareTo(total2)>0) {
			
			BigDecimal zakatBulan = total1.divide(new BigDecimal(40)) ;
			String bulan = fmt.format(zakatBulan).replace(",00", "");
			Double zakats = Math.rint(zakatBulan.doubleValue());
			DecimalFormat format = new DecimalFormat("0.#");
			response.put("status", "success");
			response.put("description", "Total Zakat Penghasilan yang kamu harus bayarkan setiap bulan sebesar : Rp" + format.format(zakats));
			response.put("zakatBulanan", bulan);
			httpStatus = HttpStatus.OK;
		}else {
			response.put("status", "failed");
			response.put("description", "Kamu belum wajib membayar Zakat");
			httpStatus = HttpStatus.OK;
		}
		
		return new ResponseEntity<>(response, httpStatus);
	}
	
	@PostMapping(value = "/maal")
	@ResponseBody
	public ResponseEntity saveMall(@RequestBody ZakatDto mstZakat) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;
		
		
		
		if(mstZakat.getUangTunai() == null || mstZakat.getUangTunai().equalsIgnoreCase("") ) {
			mstZakat.setUangTunai("0");
		}
		if(mstZakat.getSaham() == null || mstZakat.getSaham().equalsIgnoreCase("") ) {
			mstZakat.setSaham("0");
		}
		if(mstZakat.getRealEstate() == null || mstZakat.getRealEstate().equalsIgnoreCase("") ) {
			mstZakat.setRealEstate("0");
		}
		if(mstZakat.getEmas() == null || mstZakat.getEmas().equalsIgnoreCase("")) {
			mstZakat.setEmas("0");
		}
		if(mstZakat.getMobil() == null || mstZakat.getMobil().equalsIgnoreCase("")) {
			mstZakat.setMobil("0");
		}
		if(mstZakat.getHutangPribadi() == null || mstZakat.getHutangPribadi().equalsIgnoreCase("")) {
			mstZakat.setHutangPribadi("0");
		}
		
		BigDecimal am = new BigDecimal(mstZakat.getUangTunai());
		BigDecimal bm = new BigDecimal(mstZakat.getSaham());
		BigDecimal cm = new BigDecimal(mstZakat.getRealEstate());
		BigDecimal dm = new BigDecimal(mstZakat.getEmas());
		BigDecimal em = new BigDecimal(mstZakat.getMobil());
		BigDecimal fm = new BigDecimal(mstZakat.getHutangPribadi());
		BigDecimal emasnow = new BigDecimal(mstZakat.getHargaEmas());
		BigDecimal zero = BigDecimal.ZERO;
		
		Gson gson = new Gson();
		String json = gson.toJson(mstZakat);
		LOG.info(json);
		BigDecimal simpanMall = am.add(bm).add(cm).add(em);
		BigDecimal hartaBersih = simpanMall.subtract(fm);
		LOG.info(simpanMall);
		LOG.info(hartaBersih);
		
		BigDecimal nisabMall = emasnow.multiply(new BigDecimal(85));
		BigDecimal zakatMall = new BigDecimal(1).divide(new BigDecimal(40)).multiply(hartaBersih);
		Double zakats = Math.rint(zakatMall.doubleValue());
		BigDecimal zakatss = new BigDecimal(zakats);
		LOG.info(nisabMall);
		LOG.info(zakatMall);
		
		if(am.compareTo(new BigDecimal(0)) == 0) {
			response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Kamu Belum Wajib Membayar Zakat");
			httpStatus = HttpStatus.OK;
		}
		else if(hartaBersih.compareTo(nisabMall) >0) {
			response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Total Zakat Maal yang kamu harus bayarkan sebesar : Rp." + zakatss.stripTrailingZeros().toPlainString());
			httpStatus = HttpStatus.OK;
		}
		else {
			response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Kamu Belum Wajib Membayar Zakat");
			httpStatus = HttpStatus.OK;
		}
		
		return new ResponseEntity<>(response, httpStatus);
	}
}
