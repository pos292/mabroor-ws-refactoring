package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.NeedsTanpaJaminanService;

@RestController
@RequestMapping("/v1/api/needs/tanpajaminan")
public class NeedsTanpaJaminanController {

	@Autowired
	private NeedsTanpaJaminanService needTanpaJaminanServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",needTanpaJaminanServ.getListNeedsTanpaJaminan());
		return response;
	}
	
	@GetMapping(value = "/{needsTanpaJaminanId}")
	public LinkedHashMap<String, Object> getNeedsTanpaJaminanById(@PathVariable Long needsTanpaJaminanId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",needTanpaJaminanServ.findById(needsTanpaJaminanId));
		return response;
	}
}
