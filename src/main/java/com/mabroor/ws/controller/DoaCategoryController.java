package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.InsuranceDto;
import com.mabroor.ws.dto.MstDoaCategoryDto;
import com.mabroor.ws.entity.MstDoaCategory;
import com.mabroor.ws.service.DoaCategoryService;

@RestController
@RequestMapping("/v1/api/doaCategory")
public class DoaCategoryController {

	private static final Logger LOG = Logger.getLogger(DoaCategoryController.class);
	
	@Autowired
	private DoaCategoryService categoryServ;
	
	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getListDoaCategory() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();

		List<MstDoaCategory> doaList = categoryServ.getList();
		List<MstDoaCategoryDto> categoryListDto = new ArrayList<>();
		
		for(MstDoaCategory doa : doaList) {
			MstDoaCategoryDto dto = new MstDoaCategoryDto();
			dto.setId(doa.getId());
			dto.setCategoryName(doa.getCategoryName());
			dto.setIcon(this.imageBaseUrl + "doa/" + doa.getIcon());
			dto.setCountDoa(doa.getCountDoa() + " bagian");
			categoryListDto.add(dto);
		}
		
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", categoryListDto);
		return response;
	}
}
