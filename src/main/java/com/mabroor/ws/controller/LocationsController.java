package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.LocationsService;

@RestController
@RequestMapping("/v1/api/location")
public class LocationsController {

	@Autowired
	private LocationsService locationService;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",locationService.findAllByStatus());
		return response;
	}
	
	@GetMapping(value = "/bookingtestdrive/province")
	@ResponseBody
	public Map<String, Object> getListProvince() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",locationService.getListBtdAvailable());
		return response;
	}
}
