package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.entity.MstDp;
import com.mabroor.ws.service.DpService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/dp")
public class DpController {

	@Autowired
	private DpService dpServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",dpServ.getListDp());
		return response;
	}
	
	@GetMapping(value = "/mobil")
	public Map<String, Object> getCarDp(@RequestParam(value = "param1", required = true, defaultValue = "") String param1) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		
		List<MstDp> dpBaru = new ArrayList<MstDp>();
		List<MstDp> dps = dpServ.getListCarDp();
		
		if("bestdeal".equalsIgnoreCase(param1)) {
			MstDp dp = new MstDp();	
			dp.setDp("1");
			dpBaru.add(dp);
		}
		
		for(MstDp dpp : dps) {
			dpBaru.add(dpp);
		}
		
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",dpBaru);
		return response;
	}
	
	@GetMapping(value = "/motor")
	public Map<String, Object> getMotorDp() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",dpServ.getListMotorDp());
		return response;
	}

}
