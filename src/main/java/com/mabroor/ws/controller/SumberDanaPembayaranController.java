package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.SumberDanaPembayaranService;

@RestController
@RequestMapping("/v1/api/sumberdana")
public class SumberDanaPembayaranController {

	@Autowired
	private SumberDanaPembayaranService danaServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",danaServ.getListDanaPembayaran());
		return response;
	}
	
	@GetMapping(value = "/{sumberDanaId}")
	public LinkedHashMap<String, Object> getSumberDanaPembayaranById(@PathVariable Long sumberDanaId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",danaServ.findById(sumberDanaId));
		return response;
	}
}
