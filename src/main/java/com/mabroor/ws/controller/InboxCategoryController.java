package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Optional;

import javax.crypto.IllegalBlockSizeException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.service.InboxCategoryService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/inboxcategory")
public class InboxCategoryController {

	private static final Logger LOG = Logger.getLogger(InboxCategoryController.class);

	@Autowired
	private InboxCategoryService inboxCategoryServ;
	
	@Autowired
	private UserMobileService userMobileServ;

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public ResponseEntity getInboxCategory(@RequestHeader(value = "appToken", required = false) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = HttpStatus.OK;
		if(token != null && !"".equals(token)) {
			try {
				Claims claims = JWTGenerator.getInstance().decodeJWT(token);
				if (Objects.isNull(claims)) {
					response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
					response.put(Constant.RESPONSE_DESCRIPTION_KEY, "silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
					MstUserMobile mstUserMobile = null;
					if (!mstUserMobileOpt.isPresent()) {
						response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
						response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}else {
						mstUserMobile = mstUserMobileOpt.get();
						if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){
							response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
							response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
							response.put("data",inboxCategoryServ.getInboxCategoryAfterLogin());
						}
					}
				}
			}
			catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch(IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (NullPointerException np) {
				np.printStackTrace();
				LOG.info(np.getMessage());
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY,
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY,
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (RuntimeException re) {
				LOG.info(re.getMessage());
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY,
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_FAILED_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}else {
			response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
			response.put("data",inboxCategoryServ.getInboxCategoryBeforeLogin());
		}
		
		return new ResponseEntity<>(response, httpStatus);
	}
}
