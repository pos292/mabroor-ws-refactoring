package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.TentangKamiService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/aboutus")
public class TentangKamiController {

	
	@Autowired
	private TentangKamiService tentangKamiService;
	
	@GetMapping
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data",tentangKamiService.findById(new Long(1)).get());
		return response;
	}
}
