package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.MotorCategoryService;

@RestController
@RequestMapping("/v1/api/motor")
public class MotorCategoryController {

	@Autowired
	private MotorCategoryService categoryService;
	
	@GetMapping(value = "/category/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",categoryService.getListMotorCategory());
		return response;
	}
	
	@GetMapping(value = "/category/{ctId}")
	public Map<String, Object> getCategoryById(@PathVariable Long ctId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",categoryService.findById(ctId));
		return response;
}
}
