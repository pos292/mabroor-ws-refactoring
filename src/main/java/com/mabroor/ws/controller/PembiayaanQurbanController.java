package com.mabroor.ws.controller;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.mabroor.ws.dto.CalculateTotalHargaQurbanDto;
import com.mabroor.ws.dto.ZakatDto;
import com.mabroor.ws.entity.MstPembiayaanQurban;
import com.mabroor.ws.service.PembiayaanQurbanService;

@RestController
@RequestMapping("/v1/api/qurban")
public class PembiayaanQurbanController {
	
	private static final Logger LOG = Logger.getLogger(PembiayaanQurbanController.class);
	
	@Autowired
	private PembiayaanQurbanService qurbanServ;

	@GetMapping(value = "/list/{namePartner}")
	@ResponseBody
	public Map<String, Object> getListDoaByIdCategory(@PathVariable String namePartner) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		
		List<MstPembiayaanQurban> qurban = new ArrayList<MstPembiayaanQurban>();
		
		if(namePartner != null && "all".equalsIgnoreCase(namePartner)) {
			qurban = qurbanServ.getList();
		}else {
//			doa = doaServ.listDoaByIdCategory(idCategory, "%"+namaDoa+"%", limit, (limit * page));
			qurban = qurbanServ.getListByNamePartner(namePartner);
		}
		

		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", qurban);
		return response;
	}
	
	@PostMapping(value = "/calculateQurban")
	@ResponseBody
	public ResponseEntity hitungqurban(@RequestBody CalculateTotalHargaQurbanDto hitungQurban) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;
		
		BigDecimal am = new BigDecimal(hitungQurban.getHargaHewan());
		BigDecimal bm = new BigDecimal(hitungQurban.getJumlahHewan());
		
		Gson gson = new Gson();
		String json = gson.toJson(hitungQurban);
		LOG.info(json);
		BigDecimal totalHarga = am.multiply(bm);
		LOG.info(totalHarga);
		
		
		Double qurbans = Math.rint(totalHarga.doubleValue());
		DecimalFormat format = new DecimalFormat("0.#");
		response.put("status", "success");
		response.put("description", "Total harga hewan sebesar : Rp" + format.format(qurbans));
		httpStatus = HttpStatus.OK;
	
	
	return new ResponseEntity<>(response, httpStatus);
}
}
