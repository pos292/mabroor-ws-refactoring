package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import com.mabroor.ws.dto.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.util.GeneratePdf;

@RestController
@RequestMapping("/v1/api/generatepdf")
public class GeneratePdfController {

	@PostMapping(value = "/mobil")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfMobil(@RequestBody GeneratePdfMobilReqDto pdfMobil) {
		String tempPdf = GeneratePdf.generatePdfMobil(pdfMobil);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/mobil/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfMobilDetail(@RequestBody GeneratePdfMobilReqDto pdfMobil) {
		String tempPdf = GeneratePdf.generatePdfMobilDetail(pdfMobil);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
//	@PostMapping(value = "/mobil/bekas")
//	@ResponseBody
//	public LinkedHashMap<String, Object> pdfMobilBekas(@RequestBody GeneratePdfMobilReqDto pdfMobil) {
//		String tempPdf = GeneratePdf.generatePdfMobilBekas(pdfMobil);
//		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
//		response.put("status", "success");
//		response.put("description", "succesfully generate pdf");
//		response.put("data", tempPdf);
//		return response;
//	}
	
	@PostMapping(value = "/motor")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfMotor(@RequestBody GeneratePdfMotorReqDto pdfMotor) {
		String tempPdf = GeneratePdf.generatePdfMotor(pdfMotor);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/motor/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfMotordetail(@RequestBody GeneratePdfMotorReqDto pdfMotor) {
		String tempPdf = GeneratePdf.generatePdfMotorDetail(pdfMotor);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/motor/v2")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfMotorV2(@RequestBody GeneratePdfMotorV2ReqDto pdfMotor) {
		String tempPdf = GeneratePdf.generatePdfMotorV2(pdfMotor);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/motor/detail/v2")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfMotordetailV2(@RequestBody GeneratePdfMotorV2ReqDto pdfMotor) {
		String tempPdf = GeneratePdf.generatePdfMotorDetailV2(pdfMotor);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/pinjamanTunai")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfJaminan(@RequestBody GeneratePdfJaminanReqDto pdfJaminan) {
		String tempPdf = GeneratePdf.generatePdfJaminan(pdfJaminan);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/pinjamanTunai/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfJaminanDetail(@RequestBody GeneratePdfJaminanReqDto pdfJaminan) {
		String tempPdf = GeneratePdf.generatePdfJaminanDetail(pdfJaminan);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/tanpaJaminan")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfTanpaJaminan(@RequestBody GeneratePdfTanpaJaminanReqDto pdfTanpaJaminan) {
		String tempPdf = GeneratePdf.generatePdfTanpaJaminan(pdfTanpaJaminan);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/asuransiJiwa")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAsuransiJiwa(@RequestBody GeneratePdfAsuransiJiwaReqDto pdfJiwa) {
		String tempPdf = GeneratePdf.generatePdfAsuransiJiwa(pdfJiwa);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/penyakitKritis")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfPenyakitKritis(@RequestBody GeneratePdfPenyakitKritisReqDto pdfKritis) {
		String tempPdf = GeneratePdf.generatePdfPenyakitKritis(pdfKritis);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/wisataReligiUmroh")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfWisataReligiUmroh(@RequestBody GeneratePdfWisataReligiReqDto pdfWisata) {
		String tempPdf = GeneratePdf.generatePdfWisataReligiUmroh(pdfWisata);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/wisataReligiHaji")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfWisataReligiHaji(@RequestBody GeneratePdfWisataReligiReqDto pdfWisata) {
		String tempPdf = GeneratePdf.generatePdfWisataReligiHaji(pdfWisata);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}

	@PostMapping(value = "/wisataReligiHaji/acc")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfWisataReligiHajiAcc(@RequestBody GeneratePdfWisataReligiReqDto pdfWisata) {
		String tempPdf = GeneratePdf.generatePdfWisataReligiHajiAcc(pdfWisata);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}

	@PostMapping(value = "/wisataReligiHaji/acc/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfWisataReligiHajiAccDetail(@RequestBody GeneratePdfWisataReligiReqDto pdfWisata) {
		String tempPdf = GeneratePdf.generatePdfWisataReligiHajiAccDetail(pdfWisata);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/asuransiMobil")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAsuransiMobil(@RequestBody GeneratePdfAsuransiMobilReqDto pdfAsuransiMobil) {
		String tempPdf = GeneratePdf.generatePdfAsuransiMobil(pdfAsuransiMobil);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/asuransiMobil/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAsuransiMobilDetail(@RequestBody GeneratePdfAsuransiMobilReqDto pdfAsuransiMobil) {
		String tempPdf = GeneratePdf.generatePdfAsuransiMobilDetail(pdfAsuransiMobil);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/pengajuanAlatBerat")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfPengajuanAlatBerat(@RequestBody GeneratePdfPengajuanAlatBeratReqDto pdfAlatBerat) {
		String tempPdf = GeneratePdf.generatePdfPengajuanAlatBerat(pdfAlatBerat);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/pengajuanAlatBerat/bekas")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfPengajuanAlatBeratBekas(@RequestBody GeneratePdfPengajuanAlatBeratBekasReqDto pdfAlatBeratBekas) {
		String tempPdf = GeneratePdf.generatePdfPengajuanAlatBeratBekas(pdfAlatBeratBekas);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/asuransiKebakaran")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAsuransiKebakaran(@RequestBody GeneratePdfAsuransiKebakaranReqDto pdfAsuransiKebakaran) {
		String tempPdf = GeneratePdf.generatePdfAsuransiKebakaran(pdfAsuransiKebakaran);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/asuransiKebakaran/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAsuransiKebakaranDetail(@RequestBody GeneratePdfAsuransiKebakaranReqDto pdfAsuransiKebakaran) {
		String tempPdf = GeneratePdf.generatePdfAsuransiKebakaranDetail(pdfAsuransiKebakaran);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/rentcar")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfSewaMobil(@RequestBody GeneratePdfRentCarDto pdfRentCar) {
		String tempPdf = GeneratePdf.generatePdfRentCar(pdfRentCar);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/rentcar/denganDriver")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfSewaMobilDenganDriver(@RequestBody GeneratePdfRentCarDto pdfRentCar) {
		String tempPdf = GeneratePdf.generatePdfRentCarDenganDriver(pdfRentCar);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/rentcar/denganDriver/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfSewaMobilDenganDriverDetail(@RequestBody GeneratePdfRentCarDto pdfRentCar) {
		String tempPdf = GeneratePdf.generatePdfRentCarDenganDriverDetail(pdfRentCar);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/rentbus")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfSewaBus(@RequestBody GeneratePdfRentBusDto pdfRentBus) {
		String tempPdf = GeneratePdf.generatePdfRentBus(pdfRentBus);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/asuransiKecelakaan")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAsuransiKecelakaan(@RequestBody GeneratePdfAsuransiKecelakaanReqDto pdfAsuransiKecelakaan) {
		String tempPdf = GeneratePdf.generatePdfAsuransiKecelakaan(pdfAsuransiKecelakaan);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/asuransiKecelakaan/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAsuransiKecelekaanDetail(@RequestBody GeneratePdfAsuransiKecelakaanReqDto pdfAsuransiKecelakaan) {
		String tempPdf = GeneratePdf.generatePdfAsuransiKecelakaanDetail(pdfAsuransiKecelakaan);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/airportTransfer")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAirportTransfer(@RequestBody GeneratePdfAirportTransferReqDto pdfAirportTransfer) {
		String tempPdf = GeneratePdf.generatePdfAirportTransfer(pdfAirportTransfer);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/gardaEdu")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfGardaEdu(@RequestBody GeneratePdfGardaEduReqDto pdfGardaEduMikro) {
		String tempPdf = GeneratePdf.generatePdfGardaEdu(pdfGardaEduMikro);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/gardaEdu/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfGardaEduDetail(@RequestBody GeneratePdfGardaEduReqDto pdfGardaEduMikro) {
		String tempPdf = GeneratePdf.generatePdfGardaEduDetail(pdfGardaEduMikro);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/gardaMe")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfGardaMe(@RequestBody GeneratePdfGardaMeReqDto pdfGardaMe) {
		String tempPdf = GeneratePdf.generatePdfGardaMe(pdfGardaMe);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/gardaMe/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfGardaMeDetail(@RequestBody GeneratePdfGardaMeReqDto pdfGardaMe) {
		String tempPdf = GeneratePdf.generatePdfGardaMeDetail(pdfGardaMe);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/gardaEduMikro")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfGardaEduMikro(@RequestBody GeneratePdfGardaEduMikroReqDto pdfGardaEduMikro) {
		String tempPdf = GeneratePdf.generatePdfGardaEduMikro(pdfGardaEduMikro);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/gardaEduMikro/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfGardaEduMikroDetail(@RequestBody GeneratePdfGardaEduMikroReqDto pdfGardaEduMikro) {
		String tempPdf = GeneratePdf.generatePdfGardaEduMikroDetail(pdfGardaEduMikro);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/gardaMeMikro")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfGardaMeMikro(@RequestBody GeneratePdfGardaMeMikroReqDto pdfGardaMeMikro) {
		String tempPdf = GeneratePdf.generatePdfGardaMeMikro(pdfGardaMeMikro);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/gardaMeMikro/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfGardaMeMikroDetail(@RequestBody GeneratePdfGardaMeMikroReqDto pdfGardaMeMikro) {
		String tempPdf = GeneratePdf.generatePdfGardaMeMikroDetail(pdfGardaMeMikro);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	@PostMapping(value = "/healthbuddies")
	@ResponseBody
	public LinkedHashMap<String, Object> generatePdfAsuransiHealthBuddies(@RequestBody GeneratePdfAsuransiHealthBuddiesDto pdfHealthBuddies) {
		String tempPdf = GeneratePdf.generatePdfAsuransiHealthBuddies(pdfHealthBuddies);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/healthbuddies/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> generatePdfAsuransiHealthBuddiesDetail(@RequestBody GeneratePdfAsuransiHealthBuddiesDto pdfHealthBuddies) {
		String tempPdf = GeneratePdf.generatePdfAsuransiHealthBuddiesDetail(pdfHealthBuddies);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	@PostMapping(value = "/flexihealth")
	@ResponseBody
	public LinkedHashMap<String, Object> generatePdfAsuransiFlexiHealth(@RequestBody GeneratePdfAsuransiFlexiHealthDto pdfFlexi) {
		String tempPdf = GeneratePdf.generatePdfAsuransiFlexiHealth(pdfFlexi);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/flexihealth/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> generatePdfAsuransiFlexiHealthDetail(@RequestBody GeneratePdfAsuransiFlexiHealthDto pdfFlexi) {
		String tempPdf = GeneratePdf.generatePdfAsuransiFlexiHealthDetail(pdfFlexi);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/sport")
	@ResponseBody
	public LinkedHashMap<String, Object> generatePdfAsuransiSport(@RequestBody GeneratePdfAsuransiSportDto pdfSport) {
		String tempPdf = GeneratePdf.generatePdfAsuransiSport(pdfSport);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/sport/detail")
	@ResponseBody
	public LinkedHashMap<String, Object> generatePdfAsuransiSportDetail(@RequestBody GeneratePdfAsuransiSportDto pdfSport) {
		String tempPdf = GeneratePdf.generatePdfAsuransiSportDetail(pdfSport);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/asuransiCovid")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfAsuransiCovid(@RequestBody GeneratePdfAsuransiCovidReqDto pdfCovid) {
		String tempPdf = GeneratePdf.generatePdfAsuransiCovid(pdfCovid);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
	
	@PostMapping(value = "/kurban")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfKurban(@RequestBody GeneratePdfKurbanReqDto pdfKurban) {
		String tempPdf = GeneratePdf.generatePdfKurbanRingkasan(pdfKurban);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}

	@PostMapping(value = "/detailringkasanPengajuan")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfDetailRingkasanPengajuan(@RequestBody GeneratePdfDetailRingkasanPengajuanReqDto pdfDetailRingkasanPengajuanReqDto) {
		String tempPdf = GeneratePdf.generatePdfDetailRingkasanPengajuan(pdfDetailRingkasanPengajuanReqDto);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}

	@PostMapping(value = "/ringkasanPengajuan")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfRingkasanPengajuan(@RequestBody GeneratePdfRingkasanPengajuanReqDto pdfRingkasanPengajuanReqDto) {
		String tempPdf = GeneratePdf.generatePdfRingkasanPengajuan(pdfRingkasanPengajuanReqDto);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}

	@PostMapping(value = "/ringkasanPembiayaanUmroh")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfRingkasanPembiayaanUmroh(@RequestBody GeneratePdfRingkasanPembiayaanUmrohDto pdfRingkasanPembiayaanUmrohDto) {
		String tempPdf = GeneratePdf.generateRingkasanPembiayaanUmroh(pdfRingkasanPembiayaanUmrohDto);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}

	@PostMapping(value = "/detailRingkasanPembiayaanUmroh")
	@ResponseBody
	public LinkedHashMap<String, Object> pdfDetailRingkasanPembiayaanUmroh(@RequestBody GeneratePdfDetailRingkasanPembiayaanUmrohDto pdfDetailRingkasanPembiayaanUmrohDto) {
		String tempPdf = GeneratePdf.generatePdfDetailRingkasanPembiayaanUmroh(pdfDetailRingkasanPembiayaanUmrohDto);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}
}
