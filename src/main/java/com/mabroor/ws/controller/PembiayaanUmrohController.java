package com.mabroor.ws.controller;

import com.mabroor.ws.dto.PembiayaanUmrohDto;
import com.mabroor.ws.dto.PembiayaanUmrohRespResultDto;
import com.mabroor.ws.entity.MstPengajuanUmroh;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.repository.PembiayaanUmrohRepository;
import com.mabroor.ws.entity.*;
import com.mabroor.ws.service.LogApiService;
import com.mabroor.ws.service.PembiayaanUmrohService;
import com.mabroor.ws.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;


@RestController
@RequestMapping("/v1/api/pembiayaan/umroh")
public class PembiayaanUmrohController {

    @Autowired
    private PembiayaanUmrohService pembiayaanUmrohService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private PembiayaanUmrohRepository pembiayaanUmrohRepository;

    @Autowired
    private LogApiService logApiService;

    @PostMapping()
    public ResponseEntity<PembiayaanUmrohRespResultDto> save(@RequestHeader(value = "appToken", required = true) String token,
                                                             @RequestBody @Valid PembiayaanUmrohDto pembiayaanUmrohDto,
                                                             HttpServletRequest request) throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException, IOException {

        MstUserMobile mstUserMobile = validationService.userLoginValid(token);
       MstPengajuanUmroh mstPengajuanUmroh = pembiayaanUmrohService.saveUmroh(pembiayaanUmrohDto, mstUserMobile);

        PembiayaanUmrohRespResultDto resp = new PembiayaanUmrohRespResultDto();
        resp.setReqid(UUID.randomUUID().toString());
        resp.setError("False");
        resp.setStatus(String.valueOf(HttpStatus.CREATED.value()));
        resp.setMessage("Pembiayaan Umroh Berhasil");


        MstLogApi mstLogApi = logApiService.saveUmrohLog(request, pembiayaanUmrohDto, resp, mstUserMobile);

        return new ResponseEntity<>(resp, HttpStatus.CREATED);
    }




}
