package com.mabroor.ws.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.mabroor.ws.dto.AlQuranSearchDto;
import com.mabroor.ws.dto.AlQuransResponseDto;
import com.mabroor.ws.dto.AlquranDto;
import com.mabroor.ws.dto.AlquranJuzDto;
import com.mabroor.ws.dto.AlquranSurahDto;
import com.mabroor.ws.entity.MstAlQuran;
import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.repository.AlQuranRepository;
import com.mabroor.ws.service.AlQuranService;
import com.mabroor.ws.util.DateComparator;

@RestController
@RequestMapping("/v1/api/alquran")
public class AlquranController {

	private static final Logger LOG = Logger.getLogger(AlquranController.class);

	@Value("${image.base.url}")
	private String quranUrl;

	@Autowired
	private AlQuranService quranServ;

	@GetMapping(value = "/listSurah")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		Gson gson = new Gson(); 
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<List<AlquranSurahDto>> response1 = null;
		ParameterizedTypeReference<List<AlquranSurahDto>> type = new ParameterizedTypeReference<List<AlquranSurahDto>>() {
		};
		response1 = restTemplate.exchange(quranUrl + "al-quran/tbl_surah.json", HttpMethod.GET, entity, type);
		List<AlquranSurahDto> surahs = new ArrayList<>();
		for(AlquranSurahDto a : response1.getBody()) {
			AlquranSurahDto surah = new AlquranSurahDto();
			String ind = a.getInd().toLowerCase();
			StringBuilder kalimatBaru = new StringBuilder();
			for(String b : ind.split(" ")) {
				String output = b.substring(0, 1).toUpperCase() + b.substring(1);
				kalimatBaru.append(output).append(" ");
				LOG.info(output);
			}			
			surah.setPhonetic_ind(a.getPhonetic_ind());
			surah.set_id(a.get_id());
			surah.setAr(a.getAr());
			surah.setCount(a.getCount());
			surah.setEn(a.getEn());
			surah.setFirst_page(a.getFirst_page());
			surah.setInd(kalimatBaru.toString());
			surah.setPhonetic(a.getPhonetic());
			surah.setSura(a.getSura());
			surahs.add(surah);
		}
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", surahs);
		return response;

	}

	//	@PostMapping(value = "/searchSurah")
	//	@ResponseBody
	//	public ResponseEntity searchSurah(@RequestBody AlquranSurahDto mstQuran) {
	//		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	//		HttpStatus httpStatus = null;
	//
	//		RestTemplate restTemplate = new RestTemplate();
	//		HttpHeaders headers = new HttpHeaders();
	//
	//		HttpEntity<String> entity = new HttpEntity<>(headers);
	//		ResponseEntity<List<AlquranSurahDto>> response1 = null;
	//		ParameterizedTypeReference<List<AlquranSurahDto>> type = new ParameterizedTypeReference<List<AlquranSurahDto>>() {
	//		};
	//
	//		response1 = restTemplate.exchange(quranUrl + "al-quran/tbl_surah.json", HttpMethod.GET, entity, type);
	//		LOG.info(response1.getBody().get(0).get_id());
	//
	//		for (AlquranSurahDto a : response1.getBody()) {
	//			if (mstQuran.getPhonetic_ind().equalsIgnoreCase(a.getPhonetic_ind()) && mstQuran.getInd().equalsIgnoreCase(a.getInd())) {
	//				response.put("status", "success");
	//				response.put("description", "data ditemukan");				
	//				response.put("data", a);
	//				httpStatus = HttpStatus.OK;
	//				return new ResponseEntity<>(response, httpStatus);
	//				
	//			} else {
	//				response.put("status", "failed");
	//				response.put("description", "data tidak ditemukan");
	//				httpStatus = HttpStatus.OK;
	//				
	//			}
	//		}
	//		return new ResponseEntity<>(response, httpStatus);
	//	}

	@PostMapping(value = "/filterSurah")
	@ResponseBody
	public ResponseEntity filterSurah(@RequestBody AlquranSurahDto mstQuran) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;

		List<MstAlQuran> qurans = quranServ.filterSurah(mstQuran.getPhonetic_ind());
		AlQuransResponseDto resp = new AlQuransResponseDto();
		resp.setSurah(qurans);
		response.put("status", "success");
		response.put("description", "data ditemukan");
		response.put("data", resp);
		httpStatus = HttpStatus.OK;
		return new ResponseEntity<>(response, httpStatus);
	}

	//	@PostMapping(value = "/searchJuz")
	//	@ResponseBody
	//	public ResponseEntity searchJuz(@RequestBody AlquranJuzDto mstQurans) {
	//		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	//		HttpStatus httpStatus = null;
	//
	//		RestTemplate restTemplate = new RestTemplate();
	//		HttpHeaders headers = new HttpHeaders();
	//
	//		HttpEntity<String> entity = new HttpEntity<>(headers);
	//		ResponseEntity<List<AlquranJuzDto>> response1 = null;
	//		ParameterizedTypeReference<List<AlquranJuzDto>> type = new ParameterizedTypeReference<List<AlquranJuzDto>>() {
	//		};
	//
	//		response1 = restTemplate.exchange(quranUrl + "al-quran/tbl_quran.json", HttpMethod.GET, entity, type);
	//		LOG.info(response1.getBody().get(0).get_id());
	//		List<AlquranJuzDto> juz = new ArrayList<>();
	//		if (response1.getBody().size() > 0) {
	//		for (AlquranJuzDto a : response1.getBody()) {
	//			if (mstQurans.getJuz() == a.getJuz() && mstQurans.getName().equalsIgnoreCase(a.getName())) {
	//				juz.add(a);
	//				LOG.info("bisa");			
	//			} 
	//		}
	//		if(juz.size() > 0) {
	//			response.put("status", "success");
	//			response.put("description", "data ditemukan");				
	//			response.put("data", juz);
	//			httpStatus = HttpStatus.OK;
	//		}else {
	//			LOG.info("gagal");
	//			response.put("status", "failed");
	//			response.put("description", "data tidak ditemukan");
	//			httpStatus = HttpStatus.OK;
	//			
	//		}
	//		
	//		}else {
	//			LOG.info("gagal");
	//			response.put("status", "failed");
	//			response.put("description", "data kosong");
	//			httpStatus = HttpStatus.OK;
	//			
	//		}
	//		return new ResponseEntity<>(response, httpStatus);
	//	}

	@PostMapping(value = "/search")
	@ResponseBody
	public ResponseEntity searchJuz(@RequestBody AlQuranSearchDto mstQurans) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;

		List<MstAlQuran> listJuz = new ArrayList<>();
		List<MstAlQuran> listSurah = new ArrayList<>();
		List<MstAlQuran> quransJuz = quranServ.searchJuz("%"+mstQurans.getSearch()+"%");
		List<MstAlQuran> quransSurah = quranServ.searchSura("%"+mstQurans.getSearch()+"%");
		if(quransJuz!=null && quransJuz.size()>0) {
			for(MstAlQuran a : quransJuz) {
				MstAlQuran search = new MstAlQuran();
				String ind = a.getIndSura().toLowerCase();
				StringBuilder kalimatBaru = new StringBuilder();
				for(String b : ind.split(" ")) {
					String output = b.substring(0, 1).toUpperCase() + b.substring(1);
					kalimatBaru.append(output).append(" ");
					LOG.info(output);
				}	
				search.setId_sura(a.getId_sura());
				search.setAr(a.getAr());
				search.setAya(a.getAya());
				search.setCount(a.getCount());
				search.setEnAya(a.getEnAya());
				search.setEnSura(a.getEnSura());
				search.setFirstPage(a.getFirstPage());
				search.setPhonetic(a.getPhonetic());
				search.setPhoneticInd(a.getPhoneticInd());
				search.setIdAya(a.getIdAya());
				search.setIndopak(a.getIndopak());
				search.setIndSura(kalimatBaru.toString());
				search.setJuz(a.getJuz());
				search.setLiterasi(a.getLiterasi());
				search.setMs(a.getMs());
				search.setNameSura(a.getNameSura());
				search.setPage(a.getPage());
				search.setSura(a.getSura());
				search.setUtsmani(a.getUtsmani());
				listJuz.add(search);
			}
		}

		if(quransSurah!=null && quransSurah.size()>0) {
			for(MstAlQuran a : quransSurah) {
				MstAlQuran search = new MstAlQuran();
				String ind = a.getIndSura().toLowerCase();
				StringBuilder kalimatBaru = new StringBuilder();
				for(String b : ind.split(" ")) {
					String output = b.substring(0, 1).toUpperCase() + b.substring(1);
					kalimatBaru.append(output).append(" ");
					LOG.info(output);
				}	
				search.setId_sura(a.getId_sura());
				search.setAr(a.getAr());
				search.setAya(a.getAya());
				search.setCount(a.getCount());
				search.setEnAya(a.getEnAya());
				search.setEnSura(a.getEnSura());
				search.setFirstPage(a.getFirstPage());
				search.setPhonetic(a.getPhonetic());
				search.setPhoneticInd(a.getPhoneticInd());
				search.setIdAya(a.getIdAya());
				search.setIndopak(a.getIndopak());
				search.setIndSura(kalimatBaru.toString());
				search.setJuz(a.getJuz());
				search.setLiterasi(a.getLiterasi());
				search.setMs(a.getMs());
				search.setNameSura(a.getNameSura());
				search.setPage(a.getPage());
				search.setSura(a.getSura());
				search.setUtsmani(a.getUtsmani());
				listSurah.add(search);
			}
		}

		AlQuransResponseDto resp = new AlQuransResponseDto();
		if(listSurah!=null && listSurah.size()>0) {
			resp.setSurah(listSurah);
		}
		if(listJuz!=null && listJuz.size()>0){
			resp.setJuz(listJuz);
		}
		response.put("status", "success");
		response.put("description", "data ditemukan");
		response.put("data", resp);
		httpStatus = HttpStatus.OK;


		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/filterJuz")
	@ResponseBody
	public ResponseEntity filterJuz(@RequestBody AlquranJuzDto mstQurans) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;

		List<MstAlQuran> qurans = quranServ.filterJuz(new Long(mstQurans.getJuz()));
		AlQuransResponseDto resp = new AlQuransResponseDto();
		resp.setJuz(qurans);
		Map<String, Set<MstAlQuran>> group = new HashMap<>();

		if(qurans.size() > 0) {
			for(MstAlQuran p : qurans){
				String key = p.getNameSura();
				if(!group.containsKey(key)){
					group.put(key, new HashSet<>());
				}
				group.get(key).add(p);
			}

			//			Map<String, Set<MstAlQuran>> m1 = new HashMap<>();
			//			m1.putAll(group);

			Map<String, List<MstAlQuran>> m1new = new HashMap<>();

			for(Entry<String, Set<MstAlQuran>> entry : group.entrySet()) {
				Set<MstAlQuran> setInbox = entry.getValue();
				List<MstAlQuran> inboxSorted = setInbox.stream().collect(Collectors.toList());

				Collections.sort(inboxSorted, (o1, o2) -> o2.getNameSura().compareTo(o1.getNameSura()));
				entry.setValue(new HashSet<MstAlQuran>(inboxSorted));				
				m1new.put(entry.getKey(), inboxSorted);
			}

			Map<String, List<MstAlQuran>> m3 = new HashMap<>();
			m3.putAll(m1new);

			response.put("data", m3);
		}else {
			response.put("data", null);
		}
		response.put("status", "success");
		response.put("description", "data ditemukan");
		httpStatus = HttpStatus.OK;
		return new ResponseEntity<>(response, httpStatus);
	}

	@GetMapping(value = "/listJuz")
	@ResponseBody
	public Map<String, Object> getListJuz() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();


		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", quranServ.listJuz());
		return response;

	}
}
