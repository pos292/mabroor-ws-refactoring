package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.AvailableTimeService;

@RestController
@RequestMapping("/v1/api/availabletime")
public class AvailableTimeController {

	@Autowired
	private AvailableTimeService timeService;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",timeService.getListAvailableTime());
		return response;
	}
	
	@GetMapping(value = "/{timeId}")
	public LinkedHashMap<String, Object> getTimeById(@PathVariable Long timeId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",timeService.findById(timeId));
		return response;
	}
}
