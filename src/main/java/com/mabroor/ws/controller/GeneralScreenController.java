package com.mabroor.ws.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.mabroor.ws.dto.AxwayTokenResponseDto;
import com.mabroor.ws.dto.FIFDataDto;
import com.mabroor.ws.dto.FIFWisataReligiResultDto;
import com.mabroor.ws.dto.JamCreditSimulationSubmissionRequestDto;
import com.mabroor.ws.dto.JamCreditSimulationSubmissionResponseDto;
import com.mabroor.ws.dto.MstPengajuanMotorDto;
import com.mabroor.ws.entity.MstApplication;
import com.mabroor.ws.entity.MstApplicationTransaction;
import com.mabroor.ws.entity.MstLeadsRule;
import com.mabroor.ws.entity.MstPengajuanJaminan;
import com.mabroor.ws.entity.MstPengajuanMotor;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.entity.MstWisataReligi;
import com.mabroor.ws.service.ApplicationService;
import com.mabroor.ws.service.ApplicationTransactionService;
import com.mabroor.ws.service.LeadsRuleService;
import com.mabroor.ws.service.PengajuanJaminanService;
import com.mabroor.ws.service.PengajuanMotorService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.service.WisataReligiService;
import com.mabroor.ws.service.impl.MailClient;
import com.mabroor.ws.util.ObjectMapperUtil;
import com.mabroor.ws.util.SHA256Utils;
import com.mabroor.ws.util.Common;
import com.mabroor.ws.util.AES256Utils;
import com.mabroor.ws.util.BASE64DecodedMultipartFile;
import com.mabroor.ws.util.Constant;

import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/generalscreen/eksternal")
public class GeneralScreenController {

	private static final Logger LOG = Logger.getLogger(GeneralScreenController.class);

	@Autowired
	private PengajuanJaminanService pengajuanJamService;

	@Autowired
	private WisataReligiService wisataService;

	@Autowired
	private PengajuanMotorService pengajuanService;

	@Autowired
	private LeadsRuleService leadsRuleServ;

	@Autowired
	private UserMobileService userMobileServ;

	@Autowired
	private ApplicationService appService;

	@Autowired
	private ApplicationTransactionService appTrxService;

	@Autowired
	private MailClient mailClient;

	@Value("${axway.api.url}")
	private String axwayApiUrl;

	@Value("${axway.oauth.url}")
	private String oauthUrl;

	@Value("${axway.oauth.clientid}")
	private String oauthClientId;

	@Value("${axway.oauth.clientsecret}")
	private String oauthClientSecret;

	@Value("${axway.oauth.granttype}")
	private String oauthGrantType;

	@Value("${fif.userkey}")
	private String fifUserKey;
	
	@Value("${moxa.mabroor.channel}")
	private String channel;

	@Value("${moxa.mabroor.os}")
	private String os;

	@Value("${moxa.mabroor.userid}")
	private String userid;

	@PostMapping(value= "/hajifif")
	@ResponseBody
	public ResponseEntity submitfif(@RequestBody MstWisataReligi mstWisata, @RequestHeader(value = "phone", required = true) String phone) throws IllegalBlockSizeException {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;
		MstUserMobile byPhone = userMobileServ.getOneByPhoneNumber(AES256Utils.encrypt(phone));
		Optional<MstUserMobile> mstUserMobileOpt = null;
		if(byPhone!=null) {
			mstUserMobileOpt = userMobileServ.findById(byPhone.getId());
		}else {
			MstUserMobile user = new MstUserMobile();
			user.setName(mstWisata.getNama());
			user.setFirstName(mstWisata.getNama());
			user.setEmail(mstWisata.getEmail());
			user.setPhone(phone);
			if("Pria".equalsIgnoreCase(mstWisata.getGender())) {
				user.setGender("Laki-laki");
			}else if("Wanita".equalsIgnoreCase(mstWisata.getGender())) {
				user.setGender("Perempuan");
			}else {
				user.setGender(mstWisata.getGender());
			}
			user.setUserMobileStatus("01");

			String goals = "[1,5,4]";
			user.setGoals(goals);
			user.setUserMobileUuid(UUID.randomUUID().toString());
			user.setFingerprintToggle("ON");
			user.setPin(SHA256Utils.encrypt("123456"));
			MstUserMobile previousUser = new MstUserMobile();
			previousUser = userMobileServ.findPreviousUser(phone);
			if(previousUser != null) {
				user.setPreviousUserId(previousUser.getId());
			}
			MstUserMobile saveBaru = userMobileServ.save1(user);

			mstUserMobileOpt = userMobileServ.findById(saveBaru.getId());
		}
		MstUserMobile mstUserMobile = mstUserMobileOpt.get();
		String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
		String lastname = mstUserMobile.getLastName() != null ? mstUserMobile.getLastName() : "";
		String fullname = firstName;
		if(!"".equals(lastname)) {
			fullname += " " + lastname;
		}else {
			fullname = mstUserMobile.getName();
			mstWisata.setNamaDepan(fullname);
		}
		if(!"".equals(firstName)) {
			mstWisata.setNamaDepan(firstName);
			mstWisata.setNamaBelakang(lastname);
		}
		mstWisata.setWisataStatus("01");
		mstWisata.setUserId(mstUserMobile.getId());
		mstWisata.setEmailDataUser(mstUserMobile.getEmail());
		mstWisata.setPhoneDataUser(mstUserMobile.getPhone());
		mstWisata.setNameDataUser(fullname);

		//default set leads user to user mobile
		mstWisata.setEmail(mstUserMobile.getEmail());
		mstWisata.setNoHandphone(mstUserMobile.getPhone());
		mstWisata.setNama(fullname);

		Locale localeCurrency = new Locale("id", "ID");

		if(mstWisata.getTanggalLahir() == null || "".equals(mstWisata.getTanggalLahir())) {
			mstWisata.setTanggalLahir(new SimpleDateFormat("dd MMMM yyyy", localeCurrency).format(mstUserMobile.getBirthDate()));
		}

		if(mstWisata.getAlamatSesuaiKtp() == null || "".equals(mstWisata.getAlamatSesuaiKtp())) {
			mstWisata.setAlamatSesuaiKtp(mstUserMobile.getAddressKtp());
		}

		if(mstWisata.getAlamatTempatTinggal() == null || "".equals(mstWisata.getAlamatTempatTinggal())){
			mstWisata.setAlamatTempatTinggal(mstUserMobile.getAddress());
		}

		if(mstWisata.getProductChoose()!=null && "Haji".equalsIgnoreCase(mstWisata.getProductChoose()) && mstWisata.getPurposeId()==13) {

			Gson gson = new Gson();
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();


			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.set("User-Agent", "CaasReportAPI/1.0.0");

			MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
			map.add("client_id",oauthClientId);
			map.add("client_secret",oauthClientSecret);
			map.add("grant_type",oauthGrantType);

			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
			ResponseEntity<AxwayTokenResponseDto> response1 = null;
			ParameterizedTypeReference<AxwayTokenResponseDto> type = new ParameterizedTypeReference<AxwayTokenResponseDto>() {};
			response1 = restTemplate.exchange(oauthUrl, HttpMethod.POST, entity, type);



			if(response1 != null && response1.getBody() !=null) {
				if(response1.getBody().getAccess_token() != null) {
					String tokenFIF = response1.getBody().getAccess_token();
					System.out.println("TOKEN === " + tokenFIF);
					FIFWisataReligiResultDto hajiFIF = new FIFWisataReligiResultDto();

					hajiFIF.setCallVia("");
					hajiFIF.setCallViaName("");
					hajiFIF.setContactDetail("");
					hajiFIF.setPaketCode("");
					hajiFIF.setPaketValue("");
					hajiFIF.setTenor(mstWisata.getTenor());
					hajiFIF.setDp(mstWisata.getAmountDp());
					hajiFIF.setLeadId("");
					if("Haji".equalsIgnoreCase(mstWisata.getProductChoose())) {
						hajiFIF.setProdukPembiayaan("H");
						hajiFIF.setNamaProdukPembiayaan(mstWisata.getProductChoose());
					}else {
						hajiFIF.setProdukPembiayaan("U");
						hajiFIF.setNamaProdukPembiayaan(mstWisata.getProductChoose());
					}
					hajiFIF.setFullname(mstWisata.getNama());
					hajiFIF.setCustomerType("");
					hajiFIF.setNoKTP(mstUserMobile.getKtpNumber());
					String mobilephone1 = mstWisata.getPhoneDataUser().replaceAll("\\+62", "0");
					//								System.out.println("phoneee = " + AES256Utils.encrypt("+62899999999"));
					hajiFIF.setMobilePhone1(mobilephone1);
					String mobilephone = mstWisata.getNoHandphone().replaceAll("\\+62", "0");
					//								System.out.println("phoneee 222 = " + mobilephone);
					hajiFIF.setMobilePhone2(mobilephone);
					hajiFIF.setEmail(mstWisata.getEmail());
					hajiFIF.setBirthdate(mstWisata.getTanggalLahir());
					hajiFIF.setBirthPlace(mstWisata.getKota());
					if("Laki-laki".equalsIgnoreCase(mstUserMobile.getGender())) {
						hajiFIF.setGender("M");
					}else if("Perempuan".equalsIgnoreCase(mstUserMobile.getGender())) {
						hajiFIF.setGender("F");
					}
					hajiFIF.setMaritalStatus("");
					hajiFIF.setHomeOwnership("");
					hajiFIF.setEducation("");
					hajiFIF.setOccupationType("");
					hajiFIF.setOccupation("");
					hajiFIF.setJobPosition("");
					hajiFIF.setMonthlyIncome("");
					hajiFIF.setAddressLine(mstWisata.getAlamatTempatTinggal());
					hajiFIF.setNoRT("000");
					hajiFIF.setNoRW("000");
					hajiFIF.setStateId(mstWisata.getProvinsiId());
					hajiFIF.setCityId(mstWisata.getKabupatenId());
					hajiFIF.setDistrictId(mstWisata.getKecamatanId());
					hajiFIF.setSubDistrictId(mstWisata.getKelurahanId());
					hajiFIF.setZipcode(mstWisata.getKodePosId());
					hajiFIF.setReferral("");
					hajiFIF.setUserKey(fifUserKey);
					hajiFIF.setLeadDataSelfieId("");
					hajiFIF.setLeadDataKTPId("");
					hajiFIF.setSimilaritySelfieAndKTP("");
					hajiFIF.setLatitude("");
					hajiFIF.setLongitude("");
					hajiFIF.setSubZipcode(mstWisata.getKodePos());
					hajiFIF.setStateName(mstWisata.getProvinsi());
					hajiFIF.setCityName(mstWisata.getKota());
					hajiFIF.setDistrictName(mstWisata.getKecamatan());
					hajiFIF.setSubDistrictName(mstWisata.getKelurahan());

					headers.set("Authorization", "Bearer " + tokenFIF);
					headers.setContentType(MediaType.APPLICATION_JSON);
					String jsonFIF = gson.toJson(hajiFIF);
					System.out.println(jsonFIF);
					System.out.println("proses insert simulasi");
					HttpEntity<String> entityFIF = new HttpEntity<>(jsonFIF, headers);
					ResponseEntity<FIFDataDto> responseFIF = null;
					ParameterizedTypeReference<FIFDataDto> typeFIF = new ParameterizedTypeReference<FIFDataDto>() {
					};
					responseFIF = restTemplate.exchange(axwayApiUrl +"/backend/amitra/ext/submit", HttpMethod.POST, entityFIF, typeFIF);

					if (responseFIF != null && responseFIF.getBody() != null) {
						if (responseFIF.getBody().getErrorCode() !=null && responseFIF.getBody().getErrorCode() == 0) {
							mstWisata.setIntegrationLeadsId(responseFIF.getBody().getResult().getLeadId());

							MstWisataReligi pengajuanUmrohBaru = wisataService.save(mstWisata);

							MstLeadsRule mstLeadsRule = new MstLeadsRule();
							mstLeadsRule.setUserId(mstUserMobile.getId());
							mstLeadsRule.setLeadsId(pengajuanUmrohBaru.getId());
							mstLeadsRule.setActionStatus("NEW");
							mstLeadsRule.setCompany("FIF Amitra");
							mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
							if(mstWisata.getPurposeId() == 10) {
								mstLeadsRule.setLeadsType("UMROH");
							}else {
								mstLeadsRule.setLeadsType("HAJI");
							}
							leadsRuleServ.save(mstLeadsRule);

							MstApplication newApp = new MstApplication();
							newApp.setLeadsId(pengajuanUmrohBaru.getId());
							newApp.setCompany("FIF Amitra");
							if(mstWisata.getPurposeId() == 10) {
								newApp.setLeadsType("UMROH");
							}else {
								newApp.setLeadsType("HAJI");
							}
							newApp.setPickupBu(null);
							newApp.setStatus("Inprogress");
							newApp.setUserId(mstUserMobile.getId());
							MstApplication newApp1 = appService.save(newApp);


							MstApplicationTransaction trxApp = new MstApplicationTransaction();
							trxApp.setApplicationId(newApp1.getId());													
							trxApp.setStatus("Inprogress");													
							trxApp.setDetail(null);
							trxApp.setTransactionDate(new Date());
							trxApp.setBatchUuid("" + newApp1.getId());
							appTrxService.save(trxApp);

							//send to user profile
							if("Umroh".equalsIgnoreCase(mstWisata.getProductChoose())) { 	
								//send to user profile
								if(!mstUserMobile.getEmail().equals(pengajuanUmrohBaru.getEmail())) {
									mailClient.prepareAndSendPengajuanUmrohUserMobile(mstUserMobile.getEmail(), "Pengajuan Perjalanan Religi", pengajuanUmrohBaru);
								}
								//send to leads user
								mailClient.prepareAndSendPengajuanUmrohUserLeads(pengajuanUmrohBaru.getEmail(), "Pengajuan Perjalanan Religi", pengajuanUmrohBaru);

							}else if ("Haji".equalsIgnoreCase(mstWisata.getProductChoose())){
								if(!mstUserMobile.getEmail().equals(pengajuanUmrohBaru.getEmail())) {
									mailClient.prepareAndSendPengajuanHajiUserMobile(mstUserMobile.getEmail(), "Pengajuan Perjalanan Religi", pengajuanUmrohBaru);
								}
								//send to leads user
								mailClient.prepareAndSendPengajuanHajiUserLeads(pengajuanUmrohBaru.getEmail(), "Pengajuan Perjalanan Religi", pengajuanUmrohBaru);

							}

							response.put("status", "success");
							response.put("description", "Success! Wisata Religi successfully submitted.");
							httpStatus = HttpStatus.OK;
						}else if(responseFIF.getBody().getErrorCode() !=null && responseFIF.getBody().getErrorCode() == 70002) {
							response.put("status", "failed");
							response.put("description", "Anda telah mencapai maksimum pengajuan amitra.");
							httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
						}else {
							response.put("status", "failed");
							response.put("description", "Gagal melakukan submit data. Silakan coba beberapa saat lagi (1).");
							httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
						}
					}else {
						response.put("status", "failed");
						response.put("description", "Gagal melakukan submit data. Silakan coba beberapa saat lagi (2).");
						httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
					}
				}
			}
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value="/hajiacc")
	@ResponseBody
	public ResponseEntity<?> saveAcc(@RequestBody MstWisataReligi mstWisata, @RequestHeader(value = "phone", required = true) String phone) throws ParseException, IllegalBlockSizeException {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {MstUserMobile byPhone = userMobileServ.getOneByPhoneNumber(AES256Utils.encrypt(phone));
		Optional<MstUserMobile> mstUserMobileOpt = null;
		if(byPhone!=null) {
			mstUserMobileOpt = userMobileServ.findById(byPhone.getId());
		}else {
			MstUserMobile user = new MstUserMobile();
			user.setName(mstWisata.getNama());
			user.setFirstName(mstWisata.getNama());
			user.setEmail(mstWisata.getEmail());
			user.setPhone(phone);
			if("Pria".equalsIgnoreCase(mstWisata.getGender())) {
				user.setGender("Laki-laki");
			}else if("Wanita".equalsIgnoreCase(mstWisata.getGender())) {
				user.setGender("Perempuan");
			}else {
				user.setGender(mstWisata.getGender());
			}
			user.setUserMobileStatus("01");

			String goals = "[1,5,4]";
			user.setGoals(goals);
			user.setUserMobileUuid(UUID.randomUUID().toString());
			user.setFingerprintToggle("ON");
			user.setPin(SHA256Utils.encrypt("123456"));
			MstUserMobile previousUser = new MstUserMobile();
			previousUser = userMobileServ.findPreviousUser(phone);
			if(previousUser != null) {
				user.setPreviousUserId(previousUser.getId());
			}
			MstUserMobile saveBaru = userMobileServ.save1(user);

			mstUserMobileOpt = userMobileServ.findById(saveBaru.getId());
		}
		MstUserMobile mstUserMobile = mstUserMobileOpt.get();
		String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
		String lastname = mstUserMobile.getLastName() != null ? mstUserMobile.getLastName() : "";
		String fullname = firstName;
		if(!"".equals(lastname)) {
			fullname += " " + lastname;
		}

		mstWisata.setWisataStatus("01");
		mstWisata.setUserId(mstUserMobile.getId());
		mstWisata.setEmailDataUser(mstUserMobile.getEmail());
		mstWisata.setPhoneDataUser(mstUserMobile.getPhone());
		mstWisata.setNameDataUser(fullname);
		mstWisata.setNama(fullname);
		mstWisata.setTransactionDateTime(new Date());

		MstWisataReligi haji = wisataService.save(mstWisata);

		MstLeadsRule mstLeadsRule = new MstLeadsRule();
		mstLeadsRule.setUserId(mstUserMobile.getId());
		mstLeadsRule.setLeadsId(haji.getId());
		mstLeadsRule.setActionStatus("NEW");
		mstLeadsRule.setCompany("ACC Syariah");
		mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
		mstLeadsRule.setLeadsType("HAJI");
		leadsRuleServ.save(mstLeadsRule);

		if(!mstUserMobile.getEmail().equals(haji.getEmail())) {
			mailClient.prepareAndSendPengajuanHajiUserMobile(mstUserMobile.getEmail(), "Pengajuan Pembiayaan Haji", haji);
			System.out.println("masuk email");
		}
		//send to leads user
		mailClient.prepareAndSendPengajuanHajiUserLeads(haji.getEmail(), "Pengajuan Pembiayaan Haji", haji);
		System.out.println("masuk email leads");

		response.put("status", "success");
		response.put("description", "Success! Wisata Religi successfully submitted.");
		httpStatus = HttpStatus.OK;

		}catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		}catch (NullPointerException np) {
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch(RuntimeException re) {
			LOG.info(re.getMessage());
			re.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch(OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value="/motor")
	@ResponseBody
	public ResponseEntity saveMotor(@RequestBody MstPengajuanMotorDto mstPengajuan, @RequestHeader(value = "phone", required = true) String phone) {
		LOG.info(mstPengajuan.getName());
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;
		MstUserMobile byPhone = userMobileServ.getOneByPhoneNumber(AES256Utils.encrypt(phone));
		Optional<MstUserMobile> mstUserMobileOpt = null;
		if(byPhone!=null) {
			mstUserMobileOpt = userMobileServ.findById(byPhone.getId());
		}else {
			MstUserMobile user = new MstUserMobile();
			user.setName(mstPengajuan.getName());
			user.setFirstName(mstPengajuan.getName());
			user.setEmail(mstPengajuan.getEmail());
			user.setPhone(phone);
			if("Pria".equalsIgnoreCase(mstPengajuan.getGender())) {
				user.setGender("Laki-laki");
			}else if("Wanita".equalsIgnoreCase(mstPengajuan.getGender())) {
				user.setGender("Perempuan");
			}else {
				user.setGender(mstPengajuan.getGender());
			}
			user.setUserMobileStatus("01");

			String goals = "[1,5,4]";
			user.setGoals(goals);
			user.setUserMobileUuid(UUID.randomUUID().toString());
			user.setFingerprintToggle("ON");
			user.setPin(SHA256Utils.encrypt("123456"));
			MstUserMobile previousUser = new MstUserMobile();
			previousUser = userMobileServ.findPreviousUser(phone);
			if(previousUser != null) {
				user.setPreviousUserId(previousUser.getId());
			}
			MstUserMobile saveBaru = userMobileServ.save1(user);

			mstUserMobileOpt = userMobileServ.findById(saveBaru.getId());
		}
		MstUserMobile mstUserMobile = mstUserMobileOpt.get();

		byte[] fileByteFotoUnit = new byte[0];
		MultipartFile fileFotoUnit = null;

		byte[] fileByteFotoKtp = new byte[0];
		MultipartFile fileFotoKtp = null;
		
		if (mstPengajuan.getFotoUnit() == null
				|| "".equals(mstPengajuan.getFotoUnit())) {
			fileFotoUnit = null;
		} else {
			try {
				fileByteFotoUnit = Common.getBase64EncodedImage(mstPengajuan.getFotoUnit());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteFotoUnit);

			fileByteFotoUnit = Base64.getDecoder().decode(imgByte);
			fileFotoUnit = new BASE64DecodedMultipartFile(fileByteFotoUnit, "img.png");
		}
		
		if (mstPengajuan.getKtpPhoto() == null
				|| "".equals(mstPengajuan.getKtpPhoto())) {
			fileFotoKtp = null;
		} else {
			try {
				fileByteFotoKtp = Common.getBase64EncodedImage(mstPengajuan.getKtpPhoto());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteFotoKtp);

			fileByteFotoKtp = Base64.getDecoder().decode(imgByte);
			fileFotoKtp = new BASE64DecodedMultipartFile(fileByteFotoKtp, "img.png");
		}

		String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
		String lastname = mstPengajuan.getLastName();
		String fullname = firstName;
		if(!"".equals(lastname)) {
			fullname += " " + lastname;
		}else {
			fullname = mstUserMobile.getName();
		}

		mstPengajuan.setPengajuanStatus("01");
		mstPengajuan.setUserId(mstUserMobile.getId());
		if(mstPengajuan.getEmailDataUser() == null || "".equals(mstPengajuan.getEmailDataUser())) {
			mstPengajuan.setEmailDataUser(mstUserMobile.getEmail());
		}

		if(mstPengajuan.getPhoneDataUser() == null || "".equals(mstPengajuan.getPhoneDataUser())) {
			mstPengajuan.setPhoneDataUser(mstUserMobile.getPhone());
		}

		mstPengajuan.setNameDataUser(fullname);
		mstPengajuan.setCompanyName("FIFGROUP");

		mstPengajuan.setName(fullname);

		String temporaryBirthDate = mstPengajuan.getTransactionDateTime();
		mstPengajuan.setTransactionDateTime(null);
		MstPengajuanMotor mstMotor = ObjectMapperUtil.map(mstPengajuan,
				MstPengajuanMotor.class);
		try {
			mstMotor.setTransactionDateTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").parse(temporaryBirthDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		MstPengajuanMotor pengajuanMotorBaru = pengajuanService.save(fileFotoUnit, fileFotoKtp, mstMotor);

		MstLeadsRule mstLeadsRule = new MstLeadsRule();
		mstLeadsRule.setUserId(mstUserMobile.getId());
		mstLeadsRule.setLeadsId(pengajuanMotorBaru.getId());
		mstLeadsRule.setActionStatus("NEW");
		mstLeadsRule.setCompany("FIFGROUP");
		mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
		mstLeadsRule.setLeadsType(Constant.LEADS_TYPE_MOTOR);

		leadsRuleServ.save(mstLeadsRule);

		//send to user profile
		if(!mstUserMobile.getEmail().equals(pengajuanMotorBaru.getEmail())) {
			mailClient.prepareAndSendPengajuanMotorUserMobile(mstUserMobile.getEmail(), "Pengajuan Motor", pengajuanMotorBaru);
		}
		//send to leads user
		mailClient.prepareAndSendPengajuanMotorUserLeads(pengajuanMotorBaru.getEmail(), "Pengajuan Motor", pengajuanMotorBaru);

		response.put("status", "success");
		response.put("description", "Success! Leads Motor Data successfully submitted.");
		httpStatus = HttpStatus.OK;


		return new ResponseEntity<>(response,httpStatus);
	}

	@PostMapping(value = "/multiguna")
	@ResponseBody
	public ResponseEntity save(@RequestBody MstPengajuanJaminan mstJam,
			@RequestHeader(value = "phone", required = true) String phone) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;
		MstUserMobile byPhone = userMobileServ.getOneByPhoneNumber(AES256Utils.encrypt(phone));
		Optional<MstUserMobile> mstUserMobileOpt = null;
		if(byPhone!=null) {
			mstUserMobileOpt = userMobileServ.findById(byPhone.getId());
		}else {
			MstUserMobile user = new MstUserMobile();
			user.setName(mstJam.getName());
			user.setFirstName(mstJam.getName());
			user.setEmail(mstJam.getEmail());
			user.setPhone(phone);
			if("Pria".equalsIgnoreCase(mstJam.getGender())) {
				user.setGender("Laki-laki");
			}else if("Wanita".equalsIgnoreCase(mstJam.getGender())) {
				user.setGender("Perempuan");
			}else {
				user.setGender(mstJam.getGender());
			}
			user.setUserMobileStatus("01");

			String goals = "[1,5,4]";
			user.setGoals(goals);
			user.setUserMobileUuid(UUID.randomUUID().toString());
			user.setFingerprintToggle("ON");
			user.setPin(SHA256Utils.encrypt("123456"));
			MstUserMobile previousUser = new MstUserMobile();
			previousUser = userMobileServ.findPreviousUser(phone);
			if(previousUser != null) {
				user.setPreviousUserId(previousUser.getId());
			}
			MstUserMobile saveBaru = userMobileServ.save1(user);

			mstUserMobileOpt = userMobileServ.findById(saveBaru.getId());
		}
		MstUserMobile mstUserMobiles = mstUserMobileOpt.get();

		byte[] fileByteBpkbMobil = new byte[0];
		MultipartFile fileBpkbMobil = null;

		byte[] fileByteBpkbMotor = new byte[0];
		MultipartFile fileBpkbMotor = null;

		byte[] fileByteStnk = new byte[0];
		MultipartFile fileStnk = null;

		byte[] fileByteFotoKananDepan = new byte[0];
		MultipartFile fileFotoKananDepan = null;

		byte[] fileByteFotoKananBelakang = new byte[0];
		MultipartFile fileFotoKananBelakang = null;

		byte[] fileByteFotoKiriDepan = new byte[0];
		MultipartFile fileFotoKiriDepan = null;

		byte[] fileByteFotoKiriBelakang = new byte[0];
		MultipartFile fileFotoKiriBelakang = null;

		if (mstJam.getFotoBpkbMobil() == null
				|| "".equals(mstJam.getFotoBpkbMobil())) {
			fileBpkbMobil = null;
		} else {
			try {
				fileByteBpkbMobil = Common.getBase64EncodedImage(mstJam.getFotoBpkbMobil());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteBpkbMobil);

			fileByteBpkbMobil = Base64.getDecoder().decode(imgByte);
			fileBpkbMobil = new BASE64DecodedMultipartFile(fileByteBpkbMobil, "img.png");
		}

		if (mstJam.getFotoBpkbMotor() == null
				|| "".equals(mstJam.getFotoBpkbMotor())) {
			fileBpkbMotor = null;
		} else {
			try {
				fileByteBpkbMotor = Common.getBase64EncodedImage(mstJam.getFotoBpkbMotor());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteBpkbMotor);

			fileByteBpkbMotor = Base64.getDecoder().decode(imgByte);
			fileBpkbMotor = new BASE64DecodedMultipartFile(fileByteBpkbMotor, "img.png");
		}

		if (mstJam.getFotoStnk() == null || "".equals(mstJam.getFotoStnk())) {
			fileStnk = null;
		} else {
			try {
				fileByteStnk = Common.getBase64EncodedImage(mstJam.getFotoStnk());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteStnk);

			fileByteStnk = Base64.getDecoder().decode(imgByte);
			fileStnk = new BASE64DecodedMultipartFile(fileByteStnk, "img.png");
		}

		if (mstJam.getFotoKananDepan() == null
				|| "".equals(mstJam.getFotoKananDepan())) {
			fileFotoKananDepan = null;
		} else {
			try {
				fileByteFotoKananDepan = Common.getBase64EncodedImage(mstJam.getFotoKananDepan());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteFotoKananDepan);

			fileByteFotoKananDepan = Base64.getDecoder().decode(imgByte);
			fileFotoKananDepan = new BASE64DecodedMultipartFile(fileByteFotoKananDepan, "img.png");
		}

		if (mstJam.getFotoKananBelakang() == null
				|| "".equals(mstJam.getFotoKananBelakang())) {
			fileFotoKananBelakang = null;
		} else {
			try {
				fileByteFotoKananBelakang = Common.getBase64EncodedImage(mstJam.getFotoKananBelakang());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteFotoKananBelakang);

			fileByteFotoKananBelakang = Base64.getDecoder().decode(imgByte);
			fileFotoKananBelakang = new BASE64DecodedMultipartFile(fileByteFotoKananBelakang, "img.png");
		}

		if (mstJam.getFotoKiriBelakang() == null
				|| "".equals(mstJam.getFotoKiriBelakang())) {
			fileFotoKiriBelakang = null;
		} else {
			try {
				fileByteFotoKiriBelakang = Common.getBase64EncodedImage(mstJam.getFotoKiriBelakang());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteFotoKiriBelakang);

			fileByteFotoKiriBelakang = Base64.getDecoder().decode(imgByte);
			fileFotoKiriBelakang = new BASE64DecodedMultipartFile(fileByteFotoKiriBelakang, "img.png");
		}

		if (mstJam.getFotoKiriDepan() == null
				|| "".equals(mstJam.getFotoKiriDepan())) {
			fileFotoKiriDepan = null;
		} else {
			try {
				fileByteFotoKiriDepan = Common.getBase64EncodedImage(mstJam.getFotoKiriDepan());
			} catch (FileNotFoundException e) {
				LOG.info("Image not found" + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imgByte = Base64.getEncoder().encodeToString(fileByteFotoKiriDepan);

			fileByteFotoKiriDepan = Base64.getDecoder().decode(imgByte);
			fileFotoKiriDepan = new BASE64DecodedMultipartFile(fileByteFotoKiriDepan, "img.png");
		}

		String firstName = mstUserMobiles.getFirstName() != null ? mstUserMobiles.getFirstName() : "";
		String lastname = mstUserMobiles.getLastName() != null ? mstUserMobiles.getLastName() : "";
		String fullname = firstName;
		if (!"".equals(lastname)) {
			fullname += " " + lastname;
		}

		mstJam.setJaminanStatus("01");
		mstJam.setUserId(mstUserMobiles.getId());
		mstJam.setEmailDataUser(mstUserMobiles.getEmail());
		mstJam.setPhoneDataUser(mstUserMobiles.getPhone());
		mstJam.setNameDataUser(fullname);

		// default set leads user to user mobile
		mstJam.setEmail(mstUserMobiles.getEmail());
		mstJam.setPhone(mstUserMobiles.getPhone());

		Gson gson = new Gson();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set("User-Agent", "CaasReportAPI/1.0.0");

		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("client_id", oauthClientId);
		map.add("client_secret", oauthClientSecret);
		map.add("grant_type", oauthGrantType);

		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
		ResponseEntity<AxwayTokenResponseDto> response1 = null;
		ParameterizedTypeReference<AxwayTokenResponseDto> type = new ParameterizedTypeReference<AxwayTokenResponseDto>() {
		};
		response1 = restTemplate.exchange(oauthUrl, HttpMethod.POST, entity, type);

		if (response1 != null && response1.getBody() != null) {
			if (response1.getBody().getAccess_token() != null) {
				String tokenAcc = response1.getBody().getAccess_token();
				JamCreditSimulationSubmissionRequestDto reqSubmission = new JamCreditSimulationSubmissionRequestDto();						

				reqSubmission.setP_TENOR(mstJam.getTenor());
				reqSubmission.setP_ACCOUNT_ID("MOXA MABROOR");
				reqSubmission.setP_EMAIL(mstJam.getEmail());
				reqSubmission.setP_PHONE_NUMBER(mstJam.getPhone());
				reqSubmission.setP_NAME(mstJam.getName());
				reqSubmission.setP_CD_AREA(mstJam.getKotaPengajuanId());
				reqSubmission.setP_CD_VEHICLE_BRAND(mstJam.getBrandCode());
				reqSubmission.setP_CD_VEHICLE_TYPE(mstJam.getTypeCode());
				reqSubmission.setP_CHANNEL("MOXA");
				reqSubmission.setP_YEAR_OF_MFG(mstJam.getTahun());
				reqSubmission.setP_PRODUCT("0001");
				reqSubmission.setP_SOURCE_LEADS("108");
				reqSubmission.setP_FLAG_NEW_USED_VALID("M");
				reqSubmission.setP_AMT_CASH_PENCAIRAN(mstJam.getJumlahPinjaman());
				reqSubmission.setP_NOTES("MGU Syairah");

				Map<String, Object> objtemp1 = new HashMap<>();
				objtemp1.put("doSendDataLeads", reqSubmission);
				String json2 = gson.toJson(objtemp1);
				LOG.info(json2);
				LOG.info("proses submission simulasi");
				headers.set("Channel", channel);
				headers.set("Authorization", "Bearer "+ tokenAcc);
				LOG.info("Bearer"+ tokenAcc);
				headers.set("OS", os);
				headers.set("UserID", userid);
				headers.setContentType(MediaType.APPLICATION_JSON);
				HttpEntity<String> entity2 = new HttpEntity<>(json2, headers);
				ResponseEntity<JamCreditSimulationSubmissionResponseDto> response2 = null;
				ParameterizedTypeReference<JamCreditSimulationSubmissionResponseDto> type2 = new ParameterizedTypeReference<JamCreditSimulationSubmissionResponseDto>() {
				};
				response2 = restTemplate.exchange(axwayApiUrl + "/restv2/publicservices/submition/leads/", HttpMethod.POST,
						entity2, type2);
				LOG.info(response2.getBody().getOUT_MESS());
				if (response2 != null && response2.getBody() != null) {
					if ("T".equalsIgnoreCase(response2.getBody().getOUT_STAT())) {

						mstJam.setIntegrationLeadsId(response2.getBody().getOUT_DATA().get(0).getNOREG_LEADS());

						MstPengajuanJaminan pengajuanJaminanBaru = pengajuanJamService.save(fileBpkbMobil,
								fileBpkbMotor, fileStnk, fileFotoKananDepan, fileFotoKananBelakang,
								fileFotoKiriDepan, fileFotoKiriBelakang, mstJam);

						MstLeadsRule mstLeadsRule = new MstLeadsRule();
						mstLeadsRule.setUserId(mstUserMobiles.getId());
						mstLeadsRule.setLeadsId(pengajuanJaminanBaru.getId());
						mstLeadsRule.setActionStatus("INTEGRATION");
						mstLeadsRule.setCreatedBy(mstUserMobiles.getId().toString());
						mstLeadsRule.setLeadsType(Constant.LEADS_TYPE_KDA_MOBIL);
						mstLeadsRule.setCompany("ACC");
						leadsRuleServ.save(mstLeadsRule);				


						if (!mstUserMobiles.getEmail().equals(pengajuanJaminanBaru.getEmail())) {
							mailClient.prepareAndSendPengajuanMultiGunaJaminanUserMobile(
									mstUserMobiles.getEmail(), "Pengajuan Multiguna Jaminan",
									pengajuanJaminanBaru);
						}
						mailClient.prepareAndSendPengajuanMultiGunaJaminanUserLeads(
								pengajuanJaminanBaru.getEmail(), "Pengajuan Multiguna Jaminan",
								pengajuanJaminanBaru);

						response.put("status", "success");
						response.put("description", "Success! Leads Jaminan Data successfully submitted.");
						httpStatus = HttpStatus.OK;
					} else {
						LOG.info(response2.getBody().getOUT_MESS());

						response.put("status", "failed");
						response.put("description", "Submission failed, please try again later(1)");
						httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
					}
				} else {
					response.put("status", "failed");
					response.put("description", "Submission failed, please try again later(2)");
					httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
				}
			} else {
				response.put("status", "failed");
				response.put("description",
						"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}
		}

		return new ResponseEntity<>(response, httpStatus);
	}

}
