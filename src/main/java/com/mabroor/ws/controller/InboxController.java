package com.mabroor.ws.controller;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.crypto.IllegalBlockSizeException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.service.InboxService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.DateComparator;
import com.mabroor.ws.util.JWTGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/inbox")
public class InboxController {

	private static final Logger LOG = Logger.getLogger(InboxController.class);

	@Autowired
	private InboxService inboxServ;

	@Autowired
	private UserMobileService userMobileServ;
	

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;
	
	@GetMapping(value = "/read/{inboxId}")
	@ResponseBody
	public ResponseEntity getRead(@RequestHeader(value = "appToken", required = false) String token,
			@PathVariable String inboxId) {
		System.out.println(token);

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = HttpStatus.OK;
		if(token != null && !"".equals(token)) {
			try {
				Claims claims = JWTGenerator.getInstance().decodeJWT(token);
				if (Objects.isNull(claims)) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
					MstUserMobile mstUserMobile = null;
					if (!mstUserMobileOpt.isPresent()) {
						response.put("status", "failed");
						response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}else {
						mstUserMobile = mstUserMobileOpt.get();
						if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){

							Optional<MstInbox> inbox = inboxServ.isExistInboxId(Long.parseLong(inboxId));
							if (inbox.isPresent()) {
								// IF inboxId EXIST ON DB 
								inboxServ.updateInboxRead(Long.parseLong(inboxId));
							}
							else {
								// IF inboxId NOT EXIST ON DB
								Optional<MstInbox> InboxDB = inboxServ.findIdByIdDataAndUserId(Long.parseLong(inboxId), Long.parseLong(claims.getId()));
								if(InboxDB.isPresent()) {
									inboxServ.updateInboxRead(InboxDB.get().getId());
								}
							}
							
							response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
							response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
						}
					}
				}
			}catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch(IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (NullPointerException np) {
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (RuntimeException re) {
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}else {
			Optional<MstInbox> inbox = inboxServ.isExistInboxId(Long.parseLong(inboxId));
			if (inbox.isPresent()) {
				// IF inboxId EXIST ON DB 
				inboxServ.updateInboxRead(Long.parseLong(inboxId));
			}
			else {
				// IF inboxId NOT EXIST ON DB
				Optional<MstInbox> InboxDB = inboxServ.findIdByIdDataAndUserId(Long.parseLong(inboxId), null);
				if(InboxDB.isPresent()) {
					inboxServ.updateInboxRead(InboxDB.get().getId());
				}
			}
			
			response.put("status", "success");
			response.put("description", "succesfully read data");
		}
			
		return new ResponseEntity<>(response, httpStatus);
	}

	@GetMapping(value = "/{inboxCategory}")
	@ResponseBody
	public ResponseEntity getProduct(@RequestHeader(value = "appToken", required = false) String token,
			@PathVariable String inboxCategory) {
		System.out.println(token);

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = HttpStatus.OK;
		if(token != null && !"".equals(token)) {
			try {
				Claims claims = JWTGenerator.getInstance().decodeJWT(token);
				if (Objects.isNull(claims)) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
					MstUserMobile mstUserMobile = null;
					if (!mstUserMobileOpt.isPresent()) {
						response.put("status", "failed");
						response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}else {
						mstUserMobile = mstUserMobileOpt.get();
						if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){
							if ("all".equals(inboxCategory)) {
								List<MstInbox> inbox = inboxServ.getInboxAll(new Long(claims.getId()));
								for (MstInbox mstInbox : inbox) {
									em.detach(mstInbox);
									mstInbox.setIcon(this.imageBaseUrl + "mabroor_inbox_icon/" + mstInbox.getIcon());
								}
								
								Map<String, Set<MstInbox>> groupInbox = new HashMap<>();
								
								if(inbox.size() > 0) {
									Locale locale = new Locale("id", "ID");
									SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", locale);
									for(MstInbox p : inbox){
										String key = sdf.format(p.getCreatedDate());
										if(!groupInbox.containsKey(key)){
											groupInbox.put(key, new HashSet<>());
										}
										groupInbox.get(key).add(p);
									}
									
									Map<String, Set<MstInbox>> m1 = new TreeMap<String, Set<MstInbox>>(new DateComparator());
									m1.putAll(groupInbox);
									
									Map<String, List<MstInbox>> m1new = new HashMap<>();
									
									for(Entry<String, Set<MstInbox>> entry : m1.entrySet()) {
										Set<MstInbox> setInbox = entry.getValue();
										List<MstInbox> inboxSorted = setInbox.stream().collect(Collectors.toList());
										
										Collections.sort(inboxSorted, (o1, o2) -> o2.getCreatedDate().compareTo(o1.getCreatedDate()));
										
										entry.setValue(new HashSet<MstInbox>(inboxSorted));
										
										m1new.put(entry.getKey(), inboxSorted);
									}
									
									Map<String, List<MstInbox>> m3 = new TreeMap<String, List<MstInbox>>(new DateComparator());
									m3.putAll(m1new);
									
									response.put("data", m3);
								}else {
									response.put("data", null);
								}
								
								response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
								response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
								httpStatus = HttpStatus.OK;
							} else {
								List<MstInbox> inbox = inboxServ.getInboxId(Long.parseLong(inboxCategory), new Long(claims.getId()));
								for (MstInbox mstInbox : inbox) {
									em.detach(mstInbox);
									mstInbox.setIcon(this.imageBaseUrl + "mabroor_inbox_icon/" + mstInbox.getIcon());
								}
								
								Map<String, Set<MstInbox>> groupInbox = new HashMap<>();
								
								if(inbox.size() > 0) {
									Locale locale = new Locale("id", "ID");
									SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", locale);
									for(MstInbox p : inbox){
										String key = sdf.format(p.getCreatedDate());
										if(!groupInbox.containsKey(key)){
											groupInbox.put(key, new HashSet<>());
										}
										groupInbox.get(key).add(p);
									}
									
									Map<String, Set<MstInbox>> m1 = new TreeMap<String, Set<MstInbox>>(new DateComparator());
									m1.putAll(groupInbox);
									
									Map<String, List<MstInbox>> m1new = new HashMap<>();
									
									for(Entry<String, Set<MstInbox>> entry : m1.entrySet()) {
										Set<MstInbox> setInbox = entry.getValue();
										List<MstInbox> inboxSorted = setInbox.stream().collect(Collectors.toList());
										
										Collections.sort(inboxSorted, (o1, o2) -> o2.getCreatedDate().compareTo(o1.getCreatedDate()));
										
										entry.setValue(new HashSet<MstInbox>(inboxSorted));
										
										m1new.put(entry.getKey(), inboxSorted);
									}
									
									Map<String, List<MstInbox>> m3 = new TreeMap<String, List<MstInbox>>(new DateComparator());
									m3.putAll(m1new);
									
									response.put("data", m3);
								}else {
									response.put("data", null);
								}
								
								response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
								response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
								httpStatus = HttpStatus.OK;
							}
						}
					}
				}
			}					
			catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch(IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (NullPointerException np) {
				np.printStackTrace();
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (RuntimeException re) {
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}else {
			if ("all".equals(inboxCategory)) {
				List<MstInbox> inbox = inboxServ.getInboxAllBeforeLogin();
				for (MstInbox mstInbox : inbox) {
					em.detach(mstInbox);
					mstInbox.setIcon(this.imageBaseUrl + "mabroor_inbox_icon/" + mstInbox.getIcon());
				}
				
				Map<String, Set<MstInbox>> groupInbox = new HashMap<>();
				
				if(!inbox.isEmpty()) {
					Locale locale = new Locale("id", "ID");
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", locale);
					for(MstInbox p : inbox){
						String key = sdf.format(p.getCreatedDate());
						if(!groupInbox.containsKey(key)){
							groupInbox.put(key, new HashSet<>());
						}
						groupInbox.get(key).add(p);
					}
					
					Map<String, Set<MstInbox>> m1 = new TreeMap<String, Set<MstInbox>>(new DateComparator());
					m1.putAll(groupInbox);
					
					Map<String, List<MstInbox>> m1new = new HashMap<>();
					
					for(Entry<String, Set<MstInbox>> entry : m1.entrySet()) {
						Set<MstInbox> setInbox = entry.getValue();
						List<MstInbox> inboxSorted = setInbox.stream().collect(Collectors.toList());
						
						Collections.sort(inboxSorted, (o1, o2) -> o2.getCreatedDate().compareTo(o1.getCreatedDate()));
						
						entry.setValue(new HashSet<MstInbox>(inboxSorted));
						
						m1new.put(entry.getKey(), inboxSorted);
					}
					
					Map<String, List<MstInbox>> m3 = new TreeMap<String, List<MstInbox>>(new DateComparator());
					m3.putAll(m1new);
					
					response.put("data", m3);
				}else {
					response.put("data", null);
				}
				
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
				httpStatus = HttpStatus.OK;
			} else {
				List<MstInbox> inbox = inboxServ.getInboxIdBeforeLogin(Long.parseLong(inboxCategory));
				for (MstInbox mstInbox : inbox) {
					em.detach(mstInbox);
					mstInbox.setIcon(this.imageBaseUrl + "mabroor_inbox_icon/" + mstInbox.getIcon());
				}
				
				Map<String, Set<MstInbox>> groupInbox = new HashMap<>();
				
				if(!inbox.isEmpty()) {
					Locale locale = new Locale("id", "ID");
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", locale);
					for(MstInbox p : inbox){
						String key = sdf.format(p.getCreatedDate());
						if(!groupInbox.containsKey(key)){
							groupInbox.put(key, new HashSet<>());
						}
						groupInbox.get(key).add(p);
					}
					
					Map<String, Set<MstInbox>> m1 = new TreeMap<String, Set<MstInbox>>(new DateComparator());
					m1.putAll(groupInbox);
					
					Map<String, List<MstInbox>> m1new = new HashMap<>();
					
					for(Entry<String, Set<MstInbox>> entry : m1.entrySet()) {
						Set<MstInbox> setInbox = entry.getValue();
						List<MstInbox> inboxSorted = setInbox.stream().collect(Collectors.toList());
						
						Collections.sort(inboxSorted, (o1, o2) -> o2.getCreatedDate().compareTo(o1.getCreatedDate()));
						
						entry.setValue(new HashSet<MstInbox>(inboxSorted));
						
						m1new.put(entry.getKey(), inboxSorted);
					}
					
					Map<String, List<MstInbox>> m3 = new TreeMap<String, List<MstInbox>>(new DateComparator());
					m3.putAll(m1new);
					
					response.put("data", m3);
				}else {
					response.put("data", null);
				}
				
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
				httpStatus = HttpStatus.OK;
			}
		}
		
		return new ResponseEntity<>(response, httpStatus);
	}
	
	@GetMapping(value = "/status/inboxnotification")
	@ResponseBody
	public ResponseEntity getinboxnotif(@RequestHeader(value = "appToken", required = false) String token) {

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = HttpStatus.OK;
		if(token != null && !"".equals(token)) {
			try {
				Claims claims = JWTGenerator.getInstance().decodeJWT(token);
				if (Objects.isNull(claims)) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
					MstUserMobile mstUserMobile = null;
					if (!mstUserMobileOpt.isPresent()) {
						response.put("status", "failed");
						response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}else {
						mstUserMobile = mstUserMobileOpt.get();
						if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){
							List<MstInbox> inbox = inboxServ.getInboxNotif(new Long(claims.getId()));
							
							Map<String, String> inboxAvailable = new HashMap<>();
							
							if(!inbox.isEmpty()) {
								inboxAvailable.put("newInboxAvailable", "YES");
							}else {
								inboxAvailable.put("newInboxAvailable", "NO");
							}
							
							response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
							response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
							response.put("data", inboxAvailable);
							httpStatus = HttpStatus.OK;
						}
					}
				}
			}					
			catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch(IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (NullPointerException np) {
				np.printStackTrace();
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (RuntimeException re) {
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}else {
			List<MstInbox> inbox = inboxServ.getInboxNotifBeforeLogin();
			
			Map<String, String> inboxAvailable = new HashMap<>();
			
			if(inbox.size() > 0) {
				inboxAvailable.put("newInboxAvailable", "YES");
			}else {
				inboxAvailable.put("newInboxAvailable", "NO");
			}
			
			response.put("status", "success");
			response.put("description", "succesfully get data");
			response.put("data", inboxAvailable);
			httpStatus = HttpStatus.OK;
		}
		
		return new ResponseEntity<>(response, httpStatus);
	}
}
