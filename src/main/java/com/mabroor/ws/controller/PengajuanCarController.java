package com.mabroor.ws.controller;

import java.io.FileWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.service.ApplicationService;
import com.mabroor.ws.service.ApplicationTransactionService;
import com.mabroor.ws.service.InboxCategoryService;
import com.mabroor.ws.service.InboxService;
import com.mabroor.ws.entity.MstApplication;
import com.mabroor.ws.entity.MstApplicationTransaction;
import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.entity.MstInboxCategory;
import com.mabroor.ws.dto.PengajuanCarTafCalculatorDetaildto;
import com.mabroor.ws.dto.PengajuanCarTafDocumentReqDto;
import com.mabroor.ws.dto.PengajuanCarTafDto;
import com.mabroor.ws.dto.PengajuanCarTafReqDto;
import com.mabroor.ws.dto.ResponseNewCarTAFDto;
import com.mabroor.ws.dto.AxwayTokenResponseDto;
import com.mabroor.ws.dto.CarSubmissionRequestDto;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

import com.google.gson.Gson;
import com.mabroor.ws.dto.JamCreditSimulationSubmissionResponseDto;
import com.mabroor.ws.entity.MstLeadsRule;
import com.mabroor.ws.entity.MstPengajuanCar;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.service.LeadsRuleService;
import com.mabroor.ws.service.PengajuanCarService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.service.impl.MailClient;

@RestController
@RequestMapping("/v1/api/pengajuancar")
public class PengajuanCarController {
	private static final Logger LOG = Logger.getLogger(PengajuanCarController.class);
	public static FileWriter file;

	@Autowired
	private PengajuanCarService pengajuanService;

	@Autowired
	private UserMobileService userMobileService;
	
	@Autowired
	private ApplicationService appService;
	
	@Autowired
	private PushNotificationService pushNotificationService;
	
	@Autowired
	private InboxCategoryService inboxCategoryServ;
	
	@Autowired
	private InboxService inboxServ;

	@Autowired
	private ApplicationTransactionService appTrxService;

	@Autowired
	private LeadsRuleService leadsRuleServ;
	
	@Autowired
	private MailClient mailClient;


	@Value("${storage.upload.file}")
	private String storageUploadFile;

	@Value("${axway.api.url}")
	private String axwayApiUrl;

	@Value("${axway.oauth.url}")
	private String oauthUrl;

	@Value("${axway.oauth.clientid}")
	private String oauthClientId;

	@Value("${axway.oauth.clientsecret}")
	private String oauthClientSecret;

	@Value("${axway.oauth.granttype}")
	private String oauthGrantType;
	
	@Value("${moxa.mabroor.channel}")
	private String channel;

	@Value("${moxa.mabroor.os}")
	private String os;

	@Value("${moxa.mabroor.userid}")
	private String userid;

	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", pengajuanService.getListPengajuan());
		return response;
	}

	@PostMapping(value = "/submitAcc")
	@ResponseBody
	public ResponseEntity save(@RequestBody MstPengajuanCar mstCar,
			@RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileService.findById(new Long(claims.getId()));
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobiles = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobiles.getId()) && token.equals(mstUserMobiles.getToken())) {


						String firstName = mstUserMobiles.getFirstName() != null ? mstUserMobiles.getFirstName() : "";
						String lastname = mstUserMobiles.getLastName() != null ? mstUserMobiles.getLastName() : "";
						String fullname = firstName;
						if (!"".equals(lastname)) {
							fullname += " " + lastname;
						}

						mstCar.setPengajuanStatus("01");
						mstCar.setUserId(mstUserMobiles.getId());
						mstCar.setEmailDataUser(mstUserMobiles.getEmail());
						mstCar.setPhoneDataUser(mstUserMobiles.getPhone());
						mstCar.setNameDataUser(fullname);

						// default set leads user to user mobile
						mstCar.setEmail(mstUserMobiles.getEmail());
						mstCar.setPhone(mstUserMobiles.getPhone());
				
						Gson gson = new Gson();
						RestTemplate restTemplate = new RestTemplate();
						HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
						headers.set("User-Agent", "CaasReportAPI/1.0.0");

						MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
						map.add("client_id", oauthClientId);
						map.add("client_secret", oauthClientSecret);
						map.add("grant_type", oauthGrantType);

						HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
						ResponseEntity<AxwayTokenResponseDto> response1 = null;
						ParameterizedTypeReference<AxwayTokenResponseDto> type = new ParameterizedTypeReference<AxwayTokenResponseDto>() {
						};
						response1 = restTemplate.exchange(oauthUrl, HttpMethod.POST, entity, type);

						if (response1 != null && response1.getBody() != null) {
							if (response1.getBody().getAccess_token() != null) {
								String tokenAcc = response1.getBody().getAccess_token();
					     CarSubmissionRequestDto reqSubmission = new CarSubmissionRequestDto();						
						
						 reqSubmission.setP_ACCOUNT_ID("MOXA MABROOR");
						 reqSubmission.setP_EMAIL(mstCar.getEmail());
						 reqSubmission.setP_PHONE_NUMBER(mstCar.getPhone());
						 reqSubmission.setP_NAME(mstCar.getName());
						 reqSubmission.setP_CD_AREA(mstCar.getKotaPengajuanId());
						 reqSubmission.setP_YEAR_OF_MFG(mstCar.getYears());
						 reqSubmission.setP_CD_SP("100103");
						 reqSubmission.setP_CHANNEL("MOXA");	
						 reqSubmission.setP_CD_VEHICLE_BRAND(mstCar.getBrandCode());
						 reqSubmission.setP_CD_VEHICLE_TYPE(mstCar.getTypeCode());
						 reqSubmission.setP_TENOR(mstCar.getTenor());
						 reqSubmission.setP_DP(mstCar.getAmountDp());
						 reqSubmission.setP_PRODUCT("0002");
						 reqSubmission.setP_SOURCE_LEADS("108");
						 reqSubmission.setP_FLAG_NEW_USED_VALID("N");						
						 reqSubmission.setP_NOTES("New Car Syairah");

						Map<String, Object> objtemp1 = new HashMap<>();
						objtemp1.put("doSendDataLeads", reqSubmission);
						String json2 = gson.toJson(objtemp1);
						LOG.info(json2);
						LOG.info("proses submission simulasi");
						headers.set("Channel", channel);
						headers.set("Authorization", "Bearer "+ tokenAcc);
						LOG.info("Bearer"+ tokenAcc);
						headers.set("OS", os);
						headers.set("UserID", userid);
						headers.setContentType(MediaType.APPLICATION_JSON);
						HttpEntity<String> entity2 = new HttpEntity<>(json2, headers);
						ResponseEntity<JamCreditSimulationSubmissionResponseDto> response2 = null;
						ParameterizedTypeReference<JamCreditSimulationSubmissionResponseDto> type2 = new ParameterizedTypeReference<JamCreditSimulationSubmissionResponseDto>() {
						};
						response2 = restTemplate.exchange(axwayApiUrl + "/restv2/publicservices/submition/leads/", HttpMethod.POST,
								entity2, type2);
						LOG.info(response2.getBody().getOUT_MESS());
						if (response2 != null && response2.getBody() != null) {
							if ("T".equalsIgnoreCase(response2.getBody().getOUT_STAT())) {
								
								mstCar.setIntegrationLeadsId(response2.getBody().getOUT_DATA().get(0).getNOREG_LEADS());
							
								MstPengajuanCar pengajuanCar = pengajuanService.save(mstCar);
								
								MstLeadsRule mstLeadsRule = new MstLeadsRule();
								mstLeadsRule.setUserId(mstUserMobiles.getId());
								mstLeadsRule.setLeadsId(pengajuanCar.getId());
								mstLeadsRule.setActionStatus("INTEGRATION");
								mstLeadsRule.setCreatedBy(mstUserMobiles.getId().toString());
								mstLeadsRule.setLeadsType(Constant.LEADS_TYPE_MOBIL);
								mstLeadsRule.setCompany("ACC");
								leadsRuleServ.save(mstLeadsRule);		
								
								MstApplication newApp = new MstApplication();
								newApp.setLeadsId(pengajuanCar.getId());
								newApp.setCompany("ACC");
								newApp.setLeadsType(Constant.LEADS_TYPE_MOBIL);
								newApp.setPickupBu(null);
								newApp.setStatus("Inprogress");
								newApp.setUserId(mstUserMobiles.getId());
								MstApplication newApp1 = appService.save(newApp);


								MstApplicationTransaction trxApp = new MstApplicationTransaction();
								trxApp.setApplicationId(newApp1.getId());													
								trxApp.setStatus("Inprogress");													
								trxApp.setDetail(null);
								trxApp.setTransactionDate(new Date());
								trxApp.setBatchUuid("" + newApp1.getId());
								appTrxService.save(trxApp);
								
								Optional<MstUserMobile> userMobile = userMobileService.findById(pengajuanCar.getUserId());
								if (userMobile.isPresent()) {
									MstUserMobile temporaryUserMobile = userMobile.get();
									if (temporaryUserMobile.getFirebaseToken() != null) {

										MstInbox inbox = new MstInbox(); 
										Long id = new Long(3);
										MstInboxCategory category = inboxCategoryServ.findById(id);
										inbox.setTitle("Status pengajuan mobil");
										inbox.setDescription("Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan mobil " + pengajuanCar.getBrand() + " " + pengajuanCar.getType() + " " + pengajuanCar.getModel() + " yang kamu lakukan. Klik di sini untuk melihat status terkini!");
									    inbox.setIdInboxCategory(category);
									    inbox.setScreenName("NewCarDetail");
										inbox.setInboxType("DIRECT_SCREEN");
										inbox.setIcon("inbox_pengajuan.png");
										inbox.setInboxRead("NO");
										inbox.setUserId(temporaryUserMobile.getId());
										inbox.setIdData(pengajuanCar.getId());
										MstInbox inbx = inboxServ.save(inbox);

										PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
										LOG.info(temporaryUserMobile.getFirebaseToken());
										pushNotificationReq.setToken(temporaryUserMobile.getFirebaseToken());
										pushNotificationReq
										.setTitle("Status pengajuan mobil baru");

										Map<String, String> pushData = new HashMap<String, String>();

										Map<String, Object> jsonPush = new HashMap<String, Object>();
										jsonPush.put("screen", "NewCarDetail");


										Map<String, Object> params = new HashMap<String, Object>();
										params.put("id", pengajuanCar.getId());
										params.put("pageType", "");
										params.put("idInbox", inbx.getId());

										jsonPush.put("params", params);

										String jsonified = new Gson().toJson(jsonPush);
										pushData.put("json", jsonified);

										pushNotificationReq.setData(pushData);

										pushNotificationReq.setMessage(
										"Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan mobil " + pengajuanCar.getBrand() + " " + pengajuanCar.getType() + " " + pengajuanCar.getModel() + " yang kamu lakukan. Klik di sini untuk melihat status terkini!");
											
										try {
											pushNotificationService.sendMessageToToken(pushNotificationReq);

										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (ExecutionException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										
									} else {
										LOG.info("Firebase token not found for user " + temporaryUserMobile.getName() + " - "
												+ temporaryUserMobile.getPhone());
									}
								}


								if (!mstUserMobiles.getEmail().equals(pengajuanCar.getEmail())) {
									mailClient.prepareAndSendPengajuanMobilUserMobile(
											mstUserMobiles.getEmail(), "Pengajuan Pembiayaan Mobil Syariah",
											pengajuanCar);
								}
								mailClient. prepareAndSendPengajuanMobilUserLeads(
										pengajuanCar.getEmail(), "Pengajuan Pembiayaan Mobil Syariah",
										pengajuanCar);

								response.put("status", "success");
								response.put("description", "Success! Leads Pembiayaan Mobil successfully submitted.");
								httpStatus = HttpStatus.OK;
							} else {
								LOG.info(response2.getBody().getOUT_MESS());

								response.put("status", "failed");
								response.put("description", "Submission failed, please try again later(1)");
								httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
							}
						} else {
							response.put("status", "failed");
							response.put("description", "Submission failed, please try again later(2)");
							httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
						}
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
					}
				}
			}

		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
//			LOG.info(re.getMessage());
			re.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/submitTaf")
	@ResponseBody
	public ResponseEntity saveTaf(@RequestBody PengajuanCarTafDto mstMobil,
			@RequestHeader(value = "appToken", required = true) String token) throws IllegalBlockSizeException {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;

		LOG.info("submitnew triggered");

		Claims claims = JWTGenerator.getInstance().decodeJWT(token);
		if (Objects.isNull(claims)) {
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} else {
			Optional<MstUserMobile> mstUserMobileOpt = userMobileService.findById(new Long(claims.getId()));
			MstUserMobile mstUserMobile = null;
			if (!mstUserMobileOpt.isPresent()) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				mstUserMobile = mstUserMobileOpt.get();
				if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {

					String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
					String lastname = mstUserMobile.getLastName() != null ? mstUserMobile.getLastName() : "";
					String fullname = firstName;
					if (!"".equals(lastname)) {
						fullname += " " + lastname;
					}
					
					

					mstMobil.getData().setPengajuanStatus("01");
					mstMobil.getData().setUserId(mstUserMobile.getId());
					mstMobil.getData().setEmailDataUser(mstUserMobile.getEmail());
					mstMobil.getData().setPhoneDataUser(mstUserMobile.getPhone());
					mstMobil.getData().setNameDataUser(fullname);

					// default set leads user to user mobile
					mstMobil.getData().setEmail(mstUserMobile.getEmail());
					mstMobil.getData().setPhone(mstUserMobile.getPhone());
					mstMobil.getData().setGender(mstUserMobile.getGender());
					mstMobil.getData().setName(fullname);

					String company = "";
					String leadsType = "";

					company = "TAF";
					leadsType = Constant.LEADS_TYPE_MOBIL;
					Locale localeCurrency = new Locale("id", "ID");
					Gson gson = new Gson();
					RestTemplate restTemplate = new RestTemplate();
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
					headers.set("User-Agent", "CaasReportAPI/1.0.0");

					MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
					map.add("client_id", oauthClientId);
					map.add("client_secret", oauthClientSecret);
					map.add("grant_type", oauthGrantType);

					HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
					ResponseEntity<AxwayTokenResponseDto> response1 = null;
					ParameterizedTypeReference<AxwayTokenResponseDto> type = new ParameterizedTypeReference<AxwayTokenResponseDto>() {
					};
					response1 = restTemplate.exchange(oauthUrl, HttpMethod.POST, entity, type);
					if(response1 != null && response1.getBody() !=null) {
						if(response1.getBody().getAccess_token() != null) {					
							String tokenTaf = response1.getBody().getAccess_token();
							LOG.info("TOKEN === " + tokenTaf);
							PengajuanCarTafReqDto mobilBaru = new PengajuanCarTafReqDto();				
							mobilBaru.setMainCvgTypeCode("ALL RISK");
							mobilBaru.setOfficeCode(mstMobil.getData().getOfficeCode());
							mobilBaru.setOfficeName(mstMobil.getData().getOfficeName());
							mobilBaru.setAssetHierarchyl1Code(mstMobil.getData().getBrandCode());
							mobilBaru.setAssetHierarchyl1Name(mstMobil.getData().getBrand());
							mobilBaru.setCreditType("Mobil Baru");
							if(mstMobil.getData().getPayment()!=null && "Pembayaran DP".equalsIgnoreCase(mstMobil.getData().getPayment())) {
								mobilBaru.setAppType("budget");
							}else if(mstMobil.getData().getPayment()!=null && "Installment".equalsIgnoreCase(mstMobil.getData().getPayment())) {
								mobilBaru.setAppType("credit");
							}
							mobilBaru.setLeadSource("MOXASYARIAH");
							mobilBaru.setAssetHierarchyl2Code(mstMobil.getData().getTypeCode());
							mobilBaru.setAssetHierarchyl2Name(mstMobil.getData().getType());
							mobilBaru.setAssetName(mstMobil.getData().getModel());
							mobilBaru.setAssetCode(mstMobil.getData().getModelCode());
							mobilBaru.setDownPaymentAmt(new Long(mstMobil.getData().getAmountDp()));
							mobilBaru.setDownPaymentPrcnt(new Long(mstMobil.getData().getDp()));
							mobilBaru.setYearNo(new SimpleDateFormat("yyyy").format(new Date()));
							mobilBaru.setPurposeOfUsageCode("");
							mobilBaru.setPurposeOfAssetUsage("");
							mobilBaru.setBudgetCalcType("");
							mobilBaru.setInscoBranchCode("");
							mobilBaru.setIsCapitalized("1");
							mobilBaru.setBudgetAmt(null);
							mobilBaru.setAssetPriceAmt(new Long(mstMobil.getData().getOtrPrice()));
							List<PengajuanCarTafCalculatorDetaildto> detail = mstMobil.getCalculatorResult().getDetail();
							if(detail.size()>0) {
								for(PengajuanCarTafCalculatorDetaildto detailData : detail) {
									if((detailData.getTENOR().longValue() == new Long(mstMobil.getData().getTenor())) && "AD".equals(detailData.getFIRST_INSTALLMENT_TYPE())) {
										mobilBaru.setMainPremiAmt(detailData.getMAIN_PREMI_AMT());
										mobilBaru.setNtfAmt(detailData.getNTF_AMT());
										mobilBaru.setOsInterestAmt(detailData.getOS_INTEREST_AMT());
										mobilBaru.setOsPrincipalAmt(detailData.getOS_PRINCIPAL_AMT());
										mobilBaru.setPolicyFee(detailData.getPOLICY_FEE());
										mobilBaru.setInstAmt(detailData.getINST_AMT());
										mobilBaru.setTenor(detailData.getTENOR());
										mobilBaru.setFirstInstType(detailData.getFIRST_INSTALLMENT_TYPE());
										mobilBaru.setGrossAmt(detailData.getGROSS_PREMIUM());
										mobilBaru.setSupplFlatRatePrcnt(new BigDecimal(detailData.getRATE_PUBLISH_PRCNT()));
										mobilBaru.setTotalDownPaymentGrossAmt(detailData.getTOTAL_DOWN_PAYMENT_GROSS_AMT());
										
										mstMobil.getData().setTotalPayment1(detailData.getTOTAL_DOWN_PAYMENT_GROSS_AMT().toString());
										
									}
								}
							}
							mobilBaru.setInscoBranchId(new Long(mstMobil.getCalculatorResult().getINSCO_BRANCH_ID()));
							mobilBaru.setCustName(mstMobil.getData().getNameDataUser());
							mobilBaru.setCustType("P");
							mobilBaru.setMobilePhone1(mstMobil.getData().getPhoneDataUser().replace("+62", "0"));
							mobilBaru.setPromoCode("");
							mobilBaru.setReferralCode("");
							Calendar c = Calendar.getInstance();
							c.setTime(new Date());
							c.add(Calendar.DATE, 1);
							mobilBaru.setFollowUpDate(new SimpleDateFormat("yyyy-MM-dd HH:mm", localeCurrency).format(c.getTime()));
							mobilBaru.setStatus("");
							if(mstUserMobile.getAddressKtp() == null || "".equals(mstUserMobile.getAddressKtp())) {
								mobilBaru.setResidenceAddr("-");
							}else {
								mobilBaru.setResidenceAddr(mstUserMobile.getAddressKtp());
							}
							List<PengajuanCarTafDocumentReqDto> priceexp = new ArrayList<>();
							PengajuanCarTafDocumentReqDto a = new PengajuanCarTafDocumentReqDto();
								a.setFileName("");
								a.setFileCode("");
								a.setFileBase64("");
							priceexp.add(a);
							mobilBaru.setDocument(priceexp);
							String json = gson.toJson(mobilBaru);
							LOG.info("ini request" + json);
							headers.setContentType(MediaType.APPLICATION_JSON);
							headers.set("Authorization", "Bearer "+ tokenTaf);														
							HttpEntity<String> entitys = new HttpEntity<>(json, headers);
							ResponseEntity<ResponseNewCarTAFDto> response2 = null;
							ParameterizedTypeReference<ResponseNewCarTAFDto> types = new ParameterizedTypeReference<ResponseNewCarTAFDto>() {
							};
							response2 = restTemplate.exchange(
									axwayApiUrl + "/taf/flex/api/v1/external/submit-app/",
									HttpMethod.POST, entitys, types);
							if (response2 != null && response2.getBody() != null) {
								LOG.info(response2.getBody().getStatus());
								if (response2.getBody().getStatus() != null && "200".equals(response2.getBody().getStatus())) {		
									mstMobil.getData().setIntegrationLeadsId(response2.getBody().getData().getRequestNo());
									LOG.info("aaa123");

									MstPengajuanCar pengajuanCarBaru = pengajuanService.save(
											mstMobil.getData());

									String json1 = gson.toJson(pengajuanCarBaru);
									LOG.info("ini request mabroor" + json1);
									MstLeadsRule mstLeadsRule = new MstLeadsRule();

									mstLeadsRule.setUserId(mstUserMobile.getId());
									mstLeadsRule.setLeadsId(pengajuanCarBaru.getId());
									mstLeadsRule.setActionStatus("NEW");
									mstLeadsRule.setLeadsType(leadsType);
									mstLeadsRule.setCompany(company);
									mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
									leadsRuleServ.save(mstLeadsRule);
									
									MstApplication newApp = new MstApplication();
									newApp.setLeadsId(pengajuanCarBaru.getId());
									newApp.setCompany("TAF");
									newApp.setLeadsType(Constant.LEADS_TYPE_MOBIL);
									newApp.setPickupBu(null);
									newApp.setStatus("Inprogress");
									newApp.setUserId(mstUserMobile.getId());
									MstApplication newApp1 = appService.save(newApp);


									MstApplicationTransaction trxApp = new MstApplicationTransaction();
									trxApp.setApplicationId(newApp1.getId());													
									trxApp.setStatus("Inprogress");													
									trxApp.setDetail(null);
									trxApp.setTransactionDate(new Date());
									trxApp.setBatchUuid("" + newApp1.getId());
									appTrxService.save(trxApp);
									
									Optional<MstUserMobile> userMobile = userMobileService.findById(pengajuanCarBaru.getUserId());
									if (userMobile.isPresent()) {
										MstUserMobile temporaryUserMobile = userMobile.get();
										if (temporaryUserMobile.getFirebaseToken() != null) {

											MstInbox inbox = new MstInbox(); 
											Long id = new Long(3);
											MstInboxCategory category = inboxCategoryServ.findById(id);
											inbox.setTitle("Status pengajuan mobil");
											inbox.setDescription("Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan mobil " + pengajuanCarBaru.getBrand() + " " + pengajuanCarBaru.getType() + " " + pengajuanCarBaru.getModel() + " yang kamu lakukan. Klik di sini untuk melihat status terkini!");
										    inbox.setIdInboxCategory(category);
										    inbox.setScreenName("NewCarDetail");
											inbox.setInboxType("DIRECT_SCREEN");
											inbox.setIcon("inbox_pengajuan.png");
											inbox.setInboxRead("NO");
											inbox.setUserId(temporaryUserMobile.getId());
											inbox.setIdData(pengajuanCarBaru.getId());
											MstInbox inbx = inboxServ.save(inbox);

											PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
											LOG.info(temporaryUserMobile.getFirebaseToken());
											pushNotificationReq.setToken(temporaryUserMobile.getFirebaseToken());
											pushNotificationReq
											.setTitle("Status pengajuan mobil baru");

											Map<String, String> pushData = new HashMap<String, String>();

											Map<String, Object> jsonPush = new HashMap<String, Object>();
											jsonPush.put("screen", "NewCarDetail");


											Map<String, Object> params = new HashMap<String, Object>();
											params.put("id", pengajuanCarBaru.getId());
											params.put("pageType", "");
											params.put("idInbox", inbx.getId());
											

											jsonPush.put("params", params);

											String jsonified = new Gson().toJson(jsonPush);
											pushData.put("json", jsonified);

											pushNotificationReq.setData(pushData);

											pushNotificationReq.setMessage(
											"Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan mobil " + pengajuanCarBaru.getBrand() + " " + pengajuanCarBaru.getType() + " " + pengajuanCarBaru.getModel() + " yang kamu lakukan. Klik di sini untuk melihat status terkini!");
												
											try {
												pushNotificationService.sendMessageToToken(pushNotificationReq);

											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} catch (ExecutionException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											
										} else {
											LOG.info("Firebase token not found for user " + temporaryUserMobile.getName() + " - "
													+ temporaryUserMobile.getPhone());
										}
									}

									if (!mstUserMobile.getEmail().equals(pengajuanCarBaru.getEmail())) {
										mailClient.prepareAndSendPengajuanMobilUserMobile(
												mstUserMobile.getEmail(), "Pengajuan Mobil", pengajuanCarBaru);
									}
									// send to leads user
									mailClient.prepareAndSendPengajuanMobilUserLeads(
											pengajuanCarBaru.getEmail(), "Pengajuan Mobil", pengajuanCarBaru);

									response.put("status", "success");
									response.put("description", "Success! Leads Jaminan Data successfully submitted.");
									httpStatus = HttpStatus.OK;
								}else {
									response.put("status", "failed");
									response.put("description",
											"Gagal melakukan submit data. Silakan coba beberapa saat lagi (1).");
									httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
								}
							}else {
								response.put("status", "failed");
								response.put("description",
										"Gagal melakukan submit data. Silakan coba beberapa saat lagi (2).");
								httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
							}
						}
					}


				} else {
					response.put("status", "failed");
					response.put("description",
							"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}
			}
		}
		return new ResponseEntity<>(response, httpStatus);
	}
}
