package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.PromotionCtaButtonDto;
import com.mabroor.ws.entity.MstApplication;
import com.mabroor.ws.entity.MstPromotion;
import com.mabroor.ws.entity.MstPromotionSetting;
import com.mabroor.ws.service.PromotionService;
import com.mabroor.ws.service.PromotionSettingService;
import com.mabroor.ws.util.Constant;

@Transactional
@RestController
@RequestMapping("/v1/api/promotion")
public class PromotionController {


	@Autowired
	private PromotionService promotionServ;
	
	@Autowired
	private PromotionSettingService promotionSetServ;

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@GetMapping(value = "/list")
	@ResponseBody
	public ResponseEntity getInboxCategory(@RequestHeader(value = "appToken", required = false) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = HttpStatus.OK;
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", promotionServ.getListPromotion());
		return new ResponseEntity<>(response, httpStatus);
	}
	
	@GetMapping(value = "/setting")
	@ResponseBody
	public Map<String, Object> getSetting() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		List<MstPromotionSetting> promo = promotionSetServ.getListPromotionSetting();
		List<MstPromotion> promoss = new ArrayList<>();
		
		for (MstPromotionSetting mstPromotionSetting : promo) {
			em.detach(mstPromotionSetting.getIdPromo());
			mstPromotionSetting.getIdPromo().setBanner(this.imageBaseUrl + "promotion/" + mstPromotionSetting.getIdPromo().getBanner());
			promoss.add(mstPromotionSetting.getIdPromo());
		}
		
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", promoss);
		return response;
	}

	@GetMapping(value = "/mainProduct/{idMainProduct}")
	@ResponseBody
	public Map<String, Object> getListPromotionByMainProduct(@RequestHeader(value = "appToken", required = false) String token, @PathVariable Long idMainProduct) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		List<MstPromotion> promo = promotionServ.getListPromotionByMainProduct(idMainProduct);
		List<MstPromotion> promoss = new ArrayList<>();
		
		for (MstPromotion mstPromo : promo) {
			em.detach(mstPromo);
			mstPromo.setBanner(this.imageBaseUrl + "promotion/" + mstPromo.getBanner());
			promoss.add(mstPromo);
		}
		
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", promoss);

		return response;
	}
	
	@GetMapping(value = "/detail/{id}")
	@ResponseBody
	public Map<String, Object> getPromotionByDetailPromo(@PathVariable Long id) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();

		MstPromotion mstPromo = promotionServ.getPromotionByDetailPromo(id);
		Date date1 = mstPromo.getEndDate();
		Date date2 = new Date();

		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);

		if (cal1.after(cal2)) {
			response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	

			Map<String, Object> map = new HashMap<>();
			map.put("id", mstPromo.getId());
			map.put("title", mstPromo.getTitle());
			map.put("bestDeal", mstPromo.getBestDeal());
			map.put("description", mstPromo.getDescription());
			map.put("idMainProduct", mstPromo.getIdMainProduct());
			map.put("idMainProductChild", mstPromo.getIdMainProductChild());
			map.put("banner", this.imageBaseUrl + "promotion/" + mstPromo.getBanner());
			map.put("promoCode", mstPromo.getPromoCode());
			map.put("startDate", mstPromo.getStartDate());
			map.put("endDate", mstPromo.getEndDate());
			map.put("termsAndConditions", mstPromo.getTermsAndConditions());
			map.put("isActivePromo", mstPromo.getIsActivePromo());
			map.put("directLink", mstPromo.getDirectLink());
			map.put("ctaButton", mstPromo.getCtaButton());
			map.put("isActiveHomepage", mstPromo.getIsActiveHomepage());
			map.put("isActiveHomepageLandingPage", mstPromo.getIsActiveHomepageLandingPage());
			map.put("promotionStatus", mstPromo.getPromotionStatus());
			map.put("createdDate", mstPromo.getCreatedDate());
			map.put("createdBy", mstPromo.getCreatedBy());
			map.put("updatedDate", mstPromo.getUpdatedDate());
			map.put("updatedBy", mstPromo.getUpdatedBy());
			
			if(new Long(10).equals(mstPromo.getIdMainProduct().getId())) {
				try {
				    Thread.sleep(1500);
				} catch (InterruptedException ie) {
				    Thread.currentThread().interrupt();
				}
			}

			response.put("data", map);
		} else if (cal1.before(cal2)) {
			response.put("status", "failed");
			response.put("description", "Ups! Promo telah berakhir. Kamu dapat memilih promo lainnya");
		}

		return response;
	}
	
	@PostMapping(value = "/cta/button")
	@ResponseBody
	public Map<String, Object> adjustCta(@RequestBody PromotionCtaButtonDto mstPromos) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();

		
			Optional<MstPromotion> mstPromoss = promotionServ.findById(mstPromos.getId());
			if (mstPromoss != null && mstPromoss.get().getId().equals(mstPromos.getId())) {
				MstPromotion app = promotionServ.getPromotionByDetailPromo(mstPromoss.get().getId());
				app.setCtaButton(mstPromos.getCtaButton());
				promotionServ.save(app);
				response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
				response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
				}else {
					response.put("status", "failed");
					response.put("message", "Failed to submit data.");
				}
			
		
		
		
		
		return response;
	}

}
