package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.crypto.IllegalBlockSizeException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.entity.MstUserMobileMenu;
import com.mabroor.ws.entity.MstUserMobileMenuCategory;
import com.mabroor.ws.entity.MstUserMobileMenuLayout;
import com.mabroor.ws.service.UserMobileMenuCategoryService;
import com.mabroor.ws.service.UserMobileMenuLayoutService;
import com.mabroor.ws.service.UserMobileMenuService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.util.JWTGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/menu")
public class UserMobileMenuController {

	private static final Logger LOG = Logger.getLogger(UserMobileController.class);

	@Autowired
	private UserMobileMenuCategoryService categoryServ;

	@Autowired
	private UserMobileMenuLayoutService layoutServ;

	@Autowired
	private UserMobileMenuService menuServ;
	
	@Autowired
	private UserMobileService userMobileServ;

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@GetMapping(value = "/list")
	@ResponseBody
	public ResponseEntity getListMenu(@RequestHeader(value = "appToken", required = false) String token) {
		System.out.println(token);

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;
		Map<String, Object> map2 = new HashMap<>();

		List<MstUserMobileMenuCategory> objMenuCategory = categoryServ.getList();
		List<Map<String, Object>> menuCategory = new ArrayList<>();
		if (objMenuCategory != null && objMenuCategory.size() > 0) {
			for (MstUserMobileMenuCategory b : objMenuCategory) {
				em.detach(b);
				Map<String, Object> map = new HashMap<>();
				map.put("id_category", b.getId());
				map.put("category_name", b.getCategoryName());

				List<MstUserMobileMenu> userMenu = menuServ.getListMenu(b.getId());
				for (MstUserMobileMenu a : userMenu) {
					em.detach(a);
					a.setIcon(this.imageBaseUrl + "mabroor_menu_icon/" + a.getIcon());
				}
				map.put("menu_list", userMenu);
				menuCategory.add(map);
			}
		}
		map2.put("menu_category", menuCategory);

		List<MstUserMobileMenu> objDefaultMenu = menuServ.getDefaultMenu();
		for (MstUserMobileMenu c : objDefaultMenu) {
			em.detach(c);
			c.setIcon(this.imageBaseUrl + "mabroor_menu_icon/" + c.getIcon());
		}
		map2.put("default_menu", objDefaultMenu);
		
		List<MstUserMobileMenu> objMenu = menuServ.getFeaturedMenu();
		for(MstUserMobileMenu f : objMenu) {
			em.detach(f);
			f.setIcon(this.imageBaseUrl + "mabroor_menu_icon/" + f.getIcon());
		}
		map2.put("featured_menu", objMenu);
//		Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(profileId);
//		MstUserMobile mstUserMobile = null;
//		if (!mstUserMobileOpt.isPresent()) {
//			response.put("status", "failed");
//			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
//			httpStatus = HttpStatus.UNAUTHORIZED;
//		} else {
//			mstUserMobile = mstUserMobileOpt.get();
//			
		httpStatus = HttpStatus.OK;
		if (token != null && !"".equals(token)) {
			try {
				Claims claims = JWTGenerator.getInstance().decodeJWT(token);
				if (Objects.isNull(claims)) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
//					if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){
					Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

					List<MstUserMobileMenuLayout> menuLayout = layoutServ.findAllByUserId(new Long(claims.getId()));
					if(menuLayout.size() > 0) {
						for (MstUserMobileMenuLayout d : menuLayout) {
							em.detach(d);
							d.getIdMenu().setIcon(this.imageBaseUrl + "mabroor_menu_icon/" + d.getIdMenu().getIcon());
						}
						map2.put("saved_menu", menuLayout);
					}else {
						MstUserMobile mstUserMobile = mstUserMobileOpt.get();
						for (MstUserMobileMenu c : objDefaultMenu) {
							em.detach(c);
							MstUserMobileMenuLayout tmpMenuLayout = new MstUserMobileMenuLayout();
							tmpMenuLayout.setUserId(mstUserMobile.getId());
							tmpMenuLayout.setIdMenu(c);
							tmpMenuLayout.setIndex(c.getId().toString());
							menuLayout.add(tmpMenuLayout);
						}
						map2.put("saved_menu", menuLayout);
					}

					response.put("status", "success");
					response.put("description", "succesfully get data");
					httpStatus = HttpStatus.OK;
//					} else {
//						response.put("status", "failed");
//						response.put("description", "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
//						response.put("data",null);
//						httpStatus = HttpStatus.UNAUTHORIZED;
				}
//				}
			} catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch(IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} catch (NullPointerException np) {
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (RuntimeException re) {
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description",
						"Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} catch (OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}
		response.put("data", map2);
		return new ResponseEntity<>(response, httpStatus);

	}
}