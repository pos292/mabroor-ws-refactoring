package com.mabroor.ws.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.entity.MstInboxCategory;
import com.mabroor.ws.entity.MstLeadsRule;
import com.mabroor.ws.entity.MstPengajuanGoldFinancing;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.service.InboxCategoryService;
import com.mabroor.ws.service.InboxService;
import com.mabroor.ws.service.LeadsRuleService;
import com.mabroor.ws.service.PengajuanGoldFinancingService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.service.SysParamService;
import com.mabroor.ws.entity.SysParam;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.util.AES256Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/pengajuanGoldFinancing")
public class PengajuanGoldFinancingController {
	
	private static final Logger LOG = Logger.getLogger( PengajuanGoldFinancingController.class);
	
	@Autowired
	private UserMobileService userMobileServ;
	
	@Autowired
	private LeadsRuleService leadsRuleServ;
	
	@Autowired
	private SysParamService sysParamService;
	
	@Autowired
	private PushNotificationService pushNotificationService;
	
	@Autowired
	private InboxCategoryService inboxCategoryServ;
	
	@Autowired
	private InboxService inboxServ;

	
	@Autowired
	private PengajuanGoldFinancingService goldServ;

	@PostMapping
	@ResponseBody
	public ResponseEntity save(@RequestBody MstPengajuanGoldFinancing mstPengajuan, @RequestHeader(value = "appToken", required = true) String token) {
		LOG.info(mstPengajuan.getName());
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

	
		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");				
				httpStatus = HttpStatus.UNAUTHORIZED;
			}else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				MstUserMobile mstUserMobile = null;
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");					
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					mstUserMobile = mstUserMobileOpt.get();					
					if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){

						
						String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
						String lastname = mstUserMobile.getLastName();
						String fullname = firstName;
						if(!"".equals(lastname)) {
							fullname += " " + lastname;
						}else {
							fullname = mstUserMobile.getName();
						}

						mstPengajuan.setGoldFinancingStatus("01");
						mstPengajuan.setUserId(mstUserMobile.getId());
						if(mstPengajuan.getEmailDataUser() == null || "".equals(mstPengajuan.getEmailDataUser())) {
							mstPengajuan.setEmailDataUser(mstUserMobile.getEmail());
						}

						if(mstPengajuan.getPhoneDataUser() == null || "".equals(mstPengajuan.getPhoneDataUser())) {
							mstPengajuan.setPhoneDataUser(mstUserMobile.getPhone());
						}

						mstPengajuan.setNameDataUser(fullname);			
						mstPengajuan.setTransactionDateTime(new Date());
						
						SysParam tmpParam = sysParamService.getByValueType("max_submit_gold");
						Integer maxSubmit = new Integer (tmpParam.getValueName());
						List<MstPengajuanGoldFinancing> gold = goldServ.findByPhone(AES256Utils.encrypt(mstPengajuan.getPhoneDataUser()));
						boolean isExisting = false;
						if (gold != null && gold.size() >= maxSubmit) {
							mstPengajuan.setId(gold.get(0).getId());
							mstPengajuan.setTransactionDateTime(new Date());
							isExisting = true;
						}
						
						MstPengajuanGoldFinancing goldnew = goldServ.save(mstPengajuan);
						
						if(!isExisting) {
						MstLeadsRule mstLeadsRule = new MstLeadsRule();
						mstLeadsRule.setUserId(mstUserMobile.getId());
						mstLeadsRule.setLeadsId(goldnew.getId());
						mstLeadsRule.setActionStatus("NEW");
						mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
						mstLeadsRule.setLeadsType(Constant.LEADS_TYPE_GOLD_FINANCE);
						mstLeadsRule.setCompany("FIF Amitra");
						leadsRuleServ.save(mstLeadsRule);				
						}
						
						Optional<MstUserMobile> userMobile = userMobileServ.findById(goldnew.getUserId());
						if (userMobile.isPresent()) {
							MstUserMobile temporaryUserMobile = userMobile.get();
							if (temporaryUserMobile.getFirebaseToken() != null) {

								MstInbox inbox = new MstInbox(); 
								Long id = new Long(3);
								MstInboxCategory category = inboxCategoryServ.findById(id);
								inbox.setTitle("Status pengajuan gold finance");
								inbox.setDescription("Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan gold finance yang kamu lakukan. Klik di sini untuk melihat status terkini!");
								inbox.setIdInboxCategory(category);
								inbox.setScreenName("NewGoldFinanceDetail");
								inbox.setInboxType("DIRECT_SCREEN");
								inbox.setIcon("inbox_pengajuan.png");
								inbox.setInboxRead("NO");
								inbox.setUserId(temporaryUserMobile.getId());
								inbox.setIdData(goldnew.getId());
								MstInbox inbx = inboxServ.save(inbox);

								PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
								LOG.info(temporaryUserMobile.getFirebaseToken());
								pushNotificationReq.setToken(temporaryUserMobile.getFirebaseToken());
								pushNotificationReq
								.setTitle("Status pengajuan gold finance");

								Map<String, String> pushData = new HashMap<String, String>();

								Map<String, Object> jsonPush = new HashMap<String, Object>();
								jsonPush.put("screen", "NewGoldFinanceDetail");


								Map<String, Object> params = new HashMap<String, Object>();
								params.put("id", goldnew.getId());
								params.put("pageType", "");
								params.put("idInbox", inbx.getId());

								jsonPush.put("params", params);

								String jsonified = new Gson().toJson(jsonPush);
								pushData.put("json", jsonified);

								pushNotificationReq.setData(pushData);

								pushNotificationReq.setMessage(
										"Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan gold finance yang kamu lakukan. Klik di sini untuk melihat status terkini!");

								try {
									pushNotificationService.sendMessageToToken(pushNotificationReq);

								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (ExecutionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							} else {
								LOG.info("Firebase token not found for user " + temporaryUserMobile.getName() + " - "
										+ temporaryUserMobile.getPhone());
							}
						}
						
						response.put("status", "success");
						response.put("description", "Success! Data successfully submitted.");					
						httpStatus = HttpStatus.OK;
					}else {
						response.put("status", "failed");
						response.put("description", "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");						
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}	
			}
		}catch(MalformedJwtException mje)
		{
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");			
			httpStatus = HttpStatus.UNAUTHORIZED;
		}catch(IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");			
			httpStatus = HttpStatus.UNAUTHORIZED;
		}catch(NullPointerException np)
		{
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");			
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch(StackOverflowError st)
		{
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");			
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch(RuntimeException re)
		{
			re.printStackTrace();
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");		
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch(OutOfMemoryError oe)
		{
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");			
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response,httpStatus);
	}
}
