package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.DealerLocationService;

@RestController
@RequestMapping("/v1/api/dealerlocation")
public class DealerLocationController {

	@Autowired
	private DealerLocationService dealerService;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",dealerService.getListDealer());
		return response;
	}
	
	@GetMapping(value = "/{dealerId}")
	public LinkedHashMap<String, Object> getDealerById(@PathVariable Long dealerId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",dealerService.findById(dealerId));
		return response;
	}
	
}
