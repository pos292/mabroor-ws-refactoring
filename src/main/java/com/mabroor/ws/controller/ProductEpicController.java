package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.MstProductEpicDto;


	@RestController
	@RequestMapping("/v1/api/productepic")
	public class ProductEpicController {
		


		@GetMapping(value ="/list")
		@ResponseBody
		public Map<String, Object> getList() {			
			LinkedHashMap<String, Object> response = new LinkedHashMap<>();
			List<MstProductEpicDto> epicsbaru = new ArrayList<>();
			
			
				MstProductEpicDto epic = new MstProductEpicDto();	
				epic.setId(new Long("2"));
				epic.setTitle("Pertanyaan Umum");
				epicsbaru.add(epic);
				epic = new MstProductEpicDto();	
				epic.setId(new Long("1"));
				epic.setTitle("Promo");
	     		epicsbaru.add(epic);
		
			
			response.put("status", "success");
			response.put("description", "succesfully get data");
			response.put("data",epicsbaru);
			return response;
		}

}
