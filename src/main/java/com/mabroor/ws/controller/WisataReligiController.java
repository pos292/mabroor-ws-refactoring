package com.mabroor.ws.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.mabroor.ws.entity.MstApplicationTransaction;
import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.entity.MstInboxCategory;
import com.mabroor.ws.entity.MstLeadsRule;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.entity.MstWisataReligi;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.service.ApplicationService;
import com.mabroor.ws.service.ApplicationTransactionService;
import com.mabroor.ws.service.InboxCategoryService;
import com.mabroor.ws.service.InboxService;
import com.mabroor.ws.service.LeadsRuleService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.service.WisataReligiService;
import com.mabroor.ws.service.impl.MailClient;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.entity.MstApplication;
import com.mabroor.ws.dto.AxwayTokenResponseDto;
import com.mabroor.ws.dto.CallbackStatusbackHajiDto;
import com.mabroor.ws.dto.FIFDataDto;
import com.mabroor.ws.dto.FIFTokenDto;
import com.mabroor.ws.dto.FIFWisataReligiResultDto;
import com.mabroor.ws.dto.FifTokenRequestDto;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/wisatareligi")
public class WisataReligiController {

	private static final Logger LOG = Logger.getLogger(WisataReligiController.class);

	@Autowired
	private WisataReligiService wisataService;

	@Autowired
	private LeadsRuleService leadsRuleServ;
	
	@Autowired
	private PushNotificationService pushNotificationService;
	
	@Autowired
	private InboxCategoryService inboxCategoryServ;
	
	@Autowired
	private InboxService inboxServ;

	@Autowired
	private UserMobileService userMobileServ;

	@Autowired
	private ApplicationService appService;

	@Autowired
	private ApplicationTransactionService appTrxService;

	@Autowired
	private MailClient mailClient;

	@Value("${axway.api.url}")
	private String axwayApiUrl;

	@Value("${axway.api.key}")
	private String axwayApiKey;

	@Value("${axway.oauth.url}")
	private String oauthUrl;

	@Value("${axway.oauth.clientid}")
	private String oauthClientId;

	@Value("${axway.oauth.clientsecret}")
	private String oauthClientSecret;

	@Value("${axway.oauth.granttype}")
	private String oauthGrantType;

	@Value("${fif.userkey}")
	private String fifUserKey;

	@PostMapping
	@ResponseBody
	public ResponseEntity save(@RequestBody MstWisataReligi mstWisata,
			@RequestHeader(value = "appToken", required = true) String token) throws ParseException {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				MstUserMobile mstUserMobile = null;
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
						String lastname = mstUserMobile.getLastName() != null ? mstUserMobile.getLastName() : "";
						String fullname = firstName;
						if (!"".equals(lastname)) {
							fullname += " " + lastname;
						}

						mstWisata.setWisataStatus("01");
						mstWisata.setUserId(mstUserMobile.getId());
						mstWisata.setEmailDataUser(mstUserMobile.getEmail());
						mstWisata.setPhoneDataUser(mstUserMobile.getPhone());
						mstWisata.setNameDataUser(fullname);
						mstWisata.setNama(fullname);
						mstWisata.setTransactionDateTime(new Date());

						MstWisataReligi haji = wisataService.save(mstWisata);

						MstLeadsRule mstLeadsRule = new MstLeadsRule();
						mstLeadsRule.setUserId(mstUserMobile.getId());
						mstLeadsRule.setLeadsId(haji.getId());
						mstLeadsRule.setActionStatus("NEW");
						mstLeadsRule.setCompany("FIF Amitra");
						mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
						mstLeadsRule.setLeadsType("HAJI");
						leadsRuleServ.save(mstLeadsRule);

						if (!mstUserMobile.getEmail().equals(haji.getEmail())) {
							mailClient.prepareAndSendPengajuanHajiUserMobile(mstUserMobile.getEmail(),
									"Pengajuan Pembiayaan Haji", haji);
							System.out.println("masuk email");
						}
						// send to leads user
						mailClient.prepareAndSendPengajuanHajiUserLeads(haji.getEmail(), "Pengajuan Pembiayaan Haji",
								haji);
						System.out.println("masuk email leads");

						response.put("status", "success");
						response.put("description", "Success! Wisata Religi successfully submitted.");
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			re.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/acc")
	@ResponseBody
	public ResponseEntity<?> saveAcc(@RequestBody MstWisataReligi mstWisata,
			@RequestHeader(value = "appToken", required = true) String token) throws ParseException {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				MstUserMobile mstUserMobile = null;
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
						String lastname = mstUserMobile.getLastName() != null ? mstUserMobile.getLastName() : "";
						String fullname = firstName;
						if (!"".equals(lastname)) {
							fullname += " " + lastname;
						}

						mstWisata.setProductChoose("Pembiayaan Haji");
						mstWisata.setPartnerPembiayaan("ACC Syariah");
						mstWisata.setWisataStatus("01");
						mstWisata.setUserId(mstUserMobile.getId());
						mstWisata.setEmailDataUser(mstUserMobile.getEmail());
						mstWisata.setPhoneDataUser(mstUserMobile.getPhone());
						mstWisata.setNameDataUser(fullname);
						mstWisata.setNama(fullname);
						mstWisata.setTransactionDateTime(new Date());

						MstWisataReligi haji = wisataService.save(mstWisata);

						MstLeadsRule mstLeadsRule = new MstLeadsRule();
						mstLeadsRule.setUserId(mstUserMobile.getId());
						mstLeadsRule.setLeadsId(haji.getId());
						mstLeadsRule.setActionStatus("NEW");
						mstLeadsRule.setCompany("ACC");
						mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
						mstLeadsRule.setLeadsType("HAJI");
						leadsRuleServ.save(mstLeadsRule);
						
						Optional<MstUserMobile> userMobile = userMobileServ.findById(haji.getUserId());
						if (userMobile.isPresent()) {
							MstUserMobile temporaryUserMobile = userMobile.get();
							if (temporaryUserMobile.getFirebaseToken() != null) {

								MstInbox inbox = new MstInbox(); 
								Long id = new Long(3);
								MstInboxCategory category = inboxCategoryServ.findById(id);
								inbox.setTitle("Status pengajuan perjalanan religi haji ACC");
								inbox.setDescription("Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan " + haji.getProductPurchase() + " yang kamu lakukan. Klik di sini untuk melihat status terkini!");
								inbox.setIdInboxCategory(category);
								inbox.setScreenName("HajiDetail");
								inbox.setInboxType("DIRECT_SCREEN");
								inbox.setIcon("inbox_pengajuan.png");
								inbox.setInboxRead("NO");
								inbox.setUserId(temporaryUserMobile.getId());
								inbox.setIdData(haji.getId());
								MstInbox inbx = inboxServ.save(inbox);

								PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
								LOG.info(temporaryUserMobile.getFirebaseToken());
								pushNotificationReq.setToken(temporaryUserMobile.getFirebaseToken());
								pushNotificationReq
								.setTitle("Status pengajuan perjalanan religi haji ACC");

								Map<String, String> pushData = new HashMap<String, String>();

								Map<String, Object> jsonPush = new HashMap<String, Object>();
								jsonPush.put("screen", "HajiDetail");


								Map<String, Object> params = new HashMap<String, Object>();
								params.put("id", haji.getId());
								params.put("pageType", "");
								params.put("idInbox", inbx.getId());

								jsonPush.put("params", params);

								String jsonified = new Gson().toJson(jsonPush);
								pushData.put("json", jsonified);

								pushNotificationReq.setData(pushData);

								pushNotificationReq.setMessage(
										"Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan " + haji.getProductPurchase() + " yang kamu lakukan. Klik di sini untuk melihat status terkini!");

								try {
									pushNotificationService.sendMessageToToken(pushNotificationReq);

								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (ExecutionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							} else {
								LOG.info("Firebase token not found for user " + temporaryUserMobile.getName() + " - "
										+ temporaryUserMobile.getPhone());
							}
						}
						
						// MstApplication newApp = new MstApplication();
						// newApp.setLeadsId(haji.getId());
						// newApp.setCompany("ACC");
						// newApp.setLeadsType("HAJI");
						// newApp.setPickupBu(null);
						// newApp.setStatus("Inprogress");
						// newApp.setUserId(mstUserMobile.getId());
						// MstApplication newApp1 = appService.save(newApp);

						// MstApplicationTransaction trxApp = new MstApplicationTransaction();
						// trxApp.setApplicationId(newApp1.getId());
						// trxApp.setStatus("Inprogress");
						// trxApp.setDetail(null);
						// trxApp.setTransactionDate(new Date());
						// trxApp.setBatchUuid("" + newApp1.getId());
						// appTrxService.save(trxApp);

						if (!mstUserMobile.getEmail().equals(haji.getEmail())) {
							mailClient.prepareAndSendPengajuanHajiUserMobile(mstUserMobile.getEmail(),
									"Pengajuan Pembiayaan Haji", haji);
							System.out.println("masuk email");
						}
						// send to leads user
						mailClient.prepareAndSendPengajuanHajiUserLeads(haji.getEmail(), "Pengajuan Pembiayaan Haji",
								haji);
						System.out.println("masuk email leads");

						response.put("status", "success");
						response.put("description", "Success! Wisata Religi successfully submitted.");
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			re.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/submitfif")
	@ResponseBody
	public ResponseEntity submitfif(@RequestBody MstWisataReligi mstWisata,
			@RequestHeader(value = "appToken", required = true) String token) throws IllegalBlockSizeException {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				MstUserMobile mstUserMobile = null;
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
						String lastname = mstUserMobile.getLastName() != null ? mstUserMobile.getLastName() : "";
						String fullname = firstName;
						if (!"".equals(lastname)) {
							fullname += " " + lastname;
						} else {
							fullname = mstUserMobile.getName();
							mstWisata.setNamaDepan(fullname);
						}
						if (!"".equals(firstName)) {
							mstWisata.setNamaDepan(firstName);
							mstWisata.setNamaBelakang(lastname);
						}
						mstWisata.setWisataStatus("01");
						mstWisata.setUserId(mstUserMobile.getId());
						mstWisata.setPartnerPembiayaan("AMITRA");
						mstWisata.setEmailDataUser(mstUserMobile.getEmail());
						mstWisata.setPhoneDataUser(mstUserMobile.getPhone());
						mstWisata.setNameDataUser(fullname);
						if(mstUserMobile.getGender() != null && !"".equalsIgnoreCase(mstUserMobile.getGender())) {
							mstWisata.setGender(mstUserMobile.getGender());								
						}
						// default set leads user to user mobile
						mstWisata.setEmail(mstUserMobile.getEmail());
						mstWisata.setNoHandphone(mstUserMobile.getPhone());
						mstWisata.setNama(fullname);

						Locale localeCurrency = new Locale("id", "ID");

						if (mstWisata.getTanggalLahir() == null || "".equals(mstWisata.getTanggalLahir())) {
							mstWisata.setTanggalLahir(new SimpleDateFormat("dd MMMM yyyy", localeCurrency)
									.format(mstUserMobile.getBirthDate()));
						}

						if (mstWisata.getAlamatSesuaiKtp() == null || "".equals(mstWisata.getAlamatSesuaiKtp())) {
							mstWisata.setAlamatSesuaiKtp(mstUserMobile.getAddressKtp());
						}

						if (mstWisata.getAlamatTempatTinggal() == null
								|| "".equals(mstWisata.getAlamatTempatTinggal())) {
							mstWisata.setAlamatTempatTinggal(mstUserMobile.getAddress());
						}
						
							Gson gson = new Gson();
							RestTemplate restTemplate = new RestTemplate();
							HttpHeaders headers = new HttpHeaders();

							headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
							headers.set("User-Agent", "CaasReportAPI/1.0.0");

							MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
							map.add("client_id", oauthClientId);
							map.add("client_secret", oauthClientSecret);
							map.add("grant_type", oauthGrantType);


									FIFWisataReligiResultDto hajiFIF = new FIFWisataReligiResultDto();

									hajiFIF.setCallVia("");
									hajiFIF.setContactDetail("");									
									hajiFIF.setProdukPembiayaan(mstWisata.getProductChoose());
									hajiFIF.setTenor(mstWisata.getTenor());
									hajiFIF.setDp(mstWisata.getAmountDp());
									hajiFIF.setPaketCode(mstWisata.getProductPurchaseCode());
									hajiFIF.setNamaProdukPembiayaan(mstWisata.getProductPurchase());																
									hajiFIF.setFullname(mstWisata.getNama());
									if(mstUserMobile.getKtpNumber() != null && !"".equalsIgnoreCase(mstUserMobile.getKtpNumber())) {
										hajiFIF.setNoKTP(mstUserMobile.getKtpNumber());
									}else {
										hajiFIF.setNoKTP(mstWisata.getNoKtp());
									}									
									String mobilephone1 = mstWisata.getPhoneDataUser().replaceAll("\\+62", "0");
//								System.out.println("phoneee = " + AES256Utils.encrypt("+62899999999"));
									hajiFIF.setMobilePhone1(mobilephone1);
									String mobilephone = mstWisata.getNoHandphone().replaceAll("\\+62", "0");
//								System.out.println("phoneee 222 = " + mobilephone);
									hajiFIF.setMobilePhone2(mobilephone);
									hajiFIF.setEmail(mstWisata.getEmail());
									hajiFIF.setBirthdate(mstWisata.getTanggalLahir());
									hajiFIF.setBirthPlace(mstWisata.getKota());
									if(mstUserMobile.getGender() != null && !"".equalsIgnoreCase(mstUserMobile.getGender())) {											
										if ("Laki-laki".equalsIgnoreCase(mstUserMobile.getGender())) {
											hajiFIF.setGender("M");
										} else if ("Perempuan".equalsIgnoreCase(mstUserMobile.getGender())) {
											hajiFIF.setGender("F");
										}
									}else {
										if ("Laki-laki".equalsIgnoreCase(mstWisata.getGender())) {
											hajiFIF.setGender("M");
										} else if ("Perempuan".equalsIgnoreCase(mstWisata.getGender())) {
											hajiFIF.setGender("F");
										}
									}
									hajiFIF.setMaritalStatus("");
									hajiFIF.setHomeOwnership("");
									hajiFIF.setEducation("");
									hajiFIF.setOccupationType("");
									hajiFIF.setOccupation("");
									hajiFIF.setJobPosition("");
									hajiFIF.setMonthlyIncome("");
									hajiFIF.setAddressLine(mstWisata.getAlamatTempatTinggal());
									hajiFIF.setNoRT(mstWisata.getRt());
									hajiFIF.setNoRW(mstWisata.getRw());
									hajiFIF.setStateId(mstWisata.getProvinsiId());
									hajiFIF.setCityId(mstWisata.getKabupatenId());
									hajiFIF.setDistrictId(mstWisata.getKecamatanId());
									hajiFIF.setSubDistrictId(mstWisata.getKelurahanId());
									hajiFIF.setZipcode(mstWisata.getKodePosId());
									hajiFIF.setReferral("");
									hajiFIF.setUserKey(fifUserKey);
									hajiFIF.setLatitude("");
									hajiFIF.setLongitude("");
									hajiFIF.setMotherName(mstWisata.getNamaIbuKandung());
									hajiFIF.setSubZipcode(mstWisata.getKodePos());
									hajiFIF.setStateName(mstWisata.getProvinsi());
									hajiFIF.setCityName(mstWisata.getKota());
									hajiFIF.setDistrictName(mstWisata.getKecamatan());
									hajiFIF.setSubDistrictName(mstWisata.getKelurahan());

									headers.set("api-key", axwayApiKey);
									headers.setContentType(MediaType.APPLICATION_JSON);
									String jsonFIF = gson.toJson(hajiFIF);
									System.out.println(jsonFIF);
									System.out.println("proses insert simulasi");
									HttpEntity<String> entityFIF = new HttpEntity<>(jsonFIF, headers);
									ResponseEntity<FIFDataDto> responseFIF = null;
									ParameterizedTypeReference<FIFDataDto> typeFIF = new ParameterizedTypeReference<FIFDataDto>() {
									};
									responseFIF = restTemplate.exchange(axwayApiUrl + "/backend/amitra/ext/submit",
											HttpMethod.POST, entityFIF, typeFIF);

									if (responseFIF != null && responseFIF.getBody() != null) {
										if (responseFIF.getBody().getErrorCode() != null
												&& responseFIF.getBody().getErrorCode() == 0) {
											mstWisata.setIntegrationLeadsId(
													responseFIF.getBody().getResult().getLeadId());

											MstWisataReligi pengajuanUmrohBaru = wisataService.save(mstWisata);

											MstLeadsRule mstLeadsRule = new MstLeadsRule();
											mstLeadsRule.setUserId(mstUserMobile.getId());
											mstLeadsRule.setLeadsId(pengajuanUmrohBaru.getId());
											mstLeadsRule.setActionStatus("NEW");
											mstLeadsRule.setCompany("FIF Amitra");
											mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
											mstLeadsRule.setLeadsType("HAJI");											
											leadsRuleServ.save(mstLeadsRule);

											MstApplication newApp = new MstApplication();
											newApp.setLeadsId(pengajuanUmrohBaru.getId());
											newApp.setCompany("FIF Amitra");
											newApp.setLeadsType("HAJI");
											newApp.setPickupBu(null);
											newApp.setStatus("Inprogress");
											newApp.setUserId(mstUserMobile.getId());
											MstApplication newApp1 = appService.save(newApp);

											MstApplicationTransaction trxApp = new MstApplicationTransaction();
											trxApp.setApplicationId(newApp1.getId());
											trxApp.setStatus("Inprogress");
											trxApp.setDetail(null);
											trxApp.setTransactionDate(new Date());
											trxApp.setBatchUuid("" + newApp1.getId());
											appTrxService.save(trxApp);
											
											Optional<MstUserMobile> userMobile = userMobileServ.findById(pengajuanUmrohBaru.getUserId());
											if (userMobile.isPresent()) {
												MstUserMobile temporaryUserMobile = userMobile.get();
												if (temporaryUserMobile.getFirebaseToken() != null) {
					
													MstInbox inbox = new MstInbox(); 
													Long id = new Long(3);
													MstInboxCategory category = inboxCategoryServ.findById(id);
													inbox.setTitle("Status pengajuan perjalanan religi haji FIF");
													inbox.setDescription("Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan " + pengajuanUmrohBaru.getProductPurchase() + " yang kamu lakukan. Klik di sini untuk melihat status terkini!");
													inbox.setIdInboxCategory(category);
													inbox.setScreenName("HajiDetail");
													inbox.setInboxType("DIRECT_SCREEN");
													inbox.setIcon("inbox_pengajuan.png");
													inbox.setInboxRead("NO");
													inbox.setUserId(temporaryUserMobile.getId());
													inbox.setIdData(pengajuanUmrohBaru.getId());
													inboxServ.save(inbox);
													MstInbox inbx = inboxServ.save(inbox);

													PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
													LOG.info(temporaryUserMobile.getFirebaseToken());
													pushNotificationReq.setToken(temporaryUserMobile.getFirebaseToken());
													pushNotificationReq
													.setTitle("Status pengajuan perjalanan religi haji FIF");

													Map<String, String> pushData = new HashMap<String, String>();

													Map<String, Object> jsonPush = new HashMap<String, Object>();
													jsonPush.put("screen", "HajiDetail");

					
													Map<String, Object> params = new HashMap<String, Object>();
													params.put("id", pengajuanUmrohBaru.getId());
													params.put("pageType", "");
													params.put("idInbox", inbx.getId());
					
													jsonPush.put("params", params);
					
													String jsonified = new Gson().toJson(jsonPush);
													pushData.put("json", jsonified);
					
													pushNotificationReq.setData(pushData);
					
													pushNotificationReq.setMessage(
															"Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan " + pengajuanUmrohBaru.getProductPurchase() + " yang kamu lakukan. Klik di sini untuk melihat status terkini!");

													try {
														pushNotificationService.sendMessageToToken(pushNotificationReq);
					
													} catch (InterruptedException e) {
														// TODO Auto-generated catch block
														e.printStackTrace();
													} catch (ExecutionException e) {
														// TODO Auto-generated catch block
														e.printStackTrace();
													}
													
												} else {
													LOG.info("Firebase token not found for user " + temporaryUserMobile.getName() + " - "
															+ temporaryUserMobile.getPhone());
												}
											}

											// send to user profile

											if (!mstUserMobile.getEmail().equals(pengajuanUmrohBaru.getEmail())) {
												mailClient.prepareAndSendPengajuanHajiAmitraUserMobile(
														mstUserMobile.getEmail(), "Pengajuan Perjalanan Religi",
														pengajuanUmrohBaru);
											}
											// send to leads user
											mailClient.prepareAndSendPengajuanHajiAmitraUserLeads(
													pengajuanUmrohBaru.getEmail(), "Pengajuan Perjalanan Religi",
													pengajuanUmrohBaru);

											response.put("status", "success");
											response.put("description",
													"Success! Wisata Religi successfully submitted.");
											httpStatus = HttpStatus.OK;
										} else if (responseFIF.getBody().getErrorCode() != null
												&& responseFIF.getBody().getErrorCode() == 70002) {
											response.put("status", "failed");
											response.put("description",
													"Anda telah mencapai maksimum pengajuan amitra.");
											httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
										} else {
											response.put("status", "failed");
											response.put("description",
													"Gagal melakukan submit data. Silakan coba beberapa saat lagi (1).");
											httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
										}
									} else {
										response.put("status", "failed");
										response.put("description",
												"Gagal melakukan submit data. Silakan coba beberapa saat lagi (2).");
										httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
									}
						

					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			re.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping("/updateStatusHaji")
	@ResponseBody
	public Map<String, Object> accountLink(@RequestBody CallbackStatusbackHajiDto callback) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();

		LOG.info(new Gson().toJson(callback));

		MstWisataReligi haji = wisataService.findLeadsId(callback.getLeadsId());

		if (haji != null) {
			MstApplication app = appService.findByCarBaru(haji.getId());
			app.setStatus(callback.getLeadsStatus());
			appService.save(app);

			response.put("status", "success");
			response.put("message", "Successfully to submit data.");
		} else {

			response.put("status", "failed");
			response.put("message", "Failed to submit data.");
		}

		return response;
	}

}
