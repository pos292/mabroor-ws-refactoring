package com.mabroor.ws.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.crypto.IllegalBlockSizeException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.mabroor.ws.dto.MstPengajuanAsuransiMobilReqDto;
import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.entity.MstInboxCategory;
import com.mabroor.ws.entity.MstLeadsRule;
import com.mabroor.ws.entity.MstPengajuanAsuransiMobil;
import com.mabroor.ws.entity.MstPengajuanAsuransiMobilPerluasan;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.service.InboxCategoryService;
import com.mabroor.ws.service.InboxService;
import com.mabroor.ws.service.LeadsRuleService;
import com.mabroor.ws.service.PengajuanAsuransiMobilPerluasanService;
import com.mabroor.ws.service.PengajuanAsuransiMobilService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.service.impl.MailClient;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.entity.MstTransactionCarInsuranceBenefit;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@Transactional
@RestController
@RequestMapping("/v1/api/asuransiMobil")
public class PengajuanAsuransiMobilController {
	
	private static final Logger LOG = Logger.getLogger(PengajuanAsuransiMobilController.class);

	@Autowired
	private PengajuanAsuransiMobilService mobilServ;
	
	@Autowired
	private PushNotificationService pushNotificationService;
	
	@Autowired
	private InboxCategoryService inboxCategoryServ;
	
	@Autowired
	private InboxService inboxServ;
	
	@Autowired
	private UserMobileService userMobileServ;
	
	@Autowired
	private LeadsRuleService leadsRuleServ;
	
	@Autowired
	private PengajuanAsuransiMobilPerluasanService perluasanServ;
	
	@Autowired
	private MailClient mailClient;
	
	@PersistenceContext
	private EntityManager em;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",mobilServ.getListAsuransiMobil());
		return response;
	}
	
	@PostMapping
	@ResponseBody
	public ResponseEntity save(@RequestBody MstPengajuanAsuransiMobil mstMobil, @RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;
		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				MstUserMobile mstUserMobile = null;
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					mstUserMobile = mstUserMobileOpt.get();
					System.out.println(mstUserMobile.getEmail());
					if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){
						String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
						String lastname = mstUserMobile.getLastName() != null ? mstUserMobile.getLastName() : "";
						String fullname = firstName;
						if(!"".equals(lastname)) {
							fullname += " " + lastname;
						}
						mstMobil.setAsuransiMobilStatus("01");;
						mstMobil.setUserId(mstUserMobile.getId());
						mstMobil.setEmailDataUser(mstUserMobile.getEmail());
						mstMobil.setPhoneDataUser(mstUserMobile.getPhone());
						mstMobil.setNameDataUser(fullname);
						
						//default set leads user to user mobile
						mstMobil.setEmail(mstUserMobile.getEmail());
						mstMobil.setPhone(mstUserMobile.getPhone());
						mstMobil.setName(fullname);
						mobilServ.save(mstMobil);
						response.put("status", "success");
						response.put("description", "Success! Asuransi Mobil successfully submitted.");
						httpStatus = HttpStatus.OK;
					}else {
						response.put("status", "failed");
						response.put("description", "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
					}
				}
			}catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}catch(IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}catch (NullPointerException np) {
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch(RuntimeException re) {
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch(OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
			return new ResponseEntity<>(response, httpStatus);
		}
	
	@PostMapping(value ="/submit")
	@ResponseBody
	public ResponseEntity saveAsuransiMobil(@RequestBody MstPengajuanAsuransiMobilReqDto mstMobil, @RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;
		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				MstUserMobile mstUserMobile = null;
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){
						mstMobil.getData().setAsuransiMobilStatus("01");
						mstMobil.getData().setUserId(mstUserMobile.getId());
						mstMobil.getData().setEmailDataUser(mstUserMobile.getEmail());
						mstMobil.getData().setPhoneDataUser(mstUserMobile.getPhone());
						mstMobil.getData().setNameDataUser(mstUserMobile.getName());
						MstPengajuanAsuransiMobil newMobil = mobilServ.save(mstMobil.getData());
						List<MstPengajuanAsuransiMobilPerluasan> luasmobil = new ArrayList<>();
						
						for (MstTransactionCarInsuranceBenefit a : mstMobil.getBenefits()) {
							a.setCarInsurance(mstMobil.getData());

							MstPengajuanAsuransiMobilPerluasan luas = new MstPengajuanAsuransiMobilPerluasan();
							luas.setPerluasan(newMobil);
							luas.setTitle(a.getBenefit());
							if("Garda Oto Syariah - Total Loss Only".equals(newMobil.getProductTitle()) && "Huru Hara, Kerusuhan, Banjir, Angin Topan, Gempa Bumi, Tsunami, Gunung Meletus, Tanah Longsor".equalsIgnoreCase(a.getBenefit())) {
								luas.setAmount("Sudah Termasuk");
							}
							else if("Garda Oto Syariah - Comprehensive".equals(newMobil.getProductTitle()) && "Huru Hara, Kerusuhan, Banjir, Angin Topan, Gempa Bumi, Tsunami, Gunung Meletus, Tanah Longsor".equalsIgnoreCase(a.getBenefit())
								&& new BigDecimal(newMobil.getHargaPertanggungan()).compareTo(new BigDecimal(200000000)) > 0) {
								luas.setAmount("Sudah Termasuk");								
							}
							else {
								luas.setAmount(a.getValue());
							}						
							luas.setAnswer("YES");
							luas.setSeq(a.getSeq());
							MstPengajuanAsuransiMobilPerluasan saveluas = perluasanServ.save(luas);
							luasmobil.add(luas);
							Gson gson = new Gson();
							String json = gson.toJson(saveluas);
							LOG.info(json);
						}
						
						MstLeadsRule mstLeadsRule = new MstLeadsRule();
						mstLeadsRule.setUserId(mstUserMobile.getId());
						mstLeadsRule.setLeadsId(newMobil.getId());
						mstLeadsRule.setLeadsType(Constant.LEADS_TYPE_ASURANSIMOBIL);
						mstLeadsRule.setActionStatus("NEW");
						mstLeadsRule.setCompany("AAB");
						mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
						mstLeadsRule.setLeadsType("ASURANSIMOBIL");
						leadsRuleServ.save(mstLeadsRule);
						
						Optional<MstUserMobile> userMobile = userMobileServ.findById(newMobil.getUserId());
												if (userMobile.isPresent()) {
													MstUserMobile temporaryUserMobile = userMobile.get();
													if (temporaryUserMobile.getFirebaseToken() != null) {
						
														MstInbox inbox = new MstInbox(); 
														Long id = new Long(3);
														MstInboxCategory category = inboxCategoryServ.findById(id);
														inbox.setTitle("Status pengajuan asuransi mobil");
														inbox.setDescription("Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan asuransi mobil yang kamu lakukan. Klik di sini untuk melihat status terkini!");
														inbox.setIdInboxCategory(category);
														inbox.setScreenName("AsuransiMobilDetail");
														inbox.setInboxType("DIRECT_SCREEN");
														inbox.setIcon("inbox_pengajuan.png");
														inbox.setInboxRead("NO");
														inbox.setUserId(temporaryUserMobile.getId());
														inbox.setIdData(newMobil.getId());
														MstInbox inbx = inboxServ.save(inbox);

														PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
														LOG.info(temporaryUserMobile.getFirebaseToken());
														pushNotificationReq.setToken(temporaryUserMobile.getFirebaseToken());
														pushNotificationReq
														.setTitle("Status pengajuan asuransi mobil");
						
														Map<String, String> pushData = new HashMap<String, String>();
						
														Map<String, Object> jsonPush = new HashMap<String, Object>();
														jsonPush.put("screen", "AsuransiMobilDetail");
						
														Map<String, Object> params = new HashMap<String, Object>();
														params.put("id", newMobil.getId());
														params.put("pageType", "");
														params.put("idInbox", inbx.getId());
						
														jsonPush.put("params", params);
						
														String jsonified = new Gson().toJson(jsonPush);
														pushData.put("json", jsonified);
						
														pushNotificationReq.setData(pushData);
						
														pushNotificationReq.setMessage(
																"Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan asuransi mobil yang kamu lakukan. Klik di sini untuk melihat status terkini!");

														try {
															pushNotificationService.sendMessageToToken(pushNotificationReq);
						
														} catch (InterruptedException e) {
															// TODO Auto-generated catch block
															e.printStackTrace();
														} catch (ExecutionException e) {
															// TODO Auto-generated catch block
															e.printStackTrace();
														}
														
													} else {
														LOG.info("Firebase token not found for user " + temporaryUserMobile.getName() + " - "
																+ temporaryUserMobile.getPhone());
													}
												}
						
						if (!mstUserMobile.getEmail().equals(newMobil.getEmail())) {
							mailClient.prepareAndSendPengajuanAsuransiMobilUserMobile(mstUserMobile.getEmail(),"Pengajuan Asuransi Mobil", newMobil);
						}
						// send to leads user
						    mailClient.prepareAndSendPengajuanAsuransiMobilUserLeads(newMobil.getEmail(),"Pengajuan Asuransi Mobil", newMobil);
						response.put("status", "success");
						response.put("description", "Success! Asuransi Mobil successfully submitted.");
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description", "Akunmu telah login di device lain");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
			}catch (MalformedJwtException mje) {
				LOG.info(mje.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}catch(IllegalBlockSizeException ibe) {
				LOG.info(ibe.getMessage());
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			}catch (NullPointerException np) {
				LOG.info(np.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch (StackOverflowError st) {
				LOG.info(st.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch(RuntimeException re) {
				LOG.info(re.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}catch(OutOfMemoryError oe) {
				LOG.info(oe.getMessage());
				response.put("status", "failed");
				response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
			return new ResponseEntity<>(response, httpStatus);
		}
}
