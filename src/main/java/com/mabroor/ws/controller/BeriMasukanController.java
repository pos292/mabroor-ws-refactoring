package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.ResponseDto;
import com.mabroor.ws.entity.MstBeriMasukan;
import com.mabroor.ws.service.BeriMasukanService;
import com.mabroor.ws.util.Constant;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/v1/api/berimasukan")
@Tag(name = "Contact US (Beri Masukan)", description = "API to submit user feedback about the Mabroor Application")
public class BeriMasukanController {

	
	@Autowired
	private BeriMasukanService beriMasukanService;
	
	@GetMapping(value = "/list", produces = "application/json")
	@ResponseBody
	@Operation(summary = "Contact US (Beri Masukan) - List Data")
	@ApiResponses(value = { 
		@ApiResponse(responseCode = "200", description = "Data Found", content = { @Content(mediaType = "application/json", schema = @Schema(example = "{\"status\":\"success\",\"desciption\":\"Success! Beri Masukan successfully submitted.\"}", type = "object")) }),
		@ApiResponse(responseCode = "500", description = "There is an internal error", content = @Content(schema = @Schema(hidden = true))) 
	})
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",beriMasukanService.getListBeriMasukan());
		return response;
	}
	
	@PostMapping(produces = "application/json")
	@ResponseBody
	@Operation(summary = "Contact US (Beri Masukan) - Save Data")
	@ApiResponses(value = { 
		@ApiResponse(responseCode = "200", description = "Data Found", content = { @Content(mediaType = "application/json", schema = @Schema(example = "{\"status\":\"success\",\"desciption\":\"Success! Beri Masukan successfully submitted.\"}", type = "object")) }),
		@ApiResponse(responseCode = "500", description = "There is an internal error", content = @Content(schema = @Schema(hidden = true))) 
	})
	public ResponseDto save(@RequestBody MstBeriMasukan mstBeriMasukan) {
		System.out.println(mstBeriMasukan.getName());
		ResponseDto response = new ResponseDto();
		mstBeriMasukan.setBeriMasukanStatus("01");
		beriMasukanService.save(mstBeriMasukan);
		response.setStatus("success");
		response.setDescription("Success! Beri Masukan successfully submitted.");
		
		return response;
    }
}
