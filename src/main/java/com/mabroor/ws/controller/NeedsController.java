package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.NeedsService;

@RestController
@RequestMapping("/v1/api/needs")
public class NeedsController {

	@Autowired
	private NeedsService needServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",needServ.getListNeeds());
		return response;
	}
	
	@GetMapping(value = "/{needsId}")
	public LinkedHashMap<String, Object> getNeedsById(@PathVariable Long needsId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",needServ.findById(needsId));
		return response;
	}
}
