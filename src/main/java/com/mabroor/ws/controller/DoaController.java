package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.DoaDto;
import com.mabroor.ws.dto.DoaListDto;
import com.mabroor.ws.dto.DoaSearchDto;
import com.mabroor.ws.entity.MstDoa;
import com.mabroor.ws.entity.MstDoaCategory;
import com.mabroor.ws.service.DoaService;

@RestController
@RequestMapping("/v1/api/doa")
public class DoaController {

	private static final Logger LOG = Logger.getLogger(DoaController.class);

	@Autowired
	private DoaService doaServ;

	@PostMapping(value = "/save")
	@ResponseBody
	public ResponseEntity save(@RequestBody DoaListDto doaDto) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;

		for(DoaDto a : doaDto.getDoa()) {
			MstDoa b = new MstDoa();
			b.setId(a.getId());
			b.setNamaDoa(a.getNamaDoa());
			b.setDoa(a.getDoa());
			b.setBunyi(a.getBunyi());
			b.setArti(a.getArti());
			b.setCreatedAt(a.getCreatedAt());
			b.setUpdatedAt(a.getUpdatedAt());
			if(a.getIdCategory()!=null) {
				MstDoaCategory c = new MstDoaCategory();
				c.setId(a.getIdCategory());
				b.setIdCategory(c);
			}else {
				b.setIdCategory(null);
			}

			doaServ.save(b);

		}

		response.put("status", "success");
		response.put("description", "data ditemukan");
		httpStatus = HttpStatus.OK;
		return new ResponseEntity<>(response, httpStatus);
	}

	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getListDoa() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();

		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", doaServ.listDoa());
		return response;
	}

	@GetMapping(value = "/list/{idCategory}")
	@ResponseBody
	public Map<String, Object> getListDoaByIdCategory(@PathVariable Long idCategory) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		
		List<MstDoa> doa = new ArrayList<MstDoa>();
		
		if(idCategory != null && idCategory == 6) {
			doa = doaServ.listDoa();
		}else {
//			doa = doaServ.listDoaByIdCategory(idCategory, "%"+namaDoa+"%", limit, (limit * page));
			doa = doaServ.listDoaByIdCategory(idCategory);
		}
		

		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", doa);
		return response;
	}

	@PostMapping(value = "/search")
	@ResponseBody
	public ResponseEntity searchDoa(@RequestBody DoaSearchDto doa) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;

		List<MstDoa> doaList = doaServ.listSearchDoa("%"+doa.getNamaDoa()+"%");

		response.put("status", "success");
		response.put("description", "data ditemukan");
		response.put("data", doaList);
		httpStatus = HttpStatus.OK;


		return new ResponseEntity<>(response, httpStatus);
	}
}
