package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.ResponseDto;
import com.mabroor.ws.entity.MstHubungiKami;
import com.mabroor.ws.service.HubungiKamiService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/hubungikami")
public class HubungiKamiController {

	@Autowired
	private HubungiKamiService hubServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",hubServ.getListHubungiKami());
		return response;
	}
	
	@PostMapping
	@ResponseBody
	public ResponseDto save(@RequestBody MstHubungiKami mstHub) {
		ResponseDto response = new ResponseDto();
		mstHub.setHubungiKamiStatus("01");
		hubServ.save(mstHub);
		response.setStatus("success");
		response.setDescription("Success! Hubungi Kami successfully submitted.");
		
		return response;
    }
}
