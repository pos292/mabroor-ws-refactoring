package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.KelurahanService;

@RestController
@RequestMapping("/v1/api/kelurahan")
public class KelurahanController {

	@Autowired
	private KelurahanService lurahService;
	
	
	@GetMapping(value = "/kecamatan/{kecamatanId}")
	public Map<String, Object> getKecamatanById(@PathVariable Long kecamatanId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",lurahService.findByKecamatanId(kecamatanId));
		return response;
	}
}
