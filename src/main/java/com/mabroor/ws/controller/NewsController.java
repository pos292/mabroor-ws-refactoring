package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.NewsService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/news")
public class NewsController {
	

	@Autowired
	private NewsService newsService;

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",newsService.findAllByStatus());
		return response;
	}
	
	@GetMapping(value = "/{newsId}")
	public Map<String, Object> getNewsById(@PathVariable Long newsId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",newsService.findById(newsId));
		return response;
	}


	@GetMapping(value = "/mainProduct/{idMainProduct}")
	public Map<String, Object> getProductEpicsById(@PathVariable Long idMainProduct) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data", newsService.findByMainProductId(idMainProduct));
		return response;
	}
	
	@GetMapping(value = "/mainProduct/{idMainProduct}/{subcategoryName}")
	public Map<String, Object> getByMainProductSubCategory(@PathVariable Long idMainProduct, @PathVariable String subcategoryName) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data", newsService.findByMainProductIdandSubCategoryName(idMainProduct, subcategoryName));
		return response;
	}

	@GetMapping(value = "/mainProduct/landingpage/{idMainProduct}")
	public Map<String, Object> getProductEpicsByIdLanding(@PathVariable Long idMainProduct) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data", newsService.findByMainProductIdLimit(idMainProduct));
		return response;
	}

	@GetMapping(value = "/mainProduct/all")
	public Map<String, Object> getCategoryAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", newsService.findByMainProductAll());
		return response;
	}
	
	@GetMapping(value = "/master/subcategory")
	public Map<String, Object> getSubCategoryActive() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", newsService.findActiveSubCategory());
		return response;
	}
	
	@GetMapping(value = "/master/subcategory/all")
	public Map<String, Object> getSubCategoryAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", newsService.findAllSubCategory());
		return response;
	}
	
	@GetMapping(value = "/subcategory/{subcategoryName}")
	public Map<String, Object> getSubCategoryByName(@PathVariable String subcategoryName) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", newsService.findBySubCategoryName(subcategoryName));
		return response;
	}
	
	@GetMapping(value = "/subcategory/{subcategoryName}/all")
	public Map<String, Object> getSubCategoryByNameAll(@PathVariable String subcategoryName) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data", newsService.findBySubCategoryNameAll(subcategoryName));
		return response;
	}

}
