package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.mabroor.ws.dto.CalculateHajiAccReqDto;
import com.mabroor.ws.entity.MstHajiAccMaster;
import com.mabroor.ws.service.PembiayaanHajiAccMasterService;
import com.mabroor.ws.util.Constant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/api/hajiaccmaster")
public class PembiayaanHajiAccMasterController {

    @Autowired
	private PembiayaanHajiAccMasterService masterHajiServ;

    @GetMapping(value = "/getListProduct")
	@ResponseBody
	public Map<String, Object> getProductAcc() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		
        List<MstHajiAccMaster> listProduct = masterHajiServ.getProductAcc();
        List<Map<String, String>> data = new ArrayList<>();
        for (MstHajiAccMaster mstHajiAccMaster : listProduct) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("productName", mstHajiAccMaster.getProductName());
            map.put("productCode", mstHajiAccMaster.getProductCode());
            data.add(map);
        }

        response.put("data", data);
		return response;
	}

    @GetMapping(value = "/getListTenor")
	@ResponseBody
	public Map<String, Object> getTenorAcc() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		
        List<MstHajiAccMaster> listProduct = masterHajiServ.getTenorAcc();
        List<Map<String, String>> data = new ArrayList<>();
        for (MstHajiAccMaster mstHajiAccMaster : listProduct) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("tenor", mstHajiAccMaster.getTenor());
            data.add(map);
        }
        
        response.put("data", data);
        return response;
	}

    @GetMapping(value = "/getListJumlahPeserta")
	@ResponseBody
	public Map<String, Object> getJumlahPesertaAcc() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		
        List<MstHajiAccMaster> listProduct = masterHajiServ.getJumlahPesertaAcc();
        List<Map<String, String>> data = new ArrayList<>();
        for (MstHajiAccMaster mstHajiAccMaster : listProduct) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("jumlahPeserta", mstHajiAccMaster.getJumlahPeserta());
            data.add(map);
        }
        response.put("data", data);
		return response;
	}

    // @PostMapping(value = "/hitungBiaya")
    // @ResponseBody
    // public Map<String, Object> getCalculateMrp(@RequestBody CalculateHajiAccReqDto calculate){
    // 	LinkedHashMap<String, Object> response = new LinkedHashMap<>();
    // 	response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
	// 	response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
    //     MstHajiAccMaster calcResult = masterHajiServ.findByCalculateCost(calculate.getProductName(), calculate.getTenor(), calculate.getJumlahPeserta()).orElse(null);
    //     response.put("data", calcResult);
	// 	return response;
    // }
}
