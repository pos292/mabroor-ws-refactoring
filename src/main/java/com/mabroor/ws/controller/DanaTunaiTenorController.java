package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.DanaTunaiTenorService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/danatunaitenor")
public class DanaTunaiTenorController {

	@Autowired
	private DanaTunaiTenorService tenorServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> findAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",tenorServ.findAll());
		return response;
	}
	
	@GetMapping(value = "/{type}")
	@ResponseBody
	public Map<String, Object> getType(@PathVariable String type) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	    response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
	   	response.put("data",tenorServ.findByType(type));
	   	return response;
	}
	
	@GetMapping(value = "/{type}/{product}")
	@ResponseBody
	public Map<String, Object> getTypeProduct(@PathVariable String type,@PathVariable String product) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	    response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
	   	response.put("data",tenorServ.findByProductType(type, product));
	   	return response;
	}
}
