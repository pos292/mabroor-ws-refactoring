package com.mabroor.ws.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.crypto.IllegalBlockSizeException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.mabroor.ws.dto.MstPengajuanMotorDto;
import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.entity.MstInboxCategory;
import com.mabroor.ws.entity.MstLeadsRule;
import com.mabroor.ws.entity.MstPengajuanMotor;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.service.InboxCategoryService;
import com.mabroor.ws.service.InboxService;
import com.mabroor.ws.service.LeadsRuleService;
import com.mabroor.ws.service.PengajuanMotorService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.service.impl.MailClient;
import com.mabroor.ws.util.BASE64DecodedMultipartFile;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.util.ObjectMapperUtil;
import com.mabroor.ws.util.SHA256Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/pengajuanmotor")
public class PengajuanMotorController {

	private static final Logger LOG = Logger.getLogger(PengajuanMotorController.class);

	@Autowired
	private PengajuanMotorService pengajuanService;

	@Autowired
	private LeadsRuleService leadsRuleServ;

	@Autowired
	private UserMobileService userMobileServ;
	
	@Autowired
	private PushNotificationService pushNotificationService;
	
	@Autowired
	private InboxCategoryService inboxCategoryServ;
	
	@Autowired
	private InboxService inboxServ;

	@Autowired
	private MailClient mailClient;

	@PersistenceContext
	private EntityManager em;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Value("${moxa_api_key}")
	private String moxaApiKey;

	@Value("${moxa_salt_key}")
	private String moxaSaltKey;


	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		Map<String, Object> map = new HashMap<>();
		List<MstPengajuanMotor> pengajuanMotor = pengajuanService.getListPengajuan();
		for(MstPengajuanMotor mstPengajuanMotor : pengajuanMotor) {
			em.detach(mstPengajuanMotor);
			mstPengajuanMotor.setKtpPhoto(this.imageBaseUrl + "pengajuan-motor/" + mstPengajuanMotor.getKtpPhoto());
		}
		map.put("data", pengajuanMotor);

		response.put("data", map);
		return response;
	}

	@PostMapping
	@ResponseBody
	public ResponseEntity save(@RequestBody MstPengajuanMotorDto mstPengajuan, @RequestHeader(value = "appToken", required = true) String token,@RequestHeader(value = "x-signature-moxa", required = true) String signature) {
		LOG.info(mstPengajuan.getName());
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		Gson gson = new Gson(); 
		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				String result = gson.toJson(response);
				response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
				httpStatus = HttpStatus.UNAUTHORIZED;
			}else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				MstUserMobile mstUserMobile = null;
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					String result = gson.toJson(response);
					response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
					httpStatus = HttpStatus.UNAUTHORIZED;
				}else {
					mstUserMobile = mstUserMobileOpt.get();
					String json = gson.toJson(mstPengajuan); 
					String hash = SHA256Utils.encrypt(json + moxaApiKey + token + moxaSaltKey);
					LOG.info("json " + json);
					LOG.info("moxaApiKey " + moxaApiKey);
					LOG.info("token " + token);
					LOG.info("moxaSaltKey " + moxaSaltKey);
					LOG.info(hash);
					mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals(""+mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())){

						byte[] fileByteFotoUnit = new byte[0];
						MultipartFile fileFotoUnit = null;

						byte[] fileByteFotoKtp = new byte[0];
						MultipartFile fileFotoKtp = null;

						if(mstPengajuan.getFotoUnit() == null || "".equals(mstPengajuan.getFotoUnit())){
							fileFotoUnit = null;
						}else{
							fileByteFotoUnit = Base64.getDecoder().decode(mstPengajuan.getFotoUnit());
							fileFotoUnit = new BASE64DecodedMultipartFile(fileByteFotoUnit, "img.png");
						}

						if(mstPengajuan.getKtpPhoto() == null || "".equals(mstPengajuan.getKtpPhoto())){
							fileFotoKtp = null;
						}else{
							fileByteFotoKtp = Base64.getDecoder().decode(mstPengajuan.getKtpPhoto());
							fileFotoKtp = new BASE64DecodedMultipartFile(fileByteFotoKtp, "img.png");
						}

						String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
						String lastname = mstPengajuan.getLastName();
						String fullname = firstName;
						if(!"".equals(lastname)) {
							fullname += " " + lastname;
						}else {
							fullname = mstUserMobile.getName();
						}

						mstPengajuan.setPengajuanStatus("01");
						mstPengajuan.setUserId(mstUserMobile.getId());
						if(mstPengajuan.getEmailDataUser() == null || "".equals(mstPengajuan.getEmailDataUser())) {
							mstPengajuan.setEmailDataUser(mstUserMobile.getEmail());
						}

						if(mstPengajuan.getPhoneDataUser() == null || "".equals(mstPengajuan.getPhoneDataUser())) {
							mstPengajuan.setPhoneDataUser(mstUserMobile.getPhone());
						}

						mstPengajuan.setNameDataUser(fullname);
						mstPengajuan.setCompanyName("FIFGROUP");

						mstPengajuan.setName(fullname);

						String temporaryBirthDate = mstPengajuan.getTransactionDateTime();
						mstPengajuan.setTransactionDateTime(null);
						MstPengajuanMotor mstMotor = ObjectMapperUtil.map(mstPengajuan,
								MstPengajuanMotor.class);
						try {
							mstMotor.setTransactionDateTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").parse(temporaryBirthDate));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						MstPengajuanMotor pengajuanMotorBaru = pengajuanService.save(fileFotoUnit, fileFotoKtp, mstMotor);

						MstLeadsRule mstLeadsRule = new MstLeadsRule();
						mstLeadsRule.setUserId(mstUserMobile.getId());
						mstLeadsRule.setLeadsId(pengajuanMotorBaru.getId());
						mstLeadsRule.setActionStatus("NEW");
						mstLeadsRule.setCompany("FIFGROUP");
						mstLeadsRule.setCreatedBy(mstUserMobile.getId().toString());
						mstLeadsRule.setLeadsType(Constant.LEADS_TYPE_MOTOR);

						leadsRuleServ.save(mstLeadsRule);
						
						Optional<MstUserMobile> userMobile = userMobileServ.findById(pengajuanMotorBaru.getUserId());
												if (userMobile.isPresent()) {
													MstUserMobile temporaryUserMobile = userMobile.get();
													if (temporaryUserMobile.getFirebaseToken() != null) {
						
														MstInbox inbox = new MstInbox(); 
														Long id = new Long(3);
														MstInboxCategory category = inboxCategoryServ.findById(id);
														inbox.setTitle("Status pengajuan motor");
														inbox.setDescription("Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan motor yang kamu lakukan. Klik di sini untuk melihat status terkini!");
														inbox.setIdInboxCategory(category);
														inbox.setScreenName("NewMcyDetail");
														inbox.setInboxType("DIRECT_SCREEN");
														inbox.setIcon("inbox_pengajuan.png");
														inbox.setInboxRead("NO");
														inbox.setUserId(temporaryUserMobile.getId());
														inbox.setIdData(pengajuanMotorBaru.getId());
														MstInbox inbx = inboxServ.save(inbox);

														PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
														LOG.info(temporaryUserMobile.getFirebaseToken());
														pushNotificationReq.setToken(temporaryUserMobile.getFirebaseToken());
														pushNotificationReq
														.setTitle("Status pengajuan motor");
						
														Map<String, String> pushData = new HashMap<String, String>();
						
														Map<String, Object> jsonPush = new HashMap<String, Object>();
														jsonPush.put("screen", "NewMcyDetail");
						
														Map<String, Object> params = new HashMap<String, Object>();
														params.put("id", pengajuanMotorBaru.getId());
														params.put("pageType", "");
														params.put("idInbox", inbx.getId());
														
						
														jsonPush.put("params", params);
						
														String jsonified = new Gson().toJson(jsonPush);
														pushData.put("json", jsonified);
						
														pushNotificationReq.setData(pushData);
						
														pushNotificationReq.setMessage(
																"Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan motor yang kamu lakukan. Klik di sini untuk melihat status terkini!");
						
														try {
															pushNotificationService.sendMessageToToken(pushNotificationReq);
						
														} catch (InterruptedException e) {
															// TODO Auto-generated catch block
															e.printStackTrace();
														} catch (ExecutionException e) {
															// TODO Auto-generated catch block
															e.printStackTrace();
														}
													
													} else {
														LOG.info("Firebase token not found for user " + temporaryUserMobile.getName() + " - "
																+ temporaryUserMobile.getPhone());
													}
												}

						//send to user profile
						if(!mstUserMobile.getEmail().equals(pengajuanMotorBaru.getEmail())) {
							mailClient.prepareAndSendPengajuanMotorUserMobile(mstUserMobile.getEmail(), "Pengajuan Motor", pengajuanMotorBaru);
						}
						//send to leads user
						mailClient.prepareAndSendPengajuanMotorUserLeads(pengajuanMotorBaru.getEmail(), "Pengajuan Motor", pengajuanMotorBaru);
						
						response.put("status", "success");
						response.put("description", "Success! Leads Motor Data successfully submitted.");
						String result = gson.toJson(response);

						response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
						httpStatus = HttpStatus.OK;
					}else {
						response.put("status", "failed");
						response.put("description", "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						String result = gson.toJson(response);

						response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}	
			}
		}catch(MalformedJwtException mje)
		{
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			String result = gson.toJson(response);
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		}catch(IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			String result = gson.toJson(response);
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		}catch(NullPointerException np)
		{
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			String result = gson.toJson(response);
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch(StackOverflowError st)
		{
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			String result = gson.toJson(response);
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch(RuntimeException re)
		{
			re.printStackTrace();
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			String result = gson.toJson(response);
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}catch(OutOfMemoryError oe)
		{
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			String result = gson.toJson(response);
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response,httpStatus);
	}
}
