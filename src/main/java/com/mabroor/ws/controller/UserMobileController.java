package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.util.GeneratePdf;
import com.google.gson.Gson;
import com.mabroor.ws.dto.CounterOtpDto;
import com.mabroor.ws.dto.MstOtpDto;
import com.mabroor.ws.dto.MstOtpVreifyDto;
import com.mabroor.ws.dto.MstUserMobileDto;
import com.mabroor.ws.dto.MstUserMobileUpdateIdentityDto;
import com.mabroor.ws.dto.UserMobileChangePinDto;
import com.mabroor.ws.dto.UserMobileDto;
import com.mabroor.ws.dto.UserMobilePhoneDto;
import com.mabroor.ws.entity.MstOtp;
import com.mabroor.ws.entity.MstOtpCount;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.entity.SysParam;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.repository.UserMobileRepository;
import com.mabroor.ws.service.OtpCountService;
import com.mabroor.ws.service.OtpService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.SysParamService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.service.impl.MailClient;
import com.mabroor.ws.util.AES256Utils;
import com.mabroor.ws.util.BASE64DecodedMultipartFile;
import com.mabroor.ws.util.Common;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.util.ObjectMapperUtil;
import com.mabroor.ws.util.SHA256Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/usermobile")
public class UserMobileController {

	private static final Logger LOG = Logger.getLogger(UserMobileController.class);

	@Autowired
	private UserMobileService userMobileServ;

	@Autowired
	private SysParamService sysParamService;

	@Autowired
	private OtpService otpServ;

	@Autowired
	private OtpCountService otpCountServ;

	@Autowired
	private UserMobileRepository userMobileRepo;

	@Autowired
	private MailClient mailClient;

	@Autowired
	private PushNotificationService pushNotificationService;

	@Value("${image.base.url}")
	private String imageBaseUrl;

	@Value("${moxa_api_key}")
	private String moxaApiKey;

	@Value("${moxa_salt_key}")
	private String moxaSaltKey;

	public static final String URL_REGEX = "^((https?|ftp|http?)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";

	// @GetMapping(value = "/testOtpClearing")
	// @ResponseBody
	// public LinkedHashMap<String, Object> test(@RequestHeader(value = "appToken", required = true) String token) {

	// 	LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
	// 	try {
	// 		Claims claims = JWTGenerator.getInstance().decodeJWT(token);
	// 		Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
	// 		MstUserMobile mstUserMobile = mstUserMobileOpt.get();
			
	// 		//CLEAR OTP COUNTER
	// 		otpServ.updateAllVerificationByPhoneNumber(mstUserMobile.getPhone());

	// 		response.put("status", "success");
	// 		response.put("description", "succesfully get data");
	// 		response.put("data", mstUserMobile.getPhone());
	// 		return response;
	// 	} catch (Exception e) {

	// 		response.put("status", "fail");
	// 		response.put("description", "failed get data");
	// 		return response;
	// 	}
	// }

	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", userMobileServ.getListMobile());
		return response;
	}

	@GetMapping(value = "/listotp")
	@ResponseBody
	public LinkedHashMap<String, Object> getListOtp() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", otpServ.findAllOtp());
		return response;
	}
	
	@GetMapping(value = "/getPhone/{phone}")
	@ResponseBody
	public LinkedHashMap<String, Object> getPhone(@PathVariable String phone) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", userMobileServ.getOneByPhoneNumber(phone));
		System.out.println(phone);
		return response;
	}

	@GetMapping(value = "/testemail/{email}")
	@ResponseBody
	public LinkedHashMap<String, Object> testemail(@PathVariable String email) {
		mailClient.prepareAndSend("test subject", email, "Pengajuan Mobil", "kenfan", "Hallo this is body email",
				"email-test");
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully send email");
		return response;
	}

	@GetMapping(value = "/testnotif")
	@ResponseBody
	public LinkedHashMap<String, Object> testnotif() {

		PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
		pushNotificationReq.setTitle("Octopus Breaking News");
		pushNotificationReq.setTopic("octopus-news");

		Map<String, String> data = new HashMap<String, String>();
		pushNotificationReq.setData(data);

		pushNotificationReq.setMessage("Test Kirim Push Notif message");

		try {
			pushNotificationService.sendMessageWithoutData(pushNotificationReq);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully send notification");
		return response;
	}

	@GetMapping(value = "/testpdf")
	@ResponseBody
	public LinkedHashMap<String, Object> testpdf() {
		String tempPdf = GeneratePdf.testGenerate();

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully generate pdf");
		response.put("data", tempPdf);
		return response;
	}

	@GetMapping(value = "/profile")
	@ResponseBody
	public ResponseEntity getUserMobileById(@RequestHeader(value = "appToken", required = true) String token) {
		LOG.info(token);

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {

						if (mstUserMobile.getKtpImage() != null && !"".equals(mstUserMobile.getKtpImage())) {
							mstUserMobile.setKtpImage(this.imageBaseUrl + "user-mobile/" + mstUserMobile.getKtpImage());
						} else {
							mstUserMobile.setKtpImage(null);
						}

						if (mstUserMobile.getNpwpImage() != null && !"".equals(mstUserMobile.getNpwpImage())) {
							mstUserMobile
									.setNpwpImage(this.imageBaseUrl + "user-mobile/" + mstUserMobile.getNpwpImage());
						} else {
							mstUserMobile.setNpwpImage(null);
						}

						if (mstUserMobile.getBpkbMobilImage() != null
								&& !"".equals(mstUserMobile.getBpkbMobilImage())) {
							mstUserMobile.setBpkbMobilImage(
									this.imageBaseUrl + "user-mobile/" + mstUserMobile.getBpkbMobilImage());
						} else {
							mstUserMobile.setBpkbMobilImage(null);
						}

						if (mstUserMobile.getBpkbMotorImage() != null
								&& !"".equals(mstUserMobile.getBpkbMotorImage())) {
							mstUserMobile.setBpkbMotorImage(
									this.imageBaseUrl + "user-mobile/" + mstUserMobile.getBpkbMotorImage());
						} else {
							mstUserMobile.setBpkbMotorImage(null);
						}

						if (mstUserMobile.getKkImage() != null && !"".equals(mstUserMobile.getKkImage())) {
							mstUserMobile.setKkImage(this.imageBaseUrl + "user-mobile/" + mstUserMobile.getKkImage());
						} else {
							mstUserMobile.setKkImage(null);
						}

						if (mstUserMobile.getPpImage() != null && !"".equals(mstUserMobile.getPpImage())) {
							mstUserMobile.setPpImage(this.imageBaseUrl + "user-mobile/" + mstUserMobile.getPpImage());
						} else {
							mstUserMobile.setPpImage(null);
						}
						
						mstUserMobile.setMaskedPhone(Common.maskingString(mstUserMobile.getPhone(), 6));

						response.put("status", "success");
						response.put("description", "succesfully get data");
						response.put("data", mstUserMobile);
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}
	

	@PostMapping(value = "/profileId")
	@ResponseBody
	public ResponseEntity getUserMobilByIdBaru(@RequestBody UserMobileDto userMobileDto,
			@RequestHeader(value = "appToken", required = true) String token) {
		LOG.info(token);

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();

					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						String text = token + mstUserMobile.getUserMobileUuid() + "m0x41!";
						String checksum = SHA256Utils.encrypt(text);
						LOG.info(checksum);
						if (checksum == null || !checksum.equals(userMobileDto.getChecksum())) {
							response.put("status", "failed");
							response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (04)");
							httpStatus = HttpStatus.UNAUTHORIZED;

							return new ResponseEntity<>(response, httpStatus);
						}
						if (mstUserMobile.getKtpImage() != null && !"".equals(mstUserMobile.getKtpImage())) {
							mstUserMobile.setKtpImage(this.imageBaseUrl + "user-mobile/" + mstUserMobile.getKtpImage());
						} else {
							mstUserMobile.setKtpImage(null);
						}

						if (mstUserMobile.getNpwpImage() != null && !"".equals(mstUserMobile.getNpwpImage())) {
							mstUserMobile
									.setNpwpImage(this.imageBaseUrl + "user-mobile/" + mstUserMobile.getNpwpImage());
						} else {
							mstUserMobile.setNpwpImage(null);
						}

						if (mstUserMobile.getBpkbMobilImage() != null
								&& !"".equals(mstUserMobile.getBpkbMobilImage())) {
							mstUserMobile.setBpkbMobilImage(
									this.imageBaseUrl + "user-mobile/" + mstUserMobile.getBpkbMobilImage());
						} else {
							mstUserMobile.setBpkbMobilImage(null);
						}

						if (mstUserMobile.getBpkbMotorImage() != null
								&& !"".equals(mstUserMobile.getBpkbMotorImage())) {
							mstUserMobile.setBpkbMotorImage(
									this.imageBaseUrl + "user-mobile/" + mstUserMobile.getBpkbMotorImage());
						} else {
							mstUserMobile.setBpkbMotorImage(null);
						}

						if (mstUserMobile.getKkImage() != null && !"".equals(mstUserMobile.getKkImage())) {
							mstUserMobile.setKkImage(this.imageBaseUrl + "user-mobile/" + mstUserMobile.getKkImage());
						} else {
							mstUserMobile.setKkImage(null);
						}

						if (mstUserMobile.getPpImage() != null && !"".equals(mstUserMobile.getPpImage())) {
							mstUserMobile.setPpImage(this.imageBaseUrl + "user-mobile/" + mstUserMobile.getPpImage());
						} else {
							mstUserMobile.setPpImage(null);
						}

						mstUserMobile.setMaskedPhone(Common.maskingString(mstUserMobile.getPhone(), 6));

						response.put("status", "success");
						response.put("description", "succesfully get data");
						response.put("data", mstUserMobile);
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping
	@ResponseBody
	public ResponseEntity save(@RequestBody MstUserMobileDto mstUserMobileDto,
			@RequestHeader(value = "appToken", required = true) String token,
			@RequestHeader(value = "x-signature-moxa", required = true) String signature) {

		Gson gson = new Gson();
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				String result = gson.toJson(response);
//				String results = result.replaceAll("\"", "\\\\\"");
				response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					String result = gson.toJson(response);
//					String results = result.replaceAll("\"", "\\\\\"");
					response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
//					mstUserMobile = mstUserMobileOpt.get();
//					System.out.println(claims.getId());
//					System.out.println(""+mstUserMobileDto.getId());
//					System.out.println(mstUserMobileOpt.get().getToken());
//					System.out.println(token);

					String json = gson.toJson(mstUserMobileDto);
//					String jsons = json.replaceAll("\"", "\\\\\"");
					String hash = SHA256Utils.encrypt(json + moxaApiKey + token + moxaSaltKey);
					System.out.println("json " + json);
					System.out.println("moxaApiKey " + moxaApiKey);
					System.out.println("token " + token);
					System.out.println("moxaSaltKey " + moxaSaltKey);
					System.out.println(hash);

					if (!token.equals(mstUserMobileOpt.get().getToken())) {
						response.put("status", "failed");
						response.put("description", "you are not authorized (1)");
						String result = gson.toJson(response);
//						String results = result.replaceAll("\"", "\\\\\"");
						System.out.println("masuk siniii = " + result);
						response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
						httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
						
					} else {
						System.out.println(mstUserMobileOpt.get().getPhone());
						if (claims.getId().equals("" + mstUserMobileDto.getId())
								&& token.equals(mstUserMobileOpt.get().getToken())) {

							boolean isValidationGood = true;
							System.out.println("log"+ token);
							if(mstUserMobileDto.getPhoneNumOld() != null) {
								if(!"".equals(mstUserMobileDto.getPhoneNumOld().trim()) && !mstUserMobileOpt.get().getPhone().equals(mstUserMobileDto.getPhoneNumOld())) {
									isValidationGood = false;
								}else {
									isValidationGood = true;
								}
							}else {
								if(mstUserMobileOpt.get().getPhone().equalsIgnoreCase(mstUserMobileDto.getPhone())) {
									isValidationGood = true;
								}else {
									isValidationGood = false;
								}
							}
							
							if (!isValidationGood) {
								response.put("status", "failed");
								response.put("description", "No Handphone lama tidak sesuai.");
								String result = gson.toJson(response);
								String results = result.replaceAll("\"", "\\\\\"");
								response.put("signature",
									SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
								httpStatus = HttpStatus.OK;
							}else {
							
								LOG.info("claims payload " + claims.getAudience());
								LOG.info("Previous data : " + new Gson().toJson(mstUserMobileDto));
								LOG.info("Update data : " + new Gson().toJson(mstUserMobileDto));

								
								if (mstUserMobileDto.getNpwpNumber() == null
										|| "".equals(mstUserMobileDto.getNpwpNumber())) {
									LOG.info("NPWP NUMBER EMPTY DETECTED : " + mstUserMobileDto.getEmail() + " - "
											+ mstUserMobileDto.getName() + " - " + mstUserMobileDto.getPhone());
									LOG.info("PREVIOUS NPWP " + mstUserMobileDto.getNpwpNumber());
									mstUserMobileDto.setNpwpImage(mstUserMobileDto.getNpwpNumber());
								}

								System.out.println("log"+token);
								Pattern p = Pattern.compile(URL_REGEX);

								LOG.info(mstUserMobileDto.getName());
								byte[] fileByteBpkbMobilImage = new byte[0];
								MultipartFile bpkbMobilImage = null;

								LOG.info(mstUserMobileDto.getName());
								byte[] fileByteBpkbMotorImage = new byte[0];
								MultipartFile bpkbMotorImage = null;

								byte[] fileByteKtp = new byte[0];
								MultipartFile ktpImage = null;

								byte[] fileByteNpwp = new byte[0];
								MultipartFile npwpImage = null;

								byte[] fileByteKk = new byte[0];
								MultipartFile kkImage = null;

								byte[] fileBytePp = new byte[0];
								MultipartFile ppImage = null;

								Matcher m;
								if (mstUserMobileDto.getBpkbMobilImage() == null
										|| "".equals(mstUserMobileDto.getBpkbMobilImage())) {
									;
									bpkbMobilImage = null;
								} else {
									m = p.matcher(mstUserMobileDto.getBpkbMobilImage());
									if (m.find()) {
										bpkbMobilImage = null;
									} else {
										fileByteBpkbMobilImage = Base64.getDecoder()
												.decode(mstUserMobileDto.getBpkbMobilImage());
										bpkbMobilImage = new BASE64DecodedMultipartFile(fileByteBpkbMobilImage,
												"img.png");
									}
								}

								if (mstUserMobileDto.getBpkbMotorImage() == null
										|| "".equals(mstUserMobileDto.getBpkbMotorImage())) {
									;
									bpkbMotorImage = null;
								} else {
									m = p.matcher(mstUserMobileDto.getBpkbMotorImage());
									if (m.find()) {
										bpkbMotorImage = null;
									} else {
										fileByteBpkbMotorImage = Base64.getDecoder()
												.decode(mstUserMobileDto.getBpkbMotorImage());
										bpkbMotorImage = new BASE64DecodedMultipartFile(fileByteBpkbMotorImage,
												"img.png");
									}
								}

								if (mstUserMobileDto.getKtpImage() == null
										|| "".equals(mstUserMobileDto.getKtpImage())) {
									ktpImage = null;
								} else {
									m = p.matcher(mstUserMobileDto.getKtpImage());
									if (m.find()) {
										ktpImage = null;
									} else {
										fileByteKtp = Base64.getDecoder().decode(mstUserMobileDto.getKtpImage());
										ktpImage = new BASE64DecodedMultipartFile(fileByteKtp, "img.png");
									}
								}

								if (mstUserMobileDto.getNpwpImage() == null
										|| "".equals(mstUserMobileDto.getNpwpImage())) {
									npwpImage = null;
								} else {
									m = p.matcher(mstUserMobileDto.getNpwpImage());
									if (m.find()) {
										npwpImage = null;
									} else {
										fileByteNpwp = Base64.getDecoder().decode(mstUserMobileDto.getNpwpImage());
										npwpImage = new BASE64DecodedMultipartFile(fileByteNpwp, "img.png");
									}
								}

								if (mstUserMobileDto.getKkImage() == null || "".equals(mstUserMobileDto.getKkImage())) {
									kkImage = null;
								} else {
									m = p.matcher(mstUserMobileDto.getKkImage());
									if (m.find()) {
										kkImage = null;
									} else {
										fileByteKk = Base64.getDecoder().decode(mstUserMobileDto.getKkImage());
										kkImage = new BASE64DecodedMultipartFile(fileByteKk, "img.png");
									}
								}

								if (mstUserMobileDto.getPpImage() == null || "".equals(mstUserMobileDto.getPpImage())) {
									ppImage = null;
								} else {
									m = p.matcher(mstUserMobileDto.getPpImage());
									if (m.find()) {
										ppImage = null;
									} else {
										fileBytePp = Base64.getDecoder().decode(mstUserMobileDto.getPpImage());
										ppImage = new BASE64DecodedMultipartFile(fileBytePp, "img.png");
									}
								}
								mstUserMobileDto.setUserMobileStatus("01");
								mstUserMobileDto.setToken(token);
								System.out.println("LOG"+ token);
								String temporaryBirthDate = mstUserMobileDto.getBirthDate();
								mstUserMobileDto.setBirthDate(null); 
								
								String temporaryAcceptanceMoxa = mstUserMobileOpt.get().getAcceptanceMoxaMabroor();
								Date temporaryAcceptanceMoxaDate = mstUserMobileOpt.get().getAcceptanceMoxaMabroorDate();
								
								MstUserMobile mstUserMobile = ObjectMapperUtil.map(mstUserMobileDto,
										MstUserMobile.class);
//								try {
//									mstUserMobile.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(temporaryBirthDate)));
//									System.out.println("birthdate = " + new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(temporaryBirthDate)));
//									System.out.println("birthdate date format = " + new SimpleDateFormat("yyyy-MM-dd").parse(temporaryBirthDate));
//								} catch (ParseException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
								mstUserMobile.setAcceptanceMoxaMabroor(temporaryAcceptanceMoxa);
								mstUserMobile.setAcceptanceMoxaMabroorDate(temporaryAcceptanceMoxaDate);
								mstUserMobile.setBirthDate(temporaryBirthDate);
								
								System.out.println("LOG"+mstUserMobile.getToken());
								String statusSave = userMobileServ.save(ktpImage, npwpImage, bpkbMobilImage,
										bpkbMotorImage, kkImage, ppImage, mstUserMobile);

								if ("phone_existing".equals(statusSave)) {
									response.put("status", "failed");
									response.put("description", "No. Handphone sudah digunakan.");
									String result = gson.toJson(response);
//									String results = result.replaceAll("\"", "\\\\\"");
									response.put("signature",
											SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
									httpStatus = HttpStatus.OK;
								} else if ("email_existing".equals(statusSave)) {
									response.put("status", "failed");
									response.put("description", "Email sudah digunakan.");
									String result = gson.toJson(response);
//									String results = result.replaceAll("\"", "\\\\\"");
									response.put("signature",
											SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
									httpStatus = HttpStatus.OK;
								} else {
									response.put("status", "success");
									response.put("description", "Data kamu berhasil disimpan");
									String result = gson.toJson(response);
//									String results = result.replaceAll("\"", "\\\\\"");
									response.put("signature",
											SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
									httpStatus = HttpStatus.OK;
								}

							}
						} else {
							response.put("status", "failed");
							response.put("description",
									"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
							String result = gson.toJson(response);
//							String results = result.replaceAll("\"", "\\\\\"");
							response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
							httpStatus = HttpStatus.UNAUTHORIZED;
						}
					}
				}
			}

		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			ibe.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}
	
	

	@PostMapping(value = "/updateIdentity")
	@ResponseBody
	public ResponseEntity updateIdentity(@RequestBody MstUserMobileUpdateIdentityDto mstUserMobileUpdateDto,
			@RequestHeader(value = "appToken", required = true) String token) {

		Gson gson = new Gson();
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
//				String result = gson.toJson(response);
////				String results = result.replaceAll("\"", "\\\\\"");
//				response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				System.out.println(claims.getId());
				System.out.println(mstUserMobileOpt);

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
//					String result = gson.toJson(response);
////					String results = result.replaceAll("\"", "\\\\\"");
//					response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
//					mstUserMobile = mstUserMobileOpt.get();
//					System.out.println(claims.getId());
//					System.out.println(""+mstUserMobileDto.getId());
//					System.out.println(mstUserMobileOpt.get().getToken());
//					System.out.println(token);

//					String json = gson.toJson(mstUserMobileUpdateDto);
////					String jsons = json.replaceAll("\"", "\\\\\"");
//					String hash = SHA256Utils.encrypt(json + moxaApiKey + token + moxaSaltKey);
//					System.out.println("json " + json);
//					System.out.println("moxaApiKey " + moxaApiKey);
//					System.out.println("token " + token);
//					System.out.println("moxaSaltKey " + moxaSaltKey);
//					System.out.println(hash);
				}
				if (claims.getId().equals("" + mstUserMobileUpdateDto.getId())
						&& token.equals(mstUserMobileOpt.get().getToken())) {

					LOG.info("claims payload " + claims.getAudience());
					LOG.info("Previous data : " + new Gson().toJson(mstUserMobileUpdateDto));
					LOG.info("Update data : " + new Gson().toJson(mstUserMobileUpdateDto));
					
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
//					System.out.println(ktpImage);
//					System.out.println(npwpImage);
//					System.out.println(kkImage);
//					System.out.println(ppImage);
					System.out.println(mstUserMobile.getKtpImage());
					System.out.println(mstUserMobile.getNpwpImage());
					System.out.println(mstUserMobile.getPpImage());
					System.out.println(mstUserMobile.getKkImage());
					if (mstUserMobile != null && mstUserMobile.getId().equals(mstUserMobileUpdateDto.getId())) {
						
						if(mstUserMobileUpdateDto.getKtpImage() == null) {
							mstUserMobileUpdateDto.setKtpImage(mstUserMobile.getKtpImage());
						}
						if(mstUserMobileUpdateDto.getNpwpImage() == null) {
							mstUserMobileUpdateDto.setNpwpImage(mstUserMobile.getNpwpImage());
						}
						if(mstUserMobileUpdateDto.getKkImage() == null) {
							mstUserMobileUpdateDto.setKkImage(mstUserMobile.getKkImage());
						}
						if(mstUserMobileUpdateDto.getPpImage() == null) {
							mstUserMobileUpdateDto.setPpImage(mstUserMobile.getPpImage());
						}
						if(mstUserMobileUpdateDto.getKtpNumber() == null) {
							 mstUserMobileUpdateDto.setKtpNumber(mstUserMobile.getKtpNumber());
						}
						if(mstUserMobileUpdateDto.getNpwpNumber() == null) {
							mstUserMobileUpdateDto.setNpwpNumber(mstUserMobile.getNpwpNumber());
						}
					   
						
					}

					Pattern p = Pattern.compile(URL_REGEX);

					byte[] fileByteKtp = new byte[0];
					MultipartFile ktpImage = null;

					byte[] fileByteNpwp = new byte[0];
					MultipartFile npwpImage = null;

					byte[] fileByteKk = new byte[0];
					MultipartFile kkImage = null;

					byte[] fileBytePp = new byte[0];
					MultipartFile ppImage = null;

					Matcher m;

					if (mstUserMobileUpdateDto.getKtpImage() == null
							|| "".equals(mstUserMobileUpdateDto.getKtpImage())) {
						ktpImage = null;
					} else {
						m = p.matcher(mstUserMobileUpdateDto.getKtpImage());
						if (m.find()) {
							ktpImage = null;
						} else {
							fileByteKtp = Base64.getDecoder().decode(mstUserMobileUpdateDto.getKtpImage());
							ktpImage = new BASE64DecodedMultipartFile(fileByteKtp, "img.png");
						}
					}

					if (mstUserMobileUpdateDto.getNpwpImage() == null
							|| "".equals(mstUserMobileUpdateDto.getNpwpImage())) {
						npwpImage = null;
					} else {
						m = p.matcher(mstUserMobileUpdateDto.getNpwpImage());
						if (m.find()) {
							System.out.println("aaaaa");
							npwpImage = null;
						} else {
							System.out.println("bbbb");
							fileByteNpwp = Base64.getDecoder().decode(mstUserMobileUpdateDto.getNpwpImage());
							npwpImage = new BASE64DecodedMultipartFile(fileByteNpwp, "img.png");
						}
					}

					if (mstUserMobileUpdateDto.getKkImage() == null || "".equals(mstUserMobileUpdateDto.getKkImage())) {
						kkImage = null;
					} else {
						m = p.matcher(mstUserMobileUpdateDto.getKkImage());
						if (m.find()) {
							kkImage = null;
						} else {
							fileByteKk = Base64.getDecoder().decode(mstUserMobileUpdateDto.getKkImage());
							kkImage = new BASE64DecodedMultipartFile(fileByteKk, "img.png");
						}
					}

					if (mstUserMobileUpdateDto.getPpImage() == null || "".equals(mstUserMobileUpdateDto.getPpImage())) {
						ppImage = null;
					} else {
						m = p.matcher(mstUserMobileUpdateDto.getPpImage());
						if (m.find()) {
							ppImage = null;
						} else {
							fileBytePp = Base64.getDecoder().decode(mstUserMobileUpdateDto.getPpImage());
							ppImage = new BASE64DecodedMultipartFile(fileBytePp, "img.png");
						}
					}

					System.out.println("test");
					System.out.println(ktpImage);
					System.out.println(npwpImage);
					System.out.println(kkImage);
					System.out.println(ppImage);
					System.out.println(mstUserMobile.getKtpImage());
					System.out.println(mstUserMobile.getNpwpImage());
					System.out.println(mstUserMobile.getPpImage());
					System.out.println(mstUserMobile.getKkImage());
					
					mstUserMobile.setKtpNumber(mstUserMobileUpdateDto.getKtpNumber());
					mstUserMobile.setNpwpNumber(mstUserMobileUpdateDto.getNpwpNumber());
				
						userMobileServ.updateIdent(ktpImage, npwpImage, kkImage, ppImage, mstUserMobile);						

						response.put("status", "success");
						response.put("description", "Data kamu berhasil disimpan");
//										String result = gson.toJson(response);
////										String results = result.replaceAll("\"", "\\\\\"");
//										response.put("signature",
//												SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
						httpStatus = HttpStatus.OK;
//					}
//						if (mstUserMobileUpdateDto.getKtpImage() !=null) {
//						mstUserMobile.setId(mstUserMobileUpdateDto.getId());					
//						mstUserMobile.setKtpImage(mstUserMobileUpdateDto.getKtpImage());						
//						mstUserMobile.setKtpNumber(mstUserMobileUpdateDto.getKtpNumber());
//						mstUserMobile.setNpwpNumber(mstUserMobileUpdateDto.getNpwpNumber());
//						userMobileServ.saveIdentityKtp(ktpImage, mstUserMobile);					
//
//						response.put("status", "success");
//						response.put("description", "Data kamu berhasil disimpan");
////										String result = gson.toJson(response);
//////										String results = result.replaceAll("\"", "\\\\\"");
////										response.put("signature",
////												SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
//						httpStatus = HttpStatus.OK;
//					}
//					if (mstUserMobileUpdateDto.getNpwpImage() !=null) {
//						mstUserMobile.setId(mstUserMobileUpdateDto.getId());					
//						mstUserMobile.setNpwpImage(mstUserMobileUpdateDto.getNpwpImage());						
//						mstUserMobile.setKtpNumber(mstUserMobileUpdateDto.getKtpNumber());
//						mstUserMobile.setNpwpNumber(mstUserMobileUpdateDto.getNpwpNumber());
//						userMobileServ.saveIdentityNpwp(npwpImage, mstUserMobile);					
//
//						response.put("status", "success");
//						response.put("description", "Data kamu berhasil disimpan");
////										String result = gson.toJson(response);
//////										String results = result.replaceAll("\"", "\\\\\"");
////										response.put("signature",
////												SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
//						httpStatus = HttpStatus.OK;
//					}
//					if (mstUserMobileUpdateDto.getKkImage() !=null) {
//						mstUserMobile.setId(mstUserMobileUpdateDto.getId());					
//						mstUserMobile.setKkImage(mstUserMobileUpdateDto.getKkImage());						
//						mstUserMobile.setKtpNumber(mstUserMobileUpdateDto.getKtpNumber());
//						mstUserMobile.setNpwpNumber(mstUserMobileUpdateDto.getNpwpNumber());
//						userMobileServ.saveIdentityKk(kkImage, mstUserMobile);						
//
//						response.put("status", "success");
//						response.put("description", "Data kamu berhasil disimpan");
////										String result = gson.toJson(response);
//////										String results = result.replaceAll("\"", "\\\\\"");
////										response.put("signature",
////												SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
//						httpStatus = HttpStatus.OK;
//					}
//					if (mstUserMobileUpdateDto.getPpImage() !=null) {
//						mstUserMobile.setId(mstUserMobileUpdateDto.getId());					
//						mstUserMobile.setPpImage(mstUserMobileUpdateDto.getPpImage());						
//						mstUserMobile.setKtpNumber(mstUserMobileUpdateDto.getKtpNumber());
//						mstUserMobile.setNpwpNumber(mstUserMobileUpdateDto.getNpwpNumber());
//						userMobileServ.saveIdentityPp(ppImage, mstUserMobile);						
//
//						response.put("status", "success");
//						response.put("description", "Data kamu berhasil disimpan");
////										String result = gson.toJson(response);
//////										String results = result.replaceAll("\"", "\\\\\"");
////										response.put("signature",
////												SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
//						httpStatus = HttpStatus.OK;
//					
//					}
//					System.out.println(mstUserMobileUpdateDto.getKkImage());
//					userMobileServ.updateUserEntity(ktpImage, npwpImage, kkImage, ppImage, mstUserMobile.getId());
//					System.out.println(mstUserMobile.getPpImage());
//					System.out.println(mstUserMobile.getKkImage());
//					System.out.println(mstUserMobile.getNpwpImage());
//					System.out.println(mstUserMobile.getKtpImage());
					response.put("status", "success");
					response.put("description", "Data kamu berhasil disimpan");
//									String result = gson.toJson(response);
////									String results = result.replaceAll("\"", "\\\\\"");
//									response.put("signature",
//											SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
					httpStatus = HttpStatus.OK;
				} else {
					response.put("status", "failed");
					response.put("description", "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
//							String result = gson.toJson(response);
////							String results = result.replaceAll("\"", "\\\\\"");
//							response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
					httpStatus = HttpStatus.UNAUTHORIZED;
				}
				
			}

		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
//			String result = gson.toJson(response);
////			String results = result.replaceAll("\"", "\\\\\"");
//			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			ibe.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
//			String result = gson.toJson(response);
////			String results = result.replaceAll("\"", "\\\\\"");
//			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
//			String result = gson.toJson(response);
////			String results = result.replaceAll("\"", "\\\\\"");
//			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
////			String result = gson.toJson(response);
//////			String results = result.replaceAll("\"", "\\\\\"");
//			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
//			String result = gson.toJson(response);
////			String results = result.replaceAll("\"", "\\\\\"");
//			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
//			String result = gson.toJson(response);
////			String results = result.replaceAll("\"", "\\\\\"");
//			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}
	

	@PostMapping(value = "/sendOtpPengajuan")
	@ResponseBody
	public ResponseEntity sendOtp(@RequestBody MstOtpDto mstOtp,
			@RequestHeader(value = "x-signature-moxa", required = true) String signature) {

		Gson gson = new Gson();
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		String json = gson.toJson(mstOtp);
//		String jsons = json.replaceAll("\"", "\\\\\"");
		String hash = SHA256Utils.encrypt(json + moxaApiKey + moxaSaltKey);
		System.out.println("json " + json);
		System.out.println("moxaApiKey " + moxaApiKey);
		System.out.println("moxaSaltKey " + moxaSaltKey);
		System.out.println(hash);
		if (!signature.equalsIgnoreCase(hash)) {
			response.put("status", "failed");
			response.put("description", "you are not authorized");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} else {
			String temporaryBirthDate = mstOtp.getBirthDate();
			mstOtp.setBirthDate(null);
			MstOtp mstOtpDto = ObjectMapperUtil.map(mstOtp, MstOtp.class);
//			try {
//				mstOtpDto.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse(temporaryBirthDate));
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			mstOtpDto.setBirthDate(temporaryBirthDate);
			System.out.println("masuk resend otp");
			String otpStatus = otpServ.resendGeneral(mstOtpDto);

			SysParam tmpParam = sysParamService.getByValueType("otp_retry");
			SysParam sysParamBlockOtp = sysParamService.getByValueType("otp_expired");
			String minutesParam = sysParamBlockOtp.getValueName() + " minute";
			List<MstOtp> listOtp = new ArrayList();
//			if(mstOtp.getEncryptionStatus()!=null) {
				listOtp = otpServ.findByExpiredDate(AES256Utils.encrypt(mstOtp.getPhoneNumber()), new Date(), minutesParam);
//			}else {
//				listOtp = otpServ.findByExpiredDate(mstOtp.getPhoneNumber(), new Date(), minutesParam);
//			}

			CounterOtpDto count = new CounterOtpDto();
			count.setCurrentCount(listOtp.size());
			count.setMaxCount(tmpParam.getValueName());

			if ("success".equals(otpStatus)) {
				response.put("status", "success");
				response.put("description", "OTP berhasil dikirimkan ke nomor kamu!");
				response.put("data", count);
				String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
				response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
				httpStatus = HttpStatus.OK;
			} else if ("insufficient".equals(otpStatus)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan pada sistem, silakan coba beberapa saat lagi.");
				response.put("data", count);
				String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
				response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else if ("failedlocked".equals(otpStatus)) {

				MstUserMobile user = new MstUserMobile();
//				if(mstOtp.getEncryptionStatus()!=null) {
					user = userMobileRepo.getOneByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//				}else {
//					user = userMobileRepo.getOneByPhoneNumber(mstOtp.getPhoneNumber());
//				}

				Date now = new Date();
				Date lock = user.getPinLockDateTime();

				Long remainingTime = (lock.getTime() - now.getTime());
				Long remainingTimeMinutes = (remainingTime / (60 * 1000) % 60);
				Long remainingTimeSecond = (remainingTime / 1000 % 60);

//			System.out.println("SEND OTP PENGAJUAN DATE NOW == " + now);
//			System.out.println("SEND OTP PENGAJUAN PIN LOCK DATE TIME == " + lock);
//			System.out.println("SEND OTP PENGAJUAN REMAINING TIME == " + remainingTime);
//			System.out.println("SEND OTP PENGAJUAN REMAINING TIME MINUTES YG DIPAKAI == " + remainingTimeMinutes);

				if (remainingTimeMinutes == 0) {
					response.put("status", "failed");
					response.put("description", "Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
							+ remainingTimeSecond + " detik lagi.");
					response.put("type", "block");
					response.put("data", count);

					Map<String, Object> errorMessageMap = new HashMap<>();
					errorMessageMap.put("title", "Akun Terkunci");
					errorMessageMap.put("message", "Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
							+ remainingTimeSecond + " detik lagi.");
					response.put("errorMessage", errorMessageMap);
					String result = gson.toJson(response);
//				String results = result.replaceAll("\"", "\\\\\"");
					response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					response.put("status", "failed");
					response.put("description", "Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
							+ remainingTimeMinutes + " menit lagi.");
					response.put("type", "block");
					response.put("data", count);

					Map<String, Object> errorMessageMap = new HashMap<>();
					errorMessageMap.put("title", "Akun Terkunci");
					errorMessageMap.put("message", "Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
							+ remainingTimeMinutes + " menit lagi.");
					response.put("errorMessage", errorMessageMap);
					String result = gson.toJson(response);
//				String results = result.replaceAll("\"", "\\\\\"");
					response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
					httpStatus = HttpStatus.UNAUTHORIZED;
				}

			} else {
				response.put("status", "failed");
				response.put("description", "Silakan coba beberapa saat lagi.");
//			response.put("type", "block");
				response.put("data", count);
				String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
				response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
				httpStatus = HttpStatus.OK;
			}
		}
		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/sendOtp")
	@ResponseBody
	public LinkedHashMap<String, Object> sendOtpGeneral(@RequestBody MstOtp mstOtp) {
		String otpStatus = otpServ.resendGeneral(mstOtp);
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();

		SysParam tmpParam = sysParamService.getByValueType("otp_retry");
		SysParam sysParamBlockOtp = sysParamService.getByValueType("otp_expired");
		String minutesParam = sysParamBlockOtp.getValueName() + " minute";
		List<MstOtp> listOtp = new ArrayList();
//		if(mstOtp.getEncryptionStatus()!=null) {
			listOtp = otpServ.findByExpiredDate(AES256Utils.encrypt(mstOtp.getPhoneNumber()), new Date(), minutesParam);
//		}else {
//			listOtp = otpServ.findByExpiredDate(mstOtp.getPhoneNumber(), new Date(), minutesParam);
//		}
//		System.out.println("OTP ID = " + listOtp.get(0).getId());
		
		CounterOtpDto count = new CounterOtpDto();
		count.setCurrentCount(listOtp.size());
		count.setMaxCount(tmpParam.getValueName());

		if ("success".equals(otpStatus)) {
			response.put("status", "success");
			response.put("description", "OTP berhasil dikirimkan ke nomor kamu!");
			response.put("data", count);
		} else if ("insufficient".equals(otpStatus)) {
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan pada sistem, silakan coba beberapa saat lagi.");
			response.put("data", count);
		} else if ("failedlocked".equals(otpStatus)) {
			MstUserMobile user = new MstUserMobile();
//			if(mstOtp.getEncryptionStatus()!=null) {
				user = userMobileRepo.getOneByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//			}else {
//				user = userMobileRepo.getOneByPhoneNumber(mstOtp.getPhoneNumber());
//			}

			Date now = new Date();
			Date lock = user.getPinLockDateTime();

			Long remainingTime = (lock.getTime() - now.getTime());
			Long remainingTimeMinutes = (remainingTime / (60 * 1000) % 60);
			Long remainingTimeSecond = (remainingTime / 1000 % 60);

//			System.out.println("SEND OTP DATE NOW == " + now);
//			System.out.println("SEND OTP PIN LOCK DATE TIME == " + lock);
//			System.out.println("SEND OTP REMAINING TIME == " + remainingTime);
//			System.out.println("SEND OTP REMAINING TIME MINUTES YG DIPAKAI == " + remainingTimeMinutes);

			if (remainingTimeMinutes == 0) {
				response.put("status", "failed");
				response.put("description", "Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
						+ remainingTimeSecond + " detik lagi.");
				response.put("type", "block");
				response.put("data", count);

				Map<String, Object> errorMessageMap = new HashMap<>();
				errorMessageMap.put("title", "Akun Terkunci");
				errorMessageMap.put("message", "Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
						+ remainingTimeSecond + " detik lagi.");
				response.put("errorMessage", errorMessageMap);
			} else {
				response.put("status", "failed");
				response.put("description", "Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
						+ remainingTimeMinutes + " menit lagi.");
				response.put("type", "block");
				response.put("data", count);

				Map<String, Object> errorMessageMap = new HashMap<>();
				errorMessageMap.put("title", "Akun Terkunci");
				errorMessageMap.put("message", "Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
						+ remainingTimeMinutes + " menit lagi.");
				response.put("errorMessage", errorMessageMap);
			}
		} else {
			response.put("status", "failed");
			response.put("description", "Silakan coba beberapa saat lagi.");
//			response.put("type", "block");
			response.put("data", count);
		}
		return response;
	}

	@PostMapping(value = "/verifyOtpPengajuan")
	@ResponseBody
	public ResponseEntity verifyOtp(@RequestBody MstOtpVreifyDto mstOtp,
			@RequestHeader(value = "x-signature-moxa", required = true) String signature) {

		Gson gson = new Gson();
		LinkedHashMap<String, Object> res = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		String json = gson.toJson(mstOtp);
		String hash = SHA256Utils.encrypt(json + moxaApiKey + moxaSaltKey);
		System.out.println("json " + json);
		System.out.println("moxaApiKey " + moxaApiKey);
		System.out.println("moxaSaltKey " + moxaSaltKey);
		System.out.println("server hash : " + hash);
		LOG.info("client hash : " + signature);
		if (!signature.equalsIgnoreCase(hash)) {
			res.put("status", "failed");
			res.put("description", "you are not authorized");
			String result = gson.toJson(res);
			res.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} else {
			MstOtp mstOtpDto = ObjectMapperUtil.map(mstOtp, MstOtp.class);
			LinkedHashMap<String, Object> data = otpServ.verify(mstOtpDto);

			MstOtp mstOtp1 = new MstOtp();
				mstOtp1 = otpServ.findByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
			SysParam tmpParam = sysParamService.getByValueType("otp_retry");
			List<MstOtpCount> listOtpCount = otpCountServ.findByMstOtp(mstOtp1);

			CounterOtpDto count = new CounterOtpDto();
			count.setCurrentCount(listOtpCount.size());
			count.setMaxCount(tmpParam.getValueName());

			if (data.get("token") != null && !"".equals(data.get("token"))) {

				res.put("status", "success");
				res.put("description", "OTP berhasil diverifikasi!");
				res.put("data", count);
				res.put("token", data.get("token"));
				res.put("moxaReffId", data.get("moxaReffId"));
				String result = gson.toJson(res);
				res.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
				httpStatus = HttpStatus.OK;

				if (data.get("userProfile") != null) {
					MstUserMobile mstUserMobile = (MstUserMobile) data.get("userProfile");
					LinkedHashMap<String, Object> tmpObj = new LinkedHashMap<String, Object>();
					tmpObj.put("userMobileUuid", mstUserMobile.getUserMobileUuid());

					res.put("userProfile", tmpObj);
				} else {
					res.put("userProfile", data.get("userProfile"));
				}
			} else {
				res.put("status", "failed");
				res.put("description", data.get("message"));
				res.put("userProfile", data.get("userProfile"));
				res.put("data", count);
				String result = gson.toJson(res);
				res.put("signature", SHA256Utils.encrypt(result + moxaApiKey + moxaSaltKey));
				httpStatus = HttpStatus.OK;
			}
		}
		return new ResponseEntity<>(res, httpStatus);
	}

	@PostMapping(value = "/verifyOtp")
	@ResponseBody
	public Map<String, Object> verifyOtpGeneral(@RequestBody MstOtp mstOtp) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		String token = otpServ.verifyGeneral(mstOtp);
		MstOtp mstOtp1 = new MstOtp();
			mstOtp1 = otpServ.findByPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
		SysParam tmpParam = sysParamService.getByValueType("otp_retry");
		List<MstOtpCount> listOtpCount = otpCountServ.findByMstOtp(mstOtp1);

		CounterOtpDto count = new CounterOtpDto();
		count.setCurrentCount(listOtpCount.size());
		count.setMaxCount(tmpParam.getValueName());

		if (token != null && "success".equals(token)) {
			response.put("status", "success");
			response.put("description", "OTP berhasil diverifikasi!");
			response.put("data", count);
		} else if (token != null && !"".equals(token)) {
			response.put("status", "failed2");
			response.put("description", token);
			response.put("data", count);
		} else {
			response.put("status", "failed");
			response.put("description", "Gagal diverifikasi. OTP yang kamu input belum sesuai :(");
			response.put("data", count);
		}
		return response;
	}

	@GetMapping(value = "/checkToken")
	@ResponseBody
	public ResponseEntity checkToken(@RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang.(00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						if ("00".equals(mstUserMobile.getUserMobileStatus())) {
							response.put("status", "failed");
							response.put("description",
									"Hai %{name}.\nAkun Anda sudah dinonaktifkan. Silakan daftarkan kembali nomor ponsel Anda untuk melakukan pengajuan di aplikasi moxa.");
							httpStatus = HttpStatus.UNAUTHORIZED;
						} else {
							response.put("status", "success");
							response.put("description", "valid token");
							httpStatus = HttpStatus.OK;
						}
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/setPin")
	@ResponseBody
	public ResponseEntity setPin(@RequestBody UserMobileDto userMobileDto,
			@RequestHeader(value = "appToken", required = true) String token) {
		LOG.info(token);

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();

					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {

						String encrpytedPin = SHA256Utils.encrypt(userMobileDto.getPin());

						userMobileServ.updatePin(encrpytedPin, mstUserMobile.getId());

						response.put("status", "success");
						response.put("description", "PIN berhasil diubah.");

						//CLEAR OTP COUNTER
						// otpServ.updateAllVerificationByPhoneNumber(mstUserMobile.getPhone());

						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}
	
	@PostMapping(value = "/changePin")
	@ResponseBody
	public ResponseEntity changePin(@RequestBody UserMobileChangePinDto userMobileDto,
			@RequestHeader(value = "appToken", required = true) String token,
			@RequestHeader(value = "x-signature-moxa", required = true) String signature) {
		LOG.info(token);

		Gson gson = new Gson();
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				String result = gson.toJson(response);
				response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					String result = gson.toJson(response);
					response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					
					System.out.println(claims.getId());					
					
					System.out.println(token);

					String json = gson.toJson(userMobileDto);
					String hash = SHA256Utils.encrypt(json + moxaApiKey + token + moxaSaltKey);
					System.out.println(json);
					System.out.println(hash);
					if (!signature.equalsIgnoreCase(hash)) {
						response.put("status", "failed");
						response.put("description", "you are not authorized");
						String result = gson.toJson(response);
						response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
						httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
					} else {
						MstUserMobile mstUserMobile = mstUserMobileOpt.get();

						if (claims.getId().equals("" + mstUserMobile.getId())
								&& token.equals(mstUserMobile.getToken())) {
							String encrpytedOldPin = SHA256Utils.encrypt(userMobileDto.getOldPin());
							if(mstUserMobile.getPin() != null && mstUserMobile.getPin().equalsIgnoreCase(encrpytedOldPin)) {
								String encrpytedPin = SHA256Utils.encrypt(userMobileDto.getPin());

								userMobileServ.updatePin(encrpytedPin, mstUserMobile.getId());

								response.put("status", "success");
								response.put("description", "PIN berhasil diubah.");
								String result = gson.toJson(response);
								response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));

								//CLEAR OTP COUNTER
								// otpServ.updateAllVerificationByPhoneNumber(mstUserMobile.getPhone());

								httpStatus = HttpStatus.OK;
							}else {
								response.put("status", "failed");
								response.put("description",
										"Ganti Pin Gagal");
								String result = gson.toJson(response);
								response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
								httpStatus = HttpStatus.UNAUTHORIZED;
							}
							
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						String result = gson.toJson(response);
						response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			String result = gson.toJson(response);
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			String result = gson.toJson(response);
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}


	@PostMapping(value = "/verifyPin")
	@ResponseBody
	public ResponseEntity verifyPin(@RequestBody UserMobileDto userMobileDto,
			@RequestHeader(value = "appToken", required = true) String token,
			@RequestHeader(value = "x-signature-moxa", required = true) String signature) {
		LOG.info(token);

		Gson gson = new Gson();
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				String result = gson.toJson(response);
//				String results = result.replaceAll("\"", "\\\\\"");
				response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					String result = gson.toJson(response);
//					String results = result.replaceAll("\"", "\\\\\"");
					response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
//					mstUserMobile = mstUserMobileOpt.get();
//					System.out.println(claims.getId());
//					System.out.println(""+mstUserMobileDto.getId());
//					System.out.println(mstUserMobileOpt.get().getToken());
//					System.out.println(token);

					String json = gson.toJson(userMobileDto);
//					String jsons = json.replaceAll("\"", "\\\\\"");
					String hash = SHA256Utils.encrypt(json + moxaApiKey + token + moxaSaltKey);
//					System.out.println(jsons);
					if (!signature.equalsIgnoreCase(hash)) {
						response.put("status", "failed");
						response.put("description", "you are not authorized");
						String result = gson.toJson(response);
//						String results = result.replaceAll("\"", "\\\\\"");
						response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
						httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
					} else {
						MstUserMobile mstUserMobile = mstUserMobileOpt.get();

						if (claims.getId().equals("" + mstUserMobile.getId())
								&& token.equals(mstUserMobile.getToken())) {

							String encrpytedPin = SHA256Utils.encrypt(userMobileDto.getPin());

							SysParam tmpParam = sysParamService.getByValueType("pin_lock");

							// RESET LOCK DATE AND COUNT
							if (mstUserMobile.getPinLockDateTime() != null
									&& mstUserMobile.getPinLockDateTime().compareTo(new Date()) <= 0) {
								userMobileServ.updateCountPin(new Long(0), mstUserMobile.getId());
								userMobileServ.resetLockTImePin(mstUserMobile.getId());
								userMobileServ.updateLockStatus("NORMAL", mstUserMobile.getId());

								mstUserMobile = mstUserMobileOpt.get();
								mstUserMobile.setCountPin(new Long(0));
								mstUserMobile.setPinLockDateTime(null);
								mstUserMobile.setPinLockStatus("NORMAL");
							}

							if (encrpytedPin.equals(mstUserMobile.getPin())
									&& ("NORMAL".equals(mstUserMobile.getPinLockStatus())
											|| mstUserMobile.getPinLockStatus() == null)) {
								response.put("status", "success");
								response.put("description", "succesfully verify pin");
								String result = gson.toJson(response);
//								String results = result.replaceAll("\"", "\\\\\"");
								response.put("signature",
										SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));

								userMobileServ.resetLockTImePin(mstUserMobile.getId());
								userMobileServ.updateCountPin(new Long(0), mstUserMobile.getId());
								userMobileServ.updateLockStatus("NORMAL", mstUserMobile.getId());

								//CLEAR OTP COUNTER
								// otpServ.updateAllVerificationByPhoneNumber(mstUserMobile.getPhone());

							} else {
								SysParam tmpRetry = sysParamService.getByValueType("pin_retry");
								int maxRetryPin = 3;
								if (tmpRetry != null && !"".equals(tmpRetry.getValueName())) {
									try {
										maxRetryPin = Integer.parseInt(tmpRetry.getValueName());
									} catch (NumberFormatException nfe) {
										nfe.printStackTrace();
										maxRetryPin = 3;
									}
								}

								Long tmpCount = mstUserMobile.getCountPin() != null ? mstUserMobile.getCountPin()
										: new Long(0);
								tmpCount = tmpCount + 1;

								if (tmpCount >= maxRetryPin) {

									if (tmpCount == maxRetryPin) {
										Calendar date = Calendar.getInstance();
										long t = date.getTimeInMillis();

										Date tmpExpire = new Date(
												t + (Integer.valueOf(tmpParam.getValueName()) * 60000));

										userMobileServ.updateCountPin(tmpCount, mstUserMobile.getId());
										userMobileServ.updateLockTimePin(tmpExpire, mstUserMobile.getId());
										userMobileServ.updateLockStatus("LOCKED", mstUserMobile.getId());

										mstUserMobile.setPinLockDateTime(tmpExpire);
									}

									Date now = new Date();
									Date lock = mstUserMobile.getPinLockDateTime();

									Long remainingTime = (lock.getTime() - now.getTime());
									Long remainingTimeMinutes = (remainingTime / (60 * 1000) % 60);
									Long remainingTimeSecond = (remainingTime / 1000 % 60);

//								System.out.println("SET PIN DATE NOW == " + now);
//								System.out.println("SET PIN PIN LOCK DATE TIME == " + lock);
//								System.out.println("SET PIN REMAINING TIME == " + remainingTime);
//								System.out.println("SET PIN REMAINING TIME MINUTES YG DIPAKAI == " + remainingTimeMinutes);

									if (remainingTimeMinutes == 0) {
										response.put("status", "failed");
										response.put("description",
												"Maaf, proses verifikasi anda telah di block. silahkan tunggu "
														+ remainingTimeSecond + " detik");
										response.put("type", "block");
										String result = gson.toJson(response);
//									String results = result.replaceAll("\"", "\\\\\"");
										response.put("signature",
												SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));

										Map<String, Object> errorMessageMap = new HashMap<>();
										errorMessageMap.put("title", "Akun Terkunci");
										errorMessageMap.put("message",
												"Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
														+ remainingTimeSecond + " detik lagi.");
										response.put("errorMessage", errorMessageMap);
									} else {
										response.put("status", "failed");
										response.put("description",
												"Maaf, proses verifikasi anda telah di block. silahkan tunggu "
														+ remainingTimeMinutes + " menit");
										response.put("type", "block");

										Map<String, Object> errorMessageMap = new HashMap<>();
										errorMessageMap.put("title", "Akun Terkunci");
										errorMessageMap.put("message",
												"Terlalu banyak percobaan masuk, akun kamu terkunci. Silakan coba "
														+ remainingTimeMinutes + " menit lagi.");
										response.put("errorMessage", errorMessageMap);
										String results = gson.toJson(response);
//									String results = result.replaceAll("\"", "\\\\\"");
										response.put("signature",
												SHA256Utils.encrypt(results + moxaApiKey + token + moxaSaltKey));
									}

								} else {
									response.put("status", "failed");
									if ((maxRetryPin - tmpCount) == 1) {
										response.put("description", "PIN masih salah, silakan coba 1 kali lagi.");
										String result = gson.toJson(response);
//									String results = result.replaceAll("\"", "\\\\\"");
										response.put("signature",
												SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
									} else {
										response.put("description", "PIN salah, silakan coba lagi.");
										String result = gson.toJson(response);
//									String results = result.replaceAll("\"", "\\\\\"");
										response.put("signature",
												SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
									}
									userMobileServ.updateCountPin(tmpCount, mstUserMobile.getId());
								}
							}

							httpStatus = HttpStatus.OK;
						} else {
							response.put("status", "failed");
							response.put("description",
									"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
							String result = gson.toJson(response);
//						String results = result.replaceAll("\"", "\\\\\"");
							response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
							httpStatus = HttpStatus.UNAUTHORIZED;
						}
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			np.printStackTrace();
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			String result = gson.toJson(response);
//			String results = result.replaceAll("\"", "\\\\\"");
			response.put("signature", SHA256Utils.encrypt(result + moxaApiKey + token + moxaSaltKey));
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@GetMapping(value = "/checkPin")
	@ResponseBody
	public ResponseEntity checkPin(@RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		System.out.println("checkPin : token " + token);

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang.(00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				System.out.println("checkPin : token " + claims.getId());
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
				System.out.println("mstUserMobileOpt.isPresent() " + mstUserMobileOpt.isPresent());
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						if (mstUserMobile.getPin() == null) {
							response.put("status", "failed");
							response.put("description", "Pin Belum diset.");
							httpStatus = HttpStatus.OK;
						} else {
							response.put("status", "success");
							response.put("description", "Pin sudah diset");
							httpStatus = HttpStatus.OK;
						}
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/checkPhoneNumber")
	@ResponseBody
	public ResponseEntity checkPhoneNumber(@RequestBody MstOtp mstOtp,
			@RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang.(00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						String checkPhone = "";
//						if(mstOtp.getEncryptionStatus()!=null) {
							checkPhone = userMobileServ.checkPhoneNumber(AES256Utils.encrypt(mstOtp.getPhoneNumber()));
//						}else {
//							checkPhone = userMobileServ.checkPhoneNumber(mstOtp.getPhoneNumber());
//						}
						if ("success".equals(checkPhone)) {
							response.put("status", "success");
							response.put("description", "valid phone number");
						} else {
							response.put("status", "failed");
							response.put("description",
									"Nomor baru anda telah terdaftar. Silakan menggunakan nomor lain.");
						}

						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/updateFirebaseToken")
	@ResponseBody
	public ResponseEntity updateFirebaseToken(@RequestBody MstUserMobile userMobile,
			@RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang.(00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {

						LOG.info("Previous data : " + new Gson().toJson(mstUserMobile));
						LOG.info("Update data : " + new Gson().toJson(userMobile));

						if (userMobile.getNpwpNumber() == null || "".equals(userMobile.getNpwpNumber())) {
							LOG.info("NPWP NUMBER EMPTY DETECTED : " + userMobile.getEmail() + " - "
									+ userMobile.getName() + " - " + userMobile.getPhone());
							LOG.info("PREVIOUS NPWP " + mstUserMobile.getNpwpNumber());
						}

						userMobileServ.updateFirebaseToken(userMobile.getFirebaseToken(), mstUserMobile.getId());
						response.put("status", "success");
						response.put("description", "Data successfully updated");
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/updateFingerprintToggle")
	@ResponseBody
	public ResponseEntity updateFingerprintToggle(@RequestBody MstUserMobile userMobile,
			@RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang.(00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {

						LOG.info("Previous data : " + new Gson().toJson(mstUserMobile));
						LOG.info("Update data : " + new Gson().toJson(userMobile));

						if (userMobile.getNpwpNumber() == null || "".equals(userMobile.getNpwpNumber())) {
							LOG.info("NPWP NUMBER EMPTY DETECTED : " + userMobile.getEmail() + " - "
									+ userMobile.getName() + " - " + userMobile.getPhone());
							LOG.info("PREVIOUS NPWP " + mstUserMobile.getNpwpNumber());
						}

						userMobileServ.updateFingerprintToggle(userMobile.getFingerprintToggle(),
								mstUserMobile.getId());
						response.put("status", "success");
						response.put("description", "Data successfully updated");
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}

	@PostMapping(value = "/phoneNumberExist")
	@ResponseBody
	public LinkedHashMap<String, Object> save(@RequestBody MstUserMobile mstUserMobile) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();

		String checkPhone = "";
//		if(mstUserMobile.getEncryptionStatus()!=null) {
			checkPhone = userMobileServ.checkPhoneNumber(AES256Utils.encrypt(mstUserMobile.getPhone()));
//		}else {
		System.out.println("phone number check = " + mstUserMobile.getPhone());
//			checkPhone = userMobileServ.checkPhoneNumber(mstUserMobile.getPhone());
//		}

		if ("success".equals(checkPhone)) {
			response.put("status", "failed");
			response.put("description",
					"Ups! Nomor kamu belum terdaftar.Silakan klik tombol di bawah ini untuk melakukan pendaftaran.");
		} else {
			response.put("status", "success");
			response.put("description", "Valid phone number");
		}
		return response;
	}
	
	@PostMapping(value = "/checkMoxaExistingUser")
	@ResponseBody
	public ResponseEntity<LinkedHashMap<String, Object>> checkMoxaExistingUser(@RequestBody UserMobilePhoneDto userMobilePhoneDto, @RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;
		
		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang.(00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {

						boolean needConsentAcceptance = userMobileServ.checkMoxaExistingUser(AES256Utils.encrypt(userMobilePhoneDto.getPhone()));
						
						LOG.info(AES256Utils.encrypt(userMobilePhoneDto.getPhone()) + " - needConsentAcceptance " + needConsentAcceptance);
						
						response.put("status", "success");
						response.put("description", "Successfully get data");
						
						Map<String, Object> map = new HashMap<>();
						map.put("needConsentAcceptance", needConsentAcceptance);
						response.put("data", map);
						httpStatus = HttpStatus.OK;
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
	}
	
	@GetMapping(value = "/acceptanceMoxaMabroor/{state}")
	@ResponseBody
	public ResponseEntity<LinkedHashMap<String, Object>> acceptanceMoxaMabroor(@PathVariable String state, @RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		HttpStatus httpStatus = null;
		
		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang.(00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));

				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobile = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
						
						LOG.info("[-acceptanceMoxaMabroor-] : " + AES256Utils.encrypt(mstUserMobile.getPhone()));

						boolean result = userMobileServ.acceptanceMoxaMabroor(state, AES256Utils.encrypt(mstUserMobile.getPhone()));
						
						LOG.info("[-acceptanceMoxaMabroor-] result : " + result);
						
						if(result) {
							response.put("status", "success");
							response.put("description", "Successfully accept consent");
							httpStatus = HttpStatus.OK;
						}else {
							response.put("status", "failed");
							response.put("description", "Consent already accepted");
							httpStatus = HttpStatus.OK;
						}
						
						
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
			}
		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
			LOG.info(re.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, httpStatus);
		
	}
}