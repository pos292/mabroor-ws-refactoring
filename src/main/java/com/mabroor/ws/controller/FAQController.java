package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.FAQService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/faq")
public class FAQController {
	
	@Autowired
	private FAQService faqService;
	
	@GetMapping(value ="/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",faqService.findAllByStatus());
		return response;
	}
	
	@GetMapping(value = "/mainProduct/{idMainProduct}")
	public Map<String, Object> getFaqByCategory(@PathVariable Long idMainProduct) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",faqService.findByMainProduct(idMainProduct));
		return response;
	}
	
	@GetMapping(value = "/mainProduct/landingpage/{idMainProduct}")
	public Map<String, Object> getFaqByCategoryLandingPage(@PathVariable Long idMainProduct) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",faqService.findByMainProductLandingPage(idMainProduct));
		return response;
	}
}
