package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.PendidikanTerakhirService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/pendidikanterakhir")
public class PendidikanTerakhirController {

	@Autowired
	private PendidikanTerakhirService pendidikanServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data",pendidikanServ.getListPendidikanTerakhir());
		return response;
	}
	
	@GetMapping(value = "/{kawinId}")
	public Map<String, Object> getStatusPerkawinanById(@PathVariable Long pendidikanId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);	
		response.put("data",pendidikanServ.findById(pendidikanId));
		return response;
	}
}
