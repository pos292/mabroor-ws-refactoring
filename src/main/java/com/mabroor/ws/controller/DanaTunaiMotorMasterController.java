package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.CalculateMrpMotorReqDto;
import com.mabroor.ws.service.DanaTunaiMotorMasterService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/danatunai/mastermotor")
public class DanaTunaiMotorMasterController {

	@Autowired
	private DanaTunaiMotorMasterService danaMotorServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> findAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",danaMotorServ.findAll());
		return response;
	}
	
	@GetMapping(value = "/brand/list")
	@ResponseBody
	public Map<String, Object> getBrandMotor() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",danaMotorServ.findByBrandMotor());
		return response;
	}
	
	@GetMapping(value = "/type/list")
	@ResponseBody
	public Map<String, Object> getListType() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",danaMotorServ.findByTypeMotorDistinct());
		return response;
	}
	 
	@GetMapping(value = "/type/{brand}")
	@ResponseBody
	public Map<String, Object> getTypeMotor(@PathVariable String brand) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	    response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
	   	response.put("data",danaMotorServ.findByTypeMotor(brand));
	   	return response;
	}
	
	@GetMapping(value = "/model/{type}")
	@ResponseBody
	public Map<String, Object> getModelMotor(@PathVariable String type) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	   	response.put("status", "success");
	   	response.put("description", "succesfully get data");
	   	response.put("data",danaMotorServ.findByModelMotor(type));
	   	return response;
	}
	
	@GetMapping(value = "/model/{brand}/{type}")
	@ResponseBody
	public Map<String, Object> getModelMotor2(@PathVariable String brand, @PathVariable String type) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	    response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
	   	response.put("data",danaMotorServ.findByBrandTypeModelMotor(brand, type));
	   	return response;
	}
	
	@GetMapping(value = "/year/{model}")
	@ResponseBody
	public Map<String, Object> getYearMotor(@PathVariable String model) {
	    LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	    response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
	   	response.put("data",danaMotorServ.findByYearMotor(model));
	   	return response;
	}
	
	 @GetMapping(value = "/year/{brand}/{type}/{model}")
	   	@ResponseBody
	   	public Map<String, Object> getYearsMotor(@PathVariable String brand, @PathVariable String type, @PathVariable String model) {
	   		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
	   		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
			response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
	   		response.put("data",danaMotorServ.findByYearAndModelTypeMotor(brand, type, model));
	   		return response;
	   	}
	
    @PostMapping(value = "/calculatemrp")
    @ResponseBody
    public Map<String, Object> getCalculateMrp(@RequestBody CalculateMrpMotorReqDto calculateMrpMotor){
    	LinkedHashMap<String, Object> response = new LinkedHashMap<>();
    	response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",danaMotorServ.findByCalculateMrp(calculateMrpMotor));
		return response;
    }
}
