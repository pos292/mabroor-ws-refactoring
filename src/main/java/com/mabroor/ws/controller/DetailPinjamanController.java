package com.mabroor.ws.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mabroor.ws.dto.*;
import com.mabroor.ws.entity.*;
import com.mabroor.ws.service.*;
import com.mabroor.ws.service.impl.MailClient;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.util.ResultUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/api")
public class DetailPinjamanController {

    Logger log = Logger.getLogger(DetailPinjamanController.class);

    @Autowired
    private DetailPinjamanService detailPinjamanService;

    @Autowired
    private UserMobileService userMobileService;

    @Autowired
    private LeadsRuleService leadsRuleServ;

    @Autowired
    private ApplicationService appService;

    @Autowired
    private ApplicationTransactionService appTrxService;

    @Autowired
    private MailClient mailClient;

    @GetMapping("/list/tujuanpinjaman")
    @ResponseBody
    public ResponseEntity<RespResultDto<RespTujuanPinjamanDto>> getAll() {
        HttpStatus httpStatus = null;
        List<MstTujuanPinjaman> get = detailPinjamanService.findall();
        List<RespTujuanPinjamanDto> resp = get.stream().map(mstTujuanPinjaman -> {
            RespTujuanPinjamanDto dto = new RespTujuanPinjamanDto();
            dto.setTujuanPinjaman(mstTujuanPinjaman.getListTujuanPinjaman());
            return dto;
        }).collect(Collectors.toList());

        httpStatus = HttpStatus.OK;
        return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(), null, "Successfully get data Tujuan Pinjaman", resp), httpStatus);
    }

    @GetMapping("/list/bankpenerbit")
    @ResponseBody
    public ResponseEntity<RespResultDto<RespBankPenerbitDto>> getAllbank() {
        HttpStatus httpStatus = null;
        List<MstBankPenerbit> get = detailPinjamanService.findallbank();
        List<RespBankPenerbitDto> respbank = get.stream().map(mstBankPenerbit -> {
            RespBankPenerbitDto dto = new RespBankPenerbitDto();
            dto.setListBankPenerbit(mstBankPenerbit.getListBankPenerbit());
            return dto;
        }).collect(Collectors.toList());

        httpStatus = HttpStatus.OK;
        return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(), null, "Successfully get data Bank", respbank), httpStatus);
    }

    @GetMapping("/list/pekerjaan")
    @ResponseBody
    public ResponseEntity<RespResultDto<RespPekerjaanDto>> getAllpekerjaan() {
        HttpStatus httpStatus = null;
        List<MstPekerjaan> get = detailPinjamanService.findallpekerjaan();
        List<RespPekerjaanDto> respPekerjaan = get.stream().map(mstPekerjaan -> {
            RespPekerjaanDto dto = new RespPekerjaanDto();
            dto.setListPekerjaan(mstPekerjaan.getListPekerjaan());
            return dto;
        }).collect(Collectors.toList());

        httpStatus = HttpStatus.OK;
        return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(), null, "Successfully get data Pekerjaan", respPekerjaan), httpStatus);
    }

    @GetMapping("/list/tenor")
    @ResponseBody
    public ResponseEntity<RespResultDto<RespTenorDto>> getAlltenor() {
        HttpStatus httpStatus = null;
        List<MstTenor> get = detailPinjamanService.findalltenor();
        List<RespTenorDto> respTenor = get.stream().map(mstTenor -> {
            RespTenorDto dto = new RespTenorDto();
            dto.setListTenor(mstTenor.getTenor());
            return dto;
        }).collect(Collectors.toList());

        httpStatus = HttpStatus.OK;
        return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(), null, "Successfully get data Pekerjaan", respTenor), httpStatus);
    }

    @PostMapping("/submitpengajuan")
    @ResponseBody
    public ResponseEntity submitPengajuan(@RequestHeader(value = "appToken", required = true)String token, @RequestBody ReqSubmitPengajuanDto requestDto) throws IllegalBlockSizeException, ParseException, JsonProcessingException {
        Claims claims = JWTGenerator.getInstance().decodeJWT(token);
        HttpStatus httpStatus = null;
        LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();

        ObjectMapper map = new ObjectMapper();
        String tempak = map.writeValueAsString(requestDto);
        log.info(tempak);

        if (Objects.isNull(claims)) {
            httpStatus = HttpStatus.UNAUTHORIZED;
            return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(),
                    "001", "Terjadi kesalahan. (00)", null), httpStatus);
        }else {
            MstPengajuanMultigunaTanpaJaminan pengajuan = new MstPengajuanMultigunaTanpaJaminan();
            pengajuan.setPendapatan(requestDto.getPendapatan());
            pengajuan.setJmlPinjaman(requestDto.getJumlahPinjaman());
            pengajuan.setTenor(requestDto.getTenor());
            pengajuan.setKebPinjaman(requestDto.getKebutuhanPinjaman());
            pengajuan.setKebPinjamanLainnya(requestDto.getKebutuhanPinjamanLainnya());
            pengajuan.setKartuKredit(requestDto.getIsKartuKredit());
//            if (requestDto.isKartuKredit() == true) {
//                pengajuan.setKartuKredit(Boolean.TRUE);
//            } else {
//                pengajuan.setKartuKredit(Boolean.FALSE);
//            }
            pengajuan.setBankPenerbit(requestDto.getBankPenerbit());
            pengajuan.setStatus("Pengajuan Terkirim");
            pengajuan.setUserId(new Long(claims.getId()));
            pengajuan.setTanggalPengajuan(new Date(System.currentTimeMillis()));
            MstPengajuanMultigunaTanpaJaminan save1 = detailPinjamanService.savepengajuan(pengajuan);


            MstDataDiriPengajuanMultiguna datadiri = new MstDataDiriPengajuanMultiguna();
            datadiri.setIdPengajuan(save1.getId());
            datadiri.setJenisKelamin(requestDto.getJenisKelamin());
            datadiri.setNamaKtp(requestDto.getNamaKtp());
            datadiri.setNik(requestDto.getNik());
            datadiri.setTempatLahir(requestDto.getTempatLahir());
            datadiri.setTanggalLahir(new SimpleDateFormat("dd-MM-yyyy").parse(requestDto.getTanggalLahir()));
            datadiri.setEmail(requestDto.getEmail());
            datadiri.setNoTlpn(requestDto.getNoTlpn());
            datadiri.setPekerjaan(requestDto.getPekerjaan());
            datadiri.setNpwp(requestDto.getNpwp());
            datadiri.setAlamat(requestDto.getAlamat());
            datadiri.setProvinsi(requestDto.getProvinsi());
            datadiri.setKotaKabupaten(requestDto.getKotaKabupaten());
            datadiri.setKecamatan(requestDto.getKecamatan());
            datadiri.setKelurahan(requestDto.getKelurahan());
            datadiri.setKodePost(requestDto.getKodePost());
            detailPinjamanService.savedatadiri(datadiri);


            MstLeadsRule mstLeadsRule = new MstLeadsRule();
            mstLeadsRule.setUserId(new Long(claims.getId()));
            mstLeadsRule.setLeadsId(save1.getId());
            mstLeadsRule.setActionStatus("NEW");
            mstLeadsRule.setCreatedBy(save1.getId().toString());
            mstLeadsRule.setLeadsType(Constant.LEADS_TYPE_PINJAMAN);
            mstLeadsRule.setCompany("KTA Permata");
            leadsRuleServ.save(mstLeadsRule);

            MstApplication newApp = new MstApplication();
            newApp.setLeadsId(save1.getId());
            newApp.setCompany("KTA Permata");
            newApp.setLeadsType(Constant.LEADS_TYPE_PINJAMAN);
            newApp.setPickupBu(null);
            newApp.setStatus("Pengajuan Terkirim");
            newApp.setUserId(new Long(claims.getId()));
            MstApplication newApp1 = appService.save(newApp);


            MstApplicationTransaction trxApp = new MstApplicationTransaction();
            trxApp.setApplicationId(newApp1.getId());
            trxApp.setStatus("Pengajuan Terkirim");
            trxApp.setDetail(null);
            trxApp.setTransactionDate(new Date());
            trxApp.setBatchUuid("" + newApp1.getId());
            appTrxService.save(trxApp);
//            if (!datadiri.getEmail().equals(datadiri.getEmail())) {
            // send to leads user
            mailClient.prepareAndSendPengajuanMultigunaTanpaJaminan(datadiri.getEmail(),"Pengajuan Pembiayaan Multiguna Tanpa Jaminan", pengajuan, requestDto.getNamaKtp());
            response.put("status", "success");
            response.put("description", "Pengajuan Multiguna Tanpa Jaminan successfully submitted.");
//            httpStatus = HttpStatus.OK;
//        } else {
//            response.put("status", "failed");
//            response.put("description", "Akunmu telah login di device lain");
//            httpStatus = HttpStatus.UNAUTHORIZED;
//        }

            httpStatus = HttpStatus.OK;
            return new ResponseEntity(ResultUtil.createResultMapWoData(httpStatus.value(), null, "Successfully Submit"), httpStatus);


        }
    }

    @GetMapping("/list/pengajuan")
    @ResponseBody
    public ResponseEntity<RespResultDto<RespPengajuanDto>> getAllpengajuan() {
        HttpStatus httpStatus = null;
        List<MstPengajuanMultigunaTanpaJaminan> get = detailPinjamanService.findallpengajuan();
        List<RespPengajuanDto> respPengajuan = get.stream().map(mstPengajuan -> {
            RespPengajuanDto dto = new RespPengajuanDto();
            dto.setPendapatan(mstPengajuan.getPendapatan());
            dto.setJumlahPinjaman(mstPengajuan.getJmlPinjaman());
            dto.setTenor(mstPengajuan.getTenor());
            dto.setKebutuhanPinjaman(mstPengajuan.getKebPinjaman());
            dto.setKebutuhanPinjamanLainnya(mstPengajuan.getKebPinjamanLainnya());
            dto.setKartuKredit(mstPengajuan.getKartuKredit());
            dto.setBankPenerbit(mstPengajuan.getBankPenerbit());
            dto.setStatus(mstPengajuan.getStatus());
            dto.setUserId(mstPengajuan.getUserId());
            dto.setTanggalPengajuan(mstPengajuan.getTanggalPengajuan().toString());
            return dto;
        }).collect(Collectors.toList());

        httpStatus = HttpStatus.OK;
        return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(), null, "Successfully get data Pengajuan", respPengajuan), httpStatus);
    }

    @GetMapping("/list/kota")
    @ResponseBody
    public ResponseEntity<RespResultDto<ListKotaDto>> getAllkota() {
        HttpStatus httpStatus = null;
        List<MstMultigunaKota> get = detailPinjamanService.findAllKota();
        List<ListKotaDto> kota = get.stream().map(liskot -> {
            ListKotaDto dto = new ListKotaDto();
            dto.setId(liskot.getId());
            dto.setNamaKota(liskot.getName());
            return dto;
        }).collect(Collectors.toList());

        httpStatus = HttpStatus.OK;
        return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(), null, "Successfully get data Kota", kota), httpStatus);
    }

    @GetMapping("/list/kecamatan/{id_kota}")
    @ResponseBody
    public ResponseEntity<RespResultDto<ListKecamatanDto>> getAllkecamatan(@PathVariable Long id_kota) {
        HttpStatus httpStatus = null;
        List<MstMultigunaKecamatan> get = detailPinjamanService.findAllKecamatanbyidkota(id_kota);
        List<ListKecamatanDto> kec = get.stream().map(listkec -> {
            ListKecamatanDto dto = new ListKecamatanDto();
            dto.setId(listkec.getId());
            dto.setIdKota(listkec.getIdKota());
            dto.setNamaKecamatan(listkec.getName());
            return dto;
        }).collect(Collectors.toList());

        httpStatus = HttpStatus.OK;
        return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(), null, "Successfully get data Kecamatan", kec), httpStatus);
    }


    @GetMapping("/list/kelurahan/{id_kecamatan}")
    @ResponseBody
    public ResponseEntity<RespResultDto<ListKelurahanDto>> getAllkelurahan(@PathVariable Long id_kecamatan) {
        HttpStatus httpStatus = null;
        List<MstMultigunaKelurahan> get = detailPinjamanService.findAllKelurahanbyidkecamatan(id_kecamatan);
        List<ListKelurahanDto> kel = get.stream().map(listkel -> {
            ListKelurahanDto dto = new ListKelurahanDto();
            dto.setId(listkel.getId());
            dto.setIdKecamatan(listkel.getIdKecamatan());
            dto.setNamaKelurahan(listkel.getName());
            return dto;
        }).collect(Collectors.toList());

        httpStatus = HttpStatus.OK;
        return new ResponseEntity(ResultUtil.createResultDto(httpStatus.value(), null, "Successfully get data Kelurahan", kel), httpStatus);
    }

    @GetMapping("/list/kodepos/{code}")
    @ResponseBody
    public ResponseEntity<RespResultDto<ListKodePosDto>> getAllkodePos(@PathVariable String code) {
        LinkedHashMap<String,Object> response = new LinkedHashMap<>();
        HttpStatus httpStatus = null;
        RespDetailWilayahDto dataWilayah = detailPinjamanService.findDetailWilayahByCode(code);
        if(dataWilayah != null){
            response.put("message","kode post ditemukan");
            response.put("status", "success");
            response.put("data", dataWilayah);
            httpStatus = HttpStatus.OK;
        }else{
            response.put("message","kode post tidak ditemukan");
            response.put("status", "failed");
            response.put("data", new ArrayList<>());
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity(response, httpStatus);
    }
}
