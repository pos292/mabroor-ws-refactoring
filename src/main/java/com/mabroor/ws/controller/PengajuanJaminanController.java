package com.mabroor.ws.controller;

import java.io.FileWriter;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.util.BASE64DecodedMultipartFile;
import com.mabroor.ws.util.Constant;
import com.mabroor.ws.util.JWTGenerator;
import com.mabroor.ws.dto.AxwayTokenResponseDto;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

import com.google.gson.Gson;
import com.mabroor.ws.dto.JamCreditSimulationSubmissionRequestDto;
import com.mabroor.ws.dto.JamCreditSimulationSubmissionResponseDto;
import com.mabroor.ws.entity.MstInbox;
import com.mabroor.ws.entity.MstInboxCategory;
import com.mabroor.ws.entity.MstLeadsRule;
import com.mabroor.ws.entity.MstPengajuanJaminan;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.model.PushNotificationRequest;
import com.mabroor.ws.service.InboxCategoryService;
import com.mabroor.ws.service.InboxService;
import com.mabroor.ws.service.LeadsRuleService;
import com.mabroor.ws.service.PengajuanJaminanService;
import com.mabroor.ws.service.PushNotificationService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.service.impl.MailClient;

@RestController
@RequestMapping("/v1/api/pengajuanjaminan")
public class PengajuanJaminanController {
	private static final Logger LOG = Logger.getLogger(PengajuanJaminanController.class);
	public static FileWriter file;

	@Autowired
	private PengajuanJaminanService pengajuanService;

	@Autowired
	private UserMobileService userMobileService;
	
	@Autowired
	private PushNotificationService pushNotificationService;
	
	@Autowired
	private InboxCategoryService inboxCategoryServ;
	
	@Autowired
	private InboxService inboxServ;


	@Autowired
	private LeadsRuleService leadsRuleServ;
	
	@Autowired
	private MailClient mailClient;


	@Value("${storage.upload.file}")
	private String storageUploadFile;

	@Value("${axway.api.url}")
	private String axwayApiUrl;

	@Value("${axway.oauth.url}")
	private String oauthUrl;

	@Value("${axway.oauth.clientid}")
	private String oauthClientId;

	@Value("${axway.oauth.clientsecret}")
	private String oauthClientSecret;

	@Value("${axway.oauth.granttype}")
	private String oauthGrantType;
	
	@Value("${moxa.mabroor.channel}")
	private String channel;

	@Value("${moxa.mabroor.os}")
	private String os;

	@Value("${moxa.mabroor.userid}")
	private String userid;

	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", pengajuanService.getListPengajuan());
		return response;
	}

	@GetMapping(value = "/{jaminanId}")
	public LinkedHashMap<String, Object> getJaminanById(@PathVariable Long jaminanId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data", pengajuanService.findById(jaminanId));
		return response;
	}

	@PostMapping(value = "/submitAcc")
	@ResponseBody
	public ResponseEntity save(@RequestBody MstPengajuanJaminan mstJam,
			@RequestHeader(value = "appToken", required = true) String token) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		HttpStatus httpStatus = null;

		try {
			Claims claims = JWTGenerator.getInstance().decodeJWT(token);
			if (Objects.isNull(claims)) {
				response.put("status", "failed");
				response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
				httpStatus = HttpStatus.UNAUTHORIZED;
			} else {
				Optional<MstUserMobile> mstUserMobileOpt = userMobileService.findById(new Long(claims.getId()));
				if (!mstUserMobileOpt.isPresent()) {
					response.put("status", "failed");
					response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
					httpStatus = HttpStatus.UNAUTHORIZED;
				} else {
					MstUserMobile mstUserMobiles = mstUserMobileOpt.get();
					if (claims.getId().equals("" + mstUserMobiles.getId()) && token.equals(mstUserMobiles.getToken())) {

						byte[] fileByteBpkbMobil = new byte[0];
						MultipartFile fileBpkbMobil = null;

						byte[] fileByteBpkbMotor = new byte[0];
						MultipartFile fileBpkbMotor = null;

						byte[] fileByteStnk = new byte[0];
						MultipartFile fileStnk = null;

						byte[] fileByteFotoKananDepan = new byte[0];
						MultipartFile fileFotoKananDepan = null;

						byte[] fileByteFotoKananBelakang = new byte[0];
						MultipartFile fileFotoKananBelakang = null;

						byte[] fileByteFotoKiriDepan = new byte[0];
						MultipartFile fileFotoKiriDepan = null;

						byte[] fileByteFotoKiriBelakang = new byte[0];
						MultipartFile fileFotoKiriBelakang = null;

						if (mstJam.getFotoBpkbMobil() == null
								|| "".equals(mstJam.getFotoBpkbMobil())) {
							fileBpkbMobil = null;
						} else {
							fileByteBpkbMobil = Base64.getDecoder().decode(mstJam.getFotoBpkbMobil());
							fileBpkbMobil = new BASE64DecodedMultipartFile(fileByteBpkbMobil, "img.png");
						}

						if (mstJam.getFotoBpkbMotor() == null
								|| "".equals(mstJam.getFotoBpkbMotor())) {
							fileBpkbMotor = null;
						} else {
							fileByteBpkbMotor = Base64.getDecoder().decode(mstJam.getFotoBpkbMotor());
							fileBpkbMotor = new BASE64DecodedMultipartFile(fileByteBpkbMotor, "img.png");
						}

						if (mstJam.getFotoStnk() == null || "".equals(mstJam.getFotoStnk())) {
							fileStnk = null;
						} else {
							fileByteStnk = Base64.getDecoder().decode(mstJam.getFotoStnk());
							fileStnk = new BASE64DecodedMultipartFile(fileByteStnk, "img.png");
						}

						if (mstJam.getFotoKananDepan() == null
								|| "".equals(mstJam.getFotoKananDepan())) {
							fileFotoKananDepan = null;
						} else {
							fileByteFotoKananDepan = Base64.getDecoder().decode(mstJam.getFotoKananDepan());
							fileFotoKananDepan = new BASE64DecodedMultipartFile(fileByteFotoKananDepan, "img.png");
						}

						if (mstJam.getFotoKananBelakang() == null
								|| "".equals(mstJam.getFotoKananBelakang())) {
							fileFotoKananBelakang = null;
						} else {
							fileByteFotoKananBelakang = Base64.getDecoder()
									.decode(mstJam.getFotoKananBelakang());
							fileFotoKananBelakang = new BASE64DecodedMultipartFile(fileByteFotoKananBelakang,
									"img.png");
						}

						if (mstJam.getFotoKiriBelakang() == null
								|| "".equals(mstJam.getFotoKiriBelakang())) {
							fileFotoKiriBelakang = null;
						} else {
							fileByteFotoKiriBelakang = Base64.getDecoder()
									.decode(mstJam.getFotoKiriBelakang());
							fileFotoKiriBelakang = new BASE64DecodedMultipartFile(fileByteFotoKiriBelakang, "img.png");
						}

						if (mstJam.getFotoKiriDepan() == null
								|| "".equals(mstJam.getFotoKiriDepan())) {
							fileFotoKiriDepan = null;
						} else {
							fileByteFotoKiriDepan = Base64.getDecoder().decode(mstJam.getFotoKiriDepan());
							fileFotoKiriDepan = new BASE64DecodedMultipartFile(fileByteFotoKiriDepan, "img.png");
						}

						String firstName = mstUserMobiles.getFirstName() != null ? mstUserMobiles.getFirstName() : "";
						String lastname = mstUserMobiles.getLastName() != null ? mstUserMobiles.getLastName() : "";
						String fullname = firstName;
						if (!"".equals(lastname)) {
							fullname += " " + lastname;
						}

						mstJam.setJaminanStatus("01");
						mstJam.setUserId(mstUserMobiles.getId());
						mstJam.setEmailDataUser(mstUserMobiles.getEmail());
						mstJam.setPhoneDataUser(mstUserMobiles.getPhone());
						mstJam.setNameDataUser(fullname);

						// default set leads user to user mobile
						mstJam.setEmail(mstUserMobiles.getEmail());
						mstJam.setPhone(mstUserMobiles.getPhone());
				
						Gson gson = new Gson();
						RestTemplate restTemplate = new RestTemplate();
						HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
						headers.set("User-Agent", "CaasReportAPI/1.0.0");

						MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
						map.add("client_id", oauthClientId);
						map.add("client_secret", oauthClientSecret);
						map.add("grant_type", oauthGrantType);

						HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
						ResponseEntity<AxwayTokenResponseDto> response1 = null;
						ParameterizedTypeReference<AxwayTokenResponseDto> type = new ParameterizedTypeReference<AxwayTokenResponseDto>() {
						};
						response1 = restTemplate.exchange(oauthUrl, HttpMethod.POST, entity, type);

						if (response1 != null && response1.getBody() != null) {
							if (response1.getBody().getAccess_token() != null) {
								String tokenAcc = response1.getBody().getAccess_token();
					     JamCreditSimulationSubmissionRequestDto reqSubmission = new JamCreditSimulationSubmissionRequestDto();						
						
					     reqSubmission.setP_TENOR(mstJam.getTenor());
						 reqSubmission.setP_ACCOUNT_ID("MOXA MABROOR");
						 reqSubmission.setP_EMAIL(mstJam.getEmail());
						 reqSubmission.setP_PHONE_NUMBER(mstJam.getPhone());
						 reqSubmission.setP_NAME(mstJam.getName());
						 reqSubmission.setP_CD_AREA(mstJam.getKotaPengajuanId());
						 reqSubmission.setP_CD_VEHICLE_BRAND(mstJam.getBrandCode());
						 reqSubmission.setP_CD_VEHICLE_TYPE(mstJam.getTypeCode());
						 reqSubmission.setP_CHANNEL("MOXA");
						 reqSubmission.setP_YEAR_OF_MFG(mstJam.getTahun());
						 reqSubmission.setP_PRODUCT("0001");
						 reqSubmission.setP_SOURCE_LEADS("108");
						 reqSubmission.setP_FLAG_NEW_USED_VALID("M");
						 reqSubmission.setP_AMT_CASH_PENCAIRAN(mstJam.getJumlahPinjaman());
						 reqSubmission.setP_NOTES("MGU Syairah");

						Map<String, Object> objtemp1 = new HashMap<>();
						objtemp1.put("doSendDataLeads", reqSubmission);
						String json2 = gson.toJson(objtemp1);
						LOG.info(json2);
						LOG.info("proses submission simulasi");
						headers.set("Channel", channel);
						headers.set("Authorization", "Bearer "+ tokenAcc);
						LOG.info("Bearer"+ tokenAcc);
						headers.set("OS", os);
						headers.set("UserID", userid);
						headers.setContentType(MediaType.APPLICATION_JSON);
						HttpEntity<String> entity2 = new HttpEntity<>(json2, headers);
						ResponseEntity<JamCreditSimulationSubmissionResponseDto> response2 = null;
						ParameterizedTypeReference<JamCreditSimulationSubmissionResponseDto> type2 = new ParameterizedTypeReference<JamCreditSimulationSubmissionResponseDto>() {
						};
						response2 = restTemplate.exchange(axwayApiUrl + "/restv2/publicservices/submition/leads/", HttpMethod.POST,
								entity2, type2);
						LOG.info(response2.getBody().getOUT_MESS());
						if (response2 != null && response2.getBody() != null) {
							if ("T".equalsIgnoreCase(response2.getBody().getOUT_STAT())) {
								
								mstJam.setIntegrationLeadsId(response2.getBody().getOUT_DATA().get(0).getNOREG_LEADS());
							
								MstPengajuanJaminan pengajuanJaminanBaru = pengajuanService.save(fileBpkbMobil,
										fileBpkbMotor, fileStnk, fileFotoKananDepan, fileFotoKananBelakang,
										fileFotoKiriDepan, fileFotoKiriBelakang, mstJam);
								
								MstLeadsRule mstLeadsRule = new MstLeadsRule();
								mstLeadsRule.setUserId(mstUserMobiles.getId());
								mstLeadsRule.setLeadsId(pengajuanJaminanBaru.getId());
								mstLeadsRule.setActionStatus("INTEGRATION");
								mstLeadsRule.setCreatedBy(mstUserMobiles.getId().toString());
								mstLeadsRule.setLeadsType(Constant.LEADS_TYPE_KDA_MOBIL);
								mstLeadsRule.setCompany("ACC");
								leadsRuleServ.save(mstLeadsRule);				

								Optional<MstUserMobile> userMobile = userMobileService.findById(pengajuanJaminanBaru.getUserId());
								if (userMobile.isPresent()) {
									MstUserMobile temporaryUserMobile = userMobile.get();
									if (temporaryUserMobile.getFirebaseToken() != null) {

										MstInbox inbox = new MstInbox(); 
										Long id = new Long(3);
										MstInboxCategory category = inboxCategoryServ.findById(id);
										inbox.setTitle("Status pengajuan multiguna dengan jaminan");
										inbox.setDescription("Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan multiguna dengan jaminan yang kamu lakukan. Klik di sini untuk melihat status terkini!");
										inbox.setIdInboxCategory(category);
										inbox.setScreenName("MGUJaminanDetail");
										inbox.setInboxType("DIRECT_SCREEN");
										inbox.setIcon("inbox_pengajuan.png");
										inbox.setInboxRead("NO");
										inbox.setUserId(temporaryUserMobile.getId());
										inbox.setIdData(pengajuanJaminanBaru.getId());
										MstInbox inbx = inboxServ.save(inbox);

										PushNotificationRequest pushNotificationReq = new PushNotificationRequest();
										LOG.info(temporaryUserMobile.getFirebaseToken());
										pushNotificationReq.setToken(temporaryUserMobile.getFirebaseToken());
										pushNotificationReq
										.setTitle("Status pengajuan multiguna dengan jaminan");

										Map<String, String> pushData = new HashMap<String, String>();

										Map<String, Object> jsonPush = new HashMap<String, Object>();
										jsonPush.put("screen", "MGUJaminanDetail");
		
										Map<String, Object> params = new HashMap<String, Object>();
										params.put("id", pengajuanJaminanBaru.getId());
										params.put("pageType", "");
										params.put("idInbox", inbx.getId());
		
										jsonPush.put("params", params);
		
										String jsonified = new Gson().toJson(jsonPush);
										pushData.put("json", jsonified);
		
										pushNotificationReq.setData(pushData);
		
										pushNotificationReq.setMessage(
												"Hi " + temporaryUserMobile.getName() + "! Ada update tentang pengajuan pembiayaan multiguna dengan jaminan yang kamu lakukan. Klik di sini untuk melihat status terkini!");

										try {
											pushNotificationService.sendMessageToToken(pushNotificationReq);
		
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (ExecutionException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									
									} else {
										LOG.info("Firebase token not found for user " + temporaryUserMobile.getName() + " - "
												+ temporaryUserMobile.getPhone());
									}
								}

								if (!mstUserMobiles.getEmail().equals(pengajuanJaminanBaru.getEmail())) {
									mailClient.prepareAndSendPengajuanMultiGunaJaminanUserMobile(
											mstUserMobiles.getEmail(), "Pengajuan Pembiayaan Multiguna Dengan Jaminan",
											pengajuanJaminanBaru);
								}
								mailClient.prepareAndSendPengajuanMultiGunaJaminanUserLeads(
										pengajuanJaminanBaru.getEmail(), "Pengajuan Pembiayaan Multiguna Dengan Jaminan",
										pengajuanJaminanBaru);

								response.put("status", "success");
								response.put("description", "Success! Leads Jaminan Data successfully submitted.");
								httpStatus = HttpStatus.OK;
							} else {
								LOG.info(response2.getBody().getOUT_MESS());

								response.put("status", "failed");
								response.put("description", "Submission failed, please try again later(1)");
								httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
							}
						} else {
							response.put("status", "failed");
							response.put("description", "Submission failed, please try again later(2)");
							httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
						}
					} else {
						response.put("status", "failed");
						response.put("description",
								"Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
						httpStatus = HttpStatus.UNAUTHORIZED;
					}
				}
					}
				}
			}

		} catch (MalformedJwtException mje) {
			LOG.info(mje.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (IllegalBlockSizeException ibe) {
			LOG.info(ibe.getMessage());
			response.put("status", "failed");
			response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (NullPointerException np) {
			LOG.info(np.getMessage());
			np.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (StackOverflowError st) {
			LOG.info(st.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (RuntimeException re) {
//			LOG.info(re.getMessage());
			re.printStackTrace();
			response.put("status", "failed");
			response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (OutOfMemoryError oe) {
			LOG.info(oe.getMessage());
			response.put("status", "failed");
			response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(response, httpStatus);
	}

}
