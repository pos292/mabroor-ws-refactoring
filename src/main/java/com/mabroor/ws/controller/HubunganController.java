package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.HubunganService;

@RestController
@RequestMapping("/v1/api/hubungan")
public class HubunganController {

	@Autowired
	private HubunganService hubServ;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",hubServ.getListHubungan());
		return response;
	}
	
	@GetMapping(value = "/{hubId}")
	public LinkedHashMap<String, Object> getHubunganById(@PathVariable Long hubId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",hubServ.findById(hubId));
		return response;
	}
}
