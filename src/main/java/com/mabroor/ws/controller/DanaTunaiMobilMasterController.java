package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.controller.DanaTunaiMobilMasterController;
import com.mabroor.ws.dto.CalculateMrpMobilReqDto;
import com.mabroor.ws.dto.DanaTunaiMobilDto;
import com.mabroor.ws.entity.MstDanaTunaiMobilMaster;
import com.mabroor.ws.service.DanaTunaiMobilMasterService;
import com.mabroor.ws.util.Constant;

@RestController
@RequestMapping("/v1/api/danatunai/mastermobil")
public class DanaTunaiMobilMasterController {

    @Autowired
    private DanaTunaiMobilMasterService danaMobilServ;
    
    @PersistenceContext
	private EntityManager em;
    
    @GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> findAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",danaMobilServ.findAll());
		return response;
	}
    
    @GetMapping(value = "/brand/list")
	@ResponseBody
	public Map<String, Object> getBrandCar() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",danaMobilServ.findByBrandCar());
		return response;
	}
    
    @GetMapping(value = "/type/{brand}")
   	@ResponseBody
   	public Map<String, Object> getTypeCar(@PathVariable String brand) {
   		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
   		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
   		response.put("data",danaMobilServ.findByTypeCar(brand));
   		return response;
   	}
    
    @GetMapping(value = "/model/{type}")
   	@ResponseBody
   	public Map<String, Object> getModelCar(@PathVariable String type) {
   		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
   		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
   		response.put("data",danaMobilServ.findByModelCar(type));
   		return response;
   	}
    
    @GetMapping(value = "/model/{brand}/{type}")
   	@ResponseBody
   	public Map<String, Object> getModelCar2(@PathVariable String brand, @PathVariable String type) {
   		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
   		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
   		response.put("data",danaMobilServ.findByBrandTypeModelCar(brand, type));
   		return response;
   	}
    
    @GetMapping(value = "/year/{model}")
   	@ResponseBody
   	public Map<String, Object> getYearCar(@PathVariable String model) {
   		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
   		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
   		response.put("data",danaMobilServ.findByYearCar(model));
   		return response;
   	}
    
    @GetMapping(value = "/year/{brand}/{type}/{model}")
   	@ResponseBody
   	public Map<String, Object> getYearsCar(@PathVariable String brand, @PathVariable String type, @PathVariable String model) {
   		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
   		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
   		response.put("data",danaMobilServ.findByYearAndModelTypeCar(brand, type, model));
   		return response;
   	}
    
    @GetMapping(value = "/listmobil")
   	@ResponseBody
   	public Map<String, Object> getListCarAll() {
   		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
   		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
   		List <MstDanaTunaiMobilMaster> mobilList = danaMobilServ.findAll();
   		List <DanaTunaiMobilDto> mobilDto = new ArrayList <>();
   		if(mobilList != null && !mobilList.isEmpty()) {
   			for (MstDanaTunaiMobilMaster mstDanaMobil : mobilList) {
   				em.detach(mstDanaMobil);
   				DanaTunaiMobilDto mobilListDto = new DanaTunaiMobilDto();
   				mobilListDto.setId(mstDanaMobil.getId());
   				mobilListDto.setNamaKendaraan(mstDanaMobil.getBrand() + " " + mstDanaMobil.getType() + " " + mstDanaMobil.getModel() + " " + mstDanaMobil.getYear());
   				mobilDto.add(mobilListDto);
   			}
   		}
   		response.put("data", mobilDto);
   		return response;
   	}
    
    @PostMapping(value = "/calculatemrp")
    @ResponseBody
    public Map<String, Object> getCalculateMrp(@RequestBody CalculateMrpMobilReqDto calculateMrpMobil){
    	LinkedHashMap<String, Object> response = new LinkedHashMap<>();
    	response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data",danaMobilServ.findByCalculateMrp(calculateMrpMobil));
		return response;
    }
}
