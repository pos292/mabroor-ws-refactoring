package com.mabroor.ws.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.KabupatenService;

@RestController
@RequestMapping("/v1/api/kabupaten")
public class KabupatenController {

	@Autowired
	private KabupatenService kabupatenService;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",kabupatenService.getListKabupaten());
		return response;
	}
	
	@GetMapping(value = "/provinsi/{provinsiId}")
	public Map<String, Object> getKabupatenById(@PathVariable Long provinsiId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",kabupatenService.findByProvinsiId(provinsiId));
		return response;
	}
	
}
