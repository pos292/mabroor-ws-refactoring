package com.mabroor.ws.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.dto.InsuranceCarDto;
import com.mabroor.ws.dto.InsuranceDto;
import com.mabroor.ws.entity.MstGeneralInsuranceCar;
import com.mabroor.ws.entity.MstGeneralInsuranceCarProduct;
import com.mabroor.ws.entity.MstGeneralInsuranceCarProductBenefit;
import com.mabroor.ws.service.GeneralInsuranceCarService;
import com.mabroor.ws.service.InsuranceCarProductBenefitService;
import com.mabroor.ws.service.InsuranceCarProductService;
import com.mabroor.ws.service.InsuranceQuestionService;
import com.mabroor.ws.util.Constant;

@Transactional
@RestController
@RequestMapping("/v1/api/generalInsurance/car")
public class GeneralInsuranceCarController {

	@Autowired
	private GeneralInsuranceCarService inscCarServ;
	
	@Autowired
	private InsuranceQuestionService inscQuestServ;
	
	@Autowired
	private InsuranceCarProductService inscCarProductServ;
	
	@Autowired
	private InsuranceCarProductBenefitService inscCarProductBenefServ;
	
	@PersistenceContext
	private EntityManager em;
	
	@Value("${image.base.url}")
	private String imageBaseUrl;
	
	@GetMapping(value = "/list")
	@ResponseBody
	public Map<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		List<MstGeneralInsuranceCar> insuranceList = inscCarServ.getAll();
		List<InsuranceDto> insuranceListDto = new ArrayList<>();
		if(!insuranceList.isEmpty()) {
			for (MstGeneralInsuranceCar mstInsurance : insuranceList) {
				em.detach(mstInsurance);
				InsuranceDto insuranceDto = new InsuranceDto();
				insuranceDto.setId(mstInsurance.getId());
				insuranceDto.setTitle(mstInsurance.getTitle());
				insuranceDto.setDescription(mstInsurance.getDescription());
				insuranceDto.setImage(this.imageBaseUrl + "generalInsuranceCar/" + mstInsurance.getImage());
				insuranceDto.setInsuranceQuestion(inscQuestServ.getListInscQuestByInsuranceId(mstInsurance.getId()));
				insuranceListDto.add(insuranceDto);
				
			}
			response.put("data",insuranceListDto);
		}
		return response;
	}
	
	@GetMapping(value = "/detail/{id}")
	@ResponseBody
	public Map<String, Object> getDetailById(@PathVariable Long id) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		InsuranceDto insuranceDto = new InsuranceDto();
		MstGeneralInsuranceCar mstInsurance = inscCarServ.getListInscType(id).get();
		insuranceDto.setId(mstInsurance.getId());
		insuranceDto.setTitle(mstInsurance.getTitle());
		insuranceDto.setDescription(mstInsurance.getDescription());
		insuranceDto.setImage(this.imageBaseUrl + "generalInsuranceCar/" + mstInsurance.getImage());
		insuranceDto.setLogoCompany(this.imageBaseUrl + "generalInsuranceCar/" + mstInsurance.getLogoCompany());
		insuranceDto.setMainProduct(mstInsurance.getMainProduct());
		insuranceDto.setInsuranceStatus(mstInsurance.getGeneralInsuranceCarStatus());
		insuranceDto.setInsuranceQuestion(inscQuestServ.getListInscQuestByInsuranceId(mstInsurance.getId()));

		
		List <MstGeneralInsuranceCarProduct> general = inscCarProductServ.getListCarId(mstInsurance.getId());
		List <InsuranceCarDto> inscDto = new ArrayList<>();
		for(MstGeneralInsuranceCarProduct product : general) {
			em.detach(product);
			product.setBackgroundImage(this.imageBaseUrl + "generalInsuranceCar/" + product.getBackgroundImage());
			InsuranceCarDto dto = new InsuranceCarDto();
			dto.setId(product.getId());
			dto.setTitleProduct(product.getTitleProduct());
			dto.setBackgroundImage(product.getBackgroundImage());
			dto.setKontribusi(product.getKontribusi());
			dto.setProductCode(product.getProductCode());
			List<MstGeneralInsuranceCarProductBenefit> tmpBenefList = inscCarProductBenefServ.getListCarProductByProductId(product.getId());
			for(MstGeneralInsuranceCarProductBenefit item: tmpBenefList) {
				em.detach(item);
				item.setIcon(this.imageBaseUrl + "generalInsuranceCar/" + item.getIcon());
			}
			dto.setCarProductBenefit(tmpBenefList);
			inscDto.add(dto);
		}
		insuranceDto.setInsuranceProduct(inscDto);

		response.put(Constant.RESPONSE_STATUS_KEY, Constant.RESPONSE_SUCCESS_STATUS_VALUE);
		response.put(Constant.RESPONSE_DESCRIPTION_KEY, Constant.RESPONSE_DESCRIPTION_VALUE);
		response.put("data", insuranceDto);
		return response;
	}
}
