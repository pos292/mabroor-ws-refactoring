package com.mabroor.ws.controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.crypto.IllegalBlockSizeException;

import com.mabroor.ws.entity.MstPengajuanAccountBSI;
import com.mabroor.ws.entity.MstUserMobile;
import com.mabroor.ws.service.PengajuanAccountBSIService;
import com.mabroor.ws.service.UserMobileService;
import com.mabroor.ws.util.JWTGenerator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/v1/api/accountBsi")
public class PengajuanAccountBSIController {
    private static final Logger LOG = Logger.getLogger(PengajuanAccountBSIController.class);

    @Autowired
    private PengajuanAccountBSIService accountBsiServ;

    @Autowired
    private UserMobileService userMobileServ;

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> save(@RequestHeader(value = "appToken", required = true) String token) {

        LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
        HttpStatus httpStatus = null;
        try {
            Claims claims = JWTGenerator.getInstance().decodeJWT(token);
            if (Objects.isNull(claims)) {
                response.put("status", "failed");
                response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
                httpStatus = HttpStatus.UNAUTHORIZED;
            } else {
                Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
                MstUserMobile mstUserMobile = null;
                if (!mstUserMobileOpt.isPresent()) {
                    response.put("status", "failed");
                    response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
                    httpStatus = HttpStatus.UNAUTHORIZED;
                } else {
                    mstUserMobile = mstUserMobileOpt.get();
                    if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {

                        MstPengajuanAccountBSI mstPengajuan = new MstPengajuanAccountBSI();

                        String firstName = mstUserMobile.getFirstName() != null ? mstUserMobile.getFirstName() : "";
                        String lastname = mstUserMobile.getLastName();
                        String fullname = firstName;
                        if (!"".equals(lastname)) {
                            fullname += " " + lastname;
                        } else {
                            fullname = mstUserMobile.getName();
                        }

                        mstPengajuan.setUserId(mstUserMobile.getId());
                        mstPengajuan.setEmail(mstUserMobile.getEmail());
                        mstPengajuan.setPhone(mstUserMobile.getPhone());
                        mstPengajuan.setPhoneDataUser(mstUserMobile.getPhone());
                        mstPengajuan.setNamaDepan(firstName);
                        mstPengajuan.setNamaBelakang(lastname);

                        if (mstPengajuan.getEmailDataUser() == null || "".equals(mstPengajuan.getEmailDataUser())) {
                            mstPengajuan.setEmailDataUser(mstUserMobile.getEmail());
                        }

                        if (mstPengajuan.getPhoneDataUser() == null || "".equals(mstPengajuan.getPhoneDataUser())) {
                            mstPengajuan.setPhoneDataUser(mstUserMobile.getPhone());
                        }
                        mstPengajuan.setName(fullname);
                        mstPengajuan.setNameDataUser(fullname);
                        mstPengajuan.setTransactionDateTime(new Date());

                        MstPengajuanAccountBSI cekking = accountBsiServ
                                .findByUserAndCreateDateNow(mstUserMobile.getId());
                        if (cekking != null) {
                            mstPengajuan.setId(cekking.getId());
                        }

                        accountBsiServ.save(mstPengajuan);

                        response.put("status", "success");
                        response.put("description", "Success! Data successfully submitted.");
                        httpStatus = HttpStatus.OK;
                    } else {
                        response.put("status", "failed");
                        response.put("description",
                                "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
                        httpStatus = HttpStatus.UNAUTHORIZED;
                    }
                }
            }
        } catch (MalformedJwtException mje) {
            LOG.info(mje.getMessage());
            response.put("status", "failed");
            response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
            httpStatus = HttpStatus.UNAUTHORIZED;
        } catch (IllegalBlockSizeException ibe) {
            LOG.info(ibe.getMessage());
            response.put("status", "failed");
            response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
            httpStatus = HttpStatus.UNAUTHORIZED;
        } catch (NullPointerException np) {
            LOG.info(np.getMessage());
            np.printStackTrace();
            response.put("status", "failed");
            response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        } catch (StackOverflowError st) {
            LOG.info(st.getMessage());
            response.put("status", "failed");
            response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        } catch (RuntimeException re) {
            re.printStackTrace();
            LOG.info(re.getMessage());
            response.put("status", "failed");
            response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        } catch (OutOfMemoryError oe) {
            LOG.info(oe.getMessage());
            response.put("status", "failed");
            response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(response, httpStatus);
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> checkAccountBsi(@RequestHeader(value = "appToken", required = true) String token) {
        LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
        HttpStatus httpStatus = null;
        try {
            Claims claims = JWTGenerator.getInstance().decodeJWT(token);
            if (Objects.isNull(claims)) {
                response.put("status", "failed");
                response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (00)");
                httpStatus = HttpStatus.UNAUTHORIZED;
            } else {
                Optional<MstUserMobile> mstUserMobileOpt = userMobileServ.findById(new Long(claims.getId()));
                MstUserMobile mstUserMobile = null;
                if (!mstUserMobileOpt.isPresent()) {
                    response.put("status", "failed");
                    response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (01)");
                    httpStatus = HttpStatus.UNAUTHORIZED;
                } else {
                    mstUserMobile = mstUserMobileOpt.get();
                    if (claims.getId().equals("" + mstUserMobile.getId()) && token.equals(mstUserMobile.getToken())) {
                        List<MstPengajuanAccountBSI> listPengajuan = accountBsiServ.findByUserId(mstUserMobile.getId());
                        response.put("data", listPengajuan);
                        response.put("description", "Success! Get Data successfully.");
                        response.put("status", "success");
                        httpStatus = HttpStatus.OK;
                    } else {
                        response.put("status", "failed");
                        response.put("description",
                                "Hi %{name}, sepertinya akun kamu telah terdaftar di tempat lain :)");
                        httpStatus = HttpStatus.UNAUTHORIZED;
                    }
                }
            }
        } catch (MalformedJwtException mje) {
            LOG.info(mje.getMessage());
            response.put("status", "failed");
            response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (02)");
            httpStatus = HttpStatus.UNAUTHORIZED;
        } catch (IllegalBlockSizeException ibe) {
            LOG.info(ibe.getMessage());
            response.put("status", "failed");
            response.put("description", "Terjadi kesalahan, silakan untuk melakukan login ulang. (03)");
            httpStatus = HttpStatus.UNAUTHORIZED;
        } catch (NullPointerException np) {
            LOG.info(np.getMessage());
            np.printStackTrace();
            response.put("status", "failed");
            response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (01).");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        } catch (StackOverflowError st) {
            LOG.info(st.getMessage());
            response.put("status", "failed");
            response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (02).");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        } catch (RuntimeException re) {
            re.printStackTrace();
            LOG.info(re.getMessage());
            response.put("status", "failed");
            response.put("description", "Maaf, Terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi (03).");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        } catch (OutOfMemoryError oe) {
            LOG.info(oe.getMessage());
            response.put("status", "failed");
            response.put("description", "Maaf, sistem saat ini sedang overload. Silakan coba beberapa saat lagi.");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(response, httpStatus);
    }
}
