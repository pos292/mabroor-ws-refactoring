package com.mabroor.ws.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabroor.ws.service.MotorTypeService;

@RestController
@RequestMapping("/v1/api/motor")
public class MotorTypeController {

	@Autowired
	private MotorTypeService typeService;


	@GetMapping(value = "/types/list")
	@ResponseBody
	public LinkedHashMap<String, Object> getList() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",typeService.getListMotorType());
		return response;
	}

	@GetMapping(value = "/types/{typeId}")
	public LinkedHashMap<String, Object> getTypeById(@PathVariable Long typeId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",typeService.findById(typeId));
		return response;
	}
	
	@GetMapping(value = "/types/brand/{brandId}")
	public LinkedHashMap<String, Object> getTypeByBrandId(@PathVariable Long brandId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",typeService.getListMotorBrandId(brandId));
		return response;
	}
	
	@GetMapping(value = "/types/brand/{brandId}/{categoryId}")
	public LinkedHashMap<String, Object> getTypeByBrandIdCategoryId(@PathVariable Long brandId, @PathVariable Long categoryId) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("status", "success");
		response.put("description", "succesfully get data");
		response.put("data",typeService.getListMotorBrandIdCategoryId(brandId, categoryId));
		return response;
	}

}
