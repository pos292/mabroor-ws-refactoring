package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mabroor.ws.entity.MstTenorMotor;

public interface TenorMotorRepository extends JpaRepository<MstTenorMotor, Long> {
	
	@Query(value = "SELECT * FROM mst_tenor_motor WHERE status = '01' ORDER BY id ASC", nativeQuery = true)
	List<MstTenorMotor> getList();

}
