package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mabroor.ws.entity.MstSholat;

public interface SholatRepository extends JpaRepository<MstSholat, Long>{
	
	@Query(value = "SELECT * FROM mst_sholat WHERE status = '01' ORDER BY date ASC", nativeQuery = true)
    List<MstSholat> getListSholat();
	
	@Query(value = "SELECT * FROM mst_sholat WHERE NOW()<= date and location =?1 ORDER BY date ASC LIMIT 1", nativeQuery = true)
	 List<MstSholat> getWaktuSholat(String location);
	
	@Query(value = "SELECT * FROM mst_sholat WHERE location =?1 and date\\:\\:DATE =?2\\:\\:DATE  ORDER BY date ASC", nativeQuery = true)
	List<MstSholat> getLocationAndTime(String prov, String date);
	
	@Query(value = "SELECT * FROM mst_sholat WHERE location =?1  ORDER BY location ASC", nativeQuery = true)
	List<MstSholat> getLocation(String prov);
	
	@Query(value = "SELECT * FROM mst_sholat WHERE date\\:\\:DATE =?1\\:\\:DATE ORDER BY date ASC", nativeQuery = true)
	List<MstSholat> getDate(String date);
	
	@Query(value = "SELECT DISTINCT ON (a.location) * FROM (SELECT * FROM mst_sholat ORDER BY location ASC) a", nativeQuery = true)
	List<MstSholat> findByLocation();


}
