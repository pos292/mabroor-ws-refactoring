package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstUserMobileMenuCategory;

public interface UserMobileMenuCategoryRepository extends PagingAndSortingRepository<MstUserMobileMenuCategory, Long>{
	
	@Query(value = "SELECT * FROM mst_usermobile_menu_category WHERE status = '01'", nativeQuery = true)
	 List<MstUserMobileMenuCategory> getList();

}
