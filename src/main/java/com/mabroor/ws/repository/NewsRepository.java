package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstNews;
@Transactional
@Repository
public interface NewsRepository extends PagingAndSortingRepository<MstNews, Long> {
	@Query(value = "SELECT * FROM mst_news WHERE status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstNews> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_news WHERE status = '01' AND main_product_id = ?1 ORDER BY created_date DESC ", nativeQuery = true)
	List<MstNews> findByMainProductId(Long idMainProduct);
	
	@Query(value = "SELECT * FROM mst_news WHERE status = '01' AND main_product_id = ?1 ORDER BY created_date DESC limit 3 ", nativeQuery = true)
	List<MstNews> findByMainProductIdLimit(Long idMainProduct);
	
	@Query(value = "SELECT * FROM mst_news WHERE status = '01' ORDER BY created_date DESC limit 3", nativeQuery = true)
	List<MstNews> findByMainProductLimited();

	@Query(value = "select DISTINCT on (sub_category) * from mst_news where status = '01'  AND sub_category != 'Finansial' order by sub_category asc ", nativeQuery = true)
	List<MstNews> findActiveSubCategory();
	
	@Query(value = "(SELECT * FROM mst_news WHERE status = '01' AND sub_category = ?1 ORDER BY created_date DESC limit 3)", nativeQuery = true)
	List<MstNews> findBySubCategoryName(String subCategoryName);
	
	@Query(value = "SELECT * FROM mst_news WHERE status = '01' ORDER BY created_date DESC ", nativeQuery = true)
	List<MstNews> findAllLatest();

	@Query(value = "(SELECT * FROM mst_news WHERE status = '01' AND sub_category = ?1 ORDER BY created_date DESC)", nativeQuery = true)
	List<MstNews> findBySubCategoryNameAll(String subcategoryName);

	@Query(value = "SELECT * FROM mst_news WHERE status = '01' AND main_product_id = ?1 AND sub_category = ?2 ORDER BY created_date DESC", nativeQuery = true)
	List<MstNews> findByMainProductIdandSubCategoryName(Long idMainProduct, String subcategoryName);
}
