package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPosisi;

@Transactional
public interface PosisiRepository extends PagingAndSortingRepository<MstPosisi, Long> {

	@Query(value = "SELECT * FROM mst_posisi WHERE status != '00'", nativeQuery = true)
	List<MstPosisi> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_posisi ORDER BY posisi ASC", nativeQuery = true)
    List<MstPosisi> getListPosisi();
	
}
