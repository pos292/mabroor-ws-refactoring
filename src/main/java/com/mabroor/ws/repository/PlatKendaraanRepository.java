package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPlatKendaraan;

public interface PlatKendaraanRepository extends PagingAndSortingRepository<MstPlatKendaraan, Long> {

	@Query(value = "SELECT * FROM mst_plat_kendaraan ORDER BY plat_code ASC", nativeQuery = true)
    List<MstPlatKendaraan> getListPlat();
}
