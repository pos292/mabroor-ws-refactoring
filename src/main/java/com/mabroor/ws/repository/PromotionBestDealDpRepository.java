package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPromotionBestDealDp;

public interface PromotionBestDealDpRepository extends PagingAndSortingRepository<MstPromotionBestDealDp, Long> {
	
	@Query(value = "SELECT * FROM mst_promotion_bestdeal_dp WHERE promo_id =?1 ", nativeQuery = true)
	List<MstPromotionBestDealDp> findAllByPromotion(Long id);

}
