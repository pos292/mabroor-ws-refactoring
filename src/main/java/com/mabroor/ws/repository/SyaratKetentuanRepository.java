package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstFAQ;
import com.mabroor.ws.entity.MstSyaratKetentuan;

@Repository
@Transactional
public interface SyaratKetentuanRepository extends PagingAndSortingRepository<MstSyaratKetentuan, Long>{

	@Query(value = "SELECT * FROM mst_syarat_ketentuan WHERE status != '00' ", nativeQuery = true)
	List<MstSyaratKetentuan> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_syarat_ketentuan WHERE status != '00' ", nativeQuery = true)
	List<MstSyaratKetentuan> findAllById();

	@Modifying
	@Query(value = "UPDATE mst_syarat_ketentuan SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateSyaratKetentuanById(String status, Long id);
	
	@Query(value = "SELECT * FROM mst_syarat_ketentuan WHERE status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstSyaratKetentuan> findAllEnable();
	
	@Query(value = "SELECT * FROM mst_syarat_ketentuan WHERE status = '01' ORDER BY created_date ASC", nativeQuery = true)
    List<MstSyaratKetentuan> getListSyarat();
	
	@Query(value = "SELECT * FROM mst_syarat_ketentuan where status = '01' and category = ?1 ORDER BY created_date ASC", nativeQuery = true)
	List<MstSyaratKetentuan> findByCategory(String categoryName);
	
}
