package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstKebijakanPrivasi;

@Repository
@Transactional
public interface KebijakanPrivasiRepository extends PagingAndSortingRepository<MstKebijakanPrivasi, Long> {

	@Query(value = "SELECT * FROM mst_kebijakan_privasi where status = '01' ORDER BY created_date ASC", nativeQuery = true)
    List<MstKebijakanPrivasi> getListPrivasi();
}
