package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstAvailableTime;

public interface AvailableTimeRepository extends PagingAndSortingRepository<MstAvailableTime, Long> {

	@Query(value = "SELECT * FROM mst_available_time WHERE status != '00' ORDER BY created_date DESC", nativeQuery = true)
	List<MstAvailableTime> findAllByStatus();

	@Query(value = "SELECT * FROM mst_available_time WHERE status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstAvailableTime> findAllEnable();
	
	@Query(value = "SELECT * FROM mst_available_time WHERE status = '01' ORDER BY seq ASC", nativeQuery = true)
	List<MstAvailableTime> getListAvailableTime();
	
	@Query(value = "SELECT * FROM mst_available_time WHERE id IN(1?)", nativeQuery = true)
	List<MstAvailableTime> findByListId(String[] id);
}
