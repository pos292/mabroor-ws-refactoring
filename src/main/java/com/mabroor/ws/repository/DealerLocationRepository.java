package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstDealerLocation;

@Repository
public interface DealerLocationRepository extends PagingAndSortingRepository<MstDealerLocation, Long> {
	@Query(value = "SELECT * FROM mst_dealer_location WHERE status != '00'", nativeQuery = true)
	List<MstDealerLocation> findAllByStatus();
	
	@Modifying
	@Query(value = "UPDATE mst_dealer_location SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateDealerLocationById(String status, Long id);

	@Query(value = "SELECT * FROM mst_dealer_location WHERE status = '01'", nativeQuery = true)
    List<MstDealerLocation> getListDealer();

	
	
}