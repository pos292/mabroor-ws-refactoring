package com.mabroor.ws.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotor;
import com.mabroor.ws.entity.MstMotorUnit;

@Repository
public interface MotorUnitRepository extends PagingAndSortingRepository<MstMotorUnit, Long>,JpaSpecificationExecutor<MstMotorUnit>{
	@Query(value = "SELECT * FROM mst_motor_unit WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorUnit> findAllByStatus();

	@Modifying
	@Query(value = "UPDATE mst_motor_unit SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateMotorCategoryById(String status, Long id);

	@Query(value = "SELECT * FROM mst_motor_unit WHERE status = '01' AND motor_category_id = ?1", nativeQuery = true)
	List<MstMotorUnit> findByMotorCategoryfId(Long id);

	@Query(value = "SELECT * FROM mst_motor_unit WHERE status = '01' ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorUnit> getMotorCategory();
	
	@Query(value = "SELECT * FROM mst_motor_unit WHERE status = '01' ORDER BY title ASC", nativeQuery = true)
	List<MstMotorUnit> findAllByTitleAsc();
	
	@Query(value = "SELECT * FROM mst_motor_unit WHERE id IN (?1)", nativeQuery = true)
	List<MstMotorUnit> findByListId(Long[] id);
	
	@Modifying
	@Query(value = "UPDATE mst_product_unit SET seq = newSeq FROM mst_motor_unit a "
			+ "INNER JOIN (SELECT id, row_number() OVER (ORDER BY seq ASC, updated_date DESC) AS newSeq FROM mst_motor_unit) b ON b.id = a.id", nativeQuery = true)
	int updateMotorCategorySeq();
	
	@Query(value = "select * from mst_motor_unit where status = '01' order by price asc", nativeQuery = true)
	List<MstMotorUnit> getMotorTypeByLowCicilan();
	
	@Query(value = "select * from mst_motor_unit where status = '01' and best_deal = 'YES' order by price asc limit 10", nativeQuery = true)
	List<MstMotorUnit> getMotorTypeByLowCicilanBestDeal();
	
	@Query(value = "select * from mst_motor_unit where status = '01' and motor_type_id = ?1 order by price asc", nativeQuery = true)
	List<MstMotorUnit> getMotorTypeByLowCicilanByTypeId(Long typeId);
	
	@Query(value = "select * from mst_motor_unit where status = '01' and motor_type_id = ?1 and best_deal = 'YES' order by price asc limit 10", nativeQuery = true)
	List<MstMotorUnit> getMotorTypeByLowCicilanByTypeIdBestDeal(Long typeId);
	
	@Query(value = "select min(price) from mst_motor_unit where motor_type_id = ?1 and status = '01'", nativeQuery = true)
	BigDecimal minPrice(Long typeId);
	
	@Query(value = "select max(price) from mst_motor_unit where motor_type_id = ?1 and status = '01'", nativeQuery = true)
	BigDecimal maxPrice(Long typeId);
	
	@Query(value = "select min(cicilan) from mst_motor_unit where motor_type_id = ?1 and status = '01'", nativeQuery = true)
	BigDecimal minCicilan(Long typeId);
	
	@Query(value = "select max(cicilan) from mst_motor_unit where motor_type_id = ?1 and status = '01'", nativeQuery = true)
	BigDecimal maxCicilan(Long typeId);
	
	
	
	
	

}
