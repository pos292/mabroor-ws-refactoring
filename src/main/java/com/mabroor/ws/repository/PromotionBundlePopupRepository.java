package com.mabroor.ws.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstPromotionBundlePopup;

@Repository
@Transactional
public interface PromotionBundlePopupRepository extends PagingAndSortingRepository<MstPromotionBundlePopup, Long>{

	@Query(value = "SELECT * FROM mst_promotion_bundle_popup WHERE status = '01'", nativeQuery = true)
	List<MstPromotionBundlePopup> findAllByStatus();

	@Modifying
	@Query(value = "UPDATE mst_promotion_bundle_popup SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updatePromotionById(String status, Long id);
	
	@Query(value = "SELECT * FROM mst_promotion_bundle_popup WHERE status = '01'", nativeQuery = true)
	List<MstPromotionBundlePopup> getListPromotion();
	
	@Query(value = "SELECT * FROM mst_promotion_bundle_popup \n" + 
			"where \n" + 
			"(\n" + 
			"(start_date\\:\\:date >= now()\\:\\:date and now()\\:\\:date <= end_date\\:\\:date)\n" + 
			") \n" + 
			"AND status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstPromotionBundlePopup> findAllByPeriode();
}
