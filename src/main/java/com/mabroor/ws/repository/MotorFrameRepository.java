package com.mabroor.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorFrame;

@Repository
public interface MotorFrameRepository extends JpaRepository<MstMotorFrame, Long>{
	@Query(value = "SELECT * FROM mst_motor_frame WHERE motor_unit_id = ?1", nativeQuery = true)
	MstMotorFrame findByMotorUnitId(Long id);
}
