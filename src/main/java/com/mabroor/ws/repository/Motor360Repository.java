package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotor360;
import com.mabroor.ws.entity.MstMotorColor;

@Repository
public interface Motor360Repository extends JpaRepository<MstMotor360, Long>  {
	List<MstMotor360> findByMotorColor(MstMotorColor mstMotorColor);

	@Modifying
	@Query("DELETE FROM MstMotor360 a WHERE a.id=?1")
	void delete360ById(Long id);

	@Query(value = "SELECT * FROM mst_motor_360 WHERE motor_type_id = ?1", nativeQuery = true)
	List<MstMotor360> findByMotorTypeId(Long id);
	
	@Query(value = "SELECT * FROM mst_motor_360 WHERE motor_color_id = ?1", nativeQuery = true)
	List<MstMotor360> findByMotorColorId(Long id);
}
