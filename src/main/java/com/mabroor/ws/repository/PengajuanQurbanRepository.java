package com.mabroor.ws.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPengajuanQurban;

public interface PengajuanQurbanRepository extends PagingAndSortingRepository<MstPengajuanQurban, Long>{

}
