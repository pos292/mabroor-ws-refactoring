package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstNeeds;

@Repository
public interface NeedsRepository extends PagingAndSortingRepository<MstNeeds, Long> {

	@Query(value = "SELECT * FROM mst_needs WHERE status != '00'", nativeQuery = true)
	List<MstNeeds> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_needs WHERE status = '01' ORDER BY needs_title ASC ", nativeQuery = true)
    List<MstNeeds> getListNeeds();
}
