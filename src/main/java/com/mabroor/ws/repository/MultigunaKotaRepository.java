package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstMultigunaKota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MultigunaKotaRepository extends JpaRepository<MstMultigunaKota, Long> {

    @Query(value = "select * from mst_multiguna_kota where id = ?1",nativeQuery = true)
    MstMultigunaKota findByIdKota(Long id);
}
