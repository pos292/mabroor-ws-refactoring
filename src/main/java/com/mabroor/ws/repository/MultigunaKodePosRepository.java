package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstMultigunaKelurahan;
import com.mabroor.ws.entity.MstMultigunaKodePos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MultigunaKodePosRepository extends JpaRepository<MstMultigunaKodePos, Long> {
    @Query(value = "select * from mst_multiguna_kode_pos where id_kelurahan =?1", nativeQuery = true)
    List<MstMultigunaKodePos> findByIdKelurahan(Long id_kelurahan);

    @Query(value = "select * from mst_multiguna_kode_pos where code =?1", nativeQuery = true)
    List<MstMultigunaKodePos> findByCode(String code);
}
