package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstMainProduct;

public interface MainProductRepository extends PagingAndSortingRepository<MstMainProduct, Long> {

	@Query(value = "SELECT * FROM mst_main_product WHERE status = '01' ", nativeQuery = true)
    List<MstMainProduct> getListActive();
	
}
