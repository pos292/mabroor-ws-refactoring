package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstNeedsTanpaJaminan;

@Repository
public interface NeedsTanpaJaminanRepository extends PagingAndSortingRepository<MstNeedsTanpaJaminan, Long> {

	@Query(value = "SELECT * FROM mst_needs_tanpajaminan WHERE status != '00'", nativeQuery = true)
	List<MstNeedsTanpaJaminan> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_needs_tanpajaminan WHERE status = '01' ORDER BY needs_title ASC", nativeQuery = true)
    List<MstNeedsTanpaJaminan> getListNeedsTanpaJaminan();
}
