package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstOwnerRegion;

public interface OwnerRegionRepository extends PagingAndSortingRepository<MstOwnerRegion, Long> {

	@Query(value = "SELECT * FROM mst_owner_region WHERE status = '01' order by owner_region asc", nativeQuery = true)
	List<MstOwnerRegion> getList();
}
