package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstApplication;

public interface ApplicationRepository extends PagingAndSortingRepository<MstApplication , Long> {

	@Query(value = "SELECT * FROM mst_application  WHERE leads_id =?1 AND leads_type =?2", nativeQuery = true)
	List<MstApplication> findByLeadsIdAndLeadsType(Long leadsId , String leadsType);
	
	@Query(value = "SELECT * FROM mst_application", nativeQuery = true)
	List<MstApplication> getList();
	
	@Query(value = "SELECT * FROM mst_application  WHERE leads_id =?1", nativeQuery = true)
	MstApplication findByCarBaru(Long leadsId);
}
