package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPromotion;
import com.mabroor.ws.entity.MstPromotionBundle;
import com.mabroor.ws.entity.MstPromotionBundleContentData;

public interface PromotionBundleContentDataRepository extends PagingAndSortingRepository<MstPromotionBundleContentData, Long>{
	
	@Query(value = "SELECT * FROM mst_promotion_bundle_content_data WHERE NOW()>=start_date AND NOW()<=end_date AND is_active_status = 'YES' and status = '01'", nativeQuery = true)
    Optional<MstPromotionBundleContentData> getListDetailPromoData();
	
	

}
