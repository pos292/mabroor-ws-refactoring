package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstTujuanPinjaman;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TujuanPinjamanRepository extends JpaRepository<MstTujuanPinjaman, Long> {
}
