package com.mabroor.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorEngine;

@Repository
public interface MotorEngineRepository extends JpaRepository<MstMotorEngine, Long>{
	@Query(value = "SELECT * FROM mst_motor_engine WHERE motor_unit_id = ?1", nativeQuery = true)
	MstMotorEngine findByMotorUnitId(Long id);
}
