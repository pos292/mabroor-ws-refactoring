package com.mabroor.ws.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPengajuanCar;

public interface CarRoundRobinRepository extends PagingAndSortingRepository<MstPengajuanCar, Long> {
	@Query(value = "SELECT * FROM mst_pengajuan_car WHERE status != '00' ORDER BY created_date DESC LIMIT 1", nativeQuery = true)
	MstPengajuanCar findAllByStatus();
}
