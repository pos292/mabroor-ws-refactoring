package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPengajuanAsuransiMobil;

public interface PengajuanAsuransiMobilRepository extends PagingAndSortingRepository<MstPengajuanAsuransiMobil, Long> {

	@Query(value = "SELECT * FROM mst_pengajuan_asuransi_mobil ORDER BY kendaraan ASC", nativeQuery = true)
	List<MstPengajuanAsuransiMobil> getListAsuransiMobil();
	
	@Query(value = "SELECT * FROM mst_pengajuan_asuransi_mobil WHERE status = '01' and user_id = ?1", nativeQuery = true)
	List<MstPengajuanAsuransiMobil> findAllByUserId(Long id);
	
    @Query(value = "SELECT * FROM mst_pengajuan_asuransi_mobil WHERE encryption_status ISNULL limit ?1", nativeQuery = true)
	List<MstPengajuanAsuransiMobil> findAllByNonEncryptedAsuransiMobil(Long limit);
}


