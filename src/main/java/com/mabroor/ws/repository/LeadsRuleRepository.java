package com.mabroor.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.mabroor.ws.entity.MstLeadsRule;

public interface LeadsRuleRepository extends CrudRepository<MstLeadsRule, Long> {
	
	

}
