package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPengajuanMotor;

public interface PengajuanMotorRepository extends PagingAndSortingRepository<MstPengajuanMotor, Long> {

	@Query(value = "SELECT * FROM mst_pengajuan_motor WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstPengajuanMotor> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_pengajuan_motor", nativeQuery = true)
    List<MstPengajuanMotor> getListPengajuan();
    
    @Query(value = "SELECT * FROM mst_pengajuan_motor WHERE status = '01' ORDER BY name ASC", nativeQuery = true)
	List<MstPengajuanMotor> findAllByTitleAsc();

    @Query(value = "SELECT * FROM mst_pengajuan_motor WHERE status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstPengajuanMotor> findAllEnable();

    @Query(value = "SELECT * FROM mst_pengajuan_motor WHERE status = '01' and user_id = ?1", nativeQuery = true)
	List<MstPengajuanMotor> findAllByUserId(Long id);
    
    @Query(value = "SELECT * FROM mst_pengajuan_motor WHERE encryption_status ISNULL limit ?1", nativeQuery = true)
	List<MstPengajuanMotor> findAllByNonEncryptedMotor(Long limit);
    
//    @Query(value = "SELECT * FROM mst_pengajuan_motor WHERE status != '00' and created_date\\:\\:date >= '2020-06-21 22:52:06.938' \\:\\:date and created_date\\:\\:date <= '2021-07-21 12:33:06.583' \\:\\:date ORDER BY created_date DESC", nativeQuery = true)
//	List<MstPengajuanMotor> findAllByNonEncryptedMotor(Long limit);
}
