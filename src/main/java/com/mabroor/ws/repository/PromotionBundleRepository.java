package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPromotionBundle;

public interface PromotionBundleRepository extends PagingAndSortingRepository<MstPromotionBundle, Long> {

	@Query(value = "SELECT * FROM mst_promotion_bundle WHERE status ='01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstPromotionBundle> getList();
	
	@Query(value = "SELECT * FROM mst_promotion_bundle WHERE status ='01' AND user_id=?1", nativeQuery = true)
	List<MstPromotionBundle> findByData(Long id);
	
	@Query(value = "SELECT * FROM mst_promotion_bundle WHERE email ISNULL AND user_id=?1", nativeQuery = true)
	List<MstPromotionBundle> findByEmailNull(Long id);
}
