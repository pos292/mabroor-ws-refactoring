package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstInboxCategory;

public interface InboxCategoryRepository extends PagingAndSortingRepository<MstInboxCategory, Long> {

	@Query(value = "SELECT * FROM mst_inbox_category WHERE status = '01' order by seq asc", nativeQuery = true)
    List<MstInboxCategory> getInboxCategory();

	@Query(value = "SELECT * FROM mst_inbox_category WHERE status = '01' and category in ('Promo' , 'Status Transaksi') order by seq asc", nativeQuery = true)
	List<MstInboxCategory> getInboxCategoryAfterLogin();

	@Query(value = "SELECT * FROM mst_inbox_category WHERE status = '01' and need_login = 'NO' order by seq asc", nativeQuery = true)
	List<MstInboxCategory> getInboxCategoryBeforeLogin();

}
