package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstProductInformationBenefit;

@Repository
public interface ProductInformationBenefitRepository  extends PagingAndSortingRepository<MstProductInformationBenefit, Long>{
	
	@Query(value = "SELECT * FROM mst_product_information_benefit WHERE product_information_id = ?1 and status = '01' order by created_date asc ", nativeQuery = true)
    List<MstProductInformationBenefit> getListbyProductInfo(Long idProductInformation);
}
