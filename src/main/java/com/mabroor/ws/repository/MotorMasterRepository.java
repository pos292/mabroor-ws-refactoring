package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorMaster;

@Repository
@Transactional
public interface MotorMasterRepository extends PagingAndSortingRepository<MstMotorMaster, Long>{
	
	@Query(value = "SELECT * FROM mst_motor_master order by brand asc", nativeQuery = true)
	List<MstMotorMaster> findAll();
	
	@Modifying
	@Query(value = "truncate mst_motor_master", nativeQuery = true)
	void truncateMaster();
	
	@Query(value = "SELECT DISTINCT on (brand) * FROM mst_motor_master ORDER BY brand asc", nativeQuery = true)
	List<MstMotorMaster> findByBrandMotor();
	
	@Query(value = "SELECT DISTINCT on (type) * FROM mst_motor_master WHERE brand_value =?1 ORDER BY type asc", nativeQuery = true)
	List<MstMotorMaster> findByTypeMotor(String brand);
	
	@Query(value = "SELECT DISTINCT on (model) * FROM mst_motor_master WHERE type_value =?1 ORDER BY model asc", nativeQuery = true)
	List<MstMotorMaster> findByModelMotor(String type);
	
	@Query(value = "SELECT DISTINCT on (year) * FROM mst_motor_master WHERE model_value =?1 ORDER BY year asc", nativeQuery = true)
	List<MstMotorMaster> findByYearMotor(String model);
	
	@Query(value = "SELECT DISTINCT on (year) * FROM mst_motor_master WHERE brand_value =?1 AND type_value =?2 AND model_value =?3 ORDER BY year asc", nativeQuery = true)
	List<MstMotorMaster> findByYearAndModelTypeMotor(String brand, String type, String model);
	
	@Query(value = "SELECT * FROM mst_motor_master WHERE brand_value =?1 AND type_value =?2 AND model_value =?3 AND year =?4 ORDER BY price ASC LIMIT 1", nativeQuery = true)
	Optional<MstMotorMaster> findByCalculateMrp(String brandValue, String typeValue, String modelValue, String year);

	@Query(value = "SELECT DISTINCT on (model) * FROM mst_motor_master WHERE brand_value =?1 AND type_value =?2 ORDER BY model asc", nativeQuery = true)
	List<MstMotorMaster> findByBrandTypeModelMotor(String brand, String type);

	@Query(value = "SELECT DISTINCT on (type) * FROM mst_motor_master ORDER BY type asc", nativeQuery = true)
	List<MstMotorMaster> findByTypeMotorDistinct();

}
