package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstDoa;

public interface DoaRepository extends PagingAndSortingRepository<MstDoa, Long>{

	@Query(value = "SELECT * FROM mst_doa where duplicate isnull ORDER BY id ASC", nativeQuery = true)
	List<MstDoa> listDoa();
	
//	@Query(value = "SELECT * FROM mst_doa WHERE id_category = ?1 and lower(nama_doa) like lower(?2) ORDER BY id ASC limit ?3 offset ?4", nativeQuery = true)
//	List<MstDoa> listDoaByIdCategory(Long idCategory , String namaDoa, Long limit, Long page);
	
	@Query(value = "SELECT * FROM mst_doa WHERE id_category = ?1 ORDER BY id ASC", nativeQuery = true)
	List<MstDoa> listDoaByIdCategory(Long idCategory);
	
	@Query(value = "SELECT * FROM mst_doa WHERE lower(nama_doa) like lower(?1) and duplicate isnull ORDER BY id ASC", nativeQuery = true)
	List<MstDoa> listSearchDoa(String namaDoa);
}
