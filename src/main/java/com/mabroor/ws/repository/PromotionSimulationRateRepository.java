package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mabroor.ws.entity.MstPromotionSimulationRate;

public interface PromotionSimulationRateRepository extends JpaRepository<MstPromotionSimulationRate, Long> {
	
	
	@Query(value = "SELECT * FROM mst_promotion_simulation_rate WHERE promo_id = ?1 ORDER BY created_date DESC", nativeQuery = true)
	List<MstPromotionSimulationRate> getByPromoId(Long id);

}
