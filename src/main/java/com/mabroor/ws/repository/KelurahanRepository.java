package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstKelurahan;

@Repository
public interface KelurahanRepository extends PagingAndSortingRepository<MstKelurahan, Long> {

	@Query(value = "SELECT * FROM mst_kelurahan WHERE status != '00'", nativeQuery = true)
	List<MstKelurahan> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_kelurahan WHERE status = '01'", nativeQuery = true)
    List<MstKelurahan> getListKelurahan();
	
	@Query(value = "SELECT * FROM mst_kelurahan WHERE status = '01' AND kecamatan_id = ?1", nativeQuery = true)
	List<MstKelurahan> findByKecamatanId(Long id);
}
