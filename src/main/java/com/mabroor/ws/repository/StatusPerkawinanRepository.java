package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstStatusPerkawinan;

@Transactional
public interface StatusPerkawinanRepository extends PagingAndSortingRepository<MstStatusPerkawinan, Long> {
	@Query(value = "SELECT * FROM mst_status_perkawinan WHERE status != '00'", nativeQuery = true)
	List<MstStatusPerkawinan> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_status_perkawinan", nativeQuery = true)
    List<MstStatusPerkawinan> getListPerkawinanStatus();

}
