package com.mabroor.ws.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstApplicationTransaction;

@Transactional
public interface ApplicationTransactionRepository  extends PagingAndSortingRepository<MstApplicationTransaction , Long>  {
	
	@Modifying
	@Query(value = "DELETE FROM mst_application_transaction  WHERE application_id =?1", nativeQuery = true)
	void deleteAppId(Long Id);

}
