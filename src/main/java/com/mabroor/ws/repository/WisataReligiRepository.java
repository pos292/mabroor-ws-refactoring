package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstWisataReligi;

@Transactional
@Repository
public interface WisataReligiRepository extends PagingAndSortingRepository<MstWisataReligi, Long> {
	@Query(value = "SELECT * FROM mst_wisata_religi WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstWisataReligi> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_wisata_religi WHERE status = '01'", nativeQuery = true)
    List<MstWisataReligi> getListWisata();
	
	@Query(value = "SELECT * FROM mst_wisata_religi WHERE status = '01' and user_id = ?1", nativeQuery = true)
	List<MstWisataReligi> findAllByUserId(Long id);
	
	@Query(value = "SELECT * FROM mst_wisata_religi WHERE status = '01' and integration_leads_id = ?1", nativeQuery = true)
	MstWisataReligi findLeadsId(String id);

}
