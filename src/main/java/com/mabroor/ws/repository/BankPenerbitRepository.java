package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstBankPenerbit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankPenerbitRepository extends JpaRepository<MstBankPenerbit, Long> {
}
