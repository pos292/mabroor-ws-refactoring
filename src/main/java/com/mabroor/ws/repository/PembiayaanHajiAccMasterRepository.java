package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import com.mabroor.ws.entity.MstHajiAccMaster;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface PembiayaanHajiAccMasterRepository extends PagingAndSortingRepository<MstHajiAccMaster, Long>{

    @Query(value = "SELECT * FROM mst_haji_acc_master", nativeQuery = true)
    List<MstHajiAccMaster> findAll();

    @Query(value = "SELECT DISTINCT on (jumlah_peserta) * FROM mst_haji_acc_master ORDER BY jumlah_peserta asc", nativeQuery = true)
    List<MstHajiAccMaster> getJumlahPesertaAcc();

    @Query(value = "SELECT DISTINCT on (tenor) * FROM mst_haji_acc_master ORDER BY tenor asc", nativeQuery = true)
    List<MstHajiAccMaster> getTenorAcc();

    @Query(value = "SELECT DISTINCT on (product_name) * FROM mst_haji_acc_master ORDER BY product_name asc", nativeQuery = true)
    List<MstHajiAccMaster> getProductAcc();

    // @Query(value = "SELECT * FROM mst_haji_acc_master WHERE productname = ?1 AND tenor = ?2 AND jumlahpeserta = ?3 LIMIT 1", nativeQuery = true)
    // Optional<MstHajiAccMaster> findByCalculateCost(String productName, String tenor, String jumlahPeserta);

}
