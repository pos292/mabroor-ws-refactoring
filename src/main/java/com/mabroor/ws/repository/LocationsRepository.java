package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstLocations;


@Repository
public interface LocationsRepository extends PagingAndSortingRepository<MstLocations, Long> {
	@Query(value = "SELECT * FROM mst_locations WHERE status != '00'", nativeQuery = true)
	List<MstLocations> findAllByStatus();
	@Modifying
	@Query(value = "UPDATE mst_locations SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateLocationById(String status, Long id);

	@Query(value = "SELECT * FROM mst_locations WHERE status = '01' order by name asc", nativeQuery = true)
    List<MstLocations> getListLocations();
	
	@Query(value = "SELECT * FROM mst_locations WHERE booking_test_drive_available = 'YES'", nativeQuery = true)
    List<MstLocations> getListBtdAvailable();


	
	
}
