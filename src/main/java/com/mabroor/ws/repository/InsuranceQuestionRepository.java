package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstInsuranceQuestion;

public interface InsuranceQuestionRepository extends PagingAndSortingRepository<MstInsuranceQuestion, Long> {

	@Query(value = "SELECT * FROM mst_insurance_question", nativeQuery = true)
    List<MstInsuranceQuestion> getListInscQuest();
	
	@Query(value = "SELECT * FROM mst_insurance_question WHERE insurance_id = ?1 ", nativeQuery = true)
    List<MstInsuranceQuestion> getListInscQuestByInsuranceId(Long insuranceId);
}
