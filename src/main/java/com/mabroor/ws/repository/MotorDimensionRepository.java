package com.mabroor.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorDimension;

@Repository
public interface MotorDimensionRepository extends JpaRepository<MstMotorDimension, Long> {
	@Query(value = "SELECT * FROM mst_motor_dimension WHERE motor_unit_id = ?1", nativeQuery = true)
	MstMotorDimension findByMotorUnitId(Long id);
}
