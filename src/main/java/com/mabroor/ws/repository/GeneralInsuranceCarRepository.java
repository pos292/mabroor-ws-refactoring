package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstGeneralInsuranceCar;

public interface GeneralInsuranceCarRepository extends PagingAndSortingRepository<MstGeneralInsuranceCar, Long> {

	@Query(value = "SELECT * FROM mst_general_insurance_car WHERE status != '00'", nativeQuery = true)
	List<MstGeneralInsuranceCar> getAll();

	@Query(value = "SELECT * FROM mst_general_insurance_car WHERE main_product_id = ?1 and status = '01'", nativeQuery = true)
    Optional<MstGeneralInsuranceCar> getListInscType(Long id);
}
