package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstKecamatan;

@Repository
public interface KecamatanRepository extends PagingAndSortingRepository<MstKecamatan, Long> {

	@Query(value = "SELECT * FROM mst_kecamatan WHERE status != '00'", nativeQuery = true)
	List<MstKecamatan> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_kecamatan WHERE status = '01'", nativeQuery = true)
    List<MstKecamatan> getListKecamatan();
	
	@Query(value = "SELECT * FROM mst_kecamatan WHERE status = '01' AND kabupaten_id = ?1 order by nama_kecamatan asc", nativeQuery = true)
	List<MstKecamatan> findByKabupatenId(Long id);
}
