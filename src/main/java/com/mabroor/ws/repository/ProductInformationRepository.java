package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstProductInformation;

@Repository
public interface ProductInformationRepository extends PagingAndSortingRepository<MstProductInformation, Long>{
	@Query(value = "SELECT * FROM mst_product_information WHERE status != '00' order by created_date desc", nativeQuery = true)
	List<MstProductInformation> getAll();

	@Query(value = "SELECT * FROM mst_product_information WHERE main_product_id = ?1 and status = '01' order by created_date desc", nativeQuery = true)
    List<MstProductInformation> getListMainProduct(Long idMainProduct);
}
