package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstLoanManagement;

@Repository
@Transactional
public interface LoanManagementRepository extends PagingAndSortingRepository<MstLoanManagement, Long> {

	@Query(value = "SELECT * FROM mst_loan_management where status = '01'", nativeQuery = true)
    List<MstLoanManagement> getList();
	
	@Query(value = "SELECT * FROM mst_loan_management where loan_type = ?1 and status = '01'", nativeQuery = true)
    Optional<MstLoanManagement> getListLoanType(String loanType);
}
