package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstCityBranch;

public interface CityBranchRepository extends PagingAndSortingRepository<MstCityBranch, Long> {

	@Query(value = "SELECT * FROM mst_citybranch WHERE city = ?1", nativeQuery = true)
	List<MstCityBranch> getCityByPengajuan(String kotaPengajuan);
	
	@Query(value = "SELECT * FROM mst_citybranch WHERE status = '01' ORDER BY city ASC", nativeQuery = true)
	List<MstCityBranch> getList();
	
	@Query(value = "SELECT * FROM mst_citybranch WHERE id_kabupaten = ?1", nativeQuery = true)
	List<MstCityBranch> getCityByKabupatenId(Long idKabupaten);
}
