package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mabroor.ws.entity.MstProductPurchaseTenor;

public interface ProductPurchaseTenorRepository extends JpaRepository<MstProductPurchaseTenor, Long> {
	
	@Query(value = "SELECT * FROM mst_product_purchase_tenor WHERE status = '01'", nativeQuery = true)
	List<MstProductPurchaseTenor> getList();

}
