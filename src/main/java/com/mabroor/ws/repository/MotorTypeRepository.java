package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorType;


@Repository
public interface MotorTypeRepository extends PagingAndSortingRepository<MstMotorType, Long>{
	@Query(value = "SELECT * FROM mst_motor_type WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorType> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_motor_type WHERE status != '00' AND motor_brand_id = ?1 ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorType> findAllByBrandId(Long id);
	
	@Query(value = "SELECT * FROM mst_motor_type WHERE status = '01' and id in (\n" + 
			"	select motor_type_id from mst_motor_unit where status = '01'\n" + 
			") AND motor_category_id = ?1 ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorType> findAllByCategoryId(Long id);

	@Modifying
	@Query(value = "UPDATE mst_motor_type SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateMotorTypeById(String status, Long id);

	@Query(value = "SELECT * FROM mst_motor_type WHERE status = '01' and id in (\n" + 
			"	select motor_type_id from mst_motor_unit where status = '01'\n" + 
			") ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorType> getListMotorType();
	
	@Query(value = "SELECT * FROM mst_motor_type WHERE status = '01' and id in (\n" + 
			"	select motor_type_id from mst_motor_unit where status = '01'\n" + 
			") and motor_brand_id = ?1 ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorType> getListMotorBrandId(Long brandId);
	
	@Modifying
	@Query(value = "UPDATE mst_motor_type SET seq = newSeq FROM mst_motor_type a "
			+ "INNER JOIN (SELECT id, row_number() OVER (ORDER BY seq ASC, updated_date DESC) AS newSeq FROM mst_motor_type) b ON b.id = a.id", nativeQuery = true)
	int updateMotorTypeSeq();
	
	@Query(value = "SELECT a.* FROM mst_motor_type a\n" + 
			"join mst_motor_category b\n" + 
			"on a.motor_category_id = b.id WHERE a.status = '01'\n" + 
			"and a.motor_brand_id = ?1 and b.id = ?2 ORDER BY a.seq ASC", nativeQuery = true)
	List<MstMotorType> getListMotorBrandIdCategoryId(Long brandId, Long categoryId);
}
