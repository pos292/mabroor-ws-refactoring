package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstLogApi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogApiRepository extends JpaRepository<MstLogApi, Long> {
}
