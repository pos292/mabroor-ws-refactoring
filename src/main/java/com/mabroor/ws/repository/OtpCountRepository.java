package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstOtp;
import com.mabroor.ws.entity.MstOtpCount;

@Transactional
@Repository
public interface OtpCountRepository extends PagingAndSortingRepository<MstOtpCount, Long>{
	
	List<MstOtpCount> findByMstOtp(MstOtp value);
	
}
