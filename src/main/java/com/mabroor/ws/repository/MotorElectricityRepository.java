package com.mabroor.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorElectricity;

@Repository
public interface MotorElectricityRepository extends JpaRepository<MstMotorElectricity, Long>{
	@Query(value = "SELECT * FROM mst_motor_electricity WHERE motor_unit_id = ?1", nativeQuery = true)
	MstMotorElectricity findByMotorUnitId(Long id);
}
