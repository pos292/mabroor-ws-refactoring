package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstDataDiriPengajuanMultiguna;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataDiriPengajuanMultigunaRepository extends JpaRepository<MstDataDiriPengajuanMultiguna, Long> {
    MstDataDiriPengajuanMultiguna findByIdPengajuan(Long id);
}
