package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstStatusKepemilikanRumah;

@Transactional
public interface StatusKepemilikanRumahRepository extends PagingAndSortingRepository<MstStatusKepemilikanRumah, Long>{
	@Query(value = "SELECT * FROM mst_status_kepemilikan_rumah WHERE status != '00'", nativeQuery = true)
	List<MstStatusKepemilikanRumah> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_status_kepemilikan_rumah where status = '01' order by seq asc", nativeQuery = true)
    List<MstStatusKepemilikanRumah> getListKepemilikanRumahStatus();
}
