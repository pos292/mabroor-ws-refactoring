package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstMultigunaKecamatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MultigunaKecamatanRepository extends JpaRepository<MstMultigunaKecamatan, Long> {
    @Query(value = "select * from mst_multiguna_kecamatan where id_kota =?1", nativeQuery = true)
    List<MstMultigunaKecamatan> findByIdKota(Long id_kota);

    @Query(value = "select * from mst_multiguna_kecamatan where id =?1", nativeQuery = true)
    MstMultigunaKecamatan findByIdKecamatan(Long id);
}
