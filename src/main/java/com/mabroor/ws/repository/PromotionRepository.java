package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstPromotion;

@Repository
@Transactional
public interface PromotionRepository extends PagingAndSortingRepository<MstPromotion, Long>{
	
	@Query(value = "SELECT * FROM mst_promotion WHERE NOW()>=start_date AND NOW()<=end_date AND is_active_homepage = 'YES' and status = '01' ORDER BY created_date DESC", nativeQuery = true)
    List<MstPromotion> getListPromotion();
	
	@Query(value = "SELECT * FROM mst_promotion WHERE NOW()>=start_date AND NOW()<=end_date AND id_main_product = ?1  and status = '01' ORDER BY created_date DESC", nativeQuery = true)
    List<MstPromotion> getListPromotionByMainProduct(Long idMainProduct);

	@Query(value = "SELECT * FROM mst_promotion WHERE id = ?1 ORDER BY created_date DESC", nativeQuery = true)
    MstPromotion getPromotionByDetailPromo(Long id);

}
