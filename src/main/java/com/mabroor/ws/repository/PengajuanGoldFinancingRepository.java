package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPengajuanGoldFinancing;

public interface PengajuanGoldFinancingRepository extends PagingAndSortingRepository<MstPengajuanGoldFinancing, Long> { 

	@Query(value = "SELECT * FROM mst_pengajuan_gold_financing WHERE phone_data_user =?1 and cast(created_date as date) = cast(now() as date) ORDER BY created_date DESC", nativeQuery = true)
	List<MstPengajuanGoldFinancing> findByPhone(String phone);
}
