package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstDictionary;

@Repository
@Transactional
public interface DictionaryRepository extends PagingAndSortingRepository<MstDictionary, Long>{

	@Query(value = "SELECT * FROM mst_dictionary WHERE status != '00' ORDER BY title ASC", nativeQuery = true)
	List<MstDictionary> findAllByAbjad();
	
	@Query(value = "SELECT * FROM mst_dictionary WHERE status != '00' and LOWER(title) like ?1 ORDER BY title ASC", nativeQuery = true)
	List<MstDictionary> findAllBySearchAbjad(String title);
}
