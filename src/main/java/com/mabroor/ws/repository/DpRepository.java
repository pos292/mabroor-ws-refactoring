package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstDp;

@Repository
public interface DpRepository extends PagingAndSortingRepository<MstDp, Long> {

	@Query(value = "SELECT * FROM mst_dp", nativeQuery = true)
    List<MstDp> getListDp();
	
	@Query(value = "SELECT * FROM mst_dp WHERE type ='CAR' AND status = '01'", nativeQuery = true)
    List<MstDp> getListCarDp();
	
	@Query(value = "SELECT * FROM mst_dp WHERE type ='MOTOR' AND status = '01'", nativeQuery = true)
    List<MstDp> getListMotorDp();

}
