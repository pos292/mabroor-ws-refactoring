package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorModel;

@Repository
public interface MotorModelRepository extends PagingAndSortingRepository<MstMotorModel, Long> {
	@Query(value = "SELECT * FROM mst_motor_model WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorModel> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_motor_model WHERE status != '00' AND motor_type_id = ?1 ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorModel> findAllByTypeId(Long id);

	@Modifying
	@Query(value = "UPDATE mst_motor_model SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateMotorModelById(String status, Long id);

	@Query(value = "SELECT * FROM mst_motor_model WHERE status = '01' ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorModel> getListMotorModel();
	
	@Modifying
	@Query(value = "UPDATE mst_motor_model SET seq = newSeq FROM mst_motor_model a "
			+ "INNER JOIN (SELECT id, row_number() OVER (ORDER BY seq ASC, updated_date DESC) AS newSeq FROM mst_motor_model) b ON b.id = a.id", nativeQuery = true)
	int updateMotorModelSeq();
}
