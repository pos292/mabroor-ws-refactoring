package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstDoaCategory;

public interface DoaCategoryRepository extends PagingAndSortingRepository<MstDoaCategory, Long>{

	@Query(value = "SELECT * FROM mst_doa_category WHERE status = '01' order by created_date asc", nativeQuery = true)
	 List<MstDoaCategory> getList();
}
