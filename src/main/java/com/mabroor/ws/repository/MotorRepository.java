package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotor;

@Repository
public interface MotorRepository extends JpaRepository<MstMotor, Long> {
	@Query(value = "SELECT * FROM mst_motor WHERE status != '00' AND motor_type_id = ?1 ORDER BY created_date DESC", nativeQuery = true)
	List<MstMotor> findByMotorType(Long id);

	@Modifying
	@Query(value = "UPDATE mst_motor SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateMotorById(String status, Long id);

	@Query(value = "SELECT * FROM mst_motor WHERE status = '01' AND motor_type_id = ?1", nativeQuery = true)
	List<MstMotor> findByMotorTypeId(Long id);
	
	
}
