package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstContentDictionarySetting;

@Repository
public interface ContentDictionarySettingRepository extends PagingAndSortingRepository<MstContentDictionarySetting, Long>{

	@Query(value = "SELECT * FROM mst_dictionary_setting WHERE id_dictionary = ?1 ORDER BY created_date DESC", nativeQuery = true)
	List<MstContentDictionarySetting> findByDictionaryId(Long id);
	
	@Query(value = "SELECT * FROM mst_dictionary_setting ORDER BY updated_date DESC", nativeQuery = true)
	List<MstContentDictionarySetting> getList();
}
