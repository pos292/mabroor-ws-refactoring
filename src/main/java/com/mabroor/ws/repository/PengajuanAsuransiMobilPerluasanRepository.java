package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPengajuanAsuransiMobilPerluasan;

public interface PengajuanAsuransiMobilPerluasanRepository extends PagingAndSortingRepository<MstPengajuanAsuransiMobilPerluasan, Long> {

	@Query(value = "SELECT * FROM mst_pengajuan_asuransi_mobil_perluasan", nativeQuery = true)
	List<MstPengajuanAsuransiMobilPerluasan> getListPerluasanMobil();
	
	@Query(value = "SELECT * FROM mst_pengajuan_asuransi_mobil_perluasan  WHERE mst_pengajuan_asuransi_mobil_id =?1 ORDER BY seq ASC", nativeQuery = true)
	List<MstPengajuanAsuransiMobilPerluasan> findByPerluasanId(Long asuransiMobilId);
}
