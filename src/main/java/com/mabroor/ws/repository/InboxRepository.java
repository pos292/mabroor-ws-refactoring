package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstInbox;

@Repository
@Transactional
public interface InboxRepository  extends PagingAndSortingRepository<MstInbox, Long> {

//	@Query(value = "SELECT * FROM mst_inbox WHERE inbox_read = 'YES' ", nativeQuery = true)
//    List<MstInbox> getInboxListRead();
//	
	@Query(value = "select * from (\n" + 
			"	select * from (\n" + 
			"		select * from mst_inbox where user_id = ?1 and id_inbox_category in ('1','2')\n" + 
			"		union\n" + 
			"		select * from mst_inbox where user_id = ?1 and id_inbox_category in ('1','2') and TO_CHAR(created_date, 'YYYY-MM-DD hh:mm:ss') not in (\n" + 
			"			select TO_CHAR(created_date, 'YYYY-MM-DD hh:mm:ss') from mst_inbox where user_id = ?1 and id_inbox_category in ('1','2')\n" + 
			"		)\n" + 
			"	) a \n" + 
			"	union \n" + 
			"	select * from mst_inbox where user_id = ?1 and id_inbox_category not in ('1','2')\n" + 
			") a\n" + 
			"where a.created_date > (now() - INTERVAL '30 days')\n" + " or (a.id_data is null or (a.id_data in (select id from mst_promotion where end_date>now()) and a.id_inbox_category = '2')) " + 
			"order by a.created_date desc", nativeQuery = true)
    List<MstInbox> getInboxAll(Long userId);
	
	@Query(value = "select * from (\n" + 
			"	select * from (\n" + 
			"		select * from mst_inbox where user_id = ?2 and id_inbox_category in ('1','2')\n" + 
			"		union\n" + 
			"		select * from mst_inbox where user_id = ?2 and id_inbox_category in ('1','2') and TO_CHAR(created_date, 'YYYY-MM-DD hh:mm:ss') not in (\n" + 
			"			select TO_CHAR(created_date, 'YYYY-MM-DD hh:mm:ss') from mst_inbox where user_id = ?2 and id_inbox_category in ('1','2')\n" + 
			"		)\n" + 
			"	) a \n" + 
			"	union \n" + 
			"	select * from mst_inbox where user_id = ?2 and id_inbox_category not in ('1','2')\n" + 
			") a\n" + 
			"where a.created_date > (now() - INTERVAL '30 days') and a.id_inbox_category = ?1\n" + " or (a.id_data is null or (a.id_data in (select id from mst_promotion where end_date>now()) and a.id_inbox_category = ?1)) " + 
			"order by a.created_date desc", nativeQuery = true)
    List<MstInbox> getInboxId(Long idInboxCategory, Long userId);
	
	@Query(value = "SELECT * FROM mst_inbox WHERE user_id is null and created_date > (now() - INTERVAL '30 days') and (id_data is null or id_data in (select id from mst_promotion where end_date>now())) order by created_date desc", nativeQuery = true)
    List<MstInbox> getInboxAllBeforeLogin();
	
	@Query(value = "SELECT * FROM mst_inbox WHERE id_inbox_category = ?1 and user_id is null and created_date > (now() - INTERVAL '30 days') and (id_data is null or id_data in (select id from mst_promotion where end_date>now())) order by created_date desc", nativeQuery = true)
    List<MstInbox> getInboxIdBeforeLogin(Long idInboxCategory);
	
	@Modifying
	@Query(value = "UPDATE mst_inbox SET inbox_read = 'YES' WHERE id = ?1", nativeQuery = true)
	int updateInboxRead(Long id);

	@Query(value = "SELECT * FROM mst_inbox WHERE user_id = ?1 and inbox_read = 'NO' limit 1", nativeQuery = true)
	List<MstInbox> getInboxNotif(Long userId);

	@Query(value = "SELECT * FROM mst_inbox WHERE user_id is null and inbox_read = 'NO' limit 1", nativeQuery = true)
	List<MstInbox> getInboxNotifBeforeLogin();

	@Query(value = "select * from (\n" + 
			"	select * from (\n" + 
			"		select * from mst_inbox where user_id = ?2 and id_data = ?1 and id_inbox_category in ('1','2')\n" + 
			"		union\n" + 
			"		select * from mst_inbox where user_id = ?2 and id_data = ?1 and id_inbox_category in ('1','2') and TO_CHAR(created_date, 'YYYY-MM-DD hh:mm:ss') not in (\n" + 
			"			select TO_CHAR(created_date, 'YYYY-MM-DD hh:mm:ss') from mst_inbox where user_id = ?2 and id_data = ?1 and id_inbox_category in ('1','2')\n" + 
			"		)\n" + 
			"	) a \n" + 
			"	union \n" + 
			"	select * from mst_inbox where user_id = ?2 and id_data = ?1 and id_inbox_category not in ('1','2')\n" + 
			") a\n" + 
			"where a.created_date > (now() - INTERVAL '30 days')\n" + " or (a.id_data is null or (a.id_data in (select id from mst_promotion where end_date>now()) and a.id_inbox_category = '2')) " + 
			"order by a.created_date desc limit 1", nativeQuery = true)
    Optional<MstInbox> findIdByIdDataAndUserId(Long idData, Long userId);
}
