package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstPengajuanUmrohAlamat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PembiayaanUmrohAlamatRepository extends JpaRepository<MstPengajuanUmrohAlamat, Long> {
}
