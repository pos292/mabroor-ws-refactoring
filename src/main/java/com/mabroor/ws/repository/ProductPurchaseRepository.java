package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstProductPurchase;

@Repository
public interface ProductPurchaseRepository extends JpaRepository<MstProductPurchase, Long>{

	@Query(value = "SELECT * FROM mst_product_purchase WHERE status = '01'", nativeQuery = true)
	List<MstProductPurchase> getList();
	
}
