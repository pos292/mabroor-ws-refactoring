package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import com.mabroor.ws.entity.MstMabroor101;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
@Transactional
@Repository
public interface Mabroor101Repository extends PagingAndSortingRepository<MstMabroor101, Long> {
	@Query(value = "SELECT * FROM mst_mabroor_101 WHERE status = '01' AND NOT id = 1", nativeQuery = true)
	List<MstMabroor101> findAllList();
}
