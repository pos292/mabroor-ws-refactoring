package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPromotionSetting;

public interface PromotionSettingRepository extends PagingAndSortingRepository<MstPromotionSetting, Long> {

	@Query(value = "SELECT * FROM mst_promotion_setting mps INNER JOIN mst_promotion mpt on mps.id_promo = mpt.id where NOW()>=mpt.start_date AND NOW()<=mpt.end_date AND mpt.is_active_homepage = 'YES' and mpt.status = '01' ORDER BY mps.id ASC", nativeQuery = true)
    List<MstPromotionSetting> getListPromotionSetting();
}
