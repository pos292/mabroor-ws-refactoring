package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorColor;

@Repository
public interface MotorColorRepository extends JpaRepository<MstMotorColor, Long> {
	@Query(value = "SELECT * FROM mst_motor_color WHERE status != '00' AND motor_unit_id = ?1 ORDER BY created_date DESC", nativeQuery = true)
	List<MstMotorColor> findByMotorUnit(Long id);

	@Modifying
	@Query(value = "UPDATE mst_motor_color SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateMotorColorById(String status, Long id);

	@Query(value = "SELECT * FROM mst_motor_color WHERE status = '01' AND motor_unit_id = ?1", nativeQuery = true)
	List<MstMotorColor> findByMotorUnitId(Long id);
}
