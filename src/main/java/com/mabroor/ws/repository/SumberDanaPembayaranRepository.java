package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstSumberDanaPembayaran;

@Transactional
public interface SumberDanaPembayaranRepository extends PagingAndSortingRepository<MstSumberDanaPembayaran, Long> {

	@Query(value = "SELECT * FROM mst_sumber_dana_pembayaran WHERE status != '00'", nativeQuery = true)
	List<MstSumberDanaPembayaran> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_sumber_dana_pembayaran ORDER BY sumber_dana_pembayaran ASC", nativeQuery = true)
    List<MstSumberDanaPembayaran> getListDanaPembayaran();

}
