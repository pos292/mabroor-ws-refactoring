package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstNeedsDenganJaminan;

@Repository
public interface NeedsDenganJaminanRepository extends PagingAndSortingRepository<MstNeedsDenganJaminan, Long> {

	@Query(value = "SELECT * FROM mst_needs_denganjaminan WHERE status != '00'", nativeQuery = true)
	List<MstNeedsDenganJaminan> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_needs_denganjaminan WHERE status = '01' ORDER BY seq ASC", nativeQuery = true)
    List<MstNeedsDenganJaminan> getListNeedsDenganJaminan();
	
	@Query(value = "SELECT * FROM mst_needs_denganjaminan WHERE status = '01' and needs_title =?1 ORDER BY needs_title ASC", nativeQuery = true)
    MstNeedsDenganJaminan getMstNeedsJaminanTitleDenganJaminan(String title);
}
