package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstImageRetry;

@Transactional

public interface ImageRetryRepository extends PagingAndSortingRepository<MstImageRetry, Long>{
	
	@Query(value = "SELECT * FROM mst_image_retry WHERE upload_status = 'PENDING'", nativeQuery = true)
    List<MstImageRetry> getListImg();
	
	@Modifying
	@Query(value = "UPDATE mst_image_retry SET upload_status =?1 WHERE id=?2 ", nativeQuery = true)
    void updateListImg(String Status, Long id);
	
	

}
