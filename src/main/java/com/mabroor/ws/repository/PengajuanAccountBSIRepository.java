package com.mabroor.ws.repository;

import java.util.List;

import com.mabroor.ws.entity.MstPengajuanAccountBSI;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PengajuanAccountBSIRepository extends PagingAndSortingRepository<MstPengajuanAccountBSI, Long> {
    @Query(value = "SELECT * FROM mst_pengajuan_account_bsi WHERE user_id =?1 and cast(created_date as date) = cast(now() as date) ORDER BY created_date DESC limit 1", nativeQuery = true)
	MstPengajuanAccountBSI findByUserAndCreateDateNow(Long userId);

    @Query(value = "SELECT * FROM mst_pengajuan_account_bsi WHERE user_id =?1", nativeQuery = true)
	List<MstPengajuanAccountBSI> findByUserId(Long userId);
}
