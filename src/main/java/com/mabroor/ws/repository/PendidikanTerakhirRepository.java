package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPendidikanTerakhir;

@Transactional
public interface PendidikanTerakhirRepository extends PagingAndSortingRepository<MstPendidikanTerakhir, Long>{
	@Query(value = "SELECT * FROM mst_pendidikan_terakhir WHERE status != '00'", nativeQuery = true)
	List<MstPendidikanTerakhir> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_pendidikan_terakhir where status = '01' order by seq asc", nativeQuery = true)
    List<MstPendidikanTerakhir> getListPendidikanTerakhir();
}
