package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstGeneralInsuranceCarProduct;

public interface InsuranceCarProductRepository extends PagingAndSortingRepository<MstGeneralInsuranceCarProduct, Long> {

	@Query(value = "SELECT * FROM mst_general_insurance_car_product ORDER BY created_date ASC", nativeQuery = true)
    List<MstGeneralInsuranceCarProduct> getList();
	
	@Query(value = "SELECT * FROM mst_general_insurance_car_product WHERE general_insurance_car_id = ?1 and status = '01' order by created_date asc", nativeQuery = true)
    List<MstGeneralInsuranceCarProduct> getListCarId(Long generalId);

}
