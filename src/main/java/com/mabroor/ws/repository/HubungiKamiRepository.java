package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstHubungiKami;

public interface HubungiKamiRepository extends PagingAndSortingRepository<MstHubungiKami, Long> {

	@Query(value = "SELECT * FROM mst_hubungi_kami", nativeQuery = true)
    List<MstHubungiKami> getListHubungiKami();
	
    @Query(value = "SELECT * FROM mst_hubungi_kami WHERE encryption_status ISNULL limit ?1", nativeQuery = true)
	List<MstHubungiKami> findAllByNonEncryptedHubungiKami(Long limit);
}
