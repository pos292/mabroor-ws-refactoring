package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstPengajuanUmroh;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PembiayaanUmrohRepository extends JpaRepository<MstPengajuanUmroh, Long> {
}
