package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMobileApplicationVersion;

@Repository
@Transactional
public interface MobileApplicationVersionRepository extends PagingAndSortingRepository<MstMobileApplicationVersion, Long> {

	@Query(value = "SELECT * FROM mst_mobile_application_version ORDER BY created_date DESC LIMIT 1", nativeQuery = true)
	List<MstMobileApplicationVersion> getList();

}
