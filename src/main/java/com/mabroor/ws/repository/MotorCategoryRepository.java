package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorCategory;

@Repository
public interface MotorCategoryRepository extends PagingAndSortingRepository<MstMotorCategory, Long>{
	@Query(value = "SELECT * FROM mst_motor_category WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorCategory> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_motor_category WHERE status != '00' AND motor_brand_id = ?1 ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorCategory> findAllByBrandId(Long id);

	@Modifying
	@Query(value = "UPDATE mst_motor_category SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateMotorCategoryById(String status, Long id);

	@Query(value = "SELECT * FROM mst_motor_category WHERE status = '01' and id in (\n" + 
			"	select motor_category_id from mst_motor_unit where status = '01'\n" + 
			") ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorCategory> getListMotorCategory();
	
	@Modifying
	@Query(value = "UPDATE mst_motor_category SET seq = newSeq FROM mst_motor_category a "
			+ "INNER JOIN (SELECT id, row_number() OVER (ORDER BY seq ASC, updated_date DESC) AS newSeq FROM mst_motor_category) b ON b.id = a.id", nativeQuery = true)
	int updateMotorCategorySeq();
}
