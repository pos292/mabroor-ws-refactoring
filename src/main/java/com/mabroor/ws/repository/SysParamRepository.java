package com.mabroor.ws.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.SysParam;

@Repository
public interface SysParamRepository extends CrudRepository<SysParam, String> {
	
	@Query(value = "SELECT * FROM sys_param where status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<SysParam> findAll();
	
	SysParam findByValueType(String string);
	
	@Query(value = "SELECT * FROM sys_param WHERE status = '01' AND value_type = ?1 ORDER BY seq ASC", nativeQuery = true)
	List<SysParam> findContactUs(String value_type);
}
