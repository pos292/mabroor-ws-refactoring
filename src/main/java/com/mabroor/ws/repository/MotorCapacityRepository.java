package com.mabroor.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorCapacity;

@Repository
public interface MotorCapacityRepository extends JpaRepository<MstMotorCapacity, Long>{
	@Query(value = "SELECT * FROM mst_motor_capacity WHERE motor_unit_id = ?1", nativeQuery = true)
	MstMotorCapacity findByMotorUnitId(Long id);
}
