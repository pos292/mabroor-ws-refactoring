package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPembiayaanQurban;

public interface PembiayaanQurbanRepository extends PagingAndSortingRepository<MstPembiayaanQurban, Long> {
	
	 @Query(value = "SELECT * FROM mst_pembiayaan_qurban WHERE status = '01' ORDER BY id ASC", nativeQuery = true)
	 List<MstPembiayaanQurban> getList();
	 
	 @Query(value = "SELECT * FROM mst_pembiayaan_qurban WHERE status = '01' and nama_partner = ?1 ORDER BY id ASC", nativeQuery = true)
	 List<MstPembiayaanQurban> getListByNamePartner(String namePartner);

}
