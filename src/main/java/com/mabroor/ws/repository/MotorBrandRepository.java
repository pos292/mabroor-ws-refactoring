package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorBrand;

@Repository
public interface MotorBrandRepository extends PagingAndSortingRepository<MstMotorBrand, Long> {
	@Query(value = "SELECT * FROM mst_motor_brand WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorBrand> findAllByStatus();

	@Modifying
	@Query(value = "UPDATE mst_motor_brand SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateMotorBrandById(String status, Long id);

	@Query(value = "SELECT * FROM mst_motor_brand WHERE status = '01' and id in (\n" + 
			"	select motor_brand_id from mst_motor_unit where status = '01'\n" + 
			") ORDER BY seq ASC", nativeQuery = true)
	List<MstMotorBrand> getListMotorBrand();
	
	@Modifying
	@Query(value = "UPDATE mst_motor_brand SET seq = newSeq FROM mst_motor_brand a "
			+ "INNER JOIN (SELECT id, row_number() OVER (ORDER BY seq ASC, updated_date DESC) AS newSeq FROM mst_motor_brand) b ON b.id = a.id", nativeQuery = true)
	int updateMotorBrandSeq();
}
