package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstUserMobileMenu;

@Repository
@Transactional
public interface UserMobileMenuRepository extends PagingAndSortingRepository<MstUserMobileMenu, Long>{

	@Query(value = "SELECT * FROM mst_usermobile_menu where id_menu_category = ?1 and status = '01'", nativeQuery = true)
	List<MstUserMobileMenu> getListMenu(Long idMenuCategory);
	
	@Query(value = "select a.* from mst_usermobile_menu a \r\n" + 
			"join lateral (\r\n" + 
			"	select * from (\r\n" + 
			"		select 'Mobil Bekas' as leads,count(*) as total, 2 as id_menu from mst_pengajuan_car where brand = 'Mobil Bekas'\r\n" + 
			"		union\r\n" + 
			"		select 'Mobil Baru' as leads,count(*) as total, 1 as id_menu from mst_pengajuan_car where brand != 'Mobil Bekas'\r\n" + 
			"		union\r\n" + 
			"		select 'Motor Bekas' as leads,count(*) as total, 4 as id_menu from mst_pengajuan_motor where brand = 'Motor Bekas'\r\n" + 
			"		union\r\n" + 
			"		select 'Motor Baru' as leads,count(*) as total, 3 as id_menu from mst_pengajuan_motor where brand != 'Motor Bekas'\r\n" + 
			"		union\r\n" + 
			"		select 'Jaminan' as leads,count(*) as total, 15 as id_menu from mst_pengajuan_jaminan\r\n" + 
			"		union\r\n" + 
			"		select 'Umroh' as leads,count(*) as total, 18 as id_menu from mst_wisata_religi where purpose_id = '10'\r\n" + 
			"		union\r\n" + 
			"		select 'Haji' as leads,count(*) as total, 17 as id_menu from mst_wisata_religi where purpose_id = '13'\r\n" + 
			"	) a\r\n" + 
			") b on a.id = b.id_menu\r\n" + 
			"order by b.total desc\r\n" + 
			"limit 4", nativeQuery = true)
	List<MstUserMobileMenu> getFeaturedMenu();
	
	@Query(value="SELECT a.* FROM mst_usermobile_menu a\r\n" + 
			"JOIN mst_usermobile_default_menu b\r\n" + 
			"on a.id = b.id_menu WHERE a.status = '01' ORDER BY a.seq ASC",nativeQuery = true)
	List<MstUserMobileMenu> getDefaultMenu();
}
