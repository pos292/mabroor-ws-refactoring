package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstActivityUser;

public interface ActivityUserRepository  extends PagingAndSortingRepository<MstActivityUser, Long> {

	@Query(value = "SELECT * FROM mst_activity_user", nativeQuery = true)
	List<MstActivityUser> getListActivityUser();
	
	@Query(value = "SELECT * FROM mst_activity_user WHERE user_id = ?1", nativeQuery = true)
	List<MstActivityUser> findAllByUserId(Long id);
}
