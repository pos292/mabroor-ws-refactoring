package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstMultigunaKecamatan;
import com.mabroor.ws.entity.MstMultigunaKelurahan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MultigunaKelurahanRepository extends JpaRepository<MstMultigunaKelurahan, Long> {
    @Query(value = "select * from mst_multiguna_kelurahan where id_kecamatan =?1", nativeQuery = true)
    List<MstMultigunaKelurahan> findByIdKecamatan(Long id_kecamatan);

    @Query(value = "select * from mst_multiguna_kelurahan where id =?1", nativeQuery = true)
    MstMultigunaKelurahan findByIdKelurahan(Long id);
}
