package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstUserMobileDefaultMenu;

public interface UserMobileDefaultMenuRepository extends PagingAndSortingRepository<MstUserMobileDefaultMenu, Long> {

	@Query(value = "SELECT * FROM mst_usermobile_default_menu WHERE status = '01'", nativeQuery = true)
	 List<MstUserMobileDefaultMenu> getList();
	
}
