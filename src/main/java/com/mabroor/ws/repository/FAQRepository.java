package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstFAQ;

@Repository
@Transactional
public interface FAQRepository extends PagingAndSortingRepository<MstFAQ, Long>{

	@Query(value = "SELECT * FROM mst_faq WHERE status != '00'", nativeQuery = true)
	List<MstFAQ> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_faq where status = '01' and main_product_id = ?1 ORDER BY created_date DESC limit 3", nativeQuery = true)
	List<MstFAQ> findByMainProductLandingPage(Long idMainProduct);
	
	@Query(value = "SELECT * FROM mst_faq where status = '01' and main_product_id = ?1 ORDER BY created_date DESC", nativeQuery = true)
	List<MstFAQ> findByMainProduct(Long idMainProduct);
}
