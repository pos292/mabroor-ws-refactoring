package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstUserBussinessUnit;

public interface UserBussinessUnitRepository extends PagingAndSortingRepository<MstUserBussinessUnit, Long> {
	
	@Query(value = "SELECT * FROM mst_user_bussiness_unit", nativeQuery = true)
    List<MstUserBussinessUnit> getList();
}
