package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstAlQuran;

public interface AlQuranRepository extends PagingAndSortingRepository<MstAlQuran, Long> {
	
	@Query(value = "SELECT * FROM mst_al_quran WHERE LOWER(name_sura) like lower(?1) or CAST(juz AS text) like ?1 ORDER BY id ASC", nativeQuery = true)
	List<MstAlQuran> findAll(String title);
	
	@Query(value = "SELECT DISTINCT ON (a.juz) * FROM (SELECT * FROM mst_al_quran ORDER BY id ASC) a", nativeQuery = true)
	List<MstAlQuran> listJuz();
	
	@Query(value = "SELECT * FROM mst_al_quran WHERE LOWER(phonetic_ind) = LOWER(?1) ORDER BY id ASC", nativeQuery = true)
	List<MstAlQuran> filterSurah(String title);
	
	@Query(value = "SELECT * FROM mst_al_quran WHERE juz = ?1 ORDER BY id ASC", nativeQuery = true)
	List<MstAlQuran> filterJuz(Long juz);

	@Query(value = "SELECT DISTINCT ON (a.id_sura) * FROM (SELECT * FROM mst_al_quran WHERE LOWER(ind_sura) like lower(?1) or LOWER(phonetic_ind) like lower(?1) ORDER BY id asc) a", nativeQuery = true)
	List<MstAlQuran> searchSura(String title);
	
	@Query(value = "SELECT DISTINCT ON (a.juz) * FROM (SELECT * FROM mst_al_quran WHERE CAST(juz AS text) like ?1 ORDER BY id asc ) a", nativeQuery = true)
	List<MstAlQuran> searchJuz(String title);
}
