package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstPekerjaan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PekerjaanRepository extends JpaRepository<MstPekerjaan, Long> {
}
