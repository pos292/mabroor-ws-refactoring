package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorVideo;

@Repository
@Transactional
public interface MotorVideoRepository extends JpaRepository<MstMotorVideo, Long> {
	@Query(value = "SELECT * FROM mst_motor_video WHERE status != '00' ORDER BY created_date DESC", nativeQuery = true)
	List<MstMotorVideo> findByAllByStatus();

	@Modifying
	@Query(value = "UPDATE mst_motor_video SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateMotorVideoById(String status, Long id);

	@Query(value = "SELECT * FROM mst_motor_video WHERE status = '01' AND motor_unit_id = ?1", nativeQuery = true)
	List<MstMotorVideo> findByMotorUnitId(Long id);
}
