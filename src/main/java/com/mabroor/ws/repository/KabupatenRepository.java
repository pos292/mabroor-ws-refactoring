package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstKabupaten;

@Repository
public interface KabupatenRepository extends PagingAndSortingRepository<MstKabupaten, Long> {

	@Query(value = "SELECT * FROM mst_kabupaten WHERE status != '00'", nativeQuery = true)
	List<MstKabupaten> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_kabupaten WHERE status = '01'", nativeQuery = true)
    List<MstKabupaten> getListKabupaten();
	
	@Query(value = "SELECT * FROM mst_kabupaten WHERE status = '01' AND provinsi_id = ?1 order by nama_kabupaten asc", nativeQuery = true)
    List<MstKabupaten> getListKabupatenByProvinsi(Long id);
	
	@Query(value = "SELECT * FROM mst_kabupaten WHERE status = '01' and nama_kabupaten = ?1", nativeQuery = true)
    List<MstKabupaten> findByNamaKabupaten(String namaKabupaten);
	
}
