package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstBeriMasukan;

public interface BeriMasukanRepository extends PagingAndSortingRepository<MstBeriMasukan, Long> {
	
	@Query(value = "SELECT * FROM mst_beri_masukan", nativeQuery = true)
    List<MstBeriMasukan> getListBeriMasukan();

}
