package com.mabroor.ws.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstOtp;

@Transactional
@Repository
public interface OtpRepository extends PagingAndSortingRepository<MstOtp, Long> {

	@Query(value = "SELECT * FROM mst_otp WHERE is_verified != 'true' AND phone_number = ?1 ORDER BY created_date DESC limit 1", nativeQuery = true)
	MstOtp findByPhoneNumber(String phoneNumber);
	
	@Query(value = "SELECT * FROM mst_otp WHERE is_verified != 'true' ORDER BY created_date DESC", nativeQuery = true)
	List<MstOtp> findOtpByNotVerify();
	
	@Query(value = "SELECT * FROM mst_otp WHERE phone_number = ?1 AND is_verified = FALSE AND created_date+?3\\:\\:interval> ?2", nativeQuery = true)
	List<MstOtp> findByExpiredDate(String value1, Date value2, String value3);
	
	@Modifying
	@Query(value = "UPDATE mst_otp SET is_verified = TRUE WHERE phone_number = ?1", nativeQuery = true)
	void updateAllVerificationByPhoneNumber(String phone);
	
    @Query(value = "SELECT * FROM mst_otp WHERE encryption_status ISNULL limit ?1", nativeQuery = true)
	List<MstOtp> findAllByNonEncryptedOtp(Long limit);
	
	@Modifying
    @Query(value = "delete from mst_otp WHERE phone_number = ?1 AND is_verified = false AND CAST(created_date as date) = CAST(NOW() as date)", nativeQuery = true)
	void deleteByPhoneNumber(String phoneNumber);
}
