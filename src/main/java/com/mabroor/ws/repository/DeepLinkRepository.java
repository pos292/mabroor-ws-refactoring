package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstDeepLink;

public interface DeepLinkRepository extends PagingAndSortingRepository<MstDeepLink, Long> {

	@Query(value = "SELECT * FROM mst_deep_link WHERE status != '00'", nativeQuery = true)
	List<MstDeepLink> getList();

	@Query(value = "SELECT * FROM mst_deep_link WHERE status != '00' and description = 'OPEN_ACCOUNT_BSI'", nativeQuery = true)
	List<MstDeepLink> getListLinkBsi();
}
