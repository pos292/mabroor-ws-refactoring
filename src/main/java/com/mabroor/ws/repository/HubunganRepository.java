package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstHubungan;

@Transactional
public interface HubunganRepository extends PagingAndSortingRepository<MstHubungan, Long> {
	
	@Query(value = "SELECT * FROM mst_hubungan WHERE status != '00'", nativeQuery = true)
	List<MstHubungan> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_hubungan ORDER BY hubungan ASC", nativeQuery = true)
    List<MstHubungan> getListHubungan();
}
