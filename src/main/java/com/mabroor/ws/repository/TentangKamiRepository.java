package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstTentangKami;

public interface TentangKamiRepository extends PagingAndSortingRepository<MstTentangKami, Long> {
	
	@Query(value = "SELECT * FROM mst_tentang_kami", nativeQuery = true)
    List<MstTentangKami> getListTentangKami();
}
