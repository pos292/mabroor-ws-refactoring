package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstPengajuanMultigunaTanpaJaminan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PengajuanMultigunaTanpaJaminanRepository extends JpaRepository<MstPengajuanMultigunaTanpaJaminan, Long> {
}
