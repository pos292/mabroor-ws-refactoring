package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPromotionBestDealPackageSimulation;

public interface PromotionBestDealPackageSimulationRepository extends PagingAndSortingRepository<MstPromotionBestDealPackageSimulation, Long> {
	
	@Query(value = "SELECT * FROM mst_promotion_bestdeal_package_simulation WHERE promo_id =?1 ", nativeQuery = true)
	MstPromotionBestDealPackageSimulation findAllByPromotion(Long id);

}
