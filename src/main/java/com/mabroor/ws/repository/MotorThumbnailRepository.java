package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorColor;
import com.mabroor.ws.entity.MstMotorThumbnail;

@Repository
public interface MotorThumbnailRepository extends JpaRepository<MstMotorThumbnail, Long> {
	List<MstMotorThumbnail> findByMotorColor(MstMotorColor mstMotorColor);

	@Modifying
	@Query("DELETE FROM MstMotorThumbnail a WHERE a.id=?1")
	void deleteThumbnailById(Long id);

	@Query(value = "SELECT * FROM mst_motor_thumbnail WHERE motor_color_id = ?1", nativeQuery = true)
	List<MstMotorThumbnail> findByMotorColorId(Long id);

	@Modifying
	@Query(value = "UPDATE mst_motor_thumbnail SET best_deal = 'NO' WHERE motor_color_id = ?1", nativeQuery = true)
	void updateAlltoNo(Long colorId);

	@Modifying
	@Query(value = "UPDATE mst_motor_thumbnail SET best_deal = 'YES' WHERE id = ?1", nativeQuery = true)
	void updateBestDeal(Long id);
}
