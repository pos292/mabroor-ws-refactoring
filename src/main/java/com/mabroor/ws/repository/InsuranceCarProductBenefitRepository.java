package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstGeneralInsuranceCarProductBenefit;

public interface InsuranceCarProductBenefitRepository extends PagingAndSortingRepository<MstGeneralInsuranceCarProductBenefit, Long> {
	
	@Query(value = "SELECT * FROM mst_general_insurance_car_product_benefit WHERE insurance_car_product_id = ?1 and status = '01' order by created_date asc ", nativeQuery = true)
    List<MstGeneralInsuranceCarProductBenefit> getListInscByInsuranceCarProductId(Long insuranceCarProductId);
	
}
