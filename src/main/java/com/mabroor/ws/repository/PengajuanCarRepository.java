package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabroor.ws.entity.MstPengajuanCar;

public interface PengajuanCarRepository extends PagingAndSortingRepository<MstPengajuanCar, Long> {
	@Query(value = "SELECT * FROM mst_pengajuan_car WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstPengajuanCar> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_pengajuan_car", nativeQuery = true)
    List<MstPengajuanCar> getListPengajuan();
	
	@Query(value = "SELECT * FROM mst_pengajuan_car WHERE brand = 'usedcar'", nativeQuery = true)
    List<MstPengajuanCar> getListPengajuanBekas();
    
    @Query(value = "SELECT * FROM mst_pengajuan_car WHERE status = '01' ORDER BY name ASC", nativeQuery = true)
	List<MstPengajuanCar> findAllByTitleAsc();

    @Query(value = "SELECT * FROM mst_pengajuan_car WHERE status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstPengajuanCar> findAllEnable();
    
    @Query(value = "SELECT * FROM mst_pengajuan_car WHERE distribution_type = 'ODDEVEN' ORDER BY created_date DESC LIMIT 1", nativeQuery = true)
   	List<MstPengajuanCar> findCompanyAvailable();

    @Query(value = "SELECT * FROM mst_pengajuan_car WHERE status = '01' and user_id = ?1", nativeQuery = true)
	List<MstPengajuanCar> findAllByUserId(Long userId);
    
    @Query(value = "select * from (\n" + 
    		"	select count(a.company_name) as count_acc from (\n" + 
    		"		select brand, company_name, created_date, distribution_type from mst_pengajuan_car limit all offset (\n" + 
    		"			select b.rownum from (\n" + 
    		"				select rownum, brand, company_name, created_date from (\n" + 
    		"					select row_number() OVER (order by created_date) as rownum, created_date, company_name, brand, distribution_type from mst_pengajuan_car\n" + 
    		"				) a where company_name = 'TAF' and distribution_type = 'SIXONE' and LOWER(brand) = ?1 order by a.rownum desc limit 1\n" + 
    		"			) b\n" + 
    		"		) \n" + 
    		"	) a where LOWER(a.brand) = ?1 and distribution_type = 'SIXONE' \n" + 
    		"	group by a.company_name \n" + 
    		") a full join lateral (\n" + 
    		"	select b.rownum as row_num from (\n" + 
    		"		select rownum, brand, company_name, created_date from (\n" + 
    		"			select row_number() OVER (order by created_date) as rownum, created_date, company_name, brand, distribution_type from mst_pengajuan_car\n" + 
    		"		) a where company_name = 'ACC' and distribution_type = 'SIXONE' and LOWER(brand) = ?1 order by a.rownum desc limit 1\n" + 
    		"	) b\n" + 
    		") b ON true", nativeQuery = true)
    List<MstPengajuanCar> getCountAccV2(String brand);
    
    @Query(value = "SELECT * FROM mst_pengajuan_car WHERE encryption_status ISNULL limit ?1", nativeQuery = true)
	List<MstPengajuanCar> findAllByNonEncryptedPengajuanCar(Long limit);
}
