package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstUserMobileMenuLayout;

@Repository
@Transactional
public interface UserMobileMenuLayoutRepository extends PagingAndSortingRepository<MstUserMobileMenuLayout, Long>{

	 @Query(value = "SELECT * FROM mst_usermobile_menu_layout WHERE user_id = ?1", nativeQuery = true)
	 List<MstUserMobileMenuLayout> findAllByUserId(Long userId);
	 
	 @Query(value = "SELECT * FROM mst_usermobile_menu_layout WHERE id_menu = ?1", nativeQuery = true)
	 List<MstUserMobileMenuLayout> findByIdMenu(Long idMenu);
	 
	 @Modifying
	 @Query(value = "DELETE from mst_usermobile_menu_layout where user_id = ?1", nativeQuery = true)
	 int resetUserMobileMenu(Long userId);
}
