package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstVideoReview;

@Repository
@Transactional
public interface VideoReviewRepository extends PagingAndSortingRepository<MstVideoReview, Long>{
	
	@Query(value = "SELECT * FROM mst_video_review WHERE status != '00' ORDER BY created_date DESC", nativeQuery = true)
	List<MstVideoReview> findAllByStatus();

	@Modifying
	@Query(value = "UPDATE mst_video_review SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateVideoReviewById(String status, Long id);
	
	@Query(value = "SELECT * FROM mst_video_review WHERE status = '01' and main_product_id = ?1 ORDER BY created_date DESC LIMIT 3", nativeQuery = true)
	List<MstVideoReview> findByMainProductId(Long idMainProduct);

	@Query(value = "SELECT * FROM mst_video_review WHERE status = '01' and main_product_id = ?1 ORDER BY created_date DESC", nativeQuery = true)
	List<MstVideoReview> findByMainProductIdList(Long idMainProduct);
	
	@Query(value = "SELECT * FROM mst_video_review WHERE status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstVideoReview> findAllEnable();

	@Query(value = "SELECT * FROM mst_video_review WHERE status = '01' ORDER BY created_date DESC LIMIT 3", nativeQuery = true)
	List<MstVideoReview> findAllEnableLimit();

	@Query(value = "SELECT * FROM mst_video_review WHERE status = '01' and sub_category = ?1 ORDER BY created_date DESC", nativeQuery = true)
	List<MstVideoReview> findByCategory(String category);

	@Query(value = "SELECT * FROM mst_video_review WHERE status = '01' and sub_category = ?1 ORDER BY created_date DESC LIMIT 3", nativeQuery = true)
	List<MstVideoReview> findByCategoryLimit(String category);
	
	@Query(value = "SELECT DISTINCT ON (sub_category) * from mst_video_review WHERE status = '01' ORDER BY sub_category ASC", nativeQuery = true)
	List<MstVideoReview> findActiveSubCategory();

	@Query(value = "SELECT * FROM mst_video_review WHERE status = '01' AND main_product_id = ?1 AND sub_category = ?2 ORDER BY created_date DESC", nativeQuery = true)
	List<MstVideoReview> findByMainProductIdandSubCategoryName(Long idMainProduct, String subcategoryName);
}
