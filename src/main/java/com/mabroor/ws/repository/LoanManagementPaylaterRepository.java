package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstLoanManagement;
import com.mabroor.ws.entity.MstLoanManagementPaylater;

@Repository
@Transactional
public interface LoanManagementPaylaterRepository extends PagingAndSortingRepository<MstLoanManagementPaylater, Long> {

	@Query(value = "SELECT * FROM mst_loan_management_paylater where status = '01'", nativeQuery = true)
    List<MstLoanManagementPaylater> getList();
	
	@Query(value = "SELECT * FROM mst_loan_management_paylater where loan_type = ?1 and status = '01'", nativeQuery = true)
    Optional<MstLoanManagementPaylater> getListLoanType(String loanType);
}
