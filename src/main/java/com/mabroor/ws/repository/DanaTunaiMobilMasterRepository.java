package com.mabroor.ws.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstDanaTunaiMobilMaster;

@Repository
@Transactional
public interface DanaTunaiMobilMasterRepository extends PagingAndSortingRepository<MstDanaTunaiMobilMaster, Long>{
	
	@Query(value = "SELECT * FROM mst_danatunai_mobil_master order by brand asc, type asc, model asc, year asc", nativeQuery = true)
	List<MstDanaTunaiMobilMaster> findAll();
	
	@Modifying
	@Query(value = "truncate mst_danatunai_mobil_master", nativeQuery = true)
	void truncateMaster();
	
	@Query(value = "SELECT DISTINCT on (brand) * FROM mst_danatunai_mobil_master ORDER BY brand asc", nativeQuery = true)
	List<MstDanaTunaiMobilMaster> findByBrandCar();
	
	@Query(value = "SELECT DISTINCT on (type) * FROM mst_danatunai_mobil_master WHERE brand_value =?1 ORDER BY type asc", nativeQuery = true)
	List<MstDanaTunaiMobilMaster> findByTypeCar(String brand);
	
	@Query(value = "SELECT DISTINCT on (model) * FROM mst_danatunai_mobil_master WHERE type_value =?1 ORDER BY model asc", nativeQuery = true)
	List<MstDanaTunaiMobilMaster> findByModelCar(String type);
	
	@Query(value = "SELECT DISTINCT on (year) * FROM mst_danatunai_mobil_master WHERE model_value =?1 ORDER BY year asc", nativeQuery = true)
	List<MstDanaTunaiMobilMaster> findByYearCar(String model);
	
	@Query(value = "SELECT DISTINCT on (year) * FROM mst_danatunai_mobil_master WHERE brand_value =?1 AND type_value =?2 AND model_value =?3 ORDER BY year asc", nativeQuery = true)
	List<MstDanaTunaiMobilMaster> findByYearAndModelTypeCar(String brand, String type, String model);
	
	@Query(value = "SELECT * FROM mst_danatunai_mobil_master WHERE brand_value =?1 AND type_value =?2 AND model_value =?3 AND year =?4 ORDER BY price ASC LIMIT 1", nativeQuery = true)
	Optional<MstDanaTunaiMobilMaster> findByCalculateMrp(String brandValue, String typeValue, String modelValue, String year);
	
	@Query(value = "SELECT * FROM mst_danatunai_mobil_master WHERE brand =?1 AND type =?2 AND model =?3 AND year =?4 ORDER BY price ASC LIMIT 1", nativeQuery = true)
	MstDanaTunaiMobilMaster getJaminanMobilByBtmy(String brand, String type, String model, String year);

	@Query(value = "SELECT DISTINCT on (model) * FROM mst_danatunai_mobil_master WHERE brand_value =?1 AND type_value =?2 ORDER BY model asc", nativeQuery = true)
	List<MstDanaTunaiMobilMaster> findByBrandTypeModelCar(String brand, String type);
	
}

