package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstPengajuanUmrohDataDiri;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PembiayaanUmrohDataDiriRepository extends JpaRepository<MstPengajuanUmrohDataDiri, Long> {
}
