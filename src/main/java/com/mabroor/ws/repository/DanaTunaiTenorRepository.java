package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstDanaTunaiTenor;

@Repository
public interface DanaTunaiTenorRepository extends PagingAndSortingRepository<MstDanaTunaiTenor, Long> {

	
	@Query(value = "SELECT * FROM mst_danatunai_tenor where status ='01' ", nativeQuery = true)
	List<MstDanaTunaiTenor> findAll();
	
	@Modifying
	@Query(value = "truncate mst_danatunai_tenor", nativeQuery = true)
	void truncateMaster();
	
	@Query(value = "SELECT * FROM mst_danatunai_tenor WHERE type =?1 ORDER BY seq asc", nativeQuery = true)
	List<MstDanaTunaiTenor> findByType(String type);
	
	@Query(value = "SELECT * FROM mst_danatunai_tenor  WHERE type =?1 and product =?2 ORDER BY seq asc", nativeQuery = true)
	List<MstDanaTunaiTenor> findByProductType(String type, String product);
}
