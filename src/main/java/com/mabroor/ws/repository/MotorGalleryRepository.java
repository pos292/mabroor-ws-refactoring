package com.mabroor.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstMotorGallery;
import com.mabroor.ws.entity.MstMotorUnit;

@Repository
public interface MotorGalleryRepository extends JpaRepository<MstMotorGallery, Long> {
	List<MstMotorGallery> findByMotorUnit(MstMotorUnit mstMotorUnit);

	@Modifying
	@Query("DELETE FROM MstMotorGallery a WHERE a.id=?1")
	void deleteGalleryById(Long id);

	@Query(value = "SELECT * FROM mst_motor_gallery WHERE motor_unit_id = ?1", nativeQuery = true)
	List<MstMotorGallery> findByMotorUnitId(Long id);
}
