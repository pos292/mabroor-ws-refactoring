package com.mabroor.ws.repository;

import com.mabroor.ws.entity.MstTenor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TenorRepository extends JpaRepository<MstTenor, Long> {
}
