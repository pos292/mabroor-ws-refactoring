package com.mabroor.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.ws.entity.MstPengajuanJaminan;

@Repository
@Transactional
public interface PengajuanJaminanRepository extends PagingAndSortingRepository<MstPengajuanJaminan , Long> {

	@Query(value = "SELECT * FROM mst_pengajuan_jaminan WHERE status != '00' ORDER BY seq ASC", nativeQuery = true)
	List<MstPengajuanJaminan> findAllByStatus();
	
	@Query(value = "SELECT * FROM mst_pengajuan_jaminan", nativeQuery = true)
    List<MstPengajuanJaminan> getListPengajuan();
    
    @Query(value = "SELECT * FROM mst_pengajuan_jaminan WHERE status = '01' ORDER BY name ASC", nativeQuery = true)
	List<MstPengajuanJaminan> findAllByTitleAsc();

    @Query(value = "SELECT * FROM mst_pengajuan_jaminan WHERE status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstPengajuanJaminan> findAllEnable();    
    
    @Query(value = "SELECT * FROM mst_pengajuan_jaminan WHERE status = '01' and user_id = ?1", nativeQuery = true)
	List<MstPengajuanJaminan> findAllByUserId(Long id);
    
    @Query(value = "SELECT * FROM mst_pengajuan_jaminan WHERE phone =?1 and cast(created_date as date) = cast(now() as date) ORDER BY created_date DESC", nativeQuery = true)
   	List<MstPengajuanJaminan> findByPhone(String phone);
    
    @Query(value = "SELECT * FROM mst_pengajuan_jaminan WHERE encryption_status ISNULL limit ?1", nativeQuery = true)
	List<MstPengajuanJaminan> findAllByNonEncryptedJaminan(Long limit);
}
