package com.mabroor.ws.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.mabroor.ws.entity.MstUserMobile;

@Repository
@Transactional
public interface UserMobileRepository extends PagingAndSortingRepository<MstUserMobile, Long>{
	@Query(value = "SELECT * FROM mst_user_mobile WHERE status != '00' ORDER BY created_date DESC", nativeQuery = true)
	List<MstUserMobile> findAllByStatus();

	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET status = ?1 WHERE id = ?2", nativeQuery = true)
	int updateUserMobileById(String status, Long id);
	
//	@Modifying
//	@Query(value = "UPDATE mst_user_mobile SET ktp_image = ?1 and npwp_image = ?2 and kk_image = ?3 and pp_image =?4 WHERE id = ?5" , nativeQuery = true)
//	void updateUserEntity(MultipartFile ktpImage,MultipartFile npwpImage,MultipartFile kkImage,MultipartFile ppImage,Long userId);
	
	@Query(value = "SELECT * FROM mst_user_mobile WHERE status = '01' ORDER BY created_date DESC", nativeQuery = true)
	List<MstUserMobile> findAllEnable();
	
	@Query(value = "SELECT * FROM mst_user_mobile", nativeQuery = true)
    List<MstUserMobile> getListMobile();
	
	@Query(value = "SELECT * FROM mst_user_mobile", nativeQuery = true)
    MstUserMobile getListProfile();
	
	@Query(value = "SELECT * FROM mst_user_mobile WHERE phone = ?1 and id != ?2 and status = '01'", nativeQuery = true)
	List<MstUserMobile> getByPhoneNumber(String phoneNumber, Long id);
	
	@Query(value = "SELECT * FROM mst_user_mobile WHERE phone = ?1 and status = '01'", nativeQuery = true)
	List<MstUserMobile> checkPhoneExist(String phoneNumber);
	
	@Query(value = "SELECT * FROM mst_user_mobile WHERE phone = ?1 and status = '01'", nativeQuery = true)
	List<MstUserMobile> checkPhoneUser(String phone);
	
	@Query(value = "SELECT * FROM mst_user_mobile WHERE phone = ?1 and status = '01'", nativeQuery = true)
	MstUserMobile getOneByPhoneNumber(String phoneNumber);

	@Query(value = "SELECT * FROM mst_user_mobile WHERE phone = ?1 and status = '01'", nativeQuery = true)
	Optional<MstUserMobile> getOneByPhoneNumber2(String phoneNumber);
	
	@Query(value = "SELECT * FROM mst_user_mobile WHERE email = ?1 and id != ?2 and status = '01'", nativeQuery = true)
	List<MstUserMobile> getByEmail(String email, Long id);
	
	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET firebase_token = ?1 WHERE id = ?2", nativeQuery = true)
	int updateFirebaseToken(String token, Long id);
	
	@Query(value = "select * from mst_user_mobile where status = '00' and phone = ?1 order by created_date desc limit 1", nativeQuery = true)
	MstUserMobile findPreviousUser(String phoneNumber);

	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET pin = ?1 WHERE id = ?2", nativeQuery = true)
	void updatePin(String pin, long userId);

	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET count_pin = ?1 WHERE id = ?2", nativeQuery = true)
	void updateCountPin(Long count, long userId);

	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET pin_lock_date_time = ?1 WHERE id = ?2", nativeQuery = true)
	void updateLockTimePin(Date lockTime, Long userId);
	
	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET pin_lock_date_time = NULL WHERE id = ?1", nativeQuery = true)
	void resetLockTImePin(Long userId);

	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET pin_lock_status = ?1 WHERE id = ?2", nativeQuery = true)
	void updateLockStatus(String lockStatus, Long userId);
	
	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET pin_user_type = ?1 WHERE id = ?2", nativeQuery = true)
	void updateUserType(String type, Long userId);

	@Modifying
	@Query(value = "UPDATE mst_user_mobile SET fingerprint_toggle = ?1 WHERE id = ?2", nativeQuery = true)
	void updateFingerprintToggle(String fingerprintToggle, Long userId);
	
    @Query(value = "SELECT * FROM mst_user_mobile WHERE encryption_status ISNULL limit ?1", nativeQuery = true)
	List<MstUserMobile> findAllByNonEncryptedUserMobile(Long limit);
    
    @Query(value = "SELECT mst_user_mobile.* FROM mst_user_mobile INNER JOIN mst_promotion_bundle on mst_user_mobile.id = mst_promotion_bundle.user_id  WHERE mst_promotion_bundle.email IS NULL", nativeQuery = true)
    List<MstUserMobile> findAllUserPromoBundle();
}
