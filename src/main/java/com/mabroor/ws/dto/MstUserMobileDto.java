package com.mabroor.ws.dto;

import java.util.Date;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mabroor.ws.entity.MstKabupaten;
import com.mabroor.ws.entity.MstKecamatan;
import com.mabroor.ws.entity.MstKelurahan;
import com.mabroor.ws.entity.MstLocations;

public class MstUserMobileDto {

    private Long id;
	
	
	private String name;
	
	
	private String firstName;
	
	
	private String lastName;
	
	
	private String email;
	
	
	private String phone;
	
	
	private String maskedPhone;
	
	
	private String ktpNumber;
	
	
	private String npwpNumber;
	
	
	private String gender;
	
	
	private String statusUser;
	
	
	private String address;
	
	
	private String addressKtp;
	
	
	private MstLocations province;
	
	
	private MstKabupaten city;
	
	
	private MstKecamatan district;
	
	
	private MstKelurahan subDistrict;
	
	
	private String birthDate;
	
	private String ktpImage;
	
	private String npwpImage;
	
	private String bpkbMobilImage;
	
	private String bpkbMotorImage;
	
	private String kkImage;
	
	private String ppImage;
	
	
	private String firebaseToken;
	
	
	private String token;
	
	
	private String userMobileUuid;
	
	
	private String goals;
	
	
	private String pin;
	
	
	private String userMobileStatus;
	
	
	private String zipcode;
	
	
	private String zipcodeKtp;
	
	
	private String fingerprintToggle;
	
	private String phoneNumOld;
	
	public String getPhoneNumOld() {
		return phoneNumOld;
	}
	public void setPhoneNumOld(String phoneNumOld) {
		this.phoneNumOld = phoneNumOld;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMaskedPhone() {
		return maskedPhone;
	}
	public void setMaskedPhone(String maskedPhone) {
		this.maskedPhone = maskedPhone;
	}
	public String getKtpNumber() {
		return ktpNumber;
	}
	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}
	public String getNpwpNumber() {
		return npwpNumber;
	}
	public void setNpwpNumber(String npwpNumber) {
		this.npwpNumber = npwpNumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStatusUser() {
		return statusUser;
	}
	public void setStatusUser(String statusUser) {
		this.statusUser = statusUser;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressKtp() {
		return addressKtp;
	}
	public void setAddressKtp(String addressKtp) {
		this.addressKtp = addressKtp;
	}
	public MstLocations getProvince() {
		return province;
	}
	public void setProvince(MstLocations province) {
		this.province = province;
	}
	public MstKabupaten getCity() {
		return city;
	}
	public void setCity(MstKabupaten city) {
		this.city = city;
	}
	public MstKecamatan getDistrict() {
		return district;
	}
	public void setDistrict(MstKecamatan district) {
		this.district = district;
	}
	public MstKelurahan getSubDistrict() {
		return subDistrict;
	}
	public void setSubDistrict(MstKelurahan subDistrict) {
		this.subDistrict = subDistrict;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getKtpImage() {
		return ktpImage;
	}
	public void setKtpImage(String ktpImage) {
		this.ktpImage = ktpImage;
	}
	public String getNpwpImage() {
		return npwpImage;
	}
	public void setNpwpImage(String npwpImage) {
		this.npwpImage = npwpImage;
	}
	public String getBpkbMobilImage() {
		return bpkbMobilImage;
	}
	public void setBpkbMobilImage(String bpkbMobilImage) {
		this.bpkbMobilImage = bpkbMobilImage;
	}
	public String getBpkbMotorImage() {
		return bpkbMotorImage;
	}
	public void setBpkbMotorImage(String bpkbMotorImage) {
		this.bpkbMotorImage = bpkbMotorImage;
	}
	public String getKkImage() {
		return kkImage;
	}
	public void setKkImage(String kkImage) {
		this.kkImage = kkImage;
	}
	public String getPpImage() {
		return ppImage;
	}
	public void setPpImage(String ppImage) {
		this.ppImage = ppImage;
	}
	public String getFirebaseToken() {
		return firebaseToken;
	}
	public void setFirebaseToken(String firebaseToken) {
		this.firebaseToken = firebaseToken;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUserMobileUuid() {
		return userMobileUuid;
	}
	public void setUserMobileUuid(String userMobileUuid) {
		this.userMobileUuid = userMobileUuid;
	}
	public String getGoals() {
		return goals;
	}
	public void setGoals(String goals) {
		this.goals = goals;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getUserMobileStatus() {
		return userMobileStatus;
	}
	public void setUserMobileStatus(String userMobileStatus) {
		this.userMobileStatus = userMobileStatus;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getZipcodeKtp() {
		return zipcodeKtp;
	}
	public void setZipcodeKtp(String zipcodeKtp) {
		this.zipcodeKtp = zipcodeKtp;
	}
	public String getFingerprintToggle() {
		return fingerprintToggle;
	}
	public void setFingerprintToggle(String fingerprintToggle) {
		this.fingerprintToggle = fingerprintToggle;
	}
	
}
