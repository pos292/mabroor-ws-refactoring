package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarCreditSimulationSubmissionNoRegDto {
	
	@JsonProperty("NOREG_LEADS")
	private String NOREG_LEADS;

	public String getNOREG_LEADS() {
		return NOREG_LEADS;
	}

	public void setNOREG_LEADS(String nOREG_LEADS) {
		NOREG_LEADS = nOREG_LEADS;
	}

	
	
	

}
