package com.mabroor.ws.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MstTransactionHealthBuddiesCaseDataJsonDto {
	
	@JsonProperty("agent_code")
	private String agent_code;
	
	@JsonProperty("agent_channel")
	private String agent_channel;
	
	@JsonProperty("channel")
	private String channel;
	
	@JsonProperty("sales_channel")
	private String sales_channel;
	
	
	@JsonProperty("benef_info")
	private List<String> benef_info;
	
	@JsonProperty("caseCreateDate")
	private String caseCreateDate;
	
	@JsonProperty("healthDeclaration")
	private String healthDeclaration;
	
	@JsonProperty("life_profiles")
	private List<HealthBuddiesLifeProfileDto> life_profiles;
	
	@JsonProperty("quotation")
	private HealthBuddiesQuotationDto quotation;
	
	@JsonProperty("sign_info")
	private HealthBuddiesSignInfoDto sign_info;

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getAgent_channel() {
		return agent_channel;
	}

	public void setAgent_channel(String agent_channel) {
		this.agent_channel = agent_channel;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getSales_channel() {
		return sales_channel;
	}

	public void setSales_channel(String sales_channel) {
		this.sales_channel = sales_channel;
	}

	public List<String> getBenef_info() {
		return benef_info;
	}

	public void setBenef_info(List<String> benef_info) {
		this.benef_info = benef_info;
	}

	public String getCaseCreateDate() {
		return caseCreateDate;
	}

	public void setCaseCreateDate(String caseCreateDate) {
		this.caseCreateDate = caseCreateDate;
	}

	public String getHealthDeclaration() {
		return healthDeclaration;
	}

	public void setHealthDeclaration(String healthDeclaration) {
		this.healthDeclaration = healthDeclaration;
	}

	public List<HealthBuddiesLifeProfileDto> getLife_profiles() {
		return life_profiles;
	}

	public void setLife_profiles(List<HealthBuddiesLifeProfileDto> life_profiles) {
		this.life_profiles = life_profiles;
	}

	public HealthBuddiesQuotationDto getQuotation() {
		return quotation;
	}

	public void setQuotation(HealthBuddiesQuotationDto quotation) {
		this.quotation = quotation;
	}

	public HealthBuddiesSignInfoDto getSign_info() {
		return sign_info;
	}

	public void setSign_info(HealthBuddiesSignInfoDto sign_info) {
		this.sign_info = sign_info;
	}
}
