package com.mabroor.ws.dto;

public class UserMobilePhoneBodyDto {
	
	private String status;
	private String description;
	private MstUserMobileBodyProfileDto data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public MstUserMobileBodyProfileDto getData() {
		return data;
	}
	public void setData(MstUserMobileBodyProfileDto data) {
		this.data = data;
	}
	
	

}
