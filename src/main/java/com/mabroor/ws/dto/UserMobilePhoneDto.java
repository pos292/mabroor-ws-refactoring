package com.mabroor.ws.dto;

public class UserMobilePhoneDto {
	
	private String phone;
	private Long id;
	private String userMobileUuid;
	private String status;

	

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserMobileUuid() {
		return userMobileUuid;
	}

	public void setUserMobileUuid(String userMobileUuid) {
		this.userMobileUuid = userMobileUuid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
