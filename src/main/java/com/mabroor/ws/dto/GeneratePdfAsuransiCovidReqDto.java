package com.mabroor.ws.dto;

public class GeneratePdfAsuransiCovidReqDto {

	private String name;
	private String namaDepan;
	private String namaBelakang;
	private String perlindungan;
	private String tglLahir;
	private String nomorPonsel;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNamaDepan() {
		return namaDepan;
	}
	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}
	public String getNamaBelakang() {
		return namaBelakang;
	}
	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}
	public String getPerlindungan() {
		return perlindungan;
	}
	public void setPerlindungan(String perlindungan) {
		this.perlindungan = perlindungan;
	}
	public String getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(String tglLahir) {
		this.tglLahir = tglLahir;
	}
	public String getNomorPonsel() {
		return nomorPonsel;
	}
	public void setNomorPonsel(String nomorPonsel) {
		this.nomorPonsel = nomorPonsel;
	}	
	
}
