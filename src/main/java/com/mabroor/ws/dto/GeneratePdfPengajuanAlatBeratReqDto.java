package com.mabroor.ws.dto;

public class GeneratePdfPengajuanAlatBeratReqDto {

	private String name;
	private String sektor;
	private String brand;
	private String tipe;
	private String model;
	private String daerahPemakaian;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSektor() {
		return sektor;
	}
	public void setSektor(String sektor) {
		this.sektor = sektor;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getDaerahPemakaian() {
		return daerahPemakaian;
	}
	public void setDaerahPemakaian(String daerahPemakaian) {
		this.daerahPemakaian = daerahPemakaian;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
}