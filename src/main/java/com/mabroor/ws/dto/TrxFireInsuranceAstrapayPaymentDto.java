package com.mabroor.ws.dto;

public class TrxFireInsuranceAstrapayPaymentDto {
		
	private boolean status;	
	private boolean checkPaymentStatus;
	private TrxCarInsuranceAstrapayPaymentDataDto data;
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isCheckPaymentStatus() {
		return checkPaymentStatus;
	}
	public void setCheckPaymentStatus(boolean checkPaymentStatus) {
		this.checkPaymentStatus = checkPaymentStatus;
	}
	public TrxCarInsuranceAstrapayPaymentDataDto getData() {
		return data;
	}
	public void setData(TrxCarInsuranceAstrapayPaymentDataDto data) {
		this.data = data;
	}
	
	

}
