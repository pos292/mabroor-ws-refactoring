package com.mabroor.ws.dto;

public class PromotionCtaButtonDto {

	private Long id;
	private String ctaButton;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCtaButton() {
		return ctaButton;
	}
	public void setCtaButton(String ctaButton) {
		this.ctaButton = ctaButton;
	}
	
	
}
