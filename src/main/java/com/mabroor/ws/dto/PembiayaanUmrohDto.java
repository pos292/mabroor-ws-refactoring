package com.mabroor.ws.dto;

import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PembiayaanUmrohDto {


    @NotNull(message = "dp tidak boleh kosong")
    @Min(value = 1000000L, message = "dp Tidak boleh kurang dari 1jt dan lebih dari 1M")
    @Max(value = 1000000000L, message = "dp Tidak boleh kurang dari 1jt dan lebih dari 1M")
    private Long dp;

    @NotNull(message = "tenor tidak boleh kosong")
    private int tenor;

    private String promoName;

    private String promoCode;

    @Valid
    private PembiayaanUmrohDataDiriDto dataDiri;

    @Valid
    private PembiayaanUmrohAlamatDto alamat;

    @Valid
    private PembiayaanUmrohAlamatKtpDto alamatKtp;


}
