package com.mabroor.ws.dto;

public class FIFWisataReligiDto {

	private Long errorCode;
	
	private String errorMsg;
	
	private FIFWisataReligiResultDto result;

	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public FIFWisataReligiResultDto getResult() {
		return result;
	}

	public void setResult(FIFWisataReligiResultDto result) {
		this.result = result;
	}
}
