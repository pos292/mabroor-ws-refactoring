package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class GeneratePdfGardaEduReqDto {
	
	private String name;
	private String job;
	private String nameDepan;
	private String nameBelakang;
	private String periodePerlindungan;
	private BigDecimal totalPremiBayar;
	private BigDecimal biayaAdmin;
	private BigDecimal premiDiinginkan;
	private BigDecimal santunanPendidikan;
	private BigDecimal tunjanganSekolah;
	private BigDecimal santunanPemakaman;
	private String kodePos;
	private String birthDate;
	private String phone;
	private String email;
	private String identityNumb;
	private String address;
	private String ahliWaris;
	private String hubunganAhliWaris;
	private String anakKandung1;
	private String anakKandung2;
	List<GeneratePdfGardaEduBenefitReqDto> detailPerlindungan;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getNameDepan() {
		return nameDepan;
	}
	public void setNameDepan(String nameDepan) {
		this.nameDepan = nameDepan;
	}
	public String getNameBelakang() {
		return nameBelakang;
	}
	public void setNameBelakang(String nameBelakang) {
		this.nameBelakang = nameBelakang;
	}
	public String getPeriodePerlindungan() {
		return periodePerlindungan;
	}
	public void setPeriodePerlindungan(String periodePerlindungan) {
		this.periodePerlindungan = periodePerlindungan;
	}
	public BigDecimal getTotalPremiBayar() {
		return totalPremiBayar;
	}
	public void setTotalPremiBayar(BigDecimal totalPremiBayar) {
		this.totalPremiBayar = totalPremiBayar;
	}
	public BigDecimal getBiayaAdmin() {
		return biayaAdmin;
	}
	public void setBiayaAdmin(BigDecimal biayaAdmin) {
		this.biayaAdmin = biayaAdmin;
	}
	public BigDecimal getPremiDiinginkan() {
		return premiDiinginkan;
	}
	public void setPremiDiinginkan(BigDecimal premiDiinginkan) {
		this.premiDiinginkan = premiDiinginkan;
	}
	public String getKodePos() {
		return kodePos;
	}
	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdentityNumb() {
		return identityNumb;
	}
	public void setIdentityNumb(String identityNumb) {
		this.identityNumb = identityNumb;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAhliWaris() {
		return ahliWaris;
	}
	public void setAhliWaris(String ahliWaris) {
		this.ahliWaris = ahliWaris;
	}
	public String getHubunganAhliWaris() {
		return hubunganAhliWaris;
	}
	public void setHubunganAhliWaris(String hubunganAhliWaris) {
		this.hubunganAhliWaris = hubunganAhliWaris;
	}
	public String getAnakKandung1() {
		return anakKandung1;
	}
	public void setAnakKandung1(String anakKandung1) {
		this.anakKandung1 = anakKandung1;
	}
	public String getAnakKandung2() {
		return anakKandung2;
	}
	public void setAnakKandung2(String anakKandung2) {
		this.anakKandung2 = anakKandung2;
	}
	public List<GeneratePdfGardaEduBenefitReqDto> getDetailPerlindungan() {
		return detailPerlindungan;
	}
	public void setDetailPerlindungan(List<GeneratePdfGardaEduBenefitReqDto> detailPerlindungan) {
		this.detailPerlindungan = detailPerlindungan;
	}
	public BigDecimal getSantunanPendidikan() {
		return santunanPendidikan;
	}
	public void setSantunanPendidikan(BigDecimal santunanPendidikan) {
		this.santunanPendidikan = santunanPendidikan;
	}
	public BigDecimal getTunjanganSekolah() {
		return tunjanganSekolah;
	}
	public void setTunjanganSekolah(BigDecimal tunjanganSekolah) {
		this.tunjanganSekolah = tunjanganSekolah;
	}
	public BigDecimal getSantunanPemakaman() {
		return santunanPemakaman;
	}
	public void setSantunanPemakaman(BigDecimal santunanPemakaman) {
		this.santunanPemakaman = santunanPemakaman;
	}
	
	

}
