package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class Multiguna4wAstraDto {
	
	private String officeCode;
	private String officeName;
	private String mainCvgTypeCode;
	private String assetHierarchyl1Code;
	private String assetHierarchyl1Name;
	private String creditType;
	private String appType;
	private String leadSource;
	private String assetHierarchyl2Code;
	private String assetHierarchyl2Name;
	private String assetName;
	private String assetCode;
	private BigDecimal downPaymentAmt;
	private Long downPaymentPrcnt;
	private String yearNo;
	private String purposeOfUsageCode;
	private String purposeOfAssetUsage;
	private String budgetCalcType;
	private String inscoBranchCode;
	private String isCapitalized;
	private String administrasi;
	private Long assetPriceAmt;
	private Long budgetAmt;
	private Long mainPremiAmt;
	private Long ntfAmt;
	private Long osInterestAmt;
	private Long osPrincipalAmt;
	private Long policyFee;
	private Long instAmt;
	private Long tenor;
	private String firstInstType;
	private Long grossAmt;
	private BigDecimal supplFlatRatePrcnt;
	private Long totalDownPaymentGrossAmt;
	private Long inscoBranchId;
	private String custName;
	private String custType;
	private String mobilePhone1;
	private String promoCode;
	private String referralCode;
	private String followUpDate;
	private String status;
	private String residenceAddr;
	private List<Multiguna4wAstraDocumentDto> document;
	public String getOfficeCode() {
		return officeCode;
	}
	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}
	public String getOfficeName() {
		return officeName;
	}
	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}
	public String getMainCvgTypeCode() {
		return mainCvgTypeCode;
	}
	public void setMainCvgTypeCode(String mainCvgTypeCode) {
		this.mainCvgTypeCode = mainCvgTypeCode;
	}
	public String getAssetHierarchyl1Code() {
		return assetHierarchyl1Code;
	}
	public void setAssetHierarchyl1Code(String assetHierarchyl1Code) {
		this.assetHierarchyl1Code = assetHierarchyl1Code;
	}
	public String getAssetHierarchyl1Name() {
		return assetHierarchyl1Name;
	}
	public void setAssetHierarchyl1Name(String assetHierarchyl1Name) {
		this.assetHierarchyl1Name = assetHierarchyl1Name;
	}
	public String getCreditType() {
		return creditType;
	}
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	public String getAppType() {
		return appType;
	}
	public void setAppType(String appType) {
		this.appType = appType;
	}
	public String getLeadSource() {
		return leadSource;
	}
	public void setLeadSource(String leadSource) {
		this.leadSource = leadSource;
	}
	public String getAssetHierarchyl2Code() {
		return assetHierarchyl2Code;
	}
	public void setAssetHierarchyl2Code(String assetHierarchyl2Code) {
		this.assetHierarchyl2Code = assetHierarchyl2Code;
	}
	public String getAssetHierarchyl2Name() {
		return assetHierarchyl2Name;
	}
	public void setAssetHierarchyl2Name(String assetHierarchyl2Name) {
		this.assetHierarchyl2Name = assetHierarchyl2Name;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getAssetCode() {
		return assetCode;
	}
	public void setAssetCode(String assetCode) {
		this.assetCode = assetCode;
	}
	public BigDecimal getDownPaymentAmt() {
		return downPaymentAmt;
	}
	public void setDownPaymentAmt(BigDecimal downPaymentAmt) {
		this.downPaymentAmt = downPaymentAmt;
	}
	public Long getDownPaymentPrcnt() {
		return downPaymentPrcnt;
	}
	public void setDownPaymentPrcnt(Long downPaymentPrcnt) {
		this.downPaymentPrcnt = downPaymentPrcnt;
	}
	public String getYearNo() {
		return yearNo;
	}
	public void setYearNo(String yearNo) {
		this.yearNo = yearNo;
	}
	public String getPurposeOfUsageCode() {
		return purposeOfUsageCode;
	}
	public void setPurposeOfUsageCode(String purposeOfUsageCode) {
		this.purposeOfUsageCode = purposeOfUsageCode;
	}
	public String getPurposeOfAssetUsage() {
		return purposeOfAssetUsage;
	}
	public void setPurposeOfAssetUsage(String purposeOfAssetUsage) {
		this.purposeOfAssetUsage = purposeOfAssetUsage;
	}
	public String getBudgetCalcType() {
		return budgetCalcType;
	}
	public void setBudgetCalcType(String budgetCalcType) {
		this.budgetCalcType = budgetCalcType;
	}
	public String getInscoBranchCode() {
		return inscoBranchCode;
	}
	public void setInscoBranchCode(String inscoBranchCode) {
		this.inscoBranchCode = inscoBranchCode;
	}
	public String getIsCapitalized() {
		return isCapitalized;
	}
	public void setIsCapitalized(String isCapitalized) {
		this.isCapitalized = isCapitalized;
	}
	public String getAdministrasi() {
		return administrasi;
	}
	public void setAdministrasi(String administrasi) {
		this.administrasi = administrasi;
	}
	public Long getAssetPriceAmt() {
		return assetPriceAmt;
	}
	public void setAssetPriceAmt(Long assetPriceAmt) {
		this.assetPriceAmt = assetPriceAmt;
	}
	public Long getBudgetAmt() {
		return budgetAmt;
	}
	public void setBudgetAmt(Long budgetAmt) {
		this.budgetAmt = budgetAmt;
	}
	public Long getMainPremiAmt() {
		return mainPremiAmt;
	}
	public void setMainPremiAmt(Long mainPremiAmt) {
		this.mainPremiAmt = mainPremiAmt;
	}
	public Long getNtfAmt() {
		return ntfAmt;
	}
	public void setNtfAmt(Long ntfAmt) {
		this.ntfAmt = ntfAmt;
	}
	public Long getOsInterestAmt() {
		return osInterestAmt;
	}
	public void setOsInterestAmt(Long osInterestAmt) {
		this.osInterestAmt = osInterestAmt;
	}
	public Long getOsPrincipalAmt() {
		return osPrincipalAmt;
	}
	public void setOsPrincipalAmt(Long osPrincipalAmt) {
		this.osPrincipalAmt = osPrincipalAmt;
	}
	public Long getPolicyFee() {
		return policyFee;
	}
	public void setPolicyFee(Long policyFee) {
		this.policyFee = policyFee;
	}
	public Long getInstAmt() {
		return instAmt;
	}
	public void setInstAmt(Long instAmt) {
		this.instAmt = instAmt;
	}
	public Long getTenor() {
		return tenor;
	}
	public void setTenor(Long tenor) {
		this.tenor = tenor;
	}
	public String getFirstInstType() {
		return firstInstType;
	}
	public void setFirstInstType(String firstInstType) {
		this.firstInstType = firstInstType;
	}
	public Long getGrossAmt() {
		return grossAmt;
	}
	public void setGrossAmt(Long grossAmt) {
		this.grossAmt = grossAmt;
	}
	public BigDecimal getSupplFlatRatePrcnt() {
		return supplFlatRatePrcnt;
	}
	public void setSupplFlatRatePrcnt(BigDecimal supplFlatRatePrcnt) {
		this.supplFlatRatePrcnt = supplFlatRatePrcnt;
	}
	public Long getTotalDownPaymentGrossAmt() {
		return totalDownPaymentGrossAmt;
	}
	public void setTotalDownPaymentGrossAmt(Long totalDownPaymentGrossAmt) {
		this.totalDownPaymentGrossAmt = totalDownPaymentGrossAmt;
	}
	public Long getInscoBranchId() {
		return inscoBranchId;
	}
	public void setInscoBranchId(Long inscoBranchId) {
		this.inscoBranchId = inscoBranchId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	public String getMobilePhone1() {
		return mobilePhone1;
	}
	public void setMobilePhone1(String mobilePhone1) {
		this.mobilePhone1 = mobilePhone1;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public String getFollowUpDate() {
		return followUpDate;
	}
	public void setFollowUpDate(String followUpDate) {
		this.followUpDate = followUpDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResidenceAddr() {
		return residenceAddr;
	}
	public void setResidenceAddr(String residenceAddr) {
		this.residenceAddr = residenceAddr;
	}
	public List<Multiguna4wAstraDocumentDto> getDocument() {
		return document;
	}
	public void setDocument(List<Multiguna4wAstraDocumentDto> document) {
		this.document = document;
	}
	
	
	
	

}
