package com.mabroor.ws.dto;

public class GeneratePdfAirportTransferReqDto {
	
	private String name;
	private String serviceType;
	private String airport;
	private String address;
	private String destinationType;
	private String date;
	private String carModel;
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getAirport() {
		return airport;
	}
	public String getAddress() {
		return address;
	}
	public String getDestinationType() {
		return destinationType;
	}
	public void setAirport(String airport) {
		this.airport = airport;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
