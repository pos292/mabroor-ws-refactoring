package com.mabroor.ws.dto;

import java.util.Date;

public class WaktuTerdekatDto {
	
	private String time;
	private String judul;
	private String jam;
	private String wording1;
	private String wording2;
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}

	public String getWording1() {
		return wording1;
	}

	public void setWording1(String wording1) {
		this.wording1 = wording1;
	}

	public String getWording2() {
		return wording2;
	}

	public void setWording2(String wording2) {
		this.wording2 = wording2;
	}
}
