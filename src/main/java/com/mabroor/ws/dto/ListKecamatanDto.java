package com.mabroor.ws.dto;

public class ListKecamatanDto {

    private Long id;
    private Long idKota;
    private String namaKecamatan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdKota() {
        return idKota;
    }

    public void setIdKota(Long idKota) {
        this.idKota = idKota;
    }

    public String getNamaKecamatan() {
        return namaKecamatan;
    }

    public void setNamaKecamatan(String namaKecamatan) {
        this.namaKecamatan = namaKecamatan;
    }
}
