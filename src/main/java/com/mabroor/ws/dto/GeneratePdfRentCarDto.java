package com.mabroor.ws.dto;

import java.math.BigDecimal;

public class GeneratePdfRentCarDto {
	
	private String name;
	private String withDriver;
	private String kota;
	private String paketSewaJam;
	private String pickUpTime;
	private String durasiSewaTime;
	private String modelMobil;
	private String paketSewa;
	private String lokasi;
	private String catatan;
	private String namaDepan;
	private String namaBelakang;
	private String tanggalLahir;
	private String jenisKelamin;
	private String noPonsel;
	private String email;
	private String noIdenty;
	private String durasiSewa;
	private BigDecimal totalBiaya;
	private String paketTambahan;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKota() {
		return kota;
	}
	public void setKota(String kota) {
		this.kota = kota;
	}
	public String getWithDriver() {
		return withDriver;
	}
	public void setWithDriver(String withDriver) {
		this.withDriver = withDriver;
	}
	public String getPickUpTime() {
		return pickUpTime;
	}
	public void setPickUpTime(String pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	public String getModelMobil() {
		return modelMobil;
	}
	public void setModelMobil(String modelMobil) {
		this.modelMobil = modelMobil;
	}
	public String getPaketSewa() {
		return paketSewa;
	}
	public void setPaketSewa(String paketSewa) {
		this.paketSewa = paketSewa;
	}
	public String getPaketTambahan() {
		return paketTambahan;
	}
	public void setPaketTambahan(String paketTambahan) {
		this.paketTambahan = paketTambahan;
	}
	public String getDurasiSewa() {
		return durasiSewa;
	}
	public void setDurasiSewa(String durasiSewa) {
		this.durasiSewa = durasiSewa;
	}
	public BigDecimal getTotalBiaya() {
		return totalBiaya;
	}
	public void setTotalBiaya(BigDecimal totalBiaya) {
		this.totalBiaya = totalBiaya;
	}
	public String getDurasiSewaTime() {
		return durasiSewaTime;
	}
	public void setDurasiSewaTime(String durasiSewaTime) {
		this.durasiSewaTime = durasiSewaTime;
	}
	public String getPaketSewaJam() {
		return paketSewaJam;
	}
	public void setPaketSewaJam(String paketSewaJam) {
		this.paketSewaJam = paketSewaJam;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	public String getCatatan() {
		return catatan;
	}
	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}
	public String getNamaDepan() {
		return namaDepan;
	}
	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}
	public String getNamaBelakang() {
		return namaBelakang;
	}
	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}
	public String getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getNoPonsel() {
		return noPonsel;
	}
	public void setNoPonsel(String noPonsel) {
		this.noPonsel = noPonsel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNoIdenty() {
		return noIdenty;
	}
	public void setNoIdenty(String noIdenty) {
		this.noIdenty = noIdenty;
	}
	

}
