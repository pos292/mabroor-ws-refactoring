package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FlexiHealthLifeProfileDto {
	
	@JsonProperty("surname")
	private String surname;
	
	@JsonProperty("given_name")
	private String given_name;
	
	@JsonProperty("birthplace")
	private String birthplace;
	
	@JsonProperty("dob")
	private String dob;
	
	@JsonProperty("gender_code")
	private String gender_code;
	
	@JsonProperty("id_no")
	private String id_no;
	
	@JsonProperty("mobile_no")
	private String mobile_no;
	
	@JsonProperty("address")
	private String address;
	
	@JsonProperty("kelurahan")
	private String kelurahan;
	
	@JsonProperty("kecamatan")
	private String kecamatan;
	
	@JsonProperty("province_code")
	private String province_code;
	
	@JsonProperty("zip_code")
	private String zip_code;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("account_name")
	private String account_name;
	
	@JsonProperty("bank_code")
	private String bank_code;
	
	@JsonProperty("account_no")
	private String account_no;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("life_index")
	private String life_index;
	
	@JsonProperty("rel_code")
	private String rel_code;
	
	@JsonProperty("isUSResident")
	private String isUSResident;
	
	@JsonProperty("riders")
	private List<String> riders;
	
	@JsonProperty("cust_type")
	private String cust_type;
	
	@JsonProperty("forceNewClient")
	private String forceNewClient;
	
	@JsonProperty("confirmExisting")
	private String confirmExisting;
	
	@JsonProperty("master_client_id")
	private String master_client_id;
	
	@JsonProperty("contract_structures")
	private List<FlexiHealthLifeProfileContractStructureDto> contract_structures;

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGiven_name() {
		return given_name;
	}

	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	public String getBirthplace() {
		return birthplace;
	}

	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender_code() {
		return gender_code;
	}

	public void setGender_code(String gender_code) {
		this.gender_code = gender_code;
	}

	public String getId_no() {
		return id_no;
	}

	public void setId_no(String id_no) {
		this.id_no = id_no;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getProvince_code() {
		return province_code;
	}

	public void setProvince_code(String province_code) {
		this.province_code = province_code;
	}

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getBank_code() {
		return bank_code;
	}

	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}

	public String getAccount_no() {
		return account_no;
	}

	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLife_index() {
		return life_index;
	}

	public void setLife_index(String life_index) {
		this.life_index = life_index;
	}

	public String getRel_code() {
		return rel_code;
	}

	public void setRel_code(String rel_code) {
		this.rel_code = rel_code;
	}

	public String getIsUSResident() {
		return isUSResident;
	}

	public void setIsUSResident(String isUSResident) {
		this.isUSResident = isUSResident;
	}

	public List<String> getRiders() {
		return riders;
	}

	public void setRiders(List<String> riders) {
		this.riders = riders;
	}

	public String getCust_type() {
		return cust_type;
	}

	public void setCust_type(String cust_type) {
		this.cust_type = cust_type;
	}

	public String getForceNewClient() {
		return forceNewClient;
	}

	public void setForceNewClient(String forceNewClient) {
		this.forceNewClient = forceNewClient;
	}

	public String getConfirmExisting() {
		return confirmExisting;
	}

	public void setConfirmExisting(String confirmExisting) {
		this.confirmExisting = confirmExisting;
	}

	public String getMaster_client_id() {
		return master_client_id;
	}

	public void setMaster_client_id(String master_client_id) {
		this.master_client_id = master_client_id;
	}

	public List<FlexiHealthLifeProfileContractStructureDto> getContract_structures() {
		return contract_structures;
	}

	public void setContract_structures(List<FlexiHealthLifeProfileContractStructureDto> contract_structures) {
		this.contract_structures = contract_structures;
	}
	
	

}
