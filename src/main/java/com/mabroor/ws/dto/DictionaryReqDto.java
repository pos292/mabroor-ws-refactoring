package com.mabroor.ws.dto;

public class DictionaryReqDto {
	
	private Long id;
	
	private String title;
	
	private String description;
	
	private String image;
	
	private String isActiveHomepage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getIsActiveHomepage() {
		return isActiveHomepage;
	}

	public void setIsActiveHomepage(String isActiveHomepage) {
		this.isActiveHomepage = isActiveHomepage;
	}
}
