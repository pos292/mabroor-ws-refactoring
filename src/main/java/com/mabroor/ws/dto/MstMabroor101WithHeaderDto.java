package com.mabroor.ws.dto;

import java.util.List;

import com.mabroor.ws.entity.MstMabroor101;

public class MstMabroor101WithHeaderDto {
	private String backgroundImage;
	private String titleHeader;
	private String subTitleHeader;
	private List<MstMabroor101> list;

	public String getBackgroundImage() {
		return backgroundImage;
	}
	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}
	public String getTitleHeader() {
		return titleHeader;
	}
	public void setTitleHeader(String titleHeader) {
		this.titleHeader = titleHeader;
	}
	public String getSubTitleHeader() {
		return subTitleHeader;
	}
	public void setSubTitleHeader(String subTitleHeader) {
		this.subTitleHeader = subTitleHeader;
	}
	public List<MstMabroor101> getList() {
		return list;
	}
	public void setList(List<MstMabroor101> list) {
		this.list = list;
	}
}
