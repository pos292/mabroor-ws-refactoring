package com.mabroor.ws.dto;

import java.util.HashMap;
import java.util.List;

public class RespDetailWilayahDto {

    private String kodePos;

    private List<HashMap<String,String>> kelurahan;

    private String kecamatan;

    private String kota;

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public List<HashMap<String, String>> getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(List<HashMap<String, String>> kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }
}
