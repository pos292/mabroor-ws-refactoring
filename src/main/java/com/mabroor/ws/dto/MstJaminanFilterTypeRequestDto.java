package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MstJaminanFilterTypeRequestDto {
	
	@JsonProperty("P_LANGUAGE")
	private String P_LANGUAGE;
	
	@JsonProperty("P_CD_BRAND")
	private String P_CD_BRAND;
	
	@JsonProperty("P_YEAR")
	private String P_YEAR;

	public String getP_LANGUAGE() {
		return P_LANGUAGE;
	}

	public void setP_LANGUAGE(String p_LANGUAGE) {
		P_LANGUAGE = p_LANGUAGE;
	}

	public String getP_CD_BRAND() {
		return P_CD_BRAND;
	}

	public void setP_CD_BRAND(String p_CD_BRAND) {
		P_CD_BRAND = p_CD_BRAND;
	}

	public String getP_YEAR() {
		return P_YEAR;
	}

	public void setP_YEAR(String p_YEAR) {
		P_YEAR = p_YEAR;
	}
	
	

}
