package com.mabroor.ws.dto;

import java.util.List;

public class RecommendationDto {
	
	private List<RecommendationListDto> recommendation;
	private List<NormalRecomDto> normal;
	
	public List<RecommendationListDto> getRecommendation() {
		return recommendation;
	}
	public void setRecommendation(List<RecommendationListDto> recommendation) {
		this.recommendation = recommendation;
	}
	public List<NormalRecomDto> getNormal() {
		return normal;
	}
	public void setNormal(List<NormalRecomDto> normal) {
		this.normal = normal;
	}
	
	

}
