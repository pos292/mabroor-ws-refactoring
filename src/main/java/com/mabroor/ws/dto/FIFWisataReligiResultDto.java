package com.mabroor.ws.dto;

public class FIFWisataReligiResultDto {

	private String amitraId;
	
	private String leadId;
	
    private String leadDataSelfieId;
	
	private String leadDataKTPId;
	
	private String similaritySelfieAndKTP;

	private String digitalLeadId;
	
	private String produkPembiayaan;
	
	private String paketCode;
	
	private String paketValue;
	
	private String tenor;
	
	private String dp;
	
	private String fullname;
	
	private String customerType;
	
	private String noKTP;
	
	private String mobilePhone1;
	
	private String mobilePhone2;
	
	private String email;
	
	private String birthdate;
	
	private String birthPlace;
	
	private String gender;
	
	private String maritalStatus;
	
	private String homeOwnership;
	
	private String education;
	
	private String occupationType;
	
	private String occupation;
	
	private String jobPosition;
	
	private String monthlyIncome;
	
	private String addressLine;
	
	private String noRT;
	
	private String noRW;
	
	private String stateId;
	
	private String cityId;
	
	private String districtId;
	
	private String subDistrictId;
	
	private String zipcode;
	
	private String stateName;
	
	private String cityName;
	
	private String districtName;
	
	private String subDistrictName;
	
	private String subZipcode;
	
	private String referral;
	
	private String submitDate;
	
	private String interest;
	
	private String approved;
	
	private String sourceOfInput;
	
	private String interestDate;
	
	private String interestBy;
	
	private String approvedDate;
	
	private String approvedBy;
	
	private String remark;
	
	private String latitude;
	
	private String longitude;
	
	private String customerNo;
	
	private String fgcNo;
	
	private String callVia;
	
	private String contactDetail;
	
	private String userKey;
	
	private String namaProdukPembiayaan;
	
	private String customerTypeName;
	
	private String communityName;
	
	private String leadDataSelfie;
	
	private String leadDataKTP;
	
	private String status;
	
	private String callViaName;
	
	private String motherName;

	public String getAmitraId() {
		return amitraId;
	}

	public void setAmitraId(String amitraId) {
		this.amitraId = amitraId;
	}


	public String getDigitalLeadId() {
		return digitalLeadId;
	}

	public void setDigitalLeadId(String digitalLeadId) {
		this.digitalLeadId = digitalLeadId;
	}

	public String getProdukPembiayaan() {
		return produkPembiayaan;
	}

	public void setProdukPembiayaan(String produkPembiayaan) {
		this.produkPembiayaan = produkPembiayaan;
	}

	public String getPaketCode() {
		return paketCode;
	}

	public void setPaketCode(String paketCode) {
		this.paketCode = paketCode;
	}

	public String getPaketValue() {
		return paketValue;
	}

	public void setPaketValue(String paketValue) {
		this.paketValue = paketValue;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getDp() {
		return dp;
	}

	public void setDp(String dp) {
		this.dp = dp;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getNoKTP() {
		return noKTP;
	}

	public void setNoKTP(String noKTP) {
		this.noKTP = noKTP;
	}

	public String getMobilePhone1() {
		return mobilePhone1;
	}

	public void setMobilePhone1(String mobilePhone1) {
		this.mobilePhone1 = mobilePhone1;
	}

	public String getMobilePhone2() {
		return mobilePhone2;
	}

	public void setMobilePhone2(String mobilePhone2) {
		this.mobilePhone2 = mobilePhone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getHomeOwnership() {
		return homeOwnership;
	}

	public void setHomeOwnership(String homeOwnership) {
		this.homeOwnership = homeOwnership;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getOccupationType() {
		return occupationType;
	}

	public void setOccupationType(String occupationType) {
		this.occupationType = occupationType;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public String getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getNoRT() {
		return noRT;
	}

	public void setNoRT(String noRT) {
		this.noRT = noRT;
	}

	public String getNoRW() {
		return noRW;
	}

	public void setNoRW(String noRW) {
		this.noRW = noRW;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getDistrictId() {
		return districtId;
	}

	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	public String getSubDistrictId() {
		return subDistrictId;
	}

	public void setSubDistrictId(String subDistrictId) {
		this.subDistrictId = subDistrictId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getSubDistrictName() {
		return subDistrictName;
	}

	public void setSubDistrictName(String subDistrictName) {
		this.subDistrictName = subDistrictName;
	}

	public String getSubZipcode() {
		return subZipcode;
	}

	public void setSubZipcode(String subZipcode) {
		this.subZipcode = subZipcode;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getApproved() {
		return approved;
	}

	public void setApproved(String approved) {
		this.approved = approved;
	}

	public String getSourceOfInput() {
		return sourceOfInput;
	}

	public void setSourceOfInput(String sourceOfInput) {
		this.sourceOfInput = sourceOfInput;
	}

	public String getInterestDate() {
		return interestDate;
	}

	public void setInterestDate(String interestDate) {
		this.interestDate = interestDate;
	}

	public String getInterestBy() {
		return interestBy;
	}

	public void setInterestBy(String interestBy) {
		this.interestBy = interestBy;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getFgcNo() {
		return fgcNo;
	}

	public void setFgcNo(String fgcNo) {
		this.fgcNo = fgcNo;
	}

	public String getCallVia() {
		return callVia;
	}

	public void setCallVia(String callVia) {
		this.callVia = callVia;
	}

	public String getContactDetail() {
		return contactDetail;
	}

	public void setContactDetail(String contactDetail) {
		this.contactDetail = contactDetail;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getNamaProdukPembiayaan() {
		return namaProdukPembiayaan;
	}

	public void setNamaProdukPembiayaan(String namaProdukPembiayaan) {
		this.namaProdukPembiayaan = namaProdukPembiayaan;
	}

	public String getCustomerTypeName() {
		return customerTypeName;
	}

	public void setCustomerTypeName(String customerTypeName) {
		this.customerTypeName = customerTypeName;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getLeadDataSelfie() {
		return leadDataSelfie;
	}

	public void setLeadDataSelfie(String leadDataSelfie) {
		this.leadDataSelfie = leadDataSelfie;
	}

	public String getLeadDataKTP() {
		return leadDataKTP;
	}

	public void setLeadDataKTP(String leadDataKTP) {
		this.leadDataKTP = leadDataKTP;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCallViaName() {
		return callViaName;
	}

	public void setCallViaName(String callViaName) {
		this.callViaName = callViaName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	public String getLeadDataSelfieId() {
		return leadDataSelfieId;
	}

	public void setLeadDataSelfieId(String leadDataSelfieId) {
		this.leadDataSelfieId = leadDataSelfieId;
	}

	public String getLeadDataKTPId() {
		return leadDataKTPId;
	}

	public void setLeadDataKTPId(String leadDataKTPId) {
		this.leadDataKTPId = leadDataKTPId;
	}

	public String getSimilaritySelfieAndKTP() {
		return similaritySelfieAndKTP;
	}

	public void setSimilaritySelfieAndKTP(String similaritySelfieAndKTP) {
		this.similaritySelfieAndKTP = similaritySelfieAndKTP;
	}
	
}
