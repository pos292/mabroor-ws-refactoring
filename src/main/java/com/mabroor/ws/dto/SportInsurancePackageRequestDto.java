package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SportInsurancePackageRequestDto {

	@JsonProperty("package_code")
	private String package_code;
	
	@JsonProperty("package_name")
	private String package_name;
	
	@JsonProperty("premium_package")
	private List<SportInsurancePremiumPackageDto> premium_package;
	
	@JsonProperty("life_assured_count")
	private String life_assured_count;
	
	private String icon;

	public String getPackage_code() {
		return package_code;
	}

	public void setPackage_code(String package_code) {
		this.package_code = package_code;
	}

	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}

	public List<SportInsurancePremiumPackageDto> getPremium_package() {
		return premium_package;
	}

	public void setPremium_package(List<SportInsurancePremiumPackageDto> premium_package) {
		this.premium_package = premium_package;
	}

	public String getLife_assured_count() {
		return life_assured_count;
	}

	public void setLife_assured_count(String life_assured_count) {
		this.life_assured_count = life_assured_count;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
