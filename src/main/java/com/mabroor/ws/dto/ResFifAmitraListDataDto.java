package com.mabroor.ws.dto;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.List;

@Data
public class ResFifAmitraListDataDto {
    private String errorCode;
    private String errorMsg;
    private List<LinkedHashMap<String, Object>> result;
}
