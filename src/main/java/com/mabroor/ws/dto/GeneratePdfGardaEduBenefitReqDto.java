package com.mabroor.ws.dto;

public class GeneratePdfGardaEduBenefitReqDto {

	private String benefit;
	private String value;
	private String type;
	
	public String getBenefit() {
		return benefit;
	}
	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
