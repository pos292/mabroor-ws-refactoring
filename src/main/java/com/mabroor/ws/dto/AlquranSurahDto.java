package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AlquranSurahDto {

	@JsonProperty("_id")
	private int _id;
	private String phonetic;
	private String ar;
	private int count;
	private String en;
	@JsonProperty("first_page")
	private int first_page;
	private String ind;
	@JsonProperty("phonetic_ind")
	private String phonetic_ind;
	private int sura;
	
	
	
	public String getPhonetic() {
		return phonetic;
	}
	public void setPhonetic(String phonetic) {
		this.phonetic = phonetic;
	}
	public int get_id() {
		return _id;
	}
	public void set_id(int _id) {
		this._id = _id;
	}
	public String getAr() {
		return ar;
	}
	public void setAr(String ar) {
		this.ar = ar;
	}
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getEn() {
		return en;
	}
	public void setEn(String en) {
		this.en = en;
	}
	public int getFirst_page() {
		return first_page;
	}
	public void setFirst_page(int first_page) {
		this.first_page = first_page;
	}
	public String getInd() {
		return ind;
	}
	public void setInd(String ind) {
		this.ind = ind;
	}
	public String getPhonetic_ind() {
		return phonetic_ind;
	}
	public void setPhonetic_ind(String phonetic_ind) {
		this.phonetic_ind = phonetic_ind;
	}
	public int getSura() {
		return sura;
	}
	public void setSura(int sura) {
		this.sura = sura;
	}
	
	
	
}
