package com.mabroor.ws.dto;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ResponseApiDto {
	private String status;
	private String description;
	private LinkedHashMap<String, Object> data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LinkedHashMap<String, Object> getData() {
		return data;
	}
	public void setData(LinkedHashMap<String, Object> data) {
		this.data = data;
	}
	

}
