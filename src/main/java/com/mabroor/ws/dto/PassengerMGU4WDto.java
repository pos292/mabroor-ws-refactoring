package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PassengerMGU4WDto {
	
	private String name;
	private String phoneNumber;
	private String email;
	private String idCardNumber;	
	private String npwpNumber;
	private String licenseNumber;
	private String passportNumber;
	private String address;
	private boolean isPIC;
	private String imageKTP;
	private String imageSIM;
	private boolean isForeigner;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdCardNumber() {
		return idCardNumber;
	}
	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}
	public String getNpwpNumber() {
		return npwpNumber;
	}
	public void setNpwpNumber(String npwpNumber) {
		this.npwpNumber = npwpNumber;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public boolean isPIC() {
		return isPIC;
	}
	public void setPIC(boolean isPIC) {
		this.isPIC = isPIC;
	}
	public String getImageKTP() {
		return imageKTP;
	}
	public void setImageKTP(String imageKTP) {
		this.imageKTP = imageKTP;
	}
	public String getImageSIM() {
		return imageSIM;
	}
	public void setImageSIM(String imageSIM) {
		this.imageSIM = imageSIM;
	}
	public boolean isForeigner() {
		return isForeigner;
	}
	public void setForeigner(boolean isForeigner) {
		this.isForeigner = isForeigner;
	}
	
	

}
