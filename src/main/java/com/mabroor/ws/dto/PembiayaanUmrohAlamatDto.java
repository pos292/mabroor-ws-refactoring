package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
public class PembiayaanUmrohAlamatDto {

    @NotBlank(message = "tempatTinggal tidak boleh kosong")
    @Size(max = 130)
    private String tempatTinggal;
    @NotBlank(message = "provinsi tidak boleh kosong")
    private String provinsi;
    @NotBlank(message = "kota tidak boleh kosong")
    private String kota;
    @NotBlank(message = "kecamatan tidak boleh kosong")
    private String kecamatan;
    @NotBlank(message = "kelurahan tidak boleh kosong")
    private String kelurahan;
    @NotBlank(message = "rt tidak boleh kosong")
    private String rt;
    @NotBlank(message = "rw tidak boleh kosong")
    private String rw;
    @NotBlank(message = "kodePos tidak boleh kosong")
    private String kodePos;

    public String getTempatTinggal() {
        return tempatTinggal;
    }

    public void setTempatTinggal(String tempatTinggal) {
        this.tempatTinggal = tempatTinggal;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }
}
