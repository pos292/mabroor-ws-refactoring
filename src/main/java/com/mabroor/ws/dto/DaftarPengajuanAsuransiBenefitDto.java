package com.mabroor.ws.dto;

public class DaftarPengajuanAsuransiBenefitDto {

	private String title;

	private String answer;
	
	private String amount;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	} 
	
	
}
