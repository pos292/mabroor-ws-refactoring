package com.mabroor.ws.dto;

public class RentCarTracResponseReservationDetailDto {
	private String bookingOrderId;

	public String getBookingOrderId() {
		return bookingOrderId;
	}

	public void setBookingOrderId(String bookingOrderId) {
		this.bookingOrderId = bookingOrderId;
	}
	
}
