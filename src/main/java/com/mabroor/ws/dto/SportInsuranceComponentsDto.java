package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SportInsuranceComponentsDto {

	@JsonProperty("component_code")
	private String component_code;
	
	@JsonProperty("component_description")
	private String component_description;
	
	@JsonProperty("premium_amount")
	private String premium_amount;
	
	@JsonProperty("component_type")
	private String component_type;
	
	@JsonProperty("component_status")
	private String component_status;
	
	private String icon;

	public String getComponent_code() {
		return component_code;
	}

	public void setComponent_code(String component_code) {
		this.component_code = component_code;
	}

	public String getComponent_description() {
		return component_description;
	}

	public void setComponent_description(String component_description) {
		this.component_description = component_description;
	}

	public String getPremium_amount() {
		return premium_amount;
	}

	public void setPremium_amount(String premium_amount) {
		this.premium_amount = premium_amount;
	}

	public String getComponent_type() {
		return component_type;
	}

	public void setComponent_type(String component_type) {
		this.component_type = component_type;
	}

	public String getComponent_status() {
		return component_status;
	}

	public void setComponent_status(String component_status) {
		this.component_status = component_status;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
