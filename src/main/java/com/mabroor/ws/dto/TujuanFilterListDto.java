package com.mabroor.ws.dto;

import java.util.List;

public class TujuanFilterListDto {

	private List<Long> tujuan;
	
	private Long limit;

	public Long getLimit() {
		return limit;
	}

	public void setLimit(Long limit) {
		this.limit = limit;
	}

	public List<Long> getTujuan() {
		return tujuan;
	}

	public void setTujuan(List<Long> tujuan) {
		this.tujuan = tujuan;
	}
}
