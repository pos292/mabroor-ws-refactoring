package com.mabroor.ws.dto;

import com.mabroor.ws.entity.MstPengajuanCar;

public class PengajuanCarTafDto {

	private MstPengajuanCar data;
	
	private PengajuanCarTafCalculatordto calculatorResult;

	public MstPengajuanCar getData() {
		return data;
	}

	public void setData(MstPengajuanCar data) {
		this.data = data;
	}

	public PengajuanCarTafCalculatordto getCalculatorResult() {
		return calculatorResult;
	}

	public void setCalculatorResult(PengajuanCarTafCalculatordto calculatorResult) {
		this.calculatorResult = calculatorResult;
	}
}
