package com.mabroor.ws.dto;

import java.util.List;

public class MenuUserDto {
	private List<MainMenuDto> parent;
	private List<MenuDto> child;

	public List<MainMenuDto> getParent() {
		return parent;
	}

	public void setParent(List<MainMenuDto> parent) {
		this.parent = parent;
	}

	public List<MenuDto> getChild() {
		return child;
	}

	public void setChild(List<MenuDto> child) {
		this.child = child;
	}

}
