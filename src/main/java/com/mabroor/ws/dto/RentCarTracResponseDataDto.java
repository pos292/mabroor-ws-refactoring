package com.mabroor.ws.dto;

import java.util.List;

public class RentCarTracResponseDataDto {
	private List<RentCarTracResponseReservationDetailDto> reservationDetail;
	private String id;

	public List<RentCarTracResponseReservationDetailDto> getReservationDetail() {
		return reservationDetail;
	}

	public void setReservationDetail(List<RentCarTracResponseReservationDetailDto> reservationDetail) {
		this.reservationDetail = reservationDetail;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
