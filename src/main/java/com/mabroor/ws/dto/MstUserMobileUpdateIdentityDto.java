package com.mabroor.ws.dto;

public class MstUserMobileUpdateIdentityDto {

	private Long id;
	
    private String ktpNumber;
	
	private String npwpNumber;
	
    private String ktpImage;
	
	private String npwpImage;
	
	private String kkImage;
	
	private String ppImage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKtpNumber() {
		return ktpNumber;
	}

	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}

	public String getNpwpNumber() {
		return npwpNumber;
	}

	public void setNpwpNumber(String npwpNumber) {
		this.npwpNumber = npwpNumber;
	}

	public String getKtpImage() {
		return ktpImage;
	}

	public void setKtpImage(String ktpImage) {
		this.ktpImage = ktpImage;
	}

	public String getNpwpImage() {
		return npwpImage;
	}

	public void setNpwpImage(String npwpImage) {
		this.npwpImage = npwpImage;
	}

	public String getKkImage() {
		return kkImage;
	}

	public void setKkImage(String kkImage) {
		this.kkImage = kkImage;
	}

	public String getPpImage() {
		return ppImage;
	}

	public void setPpImage(String ppImage) {
		this.ppImage = ppImage;
	}
	
	
}
