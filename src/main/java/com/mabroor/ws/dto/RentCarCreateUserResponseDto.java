package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RentCarCreateUserResponseDto {
	
	@JsonProperty("Status")
	private Long Status;
	@JsonProperty("Data")
	private RentCarCreateUserResponseDataDto Data;
	@JsonProperty("Message")
	private String Message;
	public Long getStatus() {
		return Status;
	}
	public void setStatus(Long status) {
		Status = status;
	}
	public RentCarCreateUserResponseDataDto getData() {
		return Data;
	}
	public void setData(RentCarCreateUserResponseDataDto data) {
		Data = data;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	
	

}
