package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarCreditSimulationInsertRequestDto {

	@JsonProperty("P_CD_PRODUCT")
	private String P_CD_PRODUCT;
	
	@JsonProperty("P_TYPE_INSTALLMENT")
	private String P_TYPE_INSTALLMENT;
	
	@JsonProperty("P_OTR")
	private String P_OTR;
	
	@JsonProperty("P_AMT_DP")
	private String P_AMT_DP;
	
	@JsonProperty("P_AMT_TDP")
	private String P_AMT_TDP;
	
	@JsonProperty("P_TENOR")
	private String P_TENOR;
	
	@JsonProperty("P_AMT_INSTALLMENT")
	private String P_AMT_INSTALLMENT;
	
	@JsonProperty("P_FLAT_RATE")
	private String P_FLAT_RATE;
	
	@JsonProperty("P_EFF_RATE")
	private String P_EFF_RATE;
	
	@JsonProperty("P_FLAG_ACP")
	private String P_FLAG_ACP;
	
	@JsonProperty("P_MODE_ACP")
	private String P_MODE_ACP;
	
	@JsonProperty("P_TYPE_INSU")
	private String P_TYPE_INSU;
	
	@JsonProperty("P_TENOR_AR")
	private String P_TENOR_AR;
	
	@JsonProperty("P_TENOR_TLO")
	private String P_TENOR_TLO;
	
	@JsonProperty("P_TENOR_CASH")
	private String P_TENOR_CASH;
	
	@JsonProperty("P_TENOR_KREDIT")
	private String P_TENOR_KREDIT;
	
	@JsonProperty("P_ADMIN_FEE")
	private String P_ADMIN_FEE;
	
	@JsonProperty("P_AMT_POLIS")
	private String P_AMT_POLIS;
	
	@JsonProperty("P_AMT_TJH")
	private String P_AMT_TJH;
	
	@JsonProperty("P_RATE_PDC")
	private String P_RATE_PDC;

	public String getP_CD_PRODUCT() {
		return P_CD_PRODUCT;
	}

	public void setP_CD_PRODUCT(String p_CD_PRODUCT) {
		P_CD_PRODUCT = p_CD_PRODUCT;
	}

	public String getP_TYPE_INSTALLMENT() {
		return P_TYPE_INSTALLMENT;
	}

	public void setP_TYPE_INSTALLMENT(String p_TYPE_INSTALLMENT) {
		P_TYPE_INSTALLMENT = p_TYPE_INSTALLMENT;
	}

	public String getP_OTR() {
		return P_OTR;
	}

	public void setP_OTR(String p_OTR) {
		P_OTR = p_OTR;
	}

	public String getP_AMT_DP() {
		return P_AMT_DP;
	}

	public void setP_AMT_DP(String p_AMT_DP) {
		P_AMT_DP = p_AMT_DP;
	}

	public String getP_AMT_TDP() {
		return P_AMT_TDP;
	}

	public void setP_AMT_TDP(String p_AMT_TDP) {
		P_AMT_TDP = p_AMT_TDP;
	}

	public String getP_TENOR() {
		return P_TENOR;
	}

	public void setP_TENOR(String p_TENOR) {
		P_TENOR = p_TENOR;
	}

	public String getP_AMT_INSTALLMENT() {
		return P_AMT_INSTALLMENT;
	}

	public void setP_AMT_INSTALLMENT(String p_AMT_INSTALLMENT) {
		P_AMT_INSTALLMENT = p_AMT_INSTALLMENT;
	}

	public String getP_FLAT_RATE() {
		return P_FLAT_RATE;
	}

	public void setP_FLAT_RATE(String p_FLAT_RATE) {
		P_FLAT_RATE = p_FLAT_RATE;
	}

	public String getP_EFF_RATE() {
		return P_EFF_RATE;
	}

	public void setP_EFF_RATE(String p_EFF_RATE) {
		P_EFF_RATE = p_EFF_RATE;
	}

	public String getP_FLAG_ACP() {
		return P_FLAG_ACP;
	}

	public void setP_FLAG_ACP(String p_FLAG_ACP) {
		P_FLAG_ACP = p_FLAG_ACP;
	}

	public String getP_MODE_ACP() {
		return P_MODE_ACP;
	}

	public void setP_MODE_ACP(String p_MODE_ACP) {
		P_MODE_ACP = p_MODE_ACP;
	}

	public String getP_TYPE_INSU() {
		return P_TYPE_INSU;
	}

	public void setP_TYPE_INSU(String p_TYPE_INSU) {
		P_TYPE_INSU = p_TYPE_INSU;
	}

	public String getP_TENOR_AR() {
		return P_TENOR_AR;
	}

	public void setP_TENOR_AR(String p_TENOR_AR) {
		P_TENOR_AR = p_TENOR_AR;
	}

	public String getP_TENOR_TLO() {
		return P_TENOR_TLO;
	}

	public void setP_TENOR_TLO(String p_TENOR_TLO) {
		P_TENOR_TLO = p_TENOR_TLO;
	}

	public String getP_TENOR_CASH() {
		return P_TENOR_CASH;
	}

	public void setP_TENOR_CASH(String p_TENOR_CASH) {
		P_TENOR_CASH = p_TENOR_CASH;
	}

	public String getP_TENOR_KREDIT() {
		return P_TENOR_KREDIT;
	}

	public void setP_TENOR_KREDIT(String p_TENOR_KREDIT) {
		P_TENOR_KREDIT = p_TENOR_KREDIT;
	}

	public String getP_ADMIN_FEE() {
		return P_ADMIN_FEE;
	}

	public void setP_ADMIN_FEE(String p_ADMIN_FEE) {
		P_ADMIN_FEE = p_ADMIN_FEE;
	}

	public String getP_AMT_POLIS() {
		return P_AMT_POLIS;
	}

	public void setP_AMT_POLIS(String p_AMT_POLIS) {
		P_AMT_POLIS = p_AMT_POLIS;
	}

	public String getP_AMT_TJH() {
		return P_AMT_TJH;
	}

	public void setP_AMT_TJH(String p_AMT_TJH) {
		P_AMT_TJH = p_AMT_TJH;
	}

	public String getP_RATE_PDC() {
		return P_RATE_PDC;
	}

	public void setP_RATE_PDC(String p_RATE_PDC) {
		P_RATE_PDC = p_RATE_PDC;
	}
	
}
