package com.mabroor.ws.dto;

public class RentCarCallbackCancelDto {

	private String id;
	
	private String cancellationReasonId;
	
	private String cancellationReason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCancellationReasonId() {
		return cancellationReasonId;
	}

	public void setCancellationReasonId(String cancellationReasonId) {
		this.cancellationReasonId = cancellationReasonId;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
}
