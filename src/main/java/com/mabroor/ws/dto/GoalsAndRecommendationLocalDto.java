package com.mabroor.ws.dto;

public class GoalsAndRecommendationLocalDto {
	
	private String gender;
	private Integer age;
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	

}
