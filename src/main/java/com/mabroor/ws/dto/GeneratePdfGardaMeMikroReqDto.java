package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class GeneratePdfGardaMeMikroReqDto {

	private String name;
	private String nameDepan;
	private String nameBelakang;
	private String periode;
	private BigDecimal totalPertanggungan;
	private BigDecimal premi;
	private BigDecimal santunanMeninggal;
	private BigDecimal santunanKedukaan;
	private BigDecimal santunanPemakaman;
	private String fullName;
	private String birthDate;
	private String phone;
	private String job;
	private String email;
	private String identityNumb;
	private String address;
	private String zipCode;
	private String ahliWaris;
	private String hubunganAhliWaris;
	List<GeneratePdfGardaMeMikroBenefitReqDto> benefits;
	
	public List<GeneratePdfGardaMeMikroBenefitReqDto> getBenefits() {
		return benefits;
	}
	public void setBenefits(List<GeneratePdfGardaMeMikroBenefitReqDto> benefits) {
		this.benefits = benefits;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNameDepan() {
		return nameDepan;
	}
	public void setNameDepan(String nameDepan) {
		this.nameDepan = nameDepan;
	}
	public String getNameBelakang() {
		return nameBelakang;
	}
	public void setNameBelakang(String nameBelakang) {
		this.nameBelakang = nameBelakang;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public BigDecimal getTotalPertanggungan() {
		return totalPertanggungan;
	}
	public void setTotalPertanggungan(BigDecimal totalPertanggungan) {
		this.totalPertanggungan = totalPertanggungan;
	}
	public BigDecimal getPremi() {
		return premi;
	}
	public void setPremi(BigDecimal premi) {
		this.premi = premi;
	}
	public BigDecimal getSantunanMeninggal() {
		return santunanMeninggal;
	}
	public void setSantunanMeninggal(BigDecimal santunanMeninggal) {
		this.santunanMeninggal = santunanMeninggal;
	}
	public BigDecimal getSantunanKedukaan() {
		return santunanKedukaan;
	}
	public void setSantunanKedukaan(BigDecimal santunanKedukaan) {
		this.santunanKedukaan = santunanKedukaan;
	}
	public BigDecimal getSantunanPemakaman() {
		return santunanPemakaman;
	}
	public void setSantunanPemakaman(BigDecimal santunanPemakaman) {
		this.santunanPemakaman = santunanPemakaman;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdentityNumb() {
		return identityNumb;
	}
	public void setIdentityNumb(String identityNumb) {
		this.identityNumb = identityNumb;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getAhliWaris() {
		return ahliWaris;
	}
	public void setAhliWaris(String ahliWaris) {
		this.ahliWaris = ahliWaris;
	}
	public String getHubunganAhliWaris() {
		return hubunganAhliWaris;
	}
	public void setHubunganAhliWaris(String hubunganAhliWaris) {
		this.hubunganAhliWaris = hubunganAhliWaris;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
}
