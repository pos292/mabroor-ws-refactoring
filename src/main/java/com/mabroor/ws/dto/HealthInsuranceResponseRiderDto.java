package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceResponseRiderDto {
	
	@JsonProperty("rider_product_name")
	private String rider_product_name;
	
	@JsonProperty("rider_premium_calculated")
	private String rider_premium_calculated;

	public String getRider_product_name() {
		return rider_product_name;
	}

	public void setRider_product_name(String rider_product_name) {
		this.rider_product_name = rider_product_name;
	}

	public String getRider_premium_calculated() {
		return rider_premium_calculated;
	}

	public void setRider_premium_calculated(String rider_premium_calculated) {
		this.rider_premium_calculated = rider_premium_calculated;
	}

	
	
	

}
