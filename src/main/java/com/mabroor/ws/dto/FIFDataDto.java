package com.mabroor.ws.dto;

public class FIFDataDto {
	
	private Long errorCode;
	
	private String errorMsg;
	
	private FIFDataResultDto result;

	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public FIFDataResultDto getResult() {
		return result;
	}

	public void setResult(FIFDataResultDto result) {
		this.result = result;
	}

}
