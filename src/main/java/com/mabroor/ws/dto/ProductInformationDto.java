package com.mabroor.ws.dto;

import java.util.List;

import com.mabroor.ws.entity.MstMainProduct;

public class ProductInformationDto {

	private Long id;
	
	private MstMainProduct mainProduct;
	
	private String title;
	
	private String image;
	
	private String logoCompany;
	
	private String description;
	
	private String productStatus;
	
	private List <ProductInformationBenefitDto> productBenefit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MstMainProduct getMainProduct() {
		return mainProduct;
	}

	public void setMainProduct(MstMainProduct mainProduct) {
		this.mainProduct = mainProduct;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLogoCompany() {
		return logoCompany;
	}

	public void setLogoCompany(String logoCompany) {
		this.logoCompany = logoCompany;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	public List<ProductInformationBenefitDto> getProductBenefit() {
		return productBenefit;
	}

	public void setProductBenefit(List<ProductInformationBenefitDto> productBenefit) {
		this.productBenefit = productBenefit;
	}
}