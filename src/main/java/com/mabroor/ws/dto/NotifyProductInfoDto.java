package com.mabroor.ws.dto;

public class NotifyProductInfoDto {
	
	private String productType;
	private String accountNumber;
	private String participantId;
	private String participantName;
	private String sidNumber;
	private String sreNumber;
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getParticipantId() {
		return participantId;
	}
	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}
	public String getParticipantName() {
		return participantName;
	}
	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}
	public String getSidNumber() {
		return sidNumber;
	}
	public void setSidNumber(String sidNumber) {
		this.sidNumber = sidNumber;
	}
	public String getSreNumber() {
		return sreNumber;
	}
	public void setSreNumber(String sreNumber) {
		this.sreNumber = sreNumber;
	}
	
	

}
