package com.mabroor.ws.dto;

import java.util.ArrayList;
import java.util.List;

public class MGU4WReservationDetailDto {
	
	private String mstTracExperienceId;
	private String businessUnitId;
	private String unitTypeId;
	private String unitTypeName;
	private String cityId;
	private String cityName;
	private String branchId;
	private ArrayList<String> startDate;
	private ArrayList<String> endDate;
	private String duration;
	private int qtyUnit;
	private int qtyPassenger;
	private boolean isStay;
	private boolean outTown;
	private boolean isExpedition;
	private String contractId;
	private Long contractItemId;
	private String materialId;
	private Long price;
	private boolean isTransmissionManual;
	private int priceExtras;
	private int priceExpedition;
	private String msProductId;
	private String msProductServiceId;
	private String zoneId;
	private String totalDistance;
	private List<PickUpLocationMGU4WDto> pickupLocation;
	private List<DropLocationMGU4WDto> dropLocation;
	private List<PassengerMGU4WDto> passengers;
	public String getMstTracExperienceId() {
		return mstTracExperienceId;
	}
	public void setMstTracExperienceId(String mstTracExperienceId) {
		this.mstTracExperienceId = mstTracExperienceId;
	}
	public String getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(String businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	public String getUnitTypeId() {
		return unitTypeId;
	}
	public void setUnitTypeId(String unitTypeId) {
		this.unitTypeId = unitTypeId;
	}
	public String getUnitTypeName() {
		return unitTypeName;
	}
	public void setUnitTypeName(String unitTypeName) {
		this.unitTypeName = unitTypeName;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public ArrayList<String> getStartDate() {
		return startDate;
	}
	public void setStartDate(ArrayList<String> startDate) {
		this.startDate = startDate;
	}
	public ArrayList<String> getEndDate() {
		return endDate;
	}
	public void setEndDate(ArrayList<String> endDate) {
		this.endDate = endDate;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public int getQtyUnit() {
		return qtyUnit;
	}
	public void setQtyUnit(int qtyUnit) {
		this.qtyUnit = qtyUnit;
	}
	public int getQtyPassenger() {
		return qtyPassenger;
	}
	public void setQtyPassenger(int qtyPassenger) {
		this.qtyPassenger = qtyPassenger;
	}
	public boolean isStay() {
		return isStay;
	}
	public void setStay(boolean isStay) {
		this.isStay = isStay;
	}
	public boolean isOutTown() {
		return outTown;
	}
	public void setOutTown(boolean outTown) {
		this.outTown = outTown;
	}
	public boolean isExpedition() {
		return isExpedition;
	}
	public void setExpedition(boolean isExpedition) {
		this.isExpedition = isExpedition;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public Long getContractItemId() {
		return contractItemId;
	}
	public void setContractItemId(Long contractItemId) {
		this.contractItemId = contractItemId;
	}
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public boolean isTransmissionManual() {
		return isTransmissionManual;
	}
	public void setTransmissionManual(boolean isTransmissionManual) {
		this.isTransmissionManual = isTransmissionManual;
	}
	public int getPriceExtras() {
		return priceExtras;
	}
	public void setPriceExtras(int priceExtras) {
		this.priceExtras = priceExtras;
	}
	public int getPriceExpedition() {
		return priceExpedition;
	}
	public void setPriceExpedition(int priceExpedition) {
		this.priceExpedition = priceExpedition;
	}
	public String getMsProductId() {
		return msProductId;
	}
	public void setMsProductId(String msProductId) {
		this.msProductId = msProductId;
	}
	public String getMsProductServiceId() {
		return msProductServiceId;
	}
	public void setMsProductServiceId(String msProductServiceId) {
		this.msProductServiceId = msProductServiceId;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getTotalDistance() {
		return totalDistance;
	}
	public void setTotalDistance(String totalDistance) {
		this.totalDistance = totalDistance;
	}
	public List<PickUpLocationMGU4WDto> getPickupLocation() {
		return pickupLocation;
	}
	public void setPickupLocation(List<PickUpLocationMGU4WDto> pickupLocation) {
		this.pickupLocation = pickupLocation;
	}
	public List<DropLocationMGU4WDto> getDropLocation() {
		return dropLocation;
	}
	public void setDropLocation(List<DropLocationMGU4WDto> dropLocation) {
		this.dropLocation = dropLocation;
	}
	public List<PassengerMGU4WDto> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<PassengerMGU4WDto> passengers) {
		this.passengers = passengers;
	}
	
	
	

}
