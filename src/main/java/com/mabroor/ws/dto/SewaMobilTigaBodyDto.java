package com.mabroor.ws.dto;

import java.util.List;

public class SewaMobilTigaBodyDto {
	
	private String detailLayanan;
	private List<PackageTanpaDriverDto> packageWithoutDriver;
	private List<PackageDriverDto> packageWithDriver;
	public String getDetailLayanan() {
		return detailLayanan;
	}
	public void setDetailLayanan(String detailLayanan) {
		this.detailLayanan = detailLayanan;
	}
	public List<PackageTanpaDriverDto> getPackageWithoutDriver() {
		return packageWithoutDriver;
	}
	public void setPackageWithoutDriver(List<PackageTanpaDriverDto> packageWithoutDriver) {
		this.packageWithoutDriver = packageWithoutDriver;
	}
	public List<PackageDriverDto> getPackageWithDriver() {
		return packageWithDriver;
	}
	public void setPackageWithDriver(List<PackageDriverDto> packageWithDriver) {
		this.packageWithDriver = packageWithDriver;
	}
	
	

}
