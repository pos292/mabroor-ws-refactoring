package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ReqSubmitPengajuanDto {

    private Double pendapatan;
    private Double jumlahPinjaman;
    private Integer tenor;
    private String kebutuhanPinjaman;
    private String kebutuhanPinjamanLainnya;
    private boolean isKartuKredit;
    private String bankPenerbit;
    private String status;
    private Long userId;
    private String tanggalPengajuan;
    private Long idPengajuan;
    private String jenisKelamin;
    private String namaKtp;
    private String nik;
    private String tempatLahir;
    private String tanggalLahir;
    private String email;
    private String noTlpn;
    private String pekerjaan;
    private String npwp;
    private String alamat;
    private String provinsi;
    private String kotaKabupaten;
    private String kecamatan;
    private String kelurahan;
    private String kodePost;

    public Double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(Double pendapatan) {
        this.pendapatan = pendapatan;
    }

    public Double getJumlahPinjaman() {
        return jumlahPinjaman;
    }

    public void setJumlahPinjaman(Double jumlahPinjaman) {
        this.jumlahPinjaman = jumlahPinjaman;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public String getKebutuhanPinjaman() {
        return kebutuhanPinjaman;
    }

    public void setKebutuhanPinjaman(String kebutuhanPinjaman) {
        this.kebutuhanPinjaman = kebutuhanPinjaman;
    }

    public String getKebutuhanPinjamanLainnya() {
        return kebutuhanPinjamanLainnya;
    }

    public void setKebutuhanPinjamanLainnya(String kebutuhanPinjamanLainnya) {
        this.kebutuhanPinjamanLainnya = kebutuhanPinjamanLainnya;
    }

    @JsonProperty("isKartuKredit")
    public boolean getIsKartuKredit() {
        return isKartuKredit;
    }

    public void setKartuKredit(boolean isKartuKredit) {
        this.isKartuKredit = isKartuKredit;
    }

    public String getBankPenerbit() {
        return bankPenerbit;
    }

    public void setBankPenerbit(String bankPenerbit) {
        this.bankPenerbit = bankPenerbit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTanggalPengajuan() {
        return tanggalPengajuan;
    }

    public void setTanggalPengajuan(String tanggalPengajuan) {
        this.tanggalPengajuan = tanggalPengajuan;
    }

    public Long getIdPengajuan() {
        return idPengajuan;
    }

    public void setIdPengajuan(Long idPengajuan) {
        this.idPengajuan = idPengajuan;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getNamaKtp() {
        return namaKtp;
    }

    public void setNamaKtp(String namaKtp) {
        this.namaKtp = namaKtp;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNoTlpn() {
        return noTlpn;
    }

    public void setNoTlpn(String noTlpn) {
        this.noTlpn = noTlpn;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKotaKabupaten() {
        return kotaKabupaten;
    }

    public void setKotaKabupaten(String kotaKabupaten) {
        this.kotaKabupaten = kotaKabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKodePost() {
        return kodePost;
    }

    public void setKodePost(String kodePost) {
        this.kodePost = kodePost;
    }
}
