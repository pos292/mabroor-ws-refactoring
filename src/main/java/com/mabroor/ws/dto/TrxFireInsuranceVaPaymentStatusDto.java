package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxFireInsuranceVaPaymentStatusDto {
	
	private String status;
	private String message;
	@JsonProperty("SessionTimeOutF")
	private String SessionTimeOutF;
	private String isNeedPayment;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSessionTimeOutF() {
		return SessionTimeOutF;
	}
	public void setSessionTimeOutF(String sessionTimeOutF) {
		SessionTimeOutF = sessionTimeOutF;
	}
	public String getIsNeedPayment() {
		return isNeedPayment;
	}
	public void setIsNeedPayment(String isNeedPayment) {
		this.isNeedPayment = isNeedPayment;
	}
	
	

}
