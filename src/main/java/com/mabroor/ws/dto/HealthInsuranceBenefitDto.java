package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceBenefitDto {
	
	@JsonProperty("title")
	private String title;
	@JsonProperty("description")
	private String description;
	@JsonProperty("component_code")
	private String component_code;
	@JsonProperty("component_status")
	private String component_status;
	@JsonProperty("component_type")
	private String component_type;
	@JsonProperty("value")
	private String value;
	@JsonProperty("icon")
	private String icon;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getComponent_code() {
		return component_code;
	}

	public void setComponent_code(String component_code) {
		this.component_code = component_code;
	}

	public String getComponent_type() {
		return component_type;
	}

	public void setComponent_type(String component_type) {
		this.component_type = component_type;
	}

	public String getComponent_status() {
		return component_status;
	}

	public void setComponent_status(String component_status) {
		this.component_status = component_status;
	}
	
	

}
