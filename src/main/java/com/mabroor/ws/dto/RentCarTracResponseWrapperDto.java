package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RentCarTracResponseWrapperDto {
	@JsonProperty("Status")
	private Integer Status;
	
	@JsonProperty("Message")
	private String Message;
	
	@JsonProperty("Data")
	private RentCarTracResponseDataDto Data;
	
	public Integer getStatus() {
		return Status;
	}
	public void setStatus(Integer status) {
		Status = status;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public RentCarTracResponseDataDto getData() {
		return Data;
	}
	public void setData(RentCarTracResponseDataDto data) {
		Data = data;
	}
	
}
