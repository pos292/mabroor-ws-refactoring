package com.mabroor.ws.dto;

public class ZakatDto {
	
	private String pendapatanPerBulan;
	
	private String tunjanganPerBulan;
	
	private String hargaBeras;
	
	private String cicilanKebutuhanPokok;
	
	private String uangTunai;
	
	private String saham;
	
	private String realEstate;
	
	private String emas;
	
	private String hargaEmas;
	
	private String mobil;
	
	private String hutangPribadi;

	

	public String getPendapatanPerBulan() {
		return pendapatanPerBulan;
	}

	public void setPendapatanPerBulan(String pendapatanPerBulan) {
		this.pendapatanPerBulan = pendapatanPerBulan;
	}

	public String getTunjanganPerBulan() {
		return tunjanganPerBulan;
	}

	public void setTunjanganPerBulan(String tunjanganPerBulan) {
		this.tunjanganPerBulan = tunjanganPerBulan;
	}

	public String getHargaBeras() {
		return hargaBeras;
	}

	public void setHargaBeras(String hargaBeras) {
		this.hargaBeras = hargaBeras;
	}

	public String getCicilanKebutuhanPokok() {
		return cicilanKebutuhanPokok;
	}

	public void setCicilanKebutuhanPokok(String cicilanKebutuhanPokok) {
		this.cicilanKebutuhanPokok = cicilanKebutuhanPokok;
	}

	public String getUangTunai() {
		return uangTunai;
	}

	public void setUangTunai(String uangTunai) {
		this.uangTunai = uangTunai;
	}

	public String getSaham() {
		return saham;
	}

	public void setSaham(String saham) {
		this.saham = saham;
	}

	public String getRealEstate() {
		return realEstate;
	}

	public void setRealEstate(String realEstate) {
		this.realEstate = realEstate;
	}

	public String getEmas() {
		return emas;
	}

	public void setEmas(String emas) {
		this.emas = emas;
	}

	public String getHargaEmas() {
		return hargaEmas;
	}

	public void setHargaEmas(String hargaEmas) {
		this.hargaEmas = hargaEmas;
	}

	public String getMobil() {
		return mobil;
	}

	public void setMobil(String mobil) {
		this.mobil = mobil;
	}

	public String getHutangPribadi() {
		return hutangPribadi;
	}

	public void setHutangPribadi(String hutangPribadi) {
		this.hutangPribadi = hutangPribadi;
	}

	
}
