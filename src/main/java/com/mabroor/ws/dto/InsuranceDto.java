package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;
import com.mabroor.ws.dto.InsuranceCarDto;
import com.mabroor.ws.entity.MstInsuranceQuestion;
import com.mabroor.ws.entity.MstMainProduct;

public class InsuranceDto {
	private Long id;
	private String description;
	private String title;
	private String logoCompany;
	private MstMainProduct mainProduct;
	private String image;
	private String insuranceStatus;
	private List <MstInsuranceQuestion> insuranceQuestion;
	private List <InsuranceCarDto> insuranceProduct;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getInsuranceStatus() {
		return insuranceStatus;
	}
	public void setInsuranceStatus(String insuranceStatus) {
		this.insuranceStatus = insuranceStatus;
	}
	public List<MstInsuranceQuestion> getInsuranceQuestion() {
		return insuranceQuestion;
	}
	public void setInsuranceQuestion(List<MstInsuranceQuestion> insuranceQuestion) {
		this.insuranceQuestion = insuranceQuestion;
	}
	public String getLogoCompany() {
		return logoCompany;
	}
	public void setLogoCompany(String logoCompany) {
		this.logoCompany = logoCompany;
	}
	public MstMainProduct getMainProduct() {
		return mainProduct;
	}
	public void setMainProduct(MstMainProduct mainProduct) {
		this.mainProduct = mainProduct;
	}
	public List<InsuranceCarDto> getInsuranceProduct() {
		return insuranceProduct;
	}
	public void setInsuranceProduct(List<InsuranceCarDto> insuranceProduct) {
		this.insuranceProduct = insuranceProduct;
	}
	
	

	
}
