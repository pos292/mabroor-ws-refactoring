package com.mabroor.ws.dto;

public class GeneratePdfDetailRingkasanPengajuanReqDto {
    private String nama;
    private String produkPembiayaan;
    private String partnerPembiayaan;
    private String pendapatanPerBulan;
    private String jumlahPeminjaman;
    private String tenor;
    private String kebutuhanPeminjaman;
    private String memilikiKartuKredit;
    private String bankPenerbit;
    private String jenisKelamin;
    private String namaSesuaiKtp;
    private String nik;
    private String tempatLahir;
    private String tanggalLahir;
    private String email;
    private String nomorPonsel;
    private String pekerjaan;
    private String nomorNpwp;
    private String alamatLengkap;
    private String provinsi;
    private String kota;
    private String kelurahan;
    private String kecamatan;
    private String kodePos;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getProdukPembiayaan() {
        return produkPembiayaan;
    }

    public void setProdukPembiayaan(String produkPembiayaan) {
        this.produkPembiayaan = produkPembiayaan;
    }

    public String getPartnerPembiayaan() {
        return partnerPembiayaan;
    }

    public void setPartnerPembiayaan(String partnerPembiayaan) {
        this.partnerPembiayaan = partnerPembiayaan;
    }

    public String getPendapatanPerBulan() {
        return pendapatanPerBulan;
    }

    public void setPendapatanPerBulan(String pendapatanPerBulan) {
        this.pendapatanPerBulan = pendapatanPerBulan;
    }

    public String getJumlahPeminjaman() {
        return jumlahPeminjaman;
    }

    public void setJumlahPeminjaman(String jumlahPeminjaman) {
        this.jumlahPeminjaman = jumlahPeminjaman;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getKebutuhanPeminjaman() {
        return kebutuhanPeminjaman;
    }

    public void setKebutuhanPeminjaman(String kebutuhanPeminjaman) {
        this.kebutuhanPeminjaman = kebutuhanPeminjaman;
    }

    public String getMemilikiKartuKredit() {
        return memilikiKartuKredit;
    }

    public void setMemilikiKartuKredit(String memilikiKartuKredit) {
        this.memilikiKartuKredit = memilikiKartuKredit;
    }

    public String getBankPenerbit() {
        return bankPenerbit;
    }

    public void setBankPenerbit(String bankPenerbit) {
        this.bankPenerbit = bankPenerbit;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getNamaSesuaiKtp() {
        return namaSesuaiKtp;
    }

    public void setNamaSesuaiKtp(String namaSesuaiKtp) {
        this.namaSesuaiKtp = namaSesuaiKtp;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomorPonsel() {
        return nomorPonsel;
    }

    public void setNomorPonsel(String nomorPonsel) {
        this.nomorPonsel = nomorPonsel;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getNomorNpwp() {
        return nomorNpwp;
    }

    public void setNomorNpwp(String nomorNpwp) {
        this.nomorNpwp = nomorNpwp;
    }

    public String getAlamatLengkap() {
        return alamatLengkap;
    }

    public void setAlamatLengkap(String alamatLengkap) {
        this.alamatLengkap = alamatLengkap;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }
}
