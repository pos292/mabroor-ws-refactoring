package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DoaDto {

	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("nama_doa")
	private String namaDoa;
	
	@JsonProperty("doa")
	private String doa;
	
	@JsonProperty("bunyi")
	private String bunyi;
	
	@JsonProperty("arti")
	private String arti;
	
	@JsonProperty("created_at")
	private String createdAt;
	
	@JsonProperty("updated_at")
	private String updatedAt;
	
	@JsonProperty("id_category")
	private Long idCategory;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamaDoa() {
		return namaDoa;
	}

	public void setNamaDoa(String namaDoa) {
		this.namaDoa = namaDoa;
	}

	public String getDoa() {
		return doa;
	}

	public void setDoa(String doa) {
		this.doa = doa;
	}

	public String getBunyi() {
		return bunyi;
	}

	public void setBunyi(String bunyi) {
		this.bunyi = bunyi;
	}

	public String getArti() {
		return arti;
	}

	public void setArti(String arti) {
		this.arti = arti;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}
}
