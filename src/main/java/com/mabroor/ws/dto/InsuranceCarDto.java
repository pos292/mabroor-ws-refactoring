package com.mabroor.ws.dto;

import java.util.List;

import com.mabroor.ws.entity.MstGeneralInsuranceCarProductBenefit;

public class InsuranceCarDto {
	
	private Long id;
	private String titleProduct;
	private String backgroundImage;
	private String productCode;
	private String kontribusi;
	private List <MstGeneralInsuranceCarProductBenefit> carProductBenefit;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitleProduct() {
		return titleProduct;
	}
	public void setTitleProduct(String titleProduct) {
		this.titleProduct = titleProduct;
	}
	public String getBackgroundImage() {
		return backgroundImage;
	}
	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}
	public List<MstGeneralInsuranceCarProductBenefit> getCarProductBenefit() {
		return carProductBenefit;
	}
	public void setCarProductBenefit(List<MstGeneralInsuranceCarProductBenefit> carProductBenefit) {
		this.carProductBenefit = carProductBenefit;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getKontribusi() {
		return kontribusi;
	}
	public void setKontribusi(String kontribusi) {
		this.kontribusi = kontribusi;
	}
}
