package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SportInsuranceProductRequestDto {
	
	@JsonProperty("product_code")
	private String product_code;
	
	@JsonProperty("sum_assured_list")
	private List<BigDecimal> sum_assured_list;
	
	@JsonProperty("terms")
	private List<BigDecimal> terms;

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public List<BigDecimal> getSum_assured_list() {
		return sum_assured_list;
	}

	public void setSum_assured_list(List<BigDecimal> sum_assured_list) {
		this.sum_assured_list = sum_assured_list;
	}

	public List<BigDecimal> getTerms() {
		return terms;
	}

	public void setTerms(List<BigDecimal> terms) {
		this.terms = terms;
	}

}
