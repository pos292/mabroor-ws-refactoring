package com.mabroor.ws.dto;

public class TrxCarInsuranceAstrapayPaymentDataDto {
	
	private String failure;
	private String trxId;
	private String trxNumberMerchant;
	private String status;
	public String getFailure() {
		return failure;
	}
	public void setFailure(String failure) {
		this.failure = failure;
	}
	public String getTrxId() {
		return trxId;
	}
	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}
	public String getTrxNumberMerchant() {
		return trxNumberMerchant;
	}
	public void setTrxNumberMerchant(String trxNumberMerchant) {
		this.trxNumberMerchant = trxNumberMerchant;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
