package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JamCreditSimulationTaksasiRequestDto {

	private String E_NOREG;
	
	private String NO_REGISTRATION;
	
	private String CD_SP;
	
	private String TYPE_DOC;
	
	private String UPLOAD_NAME;
	
	private String FILE_NAME;
	
	private String RAW_FILE;
	
	private String DT_UPLOAD;
	
	private String ID_USER_UPLOAD;
	
	private String LOCATION_FILE;
	
	private String ID_USER;
	
	private String TYPE_IMAGE;
	
	private String RESERVED_1;
	
	private String RESERVED_2;
	
	private String RESERVED_3;
	
	private String RESERVED_4;
	
	private String RESERVED_5;
	
	private String ID_UNIT;

	

	public String getE_NOREG() {
		return E_NOREG;
	}

	public void setE_NOREG(String e_NOREG) {
		E_NOREG = e_NOREG;
	}

	public String getNO_REGISTRATION() {
		return NO_REGISTRATION;
	}

	public void setNO_REGISTRATION(String nO_REGISTRATION) {
		NO_REGISTRATION = nO_REGISTRATION;
	}

	public String getCD_SP() {
		return CD_SP;
	}

	public void setCD_SP(String cD_SP) {
		CD_SP = cD_SP;
	}

	public String getTYPE_DOC() {
		return TYPE_DOC;
	}

	public void setTYPE_DOC(String tYPE_DOC) {
		TYPE_DOC = tYPE_DOC;
	}

	public String getUPLOAD_NAME() {
		return UPLOAD_NAME;
	}

	public void setUPLOAD_NAME(String uPLOAD_NAME) {
		UPLOAD_NAME = uPLOAD_NAME;
	}

	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String fILE_NAME) {
		FILE_NAME = fILE_NAME;
	}

	public String getRAW_FILE() {
		return RAW_FILE;
	}

	public void setRAW_FILE(String rAW_FILE) {
		RAW_FILE = rAW_FILE;
	}

	public String getDT_UPLOAD() {
		return DT_UPLOAD;
	}

	public void setDT_UPLOAD(String dT_UPLOAD) {
		DT_UPLOAD = dT_UPLOAD;
	}

	public String getID_USER_UPLOAD() {
		return ID_USER_UPLOAD;
	}

	public void setID_USER_UPLOAD(String iD_USER_UPLOAD) {
		ID_USER_UPLOAD = iD_USER_UPLOAD;
	}

	public String getLOCATION_FILE() {
		return LOCATION_FILE;
	}

	public void setLOCATION_FILE(String lOCATION_FILE) {
		LOCATION_FILE = lOCATION_FILE;
	}

	public String getID_USER() {
		return ID_USER;
	}

	public void setID_USER(String iD_USER) {
		ID_USER = iD_USER;
	}

	public String getTYPE_IMAGE() {
		return TYPE_IMAGE;
	}

	public void setTYPE_IMAGE(String tYPE_IMAGE) {
		TYPE_IMAGE = tYPE_IMAGE;
	}

	public String getRESERVED_1() {
		return RESERVED_1;
	}

	public void setRESERVED_1(String rESERVED_1) {
		RESERVED_1 = rESERVED_1;
	}

	public String getRESERVED_2() {
		return RESERVED_2;
	}

	public void setRESERVED_2(String rESERVED_2) {
		RESERVED_2 = rESERVED_2;
	}

	public String getRESERVED_3() {
		return RESERVED_3;
	}

	public void setRESERVED_3(String rESERVED_3) {
		RESERVED_3 = rESERVED_3;
	}

	public String getRESERVED_4() {
		return RESERVED_4;
	}

	public void setRESERVED_4(String rESERVED_4) {
		RESERVED_4 = rESERVED_4;
	}

	public String getRESERVED_5() {
		return RESERVED_5;
	}

	public void setRESERVED_5(String rESERVED_5) {
		RESERVED_5 = rESERVED_5;
	}

	public String getID_UNIT() {
		return ID_UNIT;
	}

	public void setID_UNIT(String iD_UNIT) {
		ID_UNIT = iD_UNIT;
	}

	
	
}
