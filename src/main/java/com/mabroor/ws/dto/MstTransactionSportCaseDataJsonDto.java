package com.mabroor.ws.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MstTransactionSportCaseDataJsonDto {
	
	@JsonProperty("agent_code")
	private String agent_code;
	
	@JsonProperty("agent_channel")
	private String agent_channel;
	
	@JsonProperty("channel")
	private String channel;
	
	@JsonProperty("sales_channel")
	private String sales_channel;
	
	
	@JsonProperty("benef_info")
	private List<String> benef_info;
	
	@JsonProperty("caseCreateDate")
	private String caseCreateDate;
	
	@JsonProperty("healthDeclaration")
	private String healthDeclaration;
	
	@JsonProperty("life_profiles")
	private List<SportLifeProfileDto> life_profiles;
	
	@JsonProperty("quotation")
	private SportQuotationDto quotation;
	
	@JsonProperty("sign_info")
	private SportSignInfoDto sign_info;

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getAgent_channel() {
		return agent_channel;
	}

	public void setAgent_channel(String agent_channel) {
		this.agent_channel = agent_channel;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getSales_channel() {
		return sales_channel;
	}

	public void setSales_channel(String sales_channel) {
		this.sales_channel = sales_channel;
	}

	public List<String> getBenef_info() {
		return benef_info;
	}

	public void setBenef_info(List<String> benef_info) {
		this.benef_info = benef_info;
	}

	public String getCaseCreateDate() {
		return caseCreateDate;
	}

	public void setCaseCreateDate(String caseCreateDate) {
		this.caseCreateDate = caseCreateDate;
	}

	public String getHealthDeclaration() {
		return healthDeclaration;
	}

	public void setHealthDeclaration(String healthDeclaration) {
		this.healthDeclaration = healthDeclaration;
	}

	public List<SportLifeProfileDto> getLife_profiles() {
		return life_profiles;
	}

	public void setLife_profiles(List<SportLifeProfileDto> life_profiles) {
		this.life_profiles = life_profiles;
	}

	public SportQuotationDto getQuotation() {
		return quotation;
	}

	public void setQuotation(SportQuotationDto quotation) {
		this.quotation = quotation;
	}

	public SportSignInfoDto getSign_info() {
		return sign_info;
	}

	public void setSign_info(SportSignInfoDto sign_info) {
		this.sign_info = sign_info;
	}
}
