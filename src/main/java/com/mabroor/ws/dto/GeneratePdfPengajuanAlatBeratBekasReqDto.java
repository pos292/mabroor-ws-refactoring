package com.mabroor.ws.dto;

public class GeneratePdfPengajuanAlatBeratBekasReqDto {

	private String name;
	private String sektor;
	private String brand;
	private String tipe;
	private String model;
	private String daerahPemakaian;
	private String rentangTahun;
	private String kondisi;
	private String waktuPenggunaan;
	private String caraPembayaran;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSektor() {
		return sektor;
	}
	public void setSektor(String sektor) {
		this.sektor = sektor;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getDaerahPemakaian() {
		return daerahPemakaian;
	}
	public void setDaerahPemakaian(String daerahPemakaian) {
		this.daerahPemakaian = daerahPemakaian;
	}
	public String getRentangTahun() {
		return rentangTahun;
	}
	public void setRentangTahun(String rentangTahun) {
		this.rentangTahun = rentangTahun;
	}
	public String getKondisi() {
		return kondisi;
	}
	public void setKondisi(String kondisi) {
		this.kondisi = kondisi;
	}
	public String getWaktuPenggunaan() {
		return waktuPenggunaan;
	}
	public void setWaktuPenggunaan(String waktuPenggunaan) {
		this.waktuPenggunaan = waktuPenggunaan;
	}
	public String getCaraPembayaran() {
		return caraPembayaran;
	}
	public void setCaraPembayaran(String caraPembayaran) {
		this.caraPembayaran = caraPembayaran;
	}
	
}
