package com.mabroor.ws.dto;

public class CalculateHajiAccReqDto {
    private String productName;
    private String jumlahPeserta;
    private String tenor;
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getJumlahPeserta() {
        return jumlahPeserta;
    }
    public void setJumlahPeserta(String jumlahPeserta) {
        this.jumlahPeserta = jumlahPeserta;
    }
    public String getTenor() {
        return tenor;
    }
    public void setTenor(String tenor) {
        this.tenor = tenor;
    }
}
