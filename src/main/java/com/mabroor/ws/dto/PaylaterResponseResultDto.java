package com.mabroor.ws.dto;

import java.util.List;

public class PaylaterResponseResultDto {
	
	private List<PayLaterResponseOrderDto> data;
	private Long total;
	public List<PayLaterResponseOrderDto> getData() {
		return data;
	}
	public void setData(List<PayLaterResponseOrderDto> data) {
		this.data = data;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	
	

}
