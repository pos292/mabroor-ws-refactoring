package com.mabroor.ws.dto;

import java.math.BigDecimal;

public class GeneratePdfKurbanReqDto {

	private String pilihanHewan;
	
	private String jumlahHewan;
	
	private String pilihanPartner;
	
	private String name;
	
	private BigDecimal hargaAkhir;

	public String getPilihanHewan() {
		return pilihanHewan;
	}

	public void setPilihanHewan(String pilihanHewan) {
		this.pilihanHewan = pilihanHewan;
	}

	public String getJumlahHewan() {
		return jumlahHewan;
	}

	public void setJumlahHewan(String jumlahHewan) {
		this.jumlahHewan = jumlahHewan;
	}

	public String getPilihanPartner() {
		return pilihanPartner;
	}

	public void setPilihanPartner(String pilihanPartner) {
		this.pilihanPartner = pilihanPartner;
	}

	public BigDecimal getHargaAkhir() {
		return hargaAkhir;
	}

	public void setHargaAkhir(BigDecimal hargaAkhir) {
		this.hargaAkhir = hargaAkhir;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
