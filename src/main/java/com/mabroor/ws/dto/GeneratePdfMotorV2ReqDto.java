package com.mabroor.ws.dto;

public class GeneratePdfMotorV2ReqDto {

	private String dp;
	private String model;
	private String category;
	private String type;
	private String jenisLayanan;
	private String name;
	private String tenor;
	private String existingCustomer;
	private String contractNumber;
	private String homeOwnership;
	private String fifgroupCardnumber;
	private String maritalStatus;
	private String lastEducation;
	private String dependents;
	private String gender;
	private String firstName;
	private String lastName;
	private String birthday;
	private String phoneDataUser;
	private String phoneDataUserOptional;
	private String email;
	private String ktpNumber;
	private String address;
	private String province;
	private String city;
	private String subDistrict;
	private String village;
	private String zipCode;
	
	public String getDp() {
		return dp;
	}
	public void setDp(String dp) {
		this.dp = dp;
	}
	public String getJenisLayanan() {
		return jenisLayanan;
	}
	public void setJenisLayanan(String jenisLayanan) {
		this.jenisLayanan = jenisLayanan;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getFifgroupCardnumber() {
		return fifgroupCardnumber;
	}
	public void setFifgroupCardnumber(String fifgroupCardnumber) {
		this.fifgroupCardnumber = fifgroupCardnumber;
	}
	public String getExistingCustomer() {
		return existingCustomer;
	}
	public void setExistingCustomer(String existingCustomer) {
		this.existingCustomer = existingCustomer;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getHomeOwnership() {
		return homeOwnership;
	}
	public void setHomeOwnership(String homeOwnership) {
		this.homeOwnership = homeOwnership;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getLastEducation() {
		return lastEducation;
	}
	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}
	public String getDependents() {
		return dependents;
	}
	public void setDependents(String dependents) {
		this.dependents = dependents;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getPhoneDataUser() {
		return phoneDataUser;
	}
	public void setPhoneDataUser(String phoneDataUser) {
		this.phoneDataUser = phoneDataUser;
	}
	public String getPhoneDataUserOptional() {
		return phoneDataUserOptional;
	}
	public void setPhoneDataUserOptional(String phoneDataUserOptional) {
		this.phoneDataUserOptional = phoneDataUserOptional;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getKtpNumber() {
		return ktpNumber;
	}
	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getSubDistrict() {
		return subDistrict;
	}
	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}
