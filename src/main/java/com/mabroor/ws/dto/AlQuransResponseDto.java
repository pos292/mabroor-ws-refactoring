package com.mabroor.ws.dto;

import java.util.List;

import com.mabroor.ws.entity.MstAlQuran;

public class AlQuransResponseDto {

	private List<MstAlQuran> surah;
	private List<MstAlQuran> juz;
	public List<MstAlQuran> getSurah() {
		return surah;
	}
	public void setSurah(List<MstAlQuran> surah) {
		this.surah = surah;
	}
	public List<MstAlQuran> getJuz() {
		return juz;
	}
	public void setJuz(List<MstAlQuran> juz) {
		this.juz = juz;
	}
	
	
}
