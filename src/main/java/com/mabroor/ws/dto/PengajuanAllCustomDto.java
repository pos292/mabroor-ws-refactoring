package com.mabroor.ws.dto;

import java.util.Date;

public class PengajuanAllCustomDto {
	private Long leadsId;
	private Long variable1;
	private String variable2;
	private Date variable3;
	private String variable4;
	private String variable5;
	private String variable6;
	private String variable7;
	
	public Long getLeadsId() {
		return leadsId;
	}
	public void setLeadsId(Long leadsId) {
		this.leadsId = leadsId;
	}
	public Long getVariable1() {
		return variable1;
	}
	public void setVariable1(Long variable1) {
		this.variable1 = variable1;
	}
	public String getVariable2() {
		return variable2;
	}
	public void setVariable2(String variable2) {
		this.variable2 = variable2;
	}
	public Date getVariable3() {
		return variable3;
	}
	public void setVariable3(Date variable3) {
		this.variable3 = variable3;
	}
	public String getVariable4() {
		return variable4;
	}
	public void setVariable4(String variable4) {
		this.variable4 = variable4;
	}
	public String getVariable5() {
		return variable5;
	}
	public void setVariable5(String variable5) {
		this.variable5 = variable5;
	}
	public String getVariable6() {
		return variable6;
	}
	public void setVariable6(String variable6) {
		this.variable6 = variable6;
	}
	public String getVariable7() {
		return variable7;
	}
	public void setVariable7(String variable7) {
		this.variable7 = variable7;
	}
	public PengajuanAllCustomDto(Long leadsId, Long variable1, String variable2, Date variable3, String variable4,
			String variable5, String variable6, String variable7) {
		super();
		this.leadsId = leadsId;
		this.variable1 = variable1;
		this.variable2 = variable2;
		this.variable3 = variable3;
		this.variable4 = variable4;
		this.variable5 = variable5;
		this.variable6 = variable6;
		this.variable7 = variable7;
	}
	
}
