package com.mabroor.ws.dto;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.List;

@Data
public class ResFifAmitraListDto {
    private String errorCode;
    private String errorMsg;
    private List<LinkedHashMap<String, String>> result;
}
