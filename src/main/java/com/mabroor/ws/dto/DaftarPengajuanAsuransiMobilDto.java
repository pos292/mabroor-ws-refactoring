package com.mabroor.ws.dto;

import java.util.Date;
import java.util.List;

public class DaftarPengajuanAsuransiMobilDto {

    private Long id;	
    
	private Long userId;	
	
	private Date transactionDateTime;
	
	private String name;
	
	private String email;
	
	private String phone;
	
	private String nameDataUser;
	
	private String emailDataUser;
	
	private String phoneDataUser;
	
	private String policyHolderName;
	
	private String policyHolderLastName;
	
	private String policyHolderEmail;
	
	private String policyHolderPhone;
	
	private String policyHolderBirthdate;
	
	private String policyHolderIdentityNumber;
	
	private String policyHolderIdentityAddress;
	
	private String policyHolderZipcode;
	
	private String policyPeriod;
	
	private String premi;
	
	private String hargaPertanggungan;
	
	private String usage;
	
	private String chassisNumber;

	private String machineNumber;

	private String totalTransaction;
	
	private String sumIssured;
	
	private String kontribusiDasar;
	
	private String periodePerlindungan;
	
	private String kecelakaanDiriPengemudi;
	
	private String estimasiTotalKontribusi;
	
	private String productTitle;

	private String jenisAsuransi;
	
	private String kendaraan;
	
	private String kodeWilayahPlat;

    private String kodePromo;

    private String namaPromo;
	
	private String encryptionStatus;
	
//	private List<DaftarPengajuanAsuransiBenefitDto> benefit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNameDataUser() {
		return nameDataUser;
	}

	public void setNameDataUser(String nameDataUser) {
		this.nameDataUser = nameDataUser;
	}

	public String getEmailDataUser() {
		return emailDataUser;
	}

	public void setEmailDataUser(String emailDataUser) {
		this.emailDataUser = emailDataUser;
	}

	public String getPhoneDataUser() {
		return phoneDataUser;
	}

	public void setPhoneDataUser(String phoneDataUser) {
		this.phoneDataUser = phoneDataUser;
	}

	public String getPolicyHolderName() {
		return policyHolderName;
	}

	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}

	public String getPolicyHolderLastName() {
		return policyHolderLastName;
	}

	public void setPolicyHolderLastName(String policyHolderLastName) {
		this.policyHolderLastName = policyHolderLastName;
	}

	public String getPolicyHolderEmail() {
		return policyHolderEmail;
	}

	public void setPolicyHolderEmail(String policyHolderEmail) {
		this.policyHolderEmail = policyHolderEmail;
	}

	public String getPolicyHolderPhone() {
		return policyHolderPhone;
	}

	public void setPolicyHolderPhone(String policyHolderPhone) {
		this.policyHolderPhone = policyHolderPhone;
	}

	public String getPolicyHolderBirthdate() {
		return policyHolderBirthdate;
	}

	public void setPolicyHolderBirthdate(String policyHolderBirthdate) {
		this.policyHolderBirthdate = policyHolderBirthdate;
	}

	public String getPolicyHolderIdentityNumber() {
		return policyHolderIdentityNumber;
	}

	public void setPolicyHolderIdentityNumber(String policyHolderIdentityNumber) {
		this.policyHolderIdentityNumber = policyHolderIdentityNumber;
	}

	public String getPolicyHolderIdentityAddress() {
		return policyHolderIdentityAddress;
	}

	public void setPolicyHolderIdentityAddress(String policyHolderIdentityAddress) {
		this.policyHolderIdentityAddress = policyHolderIdentityAddress;
	}

	public String getPolicyHolderZipcode() {
		return policyHolderZipcode;
	}

	public void setPolicyHolderZipcode(String policyHolderZipcode) {
		this.policyHolderZipcode = policyHolderZipcode;
	}

	public String getPolicyPeriod() {
		return policyPeriod;
	}

	public void setPolicyPeriod(String policyPeriod) {
		this.policyPeriod = policyPeriod;
	}

	public String getPremi() {
		return premi;
	}

	public void setPremi(String premi) {
		this.premi = premi;
	}

	public String getHargaPertanggungan() {
		return hargaPertanggungan;
	}

	public void setHargaPertanggungan(String hargaPertanggungan) {
		this.hargaPertanggungan = hargaPertanggungan;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getChassisNumber() {
		return chassisNumber;
	}

	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}

	public String getMachineNumber() {
		return machineNumber;
	}

	public void setMachineNumber(String machineNumber) {
		this.machineNumber = machineNumber;
	}

	public String getTotalTransaction() {
		return totalTransaction;
	}

	public void setTotalTransaction(String totalTransaction) {
		this.totalTransaction = totalTransaction;
	}

	public String getSumIssured() {
		return sumIssured;
	}

	public void setSumIssured(String sumIssured) {
		this.sumIssured = sumIssured;
	}

	public String getKontribusiDasar() {
		return kontribusiDasar;
	}

	public void setKontribusiDasar(String kontribusiDasar) {
		this.kontribusiDasar = kontribusiDasar;
	}

	public String getKecelakaanDiriPengemudi() {
		return kecelakaanDiriPengemudi;
	}

	public void setKecelakaanDiriPengemudi(String kecelakaanDiriPengemudi) {
		this.kecelakaanDiriPengemudi = kecelakaanDiriPengemudi;
	}

	public String getEstimasiTotalKontribusi() {
		return estimasiTotalKontribusi;
	}

	public void setEstimasiTotalKontribusi(String estimasiTotalKontribusi) {
		this.estimasiTotalKontribusi = estimasiTotalKontribusi;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getJenisAsuransi() {
		return jenisAsuransi;
	}

	public void setJenisAsuransi(String jenisAsuransi) {
		this.jenisAsuransi = jenisAsuransi;
	}

	public String getKendaraan() {
		return kendaraan;
	}

	public void setKendaraan(String kendaraan) {
		this.kendaraan = kendaraan;
	}

	public String getKodeWilayahPlat() {
		return kodeWilayahPlat;
	}

	public void setKodeWilayahPlat(String kodeWilayahPlat) {
		this.kodeWilayahPlat = kodeWilayahPlat;
	}

	public String getKodePromo() {
		return kodePromo;
	}

	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}

	public String getNamaPromo() {
		return namaPromo;
	}

	public void setNamaPromo(String namaPromo) {
		this.namaPromo = namaPromo;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}

	public String getPeriodePerlindungan() {
		return periodePerlindungan;
	}

	public void setPeriodePerlindungan(String periodePerlindungan) {
		this.periodePerlindungan = periodePerlindungan;
	}
}
