package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FlexiHealthLifeProfileContractStructureDto {
	
	@JsonProperty("component_code")
	private String component_code;
	
	@JsonProperty("sum_assured")
	private String sum_assured;

	public String getComponent_code() {
		return component_code;
	}

	public void setComponent_code(String component_code) {
		this.component_code = component_code;
	}

	public String getSum_assured() {
		return sum_assured;
	}

	public void setSum_assured(String sum_assured) {
		this.sum_assured = sum_assured;
	}
	
	

}
