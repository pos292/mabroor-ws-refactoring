package com.mabroor.ws.dto;


import java.util.List;

public class DictionaryDto {

	private List<DictionaryReqDto> A;
	private List<DictionaryReqDto> B;
	private List<DictionaryReqDto> C;
	private List<DictionaryReqDto> D;
	private List<DictionaryReqDto> E;
	private List<DictionaryReqDto> F;
	private List<DictionaryReqDto> G;
	private List<DictionaryReqDto> H;
	private List<DictionaryReqDto> I;
	private List<DictionaryReqDto> J;
	private List<DictionaryReqDto> K;
	private List<DictionaryReqDto> L;
	private List<DictionaryReqDto> M;
	private List<DictionaryReqDto> N;
	private List<DictionaryReqDto> O;
	private List<DictionaryReqDto> P;
	private List<DictionaryReqDto> Q;
	private List<DictionaryReqDto> R;
	private List<DictionaryReqDto> S;
	private List<DictionaryReqDto> T;
	private List<DictionaryReqDto> U;
	private List<DictionaryReqDto> V;
	private List<DictionaryReqDto> W;
	private List<DictionaryReqDto> X;
	private List<DictionaryReqDto> Y;
	private List<DictionaryReqDto> Z;
	
	public List<DictionaryReqDto> getA() {
		return A;
	}
	public void setA(List<DictionaryReqDto> a) {
		A = a;
	}
	public List<DictionaryReqDto> getB() {
		return B;
	}
	public void setB(List<DictionaryReqDto> b) {
		B = b;
	}
	public List<DictionaryReqDto> getC() {
		return C;
	}
	public void setC(List<DictionaryReqDto> c) {
		C = c;
	}
	public List<DictionaryReqDto> getD() {
		return D;
	}
	public void setD(List<DictionaryReqDto> d) {
		D = d;
	}
	public List<DictionaryReqDto> getE() {
		return E;
	}
	public void setE(List<DictionaryReqDto> e) {
		E = e;
	}
	public List<DictionaryReqDto> getF() {
		return F;
	}
	public void setF(List<DictionaryReqDto> f) {
		F = f;
	}
	public List<DictionaryReqDto> getG() {
		return G;
	}
	public void setG(List<DictionaryReqDto> g) {
		G = g;
	}
	public List<DictionaryReqDto> getH() {
		return H;
	}
	public void setH(List<DictionaryReqDto> h) {
		H = h;
	}
	public List<DictionaryReqDto> getI() {
		return I;
	}
	public void setI(List<DictionaryReqDto> i) {
		I = i;
	}
	public List<DictionaryReqDto> getJ() {
		return J;
	}
	public void setJ(List<DictionaryReqDto> j) {
		J = j;
	}
	public List<DictionaryReqDto> getK() {
		return K;
	}
	public void setK(List<DictionaryReqDto> k) {
		K = k;
	}
	public List<DictionaryReqDto> getL() {
		return L;
	}
	public void setL(List<DictionaryReqDto> l) {
		L = l;
	}
	public List<DictionaryReqDto> getM() {
		return M;
	}
	public void setM(List<DictionaryReqDto> m) {
		M = m;
	}
	public List<DictionaryReqDto> getN() {
		return N;
	}
	public void setN(List<DictionaryReqDto> n) {
		N = n;
	}
	public List<DictionaryReqDto> getO() {
		return O;
	}
	public void setO(List<DictionaryReqDto> o) {
		O = o;
	}
	public List<DictionaryReqDto> getP() {
		return P;
	}
	public void setP(List<DictionaryReqDto> p) {
		P = p;
	}
	public List<DictionaryReqDto> getQ() {
		return Q;
	}
	public void setQ(List<DictionaryReqDto> q) {
		Q = q;
	}
	public List<DictionaryReqDto> getR() {
		return R;
	}
	public void setR(List<DictionaryReqDto> r) {
		R = r;
	}
	public List<DictionaryReqDto> getS() {
		return S;
	}
	public void setS(List<DictionaryReqDto> s) {
		S = s;
	}
	public List<DictionaryReqDto> getT() {
		return T;
	}
	public void setT(List<DictionaryReqDto> t) {
		T = t;
	}
	public List<DictionaryReqDto> getU() {
		return U;
	}
	public void setU(List<DictionaryReqDto> u) {
		U = u;
	}
	public List<DictionaryReqDto> getV() {
		return V;
	}
	public void setV(List<DictionaryReqDto> v) {
		V = v;
	}
	public List<DictionaryReqDto> getW() {
		return W;
	}
	public void setW(List<DictionaryReqDto> w) {
		W = w;
	}
	public List<DictionaryReqDto> getX() {
		return X;
	}
	public void setX(List<DictionaryReqDto> x) {
		X = x;
	}
	public List<DictionaryReqDto> getY() {
		return Y;
	}
	public void setY(List<DictionaryReqDto> y) {
		Y = y;
	}
	public List<DictionaryReqDto> getZ() {
		return Z;
	}
	public void setZ(List<DictionaryReqDto> z) {
		Z = z;
	}

}
