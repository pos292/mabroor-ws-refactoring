package com.mabroor.ws.dto;

public class RoundrobinDto {
	
	private String kotaPengajuan;
	private String brand;
	private String type;
	
	public String getKotaPengajuan() {
		return kotaPengajuan;
	}
	public void setKotaPengajuan(String kotaPengajuan) {
		this.kotaPengajuan = kotaPengajuan;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
