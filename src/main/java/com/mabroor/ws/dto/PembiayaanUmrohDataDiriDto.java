package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.*;


@AllArgsConstructor
@NoArgsConstructor
public class PembiayaanUmrohDataDiriDto {
    @NotBlank(message = "namaDepan tidak boleh kosong")
    private String namaDepan;
    @NotBlank(message = "namaBelakang tidak boleh kosong")
    private String namaBelakang;
    @NotBlank(message = "jenisKelamin tidak boleh kosong")
    private String jenisKelamin;
    @NotBlank(message = "tempatLahir tidak boleh kosong")
    @Size(max = 50)
    private String tempatLahir;
    @NotNull(message = "tanggalLahir tidak boleh kosong")
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date tanggalLahir;
    @NotBlank(message = "ibuKandung tidak boleh kosong")
    @Size(max = 50)
    private String ibuKandung;
    @NotBlank(message = "nomorTlpn tidak boleh kosong")
    @Size(max = 15)
    private String nomorTlpn;
    @Email(message = "Format Email belum sesuai")
    @Size(max = 95, message = "Email tidak boleh lebih dari 95")
    @NotBlank(message = "email tidak boleh kosong")
    private String email;
    @Size(max = 16, message = "NIK tidak boleh lebih dari 16")
    @NotBlank(message = "nik tidak boleh kosong")
    private String nik;

    public String getNamaDepan() {
        return namaDepan;
    }

    public void setNamaDepan(String namaDepan) {
        this.namaDepan = namaDepan;
    }

    public String getNamaBelakang() {
        return namaBelakang;
    }

    public void setNamaBelakang(String namaBelakang) {
        this.namaBelakang = namaBelakang;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getIbuKandung() {
        return ibuKandung;
    }

    public void setIbuKandung(String ibuKandung) {
        this.ibuKandung = ibuKandung;
    }

    public String getNomorTlpn() {
        return nomorTlpn;
    }

    public void setNomorTlpn(String nomorTlpn) {
        this.nomorTlpn = nomorTlpn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }
}
