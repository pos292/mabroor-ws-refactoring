package com.mabroor.ws.dto;

public class RespTenorDto {

    private String listTenor;

    public String getListTenor() {
        return listTenor;
    }

    public void setListTenor(String listTenor) {
        this.listTenor = listTenor;
    }
}
