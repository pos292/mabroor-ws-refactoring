package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthBuddiesRequestJsonDto {
	
	@JsonProperty("eRefNo")
	private String eRefNo;
	
	@JsonProperty("totalAttachment")
	private String totalAttachment;
	
	@JsonProperty("attachmentList")
	private List<MstAttachmentListDto> attachmentList;
	
	@JsonProperty("buildCommitNumber")
	private String buildCommitNumber;
	
	@JsonProperty("versionNumber")
	private String versionNumber;

	public String geteRefNo() {
		return eRefNo;
	}

	public void seteRefNo(String eRefNo) {
		this.eRefNo = eRefNo;
	}

	public String getTotalAttachment() {
		return totalAttachment;
	}

	public void setTotalAttachment(String totalAttachment) {
		this.totalAttachment = totalAttachment;
	}

	public List<MstAttachmentListDto> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<MstAttachmentListDto> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public String getBuildCommitNumber() {
		return buildCommitNumber;
	}

	public void setBuildCommitNumber(String buildCommitNumber) {
		this.buildCommitNumber = buildCommitNumber;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}
	
	

}
