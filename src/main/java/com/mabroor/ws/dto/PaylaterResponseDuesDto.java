package com.mabroor.ws.dto;

public class PaylaterResponseDuesDto {
	
	private int dueIndex;
	private String dueDate;
	private String dueType;
	private int amount;
	private int settledAmount;
	private int remainingAmount;
	private int couponFee;
	public int getDueIndex() {
		return dueIndex;
	}
	public void setDueIndex(int dueIndex) {
		this.dueIndex = dueIndex;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getDueType() {
		return dueType;
	}
	public void setDueType(String dueType) {
		this.dueType = dueType;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getSettledAmount() {
		return settledAmount;
	}
	public void setSettledAmount(int settledAmount) {
		this.settledAmount = settledAmount;
	}
	public int getRemainingAmount() {
		return remainingAmount;
	}
	public void setRemainingAmount(int remainingAmount) {
		this.remainingAmount = remainingAmount;
	}
	public int getCouponFee() {
		return couponFee;
	}
	public void setCouponFee(int couponFee) {
		this.couponFee = couponFee;
	}
	
	

}
