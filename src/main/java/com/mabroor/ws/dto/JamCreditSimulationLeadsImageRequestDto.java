package com.mabroor.ws.dto;

public class JamCreditSimulationLeadsImageRequestDto {
	
	private String P_LEADS_ID;
	private String P_SELFIE;
	private String P_CD_SP;
	public String getP_LEADS_ID() {
		return P_LEADS_ID;
	}
	public void setP_LEADS_ID(String p_LEADS_ID) {
		P_LEADS_ID = p_LEADS_ID;
	}
	public String getP_SELFIE() {
		return P_SELFIE;
	}
	public void setP_SELFIE(String p_SELFIE) {
		P_SELFIE = p_SELFIE;
	}
	public String getP_CD_SP() {
		return P_CD_SP;
	}
	public void setP_CD_SP(String p_CD_SP) {
		P_CD_SP = p_CD_SP;
	}
	
	
	

}
