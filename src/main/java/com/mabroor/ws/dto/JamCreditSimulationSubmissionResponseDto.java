package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JamCreditSimulationSubmissionResponseDto {
	
	@JsonProperty("OUT_STAT")
	private String OUT_STAT;
	@JsonProperty("OUT_MESS")
	private String OUT_MESS;
	@JsonProperty("OUT_DATA")
	private List<CarCreditSimulationSubmissionNoRegDto> OUT_DATA;
	public String getOUT_STAT() {
		return OUT_STAT;
	}
	public void setOUT_STAT(String oUT_STAT) {
		OUT_STAT = oUT_STAT;
	}
	public String getOUT_MESS() {
		return OUT_MESS;
	}
	public void setOUT_MESS(String oUT_MESS) {
		OUT_MESS = oUT_MESS;
	}
	public List<CarCreditSimulationSubmissionNoRegDto> getOUT_DATA() {
		return OUT_DATA;
	}
	public void setOUT_DATA(List<CarCreditSimulationSubmissionNoRegDto> oUT_DATA) {
		OUT_DATA = oUT_DATA;
	}
	
	
	
	
	

}
