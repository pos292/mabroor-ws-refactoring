package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


public class CarCreditSimulationSubmissionRequestDto {
	
	@JsonProperty("P_ACCOUNT_ID")
	private String P_ACCOUNT_ID;
	
	@JsonProperty("P_PARAM1")
	private String P_PARAM1;
	
	@JsonProperty("P_PARAM2")
	private String P_PARAM2;
	
	@JsonProperty("P_PARAM3")
	private String P_PARAM3;
	
	@JsonProperty("P_PARAM4")
	private String P_PARAM4;
	
	@JsonProperty("P_PARAM5")
	private String P_PARAM5;
	
	@JsonProperty("P_PARAM6")
	private String P_PARAM6;
	
	@JsonProperty("P_PARAM7")
	private String P_PARAM7;
	
	@JsonProperty("P_PARAM8")
	private String P_PARAM8;
	
	@JsonProperty("P_PARAM9")
	private String P_PARAM9;
	
	@JsonProperty("P_PARAM10")
	private String P_PARAM10;
	
	@JsonProperty("P_NAME")
	private String P_NAME;
	
	@JsonProperty("P_PRODUCT")
	private String P_PRODUCT;
	
	@JsonProperty("P_NO_AGGR")
	private String P_NO_AGGR;
	
	@JsonProperty("P_PHONE_NUMBER")
	private String P_PHONE_NUMBER;
	
	@JsonProperty("P_CD_VEHICLE_BRAND")
	private String P_CD_VEHICLE_BRAND;
	
	@JsonProperty("P_CD_VEHICLE_MODEL")
	private String P_CD_VEHICLE_MODEL;
	
	@JsonProperty("P_CD_VEHICLE_TYPE")
	private String P_CD_VEHICLE_TYPE;
	
	@JsonProperty("P_YEAR_OF_MFG")
	private String P_YEAR_OF_MFG;
	
	@JsonProperty("P_OTR")
	private String P_OTR;
	
	@JsonProperty("P_DP")
	private String P_DP;
	
	@JsonProperty("P_TENOR")
	private String P_TENOR;
	
	@JsonProperty("P_CD_AREA")
	private String P_CD_AREA;
	
	@JsonProperty("P_CD_SP")
	private String P_CD_SP;
	
	@JsonProperty("P_FLAT_RATE")
	private String P_FLAT_RATE;
	
	@JsonProperty("P_ASURANSI_CASH_KREDIT")
	private String P_ASURANSI_CASH_KREDIT;
	
	@JsonProperty("P_FLAG_ACP")
	private String P_FLAG_ACP;
	
	@JsonProperty("P_AMT_ACP")
	private String P_AMT_ACP;
	
	@JsonProperty("P_TDP")
	private String P_TDP;
	
	@JsonProperty("P_SOURCE_LEADS")
	private String P_SOURCE_LEADS;
	
	@JsonProperty("P_AGEN_SOURCE_LEADS")
	private String P_AGEN_SOURCE_LEADS;
	
	@JsonProperty("P_AMT_INSTALLMENT")
	private String P_AMT_INSTALLMENT;
	
	@JsonProperty("P_ADDRESS")
	private String P_ADDRESS;
	
	@JsonProperty("P_KELURAHAN")
	private String P_KELURAHAN;

	@JsonProperty("P_KECAMATAN")
	private String P_KECAMATAN;
	
	@JsonProperty("P_CITY")
	private String P_CITY;
	
	@JsonProperty("P_NO_RT_RW_RESI")
	private String P_NO_RT_RW_RESI;
	
	@JsonProperty("P_POST_CODE")
	private String P_POST_CODE;
	
	@JsonProperty("P_AREA_BRANCH")
	private String P_AREA_BRANCH;
	
	@JsonProperty("P_PHONE1_RESI")
	private String P_PHONE1_RESI;
	
	@JsonProperty("P_FLAG_NEW_USED_VALID")
	private String P_FLAG_NEW_USED_VALID;
	
	@JsonProperty("P_NO_ID")
	private String P_NO_ID;
	
	@JsonProperty("P_DT_EXPIRED_ID")
	private String P_DT_EXPIRED_ID;
	
	@JsonProperty("P_CD_SEX")
	private String P_CD_SEX;
	
	@JsonProperty("P_AREA1")
	private String P_AREA1;
	
	@JsonProperty("P_AREA2")
	private String P_AREA2;
	
	@JsonProperty("P_PLACE_OF_BIRTH")
	private String P_PLACE_OF_BIRTH;
	
	@JsonProperty("P_DT_BIRTH")
	private String P_DT_BIRTH;
	
	@JsonProperty("P_HP")
	private String P_HP;
	
	@JsonProperty("P_MARITAL_STATUS")
	private String P_MARITAL_STATUS;
	
	@JsonProperty("P_RELIGION")
	private String P_RELIGION;
	
	@JsonProperty("P_CD_GRP_BUSSI")
	private String P_CD_GRP_BUSSI;
	
	@JsonProperty("P_NO_TAX")
	private String P_NO_TAX;
	
	@JsonProperty("P_DT_BUSI_START")
	private String P_DT_BUSI_START;
	
	@JsonProperty("P_DT_START_REG")
	private String P_DT_START_REG;
	
	@JsonProperty("P_DEALER")
	private String P_DEALER;
	
	@JsonProperty("P_EMAIL")
	private String P_EMAIL;
	
	@JsonProperty("P_FLAG_PC")
	private String P_FLAG_PC;
	
	@JsonProperty("P_TENOR_TLO")
	private String P_TENOR_TLO;
	
	@JsonProperty("P_TAHUN_CASH")
	private String P_TAHUN_CASH;
	
	@JsonProperty("P_TAHUN_KREDIT")
	private String P_TAHUN_KREDIT;
	
	@JsonProperty("P_ACP_CASH_CREDIT")
	private String P_ACP_CASH_CREDIT;
	
	@JsonProperty("P_PHONE_OFFICE1")
	private String P_PHONE_OFFICE1;
	
	@JsonProperty("P_PHONE_OFFICE2")
	private String P_PHONE_OFFICE2;
	
	@JsonProperty("P_PHONE_BUSSINESS1")
	private String P_PHONE_BUSSINESS1;
	
	@JsonProperty("P_PHONE_BUSSINESS2")
	private String P_PHONE_BUSSINESS2;
	
	@JsonProperty("P_PAKET_PRODUCT")
	private String P_PAKET_PRODUCT;
	
	@JsonProperty("P_CD_CHANNEL")
	private String P_CD_CHANNEL;
	
	@JsonProperty("P_CD_SPK")
	private String P_CD_SPK;
	
	@JsonProperty("P_ADD_AMOUNT")
	private String P_ADD_AMOUNT;
	
	@JsonProperty("P_EFF_RATE")
	private String P_EFF_RATE;
	
	@JsonProperty("P_NO_ADV_INST")
	private String P_NO_ADV_INST;
	
	@JsonProperty("P_ADMIN_FEE")
	private String P_ADMIN_FEE;
	
	@JsonProperty("P_MODE_PROVISI")
	private String P_MODE_PROVISI;
	
	@JsonProperty("P_BIAYA_PROVISI")
	private String P_BIAYA_PROVISI;
	
	@JsonProperty("P_AF")
	private String P_AF;
	
	@JsonProperty("P_INTEREST")
	private String P_INTEREST;
	
	@JsonProperty("P_AR")
	private String P_AR;
	
	@JsonProperty("P_CD_BANK")
	private String P_CD_BANK;
	
	@JsonProperty("P_NO_REKENING")
	private String P_NO_REKENING;
	
	@JsonProperty("P_NAMA_REKENING")
	private String P_NAMA_REKENING;
	
	@JsonProperty("P_ASURANSI")
	private String P_ASURANSI;
	
	@JsonProperty("P_BIAYA_TJH")
	private String P_BIAYA_TJH;
	
	@JsonProperty("P_OPPURTUNITY_STATUS")
	private String P_OPPURTUNITY_STATUS;
	
	@JsonProperty("P_OPPURTUNITY_DATE")
	private String P_OPPURTUNITY_DATE;
	
	@JsonProperty("P_AMT_CASH_PENCAIRAN")
	private String P_AMT_CASH_PENCAIRAN;
	
	@JsonProperty("P_AMT_CASH_INT")
	private String P_AMT_CASH_INT;
	
	@JsonProperty("P_TENOR_CASH")
	private String P_TENOR_CASH;
	
	@JsonProperty("P_TENOR_AR")
	private String P_TENOR_AR;
	
	@JsonProperty("P_TUJUAN_DANA")
	private String P_TUJUAN_DANA;
	
	@JsonProperty("P_BARANG_JASA")
	private String P_BARANG_JASA;
	
	@JsonProperty("P_NT_EXP_STNK")
	private String P_NT_EXP_STNK;
	
	@JsonProperty("P_NOTES")
	private String P_NOTES;
	
	@JsonProperty("P_CD_COLOR")
	private String P_CD_COLOR;
	
	@JsonProperty("P_AMT_INS_BALLON")
	private String P_AMT_INS_BALLON;
	
	@JsonProperty("P_AMT_PDC")
	private String P_AMT_PDC;
	
	@JsonProperty("P_FLAG_UNIT_COMMERCIAL")
	private String P_FLAG_UNIT_COMMERCIAL;
	
	@JsonProperty("P_AMT_PH")
	private String P_AMT_PH;
	
	@JsonProperty("P_LEADS_ID")
	private String P_LEADS_ID;
	
	@JsonProperty("P_GRADING")
	private String P_GRADING;
	
	@JsonProperty("P_PROPENSITY_SCORE")
	private String P_PROPENSITY_SCORE;
	
	@JsonProperty("P_SOURCE_TO")
	private String P_SOURCE_TO;
	
	@JsonProperty("P_FLAG_GRADING")
	private String P_FLAG_GRADING;
	
	@JsonProperty("P_STATUS_HANDLING")
	private String P_STATUS_HANDLING;
	
	@JsonProperty("P_NO_REG_AOL")
	private String P_NO_REG_AOL;
	
	@JsonProperty("P_STATUS_LEADS")
	private String P_STATUS_LEADS;
	
	@JsonProperty("P_FLAG_SOURCE")
	private String P_FLAG_SOURCE;
	
	@JsonProperty("P_FLAG_PRIORITY")
	private String P_FLAG_PRIORITY;
	
	@JsonProperty("P_CD_CUSTOMER")
	private String P_CD_CUSTOMER;
	
	@JsonProperty("P_CD_JOB")
	private String P_CD_JOB;
	
	@JsonProperty("P_CD_EDUCATION")
	private String P_CD_EDUCATION;
	
	@JsonProperty("P_CD_NATIONALITY")
	private String P_CD_NATIONALITY;
	
	@JsonProperty("P_TOT_TANGGUNGAN")
	private String P_TOT_TANGGUNGAN;
	
	@JsonProperty("P_CD_BUSITYPE")
	private String P_CD_BUSITYPE;
	
	@JsonProperty("P_PHONE_MOBILE_CO")
	private String P_PHONE_MOBILE_CO;
	
	@JsonProperty("P_ST_BPKB")
	private String P_ST_BPKB;
	
	@JsonProperty("P_NO_CAR_POLICE")
	private String P_NO_CAR_POLICE;
	
	@JsonProperty("P_ST_AGGR_OLD")
	private String P_ST_AGGR_OLD;
	
	@JsonProperty("P_SISA_TENOR")
	private String P_SISA_TENOR;
	
	@JsonProperty("P_CD_VEHICLE_KIND")
	private String P_CD_VEHICLE_KIND;
	
	@JsonProperty("P_DT_DUE")
	private String P_DT_DUE;
	
	@JsonProperty("P_DT_ADDED")
	private String P_DT_ADDED;
	
	@JsonProperty("P_ID_USER_ADDED")
	private String P_ID_USER_ADDED;
	
	@JsonProperty("P_DT_UPDATED")
	private String P_DT_UPDATED;
	
	@JsonProperty("P_ID_USER_UPDATED")
	private String P_ID_USER_UPDATED;
	
	@JsonProperty("P_NPK_SALESMAN")
	private String P_NPK_SALESMAN;
	
	@JsonProperty("P_FLAG_ROAO")
	private String P_FLAG_ROAO;
	
	@JsonProperty("P_PAKET_PRODUCT2")
	private String P_PAKET_PRODUCT2;
	
	@JsonProperty("P_PAKET_PRODUCT3")
	private String P_PAKET_PRODUCT3;
	
	@JsonProperty("P_TEAM")
	private String P_TEAM;
	
	@JsonProperty("P_FLAG_DP_TO_ACC")
	private String P_FLAG_DP_TO_ACC;
	
	@JsonProperty("P_FLAG_TANPA_JADI")
	private String P_FLAG_TANPA_JADI;
	
	@JsonProperty("P_ID_SIMULATION")
	private String P_ID_SIMULATION;
	
	@JsonProperty("P_TYPE_INSTALLMENT")
	private String P_TYPE_INSTALLMENT;
	
	@JsonProperty("P_PERC_DP")
	private String P_PERC_DP;
	
	@JsonProperty("P_ID_FB_LEADS")
	private String P_ID_FB_LEADS;
	
	@JsonProperty("P_ADMIN_ASURANSI")
	private String P_ADMIN_ASURANSI;
	
	@JsonProperty("P_NO_REG_EXTERNAL")
	private String P_NO_REG_EXTERNAL;

	public String getP_ACCOUNT_ID() {
		return P_ACCOUNT_ID;
	}

	public void setP_ACCOUNT_ID(String p_ACCOUNT_ID) {
		P_ACCOUNT_ID = p_ACCOUNT_ID;
	}

	public String getP_PARAM1() {
		return P_PARAM1;
	}

	public void setP_PARAM1(String p_PARAM1) {
		P_PARAM1 = p_PARAM1;
	}

	public String getP_PARAM2() {
		return P_PARAM2;
	}

	public void setP_PARAM2(String p_PARAM2) {
		P_PARAM2 = p_PARAM2;
	}

	public String getP_PARAM3() {
		return P_PARAM3;
	}

	public void setP_PARAM3(String p_PARAM3) {
		P_PARAM3 = p_PARAM3;
	}

	public String getP_PARAM4() {
		return P_PARAM4;
	}

	public void setP_PARAM4(String p_PARAM4) {
		P_PARAM4 = p_PARAM4;
	}

	public String getP_PARAM5() {
		return P_PARAM5;
	}

	public void setP_PARAM5(String p_PARAM5) {
		P_PARAM5 = p_PARAM5;
	}

	public String getP_PARAM6() {
		return P_PARAM6;
	}

	public void setP_PARAM6(String p_PARAM6) {
		P_PARAM6 = p_PARAM6;
	}

	public String getP_PARAM7() {
		return P_PARAM7;
	}

	public void setP_PARAM7(String p_PARAM7) {
		P_PARAM7 = p_PARAM7;
	}

	public String getP_PARAM8() {
		return P_PARAM8;
	}

	public void setP_PARAM8(String p_PARAM8) {
		P_PARAM8 = p_PARAM8;
	}

	public String getP_PARAM9() {
		return P_PARAM9;
	}

	public void setP_PARAM9(String p_PARAM9) {
		P_PARAM9 = p_PARAM9;
	}

	public String getP_PARAM10() {
		return P_PARAM10;
	}

	public void setP_PARAM10(String p_PARAM10) {
		P_PARAM10 = p_PARAM10;
	}

	public String getP_NAME() {
		return P_NAME;
	}

	public void setP_NAME(String p_NAME) {
		P_NAME = p_NAME;
	}

	public String getP_PRODUCT() {
		return P_PRODUCT;
	}

	public void setP_PRODUCT(String p_PRODUCT) {
		P_PRODUCT = p_PRODUCT;
	}

	public String getP_NO_AGGR() {
		return P_NO_AGGR;
	}

	public void setP_NO_AGGR(String p_NO_AGGR) {
		P_NO_AGGR = p_NO_AGGR;
	}

	public String getP_PHONE_NUMBER() {
		return P_PHONE_NUMBER;
	}

	public void setP_PHONE_NUMBER(String p_PHONE_NUMBER) {
		P_PHONE_NUMBER = p_PHONE_NUMBER;
	}

	public String getP_CD_VEHICLE_BRAND() {
		return P_CD_VEHICLE_BRAND;
	}

	public void setP_CD_VEHICLE_BRAND(String p_CD_VEHICLE_BRAND) {
		P_CD_VEHICLE_BRAND = p_CD_VEHICLE_BRAND;
	}

	public String getP_CD_VEHICLE_MODEL() {
		return P_CD_VEHICLE_MODEL;
	}

	public void setP_CD_VEHICLE_MODEL(String p_CD_VEHICLE_MODEL) {
		P_CD_VEHICLE_MODEL = p_CD_VEHICLE_MODEL;
	}

	public String getP_CD_VEHICLE_TYPE() {
		return P_CD_VEHICLE_TYPE;
	}

	public void setP_CD_VEHICLE_TYPE(String p_CD_VEHICLE_TYPE) {
		P_CD_VEHICLE_TYPE = p_CD_VEHICLE_TYPE;
	}

	public String getP_YEAR_OF_MFG() {
		return P_YEAR_OF_MFG;
	}

	public void setP_YEAR_OF_MFG(String p_YEAR_OF_MFG) {
		P_YEAR_OF_MFG = p_YEAR_OF_MFG;
	}

	public String getP_OTR() {
		return P_OTR;
	}

	public void setP_OTR(String p_OTR) {
		P_OTR = p_OTR;
	}

	public String getP_DP() {
		return P_DP;
	}

	public void setP_DP(String p_DP) {
		P_DP = p_DP;
	}

	public String getP_TENOR() {
		return P_TENOR;
	}

	public void setP_TENOR(String p_TENOR) {
		P_TENOR = p_TENOR;
	}

	public String getP_CD_AREA() {
		return P_CD_AREA;
	}

	public void setP_CD_AREA(String p_CD_AREA) {
		P_CD_AREA = p_CD_AREA;
	}

	public String getP_CD_SP() {
		return P_CD_SP;
	}

	public void setP_CD_SP(String p_CD_SP) {
		P_CD_SP = p_CD_SP;
	}

	public String getP_FLAT_RATE() {
		return P_FLAT_RATE;
	}

	public void setP_FLAT_RATE(String p_FLAT_RATE) {
		P_FLAT_RATE = p_FLAT_RATE;
	}

	public String getP_ASURANSI_CASH_KREDIT() {
		return P_ASURANSI_CASH_KREDIT;
	}

	public void setP_ASURANSI_CASH_KREDIT(String p_ASURANSI_CASH_KREDIT) {
		P_ASURANSI_CASH_KREDIT = p_ASURANSI_CASH_KREDIT;
	}

	public String getP_FLAG_ACP() {
		return P_FLAG_ACP;
	}

	public void setP_FLAG_ACP(String p_FLAG_ACP) {
		P_FLAG_ACP = p_FLAG_ACP;
	}

	public String getP_AMT_ACP() {
		return P_AMT_ACP;
	}

	public void setP_AMT_ACP(String p_AMT_ACP) {
		P_AMT_ACP = p_AMT_ACP;
	}

	public String getP_TDP() {
		return P_TDP;
	}

	public void setP_TDP(String p_TDP) {
		P_TDP = p_TDP;
	}

	public String getP_SOURCE_LEADS() {
		return P_SOURCE_LEADS;
	}

	public void setP_SOURCE_LEADS(String p_SOURCE_LEADS) {
		P_SOURCE_LEADS = p_SOURCE_LEADS;
	}

	public String getP_AGEN_SOURCE_LEADS() {
		return P_AGEN_SOURCE_LEADS;
	}

	public void setP_AGEN_SOURCE_LEADS(String p_AGEN_SOURCE_LEADS) {
		P_AGEN_SOURCE_LEADS = p_AGEN_SOURCE_LEADS;
	}

	public String getP_AMT_INSTALLMENT() {
		return P_AMT_INSTALLMENT;
	}

	public void setP_AMT_INSTALLMENT(String p_AMT_INSTALLMENT) {
		P_AMT_INSTALLMENT = p_AMT_INSTALLMENT;
	}

	public String getP_ADDRESS() {
		return P_ADDRESS;
	}

	public void setP_ADDRESS(String p_ADDRESS) {
		P_ADDRESS = p_ADDRESS;
	}

	public String getP_KELURAHAN() {
		return P_KELURAHAN;
	}

	public void setP_KELURAHAN(String p_KELURAHAN) {
		P_KELURAHAN = p_KELURAHAN;
	}

	public String getP_KECAMATAN() {
		return P_KECAMATAN;
	}

	public void setP_KECAMATAN(String p_KECAMATAN) {
		P_KECAMATAN = p_KECAMATAN;
	}

	public String getP_CITY() {
		return P_CITY;
	}

	public void setP_CITY(String p_CITY) {
		P_CITY = p_CITY;
	}

	public String getP_NO_RT_RW_RESI() {
		return P_NO_RT_RW_RESI;
	}

	public void setP_NO_RT_RW_RESI(String p_NO_RT_RW_RESI) {
		P_NO_RT_RW_RESI = p_NO_RT_RW_RESI;
	}

	public String getP_POST_CODE() {
		return P_POST_CODE;
	}

	public void setP_POST_CODE(String p_POST_CODE) {
		P_POST_CODE = p_POST_CODE;
	}

	public String getP_AREA_BRANCH() {
		return P_AREA_BRANCH;
	}

	public void setP_AREA_BRANCH(String p_AREA_BRANCH) {
		P_AREA_BRANCH = p_AREA_BRANCH;
	}

	public String getP_PHONE1_RESI() {
		return P_PHONE1_RESI;
	}

	public void setP_PHONE1_RESI(String p_PHONE1_RESI) {
		P_PHONE1_RESI = p_PHONE1_RESI;
	}

	public String getP_FLAG_NEW_USED_VALID() {
		return P_FLAG_NEW_USED_VALID;
	}

	public void setP_FLAG_NEW_USED_VALID(String p_FLAG_NEW_USED_VALID) {
		P_FLAG_NEW_USED_VALID = p_FLAG_NEW_USED_VALID;
	}

	public String getP_NO_ID() {
		return P_NO_ID;
	}

	public void setP_NO_ID(String p_NO_ID) {
		P_NO_ID = p_NO_ID;
	}

	public String getP_DT_EXPIRED_ID() {
		return P_DT_EXPIRED_ID;
	}

	public void setP_DT_EXPIRED_ID(String p_DT_EXPIRED_ID) {
		P_DT_EXPIRED_ID = p_DT_EXPIRED_ID;
	}

	public String getP_CD_SEX() {
		return P_CD_SEX;
	}

	public void setP_CD_SEX(String p_CD_SEX) {
		P_CD_SEX = p_CD_SEX;
	}

	public String getP_AREA1() {
		return P_AREA1;
	}

	public void setP_AREA1(String p_AREA1) {
		P_AREA1 = p_AREA1;
	}

	public String getP_AREA2() {
		return P_AREA2;
	}

	public void setP_AREA2(String p_AREA2) {
		P_AREA2 = p_AREA2;
	}

	public String getP_PLACE_OF_BIRTH() {
		return P_PLACE_OF_BIRTH;
	}

	public void setP_PLACE_OF_BIRTH(String p_PLACE_OF_BIRTH) {
		P_PLACE_OF_BIRTH = p_PLACE_OF_BIRTH;
	}

	public String getP_DT_BIRTH() {
		return P_DT_BIRTH;
	}

	public void setP_DT_BIRTH(String p_DT_BIRTH) {
		P_DT_BIRTH = p_DT_BIRTH;
	}

	public String getP_HP() {
		return P_HP;
	}

	public void setP_HP(String p_HP) {
		P_HP = p_HP;
	}

	public String getP_MARITAL_STATUS() {
		return P_MARITAL_STATUS;
	}

	public void setP_MARITAL_STATUS(String p_MARITAL_STATUS) {
		P_MARITAL_STATUS = p_MARITAL_STATUS;
	}

	public String getP_RELIGION() {
		return P_RELIGION;
	}

	public void setP_RELIGION(String p_RELIGION) {
		P_RELIGION = p_RELIGION;
	}

	public String getP_CD_GRP_BUSSI() {
		return P_CD_GRP_BUSSI;
	}

	public void setP_CD_GRP_BUSSI(String p_CD_GRP_BUSSI) {
		P_CD_GRP_BUSSI = p_CD_GRP_BUSSI;
	}

	public String getP_NO_TAX() {
		return P_NO_TAX;
	}

	public void setP_NO_TAX(String p_NO_TAX) {
		P_NO_TAX = p_NO_TAX;
	}

	public String getP_DT_BUSI_START() {
		return P_DT_BUSI_START;
	}

	public void setP_DT_BUSI_START(String p_DT_BUSI_START) {
		P_DT_BUSI_START = p_DT_BUSI_START;
	}

	public String getP_DT_START_REG() {
		return P_DT_START_REG;
	}

	public void setP_DT_START_REG(String p_DT_START_REG) {
		P_DT_START_REG = p_DT_START_REG;
	}

	public String getP_DEALER() {
		return P_DEALER;
	}

	public void setP_DEALER(String p_DEALER) {
		P_DEALER = p_DEALER;
	}

	public String getP_EMAIL() {
		return P_EMAIL;
	}

	public void setP_EMAIL(String p_EMAIL) {
		P_EMAIL = p_EMAIL;
	}

	public String getP_FLAG_PC() {
		return P_FLAG_PC;
	}

	public void setP_FLAG_PC(String p_FLAG_PC) {
		P_FLAG_PC = p_FLAG_PC;
	}

	public String getP_TENOR_TLO() {
		return P_TENOR_TLO;
	}

	public void setP_TENOR_TLO(String p_TENOR_TLO) {
		P_TENOR_TLO = p_TENOR_TLO;
	}

	public String getP_TAHUN_CASH() {
		return P_TAHUN_CASH;
	}

	public void setP_TAHUN_CASH(String p_TAHUN_CASH) {
		P_TAHUN_CASH = p_TAHUN_CASH;
	}

	public String getP_TAHUN_KREDIT() {
		return P_TAHUN_KREDIT;
	}

	public void setP_TAHUN_KREDIT(String p_TAHUN_KREDIT) {
		P_TAHUN_KREDIT = p_TAHUN_KREDIT;
	}
	
	public String getP_ACP_CASH_CREDIT() {
		return P_ACP_CASH_CREDIT;
	}

	public void setP_ACP_CASH_CREDIT(String p_ACP_CASH_CREDIT) {
		P_ACP_CASH_CREDIT = p_ACP_CASH_CREDIT;
	}

	public String getP_PHONE_OFFICE1() {
		return P_PHONE_OFFICE1;
	}

	public void setP_PHONE_OFFICE1(String p_PHONE_OFFICE1) {
		P_PHONE_OFFICE1 = p_PHONE_OFFICE1;
	}

	public String getP_PHONE_OFFICE2() {
		return P_PHONE_OFFICE2;
	}

	public void setP_PHONE_OFFICE2(String p_PHONE_OFFICE2) {
		P_PHONE_OFFICE2 = p_PHONE_OFFICE2;
	}

	public String getP_PHONE_BUSSINESS1() {
		return P_PHONE_BUSSINESS1;
	}

	public void setP_PHONE_BUSSINESS1(String p_PHONE_BUSSINESS1) {
		P_PHONE_BUSSINESS1 = p_PHONE_BUSSINESS1;
	}

	public String getP_PHONE_BUSSINESS2() {
		return P_PHONE_BUSSINESS2;
	}

	public void setP_PHONE_BUSSINESS2(String p_PHONE_BUSSINESS2) {
		P_PHONE_BUSSINESS2 = p_PHONE_BUSSINESS2;
	}

	public String getP_PAKET_PRODUCT() {
		return P_PAKET_PRODUCT;
	}

	public void setP_PAKET_PRODUCT(String p_PAKET_PRODUCT) {
		P_PAKET_PRODUCT = p_PAKET_PRODUCT;
	}

	public String getP_CD_CHANNEL() {
		return P_CD_CHANNEL;
	}

	public void setP_CD_CHANNEL(String p_CD_CHANNEL) {
		P_CD_CHANNEL = p_CD_CHANNEL;
	}

	public String getP_CD_SPK() {
		return P_CD_SPK;
	}

	public void setP_CD_SPK(String p_CD_SPK) {
		P_CD_SPK = p_CD_SPK;
	}

	public String getP_ADD_AMOUNT() {
		return P_ADD_AMOUNT;
	}

	public void setP_ADD_AMOUNT(String p_ADD_AMOUNT) {
		P_ADD_AMOUNT = p_ADD_AMOUNT;
	}

	public String getP_EFF_RATE() {
		return P_EFF_RATE;
	}

	public void setP_EFF_RATE(String p_EFF_RATE) {
		P_EFF_RATE = p_EFF_RATE;
	}

	public String getP_NO_ADV_INST() {
		return P_NO_ADV_INST;
	}

	public void setP_NO_ADV_INST(String p_NO_ADV_INST) {
		P_NO_ADV_INST = p_NO_ADV_INST;
	}

	public String getP_ADMIN_FEE() {
		return P_ADMIN_FEE;
	}

	public void setP_ADMIN_FEE(String p_ADMIN_FEE) {
		P_ADMIN_FEE = p_ADMIN_FEE;
	}

	public String getP_MODE_PROVISI() {
		return P_MODE_PROVISI;
	}

	public void setP_MODE_PROVISI(String p_MODE_PROVISI) {
		P_MODE_PROVISI = p_MODE_PROVISI;
	}

	public String getP_BIAYA_PROVISI() {
		return P_BIAYA_PROVISI;
	}

	public void setP_BIAYA_PROVISI(String p_BIAYA_PROVISI) {
		P_BIAYA_PROVISI = p_BIAYA_PROVISI;
	}

	public String getP_AF() {
		return P_AF;
	}

	public void setP_AF(String p_AF) {
		P_AF = p_AF;
	}

	public String getP_INTEREST() {
		return P_INTEREST;
	}

	public void setP_INTEREST(String p_INTEREST) {
		P_INTEREST = p_INTEREST;
	}

	public String getP_AR() {
		return P_AR;
	}

	public void setP_AR(String p_AR) {
		P_AR = p_AR;
	}

	public String getP_CD_BANK() {
		return P_CD_BANK;
	}

	public void setP_CD_BANK(String p_CD_BANK) {
		P_CD_BANK = p_CD_BANK;
	}

	public String getP_NO_REKENING() {
		return P_NO_REKENING;
	}

	public void setP_NO_REKENING(String p_NO_REKENING) {
		P_NO_REKENING = p_NO_REKENING;
	}

	public String getP_NAMA_REKENING() {
		return P_NAMA_REKENING;
	}

	public void setP_NAMA_REKENING(String p_NAMA_REKENING) {
		P_NAMA_REKENING = p_NAMA_REKENING;
	}

	public String getP_ASURANSI() {
		return P_ASURANSI;
	}

	public void setP_ASURANSI(String p_ASURANSI) {
		P_ASURANSI = p_ASURANSI;
	}

	public String getP_BIAYA_TJH() {
		return P_BIAYA_TJH;
	}

	public void setP_BIAYA_TJH(String p_BIAYA_TJH) {
		P_BIAYA_TJH = p_BIAYA_TJH;
	}

	public String getP_OPPURTUNITY_STATUS() {
		return P_OPPURTUNITY_STATUS;
	}

	public void setP_OPPURTUNITY_STATUS(String p_OPPURTUNITY_STATUS) {
		P_OPPURTUNITY_STATUS = p_OPPURTUNITY_STATUS;
	}

	public String getP_OPPURTUNITY_DATE() {
		return P_OPPURTUNITY_DATE;
	}

	public void setP_OPPURTUNITY_DATE(String p_OPPURTUNITY_DATE) {
		P_OPPURTUNITY_DATE = p_OPPURTUNITY_DATE;
	}

	public String getP_AMT_CASH_PENCAIRAN() {
		return P_AMT_CASH_PENCAIRAN;
	}

	public void setP_AMT_CASH_PENCAIRAN(String p_AMT_CASH_PENCAIRAN) {
		P_AMT_CASH_PENCAIRAN = p_AMT_CASH_PENCAIRAN;
	}

	public String getP_AMT_CASH_INT() {
		return P_AMT_CASH_INT;
	}

	public void setP_AMT_CASH_INT(String p_AMT_CASH_INT) {
		P_AMT_CASH_INT = p_AMT_CASH_INT;
	}

	public String getP_TENOR_CASH() {
		return P_TENOR_CASH;
	}

	public void setP_TENOR_CASH(String p_TENOR_CASH) {
		P_TENOR_CASH = p_TENOR_CASH;
	}

	public String getP_TENOR_AR() {
		return P_TENOR_AR;
	}

	public void setP_TENOR_AR(String p_TENOR_AR) {
		P_TENOR_AR = p_TENOR_AR;
	}

	public String getP_TUJUAN_DANA() {
		return P_TUJUAN_DANA;
	}

	public void setP_TUJUAN_DANA(String p_TUJUAN_DANA) {
		P_TUJUAN_DANA = p_TUJUAN_DANA;
	}

	public String getP_BARANG_JASA() {
		return P_BARANG_JASA;
	}

	public void setP_BARANG_JASA(String p_BARANG_JASA) {
		P_BARANG_JASA = p_BARANG_JASA;
	}

	public String getP_NT_EXP_STNK() {
		return P_NT_EXP_STNK;
	}

	public void setP_NT_EXP_STNK(String p_NT_EXP_STNK) {
		P_NT_EXP_STNK = p_NT_EXP_STNK;
	}

	public String getP_NOTES() {
		return P_NOTES;
	}

	public void setP_NOTES(String p_NOTES) {
		P_NOTES = p_NOTES;
	}

	public String getP_CD_COLOR() {
		return P_CD_COLOR;
	}

	public void setP_CD_COLOR(String p_CD_COLOR) {
		P_CD_COLOR = p_CD_COLOR;
	}

	public String getP_AMT_INS_BALLON() {
		return P_AMT_INS_BALLON;
	}

	public void setP_AMT_INS_BALLON(String p_AMT_INS_BALLON) {
		P_AMT_INS_BALLON = p_AMT_INS_BALLON;
	}

	public String getP_AMT_PDC() {
		return P_AMT_PDC;
	}

	public void setP_AMT_PDC(String p_AMT_PDC) {
		P_AMT_PDC = p_AMT_PDC;
	}

	public String getP_FLAG_UNIT_COMMERCIAL() {
		return P_FLAG_UNIT_COMMERCIAL;
	}

	public void setP_FLAG_UNIT_COMMERCIAL(String p_FLAG_UNIT_COMMERCIAL) {
		P_FLAG_UNIT_COMMERCIAL = p_FLAG_UNIT_COMMERCIAL;
	}

	public String getP_AMT_PH() {
		return P_AMT_PH;
	}

	public void setP_AMT_PH(String p_AMT_PH) {
		P_AMT_PH = p_AMT_PH;
	}

	public String getP_LEADS_ID() {
		return P_LEADS_ID;
	}

	public void setP_LEADS_ID(String p_LEADS_ID) {
		P_LEADS_ID = p_LEADS_ID;
	}

	public String getP_GRADING() {
		return P_GRADING;
	}

	public void setP_GRADING(String p_GRADING) {
		P_GRADING = p_GRADING;
	}

	public String getP_PROPENSITY_SCORE() {
		return P_PROPENSITY_SCORE;
	}

	public void setP_PROPENSITY_SCORE(String p_PROPENSITY_SCORE) {
		P_PROPENSITY_SCORE = p_PROPENSITY_SCORE;
	}

	public String getP_SOURCE_TO() {
		return P_SOURCE_TO;
	}

	public void setP_SOURCE_TO(String p_SOURCE_TO) {
		P_SOURCE_TO = p_SOURCE_TO;
	}

	public String getP_FLAG_GRADING() {
		return P_FLAG_GRADING;
	}

	public void setP_FLAG_GRADING(String p_FLAG_GRADING) {
		P_FLAG_GRADING = p_FLAG_GRADING;
	}

	public String getP_STATUS_HANDLING() {
		return P_STATUS_HANDLING;
	}

	public void setP_STATUS_HANDLING(String p_STATUS_HANDLING) {
		P_STATUS_HANDLING = p_STATUS_HANDLING;
	}

	public String getP_NO_REG_AOL() {
		return P_NO_REG_AOL;
	}

	public void setP_NO_REG_AOL(String p_NO_REG_AOL) {
		P_NO_REG_AOL = p_NO_REG_AOL;
	}

	public String getP_STATUS_LEADS() {
		return P_STATUS_LEADS;
	}

	public void setP_STATUS_LEADS(String p_STATUS_LEADS) {
		P_STATUS_LEADS = p_STATUS_LEADS;
	}

	public String getP_FLAG_SOURCE() {
		return P_FLAG_SOURCE;
	}

	public void setP_FLAG_SOURCE(String p_FLAG_SOURCE) {
		P_FLAG_SOURCE = p_FLAG_SOURCE;
	}

	public String getP_FLAG_PRIORITY() {
		return P_FLAG_PRIORITY;
	}

	public void setP_FLAG_PRIORITY(String p_FLAG_PRIORITY) {
		P_FLAG_PRIORITY = p_FLAG_PRIORITY;
	}

	public String getP_CD_CUSTOMER() {
		return P_CD_CUSTOMER;
	}

	public void setP_CD_CUSTOMER(String p_CD_CUSTOMER) {
		P_CD_CUSTOMER = p_CD_CUSTOMER;
	}

	public String getP_CD_JOB() {
		return P_CD_JOB;
	}

	public void setP_CD_JOB(String p_CD_JOB) {
		P_CD_JOB = p_CD_JOB;
	}

	public String getP_CD_EDUCATION() {
		return P_CD_EDUCATION;
	}

	public void setP_CD_EDUCATION(String p_CD_EDUCATION) {
		P_CD_EDUCATION = p_CD_EDUCATION;
	}

	public String getP_CD_NATIONALITY() {
		return P_CD_NATIONALITY;
	}

	public void setP_CD_NATIONALITY(String p_CD_NATIONALITY) {
		P_CD_NATIONALITY = p_CD_NATIONALITY;
	}

	public String getP_TOT_TANGGUNGAN() {
		return P_TOT_TANGGUNGAN;
	}

	public void setP_TOT_TANGGUNGAN(String p_TOT_TANGGUNGAN) {
		P_TOT_TANGGUNGAN = p_TOT_TANGGUNGAN;
	}

	public String getP_CD_BUSITYPE() {
		return P_CD_BUSITYPE;
	}

	public void setP_CD_BUSITYPE(String p_CD_BUSITYPE) {
		P_CD_BUSITYPE = p_CD_BUSITYPE;
	}

	public String getP_PHONE_MOBILE_CO() {
		return P_PHONE_MOBILE_CO;
	}

	public void setP_PHONE_MOBILE_CO(String p_PHONE_MOBILE_CO) {
		P_PHONE_MOBILE_CO = p_PHONE_MOBILE_CO;
	}

	public String getP_ST_BPKB() {
		return P_ST_BPKB;
	}

	public void setP_ST_BPKB(String p_ST_BPKB) {
		P_ST_BPKB = p_ST_BPKB;
	}

	public String getP_NO_CAR_POLICE() {
		return P_NO_CAR_POLICE;
	}

	public void setP_NO_CAR_POLICE(String p_NO_CAR_POLICE) {
		P_NO_CAR_POLICE = p_NO_CAR_POLICE;
	}

	public String getP_ST_AGGR_OLD() {
		return P_ST_AGGR_OLD;
	}

	public void setP_ST_AGGR_OLD(String p_ST_AGGR_OLD) {
		P_ST_AGGR_OLD = p_ST_AGGR_OLD;
	}

	public String getP_SISA_TENOR() {
		return P_SISA_TENOR;
	}

	public void setP_SISA_TENOR(String p_SISA_TENOR) {
		P_SISA_TENOR = p_SISA_TENOR;
	}

	public String getP_CD_VEHICLE_KIND() {
		return P_CD_VEHICLE_KIND;
	}

	public void setP_CD_VEHICLE_KIND(String p_CD_VEHICLE_KIND) {
		P_CD_VEHICLE_KIND = p_CD_VEHICLE_KIND;
	}

	public String getP_DT_DUE() {
		return P_DT_DUE;
	}

	public void setP_DT_DUE(String p_DT_DUE) {
		P_DT_DUE = p_DT_DUE;
	}

	public String getP_DT_ADDED() {
		return P_DT_ADDED;
	}

	public void setP_DT_ADDED(String p_DT_ADDED) {
		P_DT_ADDED = p_DT_ADDED;
	}

	public String getP_ID_USER_ADDED() {
		return P_ID_USER_ADDED;
	}

	public void setP_ID_USER_ADDED(String p_ID_USER_ADDED) {
		P_ID_USER_ADDED = p_ID_USER_ADDED;
	}

	public String getP_DT_UPDATED() {
		return P_DT_UPDATED;
	}

	public void setP_DT_UPDATED(String p_DT_UPDATED) {
		P_DT_UPDATED = p_DT_UPDATED;
	}

	public String getP_ID_USER_UPDATED() {
		return P_ID_USER_UPDATED;
	}

	public void setP_ID_USER_UPDATED(String p_ID_USER_UPDATED) {
		P_ID_USER_UPDATED = p_ID_USER_UPDATED;
	}

	public String getP_NPK_SALESMAN() {
		return P_NPK_SALESMAN;
	}

	public void setP_NPK_SALESMAN(String p_NPK_SALESMAN) {
		P_NPK_SALESMAN = p_NPK_SALESMAN;
	}

	public String getP_FLAG_ROAO() {
		return P_FLAG_ROAO;
	}

	public void setP_FLAG_ROAO(String p_FLAG_ROAO) {
		P_FLAG_ROAO = p_FLAG_ROAO;
	}

	public String getP_PAKET_PRODUCT2() {
		return P_PAKET_PRODUCT2;
	}

	public void setP_PAKET_PRODUCT2(String p_PAKET_PRODUCT2) {
		P_PAKET_PRODUCT2 = p_PAKET_PRODUCT2;
	}

	public String getP_PAKET_PRODUCT3() {
		return P_PAKET_PRODUCT3;
	}

	public void setP_PAKET_PRODUCT3(String p_PAKET_PRODUCT3) {
		P_PAKET_PRODUCT3 = p_PAKET_PRODUCT3;
	}

	public String getP_TEAM() {
		return P_TEAM;
	}

	public void setP_TEAM(String p_TEAM) {
		P_TEAM = p_TEAM;
	}

	public String getP_FLAG_DP_TO_ACC() {
		return P_FLAG_DP_TO_ACC;
	}

	public void setP_FLAG_DP_TO_ACC(String p_FLAG_DP_TO_ACC) {
		P_FLAG_DP_TO_ACC = p_FLAG_DP_TO_ACC;
	}

	public String getP_FLAG_TANPA_JADI() {
		return P_FLAG_TANPA_JADI;
	}

	public void setP_FLAG_TANPA_JADI(String p_FLAG_TANPA_JADI) {
		P_FLAG_TANPA_JADI = p_FLAG_TANPA_JADI;
	}

	public String getP_ID_SIMULATION() {
		return P_ID_SIMULATION;
	}

	public void setP_ID_SIMULATION(String p_ID_SIMULATION) {
		P_ID_SIMULATION = p_ID_SIMULATION;
	}

	public String getP_TYPE_INSTALLMENT() {
		return P_TYPE_INSTALLMENT;
	}

	public void setP_TYPE_INSTALLMENT(String p_TYPE_INSTALLMENT) {
		P_TYPE_INSTALLMENT = p_TYPE_INSTALLMENT;
	}

	public String getP_PERC_DP() {
		return P_PERC_DP;
	}

	public void setP_PERC_DP(String p_PERC_DP) {
		P_PERC_DP = p_PERC_DP;
	}

	public String getP_ID_FB_LEADS() {
		return P_ID_FB_LEADS;
	}

	public void setP_ID_FB_LEADS(String p_ID_FB_LEADS) {
		P_ID_FB_LEADS = p_ID_FB_LEADS;
	}

	public String getP_ADMIN_ASURANSI() {
		return P_ADMIN_ASURANSI;
	}

	public void setP_ADMIN_ASURANSI(String p_ADMIN_ASURANSI) {
		P_ADMIN_ASURANSI = p_ADMIN_ASURANSI;
	}

	public String getP_NO_REG_EXTERNAL() {
		return P_NO_REG_EXTERNAL;
	}

	public void setP_NO_REG_EXTERNAL(String p_NO_REG_EXTERNAL) {
		P_NO_REG_EXTERNAL = p_NO_REG_EXTERNAL;
	}

	
	
}