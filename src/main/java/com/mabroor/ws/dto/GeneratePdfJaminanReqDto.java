package com.mabroor.ws.dto;

import java.math.BigDecimal;

public class GeneratePdfJaminanReqDto {

	private String name;
	private BigDecimal jumlahPinjaman;
	private String jenisPinjaman;
	private String tahunKendaraan;
	private String tipeKendaraan;
	private String kotaPengajuan;
	private String brandKendaraan;
	private String namaLengkap;
	private String phone;
	private String email;
	private String kebutuhanPinjaman;
	private String tenor;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getJumlahPinjaman() {
		return jumlahPinjaman;
	}
	public void setJumlahPinjaman(BigDecimal jumlahPinjaman) {
		this.jumlahPinjaman = jumlahPinjaman;
	}
	public String getJenisPinjaman() {
		return jenisPinjaman;
	}
	public void setJenisPinjaman(String jenisPinjaman) {
		this.jenisPinjaman = jenisPinjaman;
	}
	public String getKebutuhanPinjaman() {
		return kebutuhanPinjaman;
	}
	public void setKebutuhanPinjaman(String kebutuhanPinjaman) {
		this.kebutuhanPinjaman = kebutuhanPinjaman;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getTahunKendaraan() {
		return tahunKendaraan;
	}
	public void setTahunKendaraan(String tahunKendaraan) {
		this.tahunKendaraan = tahunKendaraan;
	}
	public String getTipeKendaraan() {
		return tipeKendaraan;
	}
	public void setTipeKendaraan(String tipeKendaraan) {
		this.tipeKendaraan = tipeKendaraan;
	}
	public String getKotaPengajuan() {
		return kotaPengajuan;
	}
	public void setKotaPengajuan(String kotaPengajuan) {
		this.kotaPengajuan = kotaPengajuan;
	}
	public String getBrandKendaraan() {
		return brandKendaraan;
	}
	public void setBrandKendaraan(String brandKendaraan) {
		this.brandKendaraan = brandKendaraan;
	}
	public String getNamaLengkap() {
		return namaLengkap;
	}
	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
