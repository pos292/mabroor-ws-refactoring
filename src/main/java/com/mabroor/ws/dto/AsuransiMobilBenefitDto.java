package com.mabroor.ws.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AsuransiMobilBenefitDto {
	
	@JsonProperty("CoverageDes")
	private String CoverageDes;
	
	@JsonProperty("AmountPremi")
	private BigDecimal AmountPremi;
	
	@JsonProperty("CoverageDesDetail")
	private String CoverageDesDetail;
	
	@JsonProperty("DefaultValue")
	private String DefaultValue;
	
	@JsonProperty("SeqNo")
	private int SeqNo;
	
	private boolean isChecked;
	
	public String getCoverageDes() {
		return CoverageDes;
	}
	public void setCoverageDes(String coverageDes) {
		CoverageDes = coverageDes;
	}
	public BigDecimal getAmountPremi() {
		return AmountPremi;
	}
	public void setAmountPremi(BigDecimal amountPremi) {
		AmountPremi = amountPremi;
	}
	public String getCoverageDesDetail() {
		return CoverageDesDetail;
	}
	public void setCoverageDesDetail(String coverageDesDetail) {
		CoverageDesDetail = coverageDesDetail;
	}
	public String getDefaultValue() {
		return DefaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		DefaultValue = defaultValue;
	}
	public int getSeqNo() {
		return SeqNo;
	}
	public void setSeqNo(int seqNo) {
		SeqNo = seqNo;
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	
	
	

}
