package com.mabroor.ws.dto;

import com.mabroor.ws.entity.MstDataDiriPengajuanMultiguna;

public class RespPengajuanDto {
    private Double pendapatan;
    private Double jumlahPinjaman;
    private Integer tenor;
    private String kebutuhanPinjaman;
    private String kebutuhanPinjamanLainnya;
    private boolean isKartuKredit;
    private String bankPenerbit;
    private String status;
    private Long userId;
    private String tanggalPengajuan;
    private MstDataDiriPengajuanMultiguna dataDiri;

    public Double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(Double pendapatan) {
        this.pendapatan = pendapatan;
    }

    public Double getJumlahPinjaman() {
        return jumlahPinjaman;
    }

    public void setJumlahPinjaman(Double jumlahPinjaman) {
        this.jumlahPinjaman = jumlahPinjaman;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public String getKebutuhanPinjaman() {
        return kebutuhanPinjaman;
    }

    public void setKebutuhanPinjaman(String kebutuhanPinjaman) {
        this.kebutuhanPinjaman = kebutuhanPinjaman;
    }

    public String getKebutuhanPinjamanLainnya() {
        return kebutuhanPinjamanLainnya;
    }

    public void setKebutuhanPinjamanLainnya(String kebutuhanPinjamanLainnya) {
        this.kebutuhanPinjamanLainnya = kebutuhanPinjamanLainnya;
    }

    public MstDataDiriPengajuanMultiguna getDataDiri() {
        return dataDiri;
    }

    public void setDataDiri(MstDataDiriPengajuanMultiguna dataDiri) {
        this.dataDiri = dataDiri;
    }

    public boolean isKartuKredit() {
        return isKartuKredit;
    }

    public void setKartuKredit(boolean kartuKredit) {
        isKartuKredit = kartuKredit;
    }

    public String getBankPenerbit() {
        return bankPenerbit;
    }

    public void setBankPenerbit(String bankPenerbit) {
        this.bankPenerbit = bankPenerbit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTanggalPengajuan() {
        return tanggalPengajuan;
    }

    public void setTanggalPengajuan(String tanggalPengajuan) {
        this.tanggalPengajuan = tanggalPengajuan;
    }
}
