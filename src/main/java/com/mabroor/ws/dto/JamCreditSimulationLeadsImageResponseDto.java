package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JamCreditSimulationLeadsImageResponseDto {
	
	@JsonProperty("OUT_STAT")
	private String outStat;
	@JsonProperty("OUT_MESS")
	private String outMess;
	public String getOutStat() {
		return outStat;
	}
	public void setOutStat(String outStat) {
		this.outStat = outStat;
	}
	public String getOutMess() {
		return outMess;
	}
	public void setOutMess(String outMess) {
		this.outMess = outMess;
	}
	
	
	

}
