package com.mabroor.ws.dto;

public class HealthInsuranceProductSummaryDto {
	
	private HealthInsuranceProductSummaryRequestDto reqDto;
	private HealthInsuranceProductSummaryResponseDto respDto;
	public HealthInsuranceProductSummaryRequestDto getReqDto() {
		return reqDto;
	}
	public void setReqDto(HealthInsuranceProductSummaryRequestDto reqDto) {
		this.reqDto = reqDto;
	}
	public HealthInsuranceProductSummaryResponseDto getRespDto() {
		return respDto;
	}
	public void setRespDto(HealthInsuranceProductSummaryResponseDto respDto) {
		this.respDto = respDto;
	}
	
	

}
