package com.mabroor.ws.dto;

import lombok.Data;

import java.util.LinkedHashMap;

@Data
public class ResFifAmitraDto {
    private String errorCode;
    private String errorMsg;
    private LinkedHashMap<String, String> result;
}
