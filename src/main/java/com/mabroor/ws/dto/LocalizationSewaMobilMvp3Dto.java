package com.mabroor.ws.dto;

import java.util.List;

public class LocalizationSewaMobilMvp3Dto {
	private List<LocalizationPackageWithoutDriverDto> packageWithoutDriver;

	public List<LocalizationPackageWithoutDriverDto> getPackageWithoutDriver() {
		return packageWithoutDriver;
	}

	public void setPackageWithoutDriver(List<LocalizationPackageWithoutDriverDto> packageWithoutDriver) {
		this.packageWithoutDriver = packageWithoutDriver;
	}
}
