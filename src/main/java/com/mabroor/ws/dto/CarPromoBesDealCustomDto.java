package com.mabroor.ws.dto;

public class CarPromoBesDealCustomDto {
	private Long promoId;
	private Long carId;
	private String bestDeal;
	private String packageName;
	private String promotionName;
	public Long getPromoId() {
		return promoId;
	}
	public void setPromoId(Long promoId) {
		this.promoId = promoId;
	}
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	public String getBestDeal() {
		return bestDeal;
	}
	public void setBestDeal(String bestDeal) {
		this.bestDeal = bestDeal;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public CarPromoBesDealCustomDto(Long promoId, Long carId, String bestDeal, String packageName,
			String promotionName) {
		super();
		this.promoId = promoId;
		this.carId = carId;
		this.bestDeal = bestDeal;
		this.packageName = packageName;
		this.promotionName = promotionName;
	}
	
}
