package com.mabroor.ws.dto;

public class FIFTokenResultDto {
	
	private String accessToken;
	
	private FIFTokenResultUserDto user;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public FIFTokenResultUserDto getUser() {
		return user;
	}

	public void setUser(FIFTokenResultUserDto user) {
		this.user = user;
	}

}
