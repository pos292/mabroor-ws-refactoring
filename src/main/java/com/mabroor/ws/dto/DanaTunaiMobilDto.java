package com.mabroor.ws.dto;

public class DanaTunaiMobilDto {

	private Long id;
	private String namaKendaraan;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNamaKendaraan() {
		return namaKendaraan;
	}
	public void setNamaKendaraan(String namaKendaraan) {
		this.namaKendaraan = namaKendaraan;
	}
	
}
