package com.mabroor.ws.dto;

import com.mabroor.ws.entity.MstDataDiriPengajuanMultiguna;
import com.mabroor.ws.entity.MstPengajuanUmrohDataDiri;

public class RespPembiayaanUmrohDto {

    private String idPengajuan;
    private String statusPengajuan;
    private String jenisLayanan;
    private String partnerPembiayaan;
    private String produkPembiayaan;
    private String dp;
    private Integer tenor;

    public String getIdPengajuan() {
        return idPengajuan;
    }

    public void setIdPengajuan(String idPengajuan) {
        this.idPengajuan = idPengajuan;
    }

    public String getStatusPengajuan() {
        return statusPengajuan;
    }

    public void setStatusPengajuan(String statusPengajuan) {
        this.statusPengajuan = statusPengajuan;
    }

    public String getJenisLayanan() {
        return jenisLayanan;
    }

    public void setJenisLayanan(String jenisLayanan) {
        this.jenisLayanan = jenisLayanan;
    }

    public String getPartnerPembiayaan() {
        return partnerPembiayaan;
    }

    public void setPartnerPembiayaan(String partnerPembiayaan) {
        this.partnerPembiayaan = partnerPembiayaan;
    }

    public String getProdukPembiayaan() {
        return produkPembiayaan;
    }

    public void setProdukPembiayaan(String produkPembiayaan) {
        this.produkPembiayaan = produkPembiayaan;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }


}
