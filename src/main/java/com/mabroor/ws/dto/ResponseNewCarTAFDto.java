package com.mabroor.ws.dto;

public class ResponseNewCarTAFDto {
	
	private String status;
	
	private String message;
	
	private ResponseNewCarTafDataDto data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ResponseNewCarTafDataDto getData() {
		return data;
	}

	public void setData(ResponseNewCarTafDataDto data) {
		this.data = data;
	}

}
