package com.mabroor.ws.dto;

import java.util.List;

public class PayLaterResponseOrderDto {
	
	private String orderNo;
	private String period;
	private String applyPeriod;
	private Integer interestAmount;
	private Integer applyAmount;
	private Integer sumSettledAmount;
	private Integer sumRemainingAmount;
	private Integer sumAmount;
	private String loanNumber;
	private List<PaylaterResponseDuesDto> duesList;
	private String status;
	
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public Integer getInterestAmount() {
		return interestAmount;
	}
	public void setInterestAmount(Integer interestAmount) {
		this.interestAmount = interestAmount;
	}
	public String getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}
	public List<PaylaterResponseDuesDto> getDuesList() {
		return duesList;
	}
	public void setDuesList(List<PaylaterResponseDuesDto> duesList) {
		this.duesList = duesList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getSumSettledAmount() {
		return sumSettledAmount;
	}
	public void setSumSettledAmount(Integer sumSettledAmount) {
		this.sumSettledAmount = sumSettledAmount;
	}
	public Integer getSumRemainingAmount() {
		return sumRemainingAmount;
	}
	public void setSumRemainingAmount(Integer sumRemainingAmount) {
		this.sumRemainingAmount = sumRemainingAmount;
	}
	public Integer getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(Integer sumAmount) {
		this.sumAmount = sumAmount;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getApplyPeriod() {
		return applyPeriod;
	}
	public void setApplyPeriod(String applyPeriod) {
		this.applyPeriod = applyPeriod;
	}
	public Integer getApplyAmount() {
		return applyAmount;
	}
	public void setApplyAmount(Integer applyAmount) {
		this.applyAmount = applyAmount;
	}
	
	

}
