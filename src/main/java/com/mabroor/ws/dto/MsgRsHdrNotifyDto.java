package com.mabroor.ws.dto;

public class MsgRsHdrNotifyDto {
	private String ResponseTimestamp;
	private String CustRefID;	
	private String StatusCode;	
	private String StatusDesc;	

	public String getResponseTimestamp() {
		return ResponseTimestamp;
	}

	public void setResponseTimestamp(String responseTimestamp) {
		ResponseTimestamp = responseTimestamp;
	}

	public String getCustRefID() {
		return CustRefID;
	}

	public void setCustRefID(String custRefID) {
		CustRefID = custRefID;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

	public String getStatusDesc() {
		return StatusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		StatusDesc = statusDesc;
	}
}
