package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class CarFilterDto {
	private Long location;
	private Long brand;
	private List<Long> type;
	private BigDecimal minPrice;
	private BigDecimal maxPrice;
	private BigDecimal minCicilan;
	private BigDecimal maxCicilan;
	private List<String> transmission;
	private List<String> fuel;
	private String bestDeal;
	public Long getLocation() {
		return location;
	}
	public void setLocation(Long location) {
		this.location = location;
	}
	public Long getBrand() {
		return brand;
	}
	public void setBrand(Long brand) {
		this.brand = brand;
	}
	public List<Long> getType() {
		return type;
	}
	public void setType(List<Long> type) {
		this.type = type;
	}
	public BigDecimal getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}
	public BigDecimal getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}
	public List<String> getTransmission() {
		return transmission;
	}
	public void setTransmission(List<String> transmission) {
		this.transmission = transmission;
	}
	public List<String> getFuel() {
		return fuel;
	}
	public void setFuel(List<String> fuel) {
		this.fuel = fuel;
	}
	public String getBestDeal() {
		return bestDeal;
	}
	public void setBestDeal(String bestDeal) {
		this.bestDeal = bestDeal;
	}
	public BigDecimal getMinCicilan() {
		return minCicilan;
	}
	public void setMinCicilan(BigDecimal minCicilan) {
		this.minCicilan = minCicilan;
	}
	public BigDecimal getMaxCicilan() {
		return maxCicilan;
	}
	public void setMaxCicilan(BigDecimal maxCicilan) {
		this.maxCicilan = maxCicilan;
	}
	
}
