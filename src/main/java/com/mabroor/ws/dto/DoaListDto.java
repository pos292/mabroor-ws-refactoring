package com.mabroor.ws.dto;

import java.util.List;

public class DoaListDto {

	private List<DoaDto> doa;

	public List<DoaDto> getDoa() {
		return doa;
	}

	public void setDoa(List<DoaDto> doa) {
		this.doa = doa;
	}
	
}
