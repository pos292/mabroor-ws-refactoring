package com.mabroor.ws.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GoalAndRecommendationDataDevDto {
	
	@JsonProperty("pkey")
	private String pkey;
	
	@JsonProperty("cust_age_group")
	private String cust_age_group;
	
	@JsonProperty("cust_city")
	private String cust_city;
	
	@JsonProperty("cust_dependant_group")
	private String cust_dependant_group;
	
	@JsonProperty("cust_education")
	private String cust_education;
	
	@JsonProperty("cust_gender")
	private String cust_gender;
	
	@JsonProperty("cust_income_group")
	private String cust_income_group;
	
	@JsonProperty("cust_job")
	private String cust_job;
	
	@JsonProperty("cust_martial_status")
	private String cust_martial_status;
	
	@JsonProperty("general_insurance_likelihood")
	private String general_insurance_likelihood;
	
	@JsonProperty("life_insurance_likelihood")
	private String life_insurance_likelihood;
	
	@JsonProperty("multiguna_likelihood")
	private String multiguna_likelihood;
	
	@JsonProperty("new_car_likelihood")
	private String new_car_likelihood;
	
	@JsonProperty("new_motorcycle_likelihood")
	private String new_motorcycle_likelihood;

	public String getPkey() {
		return pkey;
	}

	public void setPkey(String pkey) {
		this.pkey = pkey;
	}

	public String getCust_age_group() {
		return cust_age_group;
	}

	public void setCust_age_group(String cust_age_group) {
		this.cust_age_group = cust_age_group;
	}

	public String getCust_city() {
		return cust_city;
	}

	public void setCust_city(String cust_city) {
		this.cust_city = cust_city;
	}

	public String getCust_dependant_group() {
		return cust_dependant_group;
	}

	public void setCust_dependant_group(String cust_dependant_group) {
		this.cust_dependant_group = cust_dependant_group;
	}

	public String getCust_education() {
		return cust_education;
	}

	public void setCust_education(String cust_education) {
		this.cust_education = cust_education;
	}

	public String getCust_gender() {
		return cust_gender;
	}

	public void setCust_gender(String cust_gender) {
		this.cust_gender = cust_gender;
	}

	public String getCust_income_group() {
		return cust_income_group;
	}

	public void setCust_income_group(String cust_income_group) {
		this.cust_income_group = cust_income_group;
	}

	public String getCust_job() {
		return cust_job;
	}

	public void setCust_job(String cust_job) {
		this.cust_job = cust_job;
	}

	public String getCust_martial_status() {
		return cust_martial_status;
	}

	public void setCust_martial_status(String cust_martial_status) {
		this.cust_martial_status = cust_martial_status;
	}

	public String getGeneral_insurance_likelihood() {
		return general_insurance_likelihood;
	}

	public void setGeneral_insurance_likelihood(String general_insurance_likelihood) {
		this.general_insurance_likelihood = general_insurance_likelihood;
	}

	public String getLife_insurance_likelihood() {
		return life_insurance_likelihood;
	}

	public void setLife_insurance_likelihood(String life_insurance_likelihood) {
		this.life_insurance_likelihood = life_insurance_likelihood;
	}

	public String getMultiguna_likelihood() {
		return multiguna_likelihood;
	}

	public void setMultiguna_likelihood(String multiguna_likelihood) {
		this.multiguna_likelihood = multiguna_likelihood;
	}

	public String getNew_car_likelihood() {
		return new_car_likelihood;
	}

	public void setNew_car_likelihood(String new_car_likelihood) {
		this.new_car_likelihood = new_car_likelihood;
	}

	public String getNew_motorcycle_likelihood() {
		return new_motorcycle_likelihood;
	}

	public void setNew_motorcycle_likelihood(String new_motorcycle_likelihood) {
		this.new_motorcycle_likelihood = new_motorcycle_likelihood;
	}
	
	
}
