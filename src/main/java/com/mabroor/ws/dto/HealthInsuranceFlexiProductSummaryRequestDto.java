package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceFlexiProductSummaryRequestDto {

	@JsonProperty("product_code")
	private String product_code;
	
	@JsonProperty("bf_code")
	private String bf_code;
	
	@JsonProperty("cust_info")
	private List<HealthInsuranceFlexiCustomerInfoDto> cust_info;

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getBf_code() {
		return bf_code;
	}

	public void setBf_code(String bf_code) {
		this.bf_code = bf_code;
	}

	public List<HealthInsuranceFlexiCustomerInfoDto> getCust_info() {
		return cust_info;
	}

	public void setCust_info(List<HealthInsuranceFlexiCustomerInfoDto> cust_info) {
		this.cust_info = cust_info;
	}
	
	
	

	
	
}
