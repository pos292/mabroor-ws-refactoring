package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DropLocationMGU4WDto {
	
	private String cityId;
	private double longitude;
	private double lat;	
	private String alamat;
	private String time;
	private String notes;
	private List<RentCarTracPriceExpDto> priceExpedition;
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public List<RentCarTracPriceExpDto> getPriceExpedition() {
		return priceExpedition;
	}
	public void setPriceExpedition(List<RentCarTracPriceExpDto> priceExpedition) {
		this.priceExpedition = priceExpedition;
	}
	
	

}
