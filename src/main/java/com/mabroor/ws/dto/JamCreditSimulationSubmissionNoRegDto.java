package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JamCreditSimulationSubmissionNoRegDto {
	
	@JsonProperty("NOREG_LEADS")
	private String noRegLeads;

	public String getNoRegLeads() {
		return noRegLeads;
	}

	public void setNoRegLeads(String noRegLeads) {
		this.noRegLeads = noRegLeads;
	}
	
	

}
