package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SportInsurancePremiumPackageDto {
	
	@JsonProperty("eligible_bill_freq")
	private List<SportInsuranceEligibleDto> eligible_bill_freq;
	
	@JsonProperty("sum_assured")
	private String sum_assured;
	
	@JsonProperty("term")
	private String term;

	public List<SportInsuranceEligibleDto> getEligible_bill_freq() {
		return eligible_bill_freq;
	}

	public void setEligible_bill_freq(List<SportInsuranceEligibleDto> eligible_bill_freq) {
		this.eligible_bill_freq = eligible_bill_freq;
	}

	public String getSum_assured() {
		return sum_assured;
	}

	public void setSum_assured(String sum_assured) {
		this.sum_assured = sum_assured;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}
}
