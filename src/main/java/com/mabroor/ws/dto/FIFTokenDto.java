package com.mabroor.ws.dto;

public class FIFTokenDto {
	
	private Long errorCode;
	
	private String errorMsg;
	
	private FIFTokenResultDto result;

	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public FIFTokenResultDto getResult() {
		return result;
	}

	public void setResult(FIFTokenResultDto result) {
		this.result = result;
	}
	
}
