package com.mabroor.ws.dto;

import lombok.*;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RespMessageDto<T> {
    private String reqId;
    private String status;
    private String error;
    private T message;
    private List<?> Data;

}
