package com.mabroor.ws.dto;

public class SearchParamDto {
	private Integer pageNumber;
	private Integer pageSize;
	private String orderDirection;
	private Integer orderColumnNo;
	private String[] orderColumnNames;
	private String searchValue;

	public SearchParamDto() {
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	public Integer getOrderColumnNo() {
		return orderColumnNo;
	}

	public void setOrderColumnNo(Integer orderColumnNo) {
		this.orderColumnNo = orderColumnNo;
	}

	public String[] getOrderColumnNames() {
		return orderColumnNames;
	}

	public void setOrderColumnNames(String[] orderColumnNames) {
		this.orderColumnNames = orderColumnNames;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
}
