package com.mabroor.ws.dto;

public class RentCarCallbackActivityDto {

	private String bookingOrderId;
	
	private String activityId;
	
	private String activityDesc;

	public String getBookingOrderId() {
		return bookingOrderId;
	}

	public void setBookingOrderId(String bookingOrderId) {
		this.bookingOrderId = bookingOrderId;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getActivityDesc() {
		return activityDesc;
	}

	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}
}
