package com.mabroor.ws.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GoalAndRecommendationResponseDevDto {
	
	@JsonProperty("timestamp")
	private Date timestamp;
	
	@JsonProperty("rqId")
	private String rqId;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("error")
	private String error;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("data")
	private List<GoalAndRecommendationDataDevDto> data;

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getRqId() {
		return rqId;
	}

	public void setRqId(String rqId) {
		this.rqId = rqId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<GoalAndRecommendationDataDevDto> getData() {
		return data;
	}

	public void setData(List<GoalAndRecommendationDataDevDto> data) {
		this.data = data;
	}
	
	

}
