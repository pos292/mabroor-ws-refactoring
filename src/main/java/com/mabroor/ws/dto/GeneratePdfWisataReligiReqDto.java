package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.Date;

public class GeneratePdfWisataReligiReqDto {

	private String name;
	private String productPurchase;
	private String tenor;
	private String productChoose;
	private String dateDispatch;

	private BigDecimal amountCicilan;
	private String jumlahPeserta;
	private String lokasiCabang;
	private String email;
	private String noHandphone;
	private String tanggalLahir;
	private String namaDepan;
	private String namaBelakang;
	private BigDecimal amountCostTotal;
	private String partner;
	private BigDecimal dp;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductPurchase() {
		return productPurchase;
	}
	public void setProductPurchase(String productPurchase) {
		this.productPurchase = productPurchase;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getProductChoose() {
		return productChoose;
	}
	public void setProductChoose(String productChoose) {
		this.productChoose = productChoose;
	}
	public String getDateDispatch() {
		return dateDispatch;
	}
	public void setDateDispatch(String dateDispatch) {
		this.dateDispatch = dateDispatch;
	}
	public BigDecimal getAmountCicilan() {
		return amountCicilan;
	}
	public void setAmountCicilan(BigDecimal amountCicilan) {
		this.amountCicilan = amountCicilan;
	}
	public String getJumlahPeserta() {
		return jumlahPeserta;
	}
	public void setJumlahPeserta(String jumlahPeserta) {
		this.jumlahPeserta = jumlahPeserta;
	}
	public String getLokasiCabang() {
		return lokasiCabang;
	}
	public void setLokasiCabang(String lokasiCabang) {
		this.lokasiCabang = lokasiCabang;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNoHandphone() {
		return noHandphone;
	}
	public void setNoHandphone(String noHandphone) {
		this.noHandphone = noHandphone;
	}
	public String getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public String getNamaDepan() {
		return namaDepan;
	}
	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}
	public String getNamaBelakang() {
		return namaBelakang;
	}
	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}
	public BigDecimal getAmountCostTotal() {
		return amountCostTotal;
	}
	public void setAmountCostTotal(BigDecimal amountCostTotal) {
		this.amountCostTotal = amountCostTotal;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public BigDecimal getDp() {
		return dp;
	}
	public void setDp(BigDecimal dp) {
		this.dp = dp;
	}
	
}
