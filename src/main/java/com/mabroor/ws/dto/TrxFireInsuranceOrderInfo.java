package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxFireInsuranceOrderInfo {

	@JsonProperty("BulkOrderNo")
	private String bulkOrderNo;
	
	@JsonProperty("TransactionID")
	private String transactionId;
	
	@JsonProperty("BankIDXCode")
	private String bankIdxCode;
	
	@JsonProperty("CustomerFirstName")
	private String customerFirstName;
	
	@JsonProperty("CustomerLastName")
	private String customerLastName;
	
	@JsonProperty("PaymentMethodCode")
	private String paymentMethodCode;
	
	@JsonProperty("VANumber")
	private String vaNumber;
	
	@JsonProperty("VACode")
	private String vaCode;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getBankIdxCode() {
		return bankIdxCode;
	}

	public void setBankIdxCode(String bankIdxCode) {
		this.bankIdxCode = bankIdxCode;
	}
	
	public String getBulkOrderNo() {
		return bulkOrderNo;
	}

	public void setBulkOrderNo(String bulkOrderNo) {
		this.bulkOrderNo = bulkOrderNo;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getVaNumber() {
		return vaNumber;
	}

	public void setVaNumber(String vaNumber) {
		this.vaNumber = vaNumber;
	}

	public String getVaCode() {
		return vaCode;
	}

	public void setVaCode(String vaCode) {
		this.vaCode = vaCode;
	}

	
}
