package com.mabroor.ws.dto;

import java.math.BigDecimal;

public class LoanManagementDto {

	private Long id;
	private String description;
	private String title;
	private String subtitle;
	private String loanType;
	private BigDecimal price;
	private String image;
	private String loanManagementStatus;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLoanManagementStatus() {
		return loanManagementStatus;
	}
	public void setLoanManagementStatus(String loanManagementStatus) {
		this.loanManagementStatus = loanManagementStatus;
	}
	
	
	
}
