package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceFlexiCustomerInfoDto {
	
	@JsonProperty("dob")
	private String dob;
	
	@JsonProperty("gender_code")
	private String gender_code;
	
	@JsonProperty("rel_code")
	private String rel_code;
	
	@JsonProperty("role_code")
	private String role_code;
	
	@JsonProperty("life_index")
	private String life_index;
	
	@JsonProperty("contract_structures")
	private List<HealthInsuranceFlexiContractStructuresDto> contract_structures;

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender_code() {
		return gender_code;
	}

	public void setGender_code(String gender_code) {
		this.gender_code = gender_code;
	}

	public String getRel_code() {
		return rel_code;
	}

	public void setRel_code(String rel_code) {
		this.rel_code = rel_code;
	}

	public String getRole_code() {
		return role_code;
	}

	public void setRole_code(String role_code) {
		this.role_code = role_code;
	}

	public String getLife_index() {
		return life_index;
	}

	public void setLife_index(String life_index) {
		this.life_index = life_index;
	}

	public List<HealthInsuranceFlexiContractStructuresDto> getContract_structures() {
		return contract_structures;
	}

	public void setContract_structures(List<HealthInsuranceFlexiContractStructuresDto> contract_structures) {
		this.contract_structures = contract_structures;
	}

	
	
	

}
