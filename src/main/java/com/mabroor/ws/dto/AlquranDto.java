package com.mabroor.ws.dto;

import java.util.List;

public class AlquranDto {
	
	private List<AlquranSurahDto> listSurah;
	private List<AlquranJuzDto> listJuz;
	
	public List<AlquranSurahDto> getListSurah() {
		return listSurah;
	}
	public void setListSurah(List<AlquranSurahDto> listSurah) {
		this.listSurah = listSurah;
	}
	public List<AlquranJuzDto> getListJuz() {
		return listJuz;
	}
	public void setListJuz(List<AlquranJuzDto> listJuz) {
		this.listJuz = listJuz;
	}
	
	
	

}
