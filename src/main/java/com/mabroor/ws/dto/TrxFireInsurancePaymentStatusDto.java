package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxFireInsurancePaymentStatusDto {
	
	@JsonProperty("Status")
	private boolean status;
	
	@JsonProperty("CheckPaymentStatus")
	private TrxFireInsuranceCheckPaymentStatus checkPaymentStatus;
	
	@JsonProperty("OrderInfo")
	private List<TrxFireInsuranceOrderInfo> orderInfo;
	
	private String message;
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public TrxFireInsuranceCheckPaymentStatus getCheckPaymentStatus() {
		return checkPaymentStatus;
	}
	public void setCheckPaymentStatus(TrxFireInsuranceCheckPaymentStatus checkPaymentStatus) {
		this.checkPaymentStatus = checkPaymentStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<TrxFireInsuranceOrderInfo> getOrderInfo() {
		return orderInfo;
	}
	public void setOrderInfo(List<TrxFireInsuranceOrderInfo> orderInfo) {
		this.orderInfo = orderInfo;
	}

}
