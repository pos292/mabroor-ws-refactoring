package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceProductSummaryRequestDto {

	@JsonProperty("product_code")
	private String product_code;
	
	@JsonProperty("package_code")
	private String package_code;
	
	@JsonProperty("sum_assured")
	private String sum_assured;
	
	@JsonProperty("term_in_days")
	private String term_in_days;
	
	@JsonProperty("delivery_flag")
	private String delivery_flag;
	
	@JsonProperty("agent_code")
	private String agent_code;
	
	@JsonProperty("bf_code")
	private String bf_code;
	
	@JsonProperty("options")
	private List<HealthInsuranceOptionsDto> options;
	
	@JsonProperty("cust_info")
	private List<HealthInsuranceCustomerInfoDto> cust_info;
	
	@JsonProperty("optional_rider")
	private List<HealthInsuranceOptionalRiderDto> optional_rider;

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getPackage_code() {
		return package_code;
	}

	public void setPackage_code(String package_code) {
		this.package_code = package_code;
	}

	public String getSum_assured() {
		return sum_assured;
	}

	public void setSum_assured(String sum_assured) {
		this.sum_assured = sum_assured;
	}

	public String getTerm_in_days() {
		return term_in_days;
	}

	public void setTerm_in_days(String term_in_days) {
		this.term_in_days = term_in_days;
	}

	public String getDelivery_flag() {
		return delivery_flag;
	}

	public void setDelivery_flag(String delivery_flag) {
		this.delivery_flag = delivery_flag;
	}

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getBf_code() {
		return bf_code;
	}

	public void setBf_code(String bf_code) {
		this.bf_code = bf_code;
	}

	public List<HealthInsuranceOptionsDto> getOptions() {
		return options;
	}

	public void setOptions(List<HealthInsuranceOptionsDto> options) {
		this.options = options;
	}

	public List<HealthInsuranceCustomerInfoDto> getCust_info() {
		return cust_info;
	}

	public void setCust_info(List<HealthInsuranceCustomerInfoDto> cust_info) {
		this.cust_info = cust_info;
	}

	public List<HealthInsuranceOptionalRiderDto> getOptional_rider() {
		return optional_rider;
	}

	public void setOptional_rider(List<HealthInsuranceOptionalRiderDto> optional_rider) {
		this.optional_rider = optional_rider;
	}

	
}
