package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.Date;

public class GeneratePdfAsuransiKebakaranReqDto {

	private String name;
	private String namePemegangPolis;
	private String jenisPaketPerlindungan;
	private String kepemilikanBangunan;
	private BigDecimal hargaBangunan;
	private String alamat;
	private BigDecimal premiDasar;
	private BigDecimal biayaAdmin;
	private BigDecimal totalPremi;
	private String periodePertanggunan;
	private String tanggalLahir;
	private String phone;
	private String noIdentitas;
	private String kodePos;
	private String alamatRisiko;
	private String email;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getNamePemegangPolis() {
		return namePemegangPolis;
	}
	public void setNamePemegangPolis(String namePemegangPolis) {
		this.namePemegangPolis = namePemegangPolis;
	}
	public String getJenisPaketPerlindungan() {
		return jenisPaketPerlindungan;
	}
	public void setJenisPaketPerlindungan(String jenisPaketPerlindungan) {
		this.jenisPaketPerlindungan = jenisPaketPerlindungan;
	}
	public String getKepemilikanBangunan() {
		return kepemilikanBangunan;
	}
	public void setKepemilikanBangunan(String kepemilikanBangunan) {
		this.kepemilikanBangunan = kepemilikanBangunan;
	}
	public BigDecimal getHargaBangunan() {
		return hargaBangunan;
	}
	public void setHargaBangunan(BigDecimal hargaBangunan) {
		this.hargaBangunan = hargaBangunan;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public BigDecimal getPremiDasar() {
		return premiDasar;
	}
	public void setPremiDasar(BigDecimal premiDasar) {
		this.premiDasar = premiDasar;
	}
	public BigDecimal getBiayaAdmin() {
		return biayaAdmin;
	}
	public void setBiayaAdmin(BigDecimal biayaAdmin) {
		this.biayaAdmin = biayaAdmin;
	}
	public BigDecimal getTotalPremi() {
		return totalPremi;
	}
	public void setTotalPremi(BigDecimal totalPremi) {
		this.totalPremi = totalPremi;
	}
	public String getPeriodePertanggunan() {
		return periodePertanggunan;
	}
	public void setPeriodePertanggunan(String periodePertanggunan) {
		this.periodePertanggunan = periodePertanggunan;
	}
	public String getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNoIdentitas() {
		return noIdentitas;
	}
	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}
	public String getKodePos() {
		return kodePos;
	}
	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}
	public String getAlamatRisiko() {
		return alamatRisiko;
	}
	public void setAlamatRisiko(String alamatRisiko) {
		this.alamatRisiko = alamatRisiko;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	 	
}
