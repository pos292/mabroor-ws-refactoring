package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class GeneratePdfAsuransiMobilReqDto {

	private String name;
	private String jenisAsuransi;
	private String productTitle;
	private String kendaraan;
	private String kodeWilayahPlat;
	private BigDecimal sumIssured;
	private String periodePertanggungan;
	private BigDecimal kontribusiDasar;
	private BigDecimal kecelakaanDiriPengemudi;
	private BigDecimal estimasiTotal;
	private String perluasanJaminan;
	private List<AsuransiMobilBenefitDto> benefit;
	private String lainLain;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getJenisAsuransi() {
		return jenisAsuransi;
	}
	public void setJenisAsuransi(String jenisAsuransi) {
		this.jenisAsuransi = jenisAsuransi;
	}
	public String getProductTitle() {
		return productTitle;
	}
	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	public String getKendaraan() {
		return kendaraan;
	}
	public void setKendaraan(String kendaraan) {
		this.kendaraan = kendaraan;
	}
	public String getKodeWilayahPlat() {
		return kodeWilayahPlat;
	}
	public void setKodeWilayahPlat(String kodeWilayahPlat) {
		this.kodeWilayahPlat = kodeWilayahPlat;
	}
	public BigDecimal getSumIssured() {
		return sumIssured;
	}
	public void setSumIssured(BigDecimal sumIssured) {
		this.sumIssured = sumIssured;
	}
	public String getPeriodePertanggungan() {
		return periodePertanggungan;
	}
	public void setPeriodePertanggungan(String periodePertanggungan) {
		this.periodePertanggungan = periodePertanggungan;
	}
	public BigDecimal getKontribusiDasar() {
		return kontribusiDasar;
	}
	public void setKontribusiDasar(BigDecimal kontribusiDasar) {
		this.kontribusiDasar = kontribusiDasar;
	}
	public BigDecimal getKecelakaanDiriPengemudi() {
		return kecelakaanDiriPengemudi;
	}
	public void setKecelakaanDiriPengemudi(BigDecimal kecelakaanDiriPengemudi) {
		this.kecelakaanDiriPengemudi = kecelakaanDiriPengemudi;
	}
	public BigDecimal getEstimasiTotal() {
		return estimasiTotal;
	}
	public void setEstimasiTotal(BigDecimal estimasiTotal) {
		this.estimasiTotal = estimasiTotal;
	}
	public String getPerluasanJaminan() {
		return perluasanJaminan;
	}
	public void setPerluasanJaminan(String perluasanJaminan) {
		this.perluasanJaminan = perluasanJaminan;
	}
	public String getLainLain() {
		return lainLain;
	}
	public void setLainLain(String lainLain) {
		this.lainLain = lainLain;
	}
	public List<AsuransiMobilBenefitDto> getBenefit() {
		return benefit;
	}
	public void setBenefit(List<AsuransiMobilBenefitDto> benefit) {
		this.benefit = benefit;
	}
}
