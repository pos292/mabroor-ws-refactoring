package com.mabroor.ws.dto;

public class MoxaMabroorAccountBodyDto {
	
	private Long userIdReference;
	private String userMobileUuid;
	public Long getUserIdReference() {
		return userIdReference;
	}
	public void setUserIdReference(Long userIdReference) {
		this.userIdReference = userIdReference;
	}
	public String getUserMobileUuid() {
		return userMobileUuid;
	}
	public void setUserMobileUuid(String userMobileUuid) {
		this.userMobileUuid = userMobileUuid;
	}
	
	

}
