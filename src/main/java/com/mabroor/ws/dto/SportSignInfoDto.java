package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SportSignInfoDto {
	
	@JsonProperty("spajLastSign")
	private String spajLastSign;
	
	@JsonProperty("sppnLastSign")
	private String sppnLastSign;

	public String getSpajLastSign() {
		return spajLastSign;
	}

	public void setSpajLastSign(String spajLastSign) {
		this.spajLastSign = spajLastSign;
	}

	public String getSppnLastSign() {
		return sppnLastSign;
	}

	public void setSppnLastSign(String sppnLastSign) {
		this.sppnLastSign = sppnLastSign;
	}
	
	

}
