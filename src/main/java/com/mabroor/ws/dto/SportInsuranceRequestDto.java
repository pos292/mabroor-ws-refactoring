package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SportInsuranceRequestDto {

	@JsonProperty("packages")
	private List<SportInsurancePackageRequestDto> packages;

	public List<SportInsurancePackageRequestDto> getPackages() {
		return packages;
	}

	public void setPackages(List<SportInsurancePackageRequestDto> packages) {
		this.packages = packages;
	}
}
