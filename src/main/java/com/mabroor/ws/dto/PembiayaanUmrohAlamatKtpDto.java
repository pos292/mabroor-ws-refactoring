package com.mabroor.ws.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
public class PembiayaanUmrohAlamatKtpDto {
    @NotBlank(message = "alamatLengkap tidak boleh kosong")
    @Size(max = 130)
    private String alamatLengkap;
    @NotBlank(message = "kodePos tidak boleh kosong")
    private String kodePos;

    public String getAlamatLengkap() {
        return alamatLengkap;
    }

    public void setAlamatLengkap(String alamatLengkap) {
        this.alamatLengkap = alamatLengkap;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }
}
