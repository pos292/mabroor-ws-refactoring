package com.mabroor.ws.dto;

public interface CarCompanyBrandDto {

	public Long getId();
	
	public String getBrand();
	
	public String getCompany();
}
