package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarCreditSimulationSubmissionResponseDto {
	
	@JsonProperty("OUT_STAT")
	private String outStat;
	@JsonProperty("OUT_MESS")
	private String outMess;
	@JsonProperty("OUT_DATA")
	private List<CarCreditSimulationSubmissionNoRegDto> outData;
	public String getOutStat() {
		return outStat;
	}
	public void setOutStat(String outStat) {
		this.outStat = outStat;
	}
	public String getOutMess() {
		return outMess;
	}
	public void setOutMess(String outMess) {
		this.outMess = outMess;
	}
	public List<CarCreditSimulationSubmissionNoRegDto> getOutData() {
		return outData;
	}
	public void setOutData(List<CarCreditSimulationSubmissionNoRegDto> outData) {
		this.outData = outData;
	}
	
	
	
	

}
