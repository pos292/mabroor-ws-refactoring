package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class MotorFilterDto {
	private Long category;
	private List<Long> type;
	private BigDecimal minPrice;
	private BigDecimal maxPrice;
	private BigDecimal minCicilan;
	private BigDecimal maxCicilan;
	public Long getCategory() {
		return category;
	}
	public void setCategory(Long category) {
		this.category = category;
	}
	public List<Long> getType() {
		return type;
	}
	public void setType(List<Long> type) {
		this.type = type;
	}
	public BigDecimal getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}
	public BigDecimal getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}
	public BigDecimal getMinCicilan() {
		return minCicilan;
	}
	public void setMinCicilan(BigDecimal minCicilan) {
		this.minCicilan = minCicilan;
	}
	public BigDecimal getMaxCicilan() {
		return maxCicilan;
	}
	public void setMaxCicilan(BigDecimal maxCicilan) {
		this.maxCicilan = maxCicilan;
	}
	
}
