package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SportInsuranceEligibleDto {

	@JsonProperty("bf_code")
	private String bf_code;
	
	@JsonProperty("bf_name")
	private String bf_name;
	
	@JsonProperty("components")
	private List<SportInsuranceComponentsDto> components;
	
	@JsonProperty("bf_interval_in_month")
	private String bf_interval_in_month;

	public String getBf_code() {
		return bf_code;
	}

	public void setBf_code(String bf_code) {
		this.bf_code = bf_code;
	}

	public String getBf_name() {
		return bf_name;
	}

	public void setBf_name(String bf_name) {
		this.bf_name = bf_name;
	}

	public List<SportInsuranceComponentsDto> getComponents() {
		return components;
	}

	public void setComponents(List<SportInsuranceComponentsDto> components) {
		this.components = components;
	}

	public String getBf_interval_in_month() {
		return bf_interval_in_month;
	}

	public void setBf_interval_in_month(String bf_interval_in_month) {
		this.bf_interval_in_month = bf_interval_in_month;
	}
}
