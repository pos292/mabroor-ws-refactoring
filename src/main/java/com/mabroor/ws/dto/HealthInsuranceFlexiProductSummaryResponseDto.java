package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceFlexiProductSummaryResponseDto {
	
	@JsonProperty("benefits")
	private List<HealthInsuranceFlexiBenefitDto> benefits;
	
	@JsonProperty("bf_code")
	private String bf_code;
	
	@JsonProperty("basic_product_name")
	private String basic_product_name;
	
	@JsonProperty("basic_term")
	private String basic_term;
	
	@JsonProperty("basic_term_years")
	private String basic_term_years;
	
	@JsonProperty("basic_premium_calculated")
	private String basic_premium_calculated;
	
	@JsonProperty("basic_premium_before_calculated")
	private String basic_premium_before_calculated;
	
	@JsonProperty("basic_sum_assured")
	private String basic_sum_assured;
	
	@JsonProperty("total_premium_rider")
	private String total_premium_rider;
	
	@JsonProperty("total_premium")
	private String total_premium;
	
	@JsonProperty("total_premium_before_discount")
	private String total_premium_before_discount;
	
	@JsonProperty("risk_cessation_date")
	private String risk_cessation_date;
	
	@JsonProperty("summary_description")
	private List<String> summary_description;

	public List<HealthInsuranceFlexiBenefitDto> getBenefits() {
		return benefits;
	}

	public void setBenefits(List<HealthInsuranceFlexiBenefitDto> benefits) {
		this.benefits = benefits;
	}

	public String getBf_code() {
		return bf_code;
	}

	public void setBf_code(String bf_code) {
		this.bf_code = bf_code;
	}

	public String getBasic_product_name() {
		return basic_product_name;
	}

	public void setBasic_product_name(String basic_product_name) {
		this.basic_product_name = basic_product_name;
	}

	public String getBasic_term() {
		return basic_term;
	}

	public void setBasic_term(String basic_term) {
		this.basic_term = basic_term;
	}

	public String getBasic_term_years() {
		return basic_term_years;
	}

	public void setBasic_term_years(String basic_term_years) {
		this.basic_term_years = basic_term_years;
	}

	public String getBasic_premium_calculated() {
		return basic_premium_calculated;
	}

	public void setBasic_premium_calculated(String basic_premium_calculated) {
		this.basic_premium_calculated = basic_premium_calculated;
	}

	public String getBasic_premium_before_calculated() {
		return basic_premium_before_calculated;
	}

	public void setBasic_premium_before_calculated(String basic_premium_before_calculated) {
		this.basic_premium_before_calculated = basic_premium_before_calculated;
	}

	public String getBasic_sum_assured() {
		return basic_sum_assured;
	}

	public void setBasic_sum_assured(String basic_sum_assured) {
		this.basic_sum_assured = basic_sum_assured;
	}

	public String getTotal_premium_rider() {
		return total_premium_rider;
	}

	public void setTotal_premium_rider(String total_premium_rider) {
		this.total_premium_rider = total_premium_rider;
	}

	public String getTotal_premium() {
		return total_premium;
	}

	public void setTotal_premium(String total_premium) {
		this.total_premium = total_premium;
	}

	public String getTotal_premium_before_discount() {
		return total_premium_before_discount;
	}

	public void setTotal_premium_before_discount(String total_premium_before_discount) {
		this.total_premium_before_discount = total_premium_before_discount;
	}

	public String getRisk_cessation_date() {
		return risk_cessation_date;
	}

	public void setRisk_cessation_date(String risk_cessation_date) {
		this.risk_cessation_date = risk_cessation_date;
	}

	public List<String> getSummary_description() {
		return summary_description;
	}

	public void setSummary_description(List<String> summary_description) {
		this.summary_description = summary_description;
	}

	

}
