package com.mabroor.ws.dto;

public class MguResponse2wDto {

	private Integer errorCode;
	private String errorMsg;
	private Result2wMguDto result;
	
	
	
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public Result2wMguDto getResult() {
		return result;
	}
	public void setResult(Result2wMguDto result) {
		this.result = result;
	}
	
	
	
}
