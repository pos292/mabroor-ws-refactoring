package com.mabroor.ws.dto;

import java.math.BigDecimal;

public class GeneratePdfTanpaJaminanReqDto {

	private String name;
	private BigDecimal nominalLoan;
	private String loanNeeds;
	private String tenor;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getNominalLoan() {
		return nominalLoan;
	}
	public void setNominalLoan(BigDecimal nominalLoan) {
		this.nominalLoan = nominalLoan;
	}
	public String getLoanNeeds() {
		return loanNeeds;
	}
	public void setLoanNeeds(String loanNeeds) {
		this.loanNeeds = loanNeeds;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	
}
