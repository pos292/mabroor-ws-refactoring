package com.mabroor.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReqAmitraSubmission {

    private String callVia;
    private String contactDetail;
    private String paketCode;
    private String paketValue;
    private String tenor;
    private String dp;
    private String produkPembiayaan;
    private String fullname;
    private String noKTP;
    private String mobilePhone1;
    private String mobilePhone2;
    private String email;
    private String birthdate;
    private String birthPlace;
    private String gender;
    private String maritalStatus;
    private String homeOwnership;
    private String education;
    private String occupationType;
    private String occupation;
    private String jobPosition;
    private String monthlyIncome;
    private String addressLine;
    private String noRT;
    private String noRW;
    private String stateId;
    private String cityId;
    private String districtId;
    private String subDistrictId;
    private String zipcode;
    private String subZipcode;
    private String stateName;
    private String cityName;
    private String districtName;
    private String subDistrictName;
    private String referral;
    private String userKey;
    private String latitude;
    private String longitude;
    private String motherName;
}
