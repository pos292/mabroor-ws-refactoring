package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxCarInsuranceOrderInfo {
	
	@JsonProperty("PolicyOrderNo")
	private String policyOrderNo;
	
	@JsonProperty("TransactionID")
	private String transactionId;
	
	@JsonProperty("BankIDXCode")
	private String bankIdxCode;
	
	@JsonProperty("CustomerName")
	private String customerName;
	
	@JsonProperty("PaymentMethodCode")
	private String paymentMethodCode;
	
	@JsonProperty("VANumber")
	private String vaNumber;
	
	@JsonProperty("VACode")
	private String vaCode;
	
	@JsonProperty("Nominal")
	private Integer nominal;

	public String getPolicyOrderNo() {
		return policyOrderNo;
	}

	public void setPolicyOrderNo(String policyOrderNo) {
		this.policyOrderNo = policyOrderNo;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getBankIdxCode() {
		return bankIdxCode;
	}

	public void setBankIdxCode(String bankIdxCode) {
		this.bankIdxCode = bankIdxCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getVaNumber() {
		return vaNumber;
	}

	public void setVaNumber(String vaNumber) {
		this.vaNumber = vaNumber;
	}

	public String getVaCode() {
		return vaCode;
	}

	public void setVaCode(String vaCode) {
		this.vaCode = vaCode;
	}

	public Integer getNominal() {
		return nominal;
	}

	public void setNominal(Integer nominal) {
		this.nominal = nominal;
	}
	

}
