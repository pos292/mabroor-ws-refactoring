package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class RespPembiayaanUmrohDetailDto {
    private String partnerPembiayaan;
    private String produkPembiayaan;
    private String dp;
    private Integer tenor;

    private RespPembiayaanUmrohDetaiDataDiriDto respPembiayaanUmrohDetaiDataDiriDto;
    private RespPembiayaanUmrohDetailAlamatDto respPembiayaanUmrohDetailAlamatDto;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private RespPembiayaanUmrohDetailAlamatKtpDto respPembiayaanUmrohDetailAlamatKtpDto;

    public String getPartnerPembiayaan() {
        return partnerPembiayaan;
    }

    public void setPartnerPembiayaan(String partnerPembiayaan) {
        this.partnerPembiayaan = partnerPembiayaan;
    }

    public String getProdukPembiayaan() {
        return produkPembiayaan;
    }

    public void setProdukPembiayaan(String produkPembiayaan) {
        this.produkPembiayaan = produkPembiayaan;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public RespPembiayaanUmrohDetaiDataDiriDto getRespPembiayaanUmrohDetaiDataDiriDto() {
        return respPembiayaanUmrohDetaiDataDiriDto;
    }

    public void setRespPembiayaanUmrohDetaiDataDiriDto(RespPembiayaanUmrohDetaiDataDiriDto respPembiayaanUmrohDetaiDataDiriDto) {
        this.respPembiayaanUmrohDetaiDataDiriDto = respPembiayaanUmrohDetaiDataDiriDto;
    }

    public RespPembiayaanUmrohDetailAlamatDto getRespPembiayaanUmrohDetailAlamatDto() {
        return respPembiayaanUmrohDetailAlamatDto;
    }

    public void setRespPembiayaanUmrohDetailAlamatDto(RespPembiayaanUmrohDetailAlamatDto respPembiayaanUmrohDetailAlamatDto) {
        this.respPembiayaanUmrohDetailAlamatDto = respPembiayaanUmrohDetailAlamatDto;
    }

    public RespPembiayaanUmrohDetailAlamatKtpDto getRespPembiayaanUmrohDetailAlamatKtpDto() {
        return respPembiayaanUmrohDetailAlamatKtpDto;
    }

    public void setRespPembiayaanUmrohDetailAlamatKtpDto(RespPembiayaanUmrohDetailAlamatKtpDto respPembiayaanUmrohDetailAlamatKtpDto) {
        this.respPembiayaanUmrohDetailAlamatKtpDto = respPembiayaanUmrohDetailAlamatKtpDto;
    }
}
