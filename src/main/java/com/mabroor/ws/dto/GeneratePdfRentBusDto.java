package com.mabroor.ws.dto;

public class GeneratePdfRentBusDto {
	
	private String name;
	private String dalamKota;
	private String alamatJemput;
	private String alamatDestinasi;
	private String pickUpTime;
	private String durasiSewa;
	private String jumlahPenumpang;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDalamKota() {
		return dalamKota;
	}
	public void setDalamKota(String dalamKota) {
		this.dalamKota = dalamKota;
	}
	public String getAlamatJemput() {
		return alamatJemput;
	}
	public void setAlamatJemput(String alamatJemput) {
		this.alamatJemput = alamatJemput;
	}
	public String getAlamatDestinasi() {
		return alamatDestinasi;
	}
	public void setAlamatDestinasi(String alamatDestinasi) {
		this.alamatDestinasi = alamatDestinasi;
	}
	public String getPickUpTime() {
		return pickUpTime;
	}
	public void setPickUpTime(String pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	public String getDurasiSewa() {
		return durasiSewa;
	}
	public void setDurasiSewa(String durasiSewa) {
		this.durasiSewa = durasiSewa;
	}
	public String getJumlahPenumpang() {
		return jumlahPenumpang;
	}
	public void setJumlahPenumpang(String jumlahPenumpang) {
		this.jumlahPenumpang = jumlahPenumpang;
	}
	
	

}
