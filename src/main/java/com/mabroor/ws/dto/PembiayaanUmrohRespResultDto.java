package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class PembiayaanUmrohRespResultDto<T> {

    private String reqid;

    private String error;

    private String status;

    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public String getReqid() {
        return reqid;
    }

    public void setReqid(String reqid) {
        this.reqid = reqid;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
