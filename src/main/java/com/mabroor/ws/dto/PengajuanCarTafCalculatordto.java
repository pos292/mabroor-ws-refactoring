package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PengajuanCarTafCalculatordto {
	
	@JsonProperty("INSCO_BRANCH_ID")
	private String INSCO_BRANCH_ID;
	
	@JsonProperty("RESPONSE")
	private String RESPONSE;
	
	@JsonProperty("NOTE")
	private String NOTE;
	
	@JsonProperty("Detail")
	private List<PengajuanCarTafCalculatorDetaildto> Detail;

	public String getINSCO_BRANCH_ID() {
		return INSCO_BRANCH_ID;
	}

	public void setINSCO_BRANCH_ID(String iNSCO_BRANCH_ID) {
		INSCO_BRANCH_ID = iNSCO_BRANCH_ID;
	}

	public String getRESPONSE() {
		return RESPONSE;
	}

	public void setRESPONSE(String rESPONSE) {
		RESPONSE = rESPONSE;
	}

	public String getNOTE() {
		return NOTE;
	}

	public void setNOTE(String nOTE) {
		NOTE = nOTE;
	}

	public List<PengajuanCarTafCalculatorDetaildto> getDetail() {
		return Detail;
	}

	public void setDetail(List<PengajuanCarTafCalculatorDetaildto> detail) {
		Detail = detail;
	}
}
