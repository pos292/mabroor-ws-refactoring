package com.mabroor.ws.dto;

public class RespPembiayaanUmrohDetailAlamatKtpDto {

    private String alamatLengkap;

    private String kodePos;

    public String getAlamatLengkap() {
        return alamatLengkap;
    }

    public void setAlamatLengkap(String alamatLengkap) {
        this.alamatLengkap = alamatLengkap;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }
}
