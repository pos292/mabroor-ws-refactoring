package com.mabroor.ws.dto;

import java.util.List;

public class LocalizationDto {
	private LocalizationSewaMobilMvp3Dto sewaMobilMvp3;

	public LocalizationSewaMobilMvp3Dto getSewaMobilMvp3() {
		return sewaMobilMvp3;
	}

	public void setSewaMobilMvp3(LocalizationSewaMobilMvp3Dto sewaMobilMvp3) {
		this.sewaMobilMvp3 = sewaMobilMvp3;
	}
	
}
