package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceRiderDto {
	
	@JsonProperty("rider_code")
	private String rider_code;

	public String getRider_code() {
		return rider_code;
	}

	public void setRider_code(String rider_code) {
		this.rider_code = rider_code;
	}

	
	

}
