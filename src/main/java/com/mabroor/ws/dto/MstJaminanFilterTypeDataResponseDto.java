package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MstJaminanFilterTypeDataResponseDto {
	
	@JsonProperty("CD_TYPE")
	private String CD_TYPE;
	
	@JsonProperty("DESC_TYPE")
	private String DESC_TYPE;

	public String getCD_TYPE() {
		return CD_TYPE;
	}

	public void setCD_TYPE(String cD_TYPE) {
		CD_TYPE = cD_TYPE;
	}

	public String getDESC_TYPE() {
		return DESC_TYPE;
	}

	public void setDESC_TYPE(String dESC_TYPE) {
		DESC_TYPE = dESC_TYPE;
	}
	
	

}
