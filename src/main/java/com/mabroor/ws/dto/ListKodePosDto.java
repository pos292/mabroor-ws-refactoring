package com.mabroor.ws.dto;

public class ListKodePosDto {
    private Long id;
    private Long idKelurahan;
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(Long idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
