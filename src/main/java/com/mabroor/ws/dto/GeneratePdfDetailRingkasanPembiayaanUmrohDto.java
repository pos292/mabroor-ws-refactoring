package com.mabroor.ws.dto;

public class GeneratePdfDetailRingkasanPembiayaanUmrohDto {

    private String nama;
    private String partnerPembiayaan;
    private String produkPembiayaan;

    private String dp;

    private String tenor;

    private String namaDepan;

    private String namaBelakang;
    private String jenisKelamin;
    private String tempatLahir;

    private String tanggalLahir;
    private String namaIbuKandung;
    private String nomorPonsel;
    private String noKtp;
    private String alamatTempatTinggal;

    private String provinsi;
    private String kotaKabupaten;

    private String kecamatan;

    private String kelurahan;

    private String rt;
    private String rw;

    private String kodePos;

    private String alamatKtp;

    private String kodePosKtp;

    public String getAlamatKtp() {
        return alamatKtp;
    }

    public void setAlamatKtp(String alamatKtp) {
        this.alamatKtp = alamatKtp;
    }


    public String getKodePosKtp() {
        return kodePosKtp;
    }

    public void setKodePosKtp(String kodePosKtp) {
        this.kodePosKtp = kodePosKtp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPartnerPembiayaan() {
        return partnerPembiayaan;
    }

    public void setPartnerPembiayaan(String partnerPembiayaan) {
        this.partnerPembiayaan = partnerPembiayaan;
    }

    public String getProdukPembiayaan() {
        return produkPembiayaan;
    }

    public void setProdukPembiayaan(String produkPembiayaan) {
        this.produkPembiayaan = produkPembiayaan;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getNamaDepan() {
        return namaDepan;
    }

    public void setNamaDepan(String namaDepan) {
        this.namaDepan = namaDepan;
    }

    public String getNamaBelakang() {
        return namaBelakang;
    }

    public void setNamaBelakang(String namaBelakang) {
        this.namaBelakang = namaBelakang;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getNamaIbuKandung() {
        return namaIbuKandung;
    }

    public void setNamaIbuKandung(String namaIbuKandung) {
        this.namaIbuKandung = namaIbuKandung;
    }

    public String getNomorPonsel() {
        return nomorPonsel;
    }

    public void setNomorPonsel(String nomorPonsel) {
        this.nomorPonsel = nomorPonsel;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }

    public String getAlamatTempatTinggal() {
        return alamatTempatTinggal;
    }

    public void setAlamatTempatTinggal(String alamatTempatTinggal) {
        this.alamatTempatTinggal = alamatTempatTinggal;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKotaKabupaten() {
        return kotaKabupaten;
    }

    public void setKotaKabupaten(String kotaKabupaten) {
        this.kotaKabupaten = kotaKabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }
}
