package com.mabroor.ws.dto;

import java.util.Date;

public class MstUserMobileBodyProfileDto {
	
	private Long id;
	private String name;
	private String userType;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String maskedPhone;
	private String gender;
	private String birthDate;
	private String token;
	private String userMobileUuid;
	private String goals;
	private String userMobileStatus;
	private String fingerprintGoogle;
	private String zipcode;
	private String zipcodeKtp;
	private Long previousUserId;
	private Long countPin;
	private String encryptionStatus;
	private String pinLockStatus;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String firebaseToken;
	private String ktpImage;
	private String npwpImage;
	private String bpkbMobilImage;
	private String bpkbMotorImage;
	private String kkImage;
	private String ktpNumber;
	private String npwpNumber;
	private String statusUser;
	private String address;
	private String addressKtp;
	private String ppImage;
	private String phoneNumOld;
	private Date pinLockDateTime;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMaskedPhone() {
		return maskedPhone;
	}
	public void setMaskedPhone(String maskedPhone) {
		this.maskedPhone = maskedPhone;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUserMobileUuid() {
		return userMobileUuid;
	}
	public void setUserMobileUuid(String userMobileUuid) {
		this.userMobileUuid = userMobileUuid;
	}
	public String getGoals() {
		return goals;
	}
	public void setGoals(String goals) {
		this.goals = goals;
	}
	public String getUserMobileStatus() {
		return userMobileStatus;
	}
	public void setUserMobileStatus(String userMobileStatus) {
		this.userMobileStatus = userMobileStatus;
	}
	public String getFingerprintGoogle() {
		return fingerprintGoogle;
	}
	public void setFingerprintGoogle(String fingerprintGoogle) {
		this.fingerprintGoogle = fingerprintGoogle;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getZipcodeKtp() {
		return zipcodeKtp;
	}
	public void setZipcodeKtp(String zipcodeKtp) {
		this.zipcodeKtp = zipcodeKtp;
	}
	public Long getPreviousUserId() {
		return previousUserId;
	}
	public void setPreviousUserId(Long previousUserId) {
		this.previousUserId = previousUserId;
	}
	public Long getCountPin() {
		return countPin;
	}
	public void setCountPin(Long countPin) {
		this.countPin = countPin;
	}
	public String getEncryptionStatus() {
		return encryptionStatus;
	}
	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}
	public String getPinLockStatus() {
		return pinLockStatus;
	}
	public void setPinLockStatus(String pinLockStatus) {
		this.pinLockStatus = pinLockStatus;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getFirebaseToken() {
		return firebaseToken;
	}
	public void setFirebaseToken(String firebaseToken) {
		this.firebaseToken = firebaseToken;
	}
	public String getKtpImage() {
		return ktpImage;
	}
	public void setKtpImage(String ktpImage) {
		this.ktpImage = ktpImage;
	}
	public String getNpwpImage() {
		return npwpImage;
	}
	public void setNpwpImage(String npwpImage) {
		this.npwpImage = npwpImage;
	}
	public String getBpkbMobilImage() {
		return bpkbMobilImage;
	}
	public void setBpkbMobilImage(String bpkbMobilImage) {
		this.bpkbMobilImage = bpkbMobilImage;
	}
	public String getBpkbMotorImage() {
		return bpkbMotorImage;
	}
	public void setBpkbMotorImage(String bpkbMotorImage) {
		this.bpkbMotorImage = bpkbMotorImage;
	}
	public String getKkImage() {
		return kkImage;
	}
	public void setKkImage(String kkImage) {
		this.kkImage = kkImage;
	}
	public String getKtpNumber() {
		return ktpNumber;
	}
	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}
	public String getNpwpNumber() {
		return npwpNumber;
	}
	public void setNpwpNumber(String npwpNumber) {
		this.npwpNumber = npwpNumber;
	}
	public String getStatusUser() {
		return statusUser;
	}
	public void setStatusUser(String statusUser) {
		this.statusUser = statusUser;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressKtp() {
		return addressKtp;
	}
	public void setAddressKtp(String addressKtp) {
		this.addressKtp = addressKtp;
	}
	public String getPpImage() {
		return ppImage;
	}
	public void setPpImage(String ppImage) {
		this.ppImage = ppImage;
	}
	public Date getPinLockDateTime() {
		return pinLockDateTime;
	}
	public void setPinLockDateTime(Date pinLockDateTime) {
		this.pinLockDateTime = pinLockDateTime;
	}
	public String getPhoneNumOld() {
		return phoneNumOld;
	}
	public void setPhoneNumOld(String phoneNumOld) {
		this.phoneNumOld = phoneNumOld;
	}
	
	

}
