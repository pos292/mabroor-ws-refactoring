package com.mabroor.ws.dto;

public class Result2wMguDto {
	
	private String danastraId;
	
	private String leadId;
	
    private String callVia;
	
	private String contactDetail;
	
	private String fullName;
	
	private String noKTP;
	
	private String birhtDate;
	
	private String birhtPlace;
	
	private String gender;
	
	private String motherName;
	
	private String ktpAddress;
	
	private String ktpRt;
	
	private String ktpRw;
	
	private String ktpStateId;
	
	private String ktpCityId;
	
	private String ktpDistrictId;
	
	private String ktpSubDistrictId;
	
	private String ktpZipCode;
	
	private String domcAddress;
	
	private String domcRt;
	
	private String domcRw;
	
	private String domcStateId;
	
	private String domcCityId;
	
	private String domcDistrictId;
	
	private String domcSubDistrictId;
	
	private String domcZipCode;
	
	private String mobilePhone1;
	
	private String mobilePhone2;
	
	private String maritalStatus;
	
	private String houseStatus;
	
	private String education;
	
	private String npwpStatus;
	
	private String npwpNo;
	
	private String email;
	
	private String jobStatus;
	
	private String jobPosition;
	
    private String spouseJobStatus;
	
	private String spouseJobPosition;
	
	private String jobType;
	
	private String spouseJobType;
	
	private String monthlyIncome;
	
	private String monthlyExpense;
	
	private String jenisKebutuhan;
	
	private String vehicleBrand;
	
	private String vehicleModel;
	
	private String vehicleYear;
	
	private String requestPinjaman;
	
	private Long tenor;
	
	private String disbursementMethod;
	
	private String noRangka;
	
	private String noMesin;
	
	private String noPolisi;
	
	private String bpkbNo;
	
	private String bpkbOwnership;
	
	private String bpkbOwnershipName;
	
	private String bpkbOwnershipAddress;
	
	private String bpkbOwnershipRt;
	
	private String bpkbOwnershipRw;
	
	private String bpkbOwnershipStateId;
	
	private String bpkbOwnershipCityId;
	
	private String bpkbOwnershipDistrictId;
	
	private String bpkbOwnershipSubDistrictId;
	
	private String bpkbOwnershipZipCode;
	
	private String customerType;
	
	private String contactNo;
	
	private String fgcNo;
	
	private String referralCode;
	
	private String customerRemark;
	
	private String remark;
	
	private String userKey;
	
	private String leadDataSelfieId;
	
	private String leadDataKTPId;
	
	private String similiaritySelfieAndKTP;
	
	private String ktpStateName;
	
	private String ktpCityName;
	
	private String ktpDistrictName;
	
	private String ktpSubDistrictName;
	
	private String domcStateName;
	
	private String domcCityName;
	
	private String domcDistrictName;
	
	private String domcSubDistrictName;
	
	private String bpkbOwnershipStateName;
	
	private String bpkbOwnershipCityName;
	
	private String bpkbOwnershipDistrictName;
	
	private String bpkbOwnershipSubDistrictName;
	
	private String latitude;
	
	private String longtitude;
	
	private String customerNo;
	
	private String domcSubZipCode;
	
	private String bpkbOwnershipSubZipCode;
	
	private String ktpSubZipCode;
	
	private String verificationDate;
	
	private String approvePinjaman;
	
	private String interest;
	
	private String approved;
	
	private String sourceBy;
	
	private String roStatus;
	
	private String fgcStatus;
	
	private String isBlacklist;
	
	private String submitDate;
	
	private String status;
	
	private String approvedDate;
	
	private String approvedBy;
	
	private String modifiedDate;
	
	private String modifiedBy;
	
	private String interestDate;
	
	private String interestBy;
	
	private String autoReject;
	
	private String bpkbownershipdsc;
	
	private String tenordsc;
	
	private String jenisKebutuhandsc;
	
	private String customerTypedsc;
	
	private String otp;
	
	private String token;
	
	private String sendStatus;
	
	private String errorMessage;
	
	private String leadDataSelfie;
	
	private String leadDataKtp;
	
	private String communityName;
	
	private String cityName;
	
	private String stateName;
	
	private String subDistrictName;
	
	private String districtName;
	
	private String marketingPromotionId;
	
	private String callViaDsc;

	public String getDanastraId() {
		return danastraId;
	}

	public void setDanastraId(String danastraId) {
		this.danastraId = danastraId;
	}

	public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	public String getCallVia() {
		return callVia;
	}

	public void setCallVia(String callVia) {
		this.callVia = callVia;
	}

	public String getContactDetail() {
		return contactDetail;
	}

	public void setContactDetail(String contactDetail) {
		this.contactDetail = contactDetail;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNoKTP() {
		return noKTP;
	}

	public void setNoKTP(String noKTP) {
		this.noKTP = noKTP;
	}

	public String getBirhtDate() {
		return birhtDate;
	}

	public void setBirhtDate(String birhtDate) {
		this.birhtDate = birhtDate;
	}

	public String getBirhtPlace() {
		return birhtPlace;
	}

	public void setBirhtPlace(String birhtPlace) {
		this.birhtPlace = birhtPlace;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getKtpAddress() {
		return ktpAddress;
	}

	public void setKtpAddress(String ktpAddress) {
		this.ktpAddress = ktpAddress;
	}

	public String getKtpRt() {
		return ktpRt;
	}

	public void setKtpRt(String ktpRt) {
		this.ktpRt = ktpRt;
	}

	public String getKtpRw() {
		return ktpRw;
	}

	public void setKtpRw(String ktpRw) {
		this.ktpRw = ktpRw;
	}

	public String getKtpStateId() {
		return ktpStateId;
	}

	public void setKtpStateId(String ktpStateId) {
		this.ktpStateId = ktpStateId;
	}

	public String getKtpCityId() {
		return ktpCityId;
	}

	public void setKtpCityId(String ktpCityId) {
		this.ktpCityId = ktpCityId;
	}

	public String getKtpDistrictId() {
		return ktpDistrictId;
	}

	public void setKtpDistrictId(String ktpDistrictId) {
		this.ktpDistrictId = ktpDistrictId;
	}

	public String getKtpSubDistrictId() {
		return ktpSubDistrictId;
	}

	public void setKtpSubDistrictId(String ktpSubDistrictId) {
		this.ktpSubDistrictId = ktpSubDistrictId;
	}

	public String getKtpZipCode() {
		return ktpZipCode;
	}

	public void setKtpZipCode(String ktpZipCode) {
		this.ktpZipCode = ktpZipCode;
	}

	public String getDomcAddress() {
		return domcAddress;
	}

	public void setDomcAddress(String domcAddress) {
		this.domcAddress = domcAddress;
	}

	public String getDomcRt() {
		return domcRt;
	}

	public void setDomcRt(String domcRt) {
		this.domcRt = domcRt;
	}

	public String getDomcRw() {
		return domcRw;
	}

	public void setDomcRw(String domcRw) {
		this.domcRw = domcRw;
	}

	public String getDomcStateId() {
		return domcStateId;
	}

	public void setDomcStateId(String domcStateId) {
		this.domcStateId = domcStateId;
	}

	public String getDomcCityId() {
		return domcCityId;
	}

	public void setDomcCityId(String domcCityId) {
		this.domcCityId = domcCityId;
	}

	public String getDomcDistrictId() {
		return domcDistrictId;
	}

	public void setDomcDistrictId(String domcDistrictId) {
		this.domcDistrictId = domcDistrictId;
	}

	public String getDomcSubDistrictId() {
		return domcSubDistrictId;
	}

	public void setDomcSubDistrictId(String domcSubDistrictId) {
		this.domcSubDistrictId = domcSubDistrictId;
	}

	public String getDomcZipCode() {
		return domcZipCode;
	}

	public void setDomcZipCode(String domcZipCode) {
		this.domcZipCode = domcZipCode;
	}

	public String getMobilePhone1() {
		return mobilePhone1;
	}

	public void setMobilePhone1(String mobilePhone1) {
		this.mobilePhone1 = mobilePhone1;
	}

	public String getMobilePhone2() {
		return mobilePhone2;
	}

	public void setMobilePhone2(String mobilePhone2) {
		this.mobilePhone2 = mobilePhone2;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getHouseStatus() {
		return houseStatus;
	}

	public void setHouseStatus(String houseStatus) {
		this.houseStatus = houseStatus;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getNpwpStatus() {
		return npwpStatus;
	}

	public void setNpwpStatus(String npwpStatus) {
		this.npwpStatus = npwpStatus;
	}

	public String getNpwpNo() {
		return npwpNo;
	}

	public void setNpwpNo(String npwpNo) {
		this.npwpNo = npwpNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public String getSpouseJobStatus() {
		return spouseJobStatus;
	}

	public void setSpouseJobStatus(String spouseJobStatus) {
		this.spouseJobStatus = spouseJobStatus;
	}

	public String getSpouseJobPosition() {
		return spouseJobPosition;
	}

	public void setSpouseJobPosition(String spouseJobPosition) {
		this.spouseJobPosition = spouseJobPosition;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getSpouseJobType() {
		return spouseJobType;
	}

	public void setSpouseJobType(String spouseJobType) {
		this.spouseJobType = spouseJobType;
	}

	public String getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public String getMonthlyExpense() {
		return monthlyExpense;
	}

	public void setMonthlyExpense(String monthlyExpense) {
		this.monthlyExpense = monthlyExpense;
	}

	public String getJenisKebutuhan() {
		return jenisKebutuhan;
	}

	public void setJenisKebutuhan(String jenisKebutuhan) {
		this.jenisKebutuhan = jenisKebutuhan;
	}

	public String getVehicleBrand() {
		return vehicleBrand;
	}

	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleYear() {
		return vehicleYear;
	}

	public void setVehicleYear(String vehicleYear) {
		this.vehicleYear = vehicleYear;
	}

	public String getRequestPinjaman() {
		return requestPinjaman;
	}

	public void setRequestPinjaman(String requestPinjaman) {
		this.requestPinjaman = requestPinjaman;
	}

	public Long getTenor() {
		return tenor;
	}

	public void setTenor(Long tenor) {
		this.tenor = tenor;
	}

	public String getDisbursementMethod() {
		return disbursementMethod;
	}

	public void setDisbursementMethod(String disbursementMethod) {
		this.disbursementMethod = disbursementMethod;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNoPolisi() {
		return noPolisi;
	}

	public void setNoPolisi(String noPolisi) {
		this.noPolisi = noPolisi;
	}

	public String getBpkbNo() {
		return bpkbNo;
	}

	public void setBpkbNo(String bpkbNo) {
		this.bpkbNo = bpkbNo;
	}

	public String getBpkbOwnership() {
		return bpkbOwnership;
	}

	public void setBpkbOwnership(String bpkbOwnership) {
		this.bpkbOwnership = bpkbOwnership;
	}

	public String getBpkbOwnershipName() {
		return bpkbOwnershipName;
	}

	public void setBpkbOwnershipName(String bpkbOwnershipName) {
		this.bpkbOwnershipName = bpkbOwnershipName;
	}

	public String getBpkbOwnershipAddress() {
		return bpkbOwnershipAddress;
	}

	public void setBpkbOwnershipAddress(String bpkbOwnershipAddress) {
		this.bpkbOwnershipAddress = bpkbOwnershipAddress;
	}

	public String getBpkbOwnershipRt() {
		return bpkbOwnershipRt;
	}

	public void setBpkbOwnershipRt(String bpkbOwnershipRt) {
		this.bpkbOwnershipRt = bpkbOwnershipRt;
	}

	public String getBpkbOwnershipRw() {
		return bpkbOwnershipRw;
	}

	public void setBpkbOwnershipRw(String bpkbOwnershipRw) {
		this.bpkbOwnershipRw = bpkbOwnershipRw;
	}

	public String getBpkbOwnershipStateId() {
		return bpkbOwnershipStateId;
	}

	public void setBpkbOwnershipStateId(String bpkbOwnershipStateId) {
		this.bpkbOwnershipStateId = bpkbOwnershipStateId;
	}

	public String getBpkbOwnershipCityId() {
		return bpkbOwnershipCityId;
	}

	public void setBpkbOwnershipCityId(String bpkbOwnershipCityId) {
		this.bpkbOwnershipCityId = bpkbOwnershipCityId;
	}

	public String getBpkbOwnershipDistrictId() {
		return bpkbOwnershipDistrictId;
	}

	public void setBpkbOwnershipDistrictId(String bpkbOwnershipDistrictId) {
		this.bpkbOwnershipDistrictId = bpkbOwnershipDistrictId;
	}

	public String getBpkbOwnershipSubDistrictId() {
		return bpkbOwnershipSubDistrictId;
	}

	public void setBpkbOwnershipSubDistrictId(String bpkbOwnershipSubDistrictId) {
		this.bpkbOwnershipSubDistrictId = bpkbOwnershipSubDistrictId;
	}

	public String getBpkbOwnershipZipCode() {
		return bpkbOwnershipZipCode;
	}

	public void setBpkbOwnershipZipCode(String bpkbOwnershipZipCode) {
		this.bpkbOwnershipZipCode = bpkbOwnershipZipCode;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getFgcNo() {
		return fgcNo;
	}

	public void setFgcNo(String fgcNo) {
		this.fgcNo = fgcNo;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getCustomerRemark() {
		return customerRemark;
	}

	public void setCustomerRemark(String customerRemark) {
		this.customerRemark = customerRemark;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getLeadDataSelfieId() {
		return leadDataSelfieId;
	}

	public void setLeadDataSelfieId(String leadDataSelfieId) {
		this.leadDataSelfieId = leadDataSelfieId;
	}

	public String getLeadDataKTPId() {
		return leadDataKTPId;
	}

	public void setLeadDataKTPId(String leadDataKTPId) {
		this.leadDataKTPId = leadDataKTPId;
	}

	public String getSimiliaritySelfieAndKTP() {
		return similiaritySelfieAndKTP;
	}

	public void setSimiliaritySelfieAndKTP(String similiaritySelfieAndKTP) {
		this.similiaritySelfieAndKTP = similiaritySelfieAndKTP;
	}

	public String getKtpStateName() {
		return ktpStateName;
	}

	public void setKtpStateName(String ktpStateName) {
		this.ktpStateName = ktpStateName;
	}

	public String getKtpCityName() {
		return ktpCityName;
	}

	public void setKtpCityName(String ktpCityName) {
		this.ktpCityName = ktpCityName;
	}

	public String getKtpDistrictName() {
		return ktpDistrictName;
	}

	public void setKtpDistrictName(String ktpDistrictName) {
		this.ktpDistrictName = ktpDistrictName;
	}

	public String getKtpSubDistrictName() {
		return ktpSubDistrictName;
	}

	public void setKtpSubDistrictName(String ktpSubDistrictName) {
		this.ktpSubDistrictName = ktpSubDistrictName;
	}

	public String getDomcStateName() {
		return domcStateName;
	}

	public void setDomcStateName(String domcStateName) {
		this.domcStateName = domcStateName;
	}

	public String getDomcCityName() {
		return domcCityName;
	}

	public void setDomcCityName(String domcCityName) {
		this.domcCityName = domcCityName;
	}

	public String getDomcDistrictName() {
		return domcDistrictName;
	}

	public void setDomcDistrictName(String domcDistrictName) {
		this.domcDistrictName = domcDistrictName;
	}

	public String getDomcSubDistrictName() {
		return domcSubDistrictName;
	}

	public void setDomcSubDistrictName(String domcSubDistrictName) {
		this.domcSubDistrictName = domcSubDistrictName;
	}

	public String getBpkbOwnershipStateName() {
		return bpkbOwnershipStateName;
	}

	public void setBpkbOwnershipStateName(String bpkbOwnershipStateName) {
		this.bpkbOwnershipStateName = bpkbOwnershipStateName;
	}

	public String getBpkbOwnershipCityName() {
		return bpkbOwnershipCityName;
	}

	public void setBpkbOwnershipCityName(String bpkbOwnershipCityName) {
		this.bpkbOwnershipCityName = bpkbOwnershipCityName;
	}

	public String getBpkbOwnershipDistrictName() {
		return bpkbOwnershipDistrictName;
	}

	public void setBpkbOwnershipDistrictName(String bpkbOwnershipDistrictName) {
		this.bpkbOwnershipDistrictName = bpkbOwnershipDistrictName;
	}

	public String getBpkbOwnershipSubDistrictName() {
		return bpkbOwnershipSubDistrictName;
	}

	public void setBpkbOwnershipSubDistrictName(String bpkbOwnershipSubDistrictName) {
		this.bpkbOwnershipSubDistrictName = bpkbOwnershipSubDistrictName;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(String longtitude) {
		this.longtitude = longtitude;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getDomcSubZipCode() {
		return domcSubZipCode;
	}

	public void setDomcSubZipCode(String domcSubZipCode) {
		this.domcSubZipCode = domcSubZipCode;
	}

	public String getBpkbOwnershipSubZipCode() {
		return bpkbOwnershipSubZipCode;
	}

	public void setBpkbOwnershipSubZipCode(String bpkbOwnershipSubZipCode) {
		this.bpkbOwnershipSubZipCode = bpkbOwnershipSubZipCode;
	}

	public String getKtpSubZipCode() {
		return ktpSubZipCode;
	}

	public void setKtpSubZipCode(String ktpSubZipCode) {
		this.ktpSubZipCode = ktpSubZipCode;
	}

	public String getVerificationDate() {
		return verificationDate;
	}

	public void setVerificationDate(String verificationDate) {
		this.verificationDate = verificationDate;
	}

	public String getApprovePinjaman() {
		return approvePinjaman;
	}

	public void setApprovePinjaman(String approvePinjaman) {
		this.approvePinjaman = approvePinjaman;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getApproved() {
		return approved;
	}

	public void setApproved(String approved) {
		this.approved = approved;
	}

	public String getSourceBy() {
		return sourceBy;
	}

	public void setSourceBy(String sourceBy) {
		this.sourceBy = sourceBy;
	}

	public String getRoStatus() {
		return roStatus;
	}

	public void setRoStatus(String roStatus) {
		this.roStatus = roStatus;
	}

	public String getFgcStatus() {
		return fgcStatus;
	}

	public void setFgcStatus(String fgcStatus) {
		this.fgcStatus = fgcStatus;
	}

	public String getIsBlacklist() {
		return isBlacklist;
	}

	public void setIsBlacklist(String isBlacklist) {
		this.isBlacklist = isBlacklist;
	}

	public String getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getInterestDate() {
		return interestDate;
	}

	public void setInterestDate(String interestDate) {
		this.interestDate = interestDate;
	}

	public String getInterestBy() {
		return interestBy;
	}

	public void setInterestBy(String interestBy) {
		this.interestBy = interestBy;
	}

	public String getAutoReject() {
		return autoReject;
	}

	public void setAutoReject(String autoReject) {
		this.autoReject = autoReject;
	}

	public String getBpkbownershipdsc() {
		return bpkbownershipdsc;
	}

	public void setBpkbownershipdsc(String bpkbownershipdsc) {
		this.bpkbownershipdsc = bpkbownershipdsc;
	}

	public String getTenordsc() {
		return tenordsc;
	}

	public void setTenordsc(String tenordsc) {
		this.tenordsc = tenordsc;
	}

	public String getJenisKebutuhandsc() {
		return jenisKebutuhandsc;
	}

	public void setJenisKebutuhandsc(String jenisKebutuhandsc) {
		this.jenisKebutuhandsc = jenisKebutuhandsc;
	}

	public String getCustomerTypedsc() {
		return customerTypedsc;
	}

	public void setCustomerTypedsc(String customerTypedsc) {
		this.customerTypedsc = customerTypedsc;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getLeadDataSelfie() {
		return leadDataSelfie;
	}

	public void setLeadDataSelfie(String leadDataSelfie) {
		this.leadDataSelfie = leadDataSelfie;
	}

	public String getLeadDataKtp() {
		return leadDataKtp;
	}

	public void setLeadDataKtp(String leadDataKtp) {
		this.leadDataKtp = leadDataKtp;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getSubDistrictName() {
		return subDistrictName;
	}

	public void setSubDistrictName(String subDistrictName) {
		this.subDistrictName = subDistrictName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getMarketingPromotionId() {
		return marketingPromotionId;
	}

	public void setMarketingPromotionId(String marketingPromotionId) {
		this.marketingPromotionId = marketingPromotionId;
	}

	public String getCallViaDsc() {
		return callViaDsc;
	}

	public void setCallViaDsc(String callViaDsc) {
		this.callViaDsc = callViaDsc;
	}
	
	

}
