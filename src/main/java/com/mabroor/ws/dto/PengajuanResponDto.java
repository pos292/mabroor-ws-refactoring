package com.mabroor.ws.dto;

import java.util.List;

import com.mabroor.ws.entity.MstPengajuanCar;
import com.mabroor.ws.entity.MstPengajuanMotor;
import com.mabroor.ws.entity.MstPengajuanMultigunaTanpaJaminan;
import com.mabroor.ws.entity.MstWisataReligi;

public class PengajuanResponDto {

	List <MstPengajuanMotor> pengajuanMotorList;
	List <MstPengajuanCar> pengajuanCarList;
	List <MstWisataReligi> pengajuanWisataReligiList;

	List <RespPengajuanDto> pengajuanDtos;
	
	public List<MstPengajuanMotor> getPengajuanMotorList() {
		return pengajuanMotorList;
	}
	public void setPengajuanMotorList(List<MstPengajuanMotor> pengajuanMotorList) {
		this.pengajuanMotorList = pengajuanMotorList;
	}
	public List<MstPengajuanCar> getPengajuanCarList() {
		return pengajuanCarList;
	}
	public void setPengajuanCarList(List<MstPengajuanCar> pengajuanCarList) {
		this.pengajuanCarList = pengajuanCarList;
	}

	public List<MstWisataReligi> getPengajuanWisataReligiList() {
		return pengajuanWisataReligiList;
	}
	public void setPengajuanWisataReligiList(List<MstWisataReligi> pengajuanWisataReligiList) {
		this.pengajuanWisataReligiList = pengajuanWisataReligiList;
	}

	public List<RespPengajuanDto> getPengajuanDtos() {
		return pengajuanDtos;
	}

	public void setPengajuanDtos(List<RespPengajuanDto> pengajuanDtos) {
		this.pengajuanDtos = pengajuanDtos;
	}
}
