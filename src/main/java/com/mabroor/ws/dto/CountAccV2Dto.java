package com.mabroor.ws.dto;

public class CountAccV2Dto {
	private Long countAcc;
	private Long lastOffset;
	public Long getCountAcc() {
		return countAcc;
	}
	public void setCountAcc(Long countAcc) {
		this.countAcc = countAcc;
	}
	public Long getLastOffset() {
		return lastOffset;
	}
	public void setLastOffset(Long lastOffset) {
		this.lastOffset = lastOffset;
	}
	public CountAccV2Dto(Long countAcc, Long lastOffset) {
		super();
		this.countAcc = countAcc;
		this.lastOffset = lastOffset;
	}
	
}
