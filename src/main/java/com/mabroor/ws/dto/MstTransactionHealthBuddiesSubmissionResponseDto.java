package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MstTransactionHealthBuddiesSubmissionResponseDto {
	
	@JsonProperty("esubStatus")
	private String esubStatus;
	
	@JsonProperty("message")
	private String message;
	
	public String getEsubStatus() {
		return esubStatus;
	}
	public void setEsubStatus(String esubStatus) {
		this.esubStatus = esubStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
