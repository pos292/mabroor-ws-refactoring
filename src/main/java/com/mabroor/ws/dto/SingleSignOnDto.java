package com.mabroor.ws.dto;

import java.util.List;
import java.util.Map;

public class SingleSignOnDto {

	List<Map<String, Object>> carUnit;
	List<Map<String, Object>> motorUnit;
	public List<Map<String, Object>> getCarUnit() {
		return carUnit;
	}
	public void setCarUnit(List<Map<String, Object>> carUnit) {
		this.carUnit = carUnit;
	}
	public List<Map<String, Object>> getMotorUnit() {
		return motorUnit;
	}
	public void setMotorUnit(List<Map<String, Object>> motorUnit) {
		this.motorUnit = motorUnit;
	}
	
	
}
