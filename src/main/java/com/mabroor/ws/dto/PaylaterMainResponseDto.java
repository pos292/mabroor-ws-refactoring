package com.mabroor.ws.dto;

public class PaylaterMainResponseDto {
	
	private Long code;
	private String message;
	private PaylaterResponseResultDto result;
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaylaterResponseResultDto getResult() {
		return result;
	}
	public void setResult(PaylaterResponseResultDto result) {
		this.result = result;
	}
	
	

}
