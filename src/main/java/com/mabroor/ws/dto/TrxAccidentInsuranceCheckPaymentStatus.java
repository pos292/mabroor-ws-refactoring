package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxAccidentInsuranceCheckPaymentStatus {
	
	private String merchantToken;
	private String reqTm;
	private String goodsNm;
	private String resultCd;
	private String referenceNo;
	private String tXid;
	private String amt;
	private String vacctNo;
	private String instmntType;
	private String iMid;
	private String billingNm;
	private String resultMsg;
	private String vacctValidDt;
	private String payMethod;
	private String bankCd;
	private String reqDt;
	private String currency;
	private String instmntMon;
	private String vacctValidTm;
	private String status;
	private String matchCl;
	private String matchCI;
	private String receiptCode;
	private String authNo;
	private String preauthToken;
	private String recurringToken;
	private String acquBankCd;
	private String mRefNo;
	
	public String getMerchantToken() {
		return merchantToken;
	}
	public void setMerchantToken(String merchantToken) {
		this.merchantToken = merchantToken;
	}
	public String getReqTm() {
		return reqTm;
	}
	public void setReqTm(String reqTm) {
		this.reqTm = reqTm;
	}
	public String getGoodsNm() {
		return goodsNm;
	}
	public void setGoodsNm(String goodsNm) {
		this.goodsNm = goodsNm;
	}
	public String getResultCd() {
		return resultCd;
	}
	public void setResultCd(String resultCd) {
		this.resultCd = resultCd;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String gettXid() {
		return tXid;
	}
	public void settXid(String tXid) {
		this.tXid = tXid;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getVacctNo() {
		return vacctNo;
	}
	public void setVacctNo(String vacctNo) {
		this.vacctNo = vacctNo;
	}
	public String getInstmntType() {
		return instmntType;
	}
	public void setInstmntType(String instmntType) {
		this.instmntType = instmntType;
	}
	public String getiMid() {
		return iMid;
	}
	public void setiMid(String iMid) {
		this.iMid = iMid;
	}
	public String getBillingNm() {
		return billingNm;
	}
	public void setBillingNm(String billingNm) {
		this.billingNm = billingNm;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public String getVacctValidDt() {
		return vacctValidDt;
	}
	public void setVacctValidDt(String vacctValidDt) {
		this.vacctValidDt = vacctValidDt;
	}
	public String getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	public String getBankCd() {
		return bankCd;
	}
	public void setBankCd(String bankCd) {
		this.bankCd = bankCd;
	}
	public String getReqDt() {
		return reqDt;
	}
	public void setReqDt(String reqDt) {
		this.reqDt = reqDt;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getInstmntMon() {
		return instmntMon;
	}
	public void setInstmntMon(String instmntMon) {
		this.instmntMon = instmntMon;
	}
	public String getVacctValidTm() {
		return vacctValidTm;
	}
	public void setVacctValidTm(String vacctValidTm) {
		this.vacctValidTm = vacctValidTm;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMatchCl() {
		return matchCl;
	}
	public void setMatchCl(String matchCl) {
		this.matchCl = matchCl;
	}
	public String getMatchCI() {
		return matchCI;
	}
	public void setMatchCI(String matchCI) {
		this.matchCI = matchCI;
	}
	public String getReceiptCode() {
		return receiptCode;
	}
	public void setReceiptCode(String receiptCode) {
		this.receiptCode = receiptCode;
	}
	public String getAuthNo() {
		return authNo;
	}
	public void setAuthNo(String authNo) {
		this.authNo = authNo;
	}
	public String getPreauthToken() {
		return preauthToken;
	}
	public void setPreauthToken(String preauthToken) {
		this.preauthToken = preauthToken;
	}
	public String getRecurringToken() {
		return recurringToken;
	}
	public void setRecurringToken(String recurringToken) {
		this.recurringToken = recurringToken;
	}
	public String getAcquBankCd() {
		return acquBankCd;
	}
	public void setAcquBankCd(String acquBankCd) {
		this.acquBankCd = acquBankCd;
	}
	public String getmRefNo() {
		return mRefNo;
	}
	public void setmRefNo(String mRefNo) {
		this.mRefNo = mRefNo;
	}
	
}
