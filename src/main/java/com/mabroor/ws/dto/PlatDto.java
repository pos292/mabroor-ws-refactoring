package com.mabroor.ws.dto;

public class PlatDto {
	
	private Long id;
	private String kodePlatWilayah;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKodePlatWilayah() {
		return kodePlatWilayah;
	}
	public void setKodePlatWilayah(String kodePlatWilayah) {
		this.kodePlatWilayah = kodePlatWilayah;
	}

}
