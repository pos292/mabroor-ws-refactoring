package com.mabroor.ws.dto;

public class MstPengajuanRentCarDto {
	private Long id;
	private Long userId;
    private String name;
    private String email;
    private String phone;
    private String dateOfBirth;
    private String gender;
    private String identityNumber;
    private String pickUpLocation;
    private String rentDuration;
    private String note;
    private String cost;
    private String noPassport;
    private String fotoPassport;
    private String fotoKtp;
    private String transactionDateTime;
    private String withDriver;
    private String cityCarRental;
    private String pickUpTime;
    private String rentPackage;
    private String carModel;
    private String additionalPackage;
    private String kodePromo;
    private String namaPromo;
    private String rentCarStatus;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getWithDriver() {
		return withDriver;
	}
	public void setWithDriver(String withDriver) {
		this.withDriver = withDriver;
	}
	public String getCityCarRental() {
		return cityCarRental;
	}
	public void setCityCarRental(String cityCarRental) {
		this.cityCarRental = cityCarRental;
	}
	public String getPickUpTime() {
		return pickUpTime;
	}
	public void setPickUpTime(String pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	public String getRentPackage() {
		return rentPackage;
	}
	public void setRentPackage(String rentPackage) {
		this.rentPackage = rentPackage;
	}
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	public String getAdditionalPackage() {
		return additionalPackage;
	}
	public void setAdditionalPackage(String additionalPackage) {
		this.additionalPackage = additionalPackage;
	}
	public String getKodePromo() {
		return kodePromo;
	}
	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}
	public String getNamaPromo() {
		return namaPromo;
	}
	public void setNamaPromo(String namaPromo) {
		this.namaPromo = namaPromo;
	}
	public String getRentCarStatus() {
		return rentCarStatus;
	}
	public void setRentCarStatus(String rentCarStatus) {
		this.rentCarStatus = rentCarStatus;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIdentityNumber() {
		return identityNumber;
	}
	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}
	public String getPickUpLocation() {
		return pickUpLocation;
	}
	public void setPickUpLocation(String pickUpLocation) {
		this.pickUpLocation = pickUpLocation;
	}
	public String getRentDuration() {
		return rentDuration;
	}
	public void setRentDuration(String rentDuration) {
		this.rentDuration = rentDuration;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getNoPassport() {
		return noPassport;
	}
	public void setNoPassport(String noPassport) {
		this.noPassport = noPassport;
	}
	public String getFotoPassport() {
		return fotoPassport;
	}
	public void setFotoPassport(String fotoPassport) {
		this.fotoPassport = fotoPassport;
	}
	public String getTransactionDateTime() {
		return transactionDateTime;
	}
	public void setTransactionDateTime(String transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}
	public String getFotoKtp() {
		return fotoKtp;
	}
	public void setFotoKtp(String fotoKtp) {
		this.fotoKtp = fotoKtp;
	}
    
}
