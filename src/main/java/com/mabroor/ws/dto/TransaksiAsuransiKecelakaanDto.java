package com.mabroor.ws.dto;

public class TransaksiAsuransiKecelakaanDto {
	
	private String ktpNumber;

	public String getKtpNumber() {
		return ktpNumber;
	}

	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}

}
