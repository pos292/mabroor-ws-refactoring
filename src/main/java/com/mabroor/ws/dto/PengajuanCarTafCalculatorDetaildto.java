package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PengajuanCarTafCalculatorDetaildto {

	@JsonProperty("MAIN_PREMI_AMT")
	private Long MAIN_PREMI_AMT;
	
	@JsonProperty("GROSS_PREMIUM")
	private Long GROSS_PREMIUM;
	
	@JsonProperty("TOTAL_DOWN_PAYMENT_GROSS_AMT")
	private Long TOTAL_DOWN_PAYMENT_GROSS_AMT;
	
	@JsonProperty("INST_AMT")
	private Long INST_AMT;
	
	@JsonProperty("NTF_AMT")
	private Long NTF_AMT;
	
	@JsonProperty("OS_PRINCIPAL_AMT")
	private Long OS_PRINCIPAL_AMT;
	
	@JsonProperty("OS_INTEREST_AMT")
	private Long OS_INTEREST_AMT;
	
	@JsonProperty("RATE_MIN_PRCNT")
	private Long RATE_MIN_PRCNT;
	
	@JsonProperty("RATE_PUBLISH_PRCNT")
	private Long RATE_PUBLISH_PRCNT;
	
	@JsonProperty("PROVISION_PRCNT")
	private Long PROVISION_PRCNT;
	
	@JsonProperty("TENOR")
	private Long TENOR;
	
	@JsonProperty("FIRST_INSTALLMENT_TYPE")
	private String FIRST_INSTALLMENT_TYPE;
	
	@JsonProperty("POLICY_FEE")
	private Long POLICY_FEE;

	public Long getMAIN_PREMI_AMT() {
		return MAIN_PREMI_AMT;
	}

	public void setMAIN_PREMI_AMT(Long mAIN_PREMI_AMT) {
		MAIN_PREMI_AMT = mAIN_PREMI_AMT;
	}

	public Long getGROSS_PREMIUM() {
		return GROSS_PREMIUM;
	}

	public void setGROSS_PREMIUM(Long gROSS_PREMIUM) {
		GROSS_PREMIUM = gROSS_PREMIUM;
	}

	public Long getTOTAL_DOWN_PAYMENT_GROSS_AMT() {
		return TOTAL_DOWN_PAYMENT_GROSS_AMT;
	}

	public void setTOTAL_DOWN_PAYMENT_GROSS_AMT(Long tOTAL_DOWN_PAYMENT_GROSS_AMT) {
		TOTAL_DOWN_PAYMENT_GROSS_AMT = tOTAL_DOWN_PAYMENT_GROSS_AMT;
	}

	public Long getINST_AMT() {
		return INST_AMT;
	}

	public void setINST_AMT(Long iNST_AMT) {
		INST_AMT = iNST_AMT;
	}

	public Long getNTF_AMT() {
		return NTF_AMT;
	}

	public void setNTF_AMT(Long nTF_AMT) {
		NTF_AMT = nTF_AMT;
	}

	public Long getOS_PRINCIPAL_AMT() {
		return OS_PRINCIPAL_AMT;
	}

	public void setOS_PRINCIPAL_AMT(Long oS_PRINCIPAL_AMT) {
		OS_PRINCIPAL_AMT = oS_PRINCIPAL_AMT;
	}

	public Long getOS_INTEREST_AMT() {
		return OS_INTEREST_AMT;
	}

	public void setOS_INTEREST_AMT(Long oS_INTEREST_AMT) {
		OS_INTEREST_AMT = oS_INTEREST_AMT;
	}

	public Long getRATE_MIN_PRCNT() {
		return RATE_MIN_PRCNT;
	}

	public void setRATE_MIN_PRCNT(Long rATE_MIN_PRCNT) {
		RATE_MIN_PRCNT = rATE_MIN_PRCNT;
	}

	public Long getRATE_PUBLISH_PRCNT() {
		return RATE_PUBLISH_PRCNT;
	}

	public void setRATE_PUBLISH_PRCNT(Long rATE_PUBLISH_PRCNT) {
		RATE_PUBLISH_PRCNT = rATE_PUBLISH_PRCNT;
	}

	public Long getPROVISION_PRCNT() {
		return PROVISION_PRCNT;
	}

	public void setPROVISION_PRCNT(Long pROVISION_PRCNT) {
		PROVISION_PRCNT = pROVISION_PRCNT;
	}

	public Long getTENOR() {
		return TENOR;
	}

	public void setTENOR(Long tENOR) {
		TENOR = tENOR;
	}

	public String getFIRST_INSTALLMENT_TYPE() {
		return FIRST_INSTALLMENT_TYPE;
	}

	public void setFIRST_INSTALLMENT_TYPE(String fIRST_INSTALLMENT_TYPE) {
		FIRST_INSTALLMENT_TYPE = fIRST_INSTALLMENT_TYPE;
	}

	public Long getPOLICY_FEE() {
		return POLICY_FEE;
	}

	public void setPOLICY_FEE(Long pOLICY_FEE) {
		POLICY_FEE = pOLICY_FEE;
	}

}
