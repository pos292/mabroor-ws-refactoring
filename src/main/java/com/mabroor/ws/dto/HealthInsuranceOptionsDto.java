package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceOptionsDto {
	
	@JsonProperty("op_code")
	private String op_code;
	
	@JsonProperty("op_value")
	private String op_value;

	public String getOp_code() {
		return op_code;
	}

	public void setOp_code(String op_code) {
		this.op_code = op_code;
	}

	public String getOp_value() {
		return op_value;
	}

	public void setOp_value(String op_value) {
		this.op_value = op_value;
	}

	
	
	

}
