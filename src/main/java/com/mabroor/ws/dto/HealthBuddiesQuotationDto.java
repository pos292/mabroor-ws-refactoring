package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthBuddiesQuotationDto {
	
	@JsonProperty("proposal_type")
	private String proposal_type;
	
	@JsonProperty("product_code")
	private String product_code;
	
	@JsonProperty("package_code")
	private String package_code;
	
	@JsonProperty("bf_code")
	private String bf_code;
	
	@JsonProperty("espaj_filename")
	private String espaj_filename;
	
	@JsonProperty("ereference_number")
	private String ereference_number;
	
	@JsonProperty("hard_copy")
	private String hard_copy;
	
	@JsonProperty("spaj_number")
	private String spaj_number;
	
	@JsonProperty("options")
	private List<String> options;
	
	@JsonProperty("quotationCreateDate")
	private String quotationCreateDate;
	
	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("register_amount")
	private String register_amount;
	
	@JsonProperty("risk_cessation_date")
	private String risk_cessation_date;
	
	@JsonProperty("term_basic")
	private String term_basic;
	
	@JsonProperty("contract_effective_date")
	private String contract_effective_date;
	
	@JsonProperty("payment_term_basic")
	private String payment_term_basic;
	
	@JsonProperty("premium_amount")
	private String premium_amount;

	public String getProposal_type() {
		return proposal_type;
	}

	public void setProposal_type(String proposal_type) {
		this.proposal_type = proposal_type;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getPackage_code() {
		return package_code;
	}

	public void setPackage_code(String package_code) {
		this.package_code = package_code;
	}

	public String getBf_code() {
		return bf_code;
	}

	public void setBf_code(String bf_code) {
		this.bf_code = bf_code;
	}

	public String getEspaj_filename() {
		return espaj_filename;
	}

	public void setEspaj_filename(String espaj_filename) {
		this.espaj_filename = espaj_filename;
	}

	public String getEreference_number() {
		return ereference_number;
	}

	public void setEreference_number(String ereference_number) {
		this.ereference_number = ereference_number;
	}

	public String getHard_copy() {
		return hard_copy;
	}

	public void setHard_copy(String hard_copy) {
		this.hard_copy = hard_copy;
	}

	public String getSpaj_number() {
		return spaj_number;
	}

	public void setSpaj_number(String spaj_number) {
		this.spaj_number = spaj_number;
	}

	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}

	public String getQuotationCreateDate() {
		return quotationCreateDate;
	}

	public void setQuotationCreateDate(String quotationCreateDate) {
		this.quotationCreateDate = quotationCreateDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRegister_amount() {
		return register_amount;
	}

	public void setRegister_amount(String register_amount) {
		this.register_amount = register_amount;
	}

	public String getRisk_cessation_date() {
		return risk_cessation_date;
	}

	public void setRisk_cessation_date(String risk_cessation_date) {
		this.risk_cessation_date = risk_cessation_date;
	}

	public String getTerm_basic() {
		return term_basic;
	}

	public void setTerm_basic(String term_basic) {
		this.term_basic = term_basic;
	}

	public String getContract_effective_date() {
		return contract_effective_date;
	}

	public void setContract_effective_date(String contract_effective_date) {
		this.contract_effective_date = contract_effective_date;
	}

	public String getPayment_term_basic() {
		return payment_term_basic;
	}

	public void setPayment_term_basic(String payment_term_basic) {
		this.payment_term_basic = payment_term_basic;
	}

	public String getPremium_amount() {
		return premium_amount;
	}

	public void setPremium_amount(String premium_amount) {
		this.premium_amount = premium_amount;
	}
	
	

}
