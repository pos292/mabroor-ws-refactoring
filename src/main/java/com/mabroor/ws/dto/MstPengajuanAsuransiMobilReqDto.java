package com.mabroor.ws.dto;

import java.util.List;

import com.mabroor.ws.entity.MstPengajuanAsuransiMobil;
import com.mabroor.ws.entity.MstTransactionCarInsuranceBenefit;

public class MstPengajuanAsuransiMobilReqDto {
	
	private MstPengajuanAsuransiMobil data;
	private List<MstTransactionCarInsuranceBenefit> benefits;
	public MstPengajuanAsuransiMobil getData() {
		return data;
	}
	public void setData(MstPengajuanAsuransiMobil data) {
		this.data = data;
	}
	public List<MstTransactionCarInsuranceBenefit> getBenefits() {
		return benefits;
	}
	public void setBenefits(List<MstTransactionCarInsuranceBenefit> benefits) {
		this.benefits = benefits;
	}

	
	

}
