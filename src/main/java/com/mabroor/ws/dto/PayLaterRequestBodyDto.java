package com.mabroor.ws.dto;

import java.util.LinkedHashMap;

public class PayLaterRequestBodyDto {
	
	private String dataString;
	private LinkedHashMap<String, Object> dataObject;
	public String getDataString() {
		return dataString;
	}
	public void setDataString(String dataString) {
		this.dataString = dataString;
	}
	public LinkedHashMap<String, Object> getDataObject() {
		return dataObject;
	}
	public void setDataObject(LinkedHashMap<String, Object> dataObject) {
		this.dataObject = dataObject;
	}
}
