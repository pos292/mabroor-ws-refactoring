package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class MGU4WTracApiIntegrationDto {
	
	private String clientId;
	private String notesReservation;
	private BigDecimal totalPrice;
	private String pic;
	private String picName;
	private String picPhone;
	private List<MGU4WReservationDetailDto> reservationDetail;
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getNotesReservation() {
		return notesReservation;
	}
	public void setNotesReservation(String notesReservation) {
		this.notesReservation = notesReservation;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getPicName() {
		return picName;
	}
	public void setPicName(String picName) {
		this.picName = picName;
	}
	public String getPicPhone() {
		return picPhone;
	}
	public void setPicPhone(String picPhone) {
		this.picPhone = picPhone;
	}
	public List<MGU4WReservationDetailDto> getReservationDetail() {
		return reservationDetail;
	}
	public void setReservationDetail(List<MGU4WReservationDetailDto> reservationDetail) {
		this.reservationDetail = reservationDetail;
	}
	
	

}
