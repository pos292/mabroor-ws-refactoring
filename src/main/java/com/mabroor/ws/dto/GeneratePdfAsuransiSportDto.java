package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class GeneratePdfAsuransiSportDto {
	
	private String name;
	private String email;
	private String periodeLindung;
	private String tanggalMulai;
	private BigDecimal premi;
	private BigDecimal uangPertanggunan;
	private BigDecimal totalPremi;
	private String phone;
	private String namaLengkap;
	private String birthDate;
	private String noKtp;
	private List<PdfAsuransiManfaatSport> benefits;
	private List<PdfAsuransiManfaatSportFisioterapi> additionalBenefit;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPeriodeLindung() {
		return periodeLindung;
	}
	public void setPeriodeLindung(String periodeLindung) {
		this.periodeLindung = periodeLindung;
	}
	public String getTanggalMulai() {
		return tanggalMulai;
	}
	public void setTanggalMulai(String tanggalMulai) {
		this.tanggalMulai = tanggalMulai;
	}
	public BigDecimal getPremi() {
		return premi;
	}
	public void setPremi(BigDecimal premi) {
		this.premi = premi;
	}
	public BigDecimal getUangPertanggunan() {
		return uangPertanggunan;
	}
	public void setUangPertanggunan(BigDecimal uangPertanggunan) {
		this.uangPertanggunan = uangPertanggunan;
	}
	public BigDecimal getTotalPremi() {
		return totalPremi;
	}
	public void setTotalPremi(BigDecimal totalPremi) {
		this.totalPremi = totalPremi;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNamaLengkap() {
		return namaLengkap;
	}
	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getNoKtp() {
		return noKtp;
	}
	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}
	public List<PdfAsuransiManfaatSport> getBenefits() {
		return benefits;
	}
	public void setBenefits(List<PdfAsuransiManfaatSport> benefits) {
		this.benefits = benefits;
	}
	public List<PdfAsuransiManfaatSportFisioterapi> getAdditionalBenefit() {
		return additionalBenefit;
	}
	public void setAdditionalBenefit(List<PdfAsuransiManfaatSportFisioterapi> additionalBenefit) {
		this.additionalBenefit = additionalBenefit;
	}
	
	
	
	

}
