package com.mabroor.ws.dto;

public class PdfAsuransiDataTambahanHealthBuddies {
	
	public int lifeIndex;
	private String namaDepanTer;
	private String namaBelakangTer;
	private String TanggalLahir;
	private String hub;
	
	public String getNamaDepanTer() {
		return namaDepanTer;
	}
	public void setNamaDepanTer(String namaDepanTer) {
		this.namaDepanTer = namaDepanTer;
	}
	public String getNamaBelakangTer() {
		return namaBelakangTer;
	}
	public void setNamaBelakangTer(String namaBelakangTer) {
		this.namaBelakangTer = namaBelakangTer;
	}
	public String getTanggalLahir() {
		return TanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		TanggalLahir = tanggalLahir;
	}
	public String getHub() {
		return hub;
	}
	public void setHub(String hub) {
		this.hub = hub;
	}
	public int getLifeIndex() {
		return lifeIndex;
	}
	public void setLifeIndex(int lifeIndex) {
		this.lifeIndex = lifeIndex;
	}
	
	


}
