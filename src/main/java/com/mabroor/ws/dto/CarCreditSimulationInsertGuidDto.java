package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarCreditSimulationInsertGuidDto {
	
	@JsonProperty("GUID")
	private String guid;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}
	
	

}
