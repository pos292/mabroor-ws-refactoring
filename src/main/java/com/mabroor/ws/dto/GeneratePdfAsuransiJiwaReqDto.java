package com.mabroor.ws.dto;

import java.math.BigDecimal;

public class GeneratePdfAsuransiJiwaReqDto {

	private String name;
	private String usia;
	private BigDecimal uangPertanggungan;
	private String gender;
	private BigDecimal yearlyPremium;
	private BigDecimal monthlyPremium;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsia() {
		return usia;
	}
	public void setUsia(String usia) {
		this.usia = usia;
	}
	public BigDecimal getUangPertanggungan() {
		return uangPertanggungan;
	}
	public void setUangPertanggungan(BigDecimal uangPertanggungan) {
		this.uangPertanggungan = uangPertanggungan;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public BigDecimal getYearlyPremium() {
		return yearlyPremium;
	}
	public void setYearlyPremium(BigDecimal yearlyPremium) {
		this.yearlyPremium = yearlyPremium;
	}
	public BigDecimal getMonthlyPremium() {
		return monthlyPremium;
	}
	public void setMonthlyPremium(BigDecimal monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}
	
	
	
	
	
	
}
