package com.mabroor.ws.dto;

public class MstDoaCategoryDto {
	
	private Long id;
	
	private String categoryName;
	
	private String icon;
	
	private String countDoa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getCountDoa() {
		return countDoa;
	}

	public void setCountDoa(String countDoa) {
		this.countDoa = countDoa;
	}
}
