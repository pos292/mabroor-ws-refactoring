package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


public class CarSubmissionRequestDto {
	
	@JsonProperty("P_ACCOUNT_ID")
	private String P_ACCOUNT_ID;
	
	@JsonProperty("P_CHANNEL")
	private String P_CHANNEL;
	
	@JsonProperty("P_NAME")
	private String P_NAME;
	
	@JsonProperty("P_EMAIL")
	private String P_EMAIL;
	
	@JsonProperty("P_FLAG_NEW_USED_VALID")
	private String P_FLAG_NEW_USED_VALID;
	
	@JsonProperty("P_PRODUCT")
	private String P_PRODUCT;
	
	@JsonProperty("P_CD_VEHICLE_BRAND")
	private String P_CD_VEHICLE_BRAND;
	
	@JsonProperty("P_CD_VEHICLE_TYPE")
	private String P_CD_VEHICLE_TYPE;
	
	@JsonProperty("P_YEAR_OF_MFG")
	private String P_YEAR_OF_MFG;
	
	@JsonProperty("P_TENOR")
	private String P_TENOR;
	
	@JsonProperty("P_DP")
	private String P_DP;
	
	@JsonProperty("P_PHONE_NUMBER")
	private String P_PHONE_NUMBER;
	
	@JsonProperty("P_CD_AREA")
	private String P_CD_AREA;
	
	@JsonProperty("P_NOTES")
	private String P_NOTES;
	
	@JsonProperty("P_CD_SP")
	private String P_CD_SP;
	
	@JsonProperty("P_SOURCE_LEADS")
	private String P_SOURCE_LEADS;

	public String getP_ACCOUNT_ID() {
		return P_ACCOUNT_ID;
	}

	public void setP_ACCOUNT_ID(String p_ACCOUNT_ID) {
		P_ACCOUNT_ID = p_ACCOUNT_ID;
	}

	public String getP_CHANNEL() {
		return P_CHANNEL;
	}

	public void setP_CHANNEL(String p_CHANNEL) {
		P_CHANNEL = p_CHANNEL;
	}

	public String getP_NAME() {
		return P_NAME;
	}

	public void setP_NAME(String p_NAME) {
		P_NAME = p_NAME;
	}

	public String getP_EMAIL() {
		return P_EMAIL;
	}

	public void setP_EMAIL(String p_EMAIL) {
		P_EMAIL = p_EMAIL;
	}
	
	public String getP_FLAG_NEW_USED_VALID() {
		return P_FLAG_NEW_USED_VALID;
	}

	public void setP_FLAG_NEW_USED_VALID(String p_FLAG_NEW_USED_VALID) {
		P_FLAG_NEW_USED_VALID = p_FLAG_NEW_USED_VALID;
	}

	public String getP_PRODUCT() {
		return P_PRODUCT;
	}

	public void setP_PRODUCT(String p_PRODUCT) {
		P_PRODUCT = p_PRODUCT;
	}

	public String getP_PHONE_NUMBER() {
		return P_PHONE_NUMBER;
	}

	public void setP_PHONE_NUMBER(String p_PHONE_NUMBER) {
		P_PHONE_NUMBER = p_PHONE_NUMBER;
	}

	public String getP_CD_AREA() {
		return P_CD_AREA;
	}

	public void setP_CD_AREA(String p_CD_AREA) {
		P_CD_AREA = p_CD_AREA;
	}

	public String getP_NOTES() {
		return P_NOTES;
	}

	public void setP_NOTES(String p_NOTES) {
		P_NOTES = p_NOTES;
	}

	public String getP_SOURCE_LEADS() {
		return P_SOURCE_LEADS;
	}

	public void setP_SOURCE_LEADS(String p_SOURCE_LEADS) {
		P_SOURCE_LEADS = p_SOURCE_LEADS;
	}

	public String getP_CD_SP() {
		return P_CD_SP;
	}

	public void setP_CD_SP(String p_CD_SP) {
		P_CD_SP = p_CD_SP;
	}

	public String getP_CD_VEHICLE_BRAND() {
		return P_CD_VEHICLE_BRAND;
	}

	public void setP_CD_VEHICLE_BRAND(String p_CD_VEHICLE_BRAND) {
		P_CD_VEHICLE_BRAND = p_CD_VEHICLE_BRAND;
	}

	public String getP_CD_VEHICLE_TYPE() {
		return P_CD_VEHICLE_TYPE;
	}

	public void setP_CD_VEHICLE_TYPE(String p_CD_VEHICLE_TYPE) {
		P_CD_VEHICLE_TYPE = p_CD_VEHICLE_TYPE;
	}

	public String getP_YEAR_OF_MFG() {
		return P_YEAR_OF_MFG;
	}

	public void setP_YEAR_OF_MFG(String p_YEAR_OF_MFG) {
		P_YEAR_OF_MFG = p_YEAR_OF_MFG;
	}

	public String getP_TENOR() {
		return P_TENOR;
	}

	public void setP_TENOR(String p_TENOR) {
		P_TENOR = p_TENOR;
	}

	public String getP_DP() {
		return P_DP;
	}

	public void setP_DP(String p_DP) {
		P_DP = p_DP;
	}
	
	
}