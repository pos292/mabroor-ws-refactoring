package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceOptionalRiderDto {
	
	@JsonProperty("rider_code")
	private String rider_code;
	
	@JsonProperty("rider_type")
	private String rider_type;

	public String getRider_code() {
		return rider_code;
	}

	public void setRider_code(String rider_code) {
		this.rider_code = rider_code;
	}

	public String getRider_type() {
		return rider_type;
	}

	public void setRider_type(String rider_type) {
		this.rider_type = rider_type;
	}
	
	

}
