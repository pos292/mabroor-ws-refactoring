package com.mabroor.ws.dto;

public class DictionaryReqBodyDto {

	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
