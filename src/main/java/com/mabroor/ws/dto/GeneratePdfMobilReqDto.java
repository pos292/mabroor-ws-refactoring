package com.mabroor.ws.dto;

import java.math.BigDecimal;

public class GeneratePdfMobilReqDto {

	private String dp;
	private String name;
	private String company;
	private String kota;
	private String tahun;
	private String brand;
	private String tipe;
	private String gender;
	private String namaDepan;
	private String namaBelakang;
	private String birth;
	private String phone;
	private String email;
	private String tenor;
	private BigDecimal nominalAsuransi;
	private BigDecimal polisAsuransi;
	private BigDecimal perluasanAsuransi;
	private BigDecimal totalAsuransi;
	
	public String getDp() {
		return dp;
	}
	public void setDp(String dp) {
		this.dp = dp;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKota() {
		return kota;
	}
	public void setKota(String kota) {
		this.kota = kota;
	}
	public String getTahun() {
		return tahun;
	}
	public void setTahun(String tahun) {
		this.tahun = tahun;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNamaDepan() {
		return namaDepan;
	}
	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}
	public String getNamaBelakang() {
		return namaBelakang;
	}
	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public BigDecimal getNominalAsuransi() {
		return nominalAsuransi;
	}
	public void setNominalAsuransi(BigDecimal nominalAsuransi) {
		this.nominalAsuransi = nominalAsuransi;
	}
	public BigDecimal getPolisAsuransi() {
		return polisAsuransi;
	}
	public void setPolisAsuransi(BigDecimal polisAsuransi) {
		this.polisAsuransi = polisAsuransi;
	}
	public BigDecimal getPerluasanAsuransi() {
		return perluasanAsuransi;
	}
	public void setPerluasanAsuransi(BigDecimal perluasanAsuransi) {
		this.perluasanAsuransi = perluasanAsuransi;
	}
	public BigDecimal getTotalAsuransi() {
		return totalAsuransi;
	}
	public void setTotalAsuransi(BigDecimal totalAsuransi) {
		this.totalAsuransi = totalAsuransi;
	}
	
	
}
