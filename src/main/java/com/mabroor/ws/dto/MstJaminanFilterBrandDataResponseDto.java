package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MstJaminanFilterBrandDataResponseDto {
	
	@JsonProperty("CD_BRAND")
	private String CD_BRAND;
	
	@JsonProperty("DESC_BRAND")
	private String DESC_BRAND;

	public String getCD_BRAND() {
		return CD_BRAND;
	}

	public void setCD_BRAND(String cD_BRAND) {
		CD_BRAND = cD_BRAND;
	}

	public String getDESC_BRAND() {
		return DESC_BRAND;
	}

	public void setDESC_BRAND(String dESC_BRAND) {
		DESC_BRAND = dESC_BRAND;
	}
	

}
