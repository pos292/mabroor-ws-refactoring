package com.mabroor.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxCarInsurancePaymentStatusDto {
	
	@JsonProperty("Status")
	private boolean status;
	@JsonProperty("CheckPaymentStatus")
	private boolean checkPaymentStatus;
	@JsonProperty("PaymentStatus")
	private String paymentStatus;
	private String message;
	@JsonProperty("OrderInfo")
	private List<TrxCarInsuranceOrderInfo> orderInfo;
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isCheckPaymentStatus() {
		return checkPaymentStatus;
	}
	public void setCheckPaymentStatus(boolean checkPaymentStatus) {
		this.checkPaymentStatus = checkPaymentStatus;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<TrxCarInsuranceOrderInfo> getOrderInfo() {
		return orderInfo;
	}
	public void setOrderInfo(List<TrxCarInsuranceOrderInfo> orderInfo) {
		this.orderInfo = orderInfo;
	}
	
	

}
