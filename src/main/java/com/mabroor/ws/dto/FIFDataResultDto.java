package com.mabroor.ws.dto;

public class FIFDataResultDto {
	
	private String fifastraId;
	
	private String tipeMotor;
	
	private String dp;
	
	private String tenor;
	
	private String fullname;
	
	private String customerType;
	
	private String noKtp;
	
	private String mobilePhone1;
	
	private String mobilePhone2;
	
	private String email;
	
	private String birthdate;
	
	private String addressLine;
	
	private String stateId;
	
	private String cityId;
	
	private String districtId;
	
	private String subDistrictId;
	
	private String zipcode;
	
	private String subZipcode;
	
	private String homeOwnership;
	
	private String maritalStatus;
	
	private String education;
	
	private String totalDependents;
	
	private String referral;
	
	private String submitDate;
	
	private String interest;
	
	private String approved;
	
	private String sourceOfInput;
	
	private String interestDate;
	
	private String interestBy;
	
	private String approvedDate;
	
	private String approvedBy;
	
	private String isBlacklist;
	
	private String noNPWP;
	
	private String birthplace;
	
	private String gender;
	
	private String motherMaidenName;
	
	private String occupation;
	
	private String monthlySalary;
	
	private String monthlyExpense;
	
	private String bank;
	
	private String bankAccountNumber;
	
	private String domAddressLine;
	
	private String domStateId;
	
	private String domCityId;
	
	private String domDistrictId;
	
	private String domSubDistrictId;
	
	private String domZipcode;
	
	private String domSubZipcode;
	
	private String ktpRT;
	
	private String ktpRW;
	
	private String domRT;
	
	private String domRW;
	
	private String remark;
	
	private String fotoKTP;
	
	private String fotoKTPPasangan;
	
	private String fotoKK;
	
	private String hasNPWP;
	
	private String hasBankAccount;
	
	private String hasCC;
	
	private String jobPosition;
	
	private String spouseOccupation;
	
	private String spouseJobPosition;
	
	private String ccBank;
	
	private String ccNo;
	
	private String pengirimanKendaraan;
	
	private String bpkbOwnership;
	
	private String bpkbOwnerName;
	
	private String bpkbAddressSameAs;
	
	private String bpkbOwnerAddressLine;
	
	private String bpkbOwnerStateId;
	
	private String bpkbOwnerCityId;
	
	private String bpkbOwnerDistrictId;
	
	private String bpkbOwnerSubDistrictId;
	
	private String bpkbOwnerZipcode;
	
	private String bpkbOwnerSubZipcode;
	
	private String bpkbOwnerRT;
	
	private String bpkbOwnerRW;
	
	private String leadId;
	
	private String leadDataSelfieId;
	
	private String leadDataKTPId;
	
	private String longitude;
	
	private String latitude;
	
	private String similaritySelfieAndKTP;
	
	private String userKey;
	
	private String stateName;
	
	private String cityName;
	
	private String districtName;
	
	private String subDistrictName;
	
	private String domStateName;
	
	private String domCityName;
	
	private String domDistrictName;
	
	private String domSubDistrictName;
	
	private String bpkbOwnerStateName;
	
	private String bpkbOwnerCityName;
	
	private String bpkbOwnerDistrictName;
	
	private String bpkbOwnerSubDistrictName;
	
	private String digitalLeadId;
	
	private String customerNo;
	
	private String fgcNo;
	
	private String occupationType;
	
	private String spouseOccupationType;
	
	private String purpose;
	
	private String callVia;
	
	private String contactDetail;
	
	private String customerTypeName;
	
	private String educationName;
	
	private String homeOwnershipName;
	
	private String occupationName;
	
	private String spouseOccupationName;
	
	private String bankName;
	
	private String tenorName;
	
	private String pengirimanFifastra;
	
	private String bpkbOwnershipName;
	
	private String communityName;
	
	private String leadDataSelfie;
	
	private String leadDataKTP;
	
	private String status;
	
	private String callViaName;

	public String getFifastraId() {
		return fifastraId;
	}

	public void setFifastraId(String fifastraId) {
		this.fifastraId = fifastraId;
	}

	public String getTipeMotor() {
		return tipeMotor;
	}

	public void setTipeMotor(String tipeMotor) {
		this.tipeMotor = tipeMotor;
	}

	public String getDp() {
		return dp;
	}

	public void setDp(String dp) {
		this.dp = dp;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getNoKtp() {
		return noKtp;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public String getMobilePhone1() {
		return mobilePhone1;
	}

	public void setMobilePhone1(String mobilePhone1) {
		this.mobilePhone1 = mobilePhone1;
	}

	public String getMobilePhone2() {
		return mobilePhone2;
	}

	public void setMobilePhone2(String mobilePhone2) {
		this.mobilePhone2 = mobilePhone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getDistrictId() {
		return districtId;
	}

	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	public String getSubDistrictId() {
		return subDistrictId;
	}

	public void setSubDistrictId(String subDistrictId) {
		this.subDistrictId = subDistrictId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSubZipcode() {
		return subZipcode;
	}

	public void setSubZipcode(String subZipcode) {
		this.subZipcode = subZipcode;
	}

	public String getHomeOwnership() {
		return homeOwnership;
	}

	public void setHomeOwnership(String homeOwnership) {
		this.homeOwnership = homeOwnership;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getTotalDependents() {
		return totalDependents;
	}

	public void setTotalDependents(String totalDependents) {
		this.totalDependents = totalDependents;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getApproved() {
		return approved;
	}

	public void setApproved(String approved) {
		this.approved = approved;
	}

	public String getSourceOfInput() {
		return sourceOfInput;
	}

	public void setSourceOfInput(String sourceOfInput) {
		this.sourceOfInput = sourceOfInput;
	}

	public String getInterestDate() {
		return interestDate;
	}

	public void setInterestDate(String interestDate) {
		this.interestDate = interestDate;
	}

	public String getInterestBy() {
		return interestBy;
	}

	public void setInterestBy(String interestBy) {
		this.interestBy = interestBy;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getIsBlacklist() {
		return isBlacklist;
	}

	public void setIsBlacklist(String isBlacklist) {
		this.isBlacklist = isBlacklist;
	}

	public String getNoNPWP() {
		return noNPWP;
	}

	public void setNoNPWP(String noNPWP) {
		this.noNPWP = noNPWP;
	}

	public String getBirthplace() {
		return birthplace;
	}

	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMotherMaidenName() {
		return motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getMonthlySalary() {
		return monthlySalary;
	}

	public void setMonthlySalary(String monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	public String getMonthlyExpense() {
		return monthlyExpense;
	}

	public void setMonthlyExpense(String monthlyExpense) {
		this.monthlyExpense = monthlyExpense;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getDomAddressLine() {
		return domAddressLine;
	}

	public void setDomAddressLine(String domAddressLine) {
		this.domAddressLine = domAddressLine;
	}

	public String getDomStateId() {
		return domStateId;
	}

	public void setDomStateId(String domStateId) {
		this.domStateId = domStateId;
	}

	public String getDomCityId() {
		return domCityId;
	}

	public void setDomCityId(String domCityId) {
		this.domCityId = domCityId;
	}

	public String getDomDistrictId() {
		return domDistrictId;
	}

	public void setDomDistrictId(String domDistrictId) {
		this.domDistrictId = domDistrictId;
	}

	public String getDomSubDistrictId() {
		return domSubDistrictId;
	}

	public void setDomSubDistrictId(String domSubDistrictId) {
		this.domSubDistrictId = domSubDistrictId;
	}

	public String getDomZipcode() {
		return domZipcode;
	}

	public void setDomZipcode(String domZipcode) {
		this.domZipcode = domZipcode;
	}

	public String getDomSubZipcode() {
		return domSubZipcode;
	}

	public void setDomSubZipcode(String domSubZipcode) {
		this.domSubZipcode = domSubZipcode;
	}

	public String getKtpRT() {
		return ktpRT;
	}

	public void setKtpRT(String ktpRT) {
		this.ktpRT = ktpRT;
	}

	public String getKtpRW() {
		return ktpRW;
	}

	public void setKtpRW(String ktpRW) {
		this.ktpRW = ktpRW;
	}

	public String getDomRT() {
		return domRT;
	}

	public void setDomRT(String domRT) {
		this.domRT = domRT;
	}

	public String getDomRW() {
		return domRW;
	}

	public void setDomRW(String domRW) {
		this.domRW = domRW;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getFotoKTP() {
		return fotoKTP;
	}

	public void setFotoKTP(String fotoKTP) {
		this.fotoKTP = fotoKTP;
	}

	public String getFotoKTPPasangan() {
		return fotoKTPPasangan;
	}

	public void setFotoKTPPasangan(String fotoKTPPasangan) {
		this.fotoKTPPasangan = fotoKTPPasangan;
	}

	public String getFotoKK() {
		return fotoKK;
	}

	public void setFotoKK(String fotoKK) {
		this.fotoKK = fotoKK;
	}

	public String getHasNPWP() {
		return hasNPWP;
	}

	public void setHasNPWP(String hasNPWP) {
		this.hasNPWP = hasNPWP;
	}

	public String getHasBankAccount() {
		return hasBankAccount;
	}

	public void setHasBankAccount(String hasBankAccount) {
		this.hasBankAccount = hasBankAccount;
	}

	public String getHasCC() {
		return hasCC;
	}

	public void setHasCC(String hasCC) {
		this.hasCC = hasCC;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public String getSpouseOccupation() {
		return spouseOccupation;
	}

	public void setSpouseOccupation(String spouseOccupation) {
		this.spouseOccupation = spouseOccupation;
	}

	public String getSpouseJobPosition() {
		return spouseJobPosition;
	}

	public void setSpouseJobPosition(String spouseJobPosition) {
		this.spouseJobPosition = spouseJobPosition;
	}

	public String getCcBank() {
		return ccBank;
	}

	public void setCcBank(String ccBank) {
		this.ccBank = ccBank;
	}

	public String getCcNo() {
		return ccNo;
	}

	public void setCcNo(String ccNo) {
		this.ccNo = ccNo;
	}

	public String getPengirimanKendaraan() {
		return pengirimanKendaraan;
	}

	public void setPengirimanKendaraan(String pengirimanKendaraan) {
		this.pengirimanKendaraan = pengirimanKendaraan;
	}

	public String getBpkbOwnership() {
		return bpkbOwnership;
	}

	public void setBpkbOwnership(String bpkbOwnership) {
		this.bpkbOwnership = bpkbOwnership;
	}

	public String getBpkbOwnerName() {
		return bpkbOwnerName;
	}

	public void setBpkbOwnerName(String bpkbOwnerName) {
		this.bpkbOwnerName = bpkbOwnerName;
	}

	public String getBpkbAddressSameAs() {
		return bpkbAddressSameAs;
	}

	public void setBpkbAddressSameAs(String bpkbAddressSameAs) {
		this.bpkbAddressSameAs = bpkbAddressSameAs;
	}

	public String getBpkbOwnerAddressLine() {
		return bpkbOwnerAddressLine;
	}

	public void setBpkbOwnerAddressLine(String bpkbOwnerAddressLine) {
		this.bpkbOwnerAddressLine = bpkbOwnerAddressLine;
	}

	public String getBpkbOwnerStateId() {
		return bpkbOwnerStateId;
	}

	public void setBpkbOwnerStateId(String bpkbOwnerStateId) {
		this.bpkbOwnerStateId = bpkbOwnerStateId;
	}

	public String getBpkbOwnerCityId() {
		return bpkbOwnerCityId;
	}

	public void setBpkbOwnerCityId(String bpkbOwnerCityId) {
		this.bpkbOwnerCityId = bpkbOwnerCityId;
	}

	public String getBpkbOwnerDistrictId() {
		return bpkbOwnerDistrictId;
	}

	public void setBpkbOwnerDistrictId(String bpkbOwnerDistrictId) {
		this.bpkbOwnerDistrictId = bpkbOwnerDistrictId;
	}

	public String getBpkbOwnerSubDistrictId() {
		return bpkbOwnerSubDistrictId;
	}

	public void setBpkbOwnerSubDistrictId(String bpkbOwnerSubDistrictId) {
		this.bpkbOwnerSubDistrictId = bpkbOwnerSubDistrictId;
	}

	public String getBpkbOwnerZipcode() {
		return bpkbOwnerZipcode;
	}

	public void setBpkbOwnerZipcode(String bpkbOwnerZipcode) {
		this.bpkbOwnerZipcode = bpkbOwnerZipcode;
	}

	public String getBpkbOwnerSubZipcode() {
		return bpkbOwnerSubZipcode;
	}

	public void setBpkbOwnerSubZipcode(String bpkbOwnerSubZipcode) {
		this.bpkbOwnerSubZipcode = bpkbOwnerSubZipcode;
	}

	public String getBpkbOwnerRT() {
		return bpkbOwnerRT;
	}

	public void setBpkbOwnerRT(String bpkbOwnerRT) {
		this.bpkbOwnerRT = bpkbOwnerRT;
	}

	public String getBpkbOwnerRW() {
		return bpkbOwnerRW;
	}

	public void setBpkbOwnerRW(String bpkbOwnerRW) {
		this.bpkbOwnerRW = bpkbOwnerRW;
	}

	public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	public String getLeadDataSelfieId() {
		return leadDataSelfieId;
	}

	public void setLeadDataSelfieId(String leadDataSelfieId) {
		this.leadDataSelfieId = leadDataSelfieId;
	}

	public String getLeadDataKTPId() {
		return leadDataKTPId;
	}

	public void setLeadDataKTPId(String leadDataKTPId) {
		this.leadDataKTPId = leadDataKTPId;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getSimilaritySelfieAndKTP() {
		return similaritySelfieAndKTP;
	}

	public void setSimilaritySelfieAndKTP(String similaritySelfieAndKTP) {
		this.similaritySelfieAndKTP = similaritySelfieAndKTP;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getSubDistrictName() {
		return subDistrictName;
	}

	public void setSubDistrictName(String subDistrictName) {
		this.subDistrictName = subDistrictName;
	}

	public String getDomStateName() {
		return domStateName;
	}

	public void setDomStateName(String domStateName) {
		this.domStateName = domStateName;
	}

	public String getDomCityName() {
		return domCityName;
	}

	public void setDomCityName(String domCityName) {
		this.domCityName = domCityName;
	}

	public String getDomDistrictName() {
		return domDistrictName;
	}

	public void setDomDistrictName(String domDistrictName) {
		this.domDistrictName = domDistrictName;
	}

	public String getDomSubDistrictName() {
		return domSubDistrictName;
	}

	public void setDomSubDistrictName(String domSubDistrictName) {
		this.domSubDistrictName = domSubDistrictName;
	}

	public String getBpkbOwnerStateName() {
		return bpkbOwnerStateName;
	}

	public void setBpkbOwnerStateName(String bpkbOwnerStateName) {
		this.bpkbOwnerStateName = bpkbOwnerStateName;
	}

	public String getBpkbOwnerCityName() {
		return bpkbOwnerCityName;
	}

	public void setBpkbOwnerCityName(String bpkbOwnerCityName) {
		this.bpkbOwnerCityName = bpkbOwnerCityName;
	}

	public String getBpkbOwnerDistrictName() {
		return bpkbOwnerDistrictName;
	}

	public void setBpkbOwnerDistrictName(String bpkbOwnerDistrictName) {
		this.bpkbOwnerDistrictName = bpkbOwnerDistrictName;
	}

	public String getBpkbOwnerSubDistrictName() {
		return bpkbOwnerSubDistrictName;
	}

	public void setBpkbOwnerSubDistrictName(String bpkbOwnerSubDistrictName) {
		this.bpkbOwnerSubDistrictName = bpkbOwnerSubDistrictName;
	}

	public String getDigitalLeadId() {
		return digitalLeadId;
	}

	public void setDigitalLeadId(String digitalLeadId) {
		this.digitalLeadId = digitalLeadId;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getFgcNo() {
		return fgcNo;
	}

	public void setFgcNo(String fgcNo) {
		this.fgcNo = fgcNo;
	}

	public String getOccupationType() {
		return occupationType;
	}

	public void setOccupationType(String occupationType) {
		this.occupationType = occupationType;
	}

	public String getSpouseOccupationType() {
		return spouseOccupationType;
	}

	public void setSpouseOccupationType(String spouseOccupationType) {
		this.spouseOccupationType = spouseOccupationType;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getCallVia() {
		return callVia;
	}

	public void setCallVia(String callVia) {
		this.callVia = callVia;
	}

	public String getContactDetail() {
		return contactDetail;
	}

	public void setContactDetail(String contactDetail) {
		this.contactDetail = contactDetail;
	}

	public String getCustomerTypeName() {
		return customerTypeName;
	}

	public void setCustomerTypeName(String customerTypeName) {
		this.customerTypeName = customerTypeName;
	}

	public String getEducationName() {
		return educationName;
	}

	public void setEducationName(String educationName) {
		this.educationName = educationName;
	}

	public String getHomeOwnershipName() {
		return homeOwnershipName;
	}

	public void setHomeOwnershipName(String homeOwnershipName) {
		this.homeOwnershipName = homeOwnershipName;
	}

	public String getOccupationName() {
		return occupationName;
	}

	public void setOccupationName(String occupationName) {
		this.occupationName = occupationName;
	}

	public String getSpouseOccupationName() {
		return spouseOccupationName;
	}

	public void setSpouseOccupationName(String spouseOccupationName) {
		this.spouseOccupationName = spouseOccupationName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getTenorName() {
		return tenorName;
	}

	public void setTenorName(String tenorName) {
		this.tenorName = tenorName;
	}

	public String getPengirimanFifastra() {
		return pengirimanFifastra;
	}

	public void setPengirimanFifastra(String pengirimanFifastra) {
		this.pengirimanFifastra = pengirimanFifastra;
	}

	public String getBpkbOwnershipName() {
		return bpkbOwnershipName;
	}

	public void setBpkbOwnershipName(String bpkbOwnershipName) {
		this.bpkbOwnershipName = bpkbOwnershipName;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getLeadDataSelfie() {
		return leadDataSelfie;
	}

	public void setLeadDataSelfie(String leadDataSelfie) {
		this.leadDataSelfie = leadDataSelfie;
	}

	public String getLeadDataKTP() {
		return leadDataKTP;
	}

	public void setLeadDataKTP(String leadDataKTP) {
		this.leadDataKTP = leadDataKTP;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCallViaName() {
		return callViaName;
	}

	public void setCallViaName(String callViaName) {
		this.callViaName = callViaName;
	}

}
