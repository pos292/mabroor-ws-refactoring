package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class GeneratePdfAsuransiHealthBuddiesDto {
	
	private String name;
	private String email;
	private List<AdditionalHealthBuddiesBenefitDto> additionalBenefits;
	private String tanggalLahirTertanggung;
	private String hubunganDenganPemegangPolis;
	private String namaDepanTertanggung1;
	private String namaBelakangTertanggung1;
	private String tanggalLahirTertanggung1;
	private String hubunganDenganPemegangPolis1;
	private BigDecimal premi;
	private BigDecimal uangPertanggunan;
	private BigDecimal totalPremi;
	private String phone;
	private String namaDepan;
	private String namaBelakang;
	private String birthDate;
	private String noKtp;
	private String address;
	private String zipCode;
	private String noRek;
	private String namaNoRek;
	private String namaBank;
	private List<PdfAsuransiManfaatHealthBuddies> benefits;
	private List<PdfAsuransiDataTambahanHealthBuddies> tertanggungTambahan;
	
	
	public String getNamaDepanTertanggung1() {
		return namaDepanTertanggung1;
	}
	public void setNamaDepanTertanggung1(String namaDepanTertanggung1) {
		this.namaDepanTertanggung1 = namaDepanTertanggung1;
	}
	public String getNamaBelakangTertanggung1() {
		return namaBelakangTertanggung1;
	}
	public void setNamaBelakangTertanggung1(String namaBelakangTertanggung1) {
		this.namaBelakangTertanggung1 = namaBelakangTertanggung1;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public BigDecimal getPremi() {
		return premi;
	}
	public void setPremi(BigDecimal premi) {
		this.premi = premi;
	}
	public BigDecimal getUangPertanggunan() {
		return uangPertanggunan;
	}
	public void setUangPertanggunan(BigDecimal uangPertanggunan) {
		this.uangPertanggunan = uangPertanggunan;
	}
	public BigDecimal getTotalPremi() {
		return totalPremi;
	}
	public void setTotalPremi(BigDecimal totalPremi) {
		this.totalPremi = totalPremi;
	}
	public String getNamaDepan() {
		return namaDepan;
	}
	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}
	public String getNamaBelakang() {
		return namaBelakang;
	}
	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getNoKtp() {
		return noKtp;
	}
	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getNoRek() {
		return noRek;
	}
	public void setNoRek(String noRek) {
		this.noRek = noRek;
	}
	public String getNamaNoRek() {
		return namaNoRek;
	}
	public void setNamaNoRek(String namaNoRek) {
		this.namaNoRek = namaNoRek;
	}
	public String getNamaBank() {
		return namaBank;
	}
	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}
	public String getTanggalLahirTertanggung() {
		return tanggalLahirTertanggung;
	}
	public void setTanggalLahirTertanggung(String tanggalLahirTertanggung) {
		this.tanggalLahirTertanggung = tanggalLahirTertanggung;
	}
	public String getHubunganDenganPemegangPolis() {
		return hubunganDenganPemegangPolis;
	}
	public void setHubunganDenganPemegangPolis(String hubunganDenganPemegangPolis) {
		this.hubunganDenganPemegangPolis = hubunganDenganPemegangPolis;
	}
	public String getTanggalLahirTertanggung1() {
		return tanggalLahirTertanggung1;
	}
	public void setTanggalLahirTertanggung1(String tanggalLahirTertanggung1) {
		this.tanggalLahirTertanggung1 = tanggalLahirTertanggung1;
	}
	public String getHubunganDenganPemegangPolis1() {
		return hubunganDenganPemegangPolis1;
	}
	public void setHubunganDenganPemegangPolis1(String hubunganDenganPemegangPolis1) {
		this.hubunganDenganPemegangPolis1 = hubunganDenganPemegangPolis1;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public List<PdfAsuransiManfaatHealthBuddies> getBenefits() {
		return benefits;
	}
	public void setBenefits(List<PdfAsuransiManfaatHealthBuddies> benefits) {
		this.benefits = benefits;
	}
	public List<PdfAsuransiDataTambahanHealthBuddies> getTertanggungTambahan() {
		return tertanggungTambahan;
	}
	public void setTertanggungTambahan(List<PdfAsuransiDataTambahanHealthBuddies> tertanggungTambahan) {
		this.tertanggungTambahan = tertanggungTambahan;
	}
	public List<AdditionalHealthBuddiesBenefitDto> getAdditionalBenefits() {
		return additionalBenefits;
	}
	public void setAdditionalBenefits(List<AdditionalHealthBuddiesBenefitDto> additionalBenefits) {
		this.additionalBenefits = additionalBenefits;
	}
	
	
	

}
