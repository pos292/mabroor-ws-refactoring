package com.mabroor.ws.dto;

public class DoaSearchDto {

	private String namaDoa;

	public String getNamaDoa() {
		return namaDoa;
	}

	public void setNamaDoa(String namaDoa) {
		this.namaDoa = namaDoa;
	}
}
