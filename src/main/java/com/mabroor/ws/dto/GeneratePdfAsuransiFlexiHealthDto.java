package com.mabroor.ws.dto;

import java.math.BigDecimal;
import java.util.List;

public class GeneratePdfAsuransiFlexiHealthDto {
	
	private String name;
	private String email;
	private String periodeLindung;
	private BigDecimal premi;
	private BigDecimal uangPertanggunan;
	private BigDecimal totalPremi;
	private String phone;
	private String namaDepan;
	private String namaBelakang;
	private String birthDate;
	private String noKtp;
	private String address;
	private String zipCode;
	private List<PdfAsuransiManfaatFlexiHealth> benefits;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public BigDecimal getPremi() {
		return premi;
	}
	public void setPremi(BigDecimal premi) {
		this.premi = premi;
	}
	public BigDecimal getUangPertanggunan() {
		return uangPertanggunan;
	}
	public void setUangPertanggunan(BigDecimal uangPertanggunan) {
		this.uangPertanggunan = uangPertanggunan;
	}
	public BigDecimal getTotalPremi() {
		return totalPremi;
	}
	public void setTotalPremi(BigDecimal totalPremi) {
		this.totalPremi = totalPremi;
	}
	public String getNamaDepan() {
		return namaDepan;
	}
	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}
	public String getNamaBelakang() {
		return namaBelakang;
	}
	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getNoKtp() {
		return noKtp;
	}
	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public List<PdfAsuransiManfaatFlexiHealth> getBenefits() {
		return benefits;
	}
	public void setBenefits(List<PdfAsuransiManfaatFlexiHealth> benefits) {
		this.benefits = benefits;
	}
	public String getPeriodeLindung() {
		return periodeLindung;
	}
	public void setPeriodeLindung(String periodeLindung) {
		this.periodeLindung = periodeLindung;
	}
	
	
	
	

}
