package com.mabroor.ws.dto;

public class GeneratePdfAsuransiKecelakaanBenefitReqDto {

	private String benefit;
	private String value;
	private String type;
	public String getBenefit() {
		return benefit;
	}
	public String getValue() {
		return value;
	}
	public String getType() {
		return type;
	}
	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
