package com.mabroor.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AlquranJuzDto {

	@JsonProperty("_id")
	private int _id;
	@JsonProperty("aya")
	private int aya;
	@JsonProperty("juz")
	private int juz;
	@JsonProperty("en")
	private String en; 
	@JsonProperty("page")
	private int page;
	@JsonProperty("id")
	private String id;
	@JsonProperty("literasi")
	private String literasi;
	@JsonProperty("indopak")
	private String indopak;
	@JsonProperty("utsmani")
	private String utsmani;
	@JsonProperty("name")
	private String name;
	@JsonProperty("ms")
	private String ms;
	@JsonProperty("sura")
	private int sura;
	public int get_id() {
		return _id;
	}
	public void set_id(int _id) {
		this._id = _id;
	}
	public int getAya() {
		return aya;
	}
	public void setAya(int aya) {
		this.aya = aya;
	}
	public int getJuz() {
		return juz;
	}
	public void setJuz(int juz) {
		this.juz = juz;
	}
	public String getEn() {
		return en;
	}
	public void setEn(String en) {
		this.en = en;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLiterasi() {
		return literasi;
	}
	public void setLiterasi(String literasi) {
		this.literasi = literasi;
	}
	public String getIndopak() {
		return indopak;
	}
	public void setIndopak(String indopak) {
		this.indopak = indopak;
	}
	public String getUtsmani() {
		return utsmani;
	}
	public void setUtsmani(String utsmani) {
		this.utsmani = utsmani;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMs() {
		return ms;
	}
	public void setMs(String ms) {
		this.ms = ms;
	}
	public int getSura() {
		return sura;
	}
	public void setSura(int sura) {
		this.sura = sura;
	}
	
	
}
