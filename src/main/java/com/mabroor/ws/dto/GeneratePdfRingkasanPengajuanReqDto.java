package com.mabroor.ws.dto;

public class GeneratePdfRingkasanPengajuanReqDto {

    private String nama;
    private String jenisLayanan;
    private String partnerPembiayaan;
    private String jumlahPeminjaman;
    private String tenor;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisLayanan() {
        return jenisLayanan;
    }

    public void setJenisLayanan(String jenisLayanan) {
        this.jenisLayanan = jenisLayanan;
    }

    public String getPartnerPembiayaan() {
        return partnerPembiayaan;
    }

    public void setPartnerPembiayaan(String partnerPembiayaan) {
        this.partnerPembiayaan = partnerPembiayaan;
    }

    public String getJumlahPeminjaman() {
        return jumlahPeminjaman;
    }

    public void setJumlahPeminjaman(String jumlahPeminjaman) {
        this.jumlahPeminjaman = jumlahPeminjaman;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }
}
