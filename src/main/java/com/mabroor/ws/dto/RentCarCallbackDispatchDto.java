package com.mabroor.ws.dto;

public class RentCarCallbackDispatchDto {

	private String bookingOrderId;
	
	private String licensePlate;
	
	private String driverName;
	
	private String driverPhoneNumber;

	public String getBookingOrderId() {
		return bookingOrderId;
	}

	public void setBookingOrderId(String bookingOrderId) {
		this.bookingOrderId = bookingOrderId;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverPhoneNumber() {
		return driverPhoneNumber;
	}

	public void setDriverPhoneNumber(String driverPhoneNumber) {
		this.driverPhoneNumber = driverPhoneNumber;
	}
}
