package com.mabroor.ws.dto;

import java.util.Date;

public class MstPengajuanMotorDto {

	
	private Long id;
	
    private String integrationLeadsId;
	
	
    private String category;

	
    private String name;
    
   
    private String email;
    
    
    private String companyName;
    
    
    private String packageName;
    
    
    private String disclaimer;
    
   
    private String bestDeal;
    
   
    private String phone;
    
    private String transactionDateTime;
    
    private Long userId;
    
   
	private String nameDataUser;
	
	
	private String emailDataUser;
	
	
	private String phoneDataUser;

    
    private String location;
    
    
    private String colour;
    
   
    private String otrPrice;
    
   
    private String dp;
    
   
    private String amountDp;
    
    
    private String tenor;
    
    
    private String administration;
    
    
    private String installment;
    
    
    private String totalInstallment;
    
    
    private String insuranceType;
    
   
    private String insuranceMotor;
    
   
    private String insurancePayment ;
    
   
    private String addedTerorismInsurance;
    
   
    private String accidentInsurance;
    
    
    private String addedDisasterInsurance;
    
    
    private String insuranceNominal;
    
    
    private String insuranceExpansion;
    
    
    private String asuransiSiagaPlus;
    
   
    private String insurancePolicy;
    
    
    private String insuranceRate;
    
    
    private String totalInsuranceTransaction;
    
   
    private String totalInsurance;
    
   
    private String totalPayment1;
    
    
    private String payment;
    
   
    private String zipCode;
    
    
    private String interestFlat;
    
   
    private String interestSold;
    
    
    private String primaryDebtStart;
    
    
    private String fidusia;
    
    
    private String totalDebt;
    
   
    private String existingCustomer;
    
    
    private String contractNumber;
    
    
    private String fifgroupCardnumber;
    
    
    private String gender;
    
   
    private String birthDay;
    
    
    private String phoneOptional;
    
   
    private String ktpNumber;
    
    
    private String address;
    
    
    private String province;
    
    
    private String city;
    
   
    private String subDistrict;
    
    
    private String village;
    
   
    private String homeOwnership;
    
   
    private String maritalStatus;
    
   
    private String lastEducation;
    
    
    private String dependents;
    
    private String ktpPhoto;
    
    private String fotoUnit;
    
    
    private String kodePromo;
    
   
    private String namaPromo;
    
    private String vehicleType;
    
    private String pengajuanStatus;
    
    private String firstName;
    
    private String lastName;
    
    private String source;


	public String getIntegrationLeadsId() {
		return integrationLeadsId;
	}


	public void setIntegrationLeadsId(String integrationLeadsId) {
		this.integrationLeadsId = integrationLeadsId;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getPackageName() {
		return packageName;
	}


	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}


	public String getDisclaimer() {
		return disclaimer;
	}


	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}


	public String getPengajuanStatus() {
		return pengajuanStatus;
	}


	public void setPengajuanStatus(String pengajuanStatus) {
		this.pengajuanStatus = pengajuanStatus;
	}


	public String getBestDeal() {
		return bestDeal;
	}


	public void setBestDeal(String bestDeal) {
		this.bestDeal = bestDeal;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getTransactionDateTime() {
		return transactionDateTime;
	}


	public void setTransactionDateTime(String transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}


	public Long getUserId() {
		return userId;
	}


	public void setUserId(Long userId) {
		this.userId = userId;
	}


	public String getNameDataUser() {
		return nameDataUser;
	}


	public void setNameDataUser(String nameDataUser) {
		this.nameDataUser = nameDataUser;
	}


	public String getEmailDataUser() {
		return emailDataUser;
	}


	public void setEmailDataUser(String emailDataUser) {
		this.emailDataUser = emailDataUser;
	}


	public String getPhoneDataUser() {
		return phoneDataUser;
	}


	public void setPhoneDataUser(String phoneDataUser) {
		this.phoneDataUser = phoneDataUser;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getColour() {
		return colour;
	}


	public void setColour(String colour) {
		this.colour = colour;
	}


	public String getOtrPrice() {
		return otrPrice;
	}


	public void setOtrPrice(String otrPrice) {
		this.otrPrice = otrPrice;
	}


	public String getDp() {
		return dp;
	}


	public void setDp(String dp) {
		this.dp = dp;
	}


	public String getAmountDp() {
		return amountDp;
	}


	public void setAmountDp(String amountDp) {
		this.amountDp = amountDp;
	}


	public String getTenor() {
		return tenor;
	}


	public void setTenor(String tenor) {
		this.tenor = tenor;
	}


	public String getAdministration() {
		return administration;
	}


	public void setAdministration(String administration) {
		this.administration = administration;
	}


	public String getInstallment() {
		return installment;
	}


	public void setInstallment(String installment) {
		this.installment = installment;
	}


	public String getTotalInstallment() {
		return totalInstallment;
	}


	public void setTotalInstallment(String totalInstallment) {
		this.totalInstallment = totalInstallment;
	}


	public String getInsuranceType() {
		return insuranceType;
	}


	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}


	public String getInsuranceMotor() {
		return insuranceMotor;
	}


	public void setInsuranceMotor(String insuranceMotor) {
		this.insuranceMotor = insuranceMotor;
	}


	public String getInsurancePayment() {
		return insurancePayment;
	}


	public void setInsurancePayment(String insurancePayment) {
		this.insurancePayment = insurancePayment;
	}


	public String getAddedTerorismInsurance() {
		return addedTerorismInsurance;
	}


	public void setAddedTerorismInsurance(String addedTerorismInsurance) {
		this.addedTerorismInsurance = addedTerorismInsurance;
	}


	public String getAccidentInsurance() {
		return accidentInsurance;
	}


	public void setAccidentInsurance(String accidentInsurance) {
		this.accidentInsurance = accidentInsurance;
	}


	public String getAddedDisasterInsurance() {
		return addedDisasterInsurance;
	}


	public void setAddedDisasterInsurance(String addedDisasterInsurance) {
		this.addedDisasterInsurance = addedDisasterInsurance;
	}


	public String getInsuranceNominal() {
		return insuranceNominal;
	}


	public void setInsuranceNominal(String insuranceNominal) {
		this.insuranceNominal = insuranceNominal;
	}


	public String getInsuranceExpansion() {
		return insuranceExpansion;
	}


	public void setInsuranceExpansion(String insuranceExpansion) {
		this.insuranceExpansion = insuranceExpansion;
	}


	public String getAsuransiSiagaPlus() {
		return asuransiSiagaPlus;
	}


	public void setAsuransiSiagaPlus(String asuransiSiagaPlus) {
		this.asuransiSiagaPlus = asuransiSiagaPlus;
	}


	public String getInsurancePolicy() {
		return insurancePolicy;
	}


	public void setInsurancePolicy(String insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}


	public String getInsuranceRate() {
		return insuranceRate;
	}


	public void setInsuranceRate(String insuranceRate) {
		this.insuranceRate = insuranceRate;
	}


	public String getTotalInsuranceTransaction() {
		return totalInsuranceTransaction;
	}


	public void setTotalInsuranceTransaction(String totalInsuranceTransaction) {
		this.totalInsuranceTransaction = totalInsuranceTransaction;
	}


	public String getTotalInsurance() {
		return totalInsurance;
	}


	public void setTotalInsurance(String totalInsurance) {
		this.totalInsurance = totalInsurance;
	}


	public String getTotalPayment1() {
		return totalPayment1;
	}


	public void setTotalPayment1(String totalPayment1) {
		this.totalPayment1 = totalPayment1;
	}


	public String getPayment() {
		return payment;
	}


	public void setPayment(String payment) {
		this.payment = payment;
	}


	public String getZipCode() {
		return zipCode;
	}


	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	public String getInterestFlat() {
		return interestFlat;
	}


	public void setInterestFlat(String interestFlat) {
		this.interestFlat = interestFlat;
	}


	public String getInterestSold() {
		return interestSold;
	}


	public void setInterestSold(String interestSold) {
		this.interestSold = interestSold;
	}


	public String getPrimaryDebtStart() {
		return primaryDebtStart;
	}


	public void setPrimaryDebtStart(String primaryDebtStart) {
		this.primaryDebtStart = primaryDebtStart;
	}


	public String getFidusia() {
		return fidusia;
	}


	public void setFidusia(String fidusia) {
		this.fidusia = fidusia;
	}


	public String getTotalDebt() {
		return totalDebt;
	}


	public void setTotalDebt(String totalDebt) {
		this.totalDebt = totalDebt;
	}


	public String getExistingCustomer() {
		return existingCustomer;
	}


	public void setExistingCustomer(String existingCustomer) {
		this.existingCustomer = existingCustomer;
	}


	public String getFifgroupCardnumber() {
		return fifgroupCardnumber;
	}


	public void setFifgroupCardnumber(String fifgroupCardnumber) {
		this.fifgroupCardnumber = fifgroupCardnumber;
	}


	public String getContractNumber() {
		return contractNumber;
	}


	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getBirthDay() {
		return birthDay;
	}


	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}


	public String getPhoneOptional() {
		return phoneOptional;
	}


	public void setPhoneOptional(String phoneOptional) {
		this.phoneOptional = phoneOptional;
	}


	public String getKtpNumber() {
		return ktpNumber;
	}


	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getProvince() {
		return province;
	}


	public void setProvince(String province) {
		this.province = province;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getSubDistrict() {
		return subDistrict;
	}


	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}


	public String getHomeOwnership() {
		return homeOwnership;
	}


	public void setHomeOwnership(String homeOwnership) {
		this.homeOwnership = homeOwnership;
	}


	public String getMaritalStatus() {
		return maritalStatus;
	}


	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}


	public String getLastEducation() {
		return lastEducation;
	}


	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}


	public String getDependents() {
		return dependents;
	}


	public void setDependents(String dependents) {
		this.dependents = dependents;
	}


	public String getKtpPhoto() {
		return ktpPhoto;
	}


	public void setKtpPhoto(String ktpPhoto) {
		this.ktpPhoto = ktpPhoto;
	}


	public String getFotoUnit() {
		return fotoUnit;
	}


	public void setFotoUnit(String fotoUnit) {
		this.fotoUnit = fotoUnit;
	}


	public String getKodePromo() {
		return kodePromo;
	}


	public void setKodePromo(String kodePromo) {
		this.kodePromo = kodePromo;
	}


	public String getNamaPromo() {
		return namaPromo;
	}


	public void setNamaPromo(String namaPromo) {
		this.namaPromo = namaPromo;
	}


	public String getVehicleType() {
		return vehicleType;
	}


	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}


	public String getVillage() {
		return village;
	}


	public void setVillage(String village) {
		this.village = village;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}
   
}
