package com.mabroor.ws.dto;

public class GeneratePdfRingkasanPembiayaanUmrohDto {

    private String name;
    private String partnerPembiayaan;
    private String productPembiayaan;
    private String dp;
    private String Tenor;

    public String getPartnerPembiayaan() {
        return partnerPembiayaan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPartnerPembiayaan(String partnerPembiayaan) {
        this.partnerPembiayaan = partnerPembiayaan;
    }

    public String getProductPembiayaan() {
        return productPembiayaan;
    }

    public void setProductPembiayaan(String productPembiayaan) {
        this.productPembiayaan = productPembiayaan;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getTenor() {
        return Tenor;
    }

    public void setTenor(String tenor) {
        Tenor = tenor;
    }
}
