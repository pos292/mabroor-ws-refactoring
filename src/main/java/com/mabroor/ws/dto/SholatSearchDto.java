package com.mabroor.ws.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SholatSearchDto {

	private String location;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private String date;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
}
