package com.mabroor.ws.dto;

public class FIFTokenResultUserDto {
	
	private String userId;
	
	private String name;
	
	private String password;
	
	private String created;
	
	private String lastLogin;
	
	private Long roleId;
	
	private String userType;
	
	private String serviceOfficeCode;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getServiceOfficeCode() {
		return serviceOfficeCode;
	}

	public void setServiceOfficeCode(String serviceOfficeCode) {
		this.serviceOfficeCode = serviceOfficeCode;
	}
}
