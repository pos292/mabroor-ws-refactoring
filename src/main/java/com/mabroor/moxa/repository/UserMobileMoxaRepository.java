package com.mabroor.moxa.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mabroor.moxa.entity.MstUserMobileMoxa;

@Repository
@Transactional
public interface UserMobileMoxaRepository extends PagingAndSortingRepository<MstUserMobileMoxa, Long>{
	@Query(value = "SELECT * FROM mst_user_mobile WHERE phone = ?1 and status = '01'", nativeQuery = true)
	Optional<MstUserMobileMoxa> getOneByPhoneNumber(String phoneNumber);
}
