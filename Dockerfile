#
# Package stage
#
# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

# Add Maintainer Info
LABEL maintainer="ken.fan@ifabula.com"

# Make port 8080 available to the world outside this container
EXPOSE 5200

COPY target/mabroorws.jar mabroorws.jar

RUN apk update && apk add --upgrade bzip2 && apk add --upgrade e2fsprogs && apk add --upgrade freetype && apk add --upgrade krb5 && apk add --upgrade libjpeg-turbo && apk add --upgrade libtasn1 && apk add --upgrade libx11 && apk add --upgrade musl && apk add --upgrade openjdk8 && apk add --upgrade openssl && apk add --upgrade sqlite

ENV TZ="Asia/Jakarta"

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/mabroorws.jar"]

